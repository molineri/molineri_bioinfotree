#include "reader.h"
#include <sstream>

using namespace std;

FastaBlock::FastaBlock(const string& header, const string& content) : _header(header), _content(content)
{
}

FastaBlock::FastaBlock(const FastaBlock& obj) : _header(obj._header), _content(obj._content)
{
}

FastaReader::FastaReader(const char* const filename) : _filename(filename), _stream(filename)
{
	if (!_stream.is_open())
		throw FastaReaderException(string("cannot open ") + _filename);
	
	read_header();
}

bool FastaReader::has_more() const
{
	return !_stream.eof();
}

FastaBlock FastaReader::next()
{
	ostringstream content;
	string block_header(_header), line;
	
	while (true)
	{
		getline(_stream, line);
		if (_stream.bad())
			throw FastaReaderException(string("cannot read from ") + _filename);
		else if (_stream.eof())
			break;
		else if (line.empty())
			throw FastaReaderException(string("found an empty line in ") + _filename);
		else if (line[0] == '>')
		{
			set_header(line);
			break;
		}
		else
			content << line;
	}
	
	return FastaBlock(block_header, content.str());
}

void FastaReader::read_header()
{
	getline(_stream, _header);
	if (_stream.bad())
		throw FastaReaderException(string("cannot read from ") + _filename);
	
	if (!_header.empty())
	{
		if (_header[0] != '>')
			throw FastaReaderException(string("invalid header found in ") + _filename + ": " + _header);
		
		set_header(_header);
	}
}

void FastaReader::set_header(const std::string& raw)
{
	_header = raw.substr(1);
}
