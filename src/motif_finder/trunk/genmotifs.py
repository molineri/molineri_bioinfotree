#!/usr/bin/env python
from random import choice, randint
from sys import argv

def generate_sequence(length):
	codes = 'ACGTRYSWKMBDHVN'
	return ''.join(choice(codes) for i in xrange(length))

def main():
	num = int(argv[1])
	for i in xrange(num):
		print '>%d' % i
		print generate_sequence(randint(20, 30))

if __name__ == '__main__':
	main()
