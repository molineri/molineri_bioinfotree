#ifndef MOTIF_H
#define MOTIF_H

#include <string>
#include <vector>

class FastaBlock;

class Motif
{
public:
	Motif(const FastaBlock& block);
	~Motif();
	bool match(const char c, const unsigned long pos);
	const std::string& name() const { return _name; }
	unsigned long start_position() const { return _start_position; }
	unsigned int length() const { return _length; }

protected:
	void fill_sequence(const std::string& sequence);
	
	std::string _name;
	int* _sequence;
	unsigned int _length;
	unsigned int _match_pos;
	unsigned long _start_position;

private:
	Motif(const Motif& obj);
	Motif& operator=(const Motif& rhs);
};

class MotifArray
{
public:
	MotifArray();
	~MotifArray();
	void add(const FastaBlock& block);
	unsigned int length() const { return _motifs.size(); }
	Motif& operator[](const unsigned int idx) { return *_motifs[idx]; }

protected:
	std::vector<Motif*> _motifs;

private:
	MotifArray(const Motif& obj);
	MotifArray& operator=(const MotifArray& rhs);
};

#endif //MOTIF_H
