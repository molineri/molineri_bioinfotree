#ifndef BUFFER_H
#define BUFFER_H

#include <sstream>

class MultiBuffer
{
public:
	MultiBuffer(std::ostream& out, const unsigned int max_size);
	~MultiBuffer();
	void write(const std::string& query, const std::string& motif, const unsigned int start, const unsigned int stop);

protected:
	std::ostream& _out;
	const unsigned int _max_size;
	const unsigned int _thread_num;
	std::ostringstream** _buffers;
	
private:
	MultiBuffer(const MultiBuffer& obj);
	MultiBuffer& operator=(const MultiBuffer& rhs);
};

#endif //BUFFER_H
