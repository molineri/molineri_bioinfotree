#ifndef READER_H
#define READER_H

#include <fstream>
#include <stdexcept>
#include <string>

class FastaBlock
{
public:
	FastaBlock(const std::string& header, const std::string& content);
	FastaBlock(const FastaBlock& obj);
	const std::string& header() const { return _header; }
	const std::string& content() const { return _content; }
	
protected:
	const std::string _header;
	const std::string _content;

private:
	FastaBlock& operator=(const FastaBlock& rhs);
};

class FastaReader
{
public:
	FastaReader(const char* const filename);
	bool has_more() const;
	FastaBlock next();

protected:
	void read_header();
	void set_header(const std::string& raw);
	
	std::string _filename;
	std::ifstream _stream;
	std::string _header;

private:
	FastaReader(const FastaReader& obj);
	FastaReader& operator=(const FastaReader& rhs);
};

class FastaReaderException : public std::runtime_error
{
public:
	FastaReaderException(const std::string& what) : std::runtime_error(what) {}
};

#endif //READER_H
