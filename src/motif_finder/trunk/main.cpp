#include "buffer.h"
#include <iostream>
#include "motif.h"
#include "reader.h"
#include <stdlib.h>
#include <string.h>

using namespace std;

const char* motif_file;
const char* sequence_file;

void parse_args(int argc, char** argv)
{
	for (int i = 1; i < argc; ++i)
	{
		if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0)
		{
			cerr << "Usage: " << argv[0] << " MOTIF_FILE SEQUENCE_FILE" << endl << endl;
			exit(1);
		}
	}
	
	if (argc != 3)
	{
		cerr << "Unexpected argument number." << endl;
		exit(1);
	}
	
	motif_file = argv[1];
	sequence_file = argv[2];
}

void load_motifs(const char* const filename, MotifArray& motifs)
{
	FastaReader reader(filename);
	while (reader.has_more())
	{
		FastaBlock block = reader.next();
		motifs.add(block);
	}
}

int main(int argc, char** argv)
{
	parse_args(argc, argv);
	
	MotifArray motifs;
	load_motifs(motif_file, motifs);
	
	FastaReader reader(sequence_file);
	while (reader.has_more())
	{
		FastaBlock block = reader.next();
		const string& sequence = block.content();
		
		#pragma omp parallel
		{
			MultiBuffer buffer(cout, 10*1024*1024);
			
			for (unsigned int pos = 0; pos < sequence.length(); ++pos)
			{
				int m;
				
				#pragma omp for private(m) nowait schedule(static)
				for (m = 0; m < (int)motifs.length(); ++m)
				{
					if (motifs[m].match(sequence[pos], pos))
					{
						const unsigned int start = motifs[m].start_position();
						const unsigned int stop = start + motifs[m].length();
						buffer.write(block.header(), motifs[m].name(), start, stop);
					}
				}
			}
		}
	}
	
	return 0;
}