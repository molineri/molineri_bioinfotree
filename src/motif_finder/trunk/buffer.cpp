#include "buffer.h"
#include <omp.h>
#include <sstream>

using namespace std;

MultiBuffer::MultiBuffer(ostream& out, const unsigned int max_size) : _out(out), _max_size(max_size), _thread_num(omp_get_num_threads())
{
	_buffers = new ostringstream*[_thread_num];
	for (unsigned int i = 0; i < _thread_num; ++i)
		_buffers[i] = new ostringstream();
}

MultiBuffer::~MultiBuffer()
{
	#pragma omp critical
	{
		for (unsigned int i = 0; i < _thread_num; ++i)
		{
			if (_buffers[i]->tellp() > 0)
				_out << _buffers[i]->str();
			delete _buffers[i];
		}
		
		delete[] _buffers;
	}
}

void MultiBuffer::write(const string& query, const string& motif, const unsigned int start, const unsigned int stop)
{
	const unsigned int i = omp_get_thread_num();
	*_buffers[i] << query << "\t" << motif << "\t" << start << "\t" << stop << endl;
	
	if (_buffers[i]->tellp() >= _max_size)
	{
		#pragma omp critical
		{
			_out << _buffers[i]->str();
		}
		
		delete _buffers[i];
		_buffers[i] = new ostringstream();
	}
}
