#include "motif.h"
#include "reader.h"
#include <stdexcept>
#include <strings.h>
#include <vector>

using namespace std;

enum Base { A = 1, C = 2, G = 4, T = 8 };

int* build_iupac_to_flags()
{
	static int flags[256];
	bzero(flags, sizeof(int)*256);
	
	flags[(int)'A'] = A;
	flags[(int)'C'] = C;
	flags[(int)'G'] = G;
	flags[(int)'T'] = T;
	flags[(int)'R'] = A | G;
	flags[(int)'Y'] = C | T;
	flags[(int)'S'] = G | C;
	flags[(int)'W'] = A | T;
	flags[(int)'K'] = G | T;
	flags[(int)'M'] = A | C;
	flags[(int)'B'] = C | G | T;
	flags[(int)'D'] = A | G | T;
	flags[(int)'H'] = A | C | T;
	flags[(int)'V'] = A | C | G;
	flags[(int)'N'] = A | C | G | T;
	
	return flags;
}

int* build_nucleotide_to_flag()
{
	static int flags[256];
	bzero(flags, sizeof(int)*256);
	
	flags[(int)'A'] = A;
	flags[(int)'C'] = C;
	flags[(int)'G'] = G;
	flags[(int)'T'] = T;
	
	return flags;
}

int* iupac_to_flags = build_iupac_to_flags();
int* nucleotide_to_flag = build_nucleotide_to_flag();

Motif::Motif(const FastaBlock& block) : _name(block.header()), _sequence(new int[block.content().length()]), \
                                        _length(block.content().length()), _match_pos(0)
{
	fill_sequence(block.content());
}

Motif::~Motif()
{
	delete[] _sequence;
}

bool Motif::match(const char c, const unsigned long pos)
{
	const int flag = nucleotide_to_flag[(int)c];
	if ((_sequence[_match_pos] & flag) == flag)
	{
		if (_match_pos == 0)
			_start_position = pos;
		
		++_match_pos;
		
		if (_match_pos == _length)
		{
			_match_pos = 0;
			return true;
		}
		else
			return false;
	}
	else
	{
		_match_pos = 0;
		return false;
	}
}

void Motif::fill_sequence(const string& sequence)
{
	for (unsigned int i = 0; i < sequence.length(); ++i)
	{
		const int flags = iupac_to_flags[(int)sequence[i]];
		if (flags == 0)
			throw runtime_error(string("invalid IUPAC code in motif ") + _name);
		_sequence[i] = flags;
	}
}

MotifArray::MotifArray()
{
}

MotifArray::~MotifArray()
{
	vector<Motif*>::iterator it, end;
	for (it = _motifs.begin(), end = _motifs.end(); it != end; ++it)
		delete *it;
}

void MotifArray::add(const FastaBlock& block)
{
	Motif *m = new Motif(block);
	_motifs.push_back(m);
}
