#include "setting.h"
#include <stdlib.h>

int *edges = NULL;
int *mark = NULL;
int nedges;
int nodes;

void  listallamarkcomp();

int load_num(char *repr, char* type) {
  char *endptr;
  int res;

  int invalid_num = repr == '\0';
  if (!invalid_num) {
    res = strtol(repr, &endptr, 10);
    invalid_num = *endptr != 0 || res < 1;
  }
  if (invalid_num) {
    fprintf(stderr, "Invalid %s number.\n", type);
    exit(1);
  }

  return res;
}

int main(int argc, char** argv) {
  int maxlen=1024;
  char str1[maxlen];
  char str2[maxlen];
  int n1, n2;
  int v, n, m;
  char **extlabel; 

  if (argc != 3) {
    fprintf(stderr, "Unexpected argument number.\n");
    return 1;
  }

  nodes = load_num(argv[1], "node");
  nedges = load_num(argv[2], "edge");
  extlabel = (char **)malloc(nodes*sizeof(char*));
  edges = (int *)malloc(nedges*2*sizeof(int));

  m=0; 
  n=0;
  while (m < 2*nedges) {

    if (fscanf(stdin, "%s\t%s\n", &str1, &str2) != 2) {
      fprintf(stderr, "Invalid content at line %d:\n", m+1);
      return 1;
    }
   
    n1=-1;
    n2=-1;
    for (v=0; v<n; v++){
      if ( strcmp(str1,extlabel[v]) == 0 )
	n1=v;
      if ( strcmp(str2,extlabel[v]) == 0 )
	n2=v;
      if (n1>=0 && n2>=0) break;
    }
    if (n1<0) {
      extlabel[n] = malloc(((int)strlen(str1)+1)*sizeof(char));
      strcpy(extlabel[n], str1);
      n1 = n;
      n++;
    }
    if (n2<0) {
      if (strcmp(str1,str2)!=0) {
	extlabel[n] = malloc(((int)strlen(str2)+1)*sizeof(char));
	strcpy(extlabel[n], str2);
	n2 = n;
	n++;
      }
      else {
	n2=n1;
      }
    }
  
    if (n>nodes){
      fprintf(stderr, "Unexpected number of nodes.\n");
      return 1;
    }

    edges[m] = n1;
    edges[m+1] = n2;
   
    m+=2;

  }
  
  if (!feof(stdin)) {
    fprintf(stderr, "Unexpected content after line %d.\n", m+1);
    return 1;
  }
  else if (m != 2*nedges) {
    fprintf(stderr, "Premature end of input at line %d.\n", m+1);
    fprintf(stderr, "%d %d\n", m, nedges);
    return 1;
  } else if (n !=nodes) {
    fprintf(stderr, "Mismatch in node count.\n");
    fprintf(stderr, "%d %d\n", n, nodes);
    return 1;
  }
  
  mark = (int *)calloc(nodes,sizeof(int));
  listallamarkcomp();

  //output
  for (v=0; v<nodes; v++){
    printf("%s\t%d\n", extlabel[v], mark[v]);
  }

  free(mark);
  free(edges);
  for (v=0; v<nodes; v++)
    free(extlabel[v]);
  free(extlabel[v]);
  return 0;
}
