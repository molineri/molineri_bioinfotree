#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <sys/stat.h>

#define max(a,b) ((a<b)? b : a)
#define min(a,b) ((a<b)? a : b)
