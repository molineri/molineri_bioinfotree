#include "simapdb.h"
#include "connc.h"

extern int nodes;
extern int *its;
extern int *sti;
extern int *selfscores;
extern int *lstaxid;
extern int *bioclass;
extern int *lslength;
extern int *z;
extern int *mark;
extern connc *cc;
extern int *jcsort;
extern int ncomp;
extern int zmax;

int sizecomp (const void *n1,const  void *n2 ) { 
  /* casting */
  const int* nn1 = (const int *)n1;
  const int* nn2 = (const int *)n2;
  if (cc[*nn1].size > cc[*nn2].size ) return(-1);
  if (cc[*nn1].size < cc[*nn2].size ) return(1);
  return(0);
}

connc *getconncomp(double p){
  int k,nc,j,b;
  int *icomp;
  char fn[128];
  FILE *fd;
  connc* c;  

  mark = (int *)malloc(nodes*sizeof(int));
  sprintf(fn,"/raid/simap/ungznewrelease/conncomps/markcomps_w_%.5lf",p);   
  fd = fopen(fn,"rb"); 
  if  (fd != NULL ) { 
    fread(mark, sizeof(int), nodes,fd);
    fclose(fd);
  } else {
    printf("Errore di apertura file %s\n", fn);
    exit(0);
  }

  z = (int *)malloc(nodes*sizeof(int));
  sprintf(fn,"/raid/simap/ungznewrelease/zlist/zlist_w_%.5lf",p); 
  fd = fopen(fn,"rb"); 
  if  (fd != NULL ) { 
    fread(z, sizeof(int), nodes,fd);
    fclose(fd);
  } else {
    printf("Errore di apertura file %s\n", fn);
    exit(0);
  }

  zmax = 0;
  ncomp = 0;  
  /* find number of components and zmax */
  icomp = (int *)malloc(nodes*sizeof(int));
  for (k=0; k<nodes; k++) {
    icomp[mark[k]]++;
    ncomp = max(ncomp, mark[k]);
    zmax = max(zmax, z[k]);
  }
  printf("%d %d\n",ncomp,zmax);
  icomp = realloc(icomp, (ncomp+1)*sizeof(int));
  nc = 0;
  for (j=0; j<=ncomp; j++){
    if (icomp[j] != 0) {
      icomp[j] = ++nc;
    }
  }
  ncomp = nc;
  
  /* comp -> list of all components */
  cc  = (connc *)malloc(ncomp*sizeof(connc));
  jcsort = malloc(ncomp*sizeof(int));
  for (j=0; j<ncomp; j++) {
    cc[j].size = 0;
    cc[j].nedge = 0;
    cc[j].nds = NULL;
    jcsort[j] = j;
    for (b=0; b<biodivision; b++)
      cc[j].bios[b] = 0; 
    cc[j].nspecies = 0;
  }
  /* filling components */
  for (k=0; k<nodes; k++){ 
    nc = icomp[mark[k]];
    c = &cc[nc-1];
    c->size +=1;
    c->nedge += (long long int)z[k];
    c->nds = (int *)realloc(c->nds, c->size*sizeof(int));
    c->nds[c->size -1] = k;  
  }
  /* sorting components */
  qsort(jcsort, ncomp, sizeof(int), sizecomp);

  return cc;
}

void freecomps(){
  int j,js;

  for (j=0; j<ncomp;j++){
    js = jcsort[j];
    free(cc[js].nds);
  }
  free(cc);
  free(jcsort);
  free(z);
  free(mark);
}
