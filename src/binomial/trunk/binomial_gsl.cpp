#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <gsl/gsl_cdf.h>



using namespace std;


int setup(int argc, char* argv[]);
void usage(char** argv);

bool options_p=false;
bool options_n=false;
bool options_k=false;
float p; 
int N,K;

int main( int argc, char* argv[] ){

	//controllo le opzioni passate da riga di comando
	if(setup(argc, argv)){
		cout << "main:: termined because setup error"<<endl;
		return(-1);
	}

	cout << gsl_cdf_binomial_Q(K, p, N) << endl;
}


int setup(int argc, char* argv[]){
	
	////////////////////////////////////////////////////////////////
	// gestione delle opzioni
	//
	int i;
	while((i=getopt(argc, argv, "p:n:k:"))!=-1){
		switch(i){
			case 'p':
				p=atof(optarg);
				options_p=true;
				break;
			case 'n':
				N=atoi(optarg);
				options_n=true;
				break;
			case 'k':
				K=atoi(optarg);
				options_k=true;
				break;
			default:
				cerr<<"wrong options given"<<endl;
				usage(argv);
				return -1;
		}
	}
	if (!options_p or !options_n or !options_k) {
		cerr<<"wrong options given"<<endl;
		usage(argv);
		return -1;
	}

	if ( (p<=0) or (p>1) or (N<=0) or (N<K) ) {
		cerr << "ERROR: p<=0 or n<=0 or n<k" << endl;
		usage(argv);
		return -1;
	}


	return(0);
}


void usage(char** argv){
	cout << "usage: " << argv[0] <<  " -p 0.48 -n 5000 -k 200"<<endl
		<<"\t-p probability of a single positive extraction"<<endl
		<<"\t-n number of extractions"<<endl
		<<"\t-k number of positive extractions"<<endl
		<<endl
		<<"Return the value of the inverse cumulative binomial distribution (probability p, trial n) at the point (success) k"<< endl;
	exit(-1);
}

