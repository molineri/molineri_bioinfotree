#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>



using namespace std;


int setup(int argc, char* argv[]);
void usage(char** argv);

bool options_p=false;
bool options_n=false;
bool options_k=false;
float p; 
int N,K;

double log_binom(int n, int k){
	double a=0.;
	for(int i = n-k+1; i <= n; i++){
		a += log(i)/log(10);
	}
	for(int i = 1; i <= k; i++){
		a -= log(i)/log(10);
	}
	//cout << n << "\t" << k << "\t" << pow(10,a) << endl;
	//exit(-1);
	return a;
}

int main( int argc, char* argv[] ){

	//controllo le opzioni passate da riga di comando
	if(setup(argc, argv)){
		cout << "main:: termined because setup error"<<endl;
		return(-1);
	}

	double cum = 0.0;
	int j;

	for (j=K; j<=N; j++) {
		cum += pow(10,log_binom(N,j)) * (pow(p,j) * pow(1-p, N-j));
	}
	cout << cum << endl;
}


int setup(int argc, char* argv[]){
	
	////////////////////////////////////////////////////////////////
	// gestione delle opzioni
	//
	int i;
	while((i=getopt(argc, argv, "p:n:k:"))!=-1){
		switch(i){
			case 'p':
				p=atof(optarg);
				options_p=true;
				break;
			case 'n':
				N=atoi(optarg);
				options_n=true;
				break;
			case 'k':
				K=atoi(optarg);
				options_k=true;
				break;
			default:
				cerr<<"wrong options given"<<endl;
				usage(argv);
				return -1;
		}
	}
	if (!options_p or !options_n or !options_k) {
		cerr<<"wrong options given"<<endl;
		usage(argv);
		return -1;
	}

	if ( (p<=0) or (p>1) or (N<=0) or (N<K) ) {
		cerr << "ERROR: p<=0 or n<=0 or n<k" << endl;
		usage(argv);
		return -1;
	}


	return(0);
}


void usage(char** argv){
	cout << "usage: " << argv[0] <<  " -p 0.48 -n 5000 -k 200"<<endl
		<<"\t-p probability of a single positive extraction"<<endl
		<<"\t-n number of extractions"<<endl
		<<"\t-k number of positive extractions"<<endl;
	exit(-1);
}

