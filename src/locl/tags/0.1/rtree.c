#include "alist.h"
#include <assert.h>
#include "rtree.h"
#include <stdlib.h>
#include <strings.h>

#ifndef NDEBUG
#include <stdio.h>
#endif

#define MIN(x,y) (x<y?x:y)
#define MAX(x,y) (x>y?x:y)
#define FAIL_IF(cond) if (cond) goto fail;

struct node
{
	struct node* parent;
	unsigned int slots;
	rectangle_t** mbrs;
	struct node** refs;
};
typedef struct node* node_t;

struct reinsert_info
{
	unsigned int height;
	rectangle_t* mbr;
	node_t node;
};
typedef struct reinsert_info* reinsert_info_t;

struct rtree
{
	unsigned int fanout;
	unsigned int min_slots;
	u_int32_t max_gap;
	node_t root;
	unsigned int depth;
	unsigned int size;

	alist_t reinsert_set;
	alist_t nl, nnl;
	
	unsigned int insertions;
	unsigned int scans;
	unsigned int overlap_checks;
	unsigned int deletions;
	unsigned int reinsertions;
};

static int append_item(rtree_t rt, node_t parent, rectangle_t* mbr, node_t node);

static rectangle_t* dup_rect(const rectangle_t* rect)
{
	rectangle_t* res = malloc(sizeof(rectangle_t));
	if (res != NULL)
		bcopy(rect, res, sizeof(rectangle_t));
	return res;
}

static node_t make_node(const char leaf, const unsigned int fanout)
{
	node_t n = (node_t)malloc(sizeof(struct node));
	if (n == NULL)
		return NULL;
	
	n->mbrs = (rectangle_t**)calloc(fanout, sizeof(rectangle_t*));
	if (n->mbrs == NULL)
	{
		free(n);
		return NULL;
	}
	
	if (!leaf)
	{
		n->refs = (node_t*)calloc(fanout, sizeof(node_t));
		if (n->refs == NULL)
		{
			free(n->mbrs);
			free(n);
			return NULL;
		}
	}
	else
		n->refs = NULL;
	
	n->parent = NULL;
	n->slots = 0;
	return n;
}

static void free_node(node_t node)
{
	free(node->refs);
	free(node->mbrs);
	free(node);
}

static int is_leaf(node_t node)
{
	return node->refs == NULL;
}

static unsigned int node_index(const node_t parent, const node_t node)
{
	assert(!is_leaf(parent));
	
	unsigned int i;
	for (i = 0; i < parent->slots && parent->refs[i] != node;)
		i++;
	return i;
}

static unsigned int free_index(const node_t node)
{
	return node->slots;
}

static u_int64_t rectangle_area(const rectangle_t* rect)
{
	return (rect->x2 - rect->x1) * (rect->y2 - rect->y1);
}

static u_int64_t joined_area(const rectangle_t* a, const rectangle_t* b)
{
	const u_int64_t p1 = MAX(a->x2, b->x2) - MIN(a->x1, b->x1);
	const u_int64_t p2 = MAX(a->y2, b->y2) - MIN(a->y1, b->y1);
	return p1 * p2;
}

static u_int64_t rectangle_enlargment(const rectangle_t* a, const rectangle_t* b, u_int64_t* rect_area)
{
	*rect_area = rectangle_area(a);
	const u_int64_t enlarged_area = joined_area(a, b);
	return enlarged_area - *rect_area;
}

static node_t choose_node(node_t node, const rectangle_t* rect, unsigned int depth)
{
	while (!is_leaf(node) && depth > 0)
	{
		u_int64_t rect_area;
		u_int64_t min_enlargment = rectangle_enlargment(node->mbrs[0], rect, &rect_area);
		unsigned int j = 0;
		
		unsigned int i;
		for (i = 1; i < node->slots; i++)
		{
			rectangle_t* mbr = node->mbrs[i];
			const u_int64_t enlargment = rectangle_enlargment(mbr, rect, &rect_area);
			if (enlargment < min_enlargment || (enlargment == min_enlargment && rect_area < rectangle_area(rect)))
			{
				min_enlargment = enlargment;
				j = i;
			}
		}
		
		node = node->refs[j];
		depth--;
	}
	
	assert(depth == 0);
	return node;
}

static int rect_overlapped(const rectangle_t* a, const rectangle_t* b, const u_int32_t max_gap)
{
	return !(a->x1 > b->x2 + max_gap || a->x2 + max_gap < b->x1 || \
	         a->y1 > b->y2 + max_gap || a->y2 + max_gap < b->y1);
}

static rectangle_t* update_rect(rectangle_t* a, const rectangle_t* b)
{
	a->x1 = MIN(a->x1, b->x1);
	a->x2 = MAX(a->x2, b->x2);
	a->y1 = MIN(a->y1, b->y1);
	a->y2 = MAX(a->y2, b->y2);
	return a;
}

static int remove_overlapping_rects(rtree_t rt, node_t leaf, rectangle_t* mbr)
{
	assert(is_leaf(leaf));
	
	unsigned int i, l;
	for (i = 0, l = leaf->slots; i < l;)
	{
		rt->overlap_checks++;
		if (rect_overlapped(leaf->mbrs[i], mbr, rt->max_gap))
		{
			update_rect(mbr, leaf->mbrs[i]);
			free(leaf->mbrs[i]);
			l--;
			if (i != l)
				leaf->mbrs[i] = leaf->mbrs[l];
			
			rt->size--;
			rt->deletions++;
		}
		else
			i++;
	}
	
	if (l != leaf->slots)
	{
		leaf->slots = l;
		return 1;
	}
	else
		return 0;
}

static void adjust_tree(node_t node)
{
	node_t parent;
	while ((parent = node->parent) != NULL)
	{
		assert(!is_leaf(parent));
		
		unsigned int i = node_index(parent, node);
		assert(i < 4);
		assert(parent->refs[i] == node);
		assert(node->slots > 0);
		rectangle_t* mbr = parent->mbrs[i];
		rectangle_t old_mbr;
		bcopy(mbr, &old_mbr, sizeof(rectangle_t));
		
		bcopy(node->mbrs[0], mbr, sizeof(rectangle_t));
		for (i = 1; i < node->slots; i++)
			update_rect(mbr, node->mbrs[i]);
		
		if (bcmp(&old_mbr, mbr, sizeof(rectangle_t)) != 0)
			node = parent;
		else
			break;
	}
}

static void swap_items(rectangle_t** mbrs, node_t* refs, const unsigned int from, const unsigned int to)
{
	if (from != to)
	{
		rectangle_t* mbr_swap = mbrs[to];
		mbrs[to] = mbrs[from];
		mbrs[from] = mbr_swap;
		
		if (refs)
		{
			node_t ref_swap = refs[to];
			refs[to] = refs[from];
			refs[from] = ref_swap;
		}
	}
}

static void swap_lists(alist_t* a, alist_t* b)
{
	alist_t swap = *a;
	*a = *b;
	*b = swap;
}

static int condense_tree(rtree_t rt, node_t node)
{
	assert(alist_size(rt->reinsert_set) == 0);
	
	node_t parent;
	reinsert_info_t info;
	unsigned int i, height = 0;
	while ((parent = node->parent) != NULL && node->slots < rt->min_slots)
	{
		assert(!is_leaf(parent));
		i = node_index(parent, node);
		assert(i < rt->fanout);
		
		free(parent->mbrs[i]);
		swap_items(parent->mbrs, parent->refs, --(parent->slots), i);
		
		for (i = 0; i < node->slots; i++)
		{
			info = (reinsert_info_t)calloc(1, sizeof(struct reinsert_info));
			FAIL_IF(!info);
			
			info->height = height;
			info->mbr = node->mbrs[i];
			if (!is_leaf(node))
				info->node = node->refs[i];
			
			FAIL_IF(!alist_append(rt->reinsert_set, info));
		}
		
		free_node(node);
		node = parent;
		height++;
	}
	
	if (parent)
		adjust_tree(node);
	else if (!is_leaf(node) && node->slots == 1)
	{
		free(node->mbrs[0]);
		node->refs[0]->parent = NULL;
		rt->root = node->refs[0];
		free_node(node);
		rt->depth--;
	}
	
	unsigned int l;
	for (i = 0, l = alist_size(rt->reinsert_set); i < l; i++)
	{
		info = (reinsert_info_t)alist_get(rt->reinsert_set, i);
		parent = choose_node(rt->root, info->mbr, rt->depth - info->height);
		append_item(rt, parent, info->mbr, info->node);
		free(info);
	}
	
	alist_clear(rt->reinsert_set);
	rt->reinsertions += l;
	
	return 1;
	
fail:
#warning missing proper cleanup in condense_tree
	//TODO: proper cleanup
	return 0;
}

int remove_overlapping_and_condense_tree(rtree_t rt, rectangle_t* mbr, int* update)
{
	assert(alist_size(rt->nl) == 0);
	assert(alist_size(rt->nnl) == 0);
	
	*update = 0;
	FAIL_IF(!alist_append(rt->nl, rt->root));
	
#ifndef NDEBUG
	int leaves = 0;
#endif
	do
	{
		unsigned int i, j, l;
		for (i = 0, l = alist_size(rt->nl); i < l; i++)
		{
			node_t node = (node_t)alist_get(rt->nl, i);
			if (is_leaf(node))
			{
#ifndef NDEBUG
				leaves = 1;
#endif
				if (remove_overlapping_rects(rt, node, mbr))
				{
					*update = 1;
					if (node->slots >= rt->min_slots)
						adjust_tree(node);
					else
					{
						FAIL_IF(!condense_tree(rt, node));
					}
				}
			}
			else
			{
				assert(!leaves);
				for (j = 0; j < node->slots; j++)
				{
					rt->overlap_checks++;
					if (rect_overlapped(node->mbrs[j], mbr, rt->max_gap))
					{
						FAIL_IF(!alist_append(rt->nnl, node->refs[j]));
					}
				}
			}
		}
		
		alist_clear(rt->nl);
		swap_lists(&rt->nl, &rt->nnl);
	} while (alist_size(rt->nl));
	
	return 1;

fail:
	alist_clear(rt->nl);
	alist_clear(rt->nnl);
	return 0;
}

static int remove_overlapping_and_update_mbr(rtree_t rt, rectangle_t* mbr)
{
	int update;
	do
	{
		if (!remove_overlapping_and_condense_tree(rt, mbr, &update))
			return 0;
		rt->scans++;
	} while (update);
	
	return 1;
}

static int make_new_root(rtree_t rt, node_t node1, rectangle_t* mbr2, node_t node2)
{
	rectangle_t* mbr1 = NULL;
	node_t root = NULL;
	
	mbr1 = (rectangle_t*)malloc(sizeof(rectangle_t));
	FAIL_IF(!mbr1);
	
	assert(node1->slots > 0);
	bcopy(node1->mbrs[0], mbr1, sizeof(rectangle_t));
	unsigned int i;
	for (i = 1; i < node1->slots; i++)
		update_rect(mbr1, node1->mbrs[i]);
	
	root = make_node(0, rt->fanout);
	FAIL_IF(!root);
	
	root->mbrs[0] = mbr1;
	root->mbrs[1] = mbr2;
	root->refs[0] = node1;
	root->refs[1] = node2;
	
	node1->parent = root;
	node2->parent = root;
	
	root->slots = 2;
	rt->root = root;
	rt->depth++;
	return 1;

fail:
	if (mbr1 != NULL)
		free(mbr1);
	if (root != NULL)
		free_node(root);
	return 0;
}

static void set_item(node_t node, const unsigned int pos, rectangle_t* mbr, node_t ref)
{
	node->mbrs[pos] = mbr;
	if (ref == NULL)
		assert(is_leaf(node));
	else
	{
		ref->parent = node;
		node->refs[pos] = ref;
	}
	node->slots++;
	
	adjust_tree(node);
}

static int pick_seeds(rectangle_t** mbrs, const unsigned int mbr_num, unsigned int* k, unsigned int* l)
{
	unsigned int i;
	u_int64_t* ds = (u_int64_t*)alloca(sizeof(u_int64_t)* mbr_num);
	if (!ds)
		return 0;
	for (i = 0; i < mbr_num; i++)
		ds[i] = rectangle_area(mbrs[i]);
	
	unsigned int j;
	u_int64_t max_diff = 0;
	for (i = 0; i < mbr_num-1; i++)
	{
		for (j = i+1; j < mbr_num; j++)
		{
			const u_int64_t diff = joined_area(mbrs[i], mbrs[j]) - ds[i] - ds[j];
			if (diff >= max_diff)
			{
				max_diff = diff;
				*k = i;
				*l = j;
			}
		}
	}
	
	return 1;
}

static int split_and_append(rtree_t rt, node_t parent, rectangle_t* mbr, node_t node)
{
	assert(parent->slots == rt->fanout);
	
	node_t ll = NULL;
	rectangle_t* ll_mbr = NULL;
	rectangle_t** mbrs = NULL;
	node_t* nodes = NULL;
	
	ll = make_node(is_leaf(parent), rt->fanout);
	FAIL_IF(!ll);
	
	ll_mbr = (rectangle_t*)malloc(sizeof(rectangle_t));
	FAIL_IF(!ll_mbr);
	
	mbrs = alloca(sizeof(rectangle_t)*(rt->fanout+1));
	FAIL_IF(!mbrs);
	if (node)
	{
		nodes = alloca(sizeof(node_t)*(rt->fanout+1));
		FAIL_IF(!nodes);
	}
	
	unsigned int i;
	for (i = 0; i < parent->slots; i++)
	{
		mbrs[i] = parent->mbrs[i];
		if (node)
			nodes[i] = parent->refs[i];
	}
	mbrs[i] = mbr;
	if (node)
		nodes[i] = node;
	
	unsigned int n = i;
	unsigned int k, l;
	FAIL_IF(!pick_seeds(mbrs, n+1, &k, &l));
	
	ll->mbrs[0] = mbrs[l];
	if (node)
	{
		ll->refs[0] = nodes[l];
		nodes[l]->parent = ll;
	}
	bcopy(ll->mbrs[0], ll_mbr, sizeof(rectangle_t));
	ll->slots = 1;
	if (l != n)
	{
		mbrs[l] = mbrs[n];
		if (node)
			nodes[l] = nodes[n];
	}
	n--;
	
	rectangle_t parent_mbr;
	parent->mbrs[0] = mbrs[k];
	if (node)
	{
		parent->refs[0] = nodes[k];
		nodes[k]->parent = parent;
	}
	bcopy(parent->mbrs[0], &parent_mbr, sizeof(rectangle_t));
	parent->slots = 1;
	if (k != n)
	{
		mbrs[k] = mbrs[n];
		if (node)
			nodes[k] = nodes[n];
	}
	
	unsigned int j = 0;
	while (n > 0)
	{
		if (rt->min_slots - parent->slots == n)
		{
			j = 0;
			l = 0;
		}
		else if (rt->min_slots - ll->slots == n)
		{
			j = 0;
			l = 1;
		}
		else
		{
			u_int64_t area, d1, d2, diff, max_diff = 0;
			for (i = 0; i < n; i++)
			{
				d1 = rectangle_enlargment(&parent_mbr, mbrs[i], &area);
				d2 = rectangle_enlargment(ll_mbr, mbrs[i], &area);
				k = d1 > d2;
				diff = k ? d1-d2 : d2-d1;
				if (diff >= max_diff)
				{
					max_diff = diff;
					j = i;
					l = k;
				}
			}
		}
		
		if (l)
		{
			ll->mbrs[ll->slots] = mbrs[j];
			if (node)
			{
				ll->refs[ll->slots] = nodes[j];
				nodes[j]->parent = ll;
			}
			ll->slots++;
			update_rect(ll_mbr, mbrs[j]);
		}
		else
		{
			parent->mbrs[parent->slots] = mbrs[j];
			if (node)
			{
				parent->refs[parent->slots] = nodes[j];
				nodes[j]->parent = parent;
			}
			parent->slots++;
			update_rect(&parent_mbr, mbrs[j]);
		}
		
		i = n-1;
		if (j != i)
		{
			mbrs[j] = mbrs[i];
			if (node)
				nodes[j] = nodes[i];
		}
		n = i;
	}
	
	adjust_tree(parent);
	return append_item(rt, parent->parent, ll_mbr, ll);

fail:
#warning missing proper cleanup in split and append
	//TODO: proper cleanup
	free(ll_mbr);
	free(ll);
	return 0;
}

static int append_item(rtree_t rt, node_t parent, rectangle_t* mbr, node_t node)
{
	if (parent == NULL)
		return make_new_root(rt, rt->root, mbr, node);
	
	assert((is_leaf(parent) && node == NULL) || (!is_leaf(parent) && node != NULL));
	
	const unsigned int i = free_index(parent);
	if (i < rt->fanout)
	{
		set_item(parent, i, mbr, node);
		return 1;
	}
	else
		return split_and_append(rt, parent, mbr, node);
}

rtree_t rtree_create(const unsigned int m, const unsigned int n, const u_int32_t max_gap)
{
	rtree_t rt = (rtree_t)calloc(1, sizeof(struct rtree));
	FAIL_IF(!rt);
	rt->min_slots = m;
	rt->fanout = n;
	rt->max_gap = max_gap;
	rt->depth = 0;
	rt->size = 0;
	
#ifndef NDEBUG
	rt->insertions = 0;
	rt->scans = 0;
	rt->overlap_checks = 0;
	rt->deletions = 0;
	rt->reinsertions = 0;
#endif
	
	rt->root = make_node(1, rt->fanout);
	FAIL_IF(!rt->root);
	
	rt->reinsert_set = alist_create(16);
	FAIL_IF(!rt->reinsert_set);
	
	rt->nl = alist_create(16);
	FAIL_IF(!rt->nl);
	
	rt->nnl = alist_create(16);
	FAIL_IF(!rt->nnl);
	
	return rt;
	
fail:
	if (rt != NULL)
	{
		if (rt->root)
			free_node(rt->root);
		if (rt->reinsert_set)
			alist_destroy(rt->reinsert_set);
		if (rt->nl)
			alist_destroy(rt->nl);
		if (rt->nnl)
			alist_destroy(rt->nnl);
		
		free(rt);
	}
	return NULL;
}

int rtree_insert(rtree_t rt, const rectangle_t* rect)
{
	rectangle_t* mbr = dup_rect(rect);
	FAIL_IF(!mbr);
	
	FAIL_IF(!remove_overlapping_and_update_mbr(rt, mbr));
	node_t leaf = choose_node(rt->root, mbr, rt->depth);
	assert(is_leaf(leaf));
	FAIL_IF(!append_item(rt, leaf, mbr, NULL));
	
	rt->size++;
#ifndef NDEBUG
	rt->insertions++;
#endif

	return 1;
	
fail:
	free(mbr);
	return 0;
}

unsigned int rtree_get_size(const rtree_t rt)
{
	return rt->size;
}

void dump_leave_rects(const node_t node, FILE* stream)
{
	unsigned int i;
	if (is_leaf(node))
	{
		for (i = 0; i < node->slots; i++)
		{
			const rectangle_t* rect = node->mbrs[i];
			fprintf(stream, "%u\t%u\t%u\t%u\n", rect->x1, rect->x2, rect->y1, rect->y2);
		}
	}
	else
	{
		for (i = 0; i < node->slots; i++)
		{
			assert(node->refs[i]->parent == node);
			dump_leave_rects(node->refs[i], stream);
		}
	}
}

void rtree_dump_rectangles(const rtree_t rt, FILE* stream)
{
	dump_leave_rects(rt->root, stream);
}

#ifndef NDEBUG
void check_node_structure(const rtree_t rt, const node_t node, unsigned int depth, unsigned int* leaf_depth)
{
	assert(node->slots >= rt->min_slots && node->slots <= rt->fanout);
	
	unsigned int i, j;
	if (node->parent)
	{
		rectangle_t mbr;
		bcopy(node->mbrs[0], &mbr, sizeof(rectangle_t));
		for (i = 1; i < node->slots; i++)
			update_rect(&mbr, node->mbrs[i]);
	
		j = node_index(node->parent, node);
		assert(j < rt->fanout);
		assert(bcmp(node->parent->mbrs[j], &mbr, sizeof(rectangle_t)) == 0);
	}
	
	if (is_leaf(node))
	{
		if (*leaf_depth == 0xffffffff)
			*leaf_depth = depth;
		else
			assert(depth == *leaf_depth);
		
		unsigned int j;
		for (i = 0; i < node->slots-1; i++)
		{
			for (j = i+1; j < node->slots; j++)
				assert(node->mbrs[i] != node->mbrs[j]);
		}
	}
	else
	{
		assert(depth != *leaf_depth);
		for (i = 0; i < node->slots; i++)
		{
			assert(node->refs[i]->parent == node);
			check_node_structure(rt, node->refs[i], depth+1, leaf_depth);
		}
	}
}

void rtree_check_structure(const rtree_t rt)
{
	if (is_leaf(rt->root))
		assert(rt->root->slots <= rt->fanout);
	else
	{
		unsigned int leaf_depth = 0xffffffff;
		check_node_structure(rt, rt->root, 0, &leaf_depth);
	}
}

void rtree_print_stats(const rtree_t rt)
{
	fprintf(stderr, "size: %u\n", rt->size);
	fprintf(stderr, "depth: %u\n", rt->depth);
	fprintf(stderr, "scans (avg per insertion): %u (%.2f)\n", rt->scans, ((float)rt->scans) / rt->insertions);
	fprintf(stderr, "overlap checks (avg per scan): %u (%.2f)\n", rt->overlap_checks, ((float)rt->overlap_checks) / rt->scans);
	fprintf(stderr, "deletions: %u\n", rt->deletions);
	fprintf(stderr, "reinsertions: %u\n", rt->reinsertions);

	rt->insertions = 0;
	rt->scans = 0;
	rt->overlap_checks = 0;
	rt->deletions = 0;
	rt->reinsertions = 0;
}

void dump_node_as_tree(const node_t node, FILE* stream)
{
	unsigned int i, l;
	for (i = 0, l = node->slots; i < l; i++)
	{
		if (node->refs == NULL)
			fprintf(stream, "\"%lx\" -- \"%lx\" [color = \"green\"]\n", (unsigned long)node, (unsigned long)node->mbrs[i]);
		else
			fprintf(stream, "\"%lx\" -- \"%lx\"\n", (unsigned long)node, (unsigned long)node->refs[i]);
		
		if (!is_leaf(node))
		{
			assert(node->refs[i]->parent == node);
			dump_node_as_tree(node->refs[i], stream);
		}
	}
	
	if (node->parent != NULL)
		fprintf(stream, "\"%lx\" -- \"%lx\" [color = \"red\"]\n", (unsigned long)node, (unsigned long)node->parent);
}

void rtree_dump_tree(const rtree_t rt, const char* filename)
{
	FILE* stream = fopen(filename, "w");
	fprintf(stream, "graph G {");
	dump_node_as_tree(rt->root, stream);
	fprintf(stream, "}");
	fclose(stream);
}
#endif
