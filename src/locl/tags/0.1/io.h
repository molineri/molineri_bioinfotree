#ifndef IO_H
#define IO_H

#include "rtree.h"
#include <stdio.h>

typedef enum
{
	OK,
	END_OF_FILE,
	IO_ERROR,
	FORMAT_ERROR
} reader_status_t;

reader_status_t read_hsp(FILE* stream, rectangle_t* rect);

#endif //IO_H
