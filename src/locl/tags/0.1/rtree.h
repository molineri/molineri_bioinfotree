#ifndef RTREE_H
#define RTREE_H

#include <stdio.h>
#include <sys/types.h>

typedef struct
{
	u_int32_t x1;
	u_int32_t x2;
	u_int32_t y1;
	u_int32_t y2;
} rectangle_t;

typedef struct rtree* rtree_t;

rtree_t rtree_create(const unsigned int m, const unsigned int n, const u_int32_t max_gap);
int rtree_insert(rtree_t rt, const rectangle_t* rect);
unsigned int rtree_get_size(const rtree_t rt);
void rtree_dump_rectangles(const rtree_t rt, FILE* stream);

#ifndef NDEBUG
void rtree_check_structure(const rtree_t rt);
void rtree_print_stats(const rtree_t rt);
void rtree_dump_tree(const rtree_t rt, const char* filename);
#endif

#endif //RTREE_H
