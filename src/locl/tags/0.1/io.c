#include "io.h"
#include <stdlib.h>

static reader_status_t parse_hsp(const char* line, rectangle_t* rect)
{
	if (sscanf(line, "%d\t%d\t%d\t%d", &rect->x1, &rect->x2, &rect->y1, &rect->y2) == 4)
		return OK;
	else
		return FORMAT_ERROR;
}

reader_status_t read_hsp(FILE* stream, rectangle_t* rect)
{
	const unsigned int buf_size = 4096;
	char buf[buf_size];
	
	if (fgets(buf, buf_size, stream) == NULL)
		return feof(stream) ? END_OF_FILE : IO_ERROR;
	else
		return parse_hsp(buf, rect);
}
