#include <assert.h>
#include "io.h"
#include <limits.h>
#include "rtree.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//TODO: make max_gap a command-line parameter
static u_int32_t max_gap = 22000;

void parse_args(int argc, char* argv[])
{
	int ch;
	long long gap;
	char* endptr;
	while ((ch = getopt(argc, argv, "g:h")) != -1)
	{
		switch (ch)
		{
		case 'g':
			gap = strtoll(optarg, &endptr, 10);
			if (*endptr != 0 || gap > 0xffffffff)
			{
				fprintf(stderr, "Invalid gap value.\n");
				exit(1);
			}
			else
				max_gap = gap;
			break;
		
		case 'h':
			fprintf(stderr, "Usage: %s <HSP_COORDS\n", argv[0]);
			fprintf(stderr, "Options:\n");
			fprintf(stderr, "  -h          show this help message and exit\n");
			fprintf(stderr, "  -g          minimum gap among location clusters (22000 by default)\n");
			exit(1);
			break;
		
		case '?':
			exit(1);
			break;
		}
	}
	
	if (optind != argc)
	{
		fprintf(stderr, "Unexpected argument number.\n");
		exit(1);
	}
}

int main(int argc, char* argv[])
{
	parse_args(argc, argv);
	
	rtree_t rt = rtree_create(2, 4, max_gap);
	if (rt == NULL)
	{
		fprintf(stderr, "Memory error.\n");
		return 1;
	}
	
	FILE* in = stdin;
	reader_status_t status;
	rectangle_t hsp;
	unsigned int c = 0;
	while ((status = read_hsp(in, &hsp)) == OK)
	{
#ifndef NDEBUG
		if (c % 100000 == 0)
		{
			fprintf(stderr, "step: %u\n", c);
			rtree_print_stats(rt);
			rtree_check_structure(rt);
		}
#endif
		
		rtree_insert(rt, &hsp);
		c++;
	}
	
	if (status == IO_ERROR)
	{
		fprintf(stderr, "I/O error.\n");
		return 1;
	}
	else if (status == FORMAT_ERROR)
	{
		fprintf(stderr, "Malformed input.\n");
		return 1;
	}
	
	rtree_dump_rectangles(rt, stdout);
	return 0;
}
