#ifndef ALIST_H
#define ALIST_H

struct alist
{
	unsigned int used;
	unsigned int available;
	void** elements;
};
typedef struct alist* alist_t;

alist_t alist_create(const unsigned int initial_size);
void alist_destroy(alist_t l);
unsigned int alist_size(const alist_t l);
void* alist_get(alist_t l, const unsigned int pos);
int alist_append(alist_t l, void* elt);
int alist_append_uniq(alist_t l, void* elt);
void alist_remove(alist_t l, const unsigned int pos);
void alist_clear(alist_t l);
void alist_sort(alist_t l, int (*cmp_func)(const void* a, const void* b));

#endif //ALIST_H
