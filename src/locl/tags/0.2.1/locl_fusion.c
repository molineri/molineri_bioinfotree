#include <assert.h>
#include "io.h"
#include "rtree.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct
{
	rectangle_t hsp;
	rectangle_t shadow;
	int updated;
} fusion_state_t;

//TODO: make max_gap a command-line parameter
static u_int32_t max_gap = 22000;

void parse_args(int argc, char* argv[])
{
	int ch;
	long long gap;
	char* endptr;
	while ((ch = getopt(argc, argv, "g:h")) != -1)
	{
		switch (ch)
		{
		case 'g':
			gap = strtoll(optarg, &endptr, 10);
			if (*endptr != 0 || gap > 0xffffffff)
			{
				fprintf(stderr, "Invalid gap value.\n");
				exit(1);
			}
			else
				max_gap = gap;
			break;
		
		case 'h':
			fprintf(stderr, "Usage: %s <HSP_COORDS\n", argv[0]);
			fprintf(stderr, "Options:\n");
			fprintf(stderr, "  -h          show this help message and exit\n");
			fprintf(stderr, "  -g          minimum gap among location clusters (22000 by default)\n");
			exit(1);
			break;
		
		case '?':
			exit(1);
			break;
		}
	}
	
	if (optind != argc)
	{
		fprintf(stderr, "Unexpected argument number.\n");
		exit(1);
	}
}

static void compute_shadow(const rectangle_t* rect, rectangle_t* shadow)
{
	shadow->x1 = rect->x1 > max_gap ? rect->x1 - max_gap : 0;
	shadow->x2 = rect->x2 + max_gap;
	shadow->y1 = rect->y1 > max_gap ? rect->y1 - max_gap : 0;
	shadow->y2 = rect->y2 + max_gap;
}

static int remove_func(const rectangle_t* rect, void** value, void* state)
{
	fusion_state_t* fs = state;
	assert(*value == NULL);
	
	update_rectangle(&fs->hsp, rect);
	fs->updated = 1;
	
	return 1;
}

static int printer(const rectangle_t* rect, void** value, void* state)
{
	assert(*value == NULL);
	printf("%u\t%u\t%u\t%u\n", rect->x1, rect->x2, rect->y1, rect->y2);
	return 1;
}

int main(int argc, char* argv[])
{
	parse_args(argc, argv);
	
	rtree_t rt = rtree_create(2, 4);
	if (rt == NULL)
	{
		fprintf(stderr, "Memory error.\n");
		return 1;
	}
	
	FILE* in = stdin;
	reader_status_t status;
	fusion_state_t fs;
	unsigned int c = 0;
	while ((status = read_hsp_coords(in, &fs.hsp)) == OK)
	{
#ifndef NDEBUG
		if (c % 100000 == 0)
		{
			fprintf(stderr, "step: %u\n", c);
			rtree_print_stats(rt);
			rtree_check_structure(rt);
		}
#endif
		
		do
		{
			compute_shadow(&fs.hsp, &fs.shadow);
			fs.updated = 0;
			
			if (!rtree_remove(rt, &fs.shadow, 1, &remove_func, &fs))
			{
				fprintf(stderr, "rtree remotion error.\n");
				return 1;
			}
		} while (fs.updated == 1);
		
		if (!rtree_insert(rt, &fs.hsp, NULL))
		{
			fprintf(stderr, "rtree insertion error.\n");
			return 1;
		}
		
#ifndef NDEBUG
		c++;
#endif
	}
	
	if (status == IO_ERROR)
	{
		fprintf(stderr, "I/O error.\n");
		return 1;
	}
	else if (status == FORMAT_ERROR)
	{
		fprintf(stderr, "Malformed input.\n");
		return 1;
	}

#ifndef NDEBUG
	rtree_check_structure(rt);
#endif
	if (!rtree_iter_sorted(rt, &printer, NULL))
	{
		fprintf(stderr, "rtree iteration error.\n");
		return 1;
	}
	
	return 0;
}
