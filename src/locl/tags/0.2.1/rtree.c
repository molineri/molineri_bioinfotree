#include <assert.h>
#include "rtree.h"
#include <stdlib.h>
#include <strings.h>

#ifndef NDEBUG
#include <stdio.h>
#endif

#define MIN(x,y) (x<y?x:y)
#define MAX(x,y) (x>y?x:y)
#define FAIL_IF(cond) if (cond) goto fail;

struct node_ptr
{
	rectangle_t mbr;
	void* ref;
};
typedef struct node_ptr* node_ptr_t;

struct node
{
	struct node* parent;
	unsigned char slots;
	char leaf;
	node_ptr_t* ptrs;
};
typedef struct node* node_t;

struct reinsert_info
{
	unsigned int height;
	node_ptr_t ptr;
};
typedef struct reinsert_info* reinsert_info_t;

struct rtree
{
	unsigned char min_slots;
	unsigned char fanout;
	node_t root;
	alist_t nl, nnl;
	unsigned int depth;
	unsigned int size;
	
#ifndef NDEBUG
	unsigned int insertions;
	unsigned int scans;
	unsigned int overlap_checks;
	unsigned int deletions;
	unsigned int reinsertions;
#endif
};

typedef int (*overlap_func_t)(const rectangle_t* a, const rectangle_t* b);

/////////////////////////
// rectangle operations
//
void update_rectangle(rectangle_t* a, const rectangle_t* b)
{
	a->x1 = MIN(a->x1, b->x1);
	a->x2 = MAX(a->x2, b->x2);
	a->y1 = MIN(a->y1, b->y1);
	a->y2 = MAX(a->y2, b->y2);
}

static u_int64_t rectangle_area(const rectangle_t* rect)
{
	return (rect->x2 - rect->x1) * (rect->y2 - rect->y1);
}

static u_int64_t joined_area(const rectangle_t* a, const rectangle_t* b)
{
	const u_int64_t p1 = MAX(a->x2, b->x2) - MIN(a->x1, b->x1);
	const u_int64_t p2 = MAX(a->y2, b->y2) - MIN(a->y1, b->y1);
	return p1 * p2;
}

static u_int64_t rectangle_enlargment(const rectangle_t* a, const rectangle_t* b, u_int64_t* rect_area)
{
	*rect_area = rectangle_area(a);
	const u_int64_t enlarged_area = joined_area(a, b);
	return enlarged_area - *rect_area;
}

static int rectangle_contains(const rectangle_t* a, const rectangle_t* b)
{
	return b->x1 >= a->x1 && b->x2 <= a->x2 && b->y1 >= a->y1 && b->y2 <= a->y2;
}

static int rectangles_partially_overlapped(const rectangle_t* a, const rectangle_t* b)
{
	return !(a->x1 > b->x2 || a->x2 < b->x1 || a->y1 > b->y2 || a->y2 < b->y1);
}

static overlap_func_t get_overlap_func(const int partial_overlap)
{
	if (partial_overlap)
		return &rectangles_partially_overlapped;
	else
		return &rectangle_contains;
}

int compare_rectangles(const rectangle_t* a, const rectangle_t* b)
{
	if (a->x1 == b->x1)
	{
		if (a->x2 == b->x2)
		{
			if (a->y1 == b->y1)
				return (a->y2 > b->y2) - (a->y2 < b->y2);
			else
				return (a->y1 > b->y1) - (a->y1 < b->y1);
		}
		else
			return (a->x2 > b->x2) - (a->x2 < b->x2);
	}
	else
		return (a->x1 > b->x1) - (a->x1 < b->x1);
}

////////////////////
// node operations
//
static void free_node(node_t node)
{
	free(node->ptrs);
	free(node);
}

static node_t make_node(const char leaf, const unsigned char fanout)
{
	node_t n = (node_t)malloc(sizeof(struct node));
	FAIL_IF(!n);
	
	n->parent = NULL;
	n->slots = 0;
	n->leaf = leaf;
	
	n->ptrs = (node_ptr_t*)calloc(fanout, sizeof(struct node_ptr));
	FAIL_IF(!n->ptrs);
	
	return n;

fail:
	if (n)
		free_node(n);
	return NULL;
}

static int is_leaf(node_t node)
{
	return node->leaf;
}

static unsigned int node_index(const node_t parent, const node_t node)
{
	assert(!is_leaf(parent));
	
	unsigned int i;
	for (i = 0; i < parent->slots && parent->ptrs[i]->ref != node;)
		i++;
	return i;
}

////////////////////
// tree operations
//
static int append_item(rtree_t rt, node_t parent, node_ptr_t item);

static node_t choose_node(node_t node, const rectangle_t* rect, unsigned int depth)
{
	while (!is_leaf(node) && depth > 0)
	{
		u_int64_t rect_area;
		u_int64_t min_enlargment = rectangle_enlargment(&node->ptrs[0]->mbr, rect, &rect_area);
		unsigned int j = 0;
		
		unsigned int i;
		for (i = 1; i < node->slots; i++)
		{
			rectangle_t* mbr = &node->ptrs[i]->mbr;
			const u_int64_t enlargment = rectangle_enlargment(mbr, rect, &rect_area);
			if (enlargment < min_enlargment || (enlargment == min_enlargment && rect_area < rectangle_area(rect)))
			{
				min_enlargment = enlargment;
				j = i;
			}
		}
		
		node = node->ptrs[j]->ref;
		depth--;
	}
	
	assert(depth == 0);
	return node;
}

static int remove_overlapping_rects(rtree_t rt, node_t leaf, const rectangle_t* mbr, const overlap_func_t overlapping, rtree_callback_t remove_func, void* state)
{
	assert(is_leaf(leaf));
	
	unsigned int i;
	for (i = 0; i < leaf->slots;)
	{
#ifndef NDEBUG
		rt->overlap_checks++;
#endif
		if (overlapping(mbr, &leaf->ptrs[i]->mbr))
		{
			node_ptr_t ptr = leaf->ptrs[i];
			if (!remove_func(&ptr->mbr, &ptr->ref, state))
				return 0;
			
			free(ptr);
			leaf->slots--;
			if (i != leaf->slots)
				leaf->ptrs[i] = leaf->ptrs[leaf->slots];
			
			rt->size--;
#ifndef NDEBUG
			rt->deletions++;
#endif
		}
		else
			i++;
	}
	
	return 1;
}

static void adjust_tree(node_t node)
{
	node_t parent;
	while ((parent = node->parent) != NULL)
	{
		assert(!is_leaf(parent));
		
		unsigned int i = node_index(parent, node);
		assert(i < 4);
		assert(parent->ptrs[i]->ref == node);
		assert(node->slots > 0);
		rectangle_t* mbr = &parent->ptrs[i]->mbr;
		rectangle_t old_mbr;
		bcopy(mbr, &old_mbr, sizeof(rectangle_t));
		
		bcopy(&node->ptrs[0]->mbr, mbr, sizeof(rectangle_t));
		for (i = 1; i < node->slots; i++)
			update_rectangle(mbr, &node->ptrs[i]->mbr);
		
		if (bcmp(&old_mbr, mbr, sizeof(rectangle_t)) != 0)
			node = parent;
		else
			break;
	}
}

static void swap_items(node_ptr_t* ptrs, const unsigned int from, const unsigned int to)
{
	if (from != to)
	{
		node_ptr_t swap = ptrs[to];
		ptrs[to] = ptrs[from];
		ptrs[from] = swap;
	}
}

static void swap_lists(alist_t* a, alist_t* b)
{
	alist_t swap = *a;
	*a = *b;
	*b = swap;
}

static int condense_tree(rtree_t rt, node_t node)
{
	assert(alist_size(rt->nnl) == 0);
	
	node_t parent;
	reinsert_info_t info;
	unsigned int i, height = 0;
	while ((parent = node->parent) != NULL && node->slots < rt->min_slots)
	{
		assert(!is_leaf(parent));
		i = node_index(parent, node);
		assert(i < rt->fanout);
		
		free(parent->ptrs[i]);
		swap_items(parent->ptrs, --(parent->slots), i);
		
		for (i = 0; i < node->slots; i++)
		{
			info = (reinsert_info_t)calloc(1, sizeof(struct reinsert_info));
			FAIL_IF(!info);
			
			info->height = height;
			info->ptr = node->ptrs[i];
			
			FAIL_IF(!alist_append(rt->nnl, info));
		}
		
		free_node(node);
		node = parent;
		height++;
	}
	
	if (parent)
		adjust_tree(node);
	else if (!is_leaf(node) && node->slots == 1)
	{
		node_t ref = node->ptrs[0]->ref;
		ref->parent = NULL;
		rt->root = ref;
		
		free(node->ptrs[0]);
		free_node(node);
		rt->depth--;
	}
	
	unsigned int l;
	for (i = 0, l = alist_size(rt->nnl); i < l; i++)
	{
		info = alist_get(rt->nnl, i);
		parent = choose_node(rt->root, &info->ptr->mbr, rt->depth - info->height);
		append_item(rt, parent, info->ptr);
		free(info);
	}
	
	alist_clear(rt->nnl);
#ifndef NDEBUG
	rt->reinsertions += l;
#endif
	
	return 1;
	
fail:
#warning missing proper cleanup in condense_tree
	//TODO: proper cleanup
	return 0;
}

static int find_leaves(rtree_t rt, const rectangle_t* mbr)
{
	assert(alist_size(rt->nl) == 0);
	assert(alist_size(rt->nnl) == 0);
#ifndef NDEBUG
	rt->scans++;
#endif
	
	FAIL_IF(!alist_append(rt->nl, rt->root));
	
	unsigned int i, j, l;
	while ((l = alist_size(rt->nl)) > 0 && !is_leaf(alist_get(rt->nl, 0)))
	{
		for (i = 0; i < l; i++)
		{
			node_t node = alist_get(rt->nl, i);
			assert(!is_leaf(node));
			
			for (j = 0; j < node->slots; j++)
			{
#ifndef NDEBUG
				rt->overlap_checks++;
#endif
				if (rectangles_partially_overlapped(&node->ptrs[j]->mbr, mbr))
				{
					FAIL_IF(!alist_append(rt->nnl, node->ptrs[j]->ref));
				}
			}
		}

		alist_clear(rt->nl);
		swap_lists(&rt->nl, &rt->nnl);
	}

	return 1;

fail:
	alist_clear(rt->nl);
	alist_clear(rt->nnl);
	return 0;
}

static int make_new_root(rtree_t rt, node_t node1, node_ptr_t node2_ptr)
{
	node_t root = make_node(0, rt->fanout);
	FAIL_IF(!root);
	
	/*mbr1 = (rectangle_t*)malloc(sizeof(rectangle_t));
	FAIL_IF(!mbr1);
	
	assert(node1->slots > 0);
	bcopy(node1->mbrs[0], mbr1, sizeof(rectangle_t));
	unsigned int i;
	for (i = 1; i < node1->slots; i++)
		update_rectangle(mbr1, node1->mbrs[i]);*/
	
	root->ptrs[0] = malloc(sizeof(struct node_ptr));
	FAIL_IF(!root->ptrs[0]);
	root->ptrs[0]->ref = node1;
	node1->parent = root;

	root->ptrs[1] = node2_ptr;
	((node_t)node2_ptr->ref)->parent = root;
	
	root->slots = 2;
	adjust_tree(node1);
	rt->root = root;
	rt->depth++;
	return 1;

fail:
	if (root)
	{
		if (root->ptrs[0])
			free(root->ptrs[0]);
		free_node(root);
	}
	return 0;
}

static void set_item(node_t node, const unsigned int pos, node_ptr_t item)
{
	node->ptrs[pos] = item;
	if (!is_leaf(node))
		((node_t)item->ref)->parent = node;
	
	node->slots++;
	adjust_tree(node);
}

static int pick_seeds(node_ptr_t* ptrs, const unsigned int mbr_num, unsigned int* k, unsigned int* l)
{
	unsigned int i;
	u_int64_t* ds = (u_int64_t*)alloca(sizeof(u_int64_t)* mbr_num);
	if (!ds)
		return 0;
	for (i = 0; i < mbr_num; i++)
		ds[i] = rectangle_area(&ptrs[i]->mbr);
	
	unsigned int j;
	u_int64_t max_diff = 0;
	for (i = 0; i < mbr_num-1; i++)
	{
		for (j = i+1; j < mbr_num; j++)
		{
			const u_int64_t diff = joined_area(&ptrs[i]->mbr, &ptrs[j]->mbr) - ds[i] - ds[j];
			if (diff >= max_diff)
			{
				max_diff = diff;
				*k = i;
				*l = j;
			}
		}
	}
	
	return 1;
}

static int split_and_append(rtree_t rt, node_t parent, node_ptr_t item)
{
	assert(parent->slots == rt->fanout);
	
	node_t ll = NULL;
	node_ptr_t ll_ptr = NULL;
	node_ptr_t* ptrs;
	
	ll = make_node(is_leaf(parent), rt->fanout);
	FAIL_IF(!ll);
	
	ll_ptr = malloc(sizeof(struct node_ptr));
	FAIL_IF(!ll_ptr);
	ll_ptr->ref = ll;
	
	ptrs = alloca(sizeof(node_ptr_t)*(rt->fanout+1));
	FAIL_IF(!ptrs);
	
	unsigned int i;
	for (i = 0; i < parent->slots; i++)
		ptrs[i] = parent->ptrs[i];
	ptrs[i] = item;
	
	unsigned int n = i;
	unsigned int k, l;
	FAIL_IF(!pick_seeds(ptrs, n+1, &k, &l));
	
	ll->ptrs[0] = ptrs[l];
	if (!is_leaf(ll))
		((node_t)ptrs[l]->ref)->parent = ll;
	bcopy(&ll->ptrs[0]->mbr, &ll_ptr->mbr, sizeof(rectangle_t));
	ll->slots = 1;
	if (l != n)
		ptrs[l] = ptrs[n];
	n--;
	
	rectangle_t parent_mbr;
	parent->ptrs[0] = ptrs[k];
	if (!is_leaf(parent))
		((node_t)ptrs[k]->ref)->parent = parent;
	bcopy(&parent->ptrs[0]->mbr, &parent_mbr, sizeof(rectangle_t));
	parent->slots = 1;
	if (k != n)
		ptrs[k] = ptrs[n];
	
	unsigned int j = 0;
	while (n > 0)
	{
		if (rt->min_slots - parent->slots == n)
		{
			j = 0;
			l = 0;
		}
		else if (rt->min_slots - ll->slots == n)
		{
			j = 0;
			l = 1;
		}
		else
		{
			u_int64_t area, d1, d2, diff, max_diff = 0;
			for (i = 0; i < n; i++)
			{
				d1 = rectangle_enlargment(&parent_mbr, &ptrs[i]->mbr, &area);
				d2 = rectangle_enlargment(&ll_ptr->mbr, &ptrs[i]->mbr, &area);
				k = d1 > d2;
				diff = k ? d1-d2 : d2-d1;
				if (diff >= max_diff)
				{
					max_diff = diff;
					j = i;
					l = k;
				}
			}
		}
		
		if (l)
		{
			ll->ptrs[ll->slots] = ptrs[j];
			if (!is_leaf(ll))
				((node_t)ptrs[j]->ref)->parent = ll;
			ll->slots++;
			update_rectangle(&ll_ptr->mbr, &ptrs[j]->mbr);
		}
		else
		{
			parent->ptrs[parent->slots] = ptrs[j];
			if (!is_leaf(parent))
				((node_t)ptrs[j]->ref)->parent = parent;
			parent->slots++;
			update_rectangle(&parent_mbr, &ptrs[j]->mbr);
		}
		
		i = n-1;
		if (j != i)
			ptrs[j] = ptrs[i];
		n = i;
	}
	
	adjust_tree(parent);
	return append_item(rt, parent->parent, ll_ptr);

fail:
#warning missing proper cleanup in split_and_append
	//TODO: proper cleanup
	free(ll_ptr);
	free(ll);
	return 0;
}

static int append_item(rtree_t rt, node_t parent, node_ptr_t item)
{
	if (parent == NULL)
	{
		assert(item->ref);
		return make_new_root(rt, rt->root, item);
	}
	else if (parent->slots < rt->fanout)
	{
		set_item(parent, parent->slots, item);
		return 1;
	}
	else
		return split_and_append(rt, parent, item);
}

rtree_t rtree_create(const unsigned char m, const unsigned char n)
{
	rtree_t rt = (rtree_t)calloc(1, sizeof(struct rtree));
	FAIL_IF(!rt);
	rt->min_slots = m;
	rt->fanout = n;
	rt->root = NULL;
	rt->nl = NULL;
	rt->nnl = NULL;
	rt->depth = 0;
	rt->size = 0;
	
#ifndef NDEBUG
	rt->insertions = 0;
	rt->scans = 0;
	rt->overlap_checks = 0;
	rt->deletions = 0;
	rt->reinsertions = 0;
#endif
	
	rt->root = make_node(1, rt->fanout);
	FAIL_IF(!rt->root);
	
	rt->nl = alist_create(16);
	FAIL_IF(!rt->nl);
	
	rt->nnl = alist_create(16);
	FAIL_IF(!rt->nnl);
	
	return rt;
	
fail:
	if (rt != NULL)
	{
		if (rt->root)
			free_node(rt->root);
		if (rt->nl)
			alist_destroy(rt->nl);
		if (rt->nnl)
			alist_destroy(rt->nnl);
		
		free(rt);
	}
	return NULL;
}

int rtree_search(rtree_t rt, const rectangle_t* rect, const int partial_overlap, rtree_callback_t match_func, void* state)
{
	if (!find_leaves(rt, rect))
		return 0;

	const unsigned int l = alist_size(rt->nl);
	const overlap_func_t overlapping = get_overlap_func(partial_overlap);
	unsigned int i, j;
	for (i = 0; i < l; i++)
	{
		node_t node = alist_get(rt->nl, i);
		assert(is_leaf(node));

		for (j = 0; j < node->slots; j++)
		{
			node_ptr_t ptr = node->ptrs[j];
			if (overlapping(&ptr->mbr, rect))
			{
				FAIL_IF(!match_func(&ptr->mbr, &ptr->ref, state));
			}
		}
	}

	alist_clear(rt->nl);
	return 1;

fail:
	alist_clear(rt->nl);
	return 0;
}

int rtree_insert(rtree_t rt, const rectangle_t* rect, void* value)
{
	node_ptr_t ptr = malloc(sizeof(struct node_ptr));
	FAIL_IF(!ptr);
	bcopy(rect, &ptr->mbr, sizeof(rectangle_t));
	ptr->ref = value;
	
	node_t leaf = choose_node(rt->root, &ptr->mbr, rt->depth);
	assert(is_leaf(leaf));
	FAIL_IF(!append_item(rt, leaf, ptr));
	
	rt->size++;
#ifndef NDEBUG
	rt->insertions++;
#endif
	
	return 1;
	
fail:
	free(ptr);
	return 0;
}

int rtree_remove(rtree_t rt, const rectangle_t* rect, const int partial_overlap, rtree_callback_t remove_func, void* state)
{
	if (!find_leaves(rt, rect))
		return 0;

	const unsigned int l = alist_size(rt->nl);
	const overlap_func_t overlapping = get_overlap_func(partial_overlap);
	unsigned int i, j;
	for (i = 0; i < l; i++)
	{
		node_t node = alist_get(rt->nl, i);
		assert(is_leaf(node));
		
		j = node->slots;
		FAIL_IF(!remove_overlapping_rects(rt, node, rect, overlapping, remove_func, state));
		if (node->slots != j)
		{
			if (node->slots >= rt->min_slots)
				adjust_tree(node);
			else
			{
				FAIL_IF(!condense_tree(rt, node));
			}
		}
	}

	alist_clear(rt->nl);
	return 1;

fail:
	alist_clear(rt->nl);
	return 0;
}

unsigned int rtree_get_size(const rtree_t rt)
{
	return rt->size;
}

static void collect_ptrs(node_ptr_t* ptrs, unsigned int* pos, node_t node)
{
	unsigned int i;
	for (i = 0; i < node->slots; i++)
	{
		if (is_leaf(node))
			ptrs[(*pos)++] = node->ptrs[i];
		else
			collect_ptrs(ptrs, pos, node->ptrs[i]->ref);
	}
}

static int compare_node_ptrs(const void* a, const void* b)
{
	node_ptr_t pa = *(node_ptr_t*)a;
	node_ptr_t pb = *(node_ptr_t*)b;
	return compare_rectangles(&pa->mbr, &pb->mbr);
}

int rtree_iter_sorted(const rtree_t rt, rtree_callback_t iter_func, void* state)
{
	node_ptr_t* ptrs = malloc(sizeof(node_ptr_t)*rt->size);
	FAIL_IF(!ptrs);
	
	unsigned int i = 0;
	collect_ptrs(ptrs, &i, rt->root);
	assert(i == rt->size);
	
	qsort(ptrs, rt->size, sizeof(node_ptr_t), &compare_node_ptrs);
	
	for (i = 0; i < rt->size; i++)
	{
		node_ptr_t ptr = ptrs[i];
		FAIL_IF(!iter_func(&ptr->mbr, &ptr->ref, state));
	}
	
	return 1;
	
fail:
	free(ptrs);
	return 0;
}

#ifndef NDEBUG
void check_node_structure(const rtree_t rt, const node_t node, unsigned int depth, unsigned int* leaf_depth)
{
	assert(node->slots >= rt->min_slots && node->slots <= rt->fanout);
	
	unsigned int i, j;
	if (node->parent)
	{
		rectangle_t mbr;
		bcopy(&node->ptrs[0]->mbr, &mbr, sizeof(rectangle_t));
		for (i = 1; i < node->slots; i++)
			update_rectangle(&mbr, &node->ptrs[i]->mbr);
	
		j = node_index(node->parent, node);
		assert(j < rt->fanout);
		assert(bcmp(&node->parent->ptrs[j]->mbr, &mbr, sizeof(rectangle_t)) == 0);
	}
	
	if (is_leaf(node))
	{
		if (*leaf_depth == 0xffffffff)
			*leaf_depth = depth;
		else
			assert(depth == *leaf_depth);
		
		unsigned int j;
		for (i = 0; i < node->slots-1; i++)
		{
			for (j = i+1; j < node->slots; j++)
				assert(&node->ptrs[i]->mbr != &node->ptrs[j]->mbr);
		}
	}
	else
	{
		assert(depth != *leaf_depth);
		for (i = 0; i < node->slots; i++)
		{
			node_t ref = node->ptrs[i]->ref;
			assert(ref->parent == node);
			check_node_structure(rt, ref, depth+1, leaf_depth);
		}
	}
}

void rtree_check_structure(const rtree_t rt)
{
	if (is_leaf(rt->root))
		assert(rt->root->slots <= rt->fanout);
	else
	{
		unsigned int leaf_depth = 0xffffffff;
		check_node_structure(rt, rt->root, 0, &leaf_depth);
	}
}

void rtree_print_stats(const rtree_t rt)
{
	fprintf(stderr, "size: %u\n", rt->size);
	fprintf(stderr, "depth: %u\n", rt->depth);
	fprintf(stderr, "scans (avg per insertion): %u (%.2f)\n", rt->scans, ((float)rt->scans) / rt->insertions);
	fprintf(stderr, "overlap checks (avg per scan): %u (%.2f)\n", rt->overlap_checks, ((float)rt->overlap_checks) / rt->scans);
	fprintf(stderr, "deletions: %u\n", rt->deletions);
	fprintf(stderr, "reinsertions: %u\n", rt->reinsertions);

	rt->insertions = 0;
	rt->scans = 0;
	rt->overlap_checks = 0;
	rt->deletions = 0;
	rt->reinsertions = 0;
}

/*void dump_node_as_tree(const node_t node, FILE* stream)
{
	unsigned int i, l;
	for (i = 0, l = node->slots; i < l; i++)
	{
		if (node->refs == NULL)
			fprintf(stream, "\"%lx\" -- \"%lx\" [color = \"green\"]\n", (unsigned long)node, (unsigned long)node->mbrs[i]);
		else
			fprintf(stream, "\"%lx\" -- \"%lx\"\n", (unsigned long)node, (unsigned long)node->refs[i]);
		
		if (!is_leaf(node))
		{
			assert(((node_t)node->refs[i])->parent == node);
			dump_node_as_tree(node->refs[i], stream);
		}
	}
	
	if (node->parent != NULL)
		fprintf(stream, "\"%lx\" -- \"%lx\" [color = \"red\"]\n", (unsigned long)node, (unsigned long)node->parent);
}

void rtree_dump_tree(const rtree_t rt, const char* filename)
{
	FILE* stream = fopen(filename, "w");
	fprintf(stream, "graph G {");
	dump_node_as_tree(rt->root, stream);
	fprintf(stream, "}");
	fclose(stream);
}*/
#endif
