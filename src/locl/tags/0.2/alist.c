#include "alist.h"
#include <assert.h>
#include <stdlib.h>

alist_t alist_create(const unsigned int initial_size)
{
	alist_t l = (alist_t)malloc(sizeof(struct alist));
	if (l == NULL)
		return NULL;
	
	l->elements = (void**)malloc(sizeof(void*)*initial_size);
	if (l->elements == NULL)
	{
		free(l);
		return NULL;
	}
	
	l->used = 0;
	l->available = initial_size;
	
	return l;
}

void alist_destroy(alist_t l)
{
	free(l->elements);
	free(l);
}

unsigned int alist_size(const alist_t l)
{
	return l->used;
}

void* alist_get(alist_t l, const unsigned int pos)
{
	assert(pos < l->used);
	return l->elements[pos];
}

int alist_append(alist_t l, void* elt)
{
	if (l->used == l->available)
	{
		void** new_elements = (void**)realloc(l->elements, sizeof(void*)*l->used*2);
		if (new_elements == NULL)
			return 0;
		
		l->elements = new_elements;
		l->available = l->used * 2;
	}
	
	l->elements[l->used] = elt;
	l->used++;
	
	return 1;
}

int alist_append_uniq(alist_t l, void* elt)
{
	unsigned int i;
	for (i = 0; i < l->used; i++)
	{
		if (l->elements[i] == elt)
			return 1;
	}
	
	return alist_append(l, elt);
}

void alist_remove(alist_t l, const unsigned int pos)
{
	assert(pos < l->used);
	
	if (pos+1 != l->used)
		l->elements[pos] = l->elements[l->used-1];
	l->used--;
}

void alist_clear(alist_t l)
{
	l->used = 0;
}

void alist_sort(alist_t l, int (*cmp_func)(const void* a, const void* b))
{
	qsort(l->elements, l->used, sizeof(void*), cmp_func);
}
