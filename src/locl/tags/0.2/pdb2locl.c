#include <assert.h>
#include "io.h"
#include "rtree.h"
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>

typedef struct
{
	rectangle_t mbr;
	char* line;
} assigned_hsp;

static const char* locl_path;
static rtree_t rt;
static int assigned;
static u_int32_t x_cut;

void parse_args(int argc, char* argv[])
{
	int ch;
	while ((ch = getopt(argc, argv, "h")) != -1)
	{
		switch (ch)
		{
		case 'h':
			fprintf(stderr, "Usage: %s LOCL_COORDS <PDB\n", argv[0]);
			fprintf(stderr, "Options:\n");
			fprintf(stderr, "  -h          show this help message and exit\n");
			exit(1);
			break;
		
		case '?':
			exit(1);
			break;
		}
	}
	
	if (optind+1 != argc)
	{
		fprintf(stderr, "Unexpected argument number.\n");
		exit(1);
	}
	
	locl_path = argv[optind];
}

void load_hsps()
{
	FILE* in = fopen(locl_path, "r");
	if (!in)
	{
		fprintf(stderr, "Cannot open location cluster coordinate file.\n");
		exit(1);
	}
	
	reader_status_t status;
	rectangle_t hsp_coords;
	while ((status = read_hsp_coords(in, &hsp_coords)) == OK)
	{
		if (!rtree_insert(rt, &hsp_coords, NULL))
		{
			fprintf(stderr, "rtree insertion error.\n");
			exit(1);
		}
	}
	
	if (status == IO_ERROR)
	{
		fprintf(stderr, "I/O error.\n");
		exit(1);
	}
	else if (status == FORMAT_ERROR)
	{
		fprintf(stderr, "Malformed location cluster input.\n");
		exit(1);
	}
	else
		fclose(in);
}

static assigned_hsp* make_assigned_hsp(rectangle_t* hsp, const char* line)
{
	assigned_hsp* ah = malloc(sizeof(assigned_hsp));
	if (!ah)
	{
		fprintf(stderr, "Cannot allocate assigned HSP structure.\n");
		exit(1);
	}
	
	bcopy(hsp, &ah->mbr, sizeof(rectangle_t));
	ah->line = strdup(line);
	if (!ah->line)
	{
		fprintf(stderr, "Cannot duplicate HSP line.\n");
		exit(1);
	}
	
	return ah;
}

static int assign_hsp(const rectangle_t* rect, void** value, void* state)
{
	assert(!assigned);
	
	if (*value == NULL)
	{
		*value = alist_create(16);
		if (!*value)
		{
			fprintf(stderr, "Cannot allocate result list.\n");
			return 0;
		}
	}
	
	alist_t hsps = (alist_t)*value;
	alist_append(hsps, state);
	assigned = 1;
	x_cut = rect->x1;
	
	return 1;
}

static int compare_assigned_hsps(const void* a, const void* b)
{
	const assigned_hsp* c = *(assigned_hsp**)a;
	const assigned_hsp* d = *(assigned_hsp**)b;
	return compare_rectangles(&c->mbr, &d->mbr);
}

static int close_locl(const rectangle_t* rect, void** value, void* state)
{
	printf(">%u_%u_%u_%u\n", rect->x1, rect->x2, rect->y1, rect->y2);
	
	alist_t hsps = (alist_t)*value;
	alist_sort(hsps, &compare_assigned_hsps);
	
	const unsigned int l = alist_size(hsps);
	unsigned int i;
	for (i = 0; i < l; i++)
	{
		assigned_hsp* ah = alist_get(hsps, i);
		printf(ah->line);
		
		free(ah->line);
		free(ah);
	}
	
	return 1;
}

static void close_locls(rectangle_t* close_region)
{
	if (!rtree_remove(rt, close_region, 0, close_locl, NULL))
	{
		fprintf(stderr, "rtree remove error.\n");
		exit(1);
	}
}

int main(int argc, char* argv[])
{
	parse_args(argc, argv);
	
	rt = rtree_create(2, 4);
	if (!rt)
	{
		fprintf(stderr, "Memory error.\n");
		return 1;
	}
	
	load_hsps(rt);
	
	reader_status_t status;
	rectangle_t hsp;
	const char* line;
	u_int32_t last_x1 = 0;
	unsigned int c = 0;
	while ((status = read_hsp(stdin, &hsp, &line)) == OK)
	{
#ifndef NDEBUG
		if (c % 10000 == 0)
		{
			fprintf(stderr, "step: %d\n", c);
			rtree_print_stats(rt);
		}
#endif
		if (last_x1 > hsp.x1)
		{
			fprintf(stderr, "Disorder detected in HSPs.\n");
			return 1;
		}
		else
			last_x1 = hsp.x1;
		
		assigned_hsp* ah = make_assigned_hsp(&hsp, line);
		assigned = 0;
		if (!rtree_search(rt, &hsp, 0, &assign_hsp, ah))
		{
			fprintf(stderr, "rtree search error.\n");
			return 1;
		}
		
		if (!assigned)
		{
			fprintf(stderr, "Cannot assign HSP to any location cluster.\n");
			fprintf(stderr, line);
			return 1;
		}
		
		if (c > 0 && c % 100 == 0)
		{
			hsp.x1 = 0;
			hsp.x2 = x_cut;
			hsp.y1 = 0;
			hsp.y2 = 0xffffffff;
			close_locls(&hsp);
		}
		
		c++;
	}
	
	if (status == IO_ERROR)
	{
		fprintf(stderr, "I/O error.\n");
		exit(1);
	}
	else if (status == FORMAT_ERROR)
	{
		fprintf(stderr, "Malformed HSP input.\n");
		exit(1);
	}
	
	if (!rtree_iter_sorted(rt, &close_locl, NULL))
	{
		fprintf(stderr, "rtree remove error.\n");
		return 1;
	}
	
	return 0;
}
