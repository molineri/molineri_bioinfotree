#include "io.h"
#include <stdlib.h>

static char buf[4096];

static reader_status_t extract_hsp_coords(const char* line, rectangle_t* rect)
{
	if (sscanf(line, "%*s\t%d\t%d\t%*s\t%*s\t%d\t%d\t", &rect->x1, &rect->x2, &rect->y1, &rect->y2) == 4)
		return OK;
	else
		return FORMAT_ERROR;
}

static reader_status_t parse_hsp_coords(const char* line, rectangle_t* rect)
{
	if (sscanf(line, "%d\t%d\t%d\t%d", &rect->x1, &rect->x2, &rect->y1, &rect->y2) == 4)
		return OK;
	else
		return FORMAT_ERROR;
}

reader_status_t read_hsp(FILE* stream, rectangle_t* rect, const char** line)
{
	if (fgets(buf, sizeof(buf), stream) == NULL)
		return feof(stream) ? END_OF_FILE : IO_ERROR;
	else
	{
		*line = buf;
		return extract_hsp_coords(buf, rect);
	}
}

reader_status_t read_hsp_coords(FILE* stream, rectangle_t* rect)
{
	if (fgets(buf, sizeof(buf), stream) == NULL)
		return feof(stream) ? END_OF_FILE : IO_ERROR;
	else
		return parse_hsp_coords(buf, rect);
}
