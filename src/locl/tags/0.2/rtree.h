#ifndef RTREE_H
#define RTREE_H

#include "alist.h"
#include <stdio.h>
#include <sys/types.h>

typedef struct
{
	u_int32_t x1;
	u_int32_t x2;
	u_int32_t y1;
	u_int32_t y2;
} rectangle_t;

typedef struct rtree* rtree_t;
typedef int (*rtree_callback_t)(const rectangle_t* rect, void** value, void* state);

void update_rectangle(rectangle_t* a, const rectangle_t* b);
int compare_rectangles(const rectangle_t* a, const rectangle_t* b);

rtree_t rtree_create(const unsigned char m, const unsigned char n);
int rtree_search(rtree_t rt, const rectangle_t* rect, const int partial_overlap, rtree_callback_t match_func, void* state);
int rtree_insert(rtree_t rt, const rectangle_t* rect, void* value);
int rtree_remove(rtree_t rt, const rectangle_t* rect, const int partial_overlap, rtree_callback_t remove_func, void* state);
unsigned int rtree_get_size(const rtree_t rt);
int rtree_iter_sorted(const rtree_t rt, rtree_callback_t iter_func, void* state);

#ifndef NDEBUG
void rtree_check_structure(const rtree_t rt);
void rtree_print_stats(const rtree_t rt);
void rtree_dump_tree(const rtree_t rt, const char* filename);
#endif

#endif //RTREE_H
