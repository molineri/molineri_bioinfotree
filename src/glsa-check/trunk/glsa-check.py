#!/usr/bin/env python
from commands import getstatusoutput
from sys import argv, exit, stdout, stderr

def main():
	if len(argv) != 1:
		exit('Unexpected argument number.')
	
	status, output = getstatusoutput('/usr/bin/glsa-check -n -l affected')
	if status != 0:
		stderr.write('glsa-check exited uncleanly:\n\n')
		stderr.write(output)
		exit(1)
	else:
		lines = output.split('\n')
		content = lines[3:]

		has_content = False
		for line in content:
			if len(line.strip()) > 0:
				has_content = True
				break

		if has_content:
			stdout.write('\n'.join(content))

if __name__ == '__main__':
	main()
