#!/usr/bin/env python
# encoding: utf8

from __future__ import division

from optparse import OptionParser
from sys import exit, stdin, stdout
from vfork.draw.surface import PNGSurface

def parse_header(fd):
	line = fd.readline()
	if len(line) == 0:
		exit('Empty input file.')
	
	try:
		tokens = line.rstrip().split('\t')
		if len(tokens) != 2:
			raise ValueError
		return int(tokens[0]), int(tokens[1])
	except ValueError:
		exit('Invalid block file header.')

class BlockPlot(object):
	BORDER_FRACTION = 20
	
	def __init__(self, width, height, chr1_len, chr2_len, x_label, y_label):
		self.surface = PNGSurface(width, height)
		self.surface.line_width = min(width, height) / 100
		self.surface.rgba = (0, 0, 0, 1)
		self._draw_chromosomes()
		self._draw_labels(x_label, y_label)
		
		self.band_parity = 0
		self._build_transformers(chr1_len, chr2_len)
		self.write_to_file = self.surface.write_to_file
		self.write_to_buffer = self.surface.write_to_buffer
	
	def draw_block(self, x1, x2, y1, y2):
		c1 = self.surface.height / self.BORDER_FRACTION
		c2 = self.surface.height - c1
		
		poly = ( (self._scale_x(x1), c1),
		         (self._scale_x(x2), c1),
		         (self._scale_y(y2), c2),
		         (self._scale_y(y1), c2) )
		
		if self.band_parity == 0:
			self.surface.rgba = (0, 0, 0, 0.3)
			self.band_parity = 1
		else:
			self.surface.rgba = (0, 0, 0, 0.4)
			self.band_parity = 0
		
		self.surface.fill_polygon(poly)
	
	def _draw_chromosomes(self):
		x_start = self.surface.width / self.BORDER_FRACTION
		x_stop  = self.surface.width - x_start
		
		y = self.surface.height / self.BORDER_FRACTION
		self.surface.draw_line(x_start, y, x_stop, y)
		
		y = self.surface.height - y
		self.surface.draw_line(x_start, y, x_stop, y)
	
	def _draw_labels(self, x_label, y_label):
		if x_label is None and y_label is None:
			return
		
		c = self.surface.width / self.BORDER_FRACTION
		
		y = self.surface.height / self.BORDER_FRACTION
		self.surface.font_size = y / 2
		
		if x_label:
			self.surface.draw_text(c, y / 2, x_label)
		if y_label:
			self.surface.draw_text(c, self.surface.height - y / 4, y_label)
	
	def _build_transformers(self, chr1_len, chr2_len):
		offset = self.surface.width / self.BORDER_FRACTION
		draw_space = (1 - 2 / self.BORDER_FRACTION) * self.surface.width
		x_scale = draw_space / chr1_len
		y_scale = draw_space / chr2_len
		
		def scale_x(x):
			return offset + x*x_scale
		
		def scale_y(y):
			return offset + y*y_scale
		
		self._scale_x = scale_x
		self._scale_y = scale_y

def main():
	parser = OptionParser(usage='%prog <BLOCKS')
	parser.add_option('-o', '--output', dest='output', help='the filename of the PNG map', metavar='FILENAME')
	parser.add_option('-w', '--width', dest='width', type='int', default=640, help='the width of the PNG map', metavar='WIDTH')
	parser.add_option('-e', '--height', dest='height', type='int', default=480, help='the height of the PNG map', metavar='HEIGHT')
	parser.add_option('-t', '--top-label', dest='top_label', help='the label for the top chromosome', metavar='LABEL')
	parser.add_option('-b', '--bottom-label', dest='bottom_label', help='the label for the bottom chromosome', metavar='LABEL')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	chr1_len, chr2_len = parse_header(stdin)
	plot = BlockPlot(options.width, options.height, chr1_len, chr2_len, options.top_label, options.bottom_label)
	
	for lineno, line in enumerate(stdin):
		try:
			block = tuple(int(d) for d in line.rstrip().split('\t'))
			if len(block) != 4:
				raise ValueError
		except ValueError:
			exit('Invalid coordinate at line %d.' % (lineno+2))
		
		plot.draw_block(*block)
	
	plot.write_to_file(options.output or stdout)

if __name__ == '__main__':
	main()
