#ifndef MASK_H
#define MASK_H

#include <list>

class Sequence;

typedef struct Region
{
	size_t start;
	size_t stop;
};

class SequenceMask
{
public:
	class const_iterator
	{
	public:
		const_iterator(const const_iterator& obj);
		const_iterator& operator=(const const_iterator& rhs);
		
		bool next();
		const Region& mask() const;
	
	private:
		friend class SequenceMask;
		const_iterator(const std::list<Region>* masks, std::list<Region>::const_iterator iterator);

		const std::list<Region>* masks_;
		bool bof_;
		std::list<Region>::const_iterator iterator_;
	};

	SequenceMask(const Sequence& sequence, const size_t collapse_threshold = 0);
	
	void add(const Region& mask);
	const_iterator iterator() const;

private:
	SequenceMask(const SequenceMask& obj);
	SequenceMask& operator=(const SequenceMask& rhs);

	const size_t sequence_length_;
	const size_t collapse_threshold_;
	std::list<Region> masks_;
};

#endif //MASK_H
