#include "io.h"
#include "seed.h"

SeedIterator::SeedIterator(const Sequence& sequence, const size_t seed_size) :
	sequence_(sequence), seed_size_(seed_size), offset_(0), search_pos_(0)
{
}

bool SeedIterator::next()
{
	size_t non_masked = 0;	
	while (non_masked < seed_size_)
	{
		if (search_pos_ >= sequence_.length())
			return false;
		
		const char base = sequence_.content()[search_pos_++];
		if (base == 'N' || base == 'n')
			non_masked = 0;
		else
			++non_masked;
	}
	
	offset_ = search_pos_ - seed_size_;
	return true;
}

void SeedIterator::skip(size_t offset)
{
	search_pos_ += offset;
}

const char* SeedIterator::seed() const
{
	return sequence_.content() + offset_;
}

size_t SeedIterator::offset() const
{
	return offset_;
}
