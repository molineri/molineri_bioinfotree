import std.stream;
import std.string;

class MalformedInput
{
	char[] msg;
	
	this(char[] msg)
	{
		this.msg = msg;
	}
}

struct AlignmentPoint
{
	int x;
	int y;
	int half_span;
	char strand;
}

class AlignmentReader
{
	private InputStream s;
	private uint last_x_start;
	private uint lineno = 0;

	this(InputStream stream)
	{
		s = stream;
		last_x_start = 0;
	}
	
	bool get_point(out AlignmentPoint pt)
	{
		char[] line = s.readLine();
		if (s.eof())
			return false;
		
		lineno++;
		
		char[][] tokens = split(line, "\t");
		if (tokens.length < 7)
			throw new MalformedInput(format("insufficient token number at line %d", lineno));
		
		int x_start = atoi(tokens[1]);
		int x_stop = atoi(tokens[2]);
		int y_start = atoi(tokens[5]);
		int y_stop = atoi(tokens[6]);
		int x_length = x_stop - x_start;
		int y_length = y_stop - y_start;
		
		if (last_x_start > x_start)
			throw new MalformedInput(format("disorder found at line %d", lineno));
		else if (x_length <= 0)
			throw new MalformedInput(format("invalid query length at line %d (inverted coordinates?)", lineno));
		else if (y_length <= 0)
			throw new MalformedInput(format("invalid target length at line %d (inverted coordinates?)", lineno));
		else if (tokens[3].length != 1 || (tokens[3][0] != '-' && tokens[3][0] != '+'))
			throw new MalformedInput(format("invalid strand at line %d", lineno));
		
		pt.x = x_stop/2 + x_start/2 + (x_stop & x_start & 1);
		pt.y = y_stop/2 + y_start/2 + (y_stop & y_start & 1);
		pt.half_span = (x_length/2 + y_length/2) / 2;
		pt.strand = tokens[3][0];
		
		return true;
	}
}
