#!/usr/bin/env python

from __future__ import with_statement
from optparse import OptionParser
from sys import exit, stdin, stdout

def load_points(filename):
	with file(filename, 'r') as fd:
		res = []
		for line in fd:
			tokens = line.rstrip().split('\t')
			assert len(tokens) == 2
			res.append(tuple(int(t) for t in tokens))
		return res

def main():
	parser = OptionParser(usage='%prog POINT_LIST <PDB')
	options, args = parser.parse_args()

	if len(args) != 1:
		exit('Unexpected argument number.')

	points = load_points(args[0])
	
	for line in stdin:
		tokens = line.rstrip().split('\t')
		assert len(tokens) >= 7
		coords = [ int(tokens[i]) for i in (1, 2, 5, 6) ]
		
		x = coords[0]//2 + coords[1]//2 + (coords[0] & coords[1] & 1)
		y = coords[2]//2 + coords[3]//2 + (coords[2] & coords[3] & 1)
		
		for point in points:
			if point[0] == x and point[1] == y:
				stdout.write(line)

if __name__ == '__main__':
	main()
