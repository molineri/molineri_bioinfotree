import getopt;
import input;
import std.cstream;
import std.c.stdlib;
import std.math;
import std.stdio;
import std.string;
import storage;

bool allow_overlap = false;

void parse_args(inout char[][] args)
{
	char[] self = args[0];
	args = args[1..args.length];
	
	getopt_t[] used_options;
	char[] short_options = "ho";
	char[][2] long_options;
	long_options[0] = "help";
	long_options[1] = "allow-overlap";
	
	try
	{
		used_options = getopt.getopt(args, short_options, long_options);
	}
	catch(GetOptException e)
	{
		derr.writeLine(format("Invalid options: %s", e.msg));
		exit(1);
	}

	foreach(getopt_t item; used_options)
	{
		if (item.option == "-h" || item.option == "--help")
		{
			writefln("Usage: %s <PDB", self);
			writefln("\nOptions:");
			writefln(" -h, --help            show this help message and exit");
			writefln(" -o, --allow-overlaps  allow overlaps between alignments");
			exit(0);
		}
		else if (item.option == "-o" || item.option == "--output")
			allow_overlap = true;
	}
}

int point_distance(AlignmentPoint a_pt, AlignmentPoint b_pt)
{
	int x_dist, y_dist;
	
	if (a_pt.x > b_pt.x)
		x_dist = a_pt.x - a_pt.half_span - (b_pt.x + b_pt.half_span);
	else
		x_dist = b_pt.x - b_pt.half_span - (a_pt.x + a_pt.half_span);
	
	if (a_pt.y > b_pt.y)
		y_dist = a_pt.y - a_pt.half_span - (b_pt.y + b_pt.half_span);
	else
		y_dist = b_pt.y - b_pt.half_span - (a_pt.y + a_pt.half_span);
	
	if (x_dist < 0 && y_dist < 0)
	{
		if (!allow_overlap && a_pt.strand == b_pt.strand)
		{
			derr.writeLine("Negative distance found between points:");
			derr.writeLine(format("%d, %d\n%d, %d", a_pt.x, a_pt.y, b_pt.x, b_pt.y));
			exit(1);
		}
		else
			return int.max;
	}
	else if (x_dist < 0)
		return y_dist;
	else if (y_dist < 0)
		return x_dist;
	else
		return x_dist > y_dist ? y_dist : x_dist;
}

int main(char[][] argv)
{
	parse_args(argv);
	if (argv.length != 0)
	{
		derr.writeLine("Unexpected argument number.");
		return 1;
	}
	
	ChunkedStorage!(AlignmentPoint) store = new ChunkedStorage!(AlignmentPoint)(0xffff);
	try
	{
		AlignmentReader reader = new AlignmentReader(din);
		AlignmentPoint pt;
		while (reader.get_point(pt))
			store.add(pt);
		
		derr.writeLine("All points loaded.");
	}
	catch (MalformedInput e)
	{
		derr.writeLine(format("Error: %s", e.msg));
		return 1;
	}
	
	int idx = 1;
	uint store_length = store.length();
	if (store_length == 0)
		return 0;
	
	AlignmentPoint a_pt, b_pt;
	while (idx < store_length - 1)
	{
		a_pt = store.get(idx);
		int min_dist = point_distance(a_pt, store.get(idx-1));
		int dist = point_distance(a_pt, store.get(idx+1));
		if (dist < min_dist)
			min_dist = dist;
		
		int offset = 2;
		bool in_range_bottom = true;
		bool in_range_top = true;
		while (in_range_bottom || in_range_top)
		{
			if (in_range_bottom)
			{
				int idx2 = idx - offset;
				in_range_bottom = idx2 >= 0;
				if (in_range_bottom)
				{
					b_pt = store.get(idx2);
					in_range_bottom = b_pt.x + b_pt.half_span >= a_pt.x - a_pt.half_span - min_dist;
					if (in_range_bottom)
					{
						dist = point_distance(a_pt, b_pt);
						if (dist < min_dist)
							min_dist = dist;
					}
				}
			}
			
			if (in_range_top)
			{
				int idx2 = idx + offset;
				in_range_top = idx2 < store_length;
				if (in_range_top)
				{
					b_pt = store.get(idx2);
					in_range_top = b_pt.x - b_pt.half_span <= a_pt.x + a_pt.half_span + min_dist;
					if (in_range_top)
					{
						dist = point_distance(a_pt, b_pt);
						if (dist < min_dist)
							min_dist = dist;
					}
				}
			}
			
			offset++;
		}
		
		idx++;
		writefln("%d", min_dist);
	}
	
	return 0;
}
