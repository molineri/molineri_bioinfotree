class ChunkedStorage(T)
{
	private T[][] ribs;
	private uint rib_size;
	private uint rib_idx;
	private uint rib_usage;
	
	this(uint rib_size)
	{
		this.rib_size = rib_size;
		rib_idx = -1;
		rib_usage = rib_size;
	}
	
	void add(T item)
	{
		if (rib_usage >= rib_size)
		{
			ribs ~= new T[rib_size];
			rib_idx++;
			rib_usage = 0;
		}
		
		ribs[rib_idx][rib_usage] = item;
		rib_usage++;
	}
	
	uint length()
	{
		if (rib_idx < 0)
			return 0;
		else
			return rib_idx*rib_size + rib_usage;
	}
	
	T get(uint idx)
	{
		uint rib = idx / rib_size;
		return ribs[rib][idx % rib_size];
	}
}
