#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <gmp.h>

char* get_str_with_n_digits(mpq_t op, unsigned int digits);

#endif //UTIL_H
