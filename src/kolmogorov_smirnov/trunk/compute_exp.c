#include "exp.h"
#include "util.h"

int main(int argc, char* argv[])
{
	unsigned int digits;
	mpq_t e;
	char* repr;
	
	if (argc != 2)
	{
		fprintf(stderr, "Unexpected argument number.\n");
		return 1;
	}
	else if (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0)
	{
		fprintf(stderr, "Usage: %s PRECISION\n", argv[0]);
		return 0;
	}
	
	digits = atoi(argv[1]);
	
	mpq_init(e);
	e_nth_digit(e, digits);
	
	repr = get_str_with_n_digits(e, digits);
	puts(repr);
	
	free(repr);
	mpq_clear(e);
	
	return 0;
}
