#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

void parse_pair(char* line, char** label, long long* value)
{
	char* endptr;
	
	*label = strsep(&line, " \t");
	if (line == NULL)
	{
		fprintf(stderr, "Malformed input: %s\n", *label);
		exit(1);
	}
	
	*value = strtoll(line, &endptr, 10);
	if (endptr == NULL || *endptr != '\n' || *value < 0)
	{
		fprintf(stderr, "Malformed count: %s\n", line);
		exit(1);
	}
}

int main(int argc, char* argv)
{
	const size_t line_max = 1024;
	char line[line_max];
	unsigned long long count, last_count;
	
	bzero(line, sizeof(line));
	count = last_count = 0;
	while (fgets(line, line_max, stdin))
	{
		char* label;
		long long value;
		
		parse_pair(line, &label, &value);
		count += value;
		
		if (count < last_count)
		{
			fprintf(stderr, "Overflow detected!");
			return 1;
		}
		else
		{
			last_count = count;
			printf("%s\t%llu\n", label, count);
		}
	}
	
	if (!feof(stdin))
	{
		perror("I/O error");
		return 1;
	}
	
	return 0;
}
