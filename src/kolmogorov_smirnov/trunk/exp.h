#ifndef EXP_H
#define EXP_H

#include <gmp.h>

void e_nth_digit(mpq_t rop, unsigned int digit);

#endif //EXP_H

