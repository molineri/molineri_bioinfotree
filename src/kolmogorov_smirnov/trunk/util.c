#include <stdlib.h>
#include <string.h>
#include "util.h"

char* get_str_with_n_digits(mpq_t op, unsigned int digits)
{
	mpf_t value;
	char* res;
	mp_exp_t exp;
	int complete_length;
	char* formatted;
	
	mpf_init2(value, 1024); //CHANGE THIS
	mpf_set_q(value, op);
	res = mpf_get_str(NULL, &exp, 10, digits, value);
	
	complete_length = strlen(res) + 1024;
	formatted = (char*)malloc(complete_length+1);
	if (res[0] == '-')
		snprintf(formatted, complete_length, "-%c.%s*10^%ld", res[1], res+2, exp-1);
	else
		snprintf(formatted, complete_length, "%c.%s*10^%ld", res[0], res+1, exp-1);
	
	free(res);
	mpf_clear(value);
	return formatted;
}
