#include "exp.h"

void ten_to_minus_n(mpq_t rop, unsigned int n)
{
	mpq_t ten;
	
	mpq_init(ten);
	mpq_set_ui(ten, 10, 1);
	
	mpq_set_ui(rop, 1, 1);
	while (n > 0)
	{
		mpq_div(rop, rop, ten);
		n--;
	}
	
	mpq_clear(ten);
}

void e_nth_digit(mpq_t rop, unsigned int digit)
{
	mpq_t last_value;
	mpq_t nth_digit;
	mpq_t one;
	mpq_t fact;
	mpq_t fact_term;
	mpq_t inv_fact;
	mpq_t delta;
	int i = 10;
	
	// init rationals
	mpq_init(last_value);
	mpq_init(nth_digit);
	mpq_init(one);
	mpq_init(fact);
	mpq_init(fact_term);
	mpq_init(inv_fact);
	mpq_init(delta);
	
	// the first term of exp approx is 2
	mpq_set_ui(rop, 2, 1);
	mpq_set_ui(last_value, 2, 1);
	
	// 1! == 1
	mpq_set_ui(one, 1, 1);
	mpq_set_ui(fact, 1, 1);
	mpq_set_ui(fact_term, 1, 1);
	
	// compute a number of the same magnitude of the last required digit
	ten_to_minus_n(nth_digit, digit);
	
	while (1)
	{
		// compute the next factorial
		mpq_add(fact_term, fact_term, one);
		mpq_mul(fact, fact, fact_term);
		
		// invert the factorial and sum it to the current approx
		mpq_inv(inv_fact, fact);
		mpq_add(rop, rop, inv_fact);
		
		// compare the current approx with the former one
		mpq_sub(delta, rop, last_value);
		mpq_abs(delta, delta);
		
		if (mpq_cmp(delta, nth_digit) < 0)
			break;
		else
			mpq_set(last_value, rop);
	}
	
	// cleanup
	mpq_clear(delta);
	mpq_clear(inv_fact);
	mpq_clear(fact_term);
	mpq_clear(fact);
	mpq_clear(one);
	mpq_clear(nth_digit);
	mpq_clear(last_value);
}
