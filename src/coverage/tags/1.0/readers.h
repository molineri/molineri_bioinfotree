#ifndef READERS_H_
#define READERS_H_

#include <string>
#include <vector>
#include "alignment.h"

class Reader
{
public:
	Reader() {}
	virtual ~Reader() {}
	virtual bool get(Alignment& a) = 0;

private:
	Reader(const Reader& obj);
	Reader& operator=(const Reader& rhs);
};

class PipeReader : public Reader
{
public:
	PipeReader() {}
	~PipeReader() {}
	
	bool get(Alignment& a);
	
private:
	PipeReader(const PipeReader& obj);
	PipeReader& operator=(const PipeReader& rhs);
};

#endif /*READERS_H_*/
