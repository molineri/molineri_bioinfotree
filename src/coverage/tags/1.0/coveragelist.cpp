#include "coveragelist.h"
#include <iostream>
#include <limits>

using namespace std;

CoverageList::CoverageList()
{
	events_ = new Event();
	Event* stop = new Event();
	
	events_->x = numeric_limits<int>::min();
	events_->y = 0;
	events_->prev = NULL;
	events_->next = stop;
	
	stop->x = numeric_limits<int>::max();
	stop->y = 0;
	stop->prev = events_;
	stop->next = NULL;
	
	cursor_ = events_;
}

CoverageList::~CoverageList()
{
	while (events_ != NULL)
	{
		Event* next = events_->next;
		delete events_;
		events_ = next;
	}
}

void CoverageList::addAlignment(int start, int stop)
{
	if (cursor_->x > start)
		cursor_ = events_;
	
	while (cursor_->x < start)
		cursor_ = cursor_->next;
	
	if (cursor_->x == start)
		cursor_->y++;
	else
	{
		Event* evt = new Event();
		evt->x = start;
		evt->y = 1;
		
		insertBefore(evt, cursor_);
		cursor_ = evt;
	}
	
	Event* stopCursor = cursor_->next;
	while (stopCursor->x < stop)
		stopCursor = stopCursor->next;
	
	if (stopCursor->x == stop)
		stopCursor->y--;
	else
	{
		Event* evt = new Event();
		evt->x = stop;
		evt->y = -1;
		
		insertBefore(evt, stopCursor);
	}
}

void CoverageList::finalize()
{
	int count = 0;
	cursor_ = events_->next;
	while (cursor_->next != NULL)
	{
		cursor_->y += count;
		count = cursor_->y;
		
		cursor_ = cursor_->next;
	}
}

void CoverageList::dump() const
{
	Event* ptr = events_;
	while (ptr != NULL)
	{
		cout << ptr->x << "," << ptr->y << endl;
		ptr = ptr->next;
	}
}

void CoverageList::insertBefore(Event* evt, Event* cursor)
{
	evt->prev = cursor->prev;
	evt->next = cursor;
		
	cursor->prev->next = evt;
	cursor->prev = evt;
}
