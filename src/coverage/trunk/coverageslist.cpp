#include "coverageslist.h"
#include <iostream>
#include <limits>
#include <math.h>
#include <stdexcept>
#include <stdlib.h>
#include <time.h>

using namespace std;

CoverageSkipList::Node::Node(int level) :
	x(-1), y(-1), level(level), forward(new Node*[level])
{
}

CoverageSkipList::Node::~Node()
{
	delete[] forward;
}

const double CoverageSkipList::levelProb_ = 1.0/2;

CoverageSkipList::CoverageSkipList(const unsigned int maxSize) :
	maxLevel_(static_cast<unsigned int>(ceil(log(maxSize) / log(1/levelProb_)))),
	head_(maxLevel_), tail_(0)
{
	head_.x = numeric_limits<int>::min();
	head_.y = 0;
	for (int level = 0; level < maxLevel_; ++level)
		head_.forward[level] = &tail_;
	
	tail_.x = numeric_limits<int>::max();
	tail_.y = 0;
	
	srandom(time(NULL));
}

CoverageSkipList::~CoverageSkipList()
{
	Node* cursor = head_.forward[0];
	while (cursor != &tail_)
	{
		Node* next = cursor->forward[0];
		delete cursor;
		cursor = next;
	}
}

void CoverageSkipList::addAlignment(int start, int stop)
{
	addNode(start, 1);
	addNode(stop, -1);
}

void CoverageSkipList::finalize()
{
	Node* cursor = head_.forward[0];
	Node* prev = &head_;
	int count = 0;
	
	while (cursor != &tail_)
	{
		if (cursor->y == 0)
		{
			const int minLevel = min(prev->level, cursor->level);
			for (int level = 0; level < minLevel; ++level)
				prev->forward[level] = cursor->forward[level];
			
			delete cursor;
			cursor = prev->forward[0];
		}
		else
		{
			cursor->y += count;
			count = cursor->y;
			
			prev = cursor;
			cursor = cursor->forward[0];
		}
	}
}

void CoverageSkipList::dump(int min_x) const
{
	const Node* cursor = &head_;
	while (cursor != &tail_)
	{
		if (cursor->x < min_x)
		{
			if (cursor->forward[0]->x > min_x)
				cout << min_x << "\t" << cursor->y << endl;
		}
		else
			cout << cursor->x << "\t" << cursor->y << endl;
		
		cursor = cursor->forward[0];
	}
}

void CoverageSkipList::addNode(int x, int y)
{
	Node* update[maxLevel_];
	Node* cursor = &head_;
	int level;
	
	for (level = maxLevel_-1; level >= 0; --level)
	{
		while (cursor->forward[level]->x < x)
			cursor = cursor->forward[level];
		
		update[level] = cursor;
	}
	
	cursor = cursor->forward[0];
	if (cursor->x == x)
		cursor->y += y;
	else
	{
		cursor = new Node(generateNodeLevel());
		cursor->x = x;
		cursor->y = y;
		
		for (int level = 0; level < cursor->level; ++level)
		{
			cursor->forward[level] = update[level]->forward[level];
			update[level]->forward[level] = cursor;
		}
	}
}

int CoverageSkipList::generateNodeLevel() const
{
	int level = 1;
	static const double maxRand = (1<<31)-1;
	
	while (random() / maxRand < levelProb_ && level < maxLevel_)
		++level;
	
	return level;
}
