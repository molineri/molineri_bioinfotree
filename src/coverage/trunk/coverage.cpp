#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include "alignment.h"
#include "coverageslist.h"
#include "readers.h"

using namespace std;
int coord_min = 0;

void printUsage(const char* self)
{
	cerr << "Usage: " << self << " <ALIGNMENT_REGIONS" << endl;
	exit(1);
}

int parseCoord(const char* coord)
{
	char* endptr;
	const int value = strtol(coord, &endptr, 10);
	if (*endptr != 0 || value < 0)
	{
		cerr << "Invalid coordinate: " << coord << endl;
		exit(1);
	}
	
	return value;
}

void parseArgs(int argc, char* argv[])
{
	int c;
	extern char *optarg;
	extern int optind, optopt;
	
	while ((c = getopt(argc, argv, "hi:")) != -1)
	{
		switch (c)
		{
		case 'h':
			printUsage(argv[0]);
			break;
		
		case 'i':
			coord_min = parseCoord(optarg);
			break;
		
		default:
			cerr << "Unrecognised option: -" << static_cast<char>(optopt) << endl;
			exit(1);
			break;
		}
	}
	
	if (optind < argc)
	{
		cerr << "Unexpected argument number." << endl;
		exit(1);
	}
}

int main(int argc, char* argv[])
{
	parseArgs(argc, argv);
	
	try
	{
		PipeReader reader;
		CoverageSkipList slist(static_cast<unsigned int>(1e7));

		Alignment alignment;
		while (reader.get(alignment))
		{
			if (alignment.stop >= coord_min)
				slist.addAlignment(alignment.start, alignment.stop);
		}
		
		slist.finalize();
		slist.dump(coord_min);
	}
	catch (runtime_error& ioe)
	{
		cerr << "[ERROR] " << ioe.what() << endl;
		exit(1);
	}
	catch (...)
	{
		cerr << "[ERROR] Unexpected exception." << endl;
		exit(1);
	}
	
	return 0;
}
