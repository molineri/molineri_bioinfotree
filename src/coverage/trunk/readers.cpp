#include <stdexcept>
#include <iostream>
#include "readers.h"

using namespace std;

bool PipeReader::get(Alignment& a)
{
	string line;
	if (!getline(cin, line))
	{
		if (cin.eof())
			return false;
		else
			throw runtime_error("read error");
	}
	
	if (sscanf(line.c_str(), "%ld\t%ld", &a.start, &a.stop) != 2)
		throw runtime_error("malformed input");
	
	if (a.start > a.stop)
		swap(a.start, a.stop);
	return true;
}
