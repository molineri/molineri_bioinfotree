#ifndef COVERAGESLIST_H_
#define COVERAGESLIST_H_

class CoverageSkipList
{
public:
	CoverageSkipList(const unsigned int maxSize);
	~CoverageSkipList();
	
	void addAlignment(int start, int stop);
	void finalize();
	void dump(int min_x=0) const;

protected:
	struct Node
	{
		Node(int level);
		~Node();
		
		int x;
		int y;
		
		int level;
		Node** forward;
	};
	
	void addNode(int x, int y);
	int generateNodeLevel() const;

	int maxLevel_;
	static const double levelProb_;
	
	Node head_;
	Node tail_;

private:
	CoverageSkipList(const CoverageSkipList& obj);
	CoverageSkipList& operator=(const CoverageSkipList& rhs);
};

#endif /*COVERAGESLIST_H_*/
