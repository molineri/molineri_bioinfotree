#ifndef COVERAGELIST_H_
#define COVERAGELIST_H_

namespace
{
	struct Event
	{
		int x;
		int y;
		
		Event* next;
		Event* prev;
	};
}

class CoverageList
{
public:
	CoverageList();
	~CoverageList();
	
	void addAlignment(int start, int stop);
	void finalize();
	void dump() const;

protected:
	void insertBefore(Event* evt, Event* cursor);
	
	Event* events_;
	Event* cursor_;

private:
	CoverageList(const CoverageList& obj);
	CoverageList& operator=(const CoverageList& rhs);
};

#endif /*COVERAGELIST_H_*/
