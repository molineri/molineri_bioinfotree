#!/usr/bin/env python

from ioutil import *
from optparse import OptionParser
from sys import exit
import config

def main():
	parser = OptionParser(usage='%prog')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	sock = ClientControlSocket(config.control_socket)
	sock.write('CLIST')
	
	header = False
	for line in sock.iter_lines():
		tokens = line.rstrip().split('\t')
		if len(tokens[1]) > 20:
			tokens[1] = '*%s' % tokens[1][-19:]
		
		if not header:
			print '% 5s\t%-20s\t%-20s\t%-10s\t%s' % ('ID', 'Command', 'Timestamp', 'Status', 'Host')
			header = True
		
		print '% 5s\t%-20s\t%-20s\t%-10s\t%s' % (tokens[0], tokens[1], tokens[2], tokens[3], tokens[4])

if __name__ == '__main__':
	main()
