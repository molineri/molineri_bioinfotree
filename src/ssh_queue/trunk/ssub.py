#!/usr/bin/env python

from ioutil import *
from optparse import OptionParser
from sys import exit
import config

def main():
	parser = OptionParser(usage='%prog CMD')
	options, args = parser.parse_args()
	
	if len(args) == 0:
		exit('Unexpected argument number.')
	elif len(args) > 1:
		args[0] = ' '.join(args)
	
	cmd = args[0]
	if len(cmd) == 0 or len(cmd) > 0xffff:
		exit('Invalid command length.')
	
	sock = ClientControlSocket(config.control_socket)
	send_message(sock, 'CSUBM', hex_len(cmd, 4), cmd)
	
	msg = sock.read_all()
	if msg[:5] == 'SUBOK':
		print 'Job submitted with id %s.' % msg[5:]
	elif msg[:5] == 'SUBFL':
		exit('Submission failure.')
	else:
		exit('Protocol error.')

if __name__ == '__main__':
	main()
