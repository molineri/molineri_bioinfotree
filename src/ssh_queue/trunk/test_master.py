from deferredunittest import DeferredTestCase
from master import Master, NodeReviver, NodeProtocol
from twisted.internet import reactor
from twisted.internet.defer import Deferred
import config
import unittest as ut

class MasterMock(object):
	def __init__(self):
		self.nodes = []
		self.event_deferred = None
	
	def node_connected(self, node):
		self.nodes.append(node)
		if self.event_deferred is not None:
			d = self.event_deferred
			self.event_deferred = None
			d.callback('node_connected')
	
	def wait_event(self):
		assert self.event_deferred is None
		self.event_deferred = Deferred()
		return self.event_deferred

class NodeReviverMock(object):
	def __init__(self, master):
		self.master = master
		self.running = False
	
	def start(self):
		self.running = True
		reactor.callLater(0, self.master.node_connected, NodeProtocolMock('node01', self.master))
	
	def stop(self):
		self.running = False
		d = Deferred()
		reactor.callLater(0, d.callback, None)
		return d
	
	def add(self, node):
		pass

class NodeProtocolMock(object):
	def __init__(self, name, consumer):
		self.name = name
		self.consumer = consumer
		self.transport = self._build_transport()
	
	def set_consumer(self, consumer):
		self.consumer = consumer
	
	def node_connected(self):
		self.consumer.node_connected(self)
	
	def node_failure(self):
		self.consumer.node_failure(self, 0)
	
	def processEnded(self, status):
		self.consumer.node_failure(self, status)
	
	def _build_transport(self):
		class TransportMock(object):
			loseConnection = self._loseConnection
		return TransportMock()
	
	def _loseConnection(self):
		reactor.callLater(0, self.processEnded, 0)

class TestNodeReviver(DeferredTestCase):
	def setUp(self):
		self.connection_trial_delays_bak = config.connection_trial_delays
		config.connection_trial_delays = [0, 1]
	
	def tearDown(self):
		config.connection_trial_delays = self.connection_trial_delays_bak
	
	def testLifecycle(self):
		mm = MasterMock()
		nr = NodeReviver(mm)
		nr.start()
		self.assertDeferredEquals(nr.stop(), None)
	
	def testSpuriousNodeFailure(self):
		mm = MasterMock()
		nr = NodeReviver(mm)
		nd = NodeProtocolMock('node01', nr)

		nr.start()
		nr.node_failure(nd, 0)
		self.assertEquals(len(nr.queue), 0)
		self.assertDeferredEquals(nr.stop(), None)
	
	def testSpuriousNodeConnected(self):
		mm = MasterMock()
		nr = NodeReviver(mm)
		nd = NodeProtocolMock('node01', nr)
		
		nr.start()
		nr.node_connected(nd)
		self.assertEquals(len(mm.nodes), 0)
		self.assertDeferredEquals(nr.stop(), None)
	
	def testNodeConnect(self):
		mm = MasterMock()
		nr = self._patch_reviver(NodeReviver(mm))
		
		nr.start()
		nr.add('node01')
		self.assertDeferredEquals(mm.wait_event(), 'node_connected')
		self.assertEquals([n.name for n in mm.nodes], ['node01'])
		
		self.assertDeferredEquals(nr.stop(), None)
	
	def testNodeConnectWithFailure(self):
		mm = MasterMock()
		nr = self._patch_reviver(NodeReviver(mm), True)
		
		nr.start()
		nr.add('node01')
		self.assertDeferredEquals(mm.wait_event(), 'node_connected')
		self.assertEquals([n.name for n in mm.nodes], ['node01'])
		
		self.assertDeferredEquals(nr.stop(), None)
	
	def _patch_reviver(self, node_reviver, fail_first=False):
		def connect_node(name, trial):
			protocol = NodeProtocolMock(name, node_reviver)
			node_reviver.connecting[name] = (protocol, trial)
			if fail_first and trial == 0:
				reactor.callLater(0, protocol.node_failure)
			else:
				reactor.callLater(0, protocol.node_connected)
		
		node_reviver._connect_node = connect_node
		return node_reviver

class TestMaster(DeferredTestCase):
	def setUp(self):
		self.command_queue_filename_bak = config.command_queue_filename
		config.command_queue_filename = ':memory:'
		
		self.master = Master()
		self.master.missing_nodes = NodeReviverMock(self.master)
	
	def tearDown(self):
		del self.master
		config.command_queue_filename = self.command_queue_filename_bak
	
	def testLifecycle(self):
		self.master.start()
		self.assertDeferredEquals(self.master.stop(), None)
	
	def testNodeLyfecicle(self):
		self.master.start()
		self.assertEquals(True, self.master.missing_nodes.running)
		self.assertDeferredEquals(self.master.stop(), None)
		self.assertEquals(False, self.master.missing_nodes.running)

if __name__ == '__main__':
	ut.main()
