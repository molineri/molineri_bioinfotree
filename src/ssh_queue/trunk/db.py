from collections import deque
from datetime import datetime, timedelta
from sqlite3 import connect as sqlite_connect
import config

class CommandQueue(object):
	def __init__(self, filename):
		self._db, self._cursor = self._open(filename)
	
	def add(self, command):
		self._cursor.execute('INSERT INTO queue (command) VALUES(?)', (command,))
		return self._cursor.lastrowid
	
	def assign(self, node):
		self._cursor.execute('SELECT oid, command FROM queue WHERE status="queued" ORDER BY timestamp LIMIT 1')
		row = self._cursor.fetchone()
		if row is not None:
			oid, command = row
			self._cursor.execute('UPDATE queue SET status="running", node=? WHERE oid=?', (node, oid))
			return oid, command
		else:
			return None
	
	def reset(self, node):
		self._cursor.execute('UPDATE queue SET status="queued", node=NULL WHERE node=? AND status="running"', (node,))
	
	def completed(self, oid):
		self._purge_old_completed()
		self._cursor.execute('UPDATE queue SET status="completed", timestamp=current_timestamp WHERE oid=?', (oid,))
	
	def abort(self, oid):
		self._cursor.execute('SELECT status, node FROM queue WHERE oid=?', (oid,))
		row = self._cursor.fetchone()
		if row is None:
			raise ValueError, 'no such oid'
		
		status = row[0]
		if status == 'running':
			res = row[1]
		elif status != 'queued':
			res = None
		else:
			raise ValueError, 'command is neither queued nor running'
		
		self._cursor.execute('DELETE FROM queue WHERE oid=?', (oid,))
		return res
	
	def list(self):
		self._purge_old_completed()
		self._cursor.execute('SELECT oid, command, timestamp, status, node FROM queue ORDER BY timestamp')
		while True:
			row = self._cursor.fetchone()
			if row is None:
				break
			else:
				yield row
	
	def load_info(self):
		self._cursor.execute('SELECT COUNT(*) FROM queue WHERE status="running"')
		running_count = self._cursor.fetchone()[0]
		
		self._cursor.execute('SELECT COUNT(*) FROM queue WHERE status ="queued"')
		queued_count = self._cursor.fetchone()[0]
		
		return running_count, queued_count
	
	def _open(self, filename):
		db = sqlite_connect(filename)
		db.isolation_level = None
		cursor = db.cursor()
		
		try:
			cursor.execute('UPDATE queue SET status="queued", node=NULL WHERE status!="completed" AND status!="aborted"')
		except:
			db = self._create(cursor)
		
		return db, cursor
	
	def _create(self, cursor):
		schema = '''
			CREATE TABLE queue (
				command TEXT NOT NULL,
				timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
				status TEXT NOT NULL DEFAULT "queued",
				node TEXT DEFAULT NULL
			)
		'''
		cursor.execute(schema)
	
	def _purge_old_completed(self):
		dt = datetime.now() - timedelta(seconds=config.completed_purge_timeout)
		self._cursor.execute('DELETE FROM queue WHERE status="completed" AND timestamp<?', (dt,))
