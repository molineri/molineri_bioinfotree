from cStringIO import StringIO
from socket import socket, AF_UNIX, SOCK_STREAM, SHUT_WR

def hex_len(obj, field_width):
	hx = str(hex(len(obj)))[2:]
	if len(hx) > field_width:
		raise ValueError, 'size too big for the given field width'
	elif len(hx) < field_width:
		hx = ('0'*(field_width-len(hx))) + hx
	return hx

def send_message(transport, *parts):
	msg = ''.join(parts).encode('utf8')
	transport.write(msg)

class ClientControlSocket(object):
	def __init__(self, path):
		self.sock = socket(AF_UNIX, SOCK_STREAM)
		self.sock.connect(path)
	
	def iter_lines(self):
		buffer = StringIO()
		while True:
			chunk = self.sock.recv(4096)
			if len(chunk) == 0:
				break
			else:
				buffer.write(chunk)

		self.sock.close()
		buffer.seek(0)
		return buffer
	
	def read_all(self):
		buffer = ''
		while True:
			chunk = self.sock.recv(4096)
			if len(chunk) == 0:
				break
			else:
				buffer += chunk
		
		self.sock.close()
		return buffer
	
	def write(self, data):
		self.sock.send(data)
	
	def close_write(self):
		self.sock.shutdown(SHUT_WR)

class CommandDecoder(object):
	def __init__(self):
		self.buffer = ''
		self.peek_pos = 0
		self.cmd_len = 5
	
	def decode(self, chunk):
		self.buffer += chunk.encode('utf8')
		
		try:
			while len(self.buffer) >= self.cmd_len:
				cmd = self.buffer[:self.cmd_len]
				self.peek_pos = self.cmd_len
				
				if cmd == 'READY':
					yield cmd, None
				elif cmd == 'SLEEP':
					yield cmd, None
				elif cmd == 'CSTRT':
					oid_size = int(self._peek(2), 16)
					oid = int(self._peek(oid_size))
					spec_size = int(self._peek(4), 16)
					spec = self._peek(spec_size)
					yield cmd, (oid, spec)
				elif cmd == 'CCMPL':
					oid_size = int(self._peek(2), 16)
					oid = int(self._peek(oid_size))
					status = int(self._peek(2), 16)
					yield cmd, (oid, status)
				elif cmd == 'CKILL':
					oid_size = int(self._peek(2), 16)
					oid = int(self._peek(oid_size))
					yield cmd, (oid,)
				else:
					raise InvalidCommand
				
				self.buffer = self.buffer[self.peek_pos:]
		except NeedMoreData:
			pass
	
	def _peek(self, size):
		if len(self.buffer) < self.peek_pos + size:
			raise NeedMoreData
		else:
			res = self.buffer[self.peek_pos:self.peek_pos+size]
			self.peek_pos += size
			return res

class InvalidCommand(Exception): pass
class NeedMoreData(Exception): pass
