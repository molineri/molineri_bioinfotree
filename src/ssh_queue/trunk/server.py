#!/usr/bin/env python

from __future__ import with_statement

from ioutil import CommandDecoder, InvalidCommand, NeedMoreData, hex_len, send_message
from master import Master
from os import getpid, unlink
from sys import exit
from timeutil import check_work_time, is_work_time
from twisted.internet import reactor
from twisted.internet.protocol import Factory, Protocol
import config

class ControlCommandDecoder(CommandDecoder):
	def decode(self, chunk):
		self.buffer += chunk.encode('utf8')

		if len(self.buffer) >= self.cmd_len:
			cmd = self.buffer[:self.cmd_len]
			self.peek_pos = self.cmd_len
			
			if cmd == 'CLIST':
				res = cmd, None
			elif cmd == 'CLOAD':
				res = cmd, None
			elif cmd == 'CSUBM':
				spec_size = int(self._peek(4), 16)
				spec = self._peek(spec_size)
				res = cmd, (spec,)
			elif cmd == 'CKILL':
				oid_size = int(self._peek(2), 16)
				oid = int(self._peek(oid_size))
				res = cmd, (oid,)
			else:
				raise InvalidCommand
			
			if len(self.buffer) > self.peek_pos:
				raise InvalidCommand
			else:
				return res
		else:
			raise NeedMoreData

class ControlChannel(Protocol):
	def connectionMade(self):
		self.decoder = ControlCommandDecoder()
	
	def dataReceived(self, data):
		try:
			cmd, args = self.decoder.decode(data)
			if cmd == 'CLIST':
				rows = []
				for info in self.factory.master.list_commands():
					rows.append('\t'.join(str(s).replace('\t', ' ') for s in info))
				send_message(self.transport, '\n'.join(rows))
			
			elif cmd == 'CLOAD':
				send_message(self.transport, '\t'.join(str(s) for s in self.factory.master.load_info()))
				
			elif cmd == 'CSUBM':
				oid = self.factory.master.add_command(args[0])
				send_message(self.transport, 'SUBOK', str(oid))
			
			elif cmd == 'CKILL':
				try:
					self.factory.master.abort_command(args[0])
					send_message(self.transport, 'KILOK', str(args[0]))
				except ValueError:
					send_message(self.transport, 'KILFL')
		
		except NeedMoreData:
			return
		except InvalidCommand:
			pass

		self.transport.loseConnection()

def write_pid(filename):
	with file(filename, 'w') as fd:
		print >>fd, str(getpid())

def clear_pid(filename):
	try:
		unlink(filename)
	except OSError:
		pass

def main():
	if not is_work_time():
		exit('This is not the proper time to work.')
	
	write_pid(config.server_pidfile)
	
	master = Master()
	reactor.callWhenRunning(master.start)
	reactor.callLater(60, check_work_time)
	
	factory = Factory()
	factory.protocol = ControlChannel
	factory.master = master
	
	reactor.addSystemEventTrigger('before', 'shutdown', master.stop)
	reactor.addSystemEventTrigger('before', 'shutdown', clear_pid, config.server_pidfile)
	reactor.listenUNIX(config.control_socket, factory)
	reactor.run()

if __name__ == '__main__':
	main()
