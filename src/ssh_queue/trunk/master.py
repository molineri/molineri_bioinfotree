from collections import deque
from db import CommandQueue
from ioutil import CommandDecoder, InvalidCommand, hex_len, send_message
from time import time
from twisted.internet.defer import Deferred, succeed
from twisted.internet.protocol import ProcessProtocol
from twisted.internet import reactor
import config
import os

class Master(object):
	def __init__(self):
		self.running = False
		self.shutdown_deferred = None
		self.nodes = {}
		self.free_nodes = NodeQueue()
		self.missing_nodes = NodeReviver(self)
		self.commands = CommandQueue(config.command_queue_filename)
	
	def start(self):
		self.shutdown_deferred = None
		self.running = True
		for node in config.nodes:
			self.missing_nodes.add(node)
		self.missing_nodes.start()
	
	def stop(self):
		self.running = False
		self.shutdown_deferred = Deferred()
		mnd = self.missing_nodes.stop()
		mnd.addCallback(self._stop_nodes)
		return self.shutdown_deferred
	
	def load_info(self):
		info = (len(self.nodes), len(self.free_nodes)) + self.commands.load_info()
		return info
	
	def add_command(self, command):
		if len(command) == 0 or len(command) > 0xffff:
			raise ValueError, 'invalid command length'
		
		oid = self.commands.add(command)
		if not self.free_nodes.empty():
			self._process_commands()
		return oid
	
	def abort_command(self, oid):
		# raises ValueError if oid doesn't correspond to an abortable job
		node_name = self.commands.abort(oid)
		
		if node_name is not None:
			node = self.nodes[node_name]
			node.abort_command(oid)
	
	def list_commands(self):
		return self.commands.list()
	
	def node_connected(self, node):
		self.nodes[node.name] = node
	
	def node_failure(self, node, status):
		self.commands.reset(node.name)
		if self.running:
			self.missing_nodes.add(node.name)
		else:
			del self.nodes[node.name]
			if len(self.nodes) == 0:
				self._fire_shutdown()
	
	def node_ready(self, node):
		if self.running:
			self.free_nodes.append(node)
			self._process_commands()
	
	def node_asleep(self, node):
		if self.running:
			del self.nodes[node.name]
			self.free_nodes.maybe_remove(node)
			self.missing_nodes.node_asleep(node)
	
	def job_completed(self, node, oid, status):
		self.commands.completed(oid)
	
	def _stop_nodes(self, res):
		if len(self.nodes) == 0:
			self._fire_shutdown()
		else:
			for node in self.nodes.itervalues():
				node.transport.loseConnection()
	
	def _process_commands(self):
		while True:
			node = self.free_nodes.peek()
			if node is None:
				break
			
			cmd_info = self.commands.assign(node.name)
			if cmd_info is None:
				break
			
			self.free_nodes.pop()
			self._start_command(node, cmd_info)
	
	def _start_command(self, node, cmd_info):
		node.start_command(*cmd_info)
	
	def _fire_shutdown(self):
		self.shutdown_deferred.callback(None)

class NodeQueue(object):
	def __init__(self):
		self.queue = deque()
	
	def __len__(self):
		return len(self.queue)
	
	def empty(self):
		return len(self.queue) == 0
	
	def append(self, node):
		self.queue.append(node)
	
	def peek(self):
		if len(self.queue):
			return self.queue[0]
		else:
			return None
	
	def pop(self):
		self.queue.popleft()
	
	def maybe_remove(self, node):
		try:
			self.queue.remove(node)
		except ValueError:
			pass

class NodeReviver(object):
	def __init__(self, master):
		self.master = master
		self.running = False
		self.shutdown_deferred = None
		self.delayed_call = None
		self.queue = deque()
		self.connecting = {}
	
	def start(self):
		self.shutdown_deferred = None
		self.running = True
		self._process_queue()
	
	def stop(self):
		self.running = False
		self.queue.clear()
		
		if self.delayed_call and self.delayed_call.active():
			self.delayed_call.cancel()
			self.delayed_call = None
		
		if len(self.connecting) > 0:
			for name, (protocol, connection_trial) in self.connecting.iteritems():
				protocol.transport.loseConnection()
			self.shutdown_deferred = Deferred()
			return self.shutdown_deferred
		else:
			return succeed(None)
	
	def add(self, node_name):
		self._append_node(node_name, 0)
	
	def node_connected(self, node):
		try:
			self.connecting.pop(node.name)
		except KeyError:
			node.transport.loseConnection()
		else:
			node.set_consumer(self.master)
			self.master.node_connected(node)
			
			if len(self.queue) > 0:
				self._process_queue()
	
	def node_asleep(self, node):
		self.connecting.pop(node.name)
		last_trial = len(config.connection_trial_delays)-1
		self._append_node(node.name, last_trial)
	
	def node_failure(self, node, status):
		try:
			protocol, connection_trial = self.connecting.pop(node.name)
		except KeyError:
			return
		
		if self.running:
			connection_trial = min(connection_trial+1, len(config.connection_trial_delays)-1)
			self._append_node(node.name, connection_trial)
			self._process_queue()
		
		elif len(self.connecting) == 0:
			self.shutdown_deferred.callback(None)
	
	def _append_node(self, name, connection_trial):
		info = (name, time()+config.connection_trial_delays[connection_trial], connection_trial)
		self.queue.append(info)
		
		if self.running:
			self._process_queue()
	
	def _process_queue(self):
		while len(self.queue) > 0 and len(self.connecting) < config.max_concurrent_revivals:
			delta = self.queue[0][1] - time()
			if delta <= 0:
				name, tstamp, trial = self.queue.popleft()
				self._connect_node(name, trial)
			else:
				if self.delayed_call is None:
					self.delayed_call = reactor.callLater(delta, self._queue_event)
				else:
					self.delayed_call.reset(delta)
				break

	def _queue_event(self):
		self.delayed_call = None
		self._process_queue()
	
	def _connect_node(self, name, trial):
		protocol = NodeProtocol(name, self)
		self.connecting[name] = (protocol, trial)
		reactor.spawnProcess(protocol, '/bin/bash', args=['/bin/bash', '-c', config.connection_cmd % name], childFDs={0:'w', 1:'r', 2:2}, env=os.environ)

class NodeProtocol(ProcessProtocol):
	def __init__(self, name, consumer):
		self.name = name
		self.consumer = consumer
		self.connection_established = False
		self.decoder = CommandDecoder()
		self.asleep = False
	
	def set_consumer(self, consumer):
		self.consumer = consumer
	
	def start_command(self, oid, command):
		oid_repr = str(oid)
		oid_size = hex_len(oid_repr, 2)
		cmd_size = hex_len(command, 4)
		send_message(self.transport, 'CSTRT', oid_size, oid_repr, cmd_size, command)
	
	def abort_command(self, oid):
		oid_repr = str(oid)
		oid_size = hex_len(oid_repr, 2)
		send_message(self.transport, 'CKILL', oid_size, oid_repr)
	
	def outReceived(self, data):
		try:
			for cmd, args in self.decoder.decode(data):
				if self.connection_established:
					if cmd == 'READY':
						self.consumer.node_ready(self)
					elif cmd == 'SLEEP':
						self._handle_sleep()
						break
					elif cmd == 'CCMPL':
						self.consumer.job_completed(self, *args)
					else:
						raise InvalidCommand
				elif cmd == 'READY':
					self.connection_established = True
					self.consumer.node_connected(self)
					self.consumer.node_ready(self)
				elif cmd == 'SLEEP':
					self._handle_sleep()
					break
				else:
					raise InvalidCommand
		
		except InvalidCommand:
			self.transport.loseConnection()
	
	def processEnded(self, status):
		if not self.asleep:
			self.consumer.node_failure(self, status)
	
	def _handle_sleep(self):
		self.asleep = True
		self.transport.loseConnection()
		self.consumer.node_asleep(self)
