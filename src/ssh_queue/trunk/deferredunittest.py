from cStringIO import StringIO
from threading import Event, Thread
from twisted.internet import reactor
from twisted.python.failure import Failure
import unittest as ut

class DeferredTestCase(ut.TestCase):
	def assertDeferredEquals(self, deferred, value, filter=None):
		res = deferredResult(deferred)
		
		if isinstance(res, Failure):
			res.raiseException()
		else:
			if filter is not None:
				res = filter(res)
			self.assertEquals(value, res)
	
	def assertDeferredRaises(self, deferred, exception):
		res = deferredResult(deferred)
		
		if isinstance(res, Failure):
			exc = res.value
			if not isinstance(exc, exception):
				msg = StringIO()
				print >>msg, 'wrong exception raised: %s\n' % exc.__class__.__name__
				res.printTraceback(file=msg)
				
				self.fail(msg.getvalue())
		else:
			self.fail('no exception raised')
	
def deferredResult(deferred):
	event = Event()
	deferredResult = []
	
	def onSuccess(res):
		deferredResult.append(res)
		try:
			reactor.crash()
			event.set()
		except:
			pass
	
	def onFailure(failure):
		deferredResult.append(failure)
		try:
			reactor.crash()
			event.set()
		except:
			pass
	
	def initDeferred():
		reactor.run()

	deferred.addCallback(onSuccess)
	deferred.addErrback(onFailure)
	
	if len(deferredResult) == 0:
		th = Thread(target=initDeferred)
		th.run()
		event.wait()
	
	return deferredResult[0]
