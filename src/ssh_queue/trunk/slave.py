#!/usr/bin/env python

from ioutil import *
from signal import SIGTERM, SIGKILL
from sys import exit
from timeutil import check_work_time, is_work_time
from twisted.internet import reactor
from twisted.internet.defer import Deferred, succeed
from twisted.internet.protocol import Protocol, ProcessProtocol
from twisted.internet.stdio import StandardIO
import config
import os

class Slave(object):
	def __init__(self):
		self.control_channel = ControlChannel(self)
		self.job_channel = None
		self.shutdown_deferred = None
		self.devnull = os.open(os.devnull, os.O_RDWR)
	
	def start(self):
		self.shutdown_deferred = None
		if is_work_time():
			reactor.callLater(60, check_work_time)
			send_message(self.control_channel.transport, 'READY')
		else:
			send_message(self.control_channel.transport, 'SLEEP')
			self.control_channel.transport.loseConnection()
	
	def stop(self):
		self.shutdown_deferred = Deferred()
		self.control_channel.stop().addCallback(self._stop_job_and_wait)
		return self.shutdown_deferred
	
	def shutdown(self):
		reactor.stop()
	
	def is_ready(self):
		return self.job_channel is None
	
	def get_oid(self):
		return self.job_channel.oid if self.job_channel else None
	
	def run_command(self, oid, command):
		self.job_channel = JobChannel(oid, self)
		reactor.spawnProcess(self.job_channel, '/bin/bash', args=['/bin/bash', '-c', command], childFDs={0:self.devnull, 1:self.devnull, 2:self.devnull}, env=os.environ)
	
	def abort_command(self):
		self.job_channel.abort()
	
	def job_completed(self, oid, status):
		if oid is not None:
			hex_status = str(hex(status))[2:]
			if len(hex_status) == 1:
				hex_status = '0' + hex_status
			oid_repr = str(oid)
			send_message(self.control_channel.transport, 'CCMPL', hex_len(oid_repr, 2), oid_repr, hex_status)
		
		self.job_channel = None
		if self.shutdown_deferred:
			self.shutdown_deferred.callback(None)
		else:
			send_message(self.control_channel.transport, 'READY')
	
	def _stop_job_and_wait(self, ignore):
		if self.job_channel:
			self.job_channel.abort()
		else:
			self.shutdown_deferred.callback(None)

class ControlChannel(Protocol):
	def __init__(self, parent):
		self.parent = parent
		self.decoder = CommandDecoder()
		self.shutdown_deferred = None
		StandardIO(self)
	
	def stop(self):
		if self.transport.disconnected:
			return succeed(None)
		else:
			self.shutdown_deferred = Deferred()
			self.transport.loseConnection()
			return self.shutdown_deferred
	
	def connectionLost(self, reason):
		if self.shutdown_deferred is None:
			self.parent.shutdown()
		else:
			self.shutdown_deferred.callback(None)
	
	def dataReceived(self, data):
		try:
			for cmd, args in self.decoder.decode(data):
				if cmd == 'CSTRT':
					if not self.parent.is_ready():
						raise InvalidCommand
					else:
						self.parent.run_command(*args)
				
				elif cmd == 'CKILL':
					if args[0] != self.parent.get_oid():
						raise InvalidCommand
					else:
						self.parent.abort_command()
				
				else:
					raise InvalidCommand
		
		except InvalidCommand:
			self.parent.shutdown()

class JobChannel(ProcessProtocol):
	def __init__(self, oid, parent):
		self.oid = oid
		self.parent = parent
		self.killer = None
		self.exit_code = None
	
	def abort(self):
		self.oid = None
		self.killer = reactor.callLater(config.killer_timeout, self._killer, self.transport.pid)
		os.kill(-self.transport.pid, SIGTERM)
	
	def processEnded(self, status):
		self.exit_code = status.value.exitCode
		if self.killer is None:
			self.parent.job_completed(self.oid, self.exit_code)
	
	def _killer(self, pid):
		try:
			os.kill(-pid, SIGKILL)
		except OSError:
			pass
		
		self.parent.job_completed(self.oid, self.exit_code)

def patch_twisted_process():
	# hack to force spawned processes to create their own process group
	try:
		import twisted.internet.process as tip
		original_setup_child = getattr(tip.Process, '_setupChild')
		
		def patched_setup_child(self, *args, **kwargs):
			os.setpgrp()
			return original_setup_child(self, *args, **kwargs)
		
		setattr(tip.Process, '_setupChild', patched_setup_child)
		return True
		
	except:
		return False

def main():
	if not patch_twisted_process():
		exit('Cannot monkey-patch twisted Process.')
	
	s = Slave()
	reactor.callWhenRunning(s.start)
	reactor.addSystemEventTrigger('before', 'shutdown', s.stop)
	reactor.run()

if __name__ == '__main__':
	main()
