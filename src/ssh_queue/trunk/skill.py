#!/usr/bin/env python

from ioutil import *
from optparse import OptionParser
from sys import exit
import config

def main():
	parser = OptionParser(usage='%prog OID')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	oid = args[0]
	if len(oid) == 0 or len(oid) > 0xff:
		exit('Invalid job ID.')
	
	sock = ClientControlSocket(config.control_socket)
	send_message(sock, 'CKILL', hex_len(oid, 2), oid)
	
	msg = sock.read_all()
	if msg[:5] == 'KILOK':
		print 'Job %s aborted.' % oid
	elif msg[:5] == 'KILFL':
		exit('Cannot abort job.')
	else:
		exit('Protocol error.')

if __name__ == '__main__':
	main()
