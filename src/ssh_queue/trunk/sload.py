#!/usr/bin/env python

from ioutil import *
from optparse import OptionParser
from sys import exit
import config

def main():
	parser = OptionParser(usage='%prog')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	sock = ClientControlSocket(config.control_socket)
	sock.write('CLOAD')
	
	msg = sock.read_all()
	try:
		nums = [ int(t) for t in msg.split('\t') ]
		if len(nums) != 4:
			raise ValueError
	except ValueError:
		exit('Protocol error.')
	
	print 'Available nodes: % 5d' % nums[0]
	print 'Idle nodes:      % 5d' % nums[1]
	print 'Running jobs:    % 5d' % nums[2]
	print 'Queued jobs:     % 5d' % nums[3]

if __name__ == '__main__':
	main()
