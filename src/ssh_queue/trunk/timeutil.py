from datetime import datetime
from twisted.internet import reactor
import config

def is_work_time():
	assert (0, 0) <= config.work_time_range[0] <= (24, 0), 'invalid start time'
	assert (0, 0) <= config.work_time_range[1] <= (24, 0), 'invalid stop time'
	
	now = datetime.now()
	c = (now.hour, now.minute)
	if config.work_time_range[0] <= config.work_time_range[1]:
		return config.work_time_range[0] <= c <= config.work_time_range[1]
	else:
		return c >= config.work_time_range[0] or c <= config.work_time_range[1]

def check_work_time():
	if is_work_time():
		reactor.callLater(60, check_work_time)
	else:
		reactor.stop()
