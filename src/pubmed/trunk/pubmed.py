from cElementTree import parse as xml_parse
from cStringIO import StringIO
from simplejson import dumps, loads
from sys import maxint
from time import mktime, sleep, strftime, strptime, time
from urllib import quote
from urllib2 import urlopen

class LocalVault(object):
	''' This class allows to retrieve and store PubMed articles into a local
	    database.
	'''

	def __init__(self, connection):
		''' Class constructor.
		
		    @param connection: a DBAPI-2.0 compatible connection object.
		'''
		self.connection = connection
	
	def fetch(self, pmids):
		''' Fetches articles given their PMIDs.
		
		    @param pmids: an iterable yielding PMIDs.
		    @return: a list of L{Article} instances and a set of PMIDs
		             corresponding to missing articles.
		'''
		articles = []
		cursor = self.connection.cursor()
		
		summaries, missing = self.fetch_summaries(pmids, cursor)
		for pmid, summary in summaries.iteritems():
			articles.append(Article(pmid, summary, *self.fetch_links(pmid, cursor)))
		
		return articles, missing
	
	def iter_articles(self):
		''' Iterates on the articles stored in the vault.
		
		    @return: an iterator over L{Article} instances.
		'''
		cursor = self.connection.cursor()
		cursor.execute('SELECT pmid, info FROM article_minimal ORDER BY pubdate DESC')
		while True:
			row = cursor.fetchone()
			if row is None:
				break
			else:
				yield Article(row[0], loads(row[1]), *self.fetch_links(row[0]))
		
	def missing(self, pmids):
		''' Checks a set of PMIDs, identifying those that are missing from the 
		    vault.
		    
		    @param pmids: a set of PMIDs.
		    @return: the set of PMIDs missing from the vault.
		'''
		if len(pmids) == 0:
			return set()

		cursor = self.connection.cursor()		
		query = 'SELECT pmid FROM article_minimal WHERE pmid IN (%s)' % ','.join(str(i) for i in pmids)
		cursor.execute(query)
		
		found = set()
		while True:	
			row = cursor.fetchone()
			if row is None:
				break
			else:
				found.add(row[0])
			
		return pmids - found
	
	def count_missing_links(self):
		''' Counts the number of linked article whose summary is missing.
		
		    @return: the count.
		'''
		cursor = self.connection.cursor()
		query = 'SELECT COUNT(DISTINCT to_pmid) FROM article_link WHERE to_pmid NOT IN (SELECT pmid FROM article_minimal)'
		cursor.execute(query)
		
		return cursor.fetchone()[0]
	
	def iter_missing_links(self):
		''' Iterates over linked articles whose summary is missing.
		
			@return: an iterator over PMIDs.
		'''
		cursor = self.connection.cursor()
		query = 'SELECT DISTINCT to_pmid FROM article_link WHERE to_pmid NOT IN (SELECT pmid FROM article_minimal)'
		cursor.execute(query)
		
		while True:
			row = cursor.fetchone()
			if row is None:
				break
			else:
				yield row[0]
	
	def store(self, article):
		''' Stores an article into the vault.
		
		    @param article: an L{Article} instance.
		'''
		info = dumps(article.summary_dict_unsafe())
		pub_date = self.extract_pub_date(article)
		
		cursor = self.connection.cursor()
		query = 'INSERT INTO article_minimal (pmid, pubdate, info) VALUES(%s, %s, %s)'
		cursor.execute(query, (article.pmid, strftime('%Y%m%d', pub_date), dumps(article.summary_dict_unsafe())))
		
		for isref, pmid_set in (('TRUE', article.references), ('FALSE', article.related)):
			for link_pmid in pmid_set:
				cursor.execute('INSERT INTO article_link (from_pmid, to_pmid, is_reference) VALUES(%s, %s, %s)', (article.pmid, link_pmid, isref))
	
	##
	## Internal use only
	##
	def fetch_summaries(self, pmids, cursor):
		''' Fetches articles summaries from the database.
		
		    @param pmids: a list of PMIDs.
		    @param cursor: a database cursor.
		    @return: a dictionary of summaries (dictionaries themselves) and a
		             set of missing PMIDs.
		'''
		pmids = set(pmids)
		summaries = {}
		
		# WARNING: the following query can be used to perform an SQL injection
		query = 'SELECT pmid, info FROM article_minimal WHERE pmid in (%s)' % ','.join(str(i) for i in pmids)
		cursor.execute(query)
		
		while True:
			row = cursor.fetchone()
			if row is None:
				break
			elif row[0] not in pmids:
				raise KeyError, 'spurious record received from the database'
			else:
				pmids.remove(row[0])
				summaries[pmid] = loads(info)
		
		return summaries, pmids
	
	def fetch_links(self, pmid, cursor):
		''' Fetches all links regarding an article.
		
		    @param pmid: a PMID.
		    @param cursor: a database cursor.
		    @return: two sets of PMIDs: one for references, one for related
		             articles.
		'''
		cursor.execute('SELECT to_pmid, is_reference FROM article_link WHERE from_pmid = %s', pmid)
		references = set()
		related = set()
		
		while True:
			row = cursor.fetchone()
			if row is None:
				break
			elif row[1] == True:
				references.add(row[0])
			else:
				related.add(row[0])
		
		return references, related

	def extract_pub_date(self, article):
		''' Extracts the pubblication date of an article.
		
		    Warning: currently broken.
		    
		    @param article: an L{Article} instance.
		    @return: a 9-uple representing the date.
		'''
# 		spaces = article.pub_date.count(' ')
# 		assert spaces in (0, 1, 2), "unexpected number of spaces in date '%s'" % article.pub_date
# 
# 		format = ' '.join(['%Y', '%b', '%d'][:spaces+1])
# 		return strptime(article.pub_date, format)
		return strptime('2006', '%Y')
	
class RemoteVault(object):
	''' This class interacts with PubMed web services to retrieve article
	    informations either by using their PMIDs or by means of full text
	    searches.
	'''
	
	def __init__(self, base_url='http://eutils.ncbi.nlm.nih.gov/entrez/eutils', query_interval = 3):
		'''5 Class constructor.
		
		    @param base_url: the base URL of PubMed web services.
		    @param query_interval: the number of seconds to wait between
		                           successive queries.
		'''
		self.base_url = base_url
		self.query_interval = query_interval
		self.last_query_timestamp = 0
	
	def fetch(self, pmids, block_size=300):
		''' Fetches articles given their PMIDs.
		
		    @param pmids: an iterable yielding PMIDs.
		    @param block_size: the maximum number of articles to fetch with a
		                       single query.
		    @return: a list of L{Article} instances (one for each PMID actually
		             existing).
		    @raises KeyError: if an inconsistency across multiple PubMed
		                      replies is detected.
		    @raise ValueError: in case of type conversion errors.
		'''
		articles = []
		
		for block in BlockSplitter(pmids, block_size):
			#DEBUG
			start = time()
			summaries = self.fetch_summaries(block)
			print 'fetch_summaries: %.1f' % (time() - start)
			
			if len(summaries):			
				#DEBUG
				start = time()
				links = self.fetch_links(summaries.iterkeys())
				print 'fetch_links: %.1f' % (time() - start)
			else:
				links = []
			
			for pmid in block:
				try:
					summary = summaries.pop(pmid)
				except KeyError:
					continue
			
				try:
					link_dict = links.pop(pmid)
				except KeyError:
					raise KeyError, "can't find links for article %d" % pmid

				references = link_dict.get('pubmed_pubmed_refs', set())
				related = link_dict.get('pubmed_pubmed', set())
				articles.append(Article(pmid, summary, references, related))
			
			if len(summaries):
				raise KeyError, 'spurious summaries found'
			if len(links):
				raise KeyError, 'spurious links found'
		
		return articles
	
	def search(self, query, offset=0, maxnum=1000, block_size=300):
		''' Performs a full text search on the PubMed archive.
		
		    @param query: the search terms.
		    @param offset: the offset of the first result to be returned.
		    @param maxnum: the maximum number of results to be returned.
		    @param block_size: the maximum number of articles to fetch with a
		                       single query.
		    @return: a tuple containing a list of L{Article} instances and the
		             total number of results matching the search terms.
		'''
		pmids, total_count = self.search_pmids(query, offset, maxnum, block_size)
		articles = self.fetch(pmids, block_size)
		return articles, total_count
	
	def search_pmids(self, query, offset=0, maxnum=1000, block_size=300):
		''' Performs a full text search on the PubMed archive, returning only
		    PMIDs of the matching articles.
		
		    @param query: the search terms.
		    @param offset: the offset of the first result to be returned.
		    @param maxnum: the maximum number of results to be returned.
		    @param block_size: the maximum number of articles to fetch with a
		                       single query.
		    @return: a tuple containing a list of PMIDs and the total number of
		             results matching the search terms.
		    @raise ValueError: in case of type conversion errors.
		'''
		pmids = []
		total_count = -1
	
		for idx in xrange(0, maxnum, block_size):
			start = offset + idx
			params = [
				('term', query),
				('retstart', str(start)),
				('retmax', str(maxnum))
			]
			
			root = self.request('esearch.fcgi', params).getroot()
			assert root.tag == 'eSearchResult', 'expected eSearchResult, got %s' % root.tag
			
			id_list = root.find('IdList')
			assert id_list is not None, 'missing IdList element' 
			for pmid in id_list.findall('Id'):
				try:
					pmids.append(int(pmid.text))
				except ValueError:
					raise ValueError, 'malformed PMID %s' % pmid
			
			total_count = self.get_value(root, 'Count', int)
			if offset + idx + block_size >= total_count:
				break
		
		expected_num = min(maxnum, total_count - offset)
		assert len(pmids) == expected_num, '%d pmids found, %d expected' % (len(pmids), expected_num)
		return pmids, total_count
	
	##
	## Internal use only
	##	
	def request(self, service, params):
		''' Sends a GET request to the eutils server and parses the returned XML.
		
		    @param service: the name of the requested web service.
		    @param params: a list of parameters for the web service, in the form
		                   of (name, value).
		    @return: an B{ElementTree.ElementTree}.
		'''
		param_list = []
		for name, value in params:
			param_list.append('%s=%s' % (name, quote(value)))
		param_list.append('db=pubmed')
		url = '%s/%s?%s' % (self.base_url, service, '&'.join(param_list))
		
		timestamp = time()
		delta = self.query_interval + self.last_query_timestamp - timestamp
		if delta > 0:
			sleep(delta)
			self.last_query_timestamp = time()
		else:
			self.last_query_timestamp = timestamp
		
		url_handle = urlopen(url)
		try:
			string_fd = StringIO(url_handle.read())
		finally:
			url_handle.close()
		
		try:
			return xml_parse(string_fd)
		except:
			self.save_trace(url, string_fd)
			raise
	
	def fetch_summaries(self, pmids):
		''' Queries PubMed web services for article summaries.
		
		    @param pmids: the PMIDs of the articles we're interested in.
		    @return: a list of summaries in the form of Python dicts.
		    @raise ValueError: in case of type conversion errors.
		'''
		assert len(pmids)
		
		root = self.request('esummary.fcgi', [('id', ','.join(str(p) for p in pmids))]).getroot()
		assert root.tag == 'eSummaryResult', 'expected eSummaryResult, got %s' % root.tag
		
		parser = SummaryParser()
		summaries = {}
		for doc_elem in root.findall('DocSum'):
			pmid, summary = parser.parse(doc_elem)
			assert pmid not in summaries
			summaries[pmid] = summary
		
		return summaries
	
	def fetch_links(self, pmids):
		''' Queries PubMed web services for article links.
		
		    @param pmids: the PMIDs of the articles we're interested in.
		    @return: a list of link lists.
		    @raise ValueError: in case of type conversion errors.
		'''
		assert len(pmids)
		
		params = [ ('fromdb', 'pubmed') ]
		for pmid in pmids:
			params.append(('id', str(pmid)))
		
		root = self.request('elink.fcgi', params).getroot()
		assert root.tag == 'eLinkResult', 'expected eLinkResult, got %s' % root.tag
		
		parser = LinkParser()
		links = {}
		for link_set_elem in root:
			pmid, link_set = parser.parse(link_set_elem)
			assert pmid not in links
			links[pmid] = link_set
		
		return links
	
	def get_value(self, root, elem_name, xform=None):
		''' Retrieves the value of a named element in a tree, optionally
		    transforming it.
		    
		    @param root: the root node of the tree (an B{ElementTree.Element}
		                 instance).
		    @param elem_name: the element name.
		    @param xform: a callable transforming the retrieved value.
		    @return: the (possibly transformed) value contained in the named
		             element.
		'''
		res = root.findtext(elem_name)
		assert res is not None, "can't find element %s" % elem_name
		
		if xform is not None:
			res = xform(res)
		return res
	
	#DEBUG
	def save_trace(self, url, xml_fd):
		fd = file('trace.log', 'a')
		print >>fd, '-*-' * 20
		
		print >>fd, '=> URL'
		print >>fd, url
		
		print >>fd, '=> XML'
		print >>fd, xml_fd.getvalue()

		print >>fd, '*-*' * 20
		fd.close()

class SummaryParser(object):
	''' This class parses XML documents encoding article summaries. '''
	
	def parse(self, doc_elem):
		''' Parses a I{DocSum} element.
		
		    @param doc_elem: an I{ElementTree.Element} instance.
		    @return: an dict representing the summary fields.
		    @raises ValueError: in case of type conversion errors.
		'''
		assert doc_elem.tag == 'DocSum', 'expected DocSum, got %s' % doc_elem.tag
		assert len(doc_elem), 'got an empty DocSum element'

		id_elem = doc_elem[0]
		assert id_elem.tag == 'Id'
		try:
			pmid = int(id_elem.text)
		except ValueError:
			raise ValueError, 'malformed PMID %s' % id_elem.text

		summary = {}		
		for elem in doc_elem[1:]:
			name, value = self.parse_elem(elem)
			summary[self.mangle_name(name)] = value
		
		return pmid, summary

	##
	## Internal use only
	##
	def parse_elem(self, elem):
		''' Parses an I{Item} element.
		
		    @param elem: an I{ElementTree.Element} instance.
		    @return: a (name, value) tuple.
		    @raises ValueError: in case of type conversion errors.
		'''
		assert elem.tag == 'Item', 'expected Item, got %s' % elem.tag

		name = elem.attrib['Name']
		type = elem.attrib['Type']
		
		if type in ('Date', 'String'):
			return name, elem.text
		
		elif type == 'Integer':
			return name, int(elem.text)
					
		elif type == 'List':
			if name.endswith('List'):
				obj = self.parse_list(elem)
			else:
				obj = self.parse_dict(elem)
			return name, obj
		
		else:
			raise ValueError, 'unexpected element type: %s' % type
	
	def parse_list(self, elem):
		''' Parses a I{List} item.
		
		    @param elem: an I{ElementTree.Element} instance.
		    @return: a B{list} instance.
		'''
		return [ self.parse_elem(se)[1] for se in elem ]
	
	def parse_dict(self, elem):
		''' Parses an item marked as I{List}, but holding a dictionary.
		
		    @param elem: an I{ElementTree.Element} instance.
		    @return: a B{dict} instance.
		'''
		return dict( self.parse_elem(se) for se in elem )
	
	def mangle_name(self, name):
		''' Converts a field name into a pythonic form.
		
		    @param name: the original name.
		    @return: the mangled name.
		'''
		return '_'.join(t.lower() for t in self.uppercase_tokenizer(name))
	
	def uppercase_tokenizer(self, name):
		''' Tokenizes a name at each uppercase letter.
		
		    @param name: the original name.
		    @return: an iterator providing tokens.
		'''
		start = 0
		idx = 1
		while idx < len(name):
			if name[idx].isupper():
				yield name[start:idx]
				start = idx
			idx += 1
		yield name[start:]

class LinkParser(object):
	''' This class parses XML documents encoding article links. '''
	
	def parse(self, link_set_elem):
		''' Parses a I{LinkSet} element.
		
		    @param link_set_elem: an I{ElementTree.Element} instance.
		    @return: a dictonary of sets of PMIDs.
		    @raise ValueError: in case of type conversion errors.
		'''
		assert link_set_elem.tag == 'LinkSet', 'expected LinkSet, got %s' % link_set_elem.tag
		
		pmid = self.parse_id(link_set_elem)
		links = {}
		
		for elem in link_set_elem.findall('LinkSetDb'):
			link_name_elem = elem.find('LinkName')
			assert link_name_elem is not None, 'missing LinkName'
			links[link_name_eleme.text] = self.parse_links(elem)
		
		return pmid, links
	
	def parse_id(self, link_set_elem):
		''' Parses the PMID of the article a LinkSet refers to.
		
		    @param link_set_elem: an I{ElementTree.Element} instance.
		    @return: a PMID.
		    @raise ValueError: in case of type conversion errors.
		'''
		id_list = link_set_elem.find('IdList')
		assert id_list is not None, 'missing IdSet element'
		assert len(id_list) == 1, 'malformed IdSet content'
		
		id_elem = id_list[0]
		assert id_elem.tag == 'Id', 'missing or misplaced Id element'
		try:
			pmid = int(id_elem.text)
		except ValueError:
			raise ValuError, 'malformed PMID %s' % id_elem.text
			
		return pmid
	
	def parse_links(self, elem):
		''' Parses a I{LinkSetDb} element.
		
		    @param elem: an I{ElementTree.Element} instance.
		    @return: an set of PMIDs.
		    @raise ValueError: in case of type conversion errors.
		'''
		links = set()
		
		for link in elem.findall('Link'):
			id_elem = link.find('Id')
			try:
				pmid = int(id_elem.text)
			except ValueError:
				raise ValueError, 'malformed PMID %s' % id_elem.text
			
			links.add(pmid)
		
		return links

class Article(object):
	''' This class represents an article stored in the PubMed database. '''
	
	def __init__(self, pmid, attribs, references, related):
		''' Class constructor. Do not call directly.
		
		    @param pmid: the PMID of this article.
		    @param attribs: a dictionary holding article attributes.
		    @param references: a set of PMIDs of referenced articles.
		    @param related: a set of PMIDs of related araticles.
		'''
		assert 'pmid' not in attribs
		assert 'references' not in attribs
		assert 'related', not in attribs
		
		self.pmid = pmid
		self.__attribs = attribs
		self.references = references
		self.related = related
			
	def __getattr__(self, name):
		''' Attribute accessor. Used to expose article details. '''
		try:
			return self.__attribs[name]
		except KeyError:
			raise AttributeError
	
	def summary_dict_unsafe(self):
		''' Return an unsafe reference to the dictionary of attributes of this
		    article.
		    
		    @return: a dictionary of attributes.
		'''
		return self.__attribs

class BlockSplitter(object):
	''' This class takes the elements of an iterator and yields them in blocks
	    of fixed size.
	'''
	def __init__(self, iterator, block_size):
		''' Class constructor.
		
		    @param iterator: an iterator.
		    @param block_size: the maximum block size.
		'''
		assert block_size > 0
		
		self.iterator = iterator
		self.block_size = block_size
	
	def __iter__(self):
		return self.__split()
	
	def __split(self):
		aux = []
		for item in self.iterator:
			if len(aux) >= self.block_size:
				yield aux
				aux = []
			aux.append(item)
		
		if len(aux):
			yield aux
