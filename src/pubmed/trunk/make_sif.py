from optparse import OptionParser
from sys import exit, stderr
from vfork.sql.databaseparams import collectDatabaseParams
import MySQLdb

if __name__ == '__main__':
	parser = OptionParser(usage='%prog SIF_FILE')
	parser.add_option('-d', '--database', dest='db', help='database name')
	parser.add_option('-s', '--host', dest='host', help='hostname of the database server')
	parser.add_option('-p', '--password', dest='passwd', help='password of the database account')
	parser.add_option('-u', '--user', dest='user', help='name of the database account')
	parser.add_option('-f', '--full', action='store_true', dest='full', help='dump also the PMIDs of article whose summary is missing')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	try:
		db_params = collectDatabaseParams(options)
	except KeyError, e:
		print >>stderr, 'Database error:', e.args[0]
		exit(1)
	
	db_connection = MySQLdb.connect(**db_params)
	db_cursor = db_connection.cursor()
	
	if options.full:
		query = 'SELECT from_pmid, to_pmid FROM article_link ORDER BY from_pmid'
	else:
		query = '''
			SELECT from_pmid, to_pmid FROM article_link
				WHERE from_pmid IN (SELECT pmid FROM article_minimal)
				ORDER BY from_pmid
		'''
	db_cursor.execute(query)
	
	fd = file(args[0], 'w')
	while True:
		row = db_cursor.fetchone()
		if row is None:
			break
		else:
			print >>fd, '%d\tref\t%d' % row

	fd.close()	
	db_cursor.close()
	db_connection.close()
