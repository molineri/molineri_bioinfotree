DROP TABLE IF EXISTS article_link, article_minimal;

CREATE TABLE article_minimal (
	pmid INT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE PRIMARY KEY,
	pubdate DATE NOT NULL,
	info TEXT NOT NULL
);

CREATE TABLE article_link (
	from_pmid INT UNSIGNED NOT NULL PRIMARY KEY,
	to_pmid INT UNSIGNED NOT NULL,
	is_reference BOOLEAN NOT NULL
);
