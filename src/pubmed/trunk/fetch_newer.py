from optparse import OptionParser
from pubmed import LocalVault, RemoteVault, BlockSplitter
from sys import exit, stderr
from vfork.sql.databaseparams import collectDatabaseParams
import MySQLdb

#DEBUG
from time import time

if __name__ == '__main__':
	parser = OptionParser(usage='%prog PMID_START')
	parser.add_option('-d', '--database', dest='db', help='database name')
	parser.add_option('-s', '--host', dest='host', help='hostname of the database server')
	parser.add_option('-p', '--password', dest='passwd', help='password of the database account')
	parser.add_option('-u', '--user', dest='user', help='name of the database account')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
		
	try:
		start_pmid = int(args[0])
	except ValueError:
		exit('Invalid starting PMID.')
	
	try:
		db_params = collectDatabaseParams(options)
	except KeyError, e:
		print >>stderr, 'Database error:', e.args[0]
		exit(1)
	
	db_connection = MySQLdb.connect(**db_params)
	lv = LocalVault(db_connection)
	rv = RemoteVault()
	
	try:
		step = 300
		start_pmid -= step
		latest_pmid = None
		
		while True:
			start_pmid += step
			missing = lv.missing(set(range(start_pmid, start_pmid+step)))
			if len(missing) == 0:
				continue
			
			print '%d articles scheduled for download.' % len(missing)
			
			found = False
			for article in rv.fetch(missing, block_size=step):
				latest_pmid = article.pmid
				found = True
				lv.store(article)
			
			if not found:
				if latest_pmid is None:
					print 'Stopping iteration: no newer articles.'
				else:
					print 'Stopping iteration at PMID %d.' % latest_pmid
				break
	
	except KeyError, e:
		print >>stderr, 'PubMed error:', e.args[0]
		exit(1)

#16446784