from optparse import OptionParser
from pubmed import LocalVault, RemoteVault
from sys import exit, stderr
from vfork.sql.databaseparams import collectDatabaseParams
import MySQLdb

if __name__ == '__main__':
	parser = OptionParser(usage='%prog PMID')
	parser.add_option('-d', '--database', dest='db', help='database name')
	parser.add_option('-s', '--host', dest='host', help='hostname of the database server')
	parser.add_option('-p', '--password', dest='passwd', help='password of the database account')
	parser.add_option('-u', '--user', dest='user', help='name of the database account')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	try:
		pmid = int(args[0])
	except ValueError:
		exit('Invalid PMID.')
	
	try:
		db_params = collectDatabaseParams(options)
	except KeyError, e:
		print >>stderr, 'Database error:', e.args[0]
		exit(1)
	
	db_connection = MySQLdb.connect(**db_params)
	lv = LocalVault(db_connection)

	missing = lv.missing(set([pmid]))	
	if len(missing) == 0:
		print 'Article already stored.'
		exit(0)
	
	rv = RemoteVault()
	try:
		articles = rv.fetch([pmid])
	except KeyError, e:
		print >>stderr, 'PubMed error:', e.args[0]
		exit(1)
	
	lv.store(articles[0])
