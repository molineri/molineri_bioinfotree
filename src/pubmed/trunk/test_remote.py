from pubmed import RemoteVault

print '=== TEST #1 ==='
rv = RemoteVault()
articles = rv.fetch([16557293, 16557294])
for article in articles:
	print article.title, article.links

print '\n=== TEST #2 ==='
rv = RemoteVault()
pmids, total_count = rv.search_pmids('plos computational biology', 10, 5)
print total_count, pmids

print '\n=== TEST #3 ==='
rv = RemoteVault()
articles, total_count = rv.search('plos computational biology', 10, 5)
for article in articles:
	print article.title, article.links