from optparse import OptionParser
from sys import exit
from vfork.sql.databaseparams import collectDatabaseParams
import MySQLdb

''' This script loads PubMed articles stored in a local vault having PMIDs 
    falling in a given range.
    It then looks for their internal links and produces a DOT file representing
    the resulting network.
'''

if __name__ == '__main__':
	parser = OptionParser(usage='%prog DOT_FILE MIN_PMID MAX_PMID')
	parser.add_option('-d', '--database', dest='db', help='database name')
	parser.add_option('-s', '--host', dest='host', help='hostname of the database server')
	parser.add_option('-p', '--password', dest='passwd', help='password of the database account')
	parser.add_option('-u', '--user', dest='user', help='name of the database account')
	options, args = parser.parse_args()
	
	if len(args) != 3:
		exit('Unexpected argument number.')
	
	try:
		min_pmid = int(args[1])
	except ValueError:
		exit('Invalid value for minimum PMID.')
	
	try:
		max_pmid = int(args[2])
	except ValueError:
		exit('Invalid value from maximum PMID.')
	
	try:
		db_params = collectDatabaseParams(options)
	except KeyError, e:
		exit('Database error: ' + e.args[0])
	
	dot_fd = file(args[0], 'w')
	db_connection = MySQLdb.connect(**db_params)
	db_cursor = db_connection.cursor()
	
	print >>dot_fd, 'graph G {'
	db_cursor.execute('SELECT * FROM article_link WHERE from_pmid BETWEEN %s AND %s AND to_pmid BETWEEN %s AND %s AND from_pmid != to_pmid', (min_pmid, max_pmid, min_pmid, max_pmid))
	while True:
		row = db_cursor.fetchone()
		if row is None:
			break
		else:
			print >>dot_fd, '\t%d -- %d;' % row
	print >>dot_fd, '}'
	
	db_cursor.close()
	dot_fd.close()
