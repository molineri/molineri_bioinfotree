from optparse import OptionParser
from pubmed import LocalVault, RemoteVault, BlockSplitter
from sys import exit, stderr
from vfork.sql.databaseparams import collectDatabaseParams
import MySQLdb

#DEBUG
from time import time

if __name__ == '__main__':
	parser = OptionParser(usage='%prog')
	parser.add_option('-d', '--database', dest='db', help='database name')
	parser.add_option('-s', '--host', dest='host', help='hostname of the database server')
	parser.add_option('-p', '--password', dest='passwd', help='password of the database account')
	parser.add_option('-u', '--user', dest='user', help='name of the database account')
	parser.add_option('-n', '--dry-run', action='store_true', dest='dry_run', help='show the actions that would have been taken')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	try:
		db_params = collectDatabaseParams(options)
	except KeyError, e:
		print >>stderr, 'Database error:', e.args[0]
		exit(1)
	
	db_connection = MySQLdb.connect(**db_params)
	lv = LocalVault(db_connection)
	
	missing_count = lv.count_missing_links()
	print '%d articles scheduled for download' % missing_count
	if options.dry_run or missing_count == 0:
		exit(0)
		
	rv = RemoteVault()
	try:
		done = 0
		
		#DEBUG
		start = time()
		
		for missing in BlockSplitter(lv.iter_missing_links(), 100000):
			#DEBUG
			print 'iter_missing_links iteration: %.1f' % (time() - start)
		
			for article in rv.fetch(missing):
				lv.store(article)
			
			done += len(block)
			print 'Done', done, 'out of', missing_count
			
			#DEBUG
			start = time()
	
	except KeyError, e:
		print >>stderr, 'PubMed error:', e.args[0]
		exit(1)
