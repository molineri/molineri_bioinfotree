#!/bin/bash

# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

KERNEL_DIR="/usr/src/"
KERNEL_SERIES="2.6-rhel5"
LUSTRE_DIR="$HOME/packages/lustre/"

set -o pipefail
no_color=0
skip_extraction=0
build_dir=""

function fatal_error {
	echo "$1" >&2
	exit 1
}

function fatal_error_with_log {
	echo "$1" >&2
	tail -n30 "$2" >&2
	exit 1
}

function display_phase {
	printf "% -30s " "$1"
}

function display_success {
	if [[ $no_color -ne 0 ]]; then
		echo "OK"
	else
		echo "$(tput setaf 2)$(tput bold)OK$(tput sgr0)"
	fi
}

function display_failure {
	if [[ $no_color -ne 0 ]]; then
		echo "FAILED"
	else
		echo "$(tput setaf 1)$(tput bold)FAILED$(tput sgr0)"
	fi
}

function rel2abs {
	if [[ "${1:0:1}" == "/" ]]; then
		echo "$1"
	else
		echo "$PWD/$1"
	fi
}

function get_tar_option {
	[[ "$1" =~ .*\.tar\.(gz|bz2) ]] || return 1
	if [[ "${BASH_REMATCH[1]}" == "gz" ]]; then
		echo "z"
	else
		echo "j"
	fi
}

function get_first_archive_item {
	# run in a separate shell to avoid pipefail
	bash -c "tar t$2f "$1" | head -n1"
}

function clean_log_dir {
	if [[ -d "$log_dir" && "$(dirname "$log_dir")" == "/tmp" ]]; then
		rm -rf "$log_dir"
	else
		echo "Did not find log_dir $log_dir" >&2
	fi
}

function get_local_version {
	[[ "$1" =~ .*-([0-9]+\.[0-9]+\.[0-9]+) ]] || return 1
	echo "-rhel5-${BASH_REMATCH[1]}-${2%/}"
}

function get_make_opts {
	make_opts="$(grep MAKEOPTS /etc/make.conf 2>/dev/null)"
	if [[ "$make_opts" =~ MAKEOPTS=\"?([^\"]*)\"? ]]; then
		echo "${BASH_REMATCH[1]}"
	fi
}

if [[ $# -lt 1 ]]; then
	fatal_error "Unexpected argument number."
elif [[ "$1" == "-h" || "$1" == "--help" ]]; then
	fatal_error "Usage: $0 [-s|--skip-extraction] KERNEL_ARCHIVE LUSTRE_ARCHIVE" >&2
elif [[ "$1" == "-s" || "$1" == "--skip-extraction" ]]; then
	skip_extraction=1
	shift
fi

if [[ $# -ne 2 ]]; then
	fatal_error "Unexpected argument number."
elif [[ ! -f "$1" ]]; then
	fatal_error "Invalid kernel archive."
elif [[ ! -f "$2" ]]; then
	fatal_error "Invalid Lustre archive."
elif [[ ! -d "$KERNEL_DIR" ]]; then
	fatal_error "Missing kernel directory $KERNEL_DIR"
elif [[ ! -d "$LUSTRE_DIR" ]]; then
	fatal_error "Missing Lustre directory $LUSTRE_DIR"
elif [[ ! -f "/proc/config.gz" ]]; then
	fatal_error "Missing /proc/config.gz"
elif [[ "$(whoami)" != "root" ]]; then
	fatal_error "You need to be root to run this script."
fi

# make absolute paths
kernel_archive="$(rel2abs "$1")"
lustre_archive="$(rel2abs "$2")"

# determine kernel version
display_phase "Determining kernel version"

if ! cd "$KERNEL_DIR"; then
	display_failure
	fatal_error "Error accessing directory $KERNEL_DIR"
fi

tar_opt="$(get_tar_option "$kernel_archive")"
if [[ $? -ne 0 ]]; then
	display_failure
	fatal_error "Unsupported kernel archive format."
fi

kernel_source_dir=$(get_first_archive_item "$kernel_archive" "$tar_opt")
if [[ $? -ne 0 ]]; then
	echo "$kernel_source_dir"
	display_failure
	fatal_error "Error processing kernel archive."
fi

display_success

# extract kernel sources
if [[ $skip_extraction -ne 1 ]]; then
	display_phase "Extracting kernel sources"
	
	if [[ -d "$kernel_source_dir" ]]; then
		display_failure
		fatal_error "Another copy of the kernel has already been extracted."
	elif ! tar x${tar_opt}f "$kernel_archive" >&/dev/null; then
		display_failure
		fatal_error "Error extracting kernel sources."
	fi
	display_success
fi

# determine Lustre version
display_phase "Determining Lustre version"

if ! cd "$LUSTRE_DIR"; then
	display_failure
	fatal_error "Error accessing directory $LUSTRE_DIR"
fi

tar_opt="$(get_tar_option "$lustre_archive")"
if [[ $? -ne 0 ]]; then
	display_failure
	fatal_error "Unsupported Lustre archive format."
fi

lustre_source_dir="$(get_first_archive_item "$lustre_archive" "$tar_opt")"
if [[ $? -ne 0 ]]; then
	display_failure
	fatal_error "Error processing Lustre archive."
fi

display_success

# extract Lustre sources
if [[ $skip_extraction -ne 1 ]]; then
	display_phase "Extracting Lustre sources"
	
	if [[ -d "$lustre_source_dir" ]]; then
		display_failure
		fatal_error "Another copy of Lustre sources has already been extracted."
	elif ! tar x${tar_opt}f "$lustre_archive" >&/dev/null; then
		display_failure
		fatal_error "Error extracting Lustre sources."
	fi
	
	display_success
fi

# create log directory
display_phase "Creating log directory"
log_dir="$(mktemp -d)"
if [[ $? -ne 0 ]]; then
	display_failure
	fatal_error "Error creating log directory."
fi
trap clean_log_dir 0
display_success

# prepare kernel sources
if [[ $skip_extraction -ne 1 ]]; then
	display_phase "Preparing kernel sources"
	if ! cd "$KERNEL_DIR/$kernel_source_dir"; then
		display_failure
		fatal_error "Error accessing directory $KERNEL_DIR/$kernel_source_dir"
	elif ! make mrproper >&/dev/null; then
		display_failure
		fatal_error "Error running mrproper."
	elif ! sed --in-place 's|\(EXTRAVERSION\s*=\).*|\1|' Makefile >&/dev/null; then
		display_failure
		fatal_error "Error updating EXTRAVERSION."
	elif ! ln -s "$LUSTRE_DIR/$lustre_source_dir/lustre/kernel_patches/patches" . >&/dev/null || \
	     ! ln -s "$LUSTRE_DIR/$lustre_source_dir/lustre/kernel_patches/series/${KERNEL_SERIES}.series" series >&/dev/null; then
		display_failure
		fatal_error "Error linking Lustre patches."
	elif ! quilt -av push >&"$log_dir/quilt.log"; then
		display_failure
		fatal_error_with_log "Error patching kernel sources:" "$log_dir/quilt.log"
	fi
	
	if ! zcat /proc/config.gz >.config 2>/dev/null; then
		display_failure
		fatal_error "Error preparing kernel config."
	fi
	display_success
fi

# set kernel local version
display_phase "Setting kernel local version"

local_version="$(get_local_version "$kernel_source_dir" "$lustre_source_dir")"
if ! sed --in-place 's|\(CONFIG_LOCALVERSION=\).*|\1"'"${local_version}"'"|' "$KERNEL_DIR/$kernel_source_dir/.config" 2>/dev/null; then
	display_failure
	fatal_error "Error setting kernel local version."
fi
display_success

# configure kernel
echo
echo "I will now run interactively 'make oldconfig' on kernel sources."
echo "If you want to abort the configuration hit CTRL+C at any time."
echo "Hit ENTER when ready."
read

cd "$KERNEL_DIR/$kernel_source_dir" || exit 1
make oldconfig || exit 1
display_phase "Kernel configuration"
display_success

# compile kernel
display_phase "Compiling kernel"
if ! genkernel --kerneldir="$KERNEL_DIR/$kernel_source_dir" \
               --no-clean all >&"$log_dir/genkernel.log"; then
	display_failure
	fatal_error_with_log "Error compiling kernel:" "$log_dir/genkernel.log"
fi
display_success

# compile Lustre
display_phase "Compiling Lustre"
if ! cd "$LUSTRE_DIR/$lustre_source_dir" >&/dev/null; then
	display_failure
	fatal_error "Error accessing directory $LUSTRE_DIR/$lustre_source_dir"
elif ! ./configure --prefix="/usr/local/stow/$lustre_source_dir" \
	               --with-linux="$KERNEL_DIR/$kernel_source_dir" >&"$log_dir/configure.log"; then
	display_failure
	fatal_error_with_log "Error configuring Lustre:" "$log_dir/configure.log"
elif ! make $(get_make_opts) >&"$log_dir/build.log"; then
	display_failure
	fatal_error_with_log "Error building Lustre:" "$log_dir/build.log"
elif ! make install >&"$log_dir/install.log"; then
	display_failure
	fatal_error_with_log "Error installing Lustre:" "$log_dir/install.log"
fi
display_success

# final instructions
echo
echo "The new system is almost ready."
echo "You still need to perform a few tasks:"
echo "* update GRUB configuration to boot the new kernel;"
echo "* link /usr/src/linux to $KERNEL_DIR/$kernel_source_dir;"
echo "* use stow to activate the new user-land utilities stored in /usr/local/stow/$lustre_source_dir;"
echo "* reboot."