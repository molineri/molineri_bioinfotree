#!/usr/bin/env python
from cStringIO import StringIO
from optparse import OptionParser
from os import popen
from os.path import basename
from smtplib import SMTP, SMTPException
from sys import argv, exit, stdin, stdout, stderr

def consume_input(in_fd, out_fd):
	log = StringIO()

	while True:
		chunk = in_fd.read()
		if len(chunk) == 0:
			break
		else:
			out_fd.write(chunk)
			log.write(chunk)
	
	return log.getvalue()

def format_mail(address, subject, msg):
	content = StringIO()
	content.write('From: %s\r\n' % address[0])
	content.write('To: %s\r\n' % ','.join(address))
	content.write('Subject: %s\r\n\r\n' % subject)
	content.write(msg)
	return content.getvalue()

def main():
	parser = OptionParser(usage='%prog SERVER EMAIL_ADDRS CMD [ARGS...]\n\nMultiple email addresses are separated by \',\'.')
	parser.add_option('-p', '--port', dest='port', type='int', default=25, help='the SMTP host PORT', metavar='PORT')
	options, args = parser.parse_args()

	if len(args) < 3:
		exit('Unexpected argument number.')
	
	my_name = basename(argv[0])
	addrs = args[1].split(',')

	cmd_fd = popen(' '.join(args[2:]) + ' 2>&1', 'r')
	log = consume_input(cmd_fd, stdout)
	if cmd_fd.close() is not None:
		msg = '[%s] error running %s' % (my_name, args[2])
		print >>stderr, msg
		log += '\n%s' % msg
	
	if len(log.strip()) == 0:
		exit(0)

	try:
		smtp = SMTP(args[0], options.port)
		smtp.sendmail(addrs[0], addrs, format_mail(addrs, '[%s] %s' % (my_name, args[2]), log))
		smtp.quit()
	except SMTPException:
		exit('[%s] cannot sent email message.' % my_name)

if __name__ == '__main__':
	main()

