#!/usr/bin/env python
from cStringIO import StringIO
from optparse import OptionParser
from os.path import basename
from select import select
from smtplib import SMTP, SMTPException
from socket import gethostname
from subprocess import Popen, PIPE
from sys import argv, exit, stdout, stderr
import errno
import fcntl
import os

def log_io(cmd):
	log = StringIO()

	set_nonblock(cmd.stdout)
	set_nonblock(cmd.stderr)
	open_fds = [ cmd.stdout, cmd.stderr ]
	while True:
		if len(open_fds) == 0:
			break

		fds, i1, i2 = select(open_fds, [], [])
		if cmd.stdout in fds:
			data = relay(cmd.stdout, stdout)
			if len(data) == 0:
				open_fds.remove(cmd.stdout)
			else:
				log.write(data)

		if cmd.stderr in fds:
			data = relay(cmd.stderr, stderr)
			if len(data) == 0:
				open_fds.remove(cmd.stderr)
			else:
				log.write(data)

	return log.getvalue()

def set_nonblock(fd):
	fcntl.fcntl(fd, fcntl.F_SETFL, os.O_NONBLOCK)

def relay(from_fd, to_fd):
	read_data = []
	while True:
		try:
			data = from_fd.read(4096)
		except IOError, e:
			if e.errno == errno.EAGAIN:
				break
			else:
				raise e

		if len(data) == 0:
			break
		else:
			to_fd.write(data)
			read_data.append(data)

	return ''.join(read_data)

def format_mail(address, subject, msg):
	content = StringIO()
	content.write('From: %s\r\n' % address[0])
	content.write('To: %s\r\n' % ','.join(address))
	content.write('Subject: %s\r\n\r\n' % subject)
	content.write(msg)
	return content.getvalue()

def main():
	parser = OptionParser(usage='%prog SERVER EMAIL_ADDRS CMD [ARGS...]\n\nMultiple email addresses are separated by \',\'.')
	parser.add_option('-p', '--port', dest='port', type='int', default=25, help='the SMTP host PORT', metavar='PORT')
	options, args = parser.parse_args()

	if len(args) < 3:
		exit('Unexpected argument number.')
	
	my_name = basename(argv[0])
	addrs = args[1].split(',')

	cmd = Popen(' '.join(args[2:]), shell=True, stdout=PIPE, stderr=PIPE)
	log = log_io(cmd)
	if cmd.wait() != 0:
		msg = '[%s] error running %s' % (my_name, args[2])
		print >>stderr, msg
		log += '\n%s' % msg
	
	if len(log.strip()) == 0:
		exit(0)

	try:
		smtp = SMTP(args[0], options.port)
		smtp.sendmail(addrs[0], addrs, format_mail(addrs, '[%s on %s] %s' % (my_name, gethostname(), args[2]), log))
		smtp.quit()
	except SMTPException:
		exit('[%s] cannot send email message.' % my_name)

if __name__ == '__main__':
	main()

