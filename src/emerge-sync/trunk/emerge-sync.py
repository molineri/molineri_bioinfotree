#!/bin/bash

EMERGE="/usr/bin/emerge"
SED="/bin/sed"

if [[ ! -x $EMERGE || ! -x $SED ]]; then
	echo "Missing external utilities." >&2
	exit 1
fi

$EMERGE -q --color n --sync 2>&1 | $SED '/^$/d;/^q:/d'
