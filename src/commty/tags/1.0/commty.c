#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#define MAXSTR 100

int lab(char *node);
void nline(void);
void jcomm(int j1, int j2, int nj);
void wlinks(void);
void wcomms(void);
long int cidq(int j1, int j2, int nj);

int nodes = 0, links = 0;
char **name = NULL;
int *ve1 = NULL;
int *ve2 = NULL;
int *wei = NULL;
int *comm = NULL;
int comms = 0;
int extls = 0;
int *a = NULL;
int *size = NULL;

int main(int argc, char *argv[]) 
{
  int test, n1, n2, i, j, j1, j2, join, *bcomm, *bsize;
  long int idq, bidq, q, bq;
  int bcomms, first;
  char node1[MAXSTR], node2[MAXSTR];
  FILE *fsize;
  
  /* read network */
  
  while ( (test = scanf("%s %s", node1, node2)) != EOF) {
    n1 = lab(node1);
    n2 = lab(node2);
    ++links;
    ve1 = (int *) realloc(ve1, links * sizeof(int));
    ve2 = (int *) realloc(ve2, links * sizeof(int));
    wei = (int *) realloc(wei, links * sizeof(int));
    ve1[links - 1] = n1;
    ve2[links - 1] = n2;
    wei[links - 1] = 1;
    ++a[n1];
    ++a[n2];
    nline();
    
    //if (links%10000 == 0)
     // printf("link caricati: %d\n", links);
  }
  

  /* initialize */
  
  q = 0;
  for (i = 0; i < nodes; ++i)
    q -= a[i] * a[i];
  
  double norm = 4.0 * (double) links * (double) links;
  
  bq = q;
  comm = (int *) malloc(nodes * sizeof(int));
  bcomm = (int *) malloc(nodes * sizeof(int));
  size = (int *) malloc(nodes * sizeof(int));
  bsize = (int *) malloc(nodes * sizeof(int));
  for (i = 0; i < nodes; ++i) {
    comm[i] = i;
    size[i] = 1;
    bcomm[i] = i;
    bsize[i] = 1;
  }
  comms = nodes;
  bcomms = nodes;
  extls = links;
  
  /* main loop */
  
  while (comms > 0 && extls > 0) {
    /*         wcomms(); */
    /*         wlinks(); */
    
    first = 1;
    for (i = 0; i < links; ++i) {
      if (wei[i] == 0) continue;
      idq = cidq(ve1[i], ve2[i], wei[i]);
      if (idq > bidq || first) {
	bidq = idq;
	j1 = ve1[i];
	j2 = ve2[i];
	join = wei[i];
      }
      first = 0;
    }
    jcomm(j1, j2, join);
    q += bidq;
    if (q > bq) {
      bq = q;
      bcomms=comms;
      for (i = 0; i < nodes; ++i) {
	bcomm[i] = comm[i];
	bsize[i] = size[i];
      }
    }
  }
  
  /* output */
  
  printf("#Modularity:\t%f\n", (double) bq / (double) norm);
  fsize = fopen("commun_size.txt", "w");
  int ci = -1;
  for (i = 0; i < nodes; ++i) {
    if (!bsize[i]) continue;
    ++ci;
    fprintf(fsize,"%d\t%d\n", ci, bsize[i]);
    for (j = 0; j < nodes; ++j)
      if (bcomm[j] == i) printf("%s\t%d\n", name[j], ci);
  }
  fclose(fsize);
  
  return 0;	
}

int lab(char *node)
{
    int i;

    for (i = 0; i < nodes; ++i)
        if (strcmp(name[i], node) == 0) return i;

    ++nodes;
    name = (char **) realloc(name, nodes * sizeof(char *));
    name[nodes - 1] = (char *) malloc((strlen(node) + 1) * sizeof(char));
    strcpy(name[nodes - 1], node);
    a = (int *) realloc(a, nodes * sizeof(int));
    a[nodes - 1] = 0;
/*   fprintf(stderr, "%d %s\n", nodes-1, name[nodes-1]); */
    return nodes - 1;
}

void nline(void)
{
    char c;

    while ( (c = getc(stdin)) != '\n');
}

long int cidq(int j1, int j2, int nj)
{
    return (long int) (4 * links * nj - 2 * (a[j1] * a[j2]));
}

void jcomm(int j1, int j2, int nj)
{
    int i, n1, n2, w;
    int *ref = NULL;

    for (i = 0; i < nodes; ++i)
        if (comm[i] == j2) comm[i] = j1;

    size[j1] += size[j2];
    size[j2] = 0;

    ref = (int *) malloc(nodes * sizeof(int));

    for (i = 0; i < nodes; ++i)	ref[i] = -1;
    for (i = 0; i < links; ++i) {

        if (wei[i] == 0) continue;
        n1 = ve1[i];
        n2 = ve2[i];
        w = wei[i];

        if ((n1 != j1) && (n1 != j2) && (n2 != j1) && (n2 != j2)) continue;

        if (((n1 == j1) && (n2 == j2)) || ((n1 == j2) && (n2 == j1))) {
            wei[i] = 0;
            continue;
        }

        if ((n1 == j1) && (n2 != j2)) {
            if (ref[n2] != -1) {
                wei[ref[n2]] += w;
                wei[i] = 0;
            } else {
                ref[n2] = i;
            }
            continue;
        }

        if ((n1 != j1) && (n2 == j2)) {
            if (ref[n1] != -1) {
                wei[ref[n1]] += w;
                wei[i] = 0;
            } else {
                ref[n1] = i;
                ve2[i] = j1;
            }
            continue;
        }

        if ((n1 == j2) && (n2 != j1)) {
            if (ref[n2] != -1) {
                wei[ref[n2]] += w;
                wei[i] = 0;
            } else {
                ref[n2] = i;
                ve1[i] = j1;
            }
            continue;
        }

        if ((n1 != j2) && (n2 == j1)) {
            if (ref[n1] != -1) {
                wei[ref[n1]] += w;
                wei[i] = 0;
            } else {
                ref[n1] = i;
            }
            continue;
        }
    }
    --comms;
    a[j1] += a[j2];
    extls -= nj;
    
    free(ref);
}

void wlinks(void)
{
    int i;

    for (i = 0; i < links; ++i) {
        printf("%d\t%d\t%d\n", ve1[i], ve2[i], wei[i]);
    }
    printf("\n");
}

void wcomms(void)
{
    int i;

    printf("%d comms\n", comms);
    printf("assign\n");
    for (i = 0; i < nodes; ++i) {
        printf("%d %d\n", i, comm[i]);
    }

}

