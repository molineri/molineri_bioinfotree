#!/usr/bin/perl

use warnings;
use strict;
use FileHandle;
use IPC::Open2;
use Getopt::Long;

$,="\t";
$\="\n";

my $usage = "$0 -s SEQ_DIR -Q gap_opening_penalty -R gap_extension_penalty score_matrix_file < qualcosa.pdb";

my $seq_dir = undef;
my $gap_opening_score   = undef;
my $gap_extension_score = undef;
GetOptions(
	'SEQ_DIR|s=s'    => \$seq_dir,
	'gaps_o|Q=f'     => \$gap_opening_score,
	'gaps_e|R=f'     => \$gap_extension_score
) or die $usage;
die $usage if (!defined $seq_dir or !defined $gap_opening_score or !defined $gap_extension_score);

my $score_matrix_file = shift @ARGV;
die $usage if !defined $score_matrix_file;

open SCORE_MTX,$score_matrix_file or die "Can't open file $score_matrix_file";

my %matrix = ();
my $intest = <SCORE_MTX>;
chomp $intest;
$intest =~ s/^\s+//;
my @letters = split /\s+/,$intest;
while (<SCORE_MTX>) {
	my @line = split /\s+/;
	my $header = shift @line;
	die "ERROR in $score_matrix_file: non square matrix" if (scalar @line != scalar @letters);
	for (my $k=0; $k<scalar @line; $k++) {
		$matrix{$header}{$letters[$k]} = $line[$k];
	}
}


my $pid = open2(*Reader, *Writer, "get_sequence_pipe $seq_dir") or die ("Can't open pipe to get_sequence_pipe");

my $q_seq;
my $t_seq;
while (<>) {
	chomp;
	my @F = split /\t/;
	#10      82867   82966   -       13      22599430        22599531        102     10153   87      1.7e-12 75,3    54,1
	print Writer $F[0],$F[1],$F[2];
	$q_seq = <Reader>;
	#my $q_seq = <Reader>;
	chomp $q_seq;
	print Writer $F[4],$F[5],$F[6];
	$t_seq = <Reader>;
	#my $t_seq = <Reader>;
	chomp $t_seq;
	print STDERR ">".$F[0],$F[1],$F[2],$F[4],$F[5],$F[6];
	print STDERR ">".$q_seq, $t_seq;
	$t_seq = &reverse_compl($t_seq) if ($F[3] eq '-');
	$q_seq = &insert_gaps($q_seq,$F[7],$F[11]) if ( (defined $F[11]) and ($F[11] =~ /^\d+,\d+/) );
	$t_seq = &insert_gaps($t_seq,$F[7],$F[12]) if ( (defined $F[12]) and ($F[12] =~ /^\d+,\d+/) );
	die "$0: dopo aver inserito i gaps, le 2 sequenze hanno lunghezze diverse" if (length $q_seq != length $t_seq);
	print STDERR $q_seq, $t_seq;  # stampa le sequenze con inseriti i gaps
	#die if ($q_seq =~ /N/ or $t_seq =~ /N/);
	my $align_score = &calculate_score($q_seq,$t_seq);
	$F[8] = $align_score;
	print @F;
}


sub reverse_compl
{
	my $seq = shift;
	$seq = reverse $seq;
	$seq =~ tr/ACGT/TGCA/;
	return $seq;
}

sub insert_gaps
{
	my $seq = shift;
	my $align_len = shift;
	my $gaps_list = shift;
	
	$gaps_list =~ tr/,;/\t/;
	my @list = split /\t/,$gaps_list;
	my $offset = 0;
	for (my $i=0; $i<scalar @list - 1; $i+=2) {
		my $before = substr $seq,0,$list[$i] + $offset,'';
		for (my $j=0; $j<$list[$i + 1]; $j++) {
			$before .= '-';
			$offset++;
		}
		$seq = $before.$seq;
	}
	die "$0: insert_gaps: error in align_length" if (length $seq != $align_len);

	return $seq;
}


sub calculate_score
{
	my $seq1 = shift;
	my $seq2 = shift;

	my $score = 0;
	my $extend_gap = 0;
	while (length $seq1 > 0) {
		my $letter1 = substr $seq1,0,1,'';
		my $letter2 = substr $seq2,0,1,'';
		die "ciao" if (!defined $letter1 or !defined $letter2);
		#die 'N' if ($letter1 eq 'N' or $letter2 eq 'N');
		die "ERROR in alignment: gap aligned with gap" if ( ($letter1 eq '-') and ($letter2 eq '-') );
		die "ERROR in alignment: N aligned with N" if ( ($letter1 eq 'N') and ($letter2 eq 'N') );
		if ( ($letter1 eq '-') or ($letter2 eq '-') ) {
			if ($extend_gap) {
				$score -= $gap_extension_score;
			} else {
				$score -= $gap_opening_score;
				$extend_gap = 1;
			}
		} else {
			if ($letter1 eq 'N' or $letter2 eq 'N') {
				print STDERR "#$q_seq\t$t_seq";
				next;
			}
			$score += $matrix{$letter1}{$letter2};
			$extend_gap = 0;
		}
	}
	return $score;
}
