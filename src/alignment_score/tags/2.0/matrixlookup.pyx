cdef extern from "Python.h":
	char* PyString_AsString(object o)

cdef extern from "matrix.h":
	ctypedef struct matrix_t
	matrix_t* build_matrix(char* labels)
	void free_matrix(matrix_t* matrix)
	int matrix_get_entry(matrix_t* matrix, char x, char y)
	void matrix_set_entry(matrix_t* matrix, char x, char y, int value)

cdef class MatrixLookup:
	cdef matrix_t* matrix
	
	def __new__(self, substitution_matrix):
		cdef char* labels_ptr
		cdef int x_idx, y_idx
		
		labels = 'ACGTN'
		labels_ptr = PyString_AsString(labels)
		
		self.matrix = build_matrix(labels)
		for y_idx from 0 <= y_idx < 5:
			for x_idx from 0 <= x_idx < 5:
				matrix_set_entry(self.matrix, labels_ptr[x_idx], labels_ptr[y_idx], substitution_matrix[(labels[x_idx],labels[y_idx])])
	
	def __dealloc__(self):
		free_matrix(self.matrix)
	
	def get(self, char* x, char* y):
		return matrix_get_entry(self.matrix, x[0], y[0])
