from distutils.core import setup
from distutils.extension import Extension
from Pyrex.Distutils import build_ext

setup(
	name = "matrixlookup",
	ext_modules = [ 
	  Extension("matrixlookup", ["matrixlookup.pyx", "matrix.c"])
	],
	cmdclass = {'build_ext': build_ext}
)
