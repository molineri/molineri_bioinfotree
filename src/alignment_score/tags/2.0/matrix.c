#include "matrix.h"
#include <stdlib.h>
#include <string.h>

#include <stdio.h>

struct matrix
{
	unsigned char label_num;
	unsigned char label_indexes[256];
	int* entries;
};

matrix_t* build_matrix(const char* labels)
{
	matrix_t* matrix = calloc(1, sizeof(struct matrix));
	if (!matrix)
		return NULL;
	
	const char* label = labels;
	while (*label)
		matrix->label_indexes[(unsigned char)*label++] = matrix->label_num++;
	
	matrix->entries = calloc(matrix->label_num*matrix->label_num, sizeof(int));
	if (!matrix->entries)
	{
		free(matrix);
		return NULL;
	}
	
	return matrix;
}

void free_matrix(matrix_t* matrix)
{
	free(matrix);
}

int matrix_get_entry(const matrix_t* matrix, const char x, const char y)
{
	const unsigned char x_idx = matrix->label_indexes[(unsigned char)x];
	const unsigned char y_idx = matrix->label_indexes[(unsigned char)y];
	return matrix->entries[x_idx + matrix->label_num*y_idx];
}

void matrix_set_entry(matrix_t* matrix, const char x, const char y, const int value)
{
	const unsigned char x_idx = matrix->label_indexes[(unsigned char)x];
	const unsigned char y_idx = matrix->label_indexes[(unsigned char)y];
	matrix->entries[x_idx + matrix->label_num*y_idx] = value;
}
