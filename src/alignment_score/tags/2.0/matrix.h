#ifndef MATRIX_H
#define MATRIX_H

typedef struct matrix matrix_t;

matrix_t* build_matrix(const char* labels);
void free_matrix(matrix_t* matrix);
int matrix_get_entry(const matrix_t* matrix, const char x, const char y);
void matrix_set_entry(matrix_t* matrix, const char x, const char y, const int value);

#endif //MATRIX
