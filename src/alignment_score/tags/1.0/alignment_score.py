#!/usr/bin/env python
#encoding: utf8
from __future__ import division
from __future__ import with_statement

from array import array
from itertools import izip
from optparse import OptionParser
from os.path import isdir, join
from sys import exit, stdin
from vfork.alignment.scoring import SubstitutionMatrix
from vfork.fasta.reader import SingleBlockReader
from vfork.io.util import safe_rstrip
import re

def parse_gap(gap_repr):
	tokens = gap_repr.split(',')
	if len(tokens) != 2:
		raise ValueError
	else:
		return (int(tokens[0]), int(tokens[1]))

def parse_gaps(gaps_repr):
	if len(gaps_repr) > 0:
		return tuple(parse_gap(g) for g in gaps_repr.split(';'))
	else:
		return None

def parse_hsp(line, lineno):
	tokens = safe_rstrip(line).split('\t')
	if len(tokens) != 13:
		exit('Invalid HSP at line %d.' % lineno)
	
	for pos, label in [(1, 'query_start'), (2, 'query stop'), (5, 'target start'), (6, 'target stop')]:
		try:
			tokens[pos] = int(tokens[pos])
		except ValueError:
			exit('Invalid %s at line %d.' % (label, lineno))
	
	try:
		tokens[11] = parse_gaps(tokens[11])
	except ValueError, e:
		exit('Invalid query gaps at line %d.' % lineno)
	
	try:
		tokens[12] = parse_gaps(tokens[12])
	except ValueError:
		exit('Invalid target gaps at line %d.' % lineno)
	
	return tokens, lineno

def open_sequence_reader(dirname, sequence_name):
	filename = join(dirname, '%s.fa' % sequence_name)
	try:
		return SingleBlockReader(filename)
	except IOError:
		pass
	
	filename = join(dirname, 'chr%s.fa' % sequence_name)
	try:
		return SingleBlockReader(filename)
	except IOError:
		exit('Cannot find FASTA file for sequence %s' % sequence_name)

def build_translation_table():
	tbl = [ chr(i) for i in xrange(256) ]
	tbl[ord('A')] = 'T'
	tbl[ord('C')] = 'G'
	tbl[ord('G')] = 'C'
	tbl[ord('T')] = 'A'
	return ''.join(tbl)

translation_table = build_translation_table()
def reverse_complement(sequence):
	a = array('c')
	a.fromstring(sequence.translate(translation_table))
	a.reverse()
	return a.tostring()

def insert_gaps(sequence, gaps):
	chunks = []
	last_pos = 0
	gap_cum = 0
	for start, length in gaps:
		start -= gap_cum
		chunks.append(sequence[last_pos:start])
		chunks.append('-'*length)
		last_pos = start
		gap_cum += length
	
	chunks.append(sequence[last_pos:])
	return ''.join(chunks)

def format_gaps(gaps):
	if gaps is None:
		return ''
	else:
		return ';'.join('%s,%s' % (g[0], g[1]) for g in gaps)

def main():
	parser = OptionParser(usage='%prog QUERY_SEQUENCE_DIR TARGET_SEQUENCE_DIR SCORE_MATRIX Q R <PDB')
	options, args = parser.parse_args()
	
	if len(args) != 5:
		exit('Unexpected argument number.')
	
	if not isdir(args[0]):
		exit('Invalid query sequence directory.')
	
	if not isdir(args[1]):
		exit('Invalid target sequence directory.')
	
	try:
		score_matrix = SubstitutionMatrix(args[2])
	except IOError:
		exit('Error loading score matrix %s' % args[2])
	
	try:
		q = int(args[3])
		if q < 0:
			raise ValueError
	except ValueError:
		exit('Invalid Q value.')
	
	try:
		r = int(args[4])
		if r < 0:
			raise ValueError
	except ValueError:
		exit('Invalid R value.')
	
	reader1_chr = None
	reader2_chr = None
	
	for hsp, lineno in (parse_hsp(line, lineno+1) for lineno, line in enumerate(stdin)):
		if reader1_chr != hsp[0]:
			reader1 = open_sequence_reader(args[0], hsp[0])
			reader1_chr = hsp[0]
		if reader2_chr != hsp[4]:
			reader2 = open_sequence_reader(args[1], hsp[4])
			reader2_chr = hsp[4]
		
		query_sequence = reader1.get(hsp[1], hsp[2]-hsp[1])
		if hsp[3] == '-':
			query_sequence = reverse_complement(query_sequence)
		if hsp[11] is not None:
			query_sequence = insert_gaps(query_sequence, hsp[11])
		
		target_sequence = reader2.get(hsp[5], hsp[6]-hsp[5])
		if hsp[12] is not None:
			target_sequence = insert_gaps(target_sequence, hsp[12])
		
		if len(query_sequence) != len(target_sequence):
			exit('Found query and target sequences having different lengths at line %d.' % lineno)
		
		score = 0
		gap_open_on_query = False
		gap_open_on_target = False
		for x, y in izip(query_sequence, target_sequence):
			gap_x = x == '-'
			gap_y = y == '-'
			
			if gap_x and gap_y:
				exit('Found a gap both on the query and on the target at line %d.' % lineno)
			elif gap_x:
				if gap_open_on_query:
					score -= r
				else:
					gap_open_on_query = True
					score -= q + r
			elif gap_y:
				if gap_open_on_target:
					score -= r
				else:
					gap_open_on_target = True
					score -= q + r
			else:
				gap_open_on_query = False
				gap_open_on_target = False
				score += score_matrix[(x,y)]
		
		hsp[8] = score
		hsp[11] = format_gaps(hsp[11])
		hsp[12] = format_gaps(hsp[12])
		print '\t'.join(str(h) for h in hsp)
		
		# if score < 0:
		# 	for i in xrange(0, len(query_sequence), 80):
		# 		print query_sequence[i:i+80]
		# 		print target_sequence[i:i+80]
		# 		print

if __name__ == '__main__':
	#import cProfile
	#cProfile.run('main()', 'profiler.data')
	main()
