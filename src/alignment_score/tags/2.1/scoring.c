#include "scoring.h"
#include <stdlib.h>

#include <stdio.h>

struct scoring_system
{
	unsigned char label_num;
	char label_map[256];
	int* score_matrix;
	int r;
};

scoring_system_t* build_scoring_system(const char* labels)
{
	scoring_system_t* system = calloc(1, sizeof(scoring_system_t));
	if (!system)
		return NULL;
	
	int i;
	for (i = 0; i < 256; i++)
		system->label_map[i] = -1;
	
	while (*labels)
	{
		if (system->label_num == 127 || *labels < 0)
		{
			free(system);
			return NULL;
		}
		else
			system->label_map[*labels++] = system->label_num++;
	}
	
	system->score_matrix = calloc(system->label_num*system->label_num, sizeof(int));
	if (!system->score_matrix)
	{
		free(system);
		return NULL;
	}
	
	return system;
}

void free_scoring_system(scoring_system_t* system)
{
	free(system->score_matrix);
	free(system);
}

int set_pair_score(scoring_system_t* system, const char x, const char y, const int score)
{
	const char x_idx = system->label_map[x];
	const char y_idx = system->label_map[y];
	if (x_idx < 0 || y_idx < 0)
		return 1;
	else
	{
		system->score_matrix[x_idx + y_idx*system->label_num] = score;
		return 0;
	}
}

int set_r(scoring_system_t* system, const int value)
{
	if (value < 0)
		return 1;
	else
	{
		system->r = value;
		return 0;
	}
}

int compute_score(scoring_system_t* system, const char* sequence1, const char* sequence2, int* score)
{
	int s = 0;
	while (*sequence1 && *sequence2)
	{
		const int gap_on_seq1 = *sequence1 == '-';
		const int gap_on_seq2 = *sequence2 == '-';
		if (gap_on_seq1 && gap_on_seq2)
			return 1;
		else if (gap_on_seq1 || gap_on_seq2)
			s -= system->r;
		else
		{
			const char x_idx = system->label_map[*sequence1];
			const char y_idx = system->label_map[*sequence2];
			if (x_idx < 0 || y_idx < 0)
				return 1;
			else
				s += system->score_matrix[x_idx + y_idx*system->label_num];
		}
		
		sequence1++;
		sequence2++;
	}
	
	if (*sequence1 || *sequence2)
		return 1;
	
	*score = s;
	return 0;
}
