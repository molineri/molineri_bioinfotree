cdef extern from "Python.h":
	char* PyString_AsString(object o)

cdef extern from "scoring.h":
	ctypedef struct scoring_system_t
	scoring_system_t* build_scoring_system(char* labels)
	void free_scoring_system(scoring_system_t* system)
	int set_pair_score(scoring_system_t* system, char x, char y, int score)
	int set_r(scoring_system_t* system, int value)
	int compute_score(scoring_system_t* system, char* sequence1, char* sequence2, int* score)

cdef class ScoringSystem:
	cdef scoring_system_t* system
	
	def __new__(self, substitution_matrix, r):
		cdef char* labels_ptr
		cdef int label_num, x_idx, y_idx
		
		labels = 'ACGTN'
		label_num = len(labels)
		labels_ptr = PyString_AsString(labels)
		
		self.system = build_scoring_system(labels)
		
		for y_idx from 0 <= y_idx < label_num:
			for x_idx from 0 <= x_idx < label_num:
				if set_pair_score(self.system, labels_ptr[x_idx], labels_ptr[y_idx], substitution_matrix[(labels[x_idx],labels[y_idx])]) != 0:
					raise ValueError, 'error setting the pair score'
		
		if set_r(self.system, r) != 0:
			raise ValueError, 'error setting the parameter r'
	
	def __dealloc__(self):
		free_scoring_system(self.system)
	
	def compute_score(self, char* sequence1, char* sequence2):
		cdef int score
		if compute_score(self.system, sequence1, sequence2, &score) != 0:
			raise ValueError, 'error computing the score'
		else:
			return score
