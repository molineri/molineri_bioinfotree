#ifndef SCORING_H
#define SCORING_H

typedef struct scoring_system scoring_system_t;

scoring_system_t* build_scoring_system(const char* labels);
void free_scoring_system(scoring_system_t* system);

int set_pair_score(scoring_system_t* system, const char x, const char y, const int score);
int set_r(scoring_system_t* system, const int value);

int compute_score(scoring_system_t* system, const char* sequence1, const char* sequence2, int* score);

#endif //SCORING_H
