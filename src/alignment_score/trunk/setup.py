from distutils.core import setup
from distutils.extension import Extension
from Pyrex.Distutils import build_ext

setup(
	name = "scoreutils",
	ext_modules = [ 
	  Extension("scoreutils", ["scoreutils.pyx", "scoring.c"])
	],
	cmdclass = {'build_ext': build_ext}
)
