#include <grp.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

const char* const group_name = "postgen";
const char* const rsync_plain = "rsync.plain";

gid_t load_gid(const char* group_name)
{
	const struct group* group_info = getgrnam(group_name);
	if (!group_info)
	{
		fprintf(stderr, "Can't find a gid for group: %s\n", group_name);
		exit(1);
	}
	
	endgrent();
	return group_info->gr_gid;
}

int main(int argc, char* argv[])
{
	gid_t gid;
	
	gid = load_gid(group_name);
	if (setgid(gid) == -1)
	{
		fprintf(stderr, "Can't update gid\n");
		return 1;
	}
	
	umask(0007);
	
	argv[0] = (char*)rsync_plain;
	execvp(rsync_plain, argv);
	
	fprintf(stderr, "Can't run rsync\n");
	return 1;
}
