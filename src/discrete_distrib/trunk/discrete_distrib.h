#ifndef RD_DISTRIB_H
#define RD_DISTRIB_H

#include <gmp.h>
#include <gsl/gsl_rng.h>

struct weight_list
{
	mpq_t weight;
	unsigned long label;
	struct weight_list* next;
};

struct element
{
	unsigned long cumulative;
	unsigned long label;
};

typedef struct {
	struct weight_list* weights;
	unsigned long element_count;
	struct element* elements;
	gsl_rng* rng;
} discrete_distrib_t;

void discrete_distrib_init(discrete_distrib_t* distrib);
void discrete_distrib_clear(discrete_distrib_t* distrib);

int discrete_distrib_add(discrete_distrib_t* distrib, const mpq_t weight, const unsigned long label);
int discrete_distrib_finalize(discrete_distrib_t* distrib);

unsigned long discrete_distrib_get_random(discrete_distrib_t* distrib);

#endif //RD_DISTRIB_H
