#include <gsl/gsl_rng.h>
#include <sys/time.h>
#include <gsl/gsl_randist.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <strings.h>

unsigned long int get_seed(){
	struct timeval time;
	struct timezone timez;
	int rc;
	rc=gettimeofday(&time, &timez);
	return time.tv_sec*1000000+time.tv_usec;
}


int main(int argc, char *argv[])
{	
	double p[] = {1./3, 1./6, 1./6, 1./3};
	const gsl_rng_type * T;
	gsl_rng * GslRng;
	gsl_ran_discrete_t* rs;
	size_t n, i;
	unsigned long counts[4];

	gsl_rng_env_setup();

	T = gsl_rng_default;
	GslRng = gsl_rng_alloc(T);
	gsl_rng_set(GslRng,get_seed());
						
	rs = gsl_ran_discrete_preproc(4, p);

	n = atoi(argv[1]);
	bzero(counts, sizeof(counts));

	for (i=0; i<n; i++)
		counts[gsl_ran_discrete (GslRng, rs)]++;

	printf("%ld\t%ld\t%ld\t%ld\n",counts[0],counts[1],counts[2],counts[3]);
	return 0;
}

