#include <gsl/gsl_rng.h>
#include <sys/time.h>
#include <gsl/gsl_randist.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <strings.h>

unsigned long int get_seed(){
	struct timeval time;
	struct timezone timez;
	int rc;
	rc=gettimeofday(&time, &timez);
	return time.tv_sec*1000000+time.tv_usec;
}

int main(int argc, char *argv[])
{	
	const gsl_rng_type * T;
	gsl_rng * GslRng;
	long long n;

	gsl_rng_env_setup();

	T = gsl_rng_default;
	GslRng = gsl_rng_alloc(T);
	gsl_rng_set(GslRng,get_seed());
					
	if (strcmp(argv[1], "min") == 0)
		printf("%lu\n", gsl_rng_min(GslRng));
	else if (strcmp(argv[1], "max") == 0)
		printf("%lu\n", gsl_rng_max(GslRng));
	else
	{
		n = atoll(argv[1]);
		while (n > 0)
		{
			printf("%lu\n", gsl_rng_get(GslRng));
			n--;
		}
	}

	return 0;
}

