#include <gmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include "discrete_distrib.h"

void check_leaks()
{
	pid_t pid = getpid();
	char cmd[1024];
	
	sprintf(cmd, "leaks %d >&2", pid);
	system(cmd);
}

int main(int argc, char* argv[])
{
	discrete_distrib_t d;
	mpq_t weight;
	char* weights[] = {"1/3", "1/6", "1/6", "1/3"};
	unsigned long pick;
	unsigned long long i, n;
	unsigned long long counts[] = {0, 0, 0, 0};
	
	if (argc != 2)
	{
		fprintf(stderr, "Unexpected argument number.");
		return 1;
	}
	n = strtoll(argv[1], NULL, 10);
	
	discrete_distrib_init(&d);
	mpq_init(weight);
	
	for (i = 0; i < 4; i++)
	{
		mpq_set_str(weight, weights[i], 10);
		if (!discrete_distrib_add(&d, weight, i))
		{
			fprintf(stderr, "Cannot add element %llu.\n", i);
			return 1;
		}
	}
	
	if (!discrete_distrib_finalize(&d))
	{
		fprintf(stderr, "Cannot finalize distribution.\n");
		return 1;
	}
	
	for (i = 0; i < n; i++)
	{
		pick = discrete_distrib_get_random(&d);
		counts[pick]++;
	}
	discrete_distrib_clear(&d);
	
	for (i = 0; i < 4; i++)
		printf("%llu: %llu\n", i, counts[i]);

	//sleep(2);
	//check_leaks();
	
	return 0;
}
