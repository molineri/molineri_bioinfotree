#include <stdlib.h>
#include <sys/time.h>
#include "discrete_distrib.h"

unsigned long int get_seed()
{
	struct timeval time;
	struct timezone timez;
	
	gettimeofday(&time, &timez);
	return time.tv_sec * 1000000 + time.tv_usec;
}

void discrete_distrib_init(discrete_distrib_t* distrib)
{
	const gsl_rng_type* rng_type;
	
	distrib->weights = NULL;
	distrib->element_count = 0;
	distrib->elements = NULL;
	
	gsl_rng_env_setup();
	rng_type = gsl_rng_default;
	distrib->rng = gsl_rng_alloc(rng_type);
	gsl_rng_set(distrib->rng, get_seed());
}

void discrete_distrib_clear(discrete_distrib_t* distrib)
{
	if (distrib->weights)
	{
		struct weight_list* ptr = distrib->weights;
		struct weight_list* next;
		while (ptr != NULL)
		{
			mpq_clear(ptr->weight);
			
			next = ptr->next;
			free(ptr);
			ptr = next;
		}
	}
	
	if (distrib->element_count > 0)
		free(distrib->elements);
	
	gsl_rng_free(distrib->rng);
}

int is_valid_weight(const mpq_t weight)
{
	mpq_t zero;
	int is_valid;
	
	mpq_init(zero);
	is_valid = mpq_cmp(weight, zero) > 0;
	mpq_clear(zero);
	
	return is_valid;
}

void insert_sorted_weight(discrete_distrib_t* distrib, struct weight_list* nl)
{
	struct weight_list *ptr, *prev;
	
	ptr = distrib->weights;
	if (ptr == NULL)
	{
		distrib->weights = nl;
		nl->next = NULL;
		return;
	}
	
	prev = NULL;
	while (ptr != NULL)
	{
		if (mpq_cmp(nl->weight, ptr->weight) > 0)
		{
			if (prev == NULL)
			{
				distrib->weights = nl;
				nl->next = NULL;
			}
			else
			{
				nl->next = prev->next;
				prev->next = nl;
			}
			
			return;
		}
		else
		{
			prev = ptr;
			ptr = ptr->next;
		}
	}
	
	prev->next = nl;
	nl->next = NULL;
}

int discrete_distrib_add(discrete_distrib_t* distrib, const mpq_t weight, const unsigned long label)
{
	struct weight_list* nl;
	
	if (!is_valid_weight(weight))
		return 0;
	
	nl = (struct weight_list*)malloc(sizeof(struct weight_list));
	if (nl == NULL)
		return 0;
	
	mpq_init(nl->weight);
	mpq_set(nl->weight, weight);
	nl->label = label;
	
	insert_sorted_weight(distrib, nl);
	return 1;
}

unsigned long element_count(const struct weight_list* ptr)
{
	unsigned long count = 0;
	
	while (ptr != NULL)
	{
		ptr = ptr->next;
		count++;
	}
	
	return count;
}

void find_weight_den_lcm(const struct weight_list* ptr, mpz_t lcm)
{
	mpz_set_ui(lcm, 1);
	while (ptr != NULL)
	{
		mpz_lcm(lcm, lcm, mpq_denref(ptr->weight));
		ptr = ptr->next;
	}
}

int rescale_weights(discrete_distrib_t* distrib)
{
	int res = 0;
	mpz_t lcm, rng_max, scale_factor;
	struct weight_list* ptr;
	
	if (gsl_rng_min(distrib->rng) != 0)
		goto clear;
	
	mpz_init(lcm);
	mpz_init_set_ui(rng_max, gsl_rng_max(distrib->rng));
	mpz_init(scale_factor);
	
	find_weight_den_lcm(distrib->weights, lcm);
	if (mpz_cmp(lcm, rng_max) > 0)
		goto clear;
	
	mpz_fdiv_q(scale_factor, rng_max, lcm);
	mpz_mul(lcm, lcm, scale_factor);
	
	ptr = distrib->weights;
	while (ptr != NULL)
	{
		mpz_fdiv_q(scale_factor, lcm, mpq_denref(ptr->weight));
		
		mpz_mul(mpq_numref(ptr->weight), mpq_numref(ptr->weight), scale_factor);
		mpz_set(mpq_denref(ptr->weight), lcm);
		if (mpz_cmp(mpq_numref(ptr->weight), rng_max) > 0)
			goto clear;
		
		ptr = ptr->next;
	}
	
	res = 1;

clear:
	mpz_clear(scale_factor);
	mpz_clear(rng_max);
	mpz_clear(lcm);
	return res;
}

#ifdef DEBUG
void dump_weights(discrete_distrib_t* distrib)
{
	struct weight_list* ptr = distrib->weights;
	unsigned long num, den;
	
	puts("[DEBUG] Weights");
	while (ptr != NULL)
	{
		num = mpz_get_ui(mpq_numref(ptr->weight));
		den = mpz_get_ui(mpq_denref(ptr->weight));
		printf("%llu: %lu/%lu\n", ptr->label, num, den);
		
		ptr = ptr->next;
	}
}
#endif

#ifdef DEBUG
void dump_elements(discrete_distrib_t* distrib)
{
	struct element* ptr = distrib->elements;
	
	puts("[DEBUG] Elements");
	while (ptr->label != NULL)
	{
		printf("%llu: %lu\n", ptr->label, ptr->cumulative);
		ptr++;
	}
}
#endif

int discrete_distrib_finalize(discrete_distrib_t* distrib)
{
	unsigned long count, i;
	struct weight_list* ptr;
	unsigned long cumulative;
	
	if (distrib->element_count != 0)
		return 0;
	
	count = element_count(distrib->weights);
	if (element_count == 0)
		return 0;
	
	if (!rescale_weights(distrib))
		return 0;
	
#ifdef DEBUG
	dump_weights(distrib);
#endif
	
	distrib->elements = (struct element*)calloc(count+1, sizeof(struct element));
	if (distrib->elements == NULL)
		return 0;
	
	cumulative = 0;
	distrib->element_count = count;
	for (i = 0; i < count; i++)
	{
		ptr = distrib->weights;
		
		cumulative += mpz_get_ui(mpq_numref(ptr->weight));
		distrib->elements[i].cumulative = cumulative;
		distrib->elements[i].label = ptr->label;
		
		mpq_clear(ptr->weight);
		distrib->weights = ptr->next;
		free(ptr);
	}
	
#ifdef DEBUG
	dump_elements(distrib);
#endif

	return 1;
}

unsigned long discrete_distrib_get_random(discrete_distrib_t* distrib)
{
	while (1)
	{
		unsigned long rnd_value = gsl_rng_get(distrib->rng);
		unsigned long i;
	
		for (i = 0; i < distrib->element_count; i++)
		{
			if (distrib->elements[i].cumulative >= rnd_value)
				return distrib->elements[i].label;
		}
	}
}
