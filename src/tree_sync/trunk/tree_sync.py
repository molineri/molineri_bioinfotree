#!/usr/bin/env python

# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from __future__ import division
from optparse import OptionParser
from os import listdir, system, walk
from os.path import abspath, isdir, join
import sys

def exit(reason):
	sys.exit('[tree_sync] %s' % reason)

def relative_path(top, path):
	if not path.startswith(top):
		raise ValueError, 'invalid path'
	else:
		return path[len(top):]

def dir_content(path):
	filenames = set()
	dirnames = set()
	for name in listdir(path):
		if isdir(join(path, name)):
			dirnames.add(name)
		else:
			filenames.add(name)
	
	return filenames, dirnames

def count_files(path):
	count = 0
	for root, dirnames, filenames in walk(path):
		count += len(filenames)
	return count

def main():
	parser = OptionParser(usage='%prog SOURCE_TREE DEST_TREE')
	parser.add_option('-l', '--level', dest='level', type='float', default=0.3, help='the maximum level of changes to allow (default: 0.3)')
	parser.add_option('-n', '--dry-run', dest='dry_run', action='store_true', default=False, help='do not sync the trees; just report the number of changes')
	parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='emit verbose output')
	options, args = parser.parse_args()

	if len(args) != 2:
		exit('Unexpected argument number.')
	elif not isdir(args[0]):
		exit('Invalid source tree: %s' % args[0])
	elif not isdir(args[1]):
		exit('Invalid destination tree: %s' % args[1])

	args[0] = abspath(args[0]) + '/'
	args[1] = abspath(args[1]) + '/'
	
	changes = 0
	total = 0
	for root, dirnames, filenames in walk(args[0]):
		rpath = relative_path(args[0], root)
		dest_filenames, dest_dirnames = dir_content(join(args[1], rpath))

		changes += len(set(filenames) ^ dest_filenames)
		total += len(dest_filenames)

		src_dirnames = set(dirnames)
		for dirname in (src_dirnames - dest_dirnames):
			if options.verbose:
				print '[missing] %s' % join(args[0], rpath, dirname)
			changes += count_files(join(args[0], dirname))
		for dirname in (dest_dirnames - src_dirnames):
			if options.verbose:
				print '[missing] %s' % join(args[1], rpath, dirname)
			file_count = count_files(join(args[1], dirname))
			changes += file_count
			total += file_count

		dirnames[:] = list(src_dirnames & dest_dirnames)
	
	if options.dry_run:
		print 'Changes: % 10d' % changes
		print 'Total:   % 10d' % total
		if total != 0 and options.level > changes / total:
			print 'Sync would work.'
		else:
			print 'Sync wouldn\'t work.'
	elif total != 0 and options.level > changes / total:
		res = system('rsync -qavP --delete %s %s' % (args[0], args[1]))
		if res != 0:
			exit('Error running rsync.')
	else:
		exit('Too many changes to attempt a sync.')

if __name__ == '__main__':
	main()

