#ifndef OUTPUT_H
#define OUTPUT_H

#include <stdio.h>
#include <string>
#include "alignment.h"

unsigned int write_alignment(FILE* stream, const Alignment& alignment);

class SortedOutput
{
public:
	SortedOutput(const unsigned int buffer_size);
	~SortedOutput();
	
	void write(const Alignment& alignment);
	void flush();

private:
	void open_sort_stream();

	unsigned int buffer_size;
	FILE* sort_stream;
	unsigned bytes_written;
};

#endif //SORT_H
