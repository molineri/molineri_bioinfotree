#include <sys/types.h>
#include <sys/wait.h>
#include <sstream>
#include <stdexcept>
#include <string>
#include "output.h"

#include <iostream>

using namespace std;

unsigned int write_alignment(FILE* stream, const Alignment& alignment)
{
	ostringstream ss;
	ss << alignment.qchr << "\t" << alignment.qstart << "\t" << alignment.qstop << "\t" \
	   << (*alignment.tokens)[3] << "\t" \
	   << alignment.tchr << "\t" << alignment.tstart << "\t" << alignment.tstop;
	
	for (unsigned int i = alignment.payload_offset; i < alignment.tokens->size(); ++i)
		ss << "\t" << (*alignment.tokens)[i];
	ss << endl;
	
	const string output = ss.str();
	const unsigned int size = output.size();
	if (fwrite(output.data(), 1, size, stream) < output.size())
		throw runtime_error("output error");
	else
		return size;
}

SortedOutput::SortedOutput(const unsigned int buffer_size) : buffer_size(buffer_size), sort_stream(NULL), bytes_written(0)
{
}

SortedOutput::~SortedOutput()
{
	buffer_size = 0;
	flush();
}

void SortedOutput::write(const Alignment& alignment)
{
	if (sort_stream == NULL)
		open_sort_stream();
	
	bytes_written += write_alignment(sort_stream, alignment);
}

void SortedOutput::flush()
{
	if (sort_stream != NULL && bytes_written >= 3 * buffer_size / 4)
	{
		cerr << "sending an EOF to sort after " << bytes_written << " bytes" << endl;
		bytes_written = 0;
		
		const int exit_status = pclose(sort_stream);
		sort_stream = NULL;
		
		if (exit_status == -1)
			throw runtime_error("cannot retrieve sort process exit status");
		else if (!WIFEXITED(exit_status) || WEXITSTATUS(exit_status) != 0)
			throw runtime_error("the sort process exited with an error");
	}
}

void SortedOutput::open_sort_stream()
{
	ostringstream ss;
	ss << "sort -k2n -k3n -k6n -k7n -S" << buffer_size << "b";
	
	sort_stream = popen(ss.str().c_str(), "w");
	if (sort_stream == NULL)
		throw runtime_error("cannot start the sort process");
}
