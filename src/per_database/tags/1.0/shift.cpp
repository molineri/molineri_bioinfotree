#include <iostream>
#include <limits.h>
#include <skiplist.h>
#include <sstream>
#include <stdexcept>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <unistd.h>
#include <vector>
#include "util.h"

using namespace skiplist;
using namespace std;

struct Hit
{
	bool operator==(const Hit& rhs)
	{
		return qstart == rhs.qstart && \
		       qstop  == rhs.qstop  && \
		       tstart == rhs.tstart && \
		       tstop  == rhs.tstop;
	}
	
	void swap()
	{
		unsigned long tmp = qstart;
		qstart = tstart;
		tstart = tmp;
		
		tmp = qstop;
		qstop = tstop;
		tstop = tmp;
	}

	unsigned long qstart;
	unsigned long qstop;
	unsigned long tstart;
	unsigned long tstop;
	
	Hit* next;
};

extern char *optarg;
extern int optind;
static bool single_species = false;
static unsigned long lineno = 0;

void parse_args(const int argc, char* const* argv)
{
	int ch;
	while ((ch = getopt(argc, argv, "hs")) != -1)
	{
		switch (ch)
		{		
		case 'h':
			cerr << "usage: " << program_name(argv[0]) << " [OPTIONS] <BLAST_OUTPUT" << endl;
			cerr << "Options:" << endl;
			cerr << "\t-h             print this help." << endl;
			cerr << "\t-s             enable single species mode (simmetry filter)." << endl;
			cerr << endl;
			exit(0);
		
		case 's':
			single_species = true;
			break;
			
		case '?':
			exit(1);
		}
	}
	
	if (optind != argc)
	{
		cerr << "Error: unexpected argument number." << endl;
		exit(1);
	}
}

void parse_label(char* value, char** chr, unsigned long* offset, const char* name)
{
	try
	{
		char* token = strsep(&value, "_\0");
		if (token == NULL)
			throw runtime_error("");
		else if (value == NULL)
		{
			*chr = token;
			*offset = 0;
		}
		else
		{
			if ((*chr = strsep(&value, "_\0")) == NULL ||
			    value == NULL)
				throw runtime_error("");

			*offset = parse_ulong(value, "");
		}

		if (strncmp(*chr, "chr", 3) == 0)
			*chr += 3;
	}
	catch (runtime_error &e)
	{
		ostringstream ss;
		ss << "invalid " << name << " label";
		throw runtime_error(ss.str());
	}
}

bool match_hit(Hit* hit, SkipList<Hit*>& hits)
{
	Hit** value = hits.get(hit->qstart);
	if (value)
	{
		Hit* cur = *value;
		while (cur)
		{
			if (*cur == *hit)
				return true;
			else
				cur = cur->next;
		}
	}
	
	return false;
}

void append_hit_unique(Hit* hit, SkipList<Hit*>& hits)
{
	Hit** value = hits.get(hit->qstart);
	if (value)
	{
		Hit* cur = *value;
		while (true)
		{
			if (*cur == *hit)
				return;
			
			if (cur->next == NULL)
				break;
			else
				cur = cur->next;
		}
		cur->next = hit;
	}
	else
		hits.set(hit->qstart, hit);
}

void purge_out_of_scope(unsigned long min_key, SkipList<Hit*>& hits)
{
	SkipList<Hit*>::ConstCursor cursor = hits.cursor();
	bool more = cursor.next();
	while (more)
	{
		const unsigned long key = cursor.key();
		if (key < min_key)
		{
			more = cursor.next();
			
			Hit* value;
			hits.del(key, &value);
			
			while (value)
			{
				Hit* tmp = value;
				value = value->next;
				delete tmp;
			}
		}
		else
			break;
	}
}

int main(int argc, char* argv[])
{
	int same_chr = -1;

	parse_args(argc, argv);

	try
	{
		SkipList<Hit*> hits(static_cast<unsigned long>(1e7));
		unsigned long last_offset = 0;
	
		while (true)
		{
			char buf[2048];
			cin.getline(buf, 2048);
			if (cin.eof())
				break;
			else if (cin.fail())
				throw runtime_error("unexpected I/O failure");
			else
				++lineno;
			
			if (strlen(buf) == 0 || buf[0] == '>')
				continue;
			
			vector<char*> tokens = tokenize_line(buf);
			if (tokens.size() != 12)
				throw runtime_error("unexpected token number");
			
			char* qchr;
			unsigned long qoffset;
			parse_label(tokens[0], &qchr, &qoffset, "query");
			
			char* tchr;
			unsigned long toffset;
			parse_label(tokens[1], &tchr, &toffset, "target");
			
			unsigned long qstart = parse_ulong(tokens[6], "qstart") + qoffset;
			unsigned long qstop  = parse_ulong(tokens[7], "tstop")  + qoffset;
			unsigned long tstart = parse_ulong(tokens[8], "tstart") + toffset;
			unsigned long tstop  = parse_ulong(tokens[9], "tstop")  + toffset;
			
			const char* strand;
			if (tstart > tstop)
			{
				const unsigned long tmp = tstart;
				tstart = tstop;
				tstop = tmp;
				strand = "-";
			}
			else
				strand = "+";
			
			bool printable = true;
			if (single_species)
			{
				if (same_chr == -1)
					same_chr = strcmp(qchr, tchr) == 0;
				
				if (same_chr)
				{
					if (qstart == tstart && qstop == tstop)
					{
						if (strand[0] == '-')
							cerr << qchr << "\t" << qstart << "\t" << qstop << "\t" \
								 << strand << "\t" \
								 << tchr << "\t" << tstart << "\t" << tstop << "\t" \
								 << tokens[3] << "\t" << tokens[2] << "\t" \
								 << tokens[11] << "\t" << tokens[10] << "\t" \
								 << tokens[4] << "\t" << tokens[5] << endl;
						
						printable = false;
					}
					else
					{
						Hit* hit = new Hit;
						hit->qstart = qstart;
						hit->qstop  = qstop;
						hit->tstart = tstart;
						hit->tstop  = tstop;
						hit->next = NULL;
						
						if (match_hit(hit, hits))
						{
							printable = false;
							delete hit;
						}
						else
						{
							hit->swap();
							append_hit_unique(hit, hits);
						}
					}
					
					if (last_offset != qoffset)
					{
						purge_out_of_scope(qoffset, hits);
						last_offset = qoffset;
					}
				}
			}
			
			if (printable)
			{
				cout << qchr << "\t" << qstart << "\t" << qstop << "\t" \
					 << strand << "\t" \
					 << tchr << "\t" << tstart << "\t" << tstop << "\t" \
					 << tokens[3] << "\t" << tokens[2] << "\t" \
					 << tokens[11] << "\t" << tokens[10] << "\t" \
					 << tokens[4] << "\t" << tokens[5] << endl;
			}
		}
	}
	catch (runtime_error& e)
	{
		cerr << "Error: " << e.what() << " at line " << lineno << endl;
		return 1;
	}
	
	return 0;
}

