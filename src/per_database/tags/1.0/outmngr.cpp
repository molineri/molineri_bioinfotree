#include <fcntl.h>
#include <limits>
#include <sstream>
#include <stdarg.h>
#include <stdexcept>
#include <sys/types.h>
#include <sys/wait.h>
#include "outmngr.h"

using namespace std;

Pipe::Pipe(int fd) : fd_(fd)
{
}

Pipe::~Pipe()
{
	close();
}

void Pipe::close()
{
	if (fd_ != -1)
	{
		::close(fd_);
		fd_ = -1;
	}
}

inline void Pipe::write(const std::string& data)
{
	if (::write(fd_, data.data(), data.length()) == -1)
		throw runtime_error("broken pipe");
}

OutputManager::OutputManager(const char* prefix, const unsigned long blockSize) :
	prefix_(prefix), blockSize_(blockSize),	fileCount_(0),
	blockCount_(blockSize), lastCoord_(numeric_limits<unsigned long>::max())
{
	if (blockSize != 0)
	{
		index_.open((prefix_ + ".idx").c_str());
		if (!index_.is_open())
			throw runtime_error("cannot open index file");
	}	
}

void OutputManager::close()
{
	if (blockSize_ != 0)
	{
		if (closePipe())
			updateIndex();
		index_.close();
	}
	else
		closePipe();
}

void OutputManager::write(const char* qchr, const unsigned long qstart, const unsigned long qstop,
		                  const char* strand, const char* tchr, const unsigned long tstart,
		                  const unsigned long tstop, const unsigned int extraFields, ...)
{
	if (blockSize_ != 0)
	{
		++blockCount_;
		if (blockCount_ >= blockSize_ && qstart != lastCoord_)
			newBlock(qstart);
		lastCoord_ = qstart;
	}
	else if (dataPipe_.get() == NULL)
	{
		updateBlockInfo();
		openPipe();
	}
	
	ostringstream line;
	line << qchr << "\t" << qstart << "\t" << qstop << "\t" \
	     << strand << "\t" \
	     << tchr << "\t" << tstart << "\t" << tstop;
	
	va_list ap;
	va_start(ap, extraFields);
	unsigned int remaining = extraFields;
	while (remaining-- > 0)
		line << "\t" << va_arg(ap, const char*);
	va_end(ap);
	
	line <<	endl;
	dataPipe_->write(line.str());
	
// 	 << tokens[3] << "\t" << tokens[2] << "\t" \
// 	 << tokens[11] << "\t2\t" << tokens[10]
}

void OutputManager::newBlock(const unsigned long blockStart)
{
	if (dataPipe_.get())
	{
		closePipe();
		updateIndex();
	}
	
	updateBlockInfo(blockStart);
	openPipe();
}

bool OutputManager::closePipe()
{
	if (dataPipe_.get())
	{
		dataPipe_->close();
	
		int status;
		wait(&status);
		if (!WIFEXITED(status) || WEXITSTATUS(status) != 0)
			throw runtime_error("data writer process exited with an error");
		
		return true;
	}
	else
		return false;
}

void OutputManager::updateIndex()
{
	index_ << blockName_ << "\t" << blockStart_ << "\t" << lastCoord_ << endl;
}

void OutputManager::updateBlockInfo(const unsigned long blockStart)
{
	ostringstream filename;
	if (blockSize_ == 0)
		filename << prefix_ << ".gz";
	else
		filename << prefix_ << "-" << fileCount_++ << ".gz";

	blockName_  = filename.str();	
	blockStart_ = blockStart;
	blockCount_ = 0;
}

void OutputManager::openPipe()
{
	int fds[2];
	if (pipe(fds) == -1)
		throw runtime_error("can't create data pipe");
	
	switch (fork())
	{
	case -1:
		throw runtime_error("fork error");
		
	case 0:
		{
			::close(fds[1]);
			const int outFd = open(blockName_.c_str(), O_WRONLY | O_CREAT, 0666);
			if (outFd == -1 ||
				dup2(fds[0], 0) == -1 ||
				dup2(outFd, 1) == -1 ||
				execlp("gzip", "gzip", NULL) == -1)
				_exit(1);
		}
		// we should never get here
		break;
	
	default:
		::close(fds[0]);
		dataPipe_.reset(new Pipe(fds[1]));
	}

}
