#ifndef UTIL_H
#define UTIL_H

#include <vector>

unsigned long parse_ulong(const char* repr, const char* name);
const char* program_name(const char* argv0);
std::vector<char*> tokenize_line(char* line);

#endif //UTIL_H
