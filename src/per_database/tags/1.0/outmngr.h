#ifndef OUTMNGR_H
#define OUTMNGR_H

#include <fstream>
#include <memory>
#include <string>
#include <unistd.h>

class Pipe
{
public:
	Pipe(int fd);
	~Pipe();
	void close();
	void write(const std::string& data);

private:
	int fd_;
};

class OutputManager
{
public:
	OutputManager(const char* prefix, const unsigned long blockSize);
	void close();
	void write(const char* qchr, const unsigned long qstart, const unsigned long qstop,
	           const char* strand, const char* tchr, const unsigned long tstart,
	           const unsigned long tstop, const unsigned int extraFields, ...);

private:
	void newBlock(const unsigned long blockStart);
	bool closePipe();
	void updateIndex();
	void updateBlockInfo(const unsigned long blockStart = 0);
	void openPipe();

	const std::string prefix_;
	const unsigned long blockSize_;
	
	std::ofstream index_;
	unsigned int fileCount_;
	std::auto_ptr<Pipe> dataPipe_;
	std::string blockName_;
	unsigned long blockCount_;
	unsigned long blockStart_;
	unsigned long lastCoord_;
};

#endif //OUTMNGR_H
