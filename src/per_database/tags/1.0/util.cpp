#include <limits.h>
#include <sstream>
#include <stdexcept>
#include <stdlib.h>
#include "util.h"

using namespace std;

unsigned long parse_ulong(const char* repr, const char* name)
{
	char* endptr;
	long value = strtol(repr, &endptr, 10);
	if (*endptr != 0 || value < 0)
	{
		ostringstream ss;
		ss << "invalid " << name << " value";
		throw runtime_error(ss.str());
	}

	return value;
}

const char* program_name(const char* argv0)
{
	int i = strlen(argv0) - 1;
	while (i >= 0 && argv0[i] != '/')
		--i;
	
	return &argv0[i+1];
}

vector<char*> tokenize_line(char* line)
{
	vector<char*> v;
	char* token;

	v.reserve(13);
	while ((token = strsep(&line, " \t\0")))
	{
		if (token[0] != 0)
			v.push_back(token);
	}
	
	return v;
}
