#ifndef OUTPUT_H
#define OUTPUT_H

#include <stdio.h>
#include <string>
#include "alignment.h"

void write_alignment(FILE* stream, const Alignment& alignment);

class SortedOutput
{
public:
	SortedOutput(const char* sort_options = "");
	~SortedOutput();
	
	void write(const Alignment& alignment);
	void flush();

private:
	void open_sort_stream();

	std::string sort_options;
	FILE* sort_stream;
};

#endif //SORT_H
