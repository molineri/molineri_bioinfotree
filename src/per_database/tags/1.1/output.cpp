#include <sys/types.h>
#include <sys/wait.h>
#include <sstream>
#include <stdexcept>
#include <string>
#include "output.h"

using namespace std;

void write_alignment(FILE* stream, const Alignment& alignment)
{
	ostringstream ss;
	ss << alignment.qchr << "\t" << alignment.qstart << "\t" << alignment.qstop << "\t" \
	   << alignment.strand << "\t" \
	   << alignment.tchr << "\t" << alignment.tstart << "\t" << alignment.tstop;
	
	for (unsigned int i = alignment.payload_offset; i < alignment.tokens->size(); ++i)
		ss << "\t" << (*alignment.tokens)[i];
	ss << endl;
	
	const string output = ss.str();
	if (fwrite(output.data(), 1, output.size(), stream) < output.size())
		throw runtime_error("output error");
}

SortedOutput::SortedOutput(const char* sort_options) : sort_options(sort_options), sort_stream(NULL)
{
}

SortedOutput::~SortedOutput()
{
	flush();
}

void SortedOutput::write(const Alignment& alignment)
{
	if (sort_stream == NULL)
		open_sort_stream();
	
	write_alignment(sort_stream, alignment);
}

void SortedOutput::flush()
{
	if (sort_stream != NULL)
	{
		const int exit_status = pclose(sort_stream);
		sort_stream = NULL;
		
		if (exit_status == -1)
			throw runtime_error("cannot retrieve sort process exit status");
		else if (!WIFEXITED(exit_status) || WEXITSTATUS(exit_status) != 0)
			throw runtime_error("the sort process exited with an error");
	}
}

void SortedOutput::open_sort_stream()
{
	string cmd = "sort -k2n -k3n -k6n -k7n";
	if (!sort_options.empty())
	{
		cmd += " ";
		cmd += sort_options;
	}
	
	sort_stream = popen(cmd.c_str(), "w");
	if (sort_stream == NULL)
		throw runtime_error("cannot start the sort process");
}
