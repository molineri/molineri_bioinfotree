#ifndef ALIGNMENT_H
#define ALIGNMENT_H

#include <vector>

struct Alignment
{
	char* qchr;
	unsigned long qstart;
	unsigned long qstop;
	char* strand;
	char* tchr;
	unsigned long tstart;
	unsigned long tstop;
	
	std::vector<char*>* tokens;
	unsigned int payload_offset;
};

#endif //ALIGNMENT_H
