#include <iostream>
#include <limits.h>
#include <signal.h>
#include <skiplist.h>
#include <sstream>
#include <stdexcept>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <unistd.h>
#include <vector>
#include "alignment.h"
#include "output.h"
#include "util.h"

using namespace skiplist;
using namespace std;

struct Hit
{
	Hit(Alignment& alignment)
	{
		qstart = alignment.qstart;
		qstop  = alignment.qstop;
		tstart = alignment.tstart;
		tstop  = alignment.tstop;
		next = NULL;
	}
	
	bool operator==(const Hit& rhs)
	{
		return qstart == rhs.qstart && \
		       qstop  == rhs.qstop  && \
		       tstart == rhs.tstart && \
		       tstop  == rhs.tstop;
	}
	
	void swap()
	{
		unsigned long tmp = qstart;
		qstart = tstart;
		tstart = tmp;
		
		tmp = qstop;
		qstop = tstop;
		tstop = tmp;
	}

	unsigned long qstart;
	unsigned long qstop;
	unsigned long tstart;
	unsigned long tstop;
	
	Hit* next;
};

extern char *optarg;
extern int optind;
static unsigned int buffer_size = 100*1024*1024;
static bool single_species = false;
static bool inversion_filter = false;
static unsigned long lineno = 0;

void parse_args(const int argc, char* const* argv)
{
	int ch;
	while ((ch = getopt(argc, argv, "hb:si")) != -1)
	{
		switch (ch)
		{		
		case 'h':
			cerr << "usage: " << program_name(argv[0]) << " [OPTIONS] <BLAST_OUTPUT" << endl;
			cerr << "Options:" << endl;
			cerr << "\t-h             print this help" << endl;
			cerr << "\t-b             buffer size for the sort process (bytes)" << endl;
			cerr << "\t-s             enable single species mode (simmetry filter)" << endl;
			cerr << "\t-i             enable inversion filter (only in single species mode)" << endl;
			cerr << endl;
			exit(0);
		
		case 'b':
			try
			{
				buffer_size = parse_ulong(optarg, "buffer size");
			}
			catch (runtime_error& e)
			{
				cerr << "Error: " << e.what() << endl;
				exit(1);
			}
			break;
		
		case 's':
			single_species = true;
			break;
		
		case 'i':
			inversion_filter = true;
			break;
		
		case '?':
			exit(1);
		}
	}
	
	if (optind != argc)
	{
		cerr << "Error: unexpected argument number." << endl;
		exit(1);
	}
	else if (!single_species && inversion_filter)
	{
		cerr << "Error: inversion filter required, but single species mode disabled." << endl;
		exit(1);
	}
}

void parse_label(char* value, char** chr, unsigned long* offset, const char* name)
{
	try
	{
		*chr = strsep(&value, "_\0");
		if (*chr == NULL)
			throw runtime_error("");
		else if (value == NULL)
			*offset = 0;
		else
		{
			char* offset_repr = strsep(&value, "_\0");
			if (offset_repr == NULL || value != NULL)
				throw runtime_error("");

			*offset = parse_ulong(offset_repr, "");
		}

		if (strncmp(*chr, "chr", 3) == 0)
			*chr += 3;
	}
	catch (runtime_error &e)
	{
		ostringstream ss;
		ss << "invalid " << name << " label";
		throw runtime_error(ss.str());
	}
}

bool match_hit(Hit* hit, SkipList<Hit*>& hits)
{
	Hit** value = hits.get(hit->qstart);
	if (value)
	{
		Hit* cur = *value;
		while (cur)
		{
			if (*cur == *hit)
				return true;
			else
				cur = cur->next;
		}
	}
	
	return false;
}

void append_hit_unique(Hit* hit, SkipList<Hit*>& hits)
{
	Hit** value = hits.get(hit->qstart);
	if (value)
	{
		Hit* cur = *value;
		while (true)
		{
			if (*cur == *hit)
				return;
			
			if (cur->next == NULL)
				break;
			else
				cur = cur->next;
		}
		cur->next = hit;
	}
	else
		hits.set(hit->qstart, hit);
}

void purge_out_of_scope(unsigned long min_key, SkipList<Hit*>& hits)
{
	SkipList<Hit*>::ConstCursor cursor = hits.cursor();
	bool more = cursor.next();
	while (more)
	{
		const unsigned long key = cursor.key();
		if (key < min_key)
		{
			more = cursor.next();
			
			Hit* value;
			hits.del(key, &value);
			
			while (value)
			{
				Hit* tmp = value;
				value = value->next;
				delete tmp;
			}
		}
		else
			break;
	}
}

int main(int argc, char* argv[])
{
	int same_chr = -1;

	parse_args(argc, argv);
	signal(SIGPIPE, SIG_IGN);

	try
	{
		Alignment alignment;
		SkipList<Hit*> hits(static_cast<unsigned long>(1e7));
		SortedOutput sorted_output(buffer_size);
		unsigned long last_offset = 0;
	
		while (true)
		{
			const unsigned int buf_size = 4096;
			char buf[buf_size];
			
			cin.getline(buf, buf_size);
			if (cin.eof())
				break;
			else if (cin.fail())
				throw runtime_error("unexpected I/O failure");
			else
				++lineno;
			
			if (strlen(buf) == 0 || buf[0] == '>')
				continue;
			
			vector<char*> tokens = tokenize_line(buf, 13);
			if (tokens.size() < 7)
				throw runtime_error("not enough tokens");
			
			alignment.tokens = &tokens;
			alignment.payload_offset = 7;
			
			unsigned long qoffset;
			parse_label(tokens[0], &alignment.qchr, &qoffset, "query");
			
			unsigned long toffset;
			parse_label(tokens[4], &alignment.tchr, &toffset, "target");
			
			alignment.qstart = parse_ulong(tokens[1], "qstart") + qoffset;
			alignment.qstop  = parse_ulong(tokens[2], "qstop")  + qoffset;
			alignment.tstart = parse_ulong(tokens[5], "tstart") + toffset;
			alignment.tstop  = parse_ulong(tokens[6], "tstop")  + toffset;
			
			if (alignment.qstart >= alignment.qstop)
				throw runtime_error("invalid query coordinates");
			else if (alignment.tstart >= alignment.tstop)
				throw runtime_error("invalid target coordinates");
			
			const char* strand = tokens[3];
			if (strlen(strand) != 1 || (strand[0] != '+' && strand[0] != '-'))
				throw runtime_error("invalid strand");

			bool printable = true;
			if (single_species)
			{
				if (same_chr == -1)
					same_chr = strcmp(alignment.qchr, alignment.tchr) == 0;
				
				if (same_chr)
				{
					if (alignment.qstop > alignment.tstart)
						printable = false;
					else if (alignment.qstart == alignment.tstart && alignment.qstop == alignment.tstop)
					{
						if (strand[0] == '-')
							write_alignment(stderr, alignment);
						
						printable = false;
					}
					else
					{
						Hit* hit = new Hit(alignment);
						if (match_hit(hit, hits))
						{
							printable = false;
							delete hit;
						}
						else
						{
							hit->swap();
							append_hit_unique(hit, hits);
						}
					}
					
					if (last_offset != qoffset)
						purge_out_of_scope(qoffset, hits);
				}
			}
			
			if (last_offset != qoffset)
			{
				sorted_output.flush();
				last_offset = qoffset;
			}
			
			if (printable)
				sorted_output.write(alignment);
		}
	}
	catch (runtime_error& e)
	{
		cerr << "Error: " << e.what() << " while processing line " << lineno << endl;
		return 1;
	}
	
	return 0;
}

