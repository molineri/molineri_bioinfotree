#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <math.h>
#include "Histogram.h"


using namespace std;


int setup(int argc, char* argv[]);
void usage(char** argv);

bool options_log=false;
bool options_min=false;
bool options_max=false;
bool options_bin_n=false;
float opt_min, opt_max; 
int opt_bin_n;


int main( int argc, char* argv[] ){

	//controllo le opzioni passate da riga di comando
	if(setup(argc, argv)){
		cout << "main:: termined because setup error"<<endl;
		return(-1);
	}

	// carico i valori nell'istogramma
	Histogram *Hist = NULL;
	Hist = new Histogram(opt_min, opt_max, opt_bin_n, options_log);

	float n;

	while (cin >> n) {
		Hist->add(n);
	}

	Hist->print();
}


int setup(int argc, char* argv[]){
	
	////////////////////////////////////////////////////////////////
	// gestione delle opzioni
	//
	int i;
	while((i=getopt(argc, argv, "m:M:ln:"))!=-1){
		switch(i){
			case 'm':
				opt_min=atof(optarg);
				options_min=true;
				break;
			case 'M':
				opt_max=atof(optarg);
				options_max=true;
				break;
			case 'l':
				options_log=true;
				break;
			case 'n':
				opt_bin_n=atoi(optarg);
				options_bin_n=true;
				if (opt_bin_n <= 1) {
					cerr<<"n must be > 1"<<endl;
					return -1;
				}
				break;
			default:
				cerr<<"wrong options given"<<endl;
				usage(argv);
				return -1;
		}
	}
	if (!options_min or !options_max or !options_bin_n) {
		cerr<<"wrong options given"<<endl;
		usage(argv);
		return -1;
	}

	return(0);
}


void usage(char** argv){
	cout << "usage: " << argv[0] <<  " -m 0 -M 5000 -n 200 [-l] filename"<<endl
		<<"\t-m minimum value"<<endl
		<<"\t-M maximum value"<<endl
		<<"\t-n bins number"<<endl
		<<"\t-l to set logscale binning"<<endl;
	exit(-1);
}

