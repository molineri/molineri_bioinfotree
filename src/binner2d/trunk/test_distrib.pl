#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;

$\="\n";
$,="\t";


my $usage = "$0 -nr number_of_randomization_cycles -xi xmin -xu xmax -xn xbin_n -yI ymin -yU ymax -yN ybin_n
";


# read options into hash
my $runs = undef;
my $xmax = undef;
my $xmin = undef;
my $xbin_n = undef;
my $ymax = undef;
my $ymin = undef;
my $ybin_n = undef;

#GetOptions(
#	'random_cycles|nr=i' => \$runs,
#	'xmin|xi=i' => \$xmin,
#	'xmax|xu=i' => \$xmax,
#	'xbin_n|xn=i'=> \$xbin_n,
#	'ymin|yI=i' => \$ymin,
#	'ymin|yU=i' => \$ymax,
#	'ybin_n|yN=i' => \$ybin_n
#) or die("problem in parsing options\n");

$runs = 10;
$xmin = 0;
$xmax = 10;
$xbin_n = 10;
$ymin = 0;
$ymax = 10;
$ybin_n = 10;

die ($usage) if !defined $runs;
die ($usage) if (!defined $xmin or !defined $xmax or !defined $xbin_n);
die ($usage) if (!defined $ymin or !defined $ymax or !defined $ybin_n);


my $out = 'xy';
my $binner = 'ris';

open OUT,">$out" or die "Can't write output file ($out)";
open BINNER,">$binner" or die "Can't write output file ($binner)";


my $xbin_size = ($xmax - $xmin) / $xbin_n;
my $ybin_size = ($ymax - $ymin) / $ybin_n;
for (my $i=0; $i<$xbin_n; $i++) {
	for (my $j=0; $j<$ybin_n; $j++) {
		my $n = `./random_series -m 0 -M 20 -l 1 -i`;
		die "./random_series -m 0 -M 20 -l 1 -i","\n",$n if ($n =~ /err/i);
		$? and die "./random_series -m 0 -M 20 -l 1 -i";
		chomp $n;
		my $xi = $xmin + $i * $xbin_size;
		my $xu = $xmin + ($i + 1) * $xbin_size;
		my $yi = $ymin + $j * $ybin_size;
		my $yu = $ymin + ($j + 1) * $ybin_size;
		if ($n != 0) {
			my $list = `./random_series -m $xi -M $xu -l $n`;
			die "./random_series -m $xi -M $xu -l $n","\n",$list if ($list =~ /err/i);
			$? and die "./random_series -m $xi -M $xu -l $n";
			chomp $list;
			my @xlist = split /\n/,$list;
			$list = `./random_series -m $yi -M $yu -l $n`;
			die "./random_series -m $yi -M $yu -l $n","\n",$list if ($list =~ /err/i);
			$? and die "./random_series -m $yi -M $yu -l $n";
			chomp $list;
			my @ylist = split /\n/,$list;
			my $n_elim = 0;
			for (my $k=0; $k<$n; $k++) {
				if ( ($xlist[$k] == $xu) or ($ylist[$k] == $yu) ) {
					$n_elim++;
					next;
				}
				print OUT $xlist[$k],$ylist[$k];
			}
			$n -= $n_elim;
		}
		print BINNER $xi,$yi,$n;
	}
}
#print "./binner2d -i $xmin -u $xmax -n $xbin_n -I $ymin -U $ymax -N $ybin_n < $out";
