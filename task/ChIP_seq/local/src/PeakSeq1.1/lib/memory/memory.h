#ifndef _EXCEPTIONAL_MEMORY_
#define _EXCEPTIONAL_MEMORY_

void* alloc_mem(size_t size);

#endif // _MEMORY_