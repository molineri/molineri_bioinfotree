#include <stdio.h>
#include <stdlib.h>
#include "annot_region_tools.h"
#include <algorithm>
#include <string.h>
#include "../../utils/file/utils.h"

#define MIN(x,y) ((x) < (y)?(x):(y))
#define MAX(x,y) ((x) > (y)?(x):(y))

void dump_BED(char* bed_fp, vector<t_annot_region*>* annot_regions)
{
	FILE* f_bed = open_f(bed_fp, "w");

	for(unsigned int i_reg = 0; i_reg < annot_regions->size(); i_reg++)
	{
		if(annot_regions->at(i_reg)->strand == '-' ||
			annot_regions->at(i_reg)->strand == '+')
		{
			fprintf(f_bed, "%s\t%d\t%d\tREGION%d\t1000\t%c\n", annot_regions->at(i_reg)->chrom, 
				annot_regions->at(i_reg)->start,
				annot_regions->at(i_reg)->end, 
				i_reg,
				annot_regions->at(i_reg)->strand);
		}
		else
		{
			fprintf(f_bed, "%s\t%d\t%d\n", annot_regions->at(i_reg)->chrom, 
				annot_regions->at(i_reg)->start,
				annot_regions->at(i_reg)->end);
		}
	} // i_ref loop.

	fclose(f_bed);
}

t_sorted_annot_region_lists* restructure_annot_regions(vector<t_annot_region*>* regions)
{
	// This is the list of regions per chromosome.
	vector<char*>* chr_ids = get_chr_ids(regions);
	t_sorted_annot_region_lists* region_lists = new t_sorted_annot_region_lists();

	// The chromosome id list is built up dynamically in the region list loop.
	//region_lists->chr_ids = new vector<char*>();
	region_lists->chr_ids = chr_ids;
	region_lists->neg_strand_regions_per_chrom = new vector<t_annot_region*>*[chr_ids->size() + 2];
	region_lists->pos_strand_regions_per_chrom = new vector<t_annot_region*>*[chr_ids->size() + 2];
	region_lists->regions_per_chrom = new vector<t_annot_region*>*[chr_ids->size() + 2];

	for(unsigned int i_chr = 0; i_chr < chr_ids->size(); i_chr++)
	{
		region_lists->pos_strand_regions_per_chrom[i_chr] = new vector<t_annot_region*>();
		region_lists->neg_strand_regions_per_chrom[i_chr] = new vector<t_annot_region*>();
		region_lists->regions_per_chrom[i_chr] = new vector<t_annot_region*>();
	} // i_chr loop.

	// Go over all the regions and separate them into appropriate list.
	for(unsigned int i_reg = 0; i_reg < regions->size(); i_reg++)
	{
		unsigned int i_chr = get_i_chr(chr_ids, regions->at(i_reg)->chrom);

		if(i_chr < chr_ids->size())
		{
			region_lists->regions_per_chrom[i_chr]->push_back(regions->at(i_reg));

			if(regions->at(i_reg)->strand == '+')
			{
				region_lists->pos_strand_regions_per_chrom[i_chr]->push_back(regions->at(i_reg));
			}
			else if(regions->at(i_reg)->strand == '-')
			{
				region_lists->neg_strand_regions_per_chrom[i_chr]->push_back(regions->at(i_reg));
			}
			else
			{
				printf("Unknown strand char %c @ %s(%d).\n", regions->at(i_reg)->strand, __FILE__, __LINE__);
				exit(0);
			}
		}
	} // i_reg loop.

	for(unsigned int i_chr = 0; i_chr < chr_ids->size(); i_chr++)
	{
		region_lists->pos_strand_regions_per_chrom[i_chr] = new vector<t_annot_region*>();
		region_lists->neg_strand_regions_per_chrom[i_chr] = new vector<t_annot_region*>();
		region_lists->regions_per_chrom[i_chr] = new vector<t_annot_region*>();
	} // i_chr loop.

	// Finally sort the regions.
	return(region_lists);
}


t_sorted_annot_region_lists* restructure_annot_regions(vector<t_annot_region*>* regions, vector<char*>* chr_ids)
{
	// This is the list of regions per chromosome.
	t_sorted_annot_region_lists* region_lists = new t_sorted_annot_region_lists();

	region_lists->chr_ids = NULL; // Do not free the memory if it is null.
	region_lists->neg_strand_regions_per_chrom = new vector<t_annot_region*>*[chr_ids->size() + 2];
	region_lists->pos_strand_regions_per_chrom = new vector<t_annot_region*>*[chr_ids->size() + 2];
	region_lists->regions_per_chrom = new vector<t_annot_region*>*[chr_ids->size() + 2];

	for(unsigned int i_chr = 0; i_chr < chr_ids->size(); i_chr++)
	{
		region_lists->pos_strand_regions_per_chrom[i_chr] = new vector<t_annot_region*>();
		region_lists->neg_strand_regions_per_chrom[i_chr] = new vector<t_annot_region*>();
		region_lists->regions_per_chrom[i_chr] = new vector<t_annot_region*>();
	} // i_chr loop.

	// Go over all the regions and separate them into appropriate list.
	for(unsigned int i_reg = 0; i_reg < regions->size(); i_reg++)
	{
		unsigned int i_chr = get_i_chr(chr_ids, regions->at(i_reg)->chrom);

		if(i_chr < chr_ids->size())
		{
			region_lists->regions_per_chrom[i_chr]->push_back(regions->at(i_reg));

			if(regions->at(i_reg)->strand == '+')
			{
				region_lists->pos_strand_regions_per_chrom[i_chr]->push_back(regions->at(i_reg));
			}
			else if(regions->at(i_reg)->strand == '-')
			{
				region_lists->neg_strand_regions_per_chrom[i_chr]->push_back(regions->at(i_reg));
			}
			else
			{
				printf("Unknown strand char %c @ %s(%d).\n", regions->at(i_reg)->strand, __FILE__, __LINE__);
				exit(0);
			}
		}
	} // i_reg loop.

	for(unsigned int i_chr = 0; i_chr < chr_ids->size(); i_chr++)
	{
		region_lists->pos_strand_regions_per_chrom[i_chr] = new vector<t_annot_region*>();
		region_lists->neg_strand_regions_per_chrom[i_chr] = new vector<t_annot_region*>();
		region_lists->regions_per_chrom[i_chr] = new vector<t_annot_region*>();
	} // i_chr loop.

	// Finally sort the regions.
	return(region_lists);
}

int get_i_chr(vector<char*>* chr_ids, char* chrom_id)
{
	for(unsigned int i_chr = 0; i_chr < chr_ids->size(); i_chr++)
	{
		if(strcmp(chr_ids->at(i_chr), chrom_id) == 0)
		{
			return(i_chr);
		}
	} // i_chr loop.

	//printf("Could not find chromosome id %s @ %s(%d), adding it to the list.\n", chrom_id, __FILE__, __LINE__);
	return(chr_ids->size());
	////exit(0);
	//getc(stdin);

	//char* new_id = new char[strlen(chrom_id) + 2];
	//strcpy(new_id, chrom_id);
	//chr_ids->push_back(new_id);

	//return(get_i_chr(chr_ids, chrom_id));
}

vector<t_annot_region*>* load_BED(char* bed_fp)
{
	vector<t_annot_region*>* bed_regions = new vector<t_annot_region*>();

	FILE* f_bed = open_f(bed_fp, "r");
	
	char* cur_line = getline(f_bed);
	while(cur_line != NULL)
	{
		bool skip_line = check_line_skip(cur_line);

		if(!skip_line)
		{
			// Read the mandatory fields.
			int start;
			int end;
			char chrom[100];
			if(sscanf(cur_line, "%s %d %d", chrom, &start, &end) != 3)
			{
				printf("Could not read the mandatory fields from BED file line:\n%s\n", cur_line);
				exit(0);
			}

			char strand = '+'; 
			if(sscanf(cur_line, "%*s %*d %*d %*s %*s %c", &strand) != 1)
			{
				// Could not read the strand from file, set it to '+' by default.
				strand = '+';
			}

			t_annot_region* new_region = new t_annot_region();
			new_region->start = start;
			new_region->end = end;
			new_region->chrom = new char[strlen(chrom) + 2];
			strcpy(new_region->chrom, chrom);
			new_region->strand = strand;
			new_region->data = NULL;
		
			bed_regions->push_back(new_region);
		} // line skip check.

		delete [] cur_line;

		// Read the next line.
		cur_line = getline(f_bed);
	} // file reading loop.

	fclose(f_bed);

	//printf("Loaded %d regions from %s\n", bed_regions->size(), bed_fp);

	return(bed_regions);
}

// Load a narrowPeak file as an annotation file: Necessary for loading and processing ENCODE peak calls.
vector<t_annot_region*>* load_narrowPeak(char* narrowPeak_fp)
{
	printf("Loading narrowPeak file %s\n", narrowPeak_fp);

	vector<t_annot_region*>* np_regions = new vector<t_annot_region*>();

	FILE* f_np = open_f(narrowPeak_fp, "r");
	if(f_np == NULL)
	{
		printf("Could not open narrowPeak file @ %s\n", narrowPeak_fp);
		exit(0);
	}

	char* cur_line = getline(f_np);
	while(cur_line != NULL)
	{
		bool skip_line = check_line_skip(cur_line);

		if(!skip_line)
		{
			int start;
			int end;
			char chrom[100];

			if(sscanf(cur_line, "%s %d %d", chrom, &start, &end) != 3)
			{
				printf("Could not parse narrowPeak file entry line:\n%s\n", cur_line);
				exit(0);
			}

			t_narrowPeak_info* new_np_info = NULL;
			char score_str[100];
			char strand_str[100];
			char signal_val_str[100];
			char pval_str[100];
			char qval_str[100];
			char peak_pos_str[100];
			if(sscanf(cur_line, "%*s %*s %*s %*s %s %s %s %s %s %s",
								score_str,
								strand_str,
								signal_val_str,
								pval_str,
								qval_str,
								peak_pos_str) != 6)
			{
				printf("Could not read narrowPeak entries, not setting custom narrowPeak info.\n");
			}
			else
			{
				new_np_info = new t_narrowPeak_info();
				if(score_str[0] != '.')
				{
					new_np_info->score = atoi(score_str);
				}

				if(strand_str[0] != '.')
				{
					new_np_info->strand = score_str[0];
				}

				if(signal_val_str[0] != '.')
				{
					new_np_info->signal_value = atof(signal_val_str);
				}

				if(pval_str[0] != '.')
				{
					new_np_info->p_value = atof(pval_str);
				}

				if(qval_str[0] != '.')
				{
					new_np_info->q_value = atof(qval_str);
				}

				if(peak_pos_str[0] != '.')
				{
					new_np_info->peak_pos = atoi(peak_pos_str);
					printf("Setting peak position at %d\n", new_np_info->peak_pos);
				}

				new_np_info->info_type = NARROWPEAK_INFO;
			}

			// narrowPeak files do not contain strand information, everything is on forward strand.
			char strand = '+'; 

			// Allocate and initialize the new region.
			t_annot_region* new_region = new t_annot_region();
			new_region->start = start;
			new_region->end = end;
			new_region->chrom = new char[strlen(chrom) + 2];
			strcpy(new_region->chrom, chrom);
			new_region->strand = strand;
			new_region->data = new_np_info;

			// Add the new region to the list.
			np_regions->push_back(new_region);
		}

		delete [] cur_line;
		cur_line = getline(f_np);
	} // narrowPeak file reading loop.

	fclose(f_np);

	printf("Loaded %d regions from %s\n", np_regions->size(), narrowPeak_fp);
	return(np_regions);
}

vector<t_annot_region*>* load_GFF(char* gff_fp)
{
	printf("Loading GFF file %s\n", gff_fp);

	vector<t_annot_region*>* gff_regions = new vector<t_annot_region*>();

	FILE* f_gff = open_f(gff_fp, "r");
	if(f_gff == NULL)
	{
		printf("Could not open GFF file @ %s\n", gff_fp);
		exit(0);
	}

	char* cur_line = getline(f_gff);
	while(cur_line != NULL)
	{
		int start;
		int end;
		char chrom[100];
		char strand;

		bool skip_line = check_line_skip(cur_line);

		if(!skip_line)
		{
			if(sscanf(cur_line, "%s %*s %*s %d %d %*s %c", chrom, &start, &end, &strand) != 4)
			{
				printf("Could not parse GFF file entry line:\n%s\n", cur_line);
				exit(0);
			}

			// Allocate and initialize the new region.
			t_annot_region* new_region = new t_annot_region();
			new_region->start = start;
			new_region->end = end;
			new_region->chrom = new char[strlen(chrom) + 2];
			strcpy(new_region->chrom, chrom);
			new_region->strand = strand;

			// Add the new region to the list.
			gff_regions->push_back(new_region);
		}

		delete [] cur_line;
		cur_line = getline(f_gff);
	} // narrowPeak file reading loop.

	fclose(f_gff);

	printf("Loaded %d regions from %s\n", gff_regions->size(), gff_fp);
	return(gff_regions);
}

// Count the number of regions overlapping with regularly placed windows of size win_size along each chromosome.
// Return the region counts.
vector<int>* distribution(vector<t_annot_region*>* annot_regions, int win_size)
{
	return(NULL);
}

int coverage(vector<t_annot_region*>* annot_regions)
{
	int covrg = 0;

	for(unsigned int i_reg = 0; i_reg < annot_regions->size(); i_reg++)
	{
		if(annot_regions->at(i_reg) != NULL)
		{
			if(annot_regions->at(i_reg)->end < annot_regions->at(i_reg)->start)
			{
				printf("Region %d has its start index larger than its end index: %d, %d\n", i_reg, 
					annot_regions->at(i_reg)->start,
					annot_regions->at(i_reg)->end);
				exit(0);
			}

			covrg += annot_regions->at(i_reg)->end - annot_regions->at(i_reg)->start;
		}
	} // i_reg loop.

	return(covrg);
}

double nuc_overlap(vector<t_annot_region*>* annot_regions1, 
					vector<t_annot_region*>* annot_regions2)
{
	// Compute the coverage of overlap divided by the coverage of all regions.
	vector<t_annot_region*>* overlapping_regions = batch_intersect_annot_regions(annot_regions1, annot_regions2, false);

	int overlap_cvg = coverage(overlapping_regions);
	int total_cvg = coverage(annot_regions1) + coverage(annot_regions2);

	if(total_cvg == 0)
	{
		return 0.0;
	}

	return((double)(2 * overlap_cvg) / total_cvg);
}

double nuc_overlap_src_over_both_overlap(vector<t_annot_region*>* annot_regions1, 
										vector<t_annot_region*>* annot_regions2)
{
	// Compute the coverage of overlap divided by the coverage of only parts that overlapped.
	vector<t_annot_region*>* overlapping_regions = batch_intersect_annot_regions(annot_regions1, annot_regions2, false);

	// Get the overlapping regions in each list: Fix the indices: Batch intersection returns only the overlapping regions.
	vector<t_annot_region*>* overlapping_regions1 = batch_intersect_annot_regions(annot_regions1, annot_regions2, false);
	for(unsigned int i_reg = 0; i_reg < overlapping_regions1->size(); i_reg++)
	{
		if(overlapping_regions1->at(i_reg) != NULL)
		{
			overlapping_regions1->at(i_reg)->start = annot_regions1->at(i_reg)->start;
			overlapping_regions1->at(i_reg)->end = annot_regions1->at(i_reg)->end;
		}
	} // i_reg loop.

	vector<t_annot_region*>* overlapping_regions2 = batch_intersect_annot_regions(annot_regions2, annot_regions1, false);
	for(unsigned int i_reg = 0; i_reg < overlapping_regions2->size(); i_reg++)
	{
		if(overlapping_regions2->at(i_reg) != NULL)
		{
			overlapping_regions2->at(i_reg)->start = annot_regions2->at(i_reg)->start;
			overlapping_regions2->at(i_reg)->end = annot_regions2->at(i_reg)->end;
		}
	} // i_reg loop.

	int overlap_cvg = coverage(overlapping_regions);
	int total_cvg = coverage(overlapping_regions1) + coverage(overlapping_regions2);

	if(total_cvg == 0)
	{
		return 0.0;
	}
	printf("2 * %d / (%d + %d)\n", coverage(overlapping_regions), coverage(overlapping_regions1), coverage(overlapping_regions2));
	return((double)(2 * overlap_cvg) / total_cvg);
}


double nuc_overlap_src_over_src_overlap(vector<t_annot_region*>* annot_regions1, 
										vector<t_annot_region*>* annot_regions2)
{
	// Get the overlapping regions in each list: Fix the indices: Batch intersection returns only the overlapping regions.
	vector<t_annot_region*>* overlapping_regions1 = batch_intersect_annot_regions(annot_regions1, annot_regions2, false);
	int n_overlaps = 0;
	for(unsigned int i_reg = 0; i_reg < overlapping_regions1->size(); i_reg++)
	{
		if(overlapping_regions1->at(i_reg) != NULL)
		{
			//overlapping_regions1->at(i_reg)->start = annot_regions1->at(i_reg)->start;
			//overlapping_regions1->at(i_reg)->end = annot_regions1->at(i_reg)->end;
			n_overlaps++;
		}
	} // i_reg loop.

	//vector<t_annot_region*>* overlapping_regions2 = batch_intersect_annot_regions(annot_regions2, annot_regions1, false);

	//for(unsigned int i_reg = 0; i_reg < overlapping_regions2->size(); i_reg++)
	//{
	//	if(overlapping_regions2->at(i_reg) != NULL)
	//	{
	//		if(strcmp(overlapping_regions2->at(i_reg)->chrom, annot_regions2->at(i_reg)->chrom) != 0 ||
	//			overlapping_regions2->at(i_reg)->end > annot_regions2->at(i_reg)->end ||
	//			overlapping_regions2->at(i_reg)->start < annot_regions2->at(i_reg)->start)
	//		{
	//			printf("overlapping did not work!\n");
	//			exit(0);
	//		}

	//		overlapping_regions2->at(i_reg)->start = annot_regions2->at(i_reg)->start;
	//		overlapping_regions2->at(i_reg)->end = annot_regions2->at(i_reg)->end;
	//	}
	//} // i_reg loop.

	//int overlap_cvg = coverage(overlapping_regions);
	//int src_cvg = coverage(overlapping_regions1);

	int overlap_cvg = n_overlaps;
	int src_cvg = annot_regions1->size();

	//if(src_cvg == 0)
	//{
	//	return 0.0;
	//}
	printf("%d/%d\n", overlap_cvg, src_cvg);
	return((double)(overlap_cvg) / src_cvg);
}

// Make sure that there is no overlap between the regions in annot_region list.
vector<int>* inter_region_distances(vector<t_annot_region*>* annot_regions)
{
	vector<char*>* chr_ids = get_chr_ids(annot_regions);

	vector<int>* inter_reg_dists = new vector<int>();

	for(unsigned int i_chr = 0; i_chr < chr_ids->size(); i_chr++)
	{
		vector<t_annot_region*>* cur_chr_regs = get_regions_per_chromosome(annot_regions,
																			chr_ids->at(i_chr));

		// Dump the distances between consecutive regions.
		for(unsigned int i_reg = 0; (i_reg + 1) < cur_chr_regs->size(); i_reg++)
		{
			signed int cur_dist = cur_chr_regs->at(i_reg+1)->start - cur_chr_regs->at(i_reg)->end;
		
			if(cur_dist < 0)
			{
				printf("Negative distance: <%d-%d> and <%d-%d>\n", 
					cur_chr_regs->at(i_reg)->start,
					cur_chr_regs->at(i_reg)->end,
					cur_chr_regs->at(i_reg+1)->start,
					cur_chr_regs->at(i_reg+1)->end);
			}

			inter_reg_dists->push_back(cur_dist);
		} // i_reg loop.

		cur_chr_regs->clear();
		delete(cur_chr_regs);
	} // i_chr loop.

	return(inter_reg_dists);
}

bool check_line_skip(char* cur_line)
{
	// Skip empty lines.
	if(strlen(cur_line) == 0)
	{
		return(true);
	}

	// Check comment.
	if(cur_line[0] == '#')
	{
		return(true);
	}
	
	// Check track info line.
	char first_word[100];
	sscanf(cur_line, "%s", first_word);

	if(strcmp(first_word, "track") == 0)
	{
		return(true);
	}

	return(false);
}

// Load a narrowPeak file as an annotation file: Necessary for loading and processing ENCODE peak calls.
//vector<t_annot_region*>* load_GFF(char* gff_fp)
//{
//	printf("Loading GFF file %s\n", gff_fp);
//
//	vector<t_annot_region*>* gff_regions = new vector<t_annot_region*>();
//
//	FILE* f_gff = fopen(gff_fp, "r");
//	if(f_gff == NULL)
//	{
//		printf("Could not open GFF file @ %s\n", gff_fp);
//		exit(0);
//	}
//
//	char cur_line[1000];
//	while(fgets(cur_line, 1000, f_gff) != NULL)
//	{
//		int start;
//		int end;
//		char chrom[100];
//		char strand;
//
//		if(sscanf(cur_line, "%s %*s %*s %d %d %*s %c", chrom, &start, &end, &strand) != 4)
//		{
//			printf("Could not parse GFF file entry line:\n%s\n", cur_line);
//			exit(0);
//		}
//
//		// Check if the chromosomes id matches with the requested chromosome id.
//		// Allocate and initialize the new region.
//		t_annot_region* new_region = new t_annot_region();
//		new_region->start = start;
//		new_region->end = end;
//		new_region->chrom = new char[strlen(chrom) + 2];
//		strcpy(new_region->chrom, chrom);
//		new_region->strand = strand;
//
//		// Add the new region to the list.
//		gff_regions->push_back(new_region);
//	} // narrowPeak file reading loop.
//
//	fclose(f_gff);
//
//	printf("Loaded %d regions from %s\n", gff_regions->size(), gff_fp);
//	return(gff_regions);
//}

vector<t_annot_region*>* load_Interval(char* interval_fp)
{
	return(NULL);
}

void dump_Interval(char* interval_fp, vector<t_annot_region*>* annot_regions)
{
	FILE* f_Interval = open_f(interval_fp, "w");

//00021   tars = readTarsFromBedFile ("-");
//00022   for (i = 0; i < arrayMax (tars); i++) {
//00023     currTar = arrp (tars,i,Tar);
//00024     printf ("BED_%d\t%s\t.\t%d\t%d\t1\t%d\t%d\n",
//00025             i + 1,
					//currTar->targetName,
					//currTar->start,
					//currTar->end,
					//currTar->start,
					//currTar->end);
//00026   }
//00027   return 0;
//00028 }

	for(unsigned int i_reg = 0; i_reg < annot_regions->size(); i_reg++)
	{
/*
1.   Name of the interval
2.   Chromosome 
3.   Strand
4.   Interval start (with respect to the "+")
5.   Interval end (with respect to the "+")
6.   Number of sub-intervals
7.   Sub-interval starts (with respect to the "+", comma-delimited)
8.   Sub-interval end (with respect to the "+", comma-delimited) 
*/
		fprintf(f_Interval, "Interval_%d\t%s\t%c\t%d\t%d\t1\t%d\t%d\n", 
			i_reg, 
			annot_regions->at(i_reg)->chrom, 
			annot_regions->at(i_reg)->strand,
			annot_regions->at(i_reg)->start,
			annot_regions->at(i_reg)->end,			
			annot_regions->at(i_reg)->start,
			annot_regions->at(i_reg)->end);
	} // i_reg loop.

	fclose(f_Interval);
}

vector<t_annot_region*>* exclude_annot_regions(vector<t_annot_region*>* annot_regions1,
													vector<t_annot_region*>* annot_regions2,
													bool match_strands)
{
	vector<t_annot_region*>* excluded_annot_regions = new vector<t_annot_region*>();

	// Sort regions.
	sort(annot_regions1->begin(), annot_regions1->end(), sort_regions);
	sort(annot_regions2->begin(), annot_regions2->end(), sort_regions);

	unsigned int i2 = 0;
	unsigned int region_list1_size = annot_regions1->size();
	unsigned int region_list2_size = annot_regions2->size();

	for(unsigned int i1 = 0; i1 < region_list1_size; i1++)
	{
		//printf("Checking <%d-%d>\n", annot_regions1->at(i1)->start, annot_regions1->at(i1)->end);

		// Find the first region2 that is completely to the left of current region1.
		while(i2 != 0 &&
			annot_regions1->at(i1)->start < annot_regions2->at(i2)->end)
		{
			i2--;
			//printf("Backtracking %d\n", i2);
		}

		bool found_overlap = false; // Does that overlap with anything?
		while(i2 < region_list2_size &&
				annot_regions1->at(i1)->end > annot_regions2->at(i2)->start)
		{
			// Check overlap.
			int i_max = MIN(annot_regions1->at(i1)->end, annot_regions2->at(i2)->end);
			int i_min = MAX(annot_regions1->at(i1)->start, annot_regions2->at(i2)->start);
											
			// Check if there is overlap, compare the strands and chromosome ids if necessary.
			if(i_min < i_max && 
				strcmp(annot_regions1->at(i1)->chrom, annot_regions2->at(i2)->chrom) == 0 &&
				(!match_strands || annot_regions1->at(i1)->strand == annot_regions2->at(i2)->strand))
			{
				// Following would be better to return as 
				/*
				fprintf(stderr, "Overlapped <%d-%d>, <%d-%d>: <%d-%d>\n",
						annot_regions1->at(i1)->start, annot_regions1->at(i1)->end, 
						annot_regions2->at(i2)->start, annot_regions2->at(i2)->end,
						i_min, i_max);
				*/
				found_overlap = true;
				break; // Move to the next region1.
			} // Overlap size check.

			// Update i2 to move to the next region in region2 list. 
			// Make sure that i2 does not go out of range of region2 list, always keep it in the valid index limits.
			i2++;
		} // i2 loop.

		if(i2 == region_list2_size)
		{
			i2--;
		}

		// If this region1 is not overlapping with anything add it to excluded region list.
		if(!found_overlap)
		{
			t_annot_region* new_region = new t_annot_region();
			new_region->start = annot_regions1->at(i1)->start;
			new_region->end = annot_regions1->at(i1)->end;
			new_region->chrom = new char[strlen(annot_regions1->at(i1)->chrom) + 2];
			strcpy(new_region->chrom, annot_regions1->at(i1)->chrom);
			new_region->strand = annot_regions1->at(i1)->strand;
			new_region->data = annot_regions1->at(i1)->data; // Copy data, too.

			excluded_annot_regions->push_back(new_region);
		}
	} // i1 loop.

	fprintf(stderr, "%d excluded regions.\n", excluded_annot_regions->size());

	return(excluded_annot_regions);
}

vector<t_annot_region*>* batch_intersect_annot_regions(vector<t_annot_region*>* annot_regions1,
														vector<t_annot_region*>* annot_regions2,
														bool match_strands)
{
	vector<t_annot_region*>* intersected_annot_regions = NULL;

	// Merged annot. regions.
	if(match_strands)
	{
		printf("------------------------------------------------------\n");
		printf("Intersecting with strand information:\n");
		printf("------------------------------------------------------\n");
		// Merge plus strand regions.
		printf("Loading and intersecting (+) strands:\n");
		vector<t_annot_region*>* plus_strand_regions1 = get_regions_per_strand(annot_regions1,
																			'+');

		vector<t_annot_region*>* plus_strand_regions2 = get_regions_per_strand(annot_regions2,
																			'+');

		vector<t_annot_region*>* intersected_plus_strand_regions = batch_intersect_annot_regions(plus_strand_regions1, plus_strand_regions2);
		printf("%d (+) strand regions intersect.\n", intersected_plus_strand_regions->size());
		//printf("%c\n", merged_plus_strand_regions->at(0)->strand);

		// Merge negative strand regions.
		printf("------------------------------------------------------\n");
		//printf("Loading and intersecting (-) strands:\n");
		vector<t_annot_region*>* neg_strand_regions1 = get_regions_per_strand(annot_regions1,
																			'-');

		vector<t_annot_region*>* neg_strand_regions2 = get_regions_per_strand(annot_regions2,
																			'-');

		vector<t_annot_region*>* intersected_neg_strand_regions = batch_intersect_annot_regions(neg_strand_regions1, neg_strand_regions2);
		printf("%d (-) strand regions intersect.\n", intersected_neg_strand_regions->size());
		printf("------------------------------------------------------\n");

		// Pool the merged plus and minus strand regions.
		intersected_annot_regions = new vector<t_annot_region*>();
		for(unsigned int i_reg = 0; i_reg < intersected_plus_strand_regions->size(); i_reg++)
		{
			intersected_annot_regions->push_back(intersected_plus_strand_regions->at(i_reg));
		} // i_reg loop

		for(unsigned int i_reg = 0; i_reg < intersected_neg_strand_regions->size(); i_reg++)
		{
			intersected_annot_regions->push_back(intersected_neg_strand_regions->at(i_reg));
		} // i_reg loop

		intersected_plus_strand_regions->clear();
		delete(intersected_plus_strand_regions);

		intersected_neg_strand_regions->clear();
		delete(intersected_neg_strand_regions);
	}
	else
	{
		intersected_annot_regions = batch_intersect_annot_regions(annot_regions1, annot_regions2);
	}

	return(intersected_annot_regions);
}

/*
The backend function that does intersections in a strand non-specific manner, do not duplicate.
*/
vector<t_annot_region*>* batch_intersect_annot_regions(vector<t_annot_region*>* annot_regions1,
														vector<t_annot_region*>* annot_regions2)
{
	vector<t_annot_region*>* intersected_annot_regions = new vector<t_annot_region*>();

	// Merge annot_regions2 such that there is no overlap between any two regions.
	printf("Merging the region2 list before intersecting.\n");

	// Do the merging without respect to the strands; the strand specificity should be handled in the function that calls this function.
	vector<t_annot_region*>* merged_annot_regions2 = merge_annot_regions(annot_regions2, false);

	// Determine the chromosome ids from the total region list.
	vector<char*>* chr_ids = get_chr_ids(annot_regions1);
	printf("Found %d different chromosomes in regions list.\n", chr_ids->size());

	// Go over all the regions in total_annot_regions and merge necessary ones.
	for(unsigned int i_chr = 0; i_chr < chr_ids->size(); i_chr++)
	{
		vector<t_annot_region*>* cur_chr_annot1_regions = get_regions_per_chromosome(annot_regions1,
																				chr_ids->at(i_chr));


		vector<t_annot_region*>* cur_chr_annot2_regions = get_regions_per_chromosome(merged_annot_regions2,
																				chr_ids->at(i_chr));

		// Sort regions for the current chromosomes.
		sort(cur_chr_annot1_regions->begin(), cur_chr_annot1_regions->end(), sort_regions);
		sort(cur_chr_annot2_regions->begin(), cur_chr_annot2_regions->end(), sort_regions);

		unsigned int i2 = 0;
		unsigned int region_list1_size = cur_chr_annot1_regions->size();
		unsigned int region_list2_size = cur_chr_annot2_regions->size();

		bool cur_region1_overlaps = false;
		for(unsigned int i1 = 0; 
			region_list1_size > 0 &&
			region_list2_size > 0 &&
			i1 < region_list1_size; i1++)
		{
			cur_region1_overlaps = false;
			//printf("Checking <%d-%d>\n", annot_regions1->at(i1)->start, annot_regions1->at(i1)->end);

			// Find the first region2 that is completely to the left of current region1.
			while(i2 != 0 &&
				cur_chr_annot1_regions->at(i1)->start <= cur_chr_annot2_regions->at(i2)->end)
			{
				i2--;
				//printf("Backtracking %d\n", i2);
			}

			while(i2 < region_list2_size &&
					cur_region1_overlaps == false &&
					cur_chr_annot1_regions->at(i1)->end > cur_chr_annot2_regions->at(i2)->start)
			{
				// Check overlap.
				int i_max = MIN(cur_chr_annot1_regions->at(i1)->end, cur_chr_annot2_regions->at(i2)->end);
				int i_min = MAX(cur_chr_annot1_regions->at(i1)->start, cur_chr_annot2_regions->at(i2)->start);
											
				// Check if there is overlap, compare the strands and chromosome ids if necessary.
				if(i_min <= i_max)
				{
					/*
					// Following would be better to return as 
					fprintf(stderr, "Overlapped <%d-%d>, <%d-%d>: <%d-%d>\n",
							annot_regions1->at(i1)->start, annot_regions1->at(i1)->end, 
							annot_regions2->at(i2)->start, annot_regions2->at(i2)->end,
							i_min, i_max);
					*/

					t_annot_region* new_region = new t_annot_region();
					new_region->start = i_min;
					new_region->end = i_max;
					new_region->chrom = new char[strlen(cur_chr_annot1_regions->at(i1)->chrom) + 2];
					strcpy(new_region->chrom, cur_chr_annot1_regions->at(i1)->chrom);
					new_region->strand = cur_chr_annot1_regions->at(i1)->strand;

					// Set the data of region1 to the intersected region: This enables to keep track of which regions are intersected.
					//cur_chr_annot1_regions->at(i1)->data = (void*)new_region;
					new_region->data = (void*)cur_chr_annot1_regions->at(i1);

					cur_region1_overlaps = true; // Move to the next region1.

					intersected_annot_regions->push_back(new_region);
				} // Overlap size check.

				i2++;
			} // i2 loop.

			// Make sure i2 does not go out of range.
			if(i2 == cur_chr_annot2_regions->size())
			{
				i2--;
			}
		} // i1 loop.
	} // i_chr loop.

	for(unsigned int i_chr = 0; i_chr < chr_ids->size(); i_chr++)
	{
		delete [] chr_ids->at(i_chr);
	} // i_chr loop.

	for(unsigned int i_reg = 0; i_reg < merged_annot_regions2->size(); i_reg++)
	{
		delete [] merged_annot_regions2->at(i_reg)->chrom;
		delete(merged_annot_regions2->at(i_reg));
	} // i_reg loop.
	delete(merged_annot_regions2);

	return(intersected_annot_regions);
}


//vector<t_annot_region*>* exclude_annot_regions(vector<t_annot_region*>* annot_regions1,
//											vector<t_annot_region*>* annot_regions2,
//											bool match_strands) // Maximum distance between two annotated regions that are in the merged region.
//{
//	// Find the set of regions in region1 list that overlap with a region in region2 list.
//	vector<t_annot_region*>* intersecting_regions = hard_intersect_annot_regions(annot_regions1, annot_regions2, match_strands);
//	printf("Intersected %d region, excluding the intersecting regions from region1 list, %d, %d.\n", intersecting_regions->size(), annot_regions1->size(), annot_regions2->size());
//	getc(stdin);
//
//	vector<t_annot_region*>* excluded_regions = new vector<t_annot_region*>();
//	for(unsigned int i_region = 0; i_region < annot_regions1->size(); i_region++)
//	{
//		// if there is a region that intersects with annot_region2, do not add it to excluded annot regions.
//		bool intersects = false;
//		for(unsigned int i_int_region = 0; !intersects && i_int_region < intersecting_regions->size(); i_int_region++)
//		{
//			if(annot_regions1->at(i_region)->start <= intersecting_regions->at(i_int_region)->start &&
//				annot_regions1->at(i_region)->end >= intersecting_regions->at(i_int_region)->end &&
//				strcmp(annot_regions1->at(i_region)->chrom, intersecting_regions->at(i_int_region)->chrom) == 0 && // Make sure that the regions are on same chromosome.
//				(!match_strands || annot_regions1->at(i_region)->strand == intersecting_regions->at(i_int_region)->strand)) 
//			{
//				intersects = true;
//			}
//		} // i_int_reg loop.
//
//		if(!intersects)
//		{
//			excluded_regions->push_back(annot_regions1->at(i_region));
//		}
//	} // i_region loop.
//
//	for(unsigned int i_int = 0; i_int < intersecting_regions->size(); i_int++)
//	{
//		delete [] intersecting_regions->at(i_int)->chrom;
//		delete(intersecting_regions->at(i_int));
//	} // i_int loop.
//	delete(intersecting_regions);
//
//	return(excluded_regions);
//}

// Get all the chromosome ids from annotated regions.
vector<char*>* get_chr_ids(vector<t_annot_region*>* annot_regions)
{
	printf("Determining the chromosome list from region list.\n");
	vector<char*>* chr_ids = new vector<char*>();

	for(unsigned int i_reg = 0; i_reg < annot_regions->size(); i_reg++)
	{
		bool new_id = true;

		// Check if the the current chromosome id is included in the list of chromosome ids already collected, otherwise add it.
		for(unsigned int i_chr = 0; 
			new_id && i_chr < chr_ids->size(); 
			i_chr++)
		{
			if(strcmp(chr_ids->at(i_chr), annot_regions->at(i_reg)->chrom) == 0)
			{
				new_id = false;
			}
		} // i_chr loop

		if(new_id)
		{
			char* new_id = new char[strlen(annot_regions->at(i_reg)->chrom) + 2];
			strcpy(new_id, annot_regions->at(i_reg)->chrom);
			chr_ids->push_back(new_id); // Add the new id.
		}
	} // i_reg loop.

	printf("Determined %d chromosomes from region list.\n", chr_ids->size());
	return(chr_ids);
}

vector<t_annot_region*>* get_regions_per_strand(vector<t_annot_region*>* annot_regions,
													char strand)
{
	vector<t_annot_region*>* chr_regions = new vector<t_annot_region*>();

	for(unsigned int i_reg = 0; i_reg < annot_regions->size(); i_reg++)
	{
		bool new_id = false;

		// Check if the the current chromosome id is included in the list of chromosome ids already collected, otherwise add it.
		if(annot_regions->at(i_reg)->strand == strand)
		{
			// Add the current region to the chromosome regions.
			chr_regions->push_back(annot_regions->at(i_reg));
		}
	} // i_reg loop.

	return(chr_regions);
}

vector<t_annot_region*>* get_regions_per_chromosome(vector<t_annot_region*>* annot_regions,
													char* chr_id)
{
	vector<t_annot_region*>* chr_regions = new vector<t_annot_region*>();

	for(unsigned int i_reg = 0; i_reg < annot_regions->size(); i_reg++)
	{
		bool new_id = false;

		// Check if the the current chromosome id is included in the list of chromosome ids already collected, otherwise add it.
		if(strcmp(annot_regions->at(i_reg)->chrom, chr_id) == 0)
		{
			// Add the current region to the chromosome regions.
			chr_regions->push_back(annot_regions->at(i_reg));
		}
	} // i_reg loop.

	return(chr_regions);
}

// Merge the regions in the regions list. This is the backend function for all the merging functions.
// Merges the regions irrespective of the chromosome id list. Build the list on the fly.
vector<t_annot_region*>* merge_annot_regions(vector<t_annot_region*>* total_annot_regions,
											int max_gap, 
											bool match_strands) // Maximum distance between two annotated regions that are in the merged region.
{
	printf("Merging %d regions.\n", total_annot_regions->size());

	fprintf(stderr, "All region list contains %d regions.\n", total_annot_regions->size());

	vector<t_annot_region*>* merged_annot_regions = NULL;

	// Merged annot. regions.
	if(match_strands)
	{
		printf("Merging with strand information.");
		// Merge plus strand regions.
		vector<t_annot_region*>* plus_strand_regions = get_regions_per_strand(total_annot_regions,
																			'+');
		printf("%d plus strand regions.\n", plus_strand_regions->size());
		vector<t_annot_region*>* merged_plus_strand_regions = merge_annot_regions(plus_strand_regions, max_gap);

		// Merge negative strand regions.
		vector<t_annot_region*>* neg_strand_regions = get_regions_per_strand(total_annot_regions,
																			'-');
		printf("%d negative strand regions.\n", neg_strand_regions->size());
		vector<t_annot_region*>* merged_neg_strand_regions = merge_annot_regions(neg_strand_regions, max_gap);

		// Pool the merged plus and minus strand regions.
		merged_annot_regions = new vector<t_annot_region*>();
		for(unsigned int i_reg = 0; i_reg < merged_plus_strand_regions->size(); i_reg++)
		{
			merged_annot_regions->push_back(merged_plus_strand_regions->at(i_reg));
		} // i_reg loop

		for(unsigned int i_reg = 0; i_reg < merged_neg_strand_regions->size(); i_reg++)
		{
			merged_annot_regions->push_back(merged_neg_strand_regions->at(i_reg));
		} // i_reg loop

		merged_plus_strand_regions->clear();
		delete(merged_plus_strand_regions);

		merged_neg_strand_regions->clear();
		delete(merged_neg_strand_regions);
	}
	else
	{
		 merged_annot_regions = merge_annot_regions(total_annot_regions, max_gap);
	}

	printf("Merged into %d regions\n", merged_annot_regions->size());
	return(merged_annot_regions);
}


// Merge the regions in the regions list. This is the backend function for all the merging functions.
// Merges the regions irrespective of the chromosome id list. Build the list on the fly.
vector<t_annot_region*>* merge_annot_regions(vector<t_annot_region*>* total_annot_regions,
											int max_gap) // Maximum distance between two annotated regions that are in the merged region.
{
	printf("Merging %d regions.\n", total_annot_regions->size());

	// Determine the chromosome ids from the total region list.
	vector<char*>* chr_ids = get_chr_ids(total_annot_regions);
	printf("Found %d different chromosomes in regions list.\n", chr_ids->size());

	fprintf(stderr, "All region list contains %d regions.\n", total_annot_regions->size());

	// Merged annot. regions.
	vector<t_annot_region*>* merged_annot_regions = new vector<t_annot_region*>();

	// Go over all the regions in total_annot_regions and merge necessary ones.
	for(unsigned int i_chr = 0; i_chr < chr_ids->size(); i_chr++)
	{
		vector<t_annot_region*>* cur_chr_regions = get_regions_per_chromosome(total_annot_regions,
																				chr_ids->at(i_chr));

        sort(cur_chr_regions->begin(), cur_chr_regions->end(), sort_regions);

		if(cur_chr_regions->size() > 0)
		{
			//printf("Loaded %d regions in %s\n", cur_chr_regions->size(), chr_ids->at(i_chr));

			unsigned int i = 0;
			int cur_start = cur_chr_regions->at(i)->start;
			int cur_end = cur_chr_regions->at(i)->end;

			// Find the next read that is to be merged in this chromosome.
			while(i < cur_chr_regions->size())
			{
				// Is this not the last region in the region list?
				if(i+1 < cur_chr_regions->size())
				{
					// Check max_gap condition for merging: Start of the next region should be at most max_gap nucs away from end of current region.
					if(cur_chr_regions->at(i+1)->start <= (cur_end + max_gap))
					{
						//printf("Merging: <%d-%d>\n", cur_chr_regions->at(i+1)->start, cur_chr_regions->at(i+1)->end);
						// Update cur_end, if the end of next region is passing cur_end, update cur_end, otherwise this region is a subset of previous regions.
						cur_end = (cur_end < cur_chr_regions->at(i+1)->end)?(cur_chr_regions->at(i+1)->end):cur_end;
					}
					else
					{
						// Add the region that has accumulated so far.
						t_annot_region* new_region = new t_annot_region();
						new_region->start = cur_start;
						new_region->end = cur_end;
						new_region->chrom = new char[strlen(cur_chr_regions->at(i)->chrom) + 2];
						strcpy(new_region->chrom, cur_chr_regions->at(i)->chrom);
						new_region->strand = cur_chr_regions->at(i)->strand;

						merged_annot_regions->push_back(new_region);
						//printf("Adding merged region <%d-%d>.\n", merged_annot_regions->back()->start, merged_annot_regions->back()->end);

						// Set the current start to start of next region.
						cur_start = cur_chr_regions->at(i+1)->start;
						cur_end = cur_chr_regions->at(i+1)->end;
						//printf("Initiated a new region @ <%d-.>\n", cur_start);
					}
				}
				else
				{
					// This is the last region. Set the max to its ending and add the last merged region.
					t_annot_region* new_region = new t_annot_region();
					new_region->start = cur_start;
					new_region->end = cur_chr_regions->at(i)->end;
					new_region->chrom = new char[strlen(cur_chr_regions->at(i)->chrom) + 2];
					strcpy(new_region->chrom, cur_chr_regions->at(i)->chrom);
					new_region->strand = cur_chr_regions->at(i)->strand;

					merged_annot_regions->push_back(new_region);

					//fprintf(stderr, "Adding merged region <%d-%d>.\n", merged_annot_regions->back()->start, merged_annot_regions->back()->end);
					break;
				}

				i++;
			} // i loop 
		} // region size check to make sure that there are regions in this chromosome.

		// Delete the regions for current chromosome. And move to the next one.
		cur_chr_regions->clear();
		delete(cur_chr_regions);
	} // chr check.

	printf("Merged into %d regions\n", merged_annot_regions->size());
	return(merged_annot_regions);
}

/*
The chromosome and strand matching are not checked, yet.
*/
vector<t_annot_region*>* merge_annot_regions(vector<t_annot_region*>* annot_regions1,
											vector<t_annot_region*>* annot_regions2,
											int max_gap, 
											bool match_strands) // Maximum distance between two annotated regions that are in the merged region.
{
	fprintf(stderr, "Merging %d versus %d regions.\n", annot_regions1->size(), annot_regions2->size());

	vector<t_annot_region*>* total_annot_regions = new vector<t_annot_region*>();
	for(unsigned int i = 0; i < annot_regions1->size(); i++)
	{
		total_annot_regions->push_back(annot_regions1->at(i));
	} // i loop.

	for(unsigned int i = 0; i < annot_regions2->size(); i++)
	{
		total_annot_regions->push_back(annot_regions2->at(i));
	} // i loop.

	vector<t_annot_region*>* merged_annot_regions = merge_annot_regions(total_annot_regions,
																			max_gap, 
																			match_strands);

	delete(total_annot_regions);

	return(merged_annot_regions);
}

vector<t_annot_region*>* merge_annot_regions(vector<vector<t_annot_region*>*>* annot_regions_list,
												int max_gap, 
												bool match_strands)
{
	vector<t_annot_region*>* total_annot_regions = new vector<t_annot_region*>();
	for(unsigned int i_reg_list = 0; i_reg_list < annot_regions_list->size(); i_reg_list++)
	{
		vector<t_annot_region*>* cur_annot_regions = annot_regions_list->at(i_reg_list);
		for(unsigned int i = 0; i < cur_annot_regions->size(); i++)
		{
			total_annot_regions->push_back(cur_annot_regions->at(i));
		} // i loop.
	} // i_reg_list loop.

	vector<t_annot_region*>* merged_annot_regions = merge_annot_regions(total_annot_regions,
																			max_gap, 
																			match_strands);

	delete(total_annot_regions);

	return(merged_annot_regions);
}

vector<t_annot_region*>* sort_regions_per_chromosome_per_strand(vector<t_annot_region*>* annot_region_list)
{
	vector<t_annot_region*>* sorted_region_list = new vector<t_annot_region*>();

	// No mallocation in following.
	vector<t_annot_region*>* pos_strand_regions = get_regions_per_strand(annot_region_list, '+');

	vector<char*>* chr_ids = get_chr_ids(annot_region_list);

	for(unsigned int i_chr = 0; i_chr < chr_ids->size(); i_chr++)
	{
		vector<t_annot_region*>* cur_chr_regions = get_regions_per_chromosome(pos_strand_regions, chr_ids->at(i_chr));

		sort(cur_chr_regions->begin(), cur_chr_regions->end(), sort_regions);

		for(unsigned int i_reg = 0; i_reg < cur_chr_regions->size(); i_reg++)
		{
			sorted_region_list->push_back(cur_chr_regions->at(i_reg));
		}
	} // i_chr loop.
	delete(pos_strand_regions);

	vector<t_annot_region*>* neg_strand_regions = get_regions_per_strand(annot_region_list, '-');
	for(unsigned int i_chr = 0; i_chr < chr_ids->size(); i_chr++)
	{
		vector<t_annot_region*>* cur_chr_regions = get_regions_per_chromosome(neg_strand_regions, chr_ids->at(i_chr));

		sort(cur_chr_regions->begin(), cur_chr_regions->end(), sort_regions);

		for(unsigned int i_reg = 0; i_reg < cur_chr_regions->size(); i_reg++)
		{
			sorted_region_list->push_back(cur_chr_regions->at(i_reg));
		}
	} // i_chr loop.
	delete(neg_strand_regions);

	// Delete chr_ids.
	for(unsigned int i_chr = 0; i_chr < chr_ids->size(); i_chr++)
	{
		delete [] chr_ids->at(i_chr);
	}
	delete(chr_ids);

	return(sorted_region_list);
}

// Necessary for fast comparison of the regions.
bool sort_regions(t_annot_region* region1, t_annot_region* region2)
{
	if(region1->start < region2->start)
	{
		return(true);
	}
	else if(region1->start == region2->start)
	{
		if(region1->end < region2->end)
		{
			return(true);
		}
		else
		{
			return(false);
		}
	}
	else
	{
		return(false);
	}
}

/*
Clean a list of annotated regions.
*/
void delete_annot_regions(vector<t_annot_region*>* region_list)
{
	for(unsigned int i_reg = 0; i_reg < region_list->size(); i_reg++)
	{
		if(region_list->at(i_reg) != NULL)
		{
			delete [] region_list->at(i_reg)->chrom;
			delete(region_list->at(i_reg));
		}
	} // i_reg loop.

	region_list->clear();
	delete(region_list);
}
