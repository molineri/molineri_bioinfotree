#ifndef __ANNOT_REGION_TOOLS__
#define  __ANNOT_REGION_TOOLS__

#include <vector>

using namespace std;

// This is just a region on a chromosome.
struct t_annot_region
{
	char* chrom;
	char strand;
	int start; // If strand is '-', this coordinate is with respect to reverse complement on the forward strand.
	int end;
	void* data; // Extra data about the region, this is a pointer to a data structure that holds specific data about the region depending on the loaded data type: narrowPeak, GFF, ... Can also be used for other purposes for storing data about a region.
};

/* 
This is a re-structured representation of a list of regions which holds three different 
lists that do not re-allocate but divides the regions, these lists can be used directly in operations like overlapping,
merging, ... because they are directly comparable.
*/
struct t_sorted_annot_region_lists
{
	vector<t_annot_region*>** regions_per_chrom;
	vector<t_annot_region*>** pos_strand_regions_per_chrom;
	vector<t_annot_region*>** neg_strand_regions_per_chrom;
	vector<char*>* chr_ids; // These are the ids that have smae ordering in the region lists.
};

// Following is the extra information that is stored in a narrowPeak file. This information is stored for each narrowPeak entry.
#define NARROWPEAK_INFO (0x1234)
struct t_narrowPeak_info
{
	int info_type;
	int score;
	char strand;
	double signal_value;
	double p_value;
	double q_value;
	int peak_pos; // This is usually important. This is where the peak is.
};

int get_i_chr(vector<char*>* chr_ids, char* chrom_id);
t_sorted_annot_region_lists* restructure_annot_regions(vector<t_annot_region*>* regions); // Re-structure the list using the chromosome ids retreived from itself.
t_sorted_annot_region_lists* restructure_annot_regions(vector<t_annot_region*>* regions, vector<char*>* chr_ids); // Re-structure the list for a given list of chromosome ids.

vector<t_annot_region*>* load_BED(char* bed_fp);
void dump_BED(char* bed_fp, vector<t_annot_region*>* annot_regions);
vector<t_annot_region*>* load_Interval(char* interval_fp);
void dump_Interval(char* interval_fp, vector<t_annot_region*>* annot_regions);
vector<t_annot_region*>* load_narrowPeak(char* narrowPeak_fp); // This function also loads the broadPeak files.
vector<t_annot_region*>* load_GFF(char* narrowPeak_fp);

// Divide into strands first, then divide into chromosomes, then sort.
vector<t_annot_region*>* sort_regions_per_chromosome_per_strand(vector<t_annot_region*>* annot_region_list);

void delete_annot_regions(vector<t_annot_region*>* region_list);

// Get the list of interregion distances for a list of regions. 
vector<int>* inter_region_distances(vector<t_annot_region*>* annot_regions);

bool check_line_skip(char* cur_line);

vector<char*>* get_chr_ids(vector<t_annot_region*>* annot_regions);

vector<t_annot_region*>* get_regions_per_chromosome(vector<t_annot_region*>* annot_regions,
													char* chr_id);

vector<t_annot_region*>* get_regions_per_strand(vector<t_annot_region*>* annot_regions,
													char strand);

int coverage(vector<t_annot_region*>* annot_regions);

double nuc_overlap_src_over_both_overlap(vector<t_annot_region*>* annot_regions1, vector<t_annot_region*>* annot_regions2);
double nuc_overlap(vector<t_annot_region*>* annot_regions1, vector<t_annot_region*>* annot_regions2);

double nuc_overlap_src_over_src_overlap(vector<t_annot_region*>* annot_regions1, 
											vector<t_annot_region*>* annot_regions2);

/*
Following intersects all the regions in regions1 list to the regions in regions2 list and
returns the overlapping regions in regions1 list: One overlapping region is returned for each region
in regions1 list; NULL is returned for non-overlapping regions.
*/
vector<t_annot_region*>* batch_intersect_annot_regions(vector<t_annot_region*>* annot_regions1,
														vector<t_annot_region*>* annot_regions2,
														bool match_strands);

vector<t_annot_region*>* batch_intersect_annot_regions(vector<t_annot_region*>* annot_regions1,
													vector<t_annot_region*>* annot_regions2);

// Backend function that does all the merge'ing: No strand specificity.
vector<t_annot_region*>* merge_annot_regions(vector<t_annot_region*>* total_annot_regions,
											int max_gap);

// Following function is strand spceific inmplementation, utilizes above backend function.
vector<t_annot_region*>* merge_annot_regions(vector<t_annot_region*>* total_annot_regions,
											int max_gap, 
											bool match_strands);
							
// Two region list version.
vector<t_annot_region*>* merge_annot_regions(vector<t_annot_region*>* annot_regions1,
												vector<t_annot_region*>* annot_regions2,
												int max_gap, 
												bool match_strands);

// Multi region list version.
vector<t_annot_region*>* merge_annot_regions(vector<vector<t_annot_region*>*>* annot_regions_list,
												int max_gap, 
												bool match_strands);

vector<t_annot_region*>* exclude_annot_regions(vector<t_annot_region*>* annot_regions1,
											vector<t_annot_region*>* annot_regions2,
											bool match_strands);

// Dump the distances between the regions. This gives an overall statistic about how dispersed the data is.
vector<int>* inter_region_distances(vector<t_annot_region*>* annot_regions);

void dump_inter_region_distances(vector<t_annot_region*>* annot_regions1,
									vector<t_annot_region*>* annot_regions2);


// Necessary for fast comparison of the regions.
bool sort_regions(t_annot_region* region1, t_annot_region* region2);


#endif // __ANNOT_REGION_TOOLS__



