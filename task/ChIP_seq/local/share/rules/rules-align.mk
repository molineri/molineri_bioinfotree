
.SECONDARY:

genome.1.ebwt: 
	bowtie-build -f $(shell ls $(SEQ_DIR)/chr*.fa | grep -v random | grep -v hap | tr "\n" "," | sed 's/,$$//' ) genome

#report only alignment with at most 2 mismatches (-v)
#only single match -m 1 (options that seems sensible reading the Yale track notes on UCSC)
#-p 4 uses 4 cores
%.sam: $(FASTQ_DIR)/%.fastq.gz genome.1.ebwt
	bowtie genome -S -p $(CORES) -v 2 -m 1 <(gunzip -c $<) > $@

%.txt: %.o
	touch $@


PROVA1 := wgEncodeYaleChIPseqRawDataRep1Gm19099InputIggrab wgEncodeYaleChIPseqRawDataRep1Gm19099NfkbIggrab wgEncodeYaleChIPseqRawDataRep2Gm19099NfkbIggrab wgEncodeYaleChIPseqRawDataRep3Gm19099NfkbIggrab

.PHONY: prova

prova:
	for i in $(PROVA1); do \
		bmake $${i}.sam; \
	done;

