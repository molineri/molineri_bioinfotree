DOWNLOAD_FTP_PREFIX := ftp://hgdownload.cse.ucsc.edu/goldenPath/$(UCSC_VERSION)/encodeDCC
DOWNLOAD_HTTP_PREFIX := http://hgdownload.cse.ucsc.edu/goldenPath/$(UCSC_VERSION)/encodeDCC

ALL+=peaks.gz
ALL+=annotation

PEAKS_DIR=peaks
.PHONY: all_narrow_peak all_liftover all_seq


peaks_files.mk:
	(for i in $(DATASETS); do\
		wget -O - $(DOWNLOAD_HTTP_PREFIX)/$$i/ | perl -lne "if(m/HREF=\"(.+Peak\.gz)\"/i){print $$i . \"/\" . \$$1;}";\
	done;) \
	| tr "\n" " " \
	| append_each_row -B 'PEAKS_FILES = ' > $@

include peaks_files.mk

.SECONDARY:

$(PEAKS_DIR)/%Peak.gz:
	mkdir -p `dirname $@`
	wget -c -O $(PEAKS_DIR)/$*Peak.gz $(DOWNLOAD_FTP_PREFIX)/$*Peak.gz;

#downloaded the 05/08/2011
download_all_peaks: $(addprefix $(PEAKS_DIR)/,$(PEAKS_FILES))
	@echo done

.META: $(PEAKS_DIR)/*Peaks.gz 
	1	chrom 	chr1	
	2	chromStart 	
	3	chromEnd 	
	4	name 	.	
	5	score 	385	
	6	strand 	.	
	7	signalValue 	
	8	pValue 	3.58603	
	9	qValue 	2.44654	
	10	peak 	0	

.DOC: $(PEAKS_DIR)/*Peak.gz
	in un file di esempio il minimo log10(fdr) (qValue) e` 3.46

annotation: $(TASK_ROOT)/local/share/data/pwm.encode_ab_uniprot_names.xls
	(for i in $(DATASETS); do\
		wget -O - $(DOWNLOAD_HTTP_PREFIX)/$$i/files.txt | perl -ne 'if(m/^(.+Peak.gz)\s/) { print $$1; m/cell=([^\s]+);/; print "\t".$$1; m/treatment=([^\s]+);/; print "\t".$$1; m/antibody=([^\s]+);/; print "\t".$$1."\n"; }'; \
	done;) \
	| translate -a -d -v -j <(xls2tab $< | bawk '$$3 != "" {print $$2,$$1,$$4,$$3}' | trim_all_cols) 4 > $@

.META: annotation
	1 	peak_file      wgEncodeHaibTfbsA549Ctcfsc5916Pcr1xDex100nmPkRep1.broadPeak.gz
	2      	cell_line	A549
	3       treatment	DEX_100nM
	4       tf	CTCF_(SC-5916)
	5       uniprot_id	P49711
	6	jaspar_name	CTCF
	7	jaspar_id	MA0139.1

peaks.gz: $(addprefix  $(PEAKS_DIR)/,$(PEAKS_FILES))
	(for i in $^; do\
		zcat $$i | sed 's/^chr/\t/' | append_each_row -B `basename $$i`;\
	done;) \
	| bsort -k 2,2 -k3,3n -S50% \
	| gzip > $@

.META: peaks.gz
	1	filename
	2	chr 	1	
	3	b
	4	e 	
	5	name 	.	
	6	score 	385	
	7	strand 	.	
	8	signalValue 	
	9	pValue 	3.58603	
	10	qValue 	2.44654	
	11	peak 	0	
	

peaks.annot.gz: peaks.gz annotation
	zcat $< \
	| cut -f -10 \           * tolgo l'ultima colonna che non tutti hanno*
	| translate -v -a -d -j  $^2 1 \
	| gzip >$@

.META: peaks.annot.gz
	1	filename	wgEncodeHaibTfbsPfsk1Pol24h8V0416101PkRep2.broadPeak.gz
	2	cell_line	PFSK-1
	3	treatment	None
	4	tf		Pol2-4H8
	5	uniprot_id	
	6	jaspar_name	
	7	jaspar_id	
	8	chr		1
	9	b		9840
	10	e		10728
	11	name		peak1
	12	score		958
	13	strand		.
	14	signalValue	3100.000
	15	pValue		-1
	16	qValue		-1

