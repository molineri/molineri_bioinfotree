DOWNLOAD_HTTP_PREFIX := http://hgdownload.cse.ucsc.edu/goldenPath/hg18/encodeDCC/wgEncodeYaleChIPseq/
DOWNLOAD_FTP_PREFIX := ftp://hgdownload.cse.ucsc.edu/goldenPath/hg18/encodeDCC/wgEncodeYaleChIPseq/

FASTQ=fastq

.PHONY: nfkb_fastq controls

.SECONDARY:

#downloaded the 20/09/2011
nfkb_fastq:
	for i in `wget -O - $(DOWNLOAD_HTTP_PREFIX) | perl -lne 'if(m/HREF="(.+Nfkb.+fastq.gz)"/){print $$1;}'`; do\
		echo $$i;\
		wget $(DOWNLOAD_FTP_PREFIX)$$i;\
	done;
	mkdir -p $(FASTQ)
	mv *.fastq.gz $(FASTQ)


nfkb_annotation: 
	wget -O - $(DOWNLOAD_HTTP_PREFIX) |  perl -ne 'if(m/HREF="(.+Nfkb.+fastq.gz)"/) { print $$1; m/cell=([^\s]+);/; print "\t".$$1; m/treatment=([^\s]+);/; print "\t".$$1; m/antibody=([^\s]+);/; print "\t".$$1; m/controlId=([^\s]+);/; print "\t".$$1."\n"; }' > $@
	wget -O - $(DOWNLOAD_HTTP_PREFIX)files.txt > files.txt
	
.META:
	1      	peak_file	wgEncodeYaleChIPseqPeaksGm10847NfkbIggrab.narrowPeak.gz
	2       cell_line	GM10847
	3       treatment	none
	4       tf		NFKB
	5	controlId	GM18526/None/Input/IgG-mus


controls: nfkb_annotation files.txt
	for i in `cut -f 1 $^2 | grep Input | grep fastq | grep Gm`; do\
		wget -O - $(DOWNLOAD_HTTP_PREFIX)$$i > $(FASTQ)/$$i;\
	done;
