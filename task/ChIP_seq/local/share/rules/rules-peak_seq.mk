
.SECONDARY:
chr_id_list.txt: 
	ls $(SEQ_DIR)/chr*.fa | grep -v random | grep -v hap | perl -ne '/^.+(chr[\d\w]+)\.fa/; print $$1."\n";'> $@

#./PeakSeq -preprocess SAM chr_id_list.txt stdin Pol2_ChIP/
#chr_id_list.txt is the list of chromosomes to be processed. These must match chromosome ids in the mapped read file.
%_preprocessed: chr_id_list.txt ../align/%.sam
	mkdir -p $@
	PeakSeq -preprocess SAM $<  $^2 $@

#./PeakSeq -peak_select config.dat

PROVA1 := wgEncodeYaleChIPseqRawDataRep1Gm19099InputIggrab wgEncodeYaleChIPseqRawDataRep1Gm19099NfkbIggrab wgEncodeYaleChIPseqRawDataRep2Gm19099NfkbIggrab wgEncodeYaleChIPseqRawDataRep3Gm19099NfkbIggrab

.PHONY: prova

prova:
	for i in $(PROVA1); do \
		bmake $${i}_preprocessed; \
	done;

#what version is this?
mappability_map:	
	wget http://archive.gersteinlab.org/proj/PeakSeq/Mappability_Map/H.sapiens/Mapability_HG.txt
	mv Mapability_HG.txt $@


#atomic! what to do (without ; / everywhere)
%_peaks: chr_id_list.txt $(PEAK_CONFIG) mappability_map
	mkdir -p data_$*
	mkdir -p input_$*
	sed "s/EFFEDIERRE/$(FDR)/; s/Q_CUTOFF/$(Q_CUTOFF)/; s/MAPPABILITY/$^3/; s/CHR_ID_LIST/$</; s/EXP_ID/$*/; s/INPUT_DIR/input_$*/; s/DATA_DIR/data_$*/ " < $^2 > $*.config.dat
	cp $(word 1,$(PROVA1))_preprocessed/* input_$*/
	for d in $(wordlist 2, $(words $(PROVA1)),$(PROVA1)); do \
		cp $${d}_preprocessed/* data_$*/;\
	done;	
	PeakSeq -peak_select $*.config.dat
	touch $@

#rm -rf $*.config.dat
#rm -rf data_$*
#rm -rf input_$*
