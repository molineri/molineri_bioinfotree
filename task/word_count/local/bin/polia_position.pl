#!/usr/bin/perl
use warnings;
use strict;
$,="\t";
$\="\n";

my $i=0;
my @G=();
my @occurrencies=();
while(<>){
	chomp;
	my @F=split;
	push @F,$i;
	push @G,\@F;
	push @occurrencies, $F[1];

	
	@G = map {${$_}[0]=~s/[^A]//g; $_} @G;
	@G = sort { length(${$b}[0])<=>length(${$a}[0]) } @G;
	
	$i++;
}
	
print length(${$G[0]}[0]), ${$G[0]}[2], ${$G[0]}[1], &avg(@occurrencies), &median(@occurrencies);

sub avg
{
	my $sum=0;
	for(@_){
		$sum+=$_;
	}
	return $sum/scalar(@_);
}
sub median
{
	@_ = sort {$a<=>$b} @_;
	return $_[int(scalar(@_)/2)];
}

