#!/usr/bin/perl
use warnings;
use strict;
$,="\t";

my @char=('A','T','G','C');

my %words=();
 
while(<>){
	chomp;
	my ($w,$c)=split;
	my $n_ref=&neighbour($w);
	foreach my $wn  (@{$n_ref}){
		if(defined($words{$wn})){
			$words{$wn}+=$c;
		}else{
			$words{$wn}=$c;
		}
	}
}

$\="\n";
for(keys %words){
	print $_,$words{$_};
}



sub neighbour
{
	my $w=shift @_;
	my @out;
	push @out,$w;
	my @ws = split(//, $w);

	my $k=length($w); 
	for(my $i=0; $i<$k; $i++){
		foreach my $c (@char){
			next if $ws[$i] eq $c;
			my @w1=@ws;
			$w1[$i]=$c;
			push @out,join('',@w1);
		}
	}
	return \@out;
}
