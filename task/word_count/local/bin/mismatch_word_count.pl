#!/usr/bin/perl
use warnings;
use strict;
use Algorithm::Combinatorics qw(variations_with_repetition);
$,="\t";

my $k=shift @ARGV;
my @char=('A','T','G','C');
my @charcount=();
my %words=();
my %neighbour=();
 
my $iter=variations_with_repetition(\@char,$k);
while (my $w = $iter->next) {
	$neighbour{join('',@{$w})} = &build_neighbour(@{$w});
}

#for(keys %neighbour){
#	print @{$neighbour{$_}};
#}
#die;

print STDERR "comincio a leggere il file\n";

$_=<>;
chomp;
my $i=0;
my $j=0;
while( 1 ){
	
	if(length($_) < $k){
		my $new_row=<>;
		$j++;
		$i++;
		#print STDERR length($_),$new_row;
		if(defined($new_row)){
			chomp $new_row;
			$_.=$new_row;
		}else{
			last;
		}
	}

	my $w=substr($_,0,$k);
	substr($_,0,1,'');
	next if $w!~/^[ACGT]+$/;
	foreach my $wn  (@{$neighbour{$w}}){
		$words{$wn}++;
	}

	if($i>1000){
		$i=0;
		print STDERR "\r",$j/4120830;
	}

}

$\="\n";
for(keys %words){
	print $_,$words{$_};
}



sub build_neighbour
{
	my @out;
	push @out,join('',@_);
	for(my $i=0; $i<$k; $i++){
		foreach my $c (@char){
			next if $_[$i] eq $c;
			my @w1=@_;
			$w1[$i]=$c;
			push @out,join('',@w1);
		}
	}
	return \@out;
}
