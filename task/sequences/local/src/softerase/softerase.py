#!/usr/bin/env python
# encoding: utf-8

from __future__ import with_statement

from optparse import OptionParser
from sys import exit, stdout, stdin
from vfork.fasta.writer import MultipleBlockWriter

def main():
	parser = OptionParser(usage='%prog <SEQUENCE_FASTA')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	writer = MultipleBlockWriter(stdout)
	charmap = ''.join([ chr(i) for i in xrange(0, 256) ])
	
	for lineno, line in enumerate(stdin):
		line = line.rstrip()
		
		if len(line) == 0:
			exit('Detected empty line at line %d' % (lineno+1,))
		
		elif line[0] == '>':
			writer.write_header(line[1:])
		
		else:
			writer.write_sequence(line.translate(charmap, 'acgtn'))
	
	writer.flush()

if __name__ == "__main__":
	main()
