#!/usr/bin/env python
# encoding: utf-8

from optparse import OptionParser
from sys import exit, stdin, stdout
from vfork.fasta.reader import MultipleBlockStreamingReader
from vfork.fasta.writer import MultipleBlockWriter

def lookahead_iterator(it):
	try:
		prev = it.next()
		while True:
			cur = it.next()
			yield prev, False
			prev = cur
	except StopIteration:
		yield prev, True
	
def main():
	parser = OptionParser(usage='%prog <FASTA')
	parser.add_option('-s', '--spacer-length', dest='spacer_length', type='int', default=1000, help='how many Ns to insert between query blocks (default: 1000)', metavar='LENGTH')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	reader = MultipleBlockStreamingReader(stdin)
	writer = MultipleBlockWriter(stdout)
	for header, sequence in reader:
		writer.write_header(header)
		writer.write_sequence(sequence)

if __name__ == "__main__":
	main()
