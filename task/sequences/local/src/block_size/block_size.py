#!/usr/bin/env python
# encoding: utf-8

from optparse import OptionParser
from vfork.fasta import MultipleBlockReader

def main():
	parser = OptionParser(usage='%prog FASTA...')
	options, args = parser.parse_args()
	
	for arg in args:
		reader = MultipleBlockReader(arg)
		
		for block in reader.iter_blocks():
			print '%s\t%d' % (block.label, block.size)
		
		reader.close()

if __name__ == '__main__':
	main()
