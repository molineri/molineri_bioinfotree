#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;

my $usage="$0 [-f|filter REGEXP]< file.dbEST
	whith -f return a fasta block only if REGEXP match the header
";

my $filter = undef;
GetOptions(
	'f|filter=s' => \$filter
) or die($usage);

my @interesting_labels=(
	'GenBank Acc',
	'Organism',
	'Lib Name'
);



my @rows=();
while(<>){
	chomp;
	next if length==0 ;
	if(/^\|\|/){
		die('unexpected empy @rows') if !scalar(@rows);
		&parse(@rows);
		@rows=();
		next;
	}
	push @rows, $_;
}
die('unexpected empy @rows') if !scalar(@rows);
&parse(@rows);

sub parse
{
	my $seq='';
	my %val=();
	for(@interesting_labels){
		$val{$_} = '';
	}
	while($_ = shift(@_)){
		my $l = length($_);
		my $label = substr($_, 0 , $l >= 16 ? 16 : $l, '');
		$label=~s/:.*//;
		if($label eq 'SEQUENCE'){
			$seq = &parse_sequence(@_);
		}else{
			next if !length($_) or !length($label);
			if(defined($val{$label})){
				$val{$label} = $_;
			}
		}
	}
	my $toprint='>';
	for(@interesting_labels){
		$toprint.=$val{$_}."\t";
	}
	return if defined($filter) and $toprint !~ /$filter/;
	chop($toprint);
	print $toprint,"\n",$seq;
}

sub parse_sequence
{
	my $retval='';
	while($_ = shift(@_)){
		if($_ =~ /^\s/){
			$retval.=substr($_,16)."\n";
		}else{
			unshift @_, $_;
			return $retval;
		}
	}
	return $retval; #nel caso il file finisse con un blocco SEQUENCE
}
