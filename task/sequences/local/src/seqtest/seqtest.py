#!/usr/bin/env python
from sys import argv, exit, stderr

if __name__ == '__main__':
	if len(argv) < 2:
		print >>stderr, 'Need at list an argument.'
		exit(1)
	
	for path in argv[1:]:
		fd = file(path, 'r')
		for lineno, line in enumerate(fd):
			if line[0] == '>':
				continue
			else:
				line = line.rstrip()
				for colno, c in enumerate(line):
					if c not in 'acgtACGTnN':
						print "Unexpected char '%s' at line %d, col %d." % (c, lineno+1, colno+1)
		fd.close()

