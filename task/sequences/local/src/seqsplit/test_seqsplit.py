#!/usr/bin/env python
# encoding: utf-8

from cStringIO import StringIO
from os import system
from os.path import join
from random import randint
from shutil import rmtree
from tempfile import mkdtemp
from vfork.fasta.reader import MultipleBlockReader
from vfork.fasta.writer import SingleBlockWriter
import re
import unittest as ut

class TestSeqsplit(ut.TestCase):
	infile = 'input'
	outfile = 'output'
	
	def setUp(self):
		self.temp_dir = mkdtemp()
	
	def tearDown(self):
		rmtree(self.temp_dir)
	
	def testUnmasked(self):
		self._test_pattern([ ('unmasked', 1024) ])
		self._test_pattern([ ('unmasked', 1024), ('unmasked', 1024) ])
	
	def testMasked(self):
		self._test_pattern([ ('unmasked', 1024), ('masked', 1000), ('unmasked', 1024), ('masked', 1000), ('unmasked', 1024) ])
		self._test_pattern([ ('unmasked', 1024), ('masked', 500), ('unmasked', 1024) ])
	
	def testBorders(self):
		self._test_pattern([ ('unmasked', 1024), ('masked', 1000) ])
		self._test_pattern([ ('masked', 1000), ('unmasked', 1024), ('masked', 100) ])
		self._test_pattern([ ('masked', 100), ('unmasked', 20), ('masked', 40), ('unmasked', 20), ('masked', 200)])
		
	def _test_pattern(self, pattern):
		self._build_sequence(pattern)
		self._seqsplit()

		#from os import system
		#system('cat %s' % join(self.temp_dir, self.infile))
		#system('cat %s' % join(self.temp_dir, self.outfile))

		extracted_pattern = self._extract_pattern()
		self._compare_patterns(pattern, extracted_pattern)
	
	def _build_sequence(self, pattern):
		writer = SingleBlockWriter(join(self.temp_dir, self.infile), 'sample')
		
		for type, length in pattern:
			if type == 'unmasked':
				writer.write(self._unmasked_sequence(length))
			elif type == 'masked':
				writer.write(self._masked_sequence(length))
			else:
				raise RuntimeError, 'unexpected pattern type %s' % type
		
		writer.close()
	
	def _unmasked_sequence(self, length):
		chrs = 'ACGT'
		sequence = StringIO()

		for i in xrange(length):
			sequence.write(chrs[randint(0, len(chrs)-1)])

		return sequence.getvalue()

	def _masked_sequence(self, length):
		return 'N' * length
	
	def _seqsplit(self):
		res = system('./seqsplit.py -g 1000 -m 1020 %s > %s' % (join(self.temp_dir, self.infile), join(self.temp_dir, self.outfile)))
		self.assertEquals(0, res, 'unexpected seqsplit error')
	
	def _extract_pattern(self):
		reader = MultipleBlockReader(join(self.temp_dir, self.outfile))		
		rx = re.compile('sample_([0-9]+)')
		pattern = []
		
		for block in reader.iter_blocks():
			m = rx.match(block.label)
			self.assertNotEquals(None, m, 'malformed label')
			pattern.append((int(m.group(1)), block.size))
		
		reader.close()
		return pattern
	
	def _compare_patterns(self, expected_pattern, extracted_pattern):
		output_pattern = []

		if expected_pattern[0][0] == 'masked':
			start = None
			length = 0
			offset = expected_pattern[0][1]
		else:
			start = 0
			length = expected_pattern[0][1]
			offset = length

		for item in expected_pattern[1:]:
			if item[1] < 1000 or item[0] == 'unmasked':
				if start is None:
					start = offset
				length += item[1]
			else:
				assert start is not None
				output_pattern.append((start, length))
				start = None
				length = 0
			
			offset += item[1]
		
		if start is not None:
			if expected_pattern[-1][0] == 'masked':
				length -= expected_pattern[-1][1]
			output_pattern.append((start, length))

		self.assertEquals(output_pattern, extracted_pattern)
    
if __name__ == '__main__':
    ut.main()
