#!/usr/bin/env python
# encoding: utf-8

from itertools import izip
from optparse import OptionParser
from sys import exit, stderr

class Reader(object):
	def __init__(self, filename, chunk_size):
		self.fd = file(filename, 'r')
		self.chunk_size = chunk_size
	
	def __iter__(self):
		buf = ''
		
		for line in self.fd:
			if line[0] == '>':
				continue
			else:
				buf += line.rstrip().replace('N', '')
				while len(buf) > self.chunk_size:
					yield buf[:self.chunk_size]
					buf = buf[self.chunk_size:]
		
		if len(buf):
			yield buf

def main():
	parser = OptionParser(usage='%prog FASTA1 FASTA2')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	r1 = Reader(args[0], 50)
	r2 = Reader(args[1], 50)
	
	for chunk1, chunk2 in izip(r1, r2):
		if chunk1 != chunk2:
			print >>stderr, 'sequence 1: %s' % chunk1
			print >>stderr, 'sequence 2: %s' % chunk2
			exit(1)

if __name__ == "__main__":
	main()
