#!/usr/bin/env python
# encoding: utf-8

from optparse import OptionParser
from os.path import splitext
from sys import exit
from vfork.fasta.reader import SingleBlockReader

def output_filename(input_path, output_path):
	if output_path is not None:
		return output_path
	else:
		return splitext(input_path)[0] + '.psq'

def main():
	parser = OptionParser(usage='%prog FASTA')
	parser.add_option('-l', '--force-lower', action='store_true', dest='force_lower', default=False, help='force all bases to lower case')
	parser.add_option('-o', '--output', dest='output', help='output to FILE', metavar='FILE')
	options, args = parser.parse_args()

	if len(args) != 1:
		exit('Unexpected argument number.')
	
	reader = SingleBlockReader(args[0], options.force_lower)
	out_fd = file(output_filename(args[0], options.output), 'w')
	
	block_size = 100*1024*1024
	for pos in xrange(0, reader.size, block_size):
		block = reader.get(pos, min(block_size, reader.size-pos))
		out_fd.write(block)

	out_fd.write('\0')

if __name__ == '__main__':
	main()
