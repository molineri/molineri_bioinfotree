#!/usr/bin/env python

from optparse import OptionParser
from sys import exit, stdin
import re

def main():
	set_id_rx = re.compile(r'/gi=(\S+)')
	current_set_id = None
	
	for lineno, line in enumerate(stdin):
		if len(line.strip()) == 0:
			continue
		
		elif line[0] == '#':
			m = set_id_rx.search(line)
			if m is None:
				exit('Malformed set header at line %d.' % (lineno+1))
			else:
				current_set_id = m.group(1)
		
		elif line[0] == '>':
			if current_set_id is None:
				exit('Unexpected block header at line %d.' % (lineno+1))
			else:
				idx = line.find(' ')
				if idx != -1:
					header = line[1:idx]
				else:
					header = line[1:]
					
				print '%s\t%s' % (header, current_set_id)

if __name__ == '__main__':
	main()
