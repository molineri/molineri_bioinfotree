#!/usr/bin/perl
use warnings;
use strict;
$\="\n";

my $pre=undef;
my $pre_seq='';
while(<>){
	chomp;
	if(m/^>(\d+)_\d+/){
		if(!defined($pre) or $1 ne $pre){
			$pre=$1;
			&my_write($pre_seq);
			$pre_seq='';
			print ">$1";
		}else{
			$pre_seq.='-'
		}
	}else{
		$pre_seq.=$_;
	}
}

sub my_write
{
	my $a=shift;
	while($a){
		print substr($a,0,80,'');
	}
}
