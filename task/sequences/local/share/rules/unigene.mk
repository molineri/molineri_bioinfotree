# Copyright 2009 Paolo Martini <paolo.cavei@gmail.com>

SPECIES     ?= hsapiens
VERSION     ?= 33

SPECIES_MAP := $(TASK_ROOT)/local/share/species-unigene.map
BASE_URL    := ftp://ftp.ncbi.nih.gov/repository/UniGene/$(shell translate $(SPECIES_MAP) 1 <<<"$(SPECIES)")


ALL         += cdna.fa.gz


check.version:
	@set -e; \
        found="$$(wget -q -O- '$(BASE_URL)/*.info' \
                  | sed -r 's|^UniGene Build\s+\#([0-9]+).*|\1|;t;d')"; \
        if [[ $$found != $(VERSION) ]]; then \
                echo "*** Version mismatch: expected $(VERSION), found $$found" >&2; \
                exit 1; \
        fi

cdna.fa.gz: check.version
	wget -O- '$(BASE_URL)/*.seq.uniq.gz' >$@
