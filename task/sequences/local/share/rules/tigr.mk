include rules-common.mk

%.fa:
	set -o pipefail; \
	set -e; \
	chr=$*; \
	num=$${chr#chr}; \
	if [ $${#num} -eq 1 ]; then \
		num="0$$num"; \
	fi; \
	url="$(URL_PREFIX)chr$${num}.dir/chr$${num}$(URL_SUFFIX)"; \
	wget -q -c -O $@ "$$url"; \
	sed_inplace -e 's|^>\([^|]*\).*$$|>$*_\1|' $@
