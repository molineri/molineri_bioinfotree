# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2009 Alessandro Coppe <alexcoppe@gmail.com>

SPECIES        ?= hsapiens
REFSEQ_VERSION ?= 33

PACK_MAP       := $(TASK_ROOT)/local/share/species-refseq_pack.map
PACK_PATH      := $(TASK_ROOT)/dataset/ncbi/refseq/pack/$(REFSEQ_VERSION)/$(shell translate $(PACK_MAP) 1 <<<"$(SPECIES)")/
SPECIES_MAP    := $(TASK_ROOT)/local/share/species-refseq.map

extern $(PACK_PATH)/cdna.fa.gz as CDNA_FASTA


ALL += cdna.fa

cdna.fa: $(CDNA_FASTA)
	zcat $< \
	| get_fasta -r "\|\s*(PREDICTED:\s+)?$$(translate $(SPECIES_MAP) 1 <<<$(SPECIES))" \
	| sed -r 's/^>.*ref\|([^\|]+).*/>\1/' >$@
