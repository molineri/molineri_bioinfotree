# Copyright 2008-2010 Gabriele Sales <gbrsales@gmail.com>

include $(BIOINFO_ROOT)/task/sequences/local/share/species/ensembl/$(SPECIES).mk
include $(BIOINFO_ROOT)/task/sequences/local/share/rules/common.mk

SPECIES_MAP := $(BIOINFO_ROOT)/task/sequences/local/share/species-ensembl.map
URL_PREFIX  := ftp://ftp.ensembl.org/pub/release-$(VERSION)/fasta/$(shell translate $(SPECIES_MAP) 1 <<<"$(SPECIES)")
MARTSERVICE_URL  = http://$(shell translate -v -e www $(TASK_ROOT)/local/share/ensembl_archive_version.map 1 <<< $(VERSION) ).ensembl.org/biomart/martservice

chr%.fa.gz:
	wget -q -c -O $@ '$(URL_PREFIX)/dna/*.$(CHR_TYPE).chromosome.$*.fa.gz'

nchr.fa.gz:
	wget -q -c -O $@ '$(URL_PREFIX)/dna/*.$(CHR_TYPE).nonchromosomal.fa.gz'

.INTERMEDIATE: cdna.fa.gz
cdna.fa.gz:
	wget -q -c -O $@ '$(URL_PREFIX)/cdna/*.cdna.all.fa.gz'

ncrna.fa.gz:
	wget -q -c -O $@ '$(URL_PREFIX)/ncrna/*.ncrna.fa.gz'

%.fa: %.fa.gz
	zcat $< \
	| sed 's/^>\([^ \t]*\).*$$/>\1/' >$@

all_chr.len: $(addsuffix .fa.len, $(ALL_DNA))
	(for i in $^; do\
		echo -en "$$i\t" | sed 's/^chr//' | sed 's/.fa.len//';\
		cat $$i;\
	done;) > $@

nchr.fa: nchr.fa.gz
	zcat $< \
	| sed 's/^>\([^ \t]*\).*$$/>\1/' >$@

nchr.fa.len: nchr.fa
	fasta2tab -s < $< | perl -ane '$$a = scalar (split (//, $$F[1])); print "$$F[0]\t$$a\n"' > $@

complete.fa.len: all_chr.len nchr.fa.len
	cat $< $^2 | bsort -k2,2rn > $@

3utr.fa.gz:
	sed 's/__SPECIES__/$(SPECIES)/g' < $(TASK_ROOT)/local/share/queries/mart_3utr.xml > $@_query.xml
	wget -q -O - --post-file $@_query.xml $(MARTSERVICE_URL) \
	| repeat_fasta_pl  '$$,="\n"; my $$to_print = $$rows[0] !~ /.\s[^\n\r]/ ? 1 : 0; if($$to_print){$$header =~ s/\|/\t/g; print "$$header\n"; print @rows; print "\n"}' \                * in order to eliminate record without sequences lyke "Sequence unavailable" *
	| gzip > $@
	rm $@_query.xml

coding.fa.gz:
	sed 's/__SPECIES__/$(SPECIES)/g' < $(TASK_ROOT)/local/share/queries/mart_coding.xml > $@_query.xml
	wget -q -O - --post-file $@_query.xml $(MARTSERVICE_URL) \
	| repeat_fasta_pl  '$$,="\n"; my $$to_print = $$rows[0] !~ /.\s[^\n\r]/ ? 1 : 0; if($$to_print){$$header =~ s/\|/\t/g; print "$$header\n"; print @rows; print "\n"}' \                * in order to eliminate record without sequences lyke "Sequence unavailable" *
	| gzip > $@
	rm $@_query.xml

primary_assembly.fa.gz:
	wget -c -O $@ $(URL_PREFIX)/dna/*$(VERSION).dna_sm.primary_assembly.fa.gz
