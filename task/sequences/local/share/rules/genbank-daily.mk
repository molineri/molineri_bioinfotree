# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

GENBANK_HOST    ?= ftp.ncbi.nlm.nih.gov
GENBANK_PATH    ?= /genbank/daily-nc

SEQ_PATTERN     := ^nc.*\.flat\.gz

define do_list
tr ";" "\n" <<<"user anonymous nothing; cd $(1); ls -l" \
| ftp -p -n $(GENBANK_HOST)
endef

seq.list:
	( \
		set -e; \
		echo -n "ALL_SEQ:="; \
		$(call do_list,$(GENBANK_PATH)) \
		| sed -r 's|.* +||' \
		| grep '$(SEQ_PATTERN)' \
		| tr "\n" " " \
	) >$@

include seq.list
INTERMEDIATE += seq.list

ALL += $(ALL_SEQ)

%.flat.gz:
	wget -O$@ ftp://$(GENBANK_HOST)/$(GENBANK_PATH)/$@