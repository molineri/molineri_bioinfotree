# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

SEQUENCE_DIR   ?= $(BIOINFO_ROOT)/task/sequences/dataset/ucsc/hsapiens/hg18/
ANNOTATION_DIR ?= $(BIOINFO_ROOT)/task/annotations/dataset/ucsc/hsapiens/hg18/

param dna as CHR_NAMES from $(SEQUENCE_DIR)
extern $(addsuffix .fa,$(addprefix $(SEQUENCE_DIR),$(CHR_NAMES))) as ALL_CHRS

ALL += cdna.fa


cdna.fa: $(ANNOTATION_DIR)/aceview_genes $(ALL_CHRS)
	select_columns 1 2 4 5 3 <$< \
	| seqlist2fasta -l $(SEQUENCE_DIR) \
	| reverse_fasta 5 \
	| sed '/^>/ s|\t.*||; /^[^>]/ y/acgt/ACGT/' >$@
