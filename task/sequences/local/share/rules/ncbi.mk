include rules-common.mk

%.fa:
	set -o pipefail; \
	chr=$*; \
	chr_id=$${chr#chr}; \
	if [ $$chr_id != X -a $$chr_id != Y -a $${#chr_id} -lt 2 ]; then \
		chr_id="0$$chr_id"; \
	fi; \
	url="$(URL_PREFIX)$$chr_id$(URL_SUFFIX)"; \
	wget -q -c -O - "$$url" | zcat >$@; \
	[ $$? -eq 0 ] || exit 1; \
	sed -i'' -e 's/^>.*$$/>$*/' $@ || exit 1
