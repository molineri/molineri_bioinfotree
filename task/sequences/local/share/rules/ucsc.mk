DATABASE_URL   = http://hgdownload.cse.ucsc.edu/goldenPath/$(VERSION)

include $(BIOINFO_ROOT)/task/sequences/local/share/species/ucsc/$(SPECIES).mk
import common

.PHONY: all.chr
all.chr: $(addprefix chr,$(addsuffix .fa.raw,$(SPECIES_CHRS))) strange_chrs

strange_chrs:
	mkdir $@.tmp
	cd $@.tmp; \
		wget -O - $(DATABASE_URL)/chromosomes \
		| perl -ne 'print if !m/chr(\d+|[XYM]).fa/ and m/.fa.gz/' \		          * remove the standard chromosomes *
		| wget -i - --base=$(DATABASE_URL)/chromosomes/ --force-html;\			  * download the other *
		gunzip *.gz;\
		for i in *.fa; do \
			mv $$i ../$$i.raw ; \
			cd ..; bmake $$i; cd -;\
		done;
	rmdir $@.tmp

contig.fa.gz:
	wget -q -c -O $@ "$(DATABASE_URL)/bigZips/$(VERSION).fa.gz" > $@

.PHONY: all.fa
all.fa: $(addprefix chr,$(addsuffix .fa,$(SPECIES_CHRS)))
chr%.fa.raw:
	set -e; \
	url="$(DATABASE_URL)/chromosomes/chr$*.fa.gz"; \
	wget -q -c -O - "$$url" \
	| zcat > $@

all_chr.len:
	wget -O - $(DATABASE_URL)/database/chromInfo.txt.gz \
	| zcat | cut -f -2 \
	| sed 's/^chr//' > $@
