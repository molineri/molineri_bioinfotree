include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

ALL_DNA        := $(addprefix chr,$(SPECIES_CHRS))
ALL_SEQUENCES  := $(addsuffix .fa,$(ALL_DNA)) cdna.fa
ALL_SPLITTED   := $(addsuffix .fa.splitted,$(ALL_DNA))
BIN_DIR        := $(BIOINFO_ROOT)/task/sequences/local/bin/
MIN_BLOCK_SIZE ?= 10000
MIN_GAP_SIZE   ?= 1000
REPEAT_COORDS  ?= $(BIOINFO_ROOT)/task/annotations/dataset/ucsc/hsapiens/hg18/repeat.coords.gz
MASK_REPEATS   ?= no

all: download genome.len len
clean: clean.raw clean.ncbi_formatted clean.wu_formatted clean.splitted clean.len
	rm -f genome.fa genome.len
clean.full: clean
	rm -f $(ALL_SEQUENCES)

.PHONY: clean.raw
clean.raw:
	rm -f $(addsuffix .gz, $(ALL_SEQUENCES))

.PHONY: clean.ncbi_formatted
clean.ncbi_formatted:
	rm -f $(addsuffix .nhr, $(ALL_SEQUENCES))
	rm -f $(addsuffix .nin, $(ALL_SEQUENCES))
	rm -f $(addsuffix .nsd, $(ALL_SEQUENCES))
	rm -f $(addsuffix .nsi, $(ALL_SEQUENCES))
	rm -f $(addsuffix .nsq, $(ALL_SEQUENCES))

.PHONY: clean.splitted
clean.splitted:
	rm -f $(ALL_SPLITTED)
	rm -f $(addsuffix .idx,$(ALL_SPLITTED))

.PHONY: clean.wu_formatted
clean.wu_formatted:
	rm -f $(addsuffix .xnd, $(ALL_SPLITTED))
	rm -f $(addsuffix .xns, $(ALL_SPLITTED))
	rm -f $(addsuffix .xnt, $(ALL_SPLITTED))

.PHONY: clean.len
clean.len:
	rm -f $(addsuffix .len, $(ALL_SEQUENCES))

.PHONY: echo.dna
echo.dna:
	@echo $(ALL_DNA)

.PHONY: download.dna
download.dna: $(addsuffix .fa, $(ALL_DNA))

.PHONY: download.cdna
download.cdna: cdna.fa

.PHONY: download
download: download.dna download.cdna

.PHONY: ncbi_formatted
ncbi_formatted: $(addsuffix .nsq, $(ALL_SEQUENCES)) 

.PHONY: splitted
splitted: $(ALL_SPLITTED)

.PHONY: wu_formatted
wu_formatted: $(addsuffix .xns, $(ALL_SPLITTED))

.PHONY: len
len: $(addsuffix .fa.len, $(ALL_DNA))

ifeq ($(strip $(MASK_REPEATS)),yes)
chr%.fa: chr%.fa.raw $(REPEAT_COORDS)
	set -e; \
	tr "acgtn" "ACGTN" <$< \
	| sed 's/^>.*$$/>chr$*/' \
	| mask_fa <(zcat $(word 2,$^) | awk '$$1=="$(subst MT,M,$*)"' | union | cut -f2,3) \
	| tr "E" "n" \
	| $(BIN_DIR)/softerase >$@
else
chr%.fa: chr%.fa.raw
	sed 's/^>.*$$/>chr$*/' <$< >$@
endif

.PHONY: all_ni
all_nib: 
	@echo $(addsuffix .nib,$(ALL_DNA))

%.nib: %.fa
	faToNib $< $@			* faToNib is a jkent software *

.INTERMEDIATE: genome.fa
genome.fa: $(addsuffix .fa,$(ALL_DNA))
	cat $^ > $@

genome.len: $(addsuffix .fa,$(ALL_DNA))
	cat $^ \
	| grep -v '^>' \
	| tr -d "\n" \
	| wc -c >$@

%.1.ebwt: %.fa
	bowtie-build $< $*

%.fa.nsq: %.fa
	formatdb -p F -o T -i $<
	rm -f formatdb.log

%.fa.splitted.xns: %.fa.splitted
	xdformat -n -o $< $<

%.fa.xns: %.fa
	xdformat -n -o $< $<

%.fa.splitted: %.fa
	$(BIN_DIR)/seqsplit -i $@.idx -g $(MIN_GAP_SIZE) -m $(MIN_BLOCK_SIZE) <$< >$@

%.fa.len: %.fa
	cat $< \
	| unhead \
	| tr -d "\n" \
	| wc \
	| awk -F" " '{print $$3}' >$@
