include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

all: protein.fa.xpd rna.fa.xnd

protein.fa.gz:
	wget -c -O $@ $(URL_PROTEIN)

protein.fa.xpd: protein.fa.gz
	zcat $< \
	| grep -v "^$$" \
	| grep -v "^\#" \
	| xdformat -p -o protein.fa -- -

#ncbi format
protein.fa.psq: protein.fa.gz
	zcat $< >  protein.fa
	formatdb -p T -o T -i protein.fa
	rm -f formatdb.log
	rm -f protein.fa

rna.fa.gz:
	wget -c -O $@ $(URL_RNA)

rna.fa.xnd: rna.fa.gz
	zcat $< \
	| grep -v "^$$" \
	| grep -v "^\#" \
	| xdformat -n -o protein.fa -- -
