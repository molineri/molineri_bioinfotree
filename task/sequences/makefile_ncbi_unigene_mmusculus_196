SPECIES_PREFIX=Mm
SPECIES_NAME=Mus_musculus
URL_PREFIX=ftp://ftp.ncbi.nih.gov/repository/UniGene
ALL_SEQ=seq.all seq.uniq

BIN_DIR:=$(BIOINFO_ROOT)/task/sequences/local/bin

.PHONY: all.gz all.xns
.SECONDARY: seq.all.gz seq.uniq.gz
.DELETE_ON_ERROR:

all: all_xns all_set_index
all_gz: $(addsuffix .gz,$(ALL_SEQ))
all_xns: $(addsuffix .xns,$(ALL_SEQ))
all_set_index: $(addsuffix .set_index,$(ALL_SEQ))

seq.%.gz:
	wget -c -O $@ $(URL_PREFIX)/$(SPECIES_NAME)/$(SPECIES_PREFIX).$@

seq.%.xns: seq.%.gz
	zcat $< \
	| repeat_fasta_pipe -n 'echo $$HEADER; sed s/[^ATGCatgc]/N/g' \
	| grep -v "^$$" \
	| grep -v "^\#" \
	| xdformat -n -o seq.$* -- -

seq.%.set_index: seq.%.gz
	zcat $< \
	| $(BIN_DIR)/unigene_set_index \
	| sort -k 1,1 -S 50% >$@
