submitted_files: $(PRJ_ROOT)/local/share/data/ena_$(VERSION).xml.gz
	(\
		(\
			zgrep 'submitted_files' $<;\
			zgrep 'fastq_files' $<\
		) \
		| sed 's|\s*</*ID>||g' \
		| bawk '{print "wget -q -O - " $$1 " | unhead"'} \
		| sh\
	) > $@

.META: submitted_files human_available
	1	Study	
	2	Sample
	3	Experiment
	4	Run
	5	Organism
	6	Instrument
	7	Platform
	8	Library_Name
	9	Library_Layout
	10	Library_Source
	11	Library_Selection
	12	Run_Read_Count
	13	Run_Base_Count
	14	File_Name
	15	File_Size
	16	md5
	17	Ftp


human_available: submitted_files
	grep -v 'ftp:////' $< | grep Homo > $@

ERP000546.metadata:
	wget -O $@ http://www.ebi.ac.uk/arrayexpress/files/E-MTAB-513/E-MTAB-513.sdrf.txt

.PHONY: download
download: human_available
	bawk '$$File_Name~$$Run {print "wget -c " $$Ftp}' $< | sh    *se $$Filename!~$$Run allora c'e` un'altra riga in cui ~ e i 2 file sono uguali a parte gli id*
