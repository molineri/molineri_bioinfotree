MYSQL_CMD          := mysql --user=genome --host=genome-mysql.cse.ucsc.edu -BCAN $(DB)
DOWNLOAD_HTTP_PREFIX := http://hgdownload.cse.ucsc.edu/goldenPath/$(DB)/encodeDCC
DOWNLOAD_FTP_PREFIX := ftp://hgdownload.cse.ucsc.edu/goldenPath/$(DB)/encodeDCC
FASTQ_URLS ?= $(addprefix $(DOWNLOAD_FTP_PREFIX)/, $(SELECTED_DIR))

%.table.gz:
	$(MYSQL_CMD) <<< "select * from $*" \
	| gzip > $@

%.bb.gz: %.bb
	bigBedToBed $< /dev/stdout \
	| cut -f -6 \
	| gzip > $@

%.bfq: %.fastq.gz
	maq fastq2bfq <(zcat $< | tr " " "~") $@; 


all_bfq: $(addsuffix .bfq, $(RNA_SEQ_USED_FASTQ))
	@echo pass

.PHONY: all_align_tables
all_align_tables:
	for i in `wget -q -O - $(DOWNLOAD_HTTP_PREFIX) | perl -lne 'if(m/RnaSeq/){s/.*<a[^>]+>([^<]+)<.*/$$1/; print}'`; do\
		echo $$i;\
		for j in `wget -q -O - $(DOWNLOAD_HTTP_PREFIX)/$$i | perl -lne 'if(m/Align/){ s/.*<a[^>]+>([^<]+)<.*/$$1/i; print}'`; do\
			echo $$j;\
			wget -c -q $(DOWNLOAD_FTP_PREFIX)/$$i/$$j; \
		done;\
	done;

.PHONY: all_wig_tables
all_wig_tables:
	for i in `wget -q -O - $(DOWNLOAD_HTTP_PREFIX) | perl -lne 'if(m/RnaSeq/){s/.*<a[^>]+>([^<]+)<.*/$$1/; print}'`; do\
		echo $$i;\
		for j in `wget -q -O - $(DOWNLOAD_HTTP_PREFIX)/$$i | perl -lne 'if(m/bigWig/){ s/.*<a[^>]+>([^<]+)<.*/$$1/i; print}'`; do\
			echo $$j;\
			wget -c -q $(DOWNLOAD_FTP_PREFIX)/$$i/$$j; \
		done;\
	done;

.META: *.pairedTagAlign.gz
	1	chrom    	chr1
	2       chromStart	7350
	3       chromEnd	7375
	4       tag	GCCAGGAGCCAGGGGGTGACGGGTG
	5       score	3	Indicates mismatches, quality, or other measurement (0-1000)
	6       strand	-

.META: *.tagAlign.gz
	1	chrom    	chr1
	2       chromStart	7350
	3       chromEnd	7375
	4       tag	GCCAGGAGCCAGGGGGTGACGGGTG
	5       score	3	Indicates mismatches, quality, or other measurement (0-1000)
	6       strand	-

.DOC: *.tagAlign.gz *.pairedTagAlign.gz
	http://genome.ucsc.edu/cgi-bin/hgTables?db=hg18&hgta_group=expression&hgta_track=wgEncodeCshlShortRnaSeq&hgta_table=wgEncodeCshlShortRnaSeqAlignmentsGm12878CellShort&hgta_doSchema=describe+table+schema

.META: *.bb.gz
	1	chrom		chr1	Chromosome (or contig, scaffold, etc.)
	2	chromStart	4774	Start position in chromosome
	3	chromEnd	4824	End position in chromosome
	4	name		CAGGGCCGCTGCCCGCCCTGGGCCAGCACCTCGTAATTCTGTCCTGCCTT	Name of item
	5	score		1000	Score from 0-1000
	6	strand		-	+ or -

.META: *Align*bed12.gz
	1	chrom		chr1	varchar(255) Reference sequence chromosome or scaffold
	2	chromStart 	80	int(10) unsigned Start position in chromosome
	3	chromEnd 	111	int(10) unsigned End position in chromosome
	4	name 		727037::3	varchar(255) Name of item
	5	score 		1000	int(10) unsigned Score from 0-1000
	6	strand 		-	char(1) + or -
	7	thickStart 	0	int(10) unsigned Start of where display should be thick (start codon)
	8	thickEnd 	0	int(10) unsigned End of where display should be thick (stop codon)
	9	reserved 	16711680	int(10) unsigned Used as itemRgb as of 2004-11-22
	10	blockCount	1	int(10) unsigned Number of blocks
	11	blockSizes 	31	longblob Comma separated list of block sizes
	12	chromStarts 	0	longblob Start positions relative to chromStart

.DOC: *Align*bed12.gz
	http://genome.ucsc.edu/cgi-bin/hgTables?db=hg18&hgta_group=expression&hgta_track=wgEncodeCaltechRnaSeq&hgta_table=wgEncodeCaltechRnaSeqAlignsRep1Gm12878CellLongpolyaBow0981x32&hgta_doSchema=describe+table+schema

.META: *.bb
	1	chrom		chr1	Chromosome (or contig, scaffold, etc.)
	2	chromStart	4774	Start position in chromosome
	3	chromiEnd	4824	End position in chromosome
	4	name		CAGGGCCGCTGCCCGCCCTGGGCCAGCACCTCGTAATTCTGTCCTGCCTT	Name of item
	5	score		1000	Score from 0-1000
	6	strand		-	+ or -
	7	thickStart	4774	Start of where display should be thick (start codon)
	8	thickEnd	4824	End of where display should be thick (stop codon)
	9	reserved	0	Used as itemRgb as of 2004-11-22
	10	blockCount	1	Number of blocks
	11	blockSizes	50,	Comma separated list of block sizes
	12	chromStarts	0,	Start positions relative to chromStart
	13	seq1		CAGGGCCGCTGCCCGCCCTGGGCCAGCACCTCGTAATTCTGTCCTGCCTT	sequence 1, normally left paired end
	14	seq2		X	sequence 2, normally right paired end

.DOC: *bb
	binary file, use bigBedToBed to decode

all_Align.gz:     * consider to bmake all_align_tables or all_tables_option before*
	(for i in *Align*gz; \
		do bawk -v D=$$i '$$chromStart {print $$chrom, $$chromStart, $$chromEnd, $$score, D}' $$i \
	; done) \
	| sed 's/^chr//' \
	| bsort -k 1,1 -k 2,2n -S80% \
	| gzip > $@

.PHONY: all_fastq

all_fastq:
	for i in $(FASTQ_URLS); do\    *verifica che non sia equivalente ma meno figo della prima riga della regola all_align_tables:*
		wget_dir -c -f fastq $$i/;\
	done;

all_bedRnaElements:
	for i in $(FASTQ_URLS); do\    *verifica che non sia equivalente ma meno figo della prima riga della regola all_align_tables:*
		wget_dir -c -f bedRnaElements.gz $$i/;\
	done;

all_GencV7.gtf.gz:
	for i in $(FASTQ_URLS); do\    *verifica che non sia equivalente ma meno figo della prima riga della regola all_align_tables:*
		wget_dir -c -f GencV7.gtf.gz $$i/;\
	done;

all_GencV10.gtf.gz:
	for i in $(FASTQ_URLS); do\    *verifica che non sia equivalente ma meno figo della prima riga della regola all_align_tables:*
		wget_dir -c -f GencV10.gtf.gz $$i/;\
	done;
	

ALL.bedRnaElements.gz:
	(for i in *bedRnaElements.gz; do zcat $$i | append_each_row $$i; done) | perl -lne 'm/RnaSeq([A-Z][a-z1-9]+)/; print $$1.";".$$_'\
	| bsort  -S 70% -k1,1 -k2,2n -T/home/molineri/tmp/ | gzip > $@

all_files.txt:
	for i in $(FASTQ_URLS); do\
		wget -c -O  `basename $$i`.files.txt $$i/files.txt;\
		gzip `basename $$i`.files.txt;\
	done;

composite.file.txt.gz:
	zcat *.files.txt.gz  | perl -lpe 's/\t/;/g; s/\s+/_/g; s/;_/\t/g; s/;/\t/g ' > $@.tmp
	LABELS=`cut -f 2- $@.tmp | tr "\t" "\n" | perl -pe 's/=.*//' | sort | uniq | tr "\n" "\t"`;\
	perl -ane "BEGIN{ @labels=split(/\t/,\"$$LABELS\"); pop(@labels); print \"file\t\"; print join(\"\t\",@labels); print \"\n\" } print shift(@F); %a={}; for(@F){m/([^=]+)=([^=]+)/; \$$a{\$$1}=\$$2} for(@labels){\$$v = defined(\$$a{\$$_}) ? \$$a{\$$_} : "NA"; print \"\t\". \$$v}; print \"\n\"" $@.tmp \
	| gzip >$@
	rm $@.tmp


.META:	composite.file.txt.gz
	1	file
	2	bioRep
	3	cell
	4	composite
	5	dataType
	6	dataVersion
	7	dateResubmitted
	8	dateSubmitted
	9	dateUnrestricted
	10	dccAccession
	11	donorId
	12	geoSampleAccession
	13	grant
	14	insertLength
	15	lab
	16	labExpId
	17	labProtocolId
	18	labVersion
	19	localization
	20	mapAlgorithm
	21	md5sum
	22	objStatus
	23	origAssembly
	24	project
	25	protocol
	26	readType
	27	replicate
	28	rnaExtract
	29	seqPlatform
	30	sex
	31	size
	32	spikeInPool
	33	subId
	34	submittedDataVersion
	35	tableName
	36	treatment
	37	type

