MYSQL_CMD          := mysql --user=genome --host=genome-mysql.cse.ucsc.edu -BCAN $(UCSC_DATABASE)

%.bedGraph.gz:
	$(MYSQL_CMD) <<<"select * from $(TABLE_PREFIX)$*$(TABLE_SUFFIX)" \
	| cut -f 2-\
	| sed s/chr// \
	| bsort -k 1,1 -k2,2n -S45% \
	| gzip > $@

.PHONY: dowload
download: $(addsuffix .bedGraph.gz,$(TISSUES))
	echo done
