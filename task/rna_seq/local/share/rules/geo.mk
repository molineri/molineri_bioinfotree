URL_PREFIX=http://www.ncbi.nlm.nih.gov/geo/browse/?view=series

series_metadata.gz:
	wget -O - "$(URL_PREFIX)&type=$(SERIES_TYPE)&tax=$(TAXON_ID)&mode=tsv&page=1&display=100000000"\
	| unhead \
	| gzip > $@

.META: series_metadata.gz
	1	Accession
	2	Title
	3	Series_Type
	4	Taxonomy
	5	Sample_Count
	6	Datasets
	7	Supplementary_Types
	8	Supplementary_Links
	9	Contact
	10	Release_Date

