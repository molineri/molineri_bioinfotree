#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
use YAML::Tiny;

$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-g glue]] meta_tracks.yaml\n";

my $help=0;
my $not_all_cases=0;
GetOptions (
	'h|help' => \$help,
	'o|optional' => \$not_all_cases,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my $yaml = YAML::Tiny->read($ARGV[0]);

$yaml=${$yaml}[0];
my %y=%{$yaml};

foreach my $k (keys(%y)){
	my %series = %{$y{$k}};
	foreach my $e (keys(%series)){
		my %E = %{$series{$e}};
		my @S = @{$E{'strands'}};
		my @R = @{$E{'replicates'}};
		my @C = @{$E{'cells'}};

		my @T=();
		@T = @{$E{'types'}} if defined $E{'types'};

		my @T2 = ();
		@T2 = @{$E{'types_not_all_cases'}} if defined $E{'types_not_all_cases'};

		foreach my $s (@S){
			foreach my $r (@R){
				foreach my $c (@C){
					if($not_all_cases){
						foreach my $t (@T2){
							print $k.$e.$s.$r.$c.$t;
						}
					}else{
						foreach my $t (@T){
							print $k.$e.$s.$r.$c.$t;
						}
					}
				}
			}
		}
	}
}
