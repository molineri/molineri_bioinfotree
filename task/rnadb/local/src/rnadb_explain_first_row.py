#!/usr/bin/env python
from codecs import EncodedFile
from optparse import OptionParser
from sys import stdin
from vfork.util import exit
from xml.sax import parse
from xml.sax.handler import ContentHandler

class ColNames(ContentHandler):
	def startDocument(self):
		self.level = 0
		self.fields = set()
	
	def endDocument(self):
		for field in sorted(self.fields):
			print field
	
	def startElement(self, name, attrs):
		self.level += 1
		if self.level == 3:
			self.fields.add(name)
	
	def endElement(self, name):
		self.level -= 1
	
def main():
	parser = OptionParser(usage='%prog <XML')
	options, args = parser.parse_args()

	if len(args) != 0:
		exit('Unexpected argument number.')
	
	parse(EncodedFile(stdin, 'utf8'), ColNames())

if __name__ == '__main__':
	main()
