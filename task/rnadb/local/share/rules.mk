include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

BIN_DIR := $(BIOINFO_ROOT)/task/rnadb/local/bin

define download
	wget -O$@ '$(RNADB_XML_URL)$1'
endef

%.xml.gz: %.zip
	set -e; \
	tmpdir="tmp-$$$$"; \
	mkdir "$$tmpdir"; \
	cd "$$tmpdir"; \
	unzip ../$<; \
	gzip *.xml; \
	mv *.xml.gz ../$@; \
	cd ..; \
	rm -r "$$tmpdir"

asoverlaps.xml.gz: asoverlaps.zip
	set -e; \
	tmpdir="tmp-$$$$"; \
	mkdir "$$tmpdir"; \
	cd "$$tmpdir"; \
	unzip ../$<; \
	rm asoverlapseqs_*.xml; \
	tac asloverlaps_mm8.xml | unhead | tac >$@.tmp;\
	unhead asloverlaps_mm8.xml >> $@.tmp;\
	gzip $@.tmp; \
	mv $@.tmp.gz ../$@; \
	cd ..; \
	rm -r "$$tmpdir"

.INTERMEDIATE: $(addsuffix .pre_tab,$(ALL_DATASETS))
%.pre_tab: %.xml.gz
	zcat $< \
	| $(BIN_DIR)/rnadb_xml2tab colseqid colpubmedid colorganism coldefinition \
	| awk '{if ($$2~/e/) {$$2=""}; gsub(/,[ ]*/,";",$$2); print}' >$@

ALL_DATASETS := snoRNA RNAz piRNA noncodingRNA miRNA hinv fantom3 evofold combinedLit asoverlaps

all: $(addsuffix .xml.gz,$(ALL_DATASETS)) rnadb.tab
clean:
	rm -f $(addsuffix .pre_tab,$(ALL_DATASETS))
clean.full:
	rm -f $(addsuffix .xml.gz,$(ALL_DATASETS)) rnadb.tab

rnadb.tab: $(addsuffix .pre_tab,$(ALL_DATASETS))
	for i in $^; do \
		append_each_row $${i%.pre_tab} < $$i; \
	done > $@


.INTERMEDIATE: snoRNA.zip
snoRNA.zip:
	$(call download,$(SNORNA_URL_ID))

.INTERMEDIATE: RNAz.zip
RNAz.zip:
	$(call download,$(RNAZ_URL_ID))

.INTERMEDIATE: piRNA.zip
piRNA.zip:
	$(call download,$(PIRNA_URL_ID))

.INTERMEDIATE: noncodingRNA.zip
noncodingRNA.zip:
	$(call download,$(NONCODING_URL_ID))

.INTERMEDIATE: miRNA.zip
miRNA.zip:
	$(call download,$(MIRNA_URL_ID))

.INTERMEDIATE: hinv.zip
hinv.zip:
	$(call download,$(HINV_URL_ID))

.INTERMEDIATE: fantom3.zip
fantom3.zip:
	$(call download,$(FANTOM3_URL_ID))

.INTERMEDIATE: evofold.zip
evofold.zip:
	$(call download,$(EVOFOLD_URL_ID))

.INTERMEDIATE: asoverlaps.zip
asoverlaps.zip:
	$(call download,$(ASOVERLAPS_URL_ID))

.INTERMEDIATE: combinedLit.zip
combinedLit.zip:
	$(call download,$(COMBINEDLIT_URL_ID))

