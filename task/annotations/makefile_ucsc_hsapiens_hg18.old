SEQ_DIR:=$(BIOINFO_ROOT)/task/sequences/dataset/ensembl/hsapiens/40
BIN_DIR:=$(BIOINFO_ROOT)/task/annotations/local/bin
ENSEMBL_ANNOT_DIR:=$(BIOINFO_ROOT)/task/annotations/dataset/ensembl/hsapiens/40
COVERAGE_BIN_DIR:=$(BIOINFO_ROOT)/task/align_coverage/local/bin
VERSION:=hg18
DATABASE_URL:=http://hgdownload.cse.ucsc.edu/goldenPath/$(VERSION)/database
ALL_CHRS := 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X Y
MIN_SCORE_FOR_FRACTION=0

.DELETE_ON_ERROR:

conservation.gz:
	wget -O - $(DATABASE_URL)/multiz17waySummary.txt.gz \
	| zcat \
	| cut -f 2-6 \
	| grep '^chr[0-9XY]*\s' \
	| sort -k 4,4 -k 1,1 -k 2,2n -k 3,3n -S80% \
	| awk '{printf "%s\t%i\t%i\t%s\t%i\n", $$1,$$2,$$3,$$4,$$5*1000 }' \
	| sed 's/^chr//' \
	| gzip > $@

conservation_phast.gz:
	wget -O - $(DATABASE_URL)/phastConsElements17way.txt.gz \
	| zcat \
	| cut -f 2-4,6 \
	| grep '^chr[0-9XY]*\s' \
	| sed 's/^chr//' \
	| sort -k 1,1 -k 2,2n \
	| gzip > $@

conservation_both.gz: conservation_phast.gz conservation.gz
	(\
		zcat $< | awk 'BEGIN{OFS="\t"}{print $$1,$$2,$$3,"phast",$$4}'; \
		zcat $(word 2,$^);\
	) | gzip >$@

conservation_histogram.gz:
	wget -O - $(DATABASE_URL)/multiz17way.txt.gz \
	| zcat \
	| cut -f 1-4,7 \
	| grep '^chr[0-9XY]*\s' \
	| sort -k 1,1 -k 2,2n -k 3,3n -S80% \
	| gzip > $@

#############################
#
#	GENI
#
#############################

known_genes:
	wget -O - $(DATABASE_URL)/knownGene.txt.gz \
	| zcat \
	| sed 's/\(\s\)chr/\1/' > $@

refseq_genes:
	wget -O - $(DATABASE_URL)/refGene.txt.gz \
	| zcat \
	| cut -f 2- \
	| sed 's/\(\s\)chr/\1/' > $@


#############################
#
#	STATISTICA
#
#############################
%.conserved_region: conservation.gz
	zcat $< \
	| sort -k 1,1 -S80% \
	| repeat_group_pipe 'cut -f 2,3 | $(COVERAGE_BIN_DIR)/coverage | $(COVERAGE_BIN_DIR)/coverage_trigger 1 >$$1.conserved_region' 1 > $@

all_conserved_fractions_species: conservation_both.gz $(addsuffix .fa.len,$(addprefix $(SEQ_DIR)/chr,$(ALL_CHRS)))
	zcat $< \
	| repeat_group_pipe ' \
		echo -ne "$$1\t$$2\t"; \
		cut -f 2,3 \
		| awk '\''$$$$5>$(MIN_SCORE_FOR_FRACTION)'\'' \
		| $(COVERAGE_BIN_DIR)/coverage \
		| $(COVERAGE_BIN_DIR)/coverage_trigger 1 \
		| awk '\''{sum += $$$$2-$$$$1} END {printf "%i\t", sum}'\''; \
		cat $(SEQ_DIR)/chr$$2.fa.len' 4 1 \
	| repeat_group_pipe 'echo -ne "$$1\t"; cut -f 3- | sum_column' 1 \
	| awk '{print $$1 "\t" $$2/$$3}' \
	| sort -k2,2nr > $@

all_conserved_fractions: $(addsuffix .conserved_region,$(addprefix chr,$(ALL_CHRS))) \
                         $(addsuffix .fa.len,$(addprefix $(SEQ_DIR)/chr,$(ALL_CHRS)))
	(\
		for i in $(filter %.conserved_region,$^); do \
			echo -ne "$${i%%.conserved_region}\t"; \
			awk '{sum += $$2-$$1} END{printf "%i\t", sum}' $$i; \
			cat $(SEQ_DIR)/$${i%%.conserved_region}.fa.len; \
		done;\
	) > $@

intersection_%_ensembl_left.distrib: % $(ENSEMBL_ANNOT_DIR)/coords_gene
	intersection2 $<:2:4:5 $(word 2,$^) \
	| awk '{print $$8 "\t" ($$3-$$2)/($$5-$$4)}' \
	| find_best 1 2 \
	| cut -f 2 \
	| awk '{print 2-$$1}' \
	| binner -n 25 -l > $@

intersection_%_ensembl_right.distrib: % $(ENSEMBL_ANNOT_DIR)/coords_gene
	intersection2 $<:2:4:5 $(word 2,$^) \
	| awk '{print $$8 "\t" ($$3-$$2)/($$7-$$6)}' \
	| find_best 1 2 \
	| cut -f 2 \
	| awk '{print 2-$$1}' \
	| binner -n 25 -l > $@

intersection_%_ensembl.ps: intersection_%_ensembl_right.distrib intersection_%_ensembl_left.distrib
	(\
		echo "set terminal postscript color"; \
		echo "set xrange [1:2]"; \
		echo "set logscale y"; \
		echo "set logscale x"; \
		echo "plot '$<' w l ti 'intersec/ucsc', '' w p pt 6, '$(word 2,$^)' w l ti 'intersec/ensembORvega', '' w p pt 6" \
	)\
	| gnuplot > $@
