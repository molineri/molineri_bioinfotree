ifndef __BMAKE_DO_LAYOUT
# Copyright
# 2008,2010 Gabriele Sales <gbrsales@gmail.com>
# 2008,2010 Ivan Molineris <ivan.molineris@gmail.com>

SPECIES ?= hsapiens
VERSION ?= 18
UPSTREAM_EXTENSION ?= 10000

SPECIES_MAP        := $(BIOINFO_ROOT)/task/annotations/local/share/ucsc_species.map
UCSC_SPECIES        = $(shell translate $(SPECIES_MAP) 1 <<<"$(SPECIES)")
UCSC_DATABASE      ?= $(UCSC_SPECIES)$(VERSION)
SEQUENCE_DIR       ?= $(BIOINFO_ROOT)/task/sequences/dataset/ucsc/$(SPECIES)/$(UCSC_DATABASE)
DATABASE_URL       := http://hgdownload.cse.ucsc.edu/goldenPath/$(UCSC_DATABASE)/database
MYSQL_CMD          := mysql --user=genome --host=genome-mysql.cse.ucsc.edu -BCAN

ifndef __BMAKE_IMPORT_task_annotations_ucsc_gene_tracks
.bmake/tungsteno/task.annotations.ucsc_gene_tracks.module: SHELL=$(LOCAL_SHELL)
.bmake/tungsteno/task.annotations.ucsc_gene_tracks.module: ; echo 'M|task/annotations/ucsc_gene_tracks' >>.bmake/tungsteno/dynpaths.info && dynpaths -f .bmake/tungsteno/fprints .bmake/tungsteno/dynpaths.info >.bmake/tungsteno/paths && touch $@
include .bmake/tungsteno/task.annotations.ucsc_gene_tracks.module
endif
ifdef __BMAKE_IMPORT_task_annotations_ucsc_gene_tracks
.bmake/tungsteno/task.annotations.ucsc_gene_tracks.mk: SHELL=$(LOCAL_SHELL)
.bmake/tungsteno/task.annotations.ucsc_gene_tracks.mk: $(__BMAKE_IMPORT_task_annotations_ucsc_gene_tracks) ; rm -f .bmake/tungsteno/task.annotations.ucsc_gene_tracks.module && bmake-filter $(<D) <$< >$@
include .bmake/tungsteno/task.annotations.ucsc_gene_tracks.mk

$(SEQUENCE_DIR)/all_chr.len: ; bmake --external $@
ALL_CHR_LEN := $(SEQUENCE_DIR)/all_chr.len
include $(BIOINFO_ROOT)/task/sequences/local/share/species/ucsc/$(SPECIES).mk



ALL_XREF_MAPS := $(addsuffix .map,$(addprefix transcript-,$(UCSC_XREF_DBS)))


define transcript_xref
	$(MYSQL_CMD) $(UCSC_DATABASE) <<<" \
		SELECT name, '$*' as db, value FROM $1" \
	| sort -S40% >$@
endef

xref_dbs.mk:
	( \
		set -e; \
		echo -n "UCSC_XREF_DBS := "; \
		$(MYSQL_CMD) $(UCSC_DATABASE) <<<" \
		SHOW TABLES LIKE 'knownTo%'" \
		| sed 's|knownTo||' \
		| bawk '$$1!="super"' \
		| bawk '$$1!="known"' \
		| sort \
		| tr "\n\t" "  "; \
	) >$@
ifndef __BMAKE_INCLUDE_xref_dbs_mk
.bmake/tungsteno/xref_dbs.mk.include: SHELL=$(LOCAL_SHELL)
.bmake/tungsteno/xref_dbs.mk.include: xref_dbs.mk; echo 'I|xref_dbs.mk' >>.bmake/tungsteno/dynpaths.info && dynpaths -f .bmake/tungsteno/fprints .bmake/tungsteno/dynpaths.info >.bmake/tungsteno/paths && touch $@
include .bmake/tungsteno/xref_dbs.mk.include
endif
ifdef __BMAKE_INCLUDE_xref_dbs_mk
.bmake/tungsteno/xref_dbs.mk.inc: SHELL=$(LOCAL_SHELL)
.bmake/tungsteno/xref_dbs.mk.inc: $(__BMAKE_INCLUDE_xref_dbs_mk) ; rm -f .bmake/tungsteno/xref_dbs.mk.include && bmake-filter $(<D) <$< >$@
include .bmake/tungsteno/xref_dbs.mk.inc


ALL += transcripts.gz transcript-gene.map.gz transcript-xref.map.gz \
     transcript_structures.gz 5utr_structures.gz 3utr_structures.gz \
     ensembl_genes \
     known_genes \
     refseq_genes \
     transcript-LocusLink.map



clean:
	rm -f xref_dbs.mk
	rm -f transcript.info
	rm -f $(ALL_XREF_MAPS)
clean.full:
	rm -f transcripts.gz transcript-gene.map.gz transcript-xref.map.gz
	rm -f transcript_structures.gz 5utr_structures.gz 3utr_structures.gz

transcripts.gz: transcript.info
	cut -f1,3-7 $< \
	| gzip >$@


transcript-gene.map.gz: transcript.info
	cut -f-2 $< \
	| gzip >$@


.INTERMEDIATE: transcript.info
transcript.info:
	$(MYSQL_CMD) $(UCSC_DATABASE) <<<" \
		SELECT t.name, g.clusterId, t.chrom, t.txStart, t.txEnd, t.strand, ti.category, t.cdsStart, t.cdsEnd, t.exonStarts, t.exonEnds \
		FROM knownGene AS t \
			LEFT JOIN knownIsoforms AS g ON t.name = g.transcript \
			LEFT JOIN kgTxInfo AS ti ON t.name = ti.name \
		WHERE t.chrom NOT LIKE \"%\_%\"" \
	| bawk '{sub(/^chr/,"",$$3); \
	         sub(/^\+$$/,"+1",$$6); \
	         sub(/^-$$/,"-1",$$6); \
	         print $$0,"KNOWN"}' \
	| sort -S40% -k1,1 -k2,2 >$@



transcript-xref.map.gz: $(ALL_XREF_MAPS)
	sort -S40% -k1,1 -k2,2 $^ \
	| gzip >$@


.INTERMEDIATE: $(ALL_XREF_MAPS)
transcript-%.map:
	$(call transcript_xref,knownTo$*)

transcript_structures.gz: transcript.info
	ucsc-transcript_structures <$< \
	| gzip >$@


%_genes.whole: %_genes
	bawk '{print $$1,$$2,$$4,$$5,$$3}' $< > $@


%_genes.intergenic: %_genes.whole
	bawk '{print $$2,$$3,$$4,$$1}' $< \
	| bsort -k1,1 -k2,2n \
	| union | complement > $@



%_genes.coding: %_genes
	cat $< \
	| perl -lane '$$,="\t"; $$\="\n"; $$F[8]=~s/,^//g; $$F[9]=~s/,^//g; @ex_b=split /,/,$$F[8]; @ex_e=split /,/,$$F[9]; for (my $$i=0; $$i < scalar @ex_b; $$i++) { next if $$ex_e[$$i]<$$F[5]; next if $$ex_b[$$i]>$$F[6]; my $$b= $$ex_b[$$i] < $$F[5] ? $$F[5] : $$ex_b[$$i]; my $$e= $$ex_e[$$i] < $$F[6] ? $$ex_e[$$i] : $$F[6]; print $$F[1],$$b,$$e,$$F[0],"C",$$F[2] if ($$b < $$e); }' \
	| sort -k 1,1 -k2,2n \
	> $@



%_genes.5utr: %_genes
	cat $< \
| perl -lane '$$,="\t"; $$\="\n";$$F[8]=~s/,^//g;$$F[9]=~s/,^//g;@ex_b=split /,/,$$F[8];@ex_e=split /,/,$$F[9];my $$utr_b=$$F[3];my $$utr_e=$$F[5];if ($$F[2]=~/^-/){$$utr_b=$$F[6]; $$utr_e=$$F[4];}for (my $$i=0; $$i < scalar @ex_b; $$i++){next if $$ex_e[$$i]<$$utr_b;next if $$ex_b[$$i]>$$utr_e;my $$b= $$ex_b[$$i] < $$utr_b ? $$utr_b : $$ex_b[$$i];my $$e= $$ex_e[$$i] < $$utr_e ? $$ex_e[$$i] : $$utr_e;print $$F[1],$$b,$$e,$$F[0],"5",$$F[2] if ($$b < $$e);}' \
	| sort -k 1,1 -k2,2n \
	> $@


%_genes.3utr: %_genes
	cat $< \
| perl -lane '$$,="\t"; $$\="\n";$$F[8]=~s/,^//g;$$F[9]=~s/,^//g;@ex_b=split /,/,$$F[8];@ex_e=split /,/,$$F[9];my $$utr_b=$$F[6];my $$utr_e=$$F[4];if ($$F[2]=~/^-/) {$$utr_b=$$F[3]; $$utr_e=$$F[5];}for (my $$i=0; $$i < scalar @ex_b; $$i++) {next if $$ex_e[$$i]<$$utr_b;next if $$ex_b[$$i]>$$utr_e;my $$b = $$ex_b[$$i] < $$utr_b ? $$utr_b : $$ex_b[$$i];my $$e = $$ex_e[$$i] < $$utr_e ? $$ex_e[$$i] : $$utr_e;print $$F[1],$$b,$$e,$$F[0],"3",$$F[2] if ($$b < $$e);}' \
	| sort -k 1,1 -k2,2n \
	> $@


%_genes.3utr.strand: %_genes
	cat $< \
	| perl -lane '$$,="\t"; $$\="\n"; $$F[8]=~s/,^//g; $$F[9]=~s/,^//g; @ex_b=split /,/,$$F[8]; @ex_e=split /,/,$$F[9]; my $$utr_b=$$F[6]; my $$utr_e=$$F[4]; if ($$F[2]=~/^-/) {$$utr_b=$$F[3]; $$utr_e=$$F[5];} for (my $$i=0; $$i < scalar @ex_b; $$i++) {next if $$ex_e[$$i]<$$utr_b; next if $$ex_b[$$i]>$$utr_e; my $$b= $$ex_b[$$i] < $$utr_b ? $$utr_b : $$ex_b[$$i]; my $$e=$$ex_e[$$i] < $$utr_e ? $$ex_e[$$i] : $$utr_e; print $$F[1],$$b,$$e,$$F[0],$$3,$$F[2] if ($$b < $$e);}' \
	| sort -k 1,1 -k2,2n \
	> $@



%_genes.exons: %_genes.5utr %_genes.coding %_genes.3utr
	cat $^ | sort -k 1,1 -k 2,2n > $@


%_genes.introns: %_genes
	cat $< \
	| perl -lane '$$,="\t"; $$\="\n"; $$F[8]=~s/,^//g; $$F[9]=~s/,^//g; @ex_b=split /,/,$$F[8]; @ex_e=split /,/,$$F[9]; if ($$F[3]<$$ex_b[0]) {print $$F[1],$$F[3],$$ex_b[0],$$F[0],"I";} shift @ex_b; for (my $$i=0; $$i < scalar @ex_b; $$i++) {print $$F[1],$$ex_e[$$i],$$ex_b[$$i],$$F[0],"I" if ($$ex_e[$$i] < $$ex_b[$$i]);} print $$F[1],$$ex_e[$$#ex_e],$$F[4],$$F[0],"I" if ($$ex_e[$$#ex_e]<$$F[4]);' \
	| sort -k 1,1 -k2,2n \
	> $@

%_genes.nr_introns: %_genes.introns %_genes.exons
	intersection \
		<(cut -f -3 $<  | union | enumerate_rows -r | sort -k 1,1 -k 2,2n) \
		<(cut -f -3 $(word 2,$^) | union | enumerate_rows -r | sort -k 1,1 -k 2,2n) \
	| sort -k 8,8 -k 2,2n \
	| complement -i \
	| sort -k 1,1 -k 2,2n \
	| intersection - refseq_genes:2:4:5 | bawk '{print $$1,$$2,$$3,$$9}' \
	| sort | uniq | collapsesets 4 \
	| sort -k 1,1 -k 2,2n > $@

%_genes.upstream: %_genes.whole $(ALL_CHR_LEN)
	upstreams $(word 2,$^) $(UPSTREAM_EXTENSION) <$< >$@


%_genes.downstream: %_genes.whole $(ALL_CHR_LEN)
	upstreams -A $(word 2,$^) $(UPSTREAM_EXTENSION) <$< >$@


%_genes.tss: %_genes.whole $(ALL_CHR_LEN)
	upstreams $(word 2,$^) 1 <$< | cut -f -3,5 > $@


repeat.gz:
	( \
	if [ `echo "show tables like '%rmsk%';" | $(MYSQL_CMD) $(UCSC_DATABASE) | wc -l` != 1 ]; then \
		for i in $(SPECIES_CHRS); do \
			echo "SELECT genoName, genoStart, genoEnd, repName, repClass, repFamily  FROM chr$${i}_rmsk" | $(MYSQL_CMD) $(UCSC_DATABASE); \
		done; \
	else \
		echo "SELECT genoName, genoStart, genoEnd, repName, repClass, repFamily  FROM rmsk" | $(MYSQL_CMD) $(UCSC_DATABASE); \
	fi;\
	) \
	| sed 's/^chr//' \
	| sort -k 1,1 -k 2,2n \
	| gzip > $@

transposon.gz: repeat.gz
	zcat $< \
	| awk '$$5=="DNA" || $$5=="LINE" || $$5=="LTR" || $$5=="SINE"' \
	| gzip > $@

transposon_wide.gz: repeat.gz
	zcat $< \
	| awk '$$5!="Low_complexity" && $$5!="Other" && $$5!="Satellite" && $$5!="Simple_repeat" && $$5!="Unknown" && $$5!="Unknown?" && $$5!="rRNA" && $$5!="tRNA"' \
	| gzip > $@

%.txt.gz:
	wget -O $@ $(DATABASE_URL)/$@

%.sql:
	wget -O $@ $(DATABASE_URL)/$@

%Ali.match_block.gz: %Ali.txt.gz
	zcat $< | unhead |perl -lane 'BEGIN{$$,="\t"} @G=split(/,/,$$F[21]); @L=split(/,/,$$F[19]); $$i=0; for(@G){print $$F[10],$$F[14],$$_,$$_+$$L[$$i],$$F[1],$$F[11];$$i++}'\
	| sed 's/\tchr/\t/' \
	| bsort -S10% -k2,2 -k3,3n \
	| gzip > $@



knownGene.txt.gz:
	wget -O $@ $(DATABASE_URL)/$@


knownGene.annot.gz:
	echo "SELECT \
		gbCdnaInfo.id,\
		gbCdnaInfo.acc,\
		gbCdnaInfo.version,\
		gbCdnaInfo.moddate,\
		gbCdnaInfo.type,\
		gbCdnaInfo.direction,\
		gbCdnaInfo.source,\
		gbCdnaInfo.organism,\
		gbCdnaInfo.library,\
		gbCdnaInfo.mrnaClone,\
		gbCdnaInfo.sex,\
		gbCdnaInfo.tissue,\
		gbCdnaInfo.development,\
		gbCdnaInfo.cell,\
		gbCdnaInfo.cds,\
		gbCdnaInfo.keyword,\
		gbCdnaInfo.description,\
		gbCdnaInfo.geneName,\
		gbCdnaInfo.productName,\
		gbCdnaInfo.author,\
		gbCdnaInfo.gi,\
		gbCdnaInfo.mol,\
		description.name,\
		description.crc,\
		knownToRefSeq.name \
	FROM gbCdnaInfo \
	RIGHT OUTER JOIN \
		description on gbCdnaInfo.description = description.id \
	LEFT OUTER JOIN \
		knownToRefSeq on knownToRefSeq.value = gbCdnaInfo.acc" \
	| $(MYSQL_CMD) --quick $(UCSC_DATABASE)\
	| gzip > $@


knownToLocusLink.txt.gz:
	wget -O $@ $(DATABASE_URL)/knownToLocusLink.txt.gz


est.gz:
	wget -O $@ --timestamping $(DATABASE_URL)/all_est.txt.gz




est.unrolled.gz: est.gz
	bawk '{print $$qName, NR, $$tName, $$blockSizes, $$tStarts, $$strand, $$matches}' $<\
	| sed 's/\tchr/\t/' \
	| perl -lane 'BEGIN{$$,="\t"} @size=split(/,/,$$F[3]); @begin=split(/,/,$$F[4]); $$F[5] = $$F[5] eq "+" ? "+1" : "-1"; $$i=0; for(@size){$$F[3]=$$begin[$$i]; $$F[4]=$$begin[$$i]+$$_; print @F; $$i++}' \
	| bsort -k3,3 -k4,4n -S20% \
	| gzip > $@


###################################
#
#	Conservation
#
###################################

conservation_phast17.gz conservation_phast28.gz: conservation_phast%.gz:
	wget -O - $(DATABASE_URL)/phastConsElements$*way.txt.gz \
	| zcat \
	| cut -f 2-4,6 \
	| grep '^chr[0-9XY]*	' \
	| sed 's/^chr//' \
	| sort -k 1,1 -k 2,2n \
	| gzip > $@





###########
#
# POLY_A_DB
#
########### #               bin,chrom,chromStart,chromEnd,strand,name,score,strand,thickStart,thickEnd \

polyA_DB:
	echo "SELECT DISTINCT \
		name,bin,chrom,strand,chromStart,chromEnd,score \
		FROM polyaDb" \
	| $(MYSQL_CMD) $(UCSC_DATABASE) \
	| $(call convert_strand) >$@


###########
#
# targetScanS
#
###########

targetscanS_track:
	echo "SELECT DISTINCT \
		name,chrom,chromStart,chromEnd,strand,score \
		FROM targetScanS" \
	| $(MYSQL_CMD) $(UCSC_DATABASE) \
	| bawk '{sub(/^chr/,"",$$2); print}' \
	| $(call convert_strand) >$@


############
#
# Encode ChIP-seq
#
############

encode_chip_seq.gz:
	echo "SELECT \
		chrom, chromStart, chromEnd, name, score, strand, thickStart, thickEnd, reserved, blockCount, blockSizes, chromStarts, expCount, expIds, expScores\
		FROM wgEncodeRegTfbsClustered"\
	| $(MYSQL_CMD) $(UCSC_DATABASE) \
	| gzip > $@

############
#
# Encode ChIP-seq
#
############

.PHONY: database_dump
database_dump:
	mkdir -p $@
	cd $@;\
	wget -O - $(DATABASE_URL)/ \
	| grep -v chain \
	| grep -v xenoEst.txt.gz \
	| grep -v gb \
	| grep -v snp \
	| grep -v RnaSeq \
	| wget -c -i - --base=$(DATABASE_URL)/ --force-html



endif

endif

endif
