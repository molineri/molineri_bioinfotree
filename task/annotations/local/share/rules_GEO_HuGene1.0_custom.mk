#require: bmake

include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk


annot.csv.zip:
	@ echo "#wget -O $@ https://www.affymetrix.com/Auth/analysis/downloads/na26/ivt/$(VERSION).csv.zip"
	@ echo "It doesn't work, you must be logged"

annot.csv: annot.csv.zip
	unzip $<
	rm -f 3prime-IVT.AFFX_README.NetAffx-CSV-Files.txt
	mv $(VERSION).csv $@
	touch $@
	@ echo touch becouse uzipped file has a old date

probe-entrez.map: annot.csv $(NCBI_DIR)/gene_history.gz $(NCBI_DIR)/gene_info.gz
	zcat $(word 2,$^) > $@.tmp.gene_history
	zcat $(word 3,$^) > $@.tmp.gene_info
	affymetrix_annotation $< $@.tmp.gene_history $@.tmp.gene_info $@.tmp $(TAXONOMY_ID)
	unhead -n 1 $@.tmp \
	| sed 's/\t/@/' | tr "\t" ";" | sed 's/@/\t/' > $@
	cat $@.tmp_entrez_annoted | sed 's/\t/@/' | tr "\t" ";" | sed 's/@/\t/' > $@_entrez_annoted
	rm $@.tmp*

.META: probe-entrez.map
	1	ProbeSetID
	2	EntrezGene

probe-refseq.map: annot.csv
	perl -lne 'm|^"(\d+)"|; $$id=$$1; while(m|RefSeq|){s|([^\s,"]+)\s*//\s*RefSeq||; print "$$id\t$$1";}' $< > $@

probe-entrez.map_entrez_retired: probe-entrez.map

probe-entrez.map_entrez_annoted: probe-entrez.map

conversion: GPL6244-38256.txt
	clean-HuGene1.0-custom $< > $@
