SHELL := /bin/bash

BIN_DIR := $(BIOINFO_ROOT)/task/annotations/local/bin

SPECIES ?= hsapiens
GO_ENS_VERSION ?= 42
ENS_VERSION ?= 40
ANNOTATION_DIR ?= $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/$(SPECIES)/$(ENS_VERSION)
GO_ANNOTATION_DIR ?= $(BIOINFO_ROOT)/task/annotations/dataset/GO/$(SPECIES)/$(GO_ENS_VERSION)

REGIONS ?= $(BIOINFO_ROOT)/task/align_coverage/dataset/hsapiens_hsapiens_ensembl40_wublast_pam10/0/regions.1_50.coverage_correct_triggered.list
BACKGROUND ?= $(BIOINFO_ROOT)/task/align_coverage/dataset/hsapiens_hsapiens_ensembl40_wublast_pam10/0/regions_1_1.coverage_correct_triggered
TARGET ?= regions_1_1

ALL_CHR ?= 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X Y


.SECONDARY:
.DELETE_ON_ERROR:


$(TARGET).annote_gene: $(REGIONS) $(ANNOTATION_DIR)/coords_gene 
	cut -f 2- $< | sort -k 1,1 -k 2,2n \
	| $(BIN_DIR)/region_nearest_genes.pl -a $(word 2,$^) > $@

%.isect_only.annote_gene: %.annote_gene 
	awk '{FS="\t"} $$7=="i"' $< > $@

%.annote_gene.family: %.annote_gene
	append_each_row -b 'regions' < $< \
        | sed -r 's/,?OTTHUMG[0123456789]+//g' | cut -f 1,7 \
	| awk '$$2' | sort -k2,2 | uniq > $@

%.annote_gene.family.go: %.annote_gene.family $(GO_ANNOTATION_DIR)/ensg_go.riannot $(GO_ANNOTATION_DIR)/keywords_term_type_level $(GO_ANNOTATION_DIR)/go_description
	$(BIN_DIR)/join_annotation.pl -a $(word 2,$^) -tmp $@.tmp -g $< \
	| sort -k2,2 \
	| join3_pl -1 1 -2 2 -i -u -- $(word 3,$^) - \
	| join3_pl -i -u -- $(word 4,$^) - \
	| sort_pl 6 \
	| awk -F "\t" 'BEGIN{OFS="\t"}{print $$3,$$1,$$2,$$4,$$5,$$6,$$7,$$8}' > $@
	rm -f $@.tmp $@.tmp1
	# formato output: 
	# 1. conn_comp_id
	# 2. GO:.....
	# 3. description
	# 4. GO_term_type (biological_process, cellular_component, molecular_function)
	# 5. livello
	# 6. pvalue
	# 7. numero di geni totali
	# 8. numero di geni totali annotati alla parola
	# 9. numero di geni associati alla famiglia
	# 10. numero di geni della famiglia associati alla parola
	# 11. elenco dei geni della famiglia associati alla parola
