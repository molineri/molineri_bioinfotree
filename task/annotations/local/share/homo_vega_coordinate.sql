SELECT DISTINCT
	exon_stable_id,
	exon_chrom_start - 1,
	exon_chrom_end,
	exon_chrom_strand,
	transcript_stable_id,
	gene_stable_id,
	chr_name,
	transcript_chrom_start - 1,
	transcript_chrom_end,
	transcript_chrom_strand,
	coding_start - 1,
	coding_end,
	5utr_start - 1,
	5utr_end,
	3utr_start - 1,
	3utr_end,
	biotype
FROM hsapiens_gene_vega_structure__structure__main
WHERE chr_name REGEXP "^[0-9]+\$|^X$|^Y$|^MT$"
