SHELL := /bin/bash

BIN_DIR := $(BIOINFO_ROOT)/task/annotations/local/bin

SPECIES ?= hsapiens
ENS_VERSION ?= 40
ANNOTATION_DIR ?= $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/$(SPECIES)/$(ENS_VERSION)


REGIONS ?= $(BIOINFO_ROOT)/task/align_coverage/dataset/hsapiens_hsapiens_ensembl40_wublast_pam10/0/regions.1_50.coverage_correct_triggered.list
BACKGROUND ?= $(BIOINFO_ROOT)/task/align_coverage/dataset/hsapiens_hsapiens_ensembl40_wublast_pam10/0/regions_1_1.coverage_correct_triggered
TARGET ?= regions_1_50


COORDS_NONREDUNDANT ?= C E 5 3 I U N
NORMALIZE_VETTORINI ?= -r

ALL_CHR ?= 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X Y


all: $(TARGET).annote.bool.overrepr.Bonferroni


$(TARGET).annote: $(REGIONS) $(addprefix $(ANNOTATION_DIR)/coords_nonredundant.,$(ALL_CHR))
	cut -f 2- $< \
	| sort -k 1,1 -k 2,2n \
	| repeat_group_pipe '\
		intersection - $(ANNOTATION_DIR)/coords_nonredundant.$$1 \
		| annote_vettorini $(NORMALIZE_VETTORINI) -l "$(COORDS_NONREDUNDANT)" \
	' 1 > $@

$(TARGET).annote.bool: $(TARGET).annote
	awk 'BEGIN{OFS="\t"} {for (i=4; i<=NF; i++) {if ($$i>0) {$$i=1}} print}' \
	< $< > $@

$(TARGET).background.annote.sum: $(BACKGROUND)
	sort -k 1,1 -k 2,2n $< \
	| repeat_group_pipe '\
		intersection - $(ANNOTATION_DIR)/coords_nonredundant.$$1 \
		| annote_vettorini -n -l "$(COORDS_NONREDUNDANT)" \
	' 1 \
	| cut -f 4- | sum_column -n > $@

$(TARGET).annote.bool.overrepr: $(TARGET).annote.bool $(TARGET).background.annote.sum
	cut -f 4- $< | sum_column -c \
	| $(BIN_DIR)/annot_binomial.pl $(word 2,$^) > $@

$(TARGET).annote.bool.overrepr.Bonferroni: $(TARGET).annote.bool.overrepr
	correzione=$$(($$(wc -l < $<) * 7)); \
	perl -lane "\$$\=\"\n\"; \$$,=\"\t\"; @F = map {\$$_= \$$_ * $$correzione; \$$_=sprintf (\"\%.2e\",\$$_) } @F; print @F" < $< > $@
	# per ogni label (COORDS_NONREDUNDANT): pvalue Bonferroni corretto, segno - se coda sx, segno + se coda dx
