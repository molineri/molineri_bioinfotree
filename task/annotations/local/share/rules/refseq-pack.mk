# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2009 Alessandro Coppe <alexcoppe@gmail.com>

REFSEQ_HOST    ?= ftp.ncbi.nlm.nih.gov
REFSEQ_PATH    ?= /refseq/release/
REFSEQ_CLASS   ?= vertebrate_mammalian
REFSEQ_VERSION ?= 33

GENBANK_SUFFIX := .rna.gbff.gz

define do_list
tr ";" "\n" <<<"user anonymous nothing; cd $(1); ls -l" \
| ftp -p -n $(REFSEQ_HOST)
endef

genbank.list:
	( \
		set -e; \
		echo -n "ALL_GENBANK:="; \
		$(call do_list,$(REFSEQ_PATH)/$(REFSEQ_CLASS)) \
		| sed -r 's|.* +||' \
		| grep -F '$(GENBANK_SUFFIX)' \
		| tr "\n" " " \
	) >$@

include genbank.list
CLEAN += genbank.list

INTERMEDIATE += check.version
check.version:
	@set -e; \
	found="$$($(call do_list,$(REFSEQ_PATH)/release-notes) | sed -r 's|.* +RefSeq-release([0-9]+).*|\1|;t;d')"; \
	if [[ $$found  != $(REFSEQ_VERSION) ]]; then \
		echo "*** Version mismatch: expected $(REFSEQ_VERSION), found $$found" >&2; \
		exit 1; \
	fi


ALL += genbank.gz
genbank.gz: $(ALL_GENBANK)
	zcat $^ \
	| gzip >$@

INTERMEDIATE += $(ALL_GENBANK)
$(ALL_GENBANK): check.version
	wget 'ftp://$(REFSEQ_HOST)/$(REFSEQ_PATH)/$(REFSEQ_CLASS)/$@'
