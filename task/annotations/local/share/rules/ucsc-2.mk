# Copyright 
# 2008,2010 Gabriele Sales <gbrsales@gmail.com> 
# 2008,2010 Ivan Molineris <ivan.molineris@gmail.com>

SPECIES ?= hsapiens
VERSION ?= 18
UPSTREAM_EXTENSION ?= 10000

SPECIES_MAP        := $(BIOINFO_ROOT)/task/annotations/local/share/ucsc_species.map
UCSC_SPECIES        = $(shell translate $(SPECIES_MAP) 1 <<<"$(SPECIES)" | cut -f 1)
UCSC_DATABASE      ?= $(UCSC_SPECIES)$(VERSION)
SEQUENCE_DIR       ?= $(BIOINFO_ROOT)/task/sequences/dataset/ucsc/$(SPECIES)/$(UCSC_DATABASE)
DATABASE_URL       := http://hgdownload.cse.ucsc.edu/goldenPath/$(UCSC_DATABASE)/database
MYSQL_CMD          := mysql --user=genome --host=genome-mysql.cse.ucsc.edu -BCAN

import ucsc_gene_tracks

extern $(SEQUENCE_DIR)/all_chr.len as ALL_CHR_LEN
include $(BIOINFO_ROOT)/task/sequences/local/share/species/ucsc/$(SPECIES).mk



ALL_XREF_MAPS := $(addsuffix .map,$(addprefix transcript-,$(UCSC_XREF_DBS)))


define transcript_xref
	$(MYSQL_CMD) $(UCSC_DATABASE) <<<" \
		SELECT name, '$*' as db, value FROM $1" \
	| sort -S40% >$@
endef

xref_dbs.mk:
	( \
		set -e; \
		echo -n "UCSC_XREF_DBS := "; \
		$(MYSQL_CMD) $(UCSC_DATABASE) <<<" \
		SHOW TABLES LIKE 'knownTo%'" \
		| sed 's|knownTo||' \
		| bawk '$$1!="super"' \
		| bawk '$$1!="known"' \
		| sort \
		| tr "\n\t" "  "; \
	) >$@
include xref_dbs.mk


ALL += transcripts.gz transcript-gene.map.gz transcript-xref.map.gz \
     transcript_structures.gz 5utr_structures.gz 3utr_structures.gz \
     ensembl_genes \
     known_genes \
     refseq_genes \
     transcript-LocusLink.map



clean:
	rm -f xref_dbs.mk
	rm -f transcript.info
	rm -f $(ALL_XREF_MAPS)
clean.full:
	rm -f transcripts.gz transcript-gene.map.gz transcript-xref.map.gz
	rm -f transcript_structures.gz 5utr_structures.gz 3utr_structures.gz

transcripts.gz: transcript.info
	cut -f1,3-7 $< \
	| gzip >$@

.META: transcripts.gz
	1  transcript_ID  uc001aaa.2
	2  chr            1
	3  start          1115
	4  stop           4121
	5  strand         +
	6  biotype        noncoding
	7  status         KNOWN

transcript-gene.map.gz: transcript.info
	cut -f-2 $< \
	| gzip >$@

.META: transcript-gene.map.gz
	1  transcript_ID  uc001aaa.2
	2  gene_ID        1

.INTERMEDIATE: transcript.info
transcript.info:
	$(MYSQL_CMD) $(UCSC_DATABASE) <<<" \
		SELECT t.name, g.clusterId, t.chrom, t.txStart, t.txEnd, t.strand, ti.category, t.cdsStart, t.cdsEnd, t.exonStarts, t.exonEnds \
		FROM knownGene AS t \
			LEFT JOIN knownIsoforms AS g ON t.name = g.transcript \
			LEFT JOIN kgTxInfo AS ti ON t.name = ti.name \
		WHERE t.chrom NOT LIKE \"%\_%\"" \
	| bawk '{sub(/^chr/,"",$$3); \
	         sub(/^\+$$/,"+1",$$6); \
	         sub(/^-$$/,"-1",$$6); \
	         print $$0,"KNOWN"}' \
	| sort -S40% -k1,1 -k2,2 >$@

.META: transcript.info
	1       transcript_id	uc001aaa.2
	2       transcrip_cluster_id		1
	3       chr		1
	4       b		1115
	5       e		4121
	6       strand		+1
	7       type		noncoding
	8       coding_b	1115
	9       coding_e	1115
	10      exon_bs		1115,2475,3083,
	11      exon_es		2090,2584,4121,
	12      other		KNOWN

.DOC: transcript.info
		transcrip_cluster_id
	First, the Known Genes are clustered based on if they have overlapping
	exons.
	Then, within a cluster, the gene with the highest number of coding bases is
	chosen as the representative canonical gene.
	The details can be found in the source code of hgClusterGenes.c under
		kent/src/hg/near/hgClusterGenes

transcript-xref.map.gz: $(ALL_XREF_MAPS)
	sort -S40% -k1,1 -k2,2 $^ \
	| gzip >$@

.META: transcript-xref.map.gz
	1  transcript_ID  ENST00000000233
	2  database_ID    AFFY_HG_Focus
	3  external_ID    201526_at

.INTERMEDIATE: $(ALL_XREF_MAPS)
transcript-%.map:
	$(call transcript_xref,knownTo$*)

transcript_structures.gz: transcript.info
	ucsc-transcript_structures <$< \
	| gzip >$@

.META: transcript_structures.gz
	FASTA
	> transcript_ID chromosome coding_start coding_stop strand
	exon_ID exon_start exon_stop

%_genes.whole: %_genes
	bawk '{print $$1,$$2,$$4,$$5,$$3}' $< > $@

.META: *_genes.whole
	1       id	ENST00000404059
	2       chr	1
	3       b	1872
	4       e	3533
	5       strand	+1

%_genes.intergenic: %_genes.whole
	bawk '{print $$2,$$3,$$4,$$1}' $< \
	| bsort -k1,1 -k2,2n \
	| union | complement > $@

.META: %_genes.intergenic
	1	chr	1
	2	b	3661579
	3	e	4280926
	4	sx_ids	uc007aet.1;uc007aeu.1;uc007aev.1
	5	dx_ids	uc007aew.1;uc007aex.1


%_genes.coding: %_genes
	cat $< \
	| perl -lane '$$,="\t"; $$\="\n"; $$F[8]=~s/,^//g; $$F[9]=~s/,^//g; @ex_b=split /,/,$$F[8]; @ex_e=split /,/,$$F[9]; for (my $$i=0; $$i < scalar @ex_b; $$i++) { next if $$ex_e[$$i]<$$F[5]; next if $$ex_b[$$i]>$$F[6]; my $$b= $$ex_b[$$i] < $$F[5] ? $$F[5] : $$ex_b[$$i]; my $$e= $$ex_e[$$i] < $$F[6] ? $$ex_e[$$i] : $$F[6]; print $$F[1],$$b,$$e,$$F[0],"C",$$F[2] if ($$b < $$e); }' \
	| sort -k 1,1 -k2,2n \
	> $@

.META: %_genes.coding
	1       chr		1
	2       b		58953
	3       e		59871
	4       trans_id	ENST00000326183
	5       type		C


%_genes.5utr: %_genes
	cat $< \
	| perl -lane '$$,="\t"; $$\="\n";\\
		$$F[8]=~s/,^//g;\\
		$$F[9]=~s/,^//g;\\
		@ex_b=split /,/,$$F[8];\\
		@ex_e=split /,/,$$F[9];\\
		my $$utr_b=$$F[3];\\
		my $$utr_e=$$F[5];\\
		if ($$F[2]=~/^-/){\\
			$$utr_b=$$F[6]; $$utr_e=$$F[4];\\
		}\\
		for (my $$i=0; $$i < scalar @ex_b; $$i++){\\
			next if $$ex_e[$$i]<$$utr_b; \\
			next if $$ex_b[$$i]>$$utr_e; \\
			my $$b= $$ex_b[$$i] < $$utr_b ? $$utr_b : $$ex_b[$$i]; \\
			my $$e= $$ex_e[$$i] < $$utr_e ? $$ex_e[$$i] : $$utr_e; \\
			print $$F[1],$$b,$$e,$$F[0],"5",$$F[2] if ($$b < $$e);\\
		}' \
	| sort -k 1,1 -k2,2n \
	> $@

.META: %_genes.5utr
	1       chr		1
	2       b		58953
	3       e		59871
	4       trans_id	ENST00000326183
	5       type		5

%_genes.3utr: %_genes
	cat $< \
	| perl -lane '$$,="\t"; $$\="\n";\\
		$$F[8]=~s/,^//g; \\
		$$F[9]=~s/,^//g; \\
		@ex_b=split /,/,$$F[8]; \\
		@ex_e=split /,/,$$F[9]; \\
		my $$utr_b=$$F[6]; \\
		my $$utr_e=$$F[4]; \\
		if ($$F[2]=~/^-/) {\\
			$$utr_b=$$F[3]; $$utr_e=$$F[5];\\
		} \\
		for (my $$i=0; $$i < scalar @ex_b; $$i++) {\\
			next if $$ex_e[$$i]<$$utr_b;\\
			next if $$ex_b[$$i]>$$utr_e; \\
			my $$b = $$ex_b[$$i] < $$utr_b ? $$utr_b : $$ex_b[$$i]; \\
			my $$e = $$ex_e[$$i] < $$utr_e ? $$ex_e[$$i] : $$utr_e; \\
			print $$F[1],$$b,$$e,$$F[0],"3",$$F[2] if ($$b < $$e);}' \
	| sort -k 1,1 -k2,2n \
	> $@

.META: %_genes.3utr
	1       chr		1
	2       b		58953
	3       e		59871
	4       trans_id	ENST00000326183
	5       type		3

%_genes.3utr.strand: %_genes
	cat $< \
	| perl -lane '$$,="\t"; $$\="\n"; $$F[8]=~s/,^//g; $$F[9]=~s/,^//g; @ex_b=split /,/,$$F[8]; @ex_e=split /,/,$$F[9]; my $$utr_b=$$F[6]; my $$utr_e=$$F[4]; if ($$F[2]=~/^-/) {$$utr_b=$$F[3]; $$utr_e=$$F[5];} for (my $$i=0; $$i < scalar @ex_b; $$i++) {next if $$ex_e[$$i]<$$utr_b; next if $$ex_b[$$i]>$$utr_e; my $$b= $$ex_b[$$i] < $$utr_b ? $$utr_b : $$ex_b[$$i]; my $$e=$$ex_e[$$i] < $$utr_e ? $$ex_e[$$i] : $$utr_e; print $$F[1],$$b,$$e,$$F[0],$$3,$$F[2] if ($$b < $$e);}' \
	| sort -k 1,1 -k2,2n \
	> $@

.META: %_genes.3utr.strand
	1	chr	1
	2	b	58953
	3	e	59871
	4	trans_id	ENST00000326183
	5	strand	+1


%_genes.exons: %_genes.5utr %_genes.coding %_genes.3utr
	cat $^ | sort -k 1,1 -k 2,2n > $@

.META: *_genes.exons
	1       chr		1
	2       b		58953
	3       e		59871
	4       trans_id	ENST00000326183
	5       type		3
	6	strand		+1

%_genes.introns: %_genes 
	cat $< \
	| perl -lane '$$,="\t"; $$\="\n"; $$F[8]=~s/,^//g; $$F[9]=~s/,^//g; @ex_b=split /,/,$$F[8]; @ex_e=split /,/,$$F[9]; if ($$F[3]<$$ex_b[0]) {print $$F[1],$$F[3],$$ex_b[0],$$F[0],"I";} shift @ex_b; for (my $$i=0; $$i < scalar @ex_b; $$i++) {print $$F[1],$$ex_e[$$i],$$ex_b[$$i],$$F[0],"I" if ($$ex_e[$$i] < $$ex_b[$$i]);} print $$F[1],$$ex_e[$$#ex_e],$$F[4],$$F[0],"I" if ($$ex_e[$$#ex_e]<$$F[4]);' \
	| sort -k 1,1 -k2,2n \
	> $@

%_genes.nr_introns: %_genes.introns %_genes.exons %_genes
	intersection \
		<(cut -f -3 $<  | union | enumerate_rows -r | sort -k 1,1 -k 2,2n) \
		<(cut -f -3 $^2 | union | enumerate_rows -r | sort -k 1,1 -k 2,2n) \
	| sort -k 8,8 -k 2,2n \
	| complement -i \
	| sort -k 1,1 -k 2,2n \
	| intersection - $^3:2:4:5 | bawk '{print $$1,$$2,$$3,$$9}' \
	| sort | uniq | collapsesets 4 \
	| sort -k 1,1 -k 2,2n > $@

%_genes.upstream: %_genes.whole $(ALL_CHR_LEN)
	upstreams $^2 $(UPSTREAM_EXTENSION) <$< >$@

.META: *_genes.upstream
	1   transcript_ID    uc007aet.1
	2   chr		    1
	3   b		    3205713
	4   e		    3215713
	5   strand	    -1
	6   up_len	    10000
	7   down_len	    0
	8   gene_b	    3195984
	9   gene_e	    3205713

%_genes.downstream: %_genes.whole $(ALL_CHR_LEN)
	upstreams -A $^2 $(UPSTREAM_EXTENSION) <$< >$@

.META: %_genes.downstream
	1   transcript_ID    uc007aet.1
	2   chr		    1
	3   b		    3205713
	4   e		    3215713
	5   strand	    -1
	6   up_len	    10000
	7   down_len	    0
	8   gene_b	    3195984
	9   gene_e	    3205713

%_genes.tss: %_genes.whole $(ALL_CHR_LEN)
	upstreams $^2 1 <$< | cut -f -3,5 > $@

.META: *_genes.tss
	1   transcript_ID   uc007aet.1
	2   chr		    1
	3   pos		    3205713
	4   strand	    -1|+1

repeat.gz:
	( \
	if [ `echo "show tables like '%rmsk%';" | $(MYSQL_CMD) $(UCSC_DATABASE) | wc -l` != 1 ]; then \
		for i in $(SPECIES_CHRS); do \
			echo "SELECT genoName, genoStart, genoEnd, repName, repClass, repFamily  FROM chr$${i}_rmsk" | $(MYSQL_CMD) $(UCSC_DATABASE); \
		done; \
	else \
		echo "SELECT genoName, genoStart, genoEnd, repName, repClass, repFamily  FROM rmsk" | $(MYSQL_CMD) $(UCSC_DATABASE); \
	fi;\
	) \
	| sed 's/^chr//' \
	| sort -k 1,1 -k 2,2n \
	| gzip > $@

transposon.gz: repeat.gz
	zcat $< \
	| awk '$$5=="DNA" || $$5=="LINE" || $$5=="LTR" || $$5=="SINE"' \
	| gzip > $@

transposon_wide.gz: repeat.gz
	zcat $< \
	| awk '$$5!="Low_complexity" && $$5!="Other" && $$5!="Satellite" && $$5!="Simple_repeat" && $$5!="Unknown" && $$5!="Unknown?" && $$5!="rRNA" && $$5!="tRNA"' \
	| gzip > $@

%.txt.gz: 
	wget -O $@ $(DATABASE_URL)/$@

%.sql: 
	wget -O $@ $(DATABASE_URL)/$@

%Ali.match_block.gz: %Ali.txt.gz
	zcat $< | unhead |perl -lane 'BEGIN{$$,="\t"} @G=split(/,/,$$F[21]); @L=split(/,/,$$F[19]); $$i=0; for(@G){print $$F[10],$$F[14],$$_,$$_+$$L[$$i],$$F[1],$$F[11];$$i++}'\
	| sed 's/\tchr/\t/' \
	| bsort -S10% -k2,2 -k3,3n \
	| gzip > $@


.META: *%Ali.match_block.gz
	1       qID	RP_050111_02_B12
	2       tchr	chr1
	3       tb	4763963
	4       te	4764597
	5       tmathc	983
	6       qsize	983

knownGene.txt.gz:
	wget -O $@ $(DATABASE_URL)/$@

.META: knownGene.txt.gz
	1	name               "Name of gene"
	2	chrom              "Chromosome name"
	3	strand             "+ or - for strand"
	4	txStart            "Transcription start position"
	5	txEnd              "Transcription end position"
	6	cdsStart           "Coding region start"
	7	cdsEnd             "Coding region end"
	8	exonCount          "Number of exons"
	9	exonStarts "Exon start positions"
	10	exonEnds   "Exon end positions"
	11	proteinID          "UniProtKB ID" 
	12	alignID            "Unique identifier for each (known gene, alignment position) pair"

knownGene.annot.gz:
	echo "SELECT \
		gbCdnaInfo.id,\
		gbCdnaInfo.acc,\
		gbCdnaInfo.version,\
		gbCdnaInfo.moddate,\
		gbCdnaInfo.type,\
		gbCdnaInfo.direction,\
		gbCdnaInfo.source,\
		gbCdnaInfo.organism,\
		gbCdnaInfo.library,\
		gbCdnaInfo.mrnaClone,\
		gbCdnaInfo.sex,\
		gbCdnaInfo.tissue,\
		gbCdnaInfo.development,\
		gbCdnaInfo.cell,\
		gbCdnaInfo.cds,\
		gbCdnaInfo.keyword,\
		gbCdnaInfo.description,\
		gbCdnaInfo.geneName,\
		gbCdnaInfo.productName,\
		gbCdnaInfo.author,\
		gbCdnaInfo.gi,\
		gbCdnaInfo.mol,\
		description.name,\
		description.crc,\
		knownToRefSeq.name \
	FROM gbCdnaInfo \
	RIGHT OUTER JOIN \
		description on gbCdnaInfo.description = description.id \
	LEFT OUTER JOIN \
		knownToRefSeq on knownToRefSeq.value = gbCdnaInfo.acc" \
	| $(MYSQL_CMD) --quick $(UCSC_DATABASE)\
	| gzip > $@

.META: knownGene.annot.gz
	1	gbCdnaInfo_id
	2	gbCdnaInfo_acc
	3	gbCdnaInfo_version
	4	gbCdnaInfo_moddate
	5	gbCdnaInfo_type
	6	gbCdnaInfo_direction
	7	gbCdnaInfo_source
	8	gbCdnaInfo_organism
	9	gbCdnaInfo_library
	10	gbCdnaInfo_mrnaClone
	11	gbCdnaInfo_sex
	12	gbCdnaInfo_tissue
	13	gbCdnaInfo_development
	14	gbCdnaInfo_cell
	15	gbCdnaInfo_cds
	16	gbCdnaInfo_keyword
	17	gbCdnaInfo_description
	18	gbCdnaInfo_geneName
	10	gbCdnaInfo_productName
	20	gbCdnaInfo_author
	21	gbCdnaInfo_gi
	22	gbCdnaInfo_mol
	23	description_name
	24	description_crc
	25	knownToRefSeq_name
	

knownToLocusLink.txt.gz:
	wget -O $@ $(DATABASE_URL)/knownToLocusLink.txt.gz

.META: knownToLocusLink.txt.gz
	1	kgID        "Known Gene ID"
	2	alias	"A gene alias"

est.gz:
	wget -O $@ --timestamping $(DATABASE_URL)/all_est.txt.gz 


.META: est.gz
	1	bin 	585	smallint(5) unsigned 	Indexing field to speed chromosome range queries.
	2	matches 	187	int(10) unsigned 	Number of bases that match that aren't repeats
	3	misMatches 	0	int(10) unsigned 	Number of bases that don't match
	4	repMatches 	0	int(10) unsigned 	Number of bases that match but are part of repeats
	5	nCount 	0	int(10) unsigned 	Number of 'N' bases
	6	qNumInsert 	0	int(10) unsigned 	Number of inserts in query
	7	qBaseInsert 	0	int(10) unsigned 	Number of bases inserted in query
	8	tNumInsert 	0	int(10) unsigned 	Number of inserts in target
	9	tBaseInsert 	0	int(10) unsigned 	Number of bases inserted in target
	10	strand 	-	char(2) 	+ or - for strand. First character query, second target (optional)
	11	qName 	AA411542	varchar(255) 	Query sequence name
	12	qSize 	187	int(10) unsigned 	Query sequence size
	13	qStart 	0	int(10) unsigned 	Alignment start position in query
	14	qEnd 	187	int(10) unsigned 	Alignment end position in query
	15	tName 	chr1	varchar(255) 	Target sequence name
	16	tSize 	249250621	int(10) unsigned 	Target sequence size
	17	tStart 	14405	int(10) unsigned 	Alignment start position in target
	18	tEnd 	14592	int(10) unsigned 	Alignment end position in target
	19	blockCount 	1	int(10) unsigned 	Number of blocks in alignment
	20	blockSizes 	187,	longblob 	Size of each block
	21	qStarts 	0,	longblob 	Start of each block in query.
	22	tStarts 	14405,	longblob 	Start of each block in target.

.DOC: est.gz
	To generate this track, human ESTs from GenBank were aligned against the genome using blat. Note that the maximum intron length allowed by blat is 750,000 bases, which may eliminate some ESTs with very long introns that might otherwise align. When a single EST aligned in multiple places, the alignment having the highest base identity was identified. Only alignments having a base identity level within 0.5% of the best and at least 96% base identity with the genomic sequence were kept.

est.unrolled.gz: est.gz
	bawk '{print $$qName, NR, $$tName, $$blockSizes, $$tStarts, $$strand, $$matches}' $<\
	| sed 's/\tchr/\t/' \
	| perl -lane 'BEGIN{$$,="\t"} @size=split(/,/,$$F[3]); @begin=split(/,/,$$F[4]); $$F[5] = $$F[5] eq "+" ? "+1" : "-1"; $$i=0; for(@size){$$F[3]=$$begin[$$i]; $$F[4]=$$begin[$$i]+$$_; print @F; $$i++}' \
	| bsort -k3,3 -k4,4n -S20% \
	| gzip > $@

.META: est.unrolled.gz
	1       EST
	2       align_set
	3       chr
	4       b       of a blat block
	5       e       of a blat block
	6       strand
	7       match   of the entire align_set

###################################
#
#	Conservation
#
###################################

conservation_phast17.gz conservation_phast28.gz: conservation_phast%.gz:
	wget -O - $(DATABASE_URL)/phastConsElements$*way.txt.gz \
	| zcat \
	| cut -f 2-4,6 \
	| grep '^chr[0-9XY]*	' \
	| sed 's/^chr//' \
	| sort -k 1,1 -k 2,2n \
	| gzip > $@

.META: conservation_phast%.gz
	1	1	chr
	2	1865	b
	3	1943	e
	4	307	score


.DOC: conservation_phast17.gz
	Predictions of conserved elements produced by the phastCons program.
	This track shows a measure of evolutionary conservation in 17 vertebrates.
	http://genome.ucsc.edu/cgi-bin/hgTables?clade=mammal&org=Human&db=hg18&hgta_group=compGeno&hgta_track=multiz17way&hgta_table=phastCons17way&hgta_doSchema=describe+table+schema&hgta_regionType=genome&position=chr14%3A52394436-52487518&hgta_outputType=wigData&boolshad.sendToGalaxy=1&hgta_outFileName=&hgta_compressType=none

.DOC: conservation_phast28.gz
	Predictions of conserved elements produced by the phastCons program, trak also called "Most Conserved".
	This track shows a measure of evolutionary conservation in 28 vertebrates.
	http://genome.ucsc.edu/cgi-bin/hgTables?clade=mammal&org=Human&db=hg18&hgta_group=compGeno&hgta_track=mostConserved28way&hgta_table=phastConsElements28wayPlacMammal&hgta_doSchema=describe+table+schema&hgta_regionType=genome

###########
#
# POLY_A_DB
#
########### #               bin,chrom,chromStart,chromEnd,strand,name,score,strand,thickStart,thickEnd \

polyA_DB:
	echo "SELECT DISTINCT \
		name,bin,chrom,strand,chromStart,chromEnd,score \
		FROM polyaDb" \
	| $(MYSQL_CMD) $(UCSC_DATABASE) \
	| $(call convert_strand) >$@

.META: polyA_DB
	1	poly.name	p.79854.1
	2	poly_bin	590
	3	chr	chr1
	4	strand	+1
	5	start	751452
	6	stop	751453
	7	score	1000

###########
#
# targetScanS
#
########### 

targetscanS_track:
	echo "SELECT DISTINCT \
		name,chrom,chromStart,chromEnd,strand,score \
		FROM targetScanS" \
	| $(MYSQL_CMD) $(UCSC_DATABASE) \
	| bawk '{sub(/^chr/,"",$$2); print}' \
	| $(call convert_strand) >$@

.META: targetscanS_track
	1	name	AGRN:miR-144
	2	chr	chr1
	3	start	980692
	4	stop	980699
	5	strand	+1
	6	score	18

############
#
# Encode ChIP-seq
#
############

encode_chip_seq.gz: 
	echo "SELECT \
		chrom, chromStart, chromEnd, name, score, strand, thickStart, thickEnd, reserved, blockCount, blockSizes, chromStarts, expCount, expIds, expScores\
		FROM wgEncodeRegTfbsClustered"\
	| $(MYSQL_CMD) $(UCSC_DATABASE) \
	| gzip > $@

############
#
# Encode ChIP-seq
#
############

.PHONY: database_dump
database_dump:
	mkdir -p $@
	cd $@;\
	wget -O - $(DATABASE_URL)/ \
	| grep -v chain \
	| grep -v xenoEst.txt.gz \
	| grep -v gb \
	| grep -v snp \
	| grep -v RnaSeq \
	| wget -c -i - --base=$(DATABASE_URL)/ --force-html

	
.META: known_genes.xref.map.gz
	1	kgID
	2	mRNA
	3	spID
	4	spDisplayID
	5	geneSymbol
	6	refseq
	7	protAcc
	8	description

cpgIsland.gz:
	echo "SELECT \
		chrom, chromStart, chromEnd, name, cpgNum, gcNum, perCpg, perGc, obsExp\
		FROM cpgIslandExt"\
	| $(MYSQL_CMD) $(UCSC_DATABASE) \
	| gzip > $@

gap.gz:
	echo "SELECT \
		chrom, chromStart, chromEnd, ix, n, size, type, bridge\
		FROM gap"\
	| $(MYSQL_CMD) $(UCSC_DATABASE) \
	| sed 's/^chr//' \
	| gzip > $@

.META: gap.gz
	1	chrom	chr1
	2	chromStart	0
	3	chromEnd	10000
	4	ix	1
	5	n	N
	6	size	10000
	7	type	telomere
	8	bridge	no

%_genes.non_coding: %_genes.exons
	bawk '{print $$trans_id,$$type}' $< | sort -S10% | uniq | collapsesets 2 | bawk '$$2=="3" {print $$1,"non_coding"} $$2!="3" {print $$1, "coding"}' >$@

