FTP_PREFIX=ftp://ftp.ncbi.nih.gov/repository/OMIM
FTP_PREFIX=ftp://anonymous:ivan.molineris%40unito.it@grcf.jhmi.edu/OMIM/

ALL += gene2omim

genemap morbidmap genemap.key omim.txt.Z:
	wget $(FTP_PREFIX)/$@

genemap.parsed: genemap
	sed 's/| \+|/||/g' < $<\
	| sed 's/,|/;/g' \
	| tr "\t" " " \
	| tr "|" "\t" \
	| cut -f 1-8,10-12,14-> $@

.META: genemap.parsed
	1	numberig_system	Numbering system, in the format  Chromosome.Map_Entry_Number
	2	ener_Month
	3	enter_Day
	4	enter_Year
	5	Location
	6	Gene
	7	Gene_Status (see genemap.key for codes)
	8	Title
	9	MIM_Number
	10	Method (see genemap.key for codes)
	11	Comments
	12	Disorders
	13	Disorders_cont1
	14	Disorders_cont2
	15	Mouse_correlate
	16	Reference

disorders_morbid: morbidmap
	sed -r 's/\s+\|/|/g' $< | cut -d'|' -f 1,3 | tr " |" "_\t" |  awk -F"\t" '{print $$2 "\t" $$1}' | sort -k1,1n > $@

omim_phenotype2readable: omim.txt.Z
	zgrep -F '*FIELD* TI' -A 2 $< \
	| grep -vF "*FIELD* TI" \
	| tr "\t"   ' ' \
	| tr "\n" "\t" \
	| sed 's/\t--\t/\n/g' \
	| sed 's/\ /\t/; s/;;//g; s/\*FIELD\*\ TX$$/NULL/' \
	| perl -lne 'BEGIN{$$,="\t"} $$s="NULL"; @F=split /\t/; if($$F[0]=~s/^([^\d])//){ $$s=$$1 }else{$$s="NULL"} $$F[0].="\t$$s"; print @F' >$@

.META: omim_phenotype2readable
	1 OMIM_ID	102200
	2 OMYM_TYPE	#|*|%|^|NULL http://www.ncbi.nlm.nih.gov/Omim/omimfaq.html#mim_number_symbols
	3 NAME		PITUITARY ADENOMA, GROWTH HORMONE-SECRETING
	4 DESCRIPT	SOMATOTROPINOMA, FAMILIAL ISOLATED; FIS

api_entry: omim_phenotype2readable
	(for i in `cut -f 1 $< | sort | uniq | bawk '{if(NR==1 || NR%50==0){print ">" NR} print}' | fasta2tab | collapsesets 2 -g "," | cut -f 2`; do\
		wget -q -O - "http://api.omim.org/api/entry?apiKey=23CD71B8712F0196A6ED524FA47279779007AC07&mimNumber=$$i&format=python"; sleep 4;\
	done) > $@

api_pheno2name: api_entry
	perl -ne 'chomp; print if s/^.mimNumber.:\s(\d+),/$$1\t/ or s/^.preferredTitle.: .(.*).,?\s*$$/$$1\n/' $< > $@

gene2omim: omim_phenotype2readable
	wget -q -O - ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/mim2gene\
	| unhead | select_columns 2 1 3	> $@.tmp
	bawk '$$3=="phenotype" {print $$1,$$2}' $@.tmp\
	| translate -v -e NULL -d -g "|" -a $< 2 > $@.tmp2
	filter_1col 1 -v <(cut -f 1 $@.tmp2) < $@.tmp \
	| translate -d -g "|" -a $< 2 \
	| bawk '$$3!~/*|+/' >$@.tmp3
	cat $@.tmp2 $@.tmp3 > $@
	rm $@.tmp $@.tmp2 $@.tmp3

.META: gene2omim
	1 GeneID	entrez		some gene my be a postulated gene without an associated sequence (see GeneID 8008)
	2 OMIM_ID	phenotype
	3 OMIM_TYPE	#
	4 OMIM_NAME	ACHONDROPLASIA; ACH
	5 OMIM_DISEASE	NULL

################
#
#	obsolete
#
ens2mim_morbid:
	echo 'select gene_stable_id,dbprimary_id from hsapiens_gene_ensembl__xref_mim_gene__dm where dbprimary_id is not null' \
	$(MYSQL_EMSEMBL) | sort -k 2,2n > $@ 

ens2mim_gene:
	echo 'select gene_stable_id,dbprimary_id from hsapiens_gene_ensembl__xref_mim_morbid__dm where dbprimary_id is not null'
	$(MYSQL_EMSEMBL) | sort -k 2,2n > $@ 

ens2disorders: ens2mim_morbid disorders_gene
	sort -k 2,2n $< | uniq | join3_pl -i -n -u -1 2 -2 1 - $(word 2,$^) | sort | uniq > $@
