# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2009 Alessandro Coppe <alexcoppe@gmail.com>

REFSEQ_CLASS   ?= vertebrate_mammalian
REFSEQ_VERSION ?= 33

SPECIES_MAP := $(TASK_ROOT)/local/share/species-ncbi.map

extern $(TASK_ROOT)/dataset/ncbi/refseq/pack/$(REFSEQ_VERSION)/$(REFSEQ_CLASS)/genbank.gz as GENBANK_DATA


define filter_genbank
	zcat $< \
	| repeat_genbank_pipe -s 10000000 '$(1) -s "$(shell translate $(SPECIES_MAP) 1 <<<"$(SPECIES)")"' \
	| sort -k1,1 \
	| gzip >$@
endef

ALL += cdnas.gz 
cdnas.gz: $(GENBANK_DATA)
	$(call filter_genbank,refseq2cdnas)

ALL += transcript-gene.map.gz
transcript-gene.map.gz: $(GENBANK_DATA)
	$(call filter_genbank,refseq2transcript-gene)
