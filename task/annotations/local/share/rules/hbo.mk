HTTP_PREFIX_ANNOTATION=http://compbio.charite.de/hudson/job/hpo.annotations.monthly/$(VERSION_ANNOTATION)/artifact/annotation
HTTP_PREFIX_ONTOLOGY=http://compbio.charite.de/hudson/job/hpo/$(VERSION_ONTOLOGY)/artifact/hp

hp.obo.gz: 
	wget -O >(gzip > $@) $(HTTP_PREFIX_ONTOLOGY)/hp.obo

hp.tree.gz: hp.obo.gz
	zcat $< \
	| perl -ne 'chomp;\\
		unless(m/^id:/ or m/^alt_id:/ or m/^is_a:/){next};\\                    *prendo solo le righe che mi interessano, cosi` butto anche via l'header
		s/^id:\s/\n/;\\								*tutti i blocchi iniziano con id:, aggiungo un \n per dire che il blocco precedente e` finito
		s/\s+!.*$$//;\\								*dopo il ! ho dei commenti o descrizioni*
		s/^alt_id:\s/;/;\\							*posso avere molti sinonimi, li incollo all'id principale con ;*
		s/^is_a:\s(.*)$$/\@$$1/;\\						*ecco l'indicazione del parent, separo con @, possono essercene piu` di uno e solo il primo diventera` \t*
		print; \\
		END{print "\n"}' \
	| unhead \									*tolgo la prima riga vuota spuria*
	| sed 's/@/\t/'\								*solo il primo, gli altri li espando*
	| bawk 'NF>=2'\									*tolgo la radice e termini "obsolete" che non sono figli di niente e fanno casino*
	| expandsets -s ';' 1\
	| expandsets -s '@' 2\
	| gzip >$@


hp.owl.gz: 
	wget -O >(gzip > $@) $(HTTP_PREFIX_ONTOLOGY)/hp.owl

%.raw.gz:
	wget -O >(gzip > $@) $(HTTP_PREFIX_ANNOTATION)/$*.txt



genes_to_diseases.gz:
	wget -O >(gzip > $@) $(HTTP_PREFIX)/genes_to_diseases.txt

diseases_to_genes.gz: 
	wget -O >(gzip > $@) $(HTTP_PREFIX)/diseases_to_genes.txt


########################
#
#	note contenute in 
#
#	http://compbio.charite.de/hudson/job/hpo.annotations.monthly/67/artifact/annotation/external_data.txt
#
## URLs to files needed for gene2phenotype
#
#
#ftp://anonymous:sebastian.koehler%40charite.de@ftp.omim.org/OMIM/mim2gene.txt
#ftp://anonymous:sebastian.koehler%40charite.de@ftp.omim.org/OMIM/genemap
#ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene_info.gz

######
#
## URLs to files provided by ORPHANET (http://www.orphadata.org/)
#
## Diseases and cross-referencing 
#http://www.orphadata.org/data/xml/en_product1.xml

# Diseases with their associated genes
# http://www.orphadata.org/data/xml/en_product6.xml
#
#
# ### HPO-data
# http://compbio.charite.de/hudson/job/hpo.annotations/lastStableBuild/artifact/misc/phenotype_annotation.tab
# http://compbio.charite.de/hudson/job/hpo/lastSuccessfulBuild/artifact/hp/hp.obo
1	HP:0000002
2	HP:0001507
