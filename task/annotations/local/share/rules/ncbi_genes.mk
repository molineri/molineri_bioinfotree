# Copyright 2009-2010 Paolo Martini <paolo.cavei@gmail.com>
# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>

PACK_DIR    := $(TASK_ROOT)/dataset/ncbi/genes/pack/$(VERSION)
SPECIES_MAP := $(TASK_ROOT)/local/share/species-ncbi_taxon.map
TAXON_ID    ?= $$(translate $(SPECIES_MAP) 1 <<<"$(SPECIES)")

extern $(PACK_DIR)/gene_info.gz
extern $(PACK_DIR)/gene2refseq.gz

ALL   += genes.gz
CLEAN += entrez-ensembl.map.gz

extern $(PACK_DIR)/gene_info.gz
extern $(PACK_DIR)/gene_history.gz

genes.gz: $(PACK_DIR)/gene_info.gz
	bawk '$$tax_id=="'$(TAXON_ID)'"' $< \
	| cut -f 2- \
	| gzip >$@

genes_with_discontinued.gz: genes.gz $(PACK_DIR)/gene_history.gz
	(\
		zcat $<;\
		bawk '$$gene_id~/\d+/ $$tax_id=="'$(TAXON_ID)'" {print $$discontinued_gene_id,$$gene_id}' $^2 \
		| translate -k <(zcat $<) 2 \
	) | gzip > $@

.META: genes.gz genes_with_discontinued.gz
	1   gene_id                                5692769
	2   symbol                                 NEWENTRY
	3   locus_tag                              -
	4   synonyms                               -
	5   db_xrefs                               -
	6   chromosome                             -
	7   map_location                           -
	8   description                            Record to support submission of GeneRIFs [...]
	9   type_of_gene                           other
	10  symbol_from_nomenclature_authority     -
	11  full_name_from_nomenclature_authority  -
	12  nomenclature_status                    -
	13  other_designations                     -
	14  modification_date                      20071023

entrez-refseq.map.gz: $(PACK_DIR)/gene2refseq.gz
	bawk -v T=$(TAXON_ID) '$$tax_id==T {if($$4!="-"){print $$2,$$4;} if($$6!="-"){print $$2,$$6;} if($$8!="-"){print $$2,$$8}}' $< \
	| tr "." "\t" \
	| sort | uniq \
	| gzip > $@

.META: entrez-refseq.map.gz
	1	gene_id		1
	2	refseq_id	NP_570602
	3	version		2

entrez-ensembl.map.gz: genes.gz
	bawk '$$db_xrefs~/Ensembl/ {print $$gene_id, $$db_xrefs}' $< \
	| perl -lane '$$,="\t"; @m = m/Ensembl:([^\|\s]+)/g; for(@m){print $$F[0],$$_}' \
	| bsort -k1,1n -u \
	| gzip >$@

.META: entrez-ensembl.map.gz
	1	entrez_gene_id
	2	ensembl_gene_id

ifdef ENSEMBL_VERSION
extern $(TASK_ROOT)/dataset/ensembl/$(SPECIES)/$(ENSEMBL_VERSION)/gene-entrez.map.gz as ENSEG2ENTREZ

CLEAN += entrez-ensembl.inclusive-map.gz

entrez-ensembl.inclusive-map.gz: entrez-ensembl.map.gz $(ENSEG2ENTREZ)
	( zcat $<; zcat $^2 | select_columns 2 1 ) | sort -n | uniq | gzip > $@
endif

entrez-HPRD.map.gz entrez-HGNC.map.gz: entrez-%.map.gz: genes.gz
	zcat $<\
	| cut -f 1,5 | expandsets -s "|" 2 \
	| grep -Fw $* \
	| tr ":" "\t" | cut -f 1,3 \
	| gzip >$@

gene2accession.gz: $(PACK_DIR)/gene2accession.gz
	bawk -v T=$(TAXON_ID) '$$tax_id==T' $< \
	| cut -f 2-\
	| gzip >$@

.META: gene2accession.gz
	1	gene_id                                  1246500
	2	status                                   -
	3	rna_nucleotide_accession                 -
	4	rna_nucleotide_gi                        -
	5	protein_accession                        AAD12597.1
	6	protein_gi                               3282737
	7	genomic_nucleotide_accession             AF041837.1
	8	genomic_nucleotide_gi                    3282736
	9	start_position_on_the_genomic_accession  -
	10	end_position_on_the_genomic_accession    -
	11	orientation                              ?
	12	assembly                                 -	

entrez-symbol.map.gz: genes.gz
	zcat $<\
	| cut -f 1,2 \
	| gzip >$@
