# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

SPECIES         ?= hsapiens
REFSEQ_VERSION  ?= 33
MAPVIEW_VERSION ?= 36.3

REFSEQ_DIR  := $(TASK_ROOT)/dataset/ncbi/refseq/$(SPECIES)/$(REFSEQ_VERSION)
MAPVIEW_DIR := $(TASK_ROOT)/dataset/ncbi/mapview/$(SPECIES)/$(MAPVIEW_VERSION)

extern $(REFSEQ_DIR)/cdnas.gz
extern $(MAPVIEW_DIR)/transcript-gene.map.gz
extern $(MAPVIEW_DIR)/transcript_structures.gz


ALL += cdnas.gz \
       transcript-gene.map.gz transcript_structures.gz

cdnas.gz: %: $(REFSEQ_DIR)/%
	link_install -f $< $@

transcript-gene.map.gz transcript_structures.gz: %: $(MAPVIEW_DIR)/%
	link_install -f $< $@
