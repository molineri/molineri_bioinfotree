# Copyright 2009-2010 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2009-2010 Paolo Martini <paolo.cavei@gmail.com>

SPECIES     ?= sscrofa
VERSION     ?= 38

SPECIES_MAP := $(TASK_ROOT)/local/share/species-unigene.map
BASE_URL    := ftp://ftp.ncbi.nih.gov/repository/UniGene/$(shell translate $(SPECIES_MAP) 1 <<<"$(SPECIES)")

extern $(BIOINFO_ROOT)/task/sequences/dataset/ncbi/unigene/$(SPECIES)/$(VERSION)/cdna.fa.gz as SEQ_FASTA


ALL   += annotation.gz unigene.gz
CLEAN += unigene-sequence.map.gz unigene-cluster.map.gz


check.version:
	@set -e; \
        found="$$(wget -q -O- '$(BASE_URL)/*.info' \
               | sed -r 's|^UniGene Build\s+\#([0-9]+).*|\1|;t;d')"; \
        if [[ $$found != $(VERSION) ]]; then \
                echo "*** Version mismatch: expected $(VERSION), found $$found" >&2; \
                exit 1; \
        fi

annotation.gz:
	wget -O- '$(BASE_URL)/*.data.gz' >$@


unigene.gz: annotation.gz
	zcat $< \
	| sed -r 's/^(\S+) +/\1\t/' \
	| bawk '$$1 == "ID"      {id=$$2; gid=sym=descr=""} \
	        $$1 == "GENE_ID" {gid=$$2}                  \
	        $$1 == "GENE"    {sym=$$2}                  \
	        $$1 == "TITLE"   {descr=$$2}                \
	        $$1 == "//"      {print id,gid,sym,descr}'  \
	| gzip >$@

.META: unigene.gz
	1  unigene_ID   Ssc.1
	2  entrez_ID    397048
	3  gene_symbol  ST5AR2
	4  description  Steroid 5-alpha-reductase 2


unigene-sequence.map.gz: annotation.gz
	zcat $< \
	| sed -r 's/^(\S+) +/\1\t/' \
	| bawk '$$1 == "ID"       {id=$$2}                                   \
	        $$1 == "SEQUENCE" {match($$2,/ACC=([^;]+)/,acc);             \
	                           match($$2,/SEQTYPE=([^;]+)/,seqtype);     \
	                           match($$2,/END=([^;]+)/,end);             \
	                           if (end[1]!="") {    \
	                             print id,acc[1],seqtype[1] "_" end[1] } \
	                           else {                                    \
	                             print id,acc[1],seqtype[1] }}'          \
	| gzip >$@

.META: unigene-sequence.map.gz
	1  unigene_ID     Ssc.1
	2  sequence_ID    NM_213988.1
	3  sequence_type  mRNA


unigene-cluster.map.gz: $(SEQ_FASTA)
	zcat $< \
	| bawk '/^>/ {match($$0,/\|([A-Z][a-z]{2}#[A-Z0-9]+).*\/ug=([A-Z][a-z]{2}\.[0-9]+)/,ids);  \
                      print ids[2],ids[1]}' \
	| gzip >$@

.META: unigene.cluster.map.gz
	1  unigene_ID  Ssc.1
	2  cluster_ID  Ssc#S20116722
