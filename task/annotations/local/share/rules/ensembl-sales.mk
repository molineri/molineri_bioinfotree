# Copyright 2008-2011 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2009      Ivan Molineris <ivan.molineris@gmail.com>

SPECIES            ?= hsapiens
VERSION            ?= 50
UPSTREAM_EXTENSION ?= 10000

SPECIES_MAP     := $(BIOINFO_ROOT)/task/annotations/local/share/species-ensembl.map
ENSEMBL_SPECIES := "$$(bawk -v s=$(SPECIES) '$$1==s {print $$2}' $(SPECIES_MAP))"
MYSQL_CMD       := mysql --user=anonymous --host=ensembldb.ensembl.org --port=5306 -BCAN
MYSQL_CMD_MART  := mysql --user=anonymous --host=martdb.ensembl.org -P 5316 -BCAN ensembl_mart_$(VERSION)
SEQUENCE_DIR    ?= $(BIOINFO_ROOT)/task/sequences/dataset/ensembl/$(SPECIES)/$(VERSION)
FTP_URL         ?= ftp://ftp.ensembl.org/pub/release-$(VERSION)/
MARTSERVICE_URL ?= http://www.ensembl.org/biomart/martservice

import gene-structures
extern $(SEQUENCE_DIR)/all_chr.len as ALL_CHR_LEN


database.name:
	set -e; \
	pattern="$(ENSEMBL_SPECIES)_core_$(VERSION)_%"; \
	$(MYSQL_CMD) <<<"SHOW DATABASES LIKE '$$pattern'" \
	| at_least_rows -e 1 >$@


## External references
##

ifeq ($(call lt,$(VERSION),67),$(true))

define xref_sql
SELECT stable_id, db_name, x.dbprimary_acc, x.display_label, x.description \
FROM object_xref AS ox \
  LEFT JOIN $(1)_stable_id AS gs ON ox.ensembl_id = gs.$(1)_id \
  LEFT JOIN xref AS x ON ox.xref_id = x.xref_id \
  LEFT JOIN external_db AS ed ON x.external_db_id = ed.external_db_id \
WHERE ensembl_object_type='$(1)' AND stable_id is not NULL
endef

else # VERSION >= 67

define xref_sql
SELECT stable_id, db_name, x.dbprimary_acc, x.display_label, x.description \
FROM object_xref AS ox \
  LEFT JOIN $(1) AS g ON ox.ensembl_id = g.$(1)_id \
  LEFT JOIN xref AS x ON ox.xref_id = x.xref_id \
  LEFT JOIN external_db AS ed ON x.external_db_id = ed.external_db_id \
WHERE ensembl_object_type='$(1)' AND stable_id is not NULL
endef

endif


define xref
	$(MYSQL_CMD) $$(cat $<) <<<"$(call xref_sql,$(1))" \
	| bsort -u \
	| gzip >$@
endef


## Genes
##

ifeq ($(call lt,$(VERSION),67),$(true))

define gene_sql
SELECT g.stable_id, name, seq_region_start-1, seq_region_end, seq_region_strand, biotype, display_label, g.description \
FROM gene AS g \
  LEFT JOIN gene_stable_id AS gs ON g.gene_id = gs.gene_id \
  LEFT JOIN seq_region AS sr ON g.seq_region_id = sr.seq_region_id \
  LEFT JOIN xref AS xr ON g.display_xref_id = xr.xref_id
endef

else # VERSION >= 67

define gene_sql
SELECT g.stable_id, name, seq_region_start-1, seq_region_end, seq_region_strand, biotype, display_label, g.description \
FROM gene AS g \
  LEFT JOIN seq_region AS sr ON g.seq_region_id = sr.seq_region_id \
  LEFT JOIN xref AS xr ON g.display_xref_id = xr.xref_id
endef

endif

genes.gz: database.name
	$(MYSQL_CMD) $$(cat $<) <<<"$(gene_sql)" \
	| bsort -u \
	| gzip >$@

.META: genes.gz
	1  gene_ID      ENSG00000000003
	2  chr          X
	3  start        99770450
	4  stop         99778450
	5  strand       -1
	6  biotype      protein_coding
	7  label        TSPAN6
	8  description  Tetraspanin-6 (Tspan-6)(Transmembrane 4 superfamily member 6)(T245 protein)(Tetraspanin TM4-D)(A15 homolog) [Source:UniProtKB/Swiss-Prot;Acc:O43657]


gene-xref.map.gz: database.name
	$(call xref,gene)

.META: gene-xref.map.gz
	1	gene_ID  ENSG00000000003
	2	database_ID    EMBL
	3	external_ID    AC100821
	4	external_NAME	PINCO
	5	external_DESC	pingo gene involved in pallino proces

gene-entrez.map.gz: gene-xref.map.gz
	bawk '$$database_ID == "EntrezGene" {print $$gene_ID, $$external_ID}' $< \
	| bsort -u \
	| gzip -9 >$@

.META: gene-entrez.map.gz
	1  gene_ID    ENSG00000000003
	2  entrez_ID  7105


## Transcripts
##

ifeq ($(call lt,$(VERSION),67),$(true))

define transcript_sql
SELECT ts.stable_id, gs.stable_id, sr.name, seq_region_start-1, seq_region_end, seq_region_strand, biotype, status \
FROM transcript AS t \
  LEFT JOIN transcript_stable_id AS ts ON t.transcript_id = ts.transcript_id \
  LEFT JOIN gene_stable_id AS gs ON t.gene_id = gs.gene_id \
  LEFT JOIN seq_region AS sr ON t.seq_region_id = sr.seq_region_id \
WHERE is_current = 1 AND \
  sr.name NOT LIKE '%\_%'            * ignore non-standard chromosomes *
endef

else # VERSION >= 67

define transcript_sql
SELECT t.stable_id, g.stable_id, sr.name, t.seq_region_start-1, t.seq_region_end, t.seq_region_strand, t.biotype, t.status \
FROM transcript AS t \
  LEFT JOIN gene AS g ON t.gene_id = g.gene_id \
  LEFT JOIN seq_region AS sr ON t.seq_region_id = sr.seq_region_id \
WHERE t.is_current = 1 AND \
  sr.name NOT LIKE '%\_%'            * ignore non-standard chromosomes *
endef

endif

transcript.info: database.name
	$(MYSQL_CMD) $$(cat $<) <<<"$(transcript_sql)" \
	| bsort -u >$@


transcripts.gz: transcript.info
	cut -f1,3- $< \
	| gzip >$@

.META: transcripts.gz
	1  transcript_ID  ENSMUST00000000001
	2  chr            3
	3  start          107910199
	4  stop           107949007
	5  strand         -1
	6  biotype        protein_coding
	7  status         KNOWN


ifeq ($(call lt,$(VERSION),67),$(true))

define transcript_gene_sql
SELECT ts.stable_id, gs.stable_id \
FROM transcript AS t \
  LEFT JOIN transcript_stable_id AS ts ON t.transcript_id = ts.transcript_id \
  LEFT JOIN gene_stable_id AS gs ON t.gene_id = gs.gene_id \
WHERE is_current = 1
endef

else # VERSION >= 67

define transcript_gene_sql
SELECT t.stable_id, g.stable_id \
FROM transcript AS t \
  LEFT JOIN gene AS g ON t.gene_id = g.gene_id \
WHERE t.is_current = 1
endef

endif

transcript-gene.map.gz: database.name 
	$(MYSQL_CMD) $$(cat $<) <<<"$(transcript_gene_sql)" \
	| bsort -u \
	| gzip >$@

.META: transcript-gene.map.gz
	1  transcript_ID  ENSMUST00000000001
	2  gene_ID        ENSMUSG00000000001


transcript-xref.map.gz: database.name
	$(call xref,transcript)

.META: transcript-xref.map.gz
	1  transcript_ID   ENST00000000412
	2  database_ID     HGNC_transcript_name
	3  external_ID     M6PR-001
	4  external_name   M6PR-001
	5  external_descr  integrin, alpha 2 (CD49B, alpha 2 subunit of VLA-2 receptor)


transcript-refseq.map.gz: transcript-xref.map.gz translation-xref.map.gz translation-transcript.map.gz 
	( bawk '$$2~/RefSeq_dna/' $<; \
	  bawk '$$2~/RefSeq_dna/' $^2 \
	  | translate <(zcat $^3) 1 \
	) \
	| cut --complement -f2 \
	| bsort -u \
	| gzip >$@


transcript_structures.gz:
	with_ensembl_api $(VERSION) fetch-ensembl-transcript_structures $(ENSEMBL_SPECIES) \
	| gzip >$@

.META: transcript_structures.gz
	FASTA
	> transcript_ID chromosome coding_start coding_stop strand
	exon_ID exon_start exon_stop


cdnas.gz:
	with_ensembl_api $(VERSION) fetch-ensembl-cdnas $(ENSEMBL_SPECIES) \
	| bsort -u \
	| gzip >$@

.META: cdnas.gz
	1  ID            ENSMUST00000000001
	2  length        3204
	3  coding_start  84
	4  coding_stop   1149
	5  5utr_start    0
	6  3utr_stop     3204


transcripts.gtf.gz:
	wget -O- '$(FTP_URL)/gtf/'$(ENSEMBL_SPECIES)'/*.gtf.gz' \
	| zcat \
	| gzip -9 >$@


## Exons
##

ifeq ($(call lt,$(VERSION),67),$(true))

define exon_sql
SELECT es.stable_id, sr.name, seq_region_start-1, seq_region_end, seq_region_strand, phase, end_phase \
FROM exon AS e \
  LEFT JOIN exon_stable_id AS es ON e.exon_id = es.exon_id \
  LEFT JOIN seq_region AS sr ON e.seq_region_id = sr.seq_region_id \
WHERE is_current = 1 AND \
  sr.name NOT LIKE '%\_%'
endef

else # VERSION >= 67

define exon_sql
SELECT e.stable_id, sr.name, seq_region_start-1, seq_region_end, seq_region_strand, phase, end_phase \
FROM exon AS e \
  LEFT JOIN seq_region AS sr ON e.seq_region_id = sr.seq_region_id \
WHERE is_current = 1 AND \
  sr.name NOT LIKE '%\_%'
endef

endif

exons.gz: database.name
	$(MYSQL_CMD) $$(cat $<) <<<"$(exon_sql)" \
	| bsort -u \
	| gzip >$@

.META: exons.gz
	1  exon_ID    ENSMUSE00000097910
	2  chr        10
	3  start      6794511
	4  stop       6794667
	5  strand     -1
	6  phase      2
	7  end_phase  0


ifeq ($(call lt,$(VERSION),67),$(true))

define exon_transcript_sql
SELECT es.stable_id, ts.stable_id \
FROM exon_transcript AS et \
  LEFT JOIN exon_stable_id AS es ON et.exon_id = es.exon_id \
  LEFT JOIN transcript_stable_id AS ts ON et.transcript_id = ts.transcript_id
endef

else # VERSION >= 67

define exon_transcript_sql
SELECT e.stable_id, t.stable_id \
FROM exon_transcript AS et \
  LEFT JOIN exon AS e ON et.exon_id = e.exon_id \
  LEFT JOIN transcript AS t ON et.transcript_id = t.transcript_id
endef

endif

exon-transcript.map.gz: database.name
	$(MYSQL_CMD) $$(cat $<) <<<"$(exon_transcript_sql)" \
	| bsort -u \
	| gzip >$@

.META: exon-transcript.map.gz
	1  exon_ID        ENSMUSE00000097910
	2  transcript_ID  ENSMUST00000019896


introns.gz: exons.gz exon-transcript.map.gz 
	zcat $<\
	| translate -a -d <(zcat $(word 2,$^)) 1 \
	| expandsets -s ',' 2 \
	| bawk '{print $$3,$$4,$$5,$$2}' \
	| introns_coords \
	| bsort -u \
	| gzip > $@ 

.META: introns.gz
	1  chr           1
	2  start         141594868
	3  stop          141598375
	4  transcrit_ID  ENSMUST00000111986


## Translations (proteins)
##

ifeq ($(call lt,$(VERSION),67),$(true))

define translation_transcript_sql
SELECT ps.stable_id, ts.stable_id \
FROM translation AS p \
  LEFT JOIN translation_stable_id AS ps ON p.translation_id = ps.translation_id \
  LEFT JOIN transcript_stable_id AS ts ON p.transcript_id = ts.transcript_id \
WHERE ps.stable_id is not NULL AND \
  ts.stable_id is not NULL
endef

else # VERSION >= 67

define translation_transcript_sql
SELECT p.stable_id, t.stable_id \
FROM translation AS p \
  LEFT JOIN transcript AS t ON p.transcript_id = t.transcript_id \
WHERE p.stable_id is not NULL AND \
  t.stable_id is not NULL
endef

endif

translation-transcript.map.gz: database.name
	$(MYSQL_CMD) $$(cat $<) <<<"$(translation_transcript_sql)" \
	| bsort -u \
	| gzip >$@

.META: translation-transcript.map.gz
	1  translation_ID  ENSP00000000233
	2  transcript_ID   ENST00000000233


translation-gene.map.gz: translation-transcript.map.gz transcript-gene.map.gz
	zcat $< \
	| translate <(zcat $^2) 2 \
	| bsort -u \
	| gzip >$@

.META: translation-gene.map.gz
	1  translation_ID  ENSP00000000233
	2  gene_ID         ENSG00000004059


translation-xref.map.gz: database.name
	$(call xref,translation)

.META: translation-xref.map.gz
	1  translation_ID  ENSP00000000412
	2  database_ID     goslim_goa
	3  external_ID     GO:0005886
	4  external_name   GO:0005886
	5  external_descr  plasma membrane


## Repeats
##
repeats.gz: database.name
	$(MYSQL_CMD) $$(cat $<) <<<" \
		SELECT sr.name, seq_region_start-1, seq_region_end, seq_region_strand, repeat_name, repeat_class \
		FROM repeat_feature AS rf \
			LEFT JOIN repeat_consensus AS rc ON rf.repeat_consensus_id = rc.repeat_consensus_id \
			LEFT JOIN seq_region AS sr ON rf.seq_region_id = sr.seq_region_id \
		WHERE sr.name NOT LIKE '%\_%'" \
	| bsort -k1,1 -k2,2n \
	| uniq \
	| gzip -9 >$@

.META: repeats.gz
	1  chr     1
	2  start   3000001
	3  stop    3000156
	4  strand  -1
	5  name    L1_Mur2
	6  class   LINE/L1


## Other
##
gene-readable.map.gz: 
	sed 's/__SPECIES__/$(SPECIES)/g' < $(TASK_ROOT)/local/share/queries/mart_readable.xml > $@_query.xml
	wget -q -O - --post-file $@_query.xml $(MARTSERVICE_URL) \
	| gzip > $@
	rm $@_query.xml

.META: gene-readable.map.gz
	1  gene_id      ENSG00000208234
	2  description  cippo lippo
	3  gene_name    AC019043.8
	4  biotype      scRNA_pseudogene
	5  status       NOVEL

INTERMEDIATE += gene-omim.query
gene-omim.query: $(TASK_ROOT)/local/share/queries/mart_omim.xml
	sed 's/__SPECIES__/$(SPECIES)/g' <$< >$@

gene-omim.map.gz: gene-omim.query
	wget -q -O - --post-file $< $(MARTSERVICE_URL) \
	| bawk '$$2' \    *select only genes whit a omimi disease associated *
	| gzip >$@

gene-homologous.map.gz:
	(\
	for i in $$( $(MYSQL_CMD_MART) <<< "SHOW TABLES LIKE '$(SPECIES)_gene_ensembl__homolog_%'" | unhead -n 2 ); do \
		species=` echo $$i | sed 's/$(SPECIES)_gene_ensembl__homolog_//' | sed 's/__dm$$//'`; \
		$(MYSQL_CMD_MART) <<< 	"SELECT	\
		stable_id_4016_r3, stable_id_4016_r1, stable_id_4016_r2, description_4014 stable_id_4016_r1 FROM $$i WHERE stable_id_4016_r3 is not NULL or stable_id_4016_r2 is not NULL ;" \
		| append_each_row $$species; \
	done; \
	) | gzip > $@

gene-homologous.one2one.to_clean: gene-homologous.map.gz
	zcat $<\
	| bawk '$$5=="ortholog_one2one" {print $$4}'\
	| symbol_count | bawk '$$2>1' > $@

gene-homologous.one2one_clean.map.gz: gene-homologous.map.gz gene-homologous.one2one.to_clean
	zcat $< \
	| bawk '$$5=="ortholog_one2one" {print $$2,$$4,$$6}'\
	| filter_1col -v 2 <(cut -f 1 $^2) \
	| gzip > $@


## Upstreams
##

upstreams.gz: genes.gz $(SEQ_DIR)/chrs.len 
	zcat genes.gz \
	| upstreams $^2 $(UPSTREAM_EXTENSION) \
	| gzip > $@

nr_upstreams.gz: upstreams.gz genes.gz
	intersection -l $@.missing \
		<(zcat $< \
			| bawk '$$3 != 0' \					* in the MT there is this case *
			| nsort -k 2,2 -k3,3n \
		):2:3:4 \
		<(zcat $^2 | bsort -k2,2 -k3,3n):2:3:4 \
	| cut -f -9 \
	| bawk '{if($$9==1){print $$1,$$3,$$5,$$8,$$9 }else{ print $$1,$$4,$$2,$$8,$$9}}' \
	| bawk '{print $$0,$$3-$$2}' \
	| find_best -r 4 6 \    * the shortest *
	| cut -f -5 > $@.tmp
	cat $@.missing $@.tmp \
	| bawk '{print $$4,$$1,$$2,$$3,$$5}'| gzip > $@
	rm $@.missing $@.tmp


ALL          += genes.gz gene-xref.map.gz \
                transcripts.gz transcript-gene.map.gz transcript-xref.map.gz transcripts.gtf.gz \
                transcript_structures.gz exons.gz exon-transcript.map.gz \
                translation-transcript.map.gz translation-xref.map.gz
CLEAN        += gene-entrez.map.gz \
                transcript-refseq.map.gz \
	        translation-gene.map.gz \
                cdnas.gz introns.gz \
                upstreams.gz nr_upstreams.gz \
                repeats.gz
INTERMEDIATE += database.name transcript.info
