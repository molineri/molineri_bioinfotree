include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

SPECIES                ?= hsapiens
VERSION                ?= hg18
ALL_GENE_TRACKS        ?= known_genes refseq_genes ccds_genes mgc_genes aceview_genes other_refseq_genes genscan_genes exoniphy_genes geneid_genes nscan_genes sgp_genes
SEQ_DIR                ?= $(BIOINFO_ROOT)/task/sequences/dataset/ensembl/hsapiens/40
ENSEMBL_ANNOT_DIR      ?= $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/hsapiens/40
ALL_CHRS               ?= 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X Y
MIN_SCORE_FOR_FRACTION ?= 0
UPSTREAM_EXTENSION     ?= 15000

BIN_DIR                := $(BIOINFO_ROOT)/task/annotations/local/bin
COVERAGE_BIN_DIR       := $(BIOINFO_ROOT)/task/align_coverage/local/bin
DATABASE_URL           := http://hgdownload.cse.ucsc.edu/goldenPath/$(VERSION)/database
MYSQL                  := mysql -BCAN --user=genome --host=genome-mysql.cse.ucsc.edu $(VERSION)
COORDS_NONREDUNDANT    := C 5 3 I U N

all: all.gene_tracks.exons

import ucsc_gene_tracks
ALL_GENE_TRACKS_TARGETS:= $(addsuffix *,$(ALL_GENE_TRACKS))

define est_mrna_coord
	perl -lane '$$,="\t"; $$\="\n"; @block_size=split/,/,$$F[9]; @q_block_b=split/,/,$$F[10]; @t_block_b=split/,/,$$F[11]; for ($$i=0; $$i<scalar @block_size; $$i++) {$$q_block_e=$$q_block_b[$$i]+$$block_size[$$i]; $$t_block_e=$$t_block_b[$$i]+$$block_size[$$i]; print $$F[0],$$q_block_b[$$i],$$q_block_e,$$F[4],$$F[5],$$t_block_b[$$i],$$t_block_e;}' <$< \
	| sed 's|chr||' \
	| sort -S40% -k5,5 -k6,6n -k7,7n >$@
endef

define est_mrna_coord_best
	find_best -s -m 1 13 $< \
	| perl -lane '$$,="\t"; $$\="\n"; @block_size=split/,/,$$F[9]; @q_block_b=split/,/,$$F[10]; @t_block_b=split/,/,$$F[11]; for ($$i=0; $$i<scalar @block_size; $$i++) {$$q_block_e=$$q_block_b[$$i]+$$block_size[$$i]; $$t_block_e=$$t_block_b[$$i]+$$block_size[$$i]; print $$F[0],$$q_block_b[$$i],$$q_block_e,$$F[4],$$F[5],$$t_block_b[$$i],$$t_block_e;}' \
	| sed 's|chr||' \
	| sort -S40% -k5,5 -k6,6n -k7,7n >$@
endef

clean:
	rm -f $(ALL_GENE_TRACKS_TARGETS)

conservation.gz:
	wget -O - $(DATABASE_URL)/multiz17waySummary.txt.gz \
	| zcat \
	| cut -f 2-6 \
	| grep '^chr[0-9XY]*\s' \
	| sort -k 4,4 -k 1,1 -k 2,2n -k 3,3n -S80% \
	| awk '{printf "%s\t%i\t%i\t%s\t%i\n", $$1,$$2,$$3,$$4,$$5*1000 }' \
	| sed 's/^chr//' \
	| gzip > $@

conservation_phast.gz:
	wget -O - $(DATABASE_URL)/phastConsElements17way.txt.gz \
	| zcat \
	| cut -f 2-4,6 \
	| grep '^chr[0-9XY]*	' \
	| sed 's/^chr//' \
	| sort -k 1,1 -k 2,2n \
	| gzip > $@

conservation_both.gz: conservation_phast.gz conservation.gz
	(\
		zcat $< | awk 'BEGIN{OFS="\t"}{print $$1,$$2,$$3,"phast",$$4}'; \
		zcat $(word 2,$^);\
	) | gzip >$@

conservation_histogram.gz:
	wget -O - $(DATABASE_URL)/multiz17way.txt.gz \
	| zcat \
	| cut -f 1-4,7 \
	| grep '^chr[0-9XY]*\s' \
	| sort -k 1,1 -k 2,2n -k 3,3n -S80% \
	| gzip > $@

#############################
#
#	GENI
#
#############################

.PHONY: all.gene_tracks.exons

all.gene_tracks.exons: $(addsuffix .coords.exons,$(ALL_GENE_TRACKS))
	

$(SEQ_DIR)/chr%.fa.len:
	cd $(SEQ_DIR);	make chr$*.fa.len;

%.coords: %
	cat $< | awk 'BEGIN {FS="\t"} {print $$2 "\t" $$4 "\t" $$5 "\t" $$3 "\t" $$1}' \
	| sort -k1,1 -k2,2n > $@ 

%_genes.coords.coding: %_genes
	cat $< \
	| perl -lane '$$,="\t"; $$\="\n"; $$F[8]=~s/,^//g; $$F[9]=~s/,^//g; @ex_b=split /,/,$$F[8]; @ex_e=split /,/,$$F[9]; for (my $$i=0; $$i < scalar @ex_b; $$i++) { next if $$ex_e[$$i]<$$F[5]; next if $$ex_b[$$i]>$$F[6]; my $$b= $$ex_b[$$i] < $$F[5] ? $$F[5] : $$ex_b[$$i]; my $$e= $$ex_e[$$i] < $$F[6] ? $$ex_e[$$i] : $$F[6]; print $$F[1],$$b,$$e,$$F[0],"C" if ($$b < $$e); }' \
	| sort -k 1,1 -k2,2n \
	> $@

%_genes.coords.5utr: %_genes
	cat $< \
	| perl -lane '$$,="\t"; $$\="\n"; $$F[8]=~s/,^//g; $$F[9]=~s/,^//g; @ex_b=split /,/,$$F[8]; @ex_e=split /,/,$$F[9]; my $$utr_b=$$F[3]; my $$utr_e=$$F[5]; if ($$F[2]=~/^-$$/) {$$utr_b=$$F[6]; $$utr_e=$$F[4];} for (my $$i=0; $$i < scalar @ex_b; $$i++) {next if $$ex_e[$$i]<$$utr_b; next if $$ex_b[$$i]>$$utr_e; my $$b= $$ex_b[$$i] < $$utr_b ? $$utr_b : $$ex_b[$$i]; my $$e=$$ex_e[$$i] < $$utr_e ? $$ex_e[$$i] : $$utr_e; print $$F[1],$$b,$$e,$$F[0],"5" if ($$b < $$e);}' \
	| sort -k 1,1 -k2,2n \
	> $@

%_genes.coords.3utr: %_genes
	cat $< \
	| perl -lane '$$,="\t"; $$\="\n"; $$F[8]=~s/,^//g; $$F[9]=~s/,^//g; @ex_b=split /,/,$$F[8]; @ex_e=split /,/,$$F[9]; my $$utr_b=$$F[6]; my $$utr_e=$$F[4]; if ($$F[2]=~/^-$$/) {$$utr_b=$$F[3]; $$utr_e=$$F[5];} for (my $$i=0; $$i < scalar @ex_b; $$i++) {next if $$ex_e[$$i]<$$utr_b; next if $$ex_b[$$i]>$$utr_e; my $$b= $$ex_b[$$i] < $$utr_b ? $$utr_b : $$ex_b[$$i]; my $$e=$$ex_e[$$i] < $$utr_e ? $$ex_e[$$i] : $$utr_e; print $$F[1],$$b,$$e,$$F[0],"3" if ($$b < $$e);}' \
	| sort -k 1,1 -k2,2n \
	> $@

%_genes.coords.exons: %_genes.coords.5utr %_genes.coords.coding %_genes.coords.3utr
	cat $^ | sort -k 1,1 -k 2,2n > $@

%_genes.coords.introns: %_genes
	cat $< \
	| perl -lane '$$,="\t"; $$\="\n"; $$F[8]=~s/,^//g; $$F[9]=~s/,^//g; @ex_b=split /,/,$$F[8]; @ex_e=split /,/,$$F[9]; if ($$F[3]<$$ex_b[0]) {print $$F[1],$$F[3],$$ex_b[0],$$F[0],"I";} shift @ex_b; for (my $$i=0; $$i < scalar @ex_b; $$i++) {print $$F[1],$$ex_e[$$i],$$ex_b[$$i],$$F[0],"I" if ($$ex_e[$$i] < $$ex_b[$$i]);} print $$F[1],$$ex_e[$$#ex_e],$$F[4],$$F[0],"I" if ($$ex_e[$$#ex_e]<$$F[4]);' \
	| sort -k 1,1 -k2,2n \
	> $@

%_genes.coords.upstream: %_genes $(addsuffix .fa.len,$(addprefix $(SEQ_DIR)/chr,$(ALL_CHRS)))
	cat $< \
	| perl -lane '$$,="\t"; $$\="\n"; next if $$F[1] =~ /_/; my $$chr_len = `cat $(SEQ_DIR)/chr$$F[1].fa.len`; chomp $$chr_len; my $$up_b=0; my $$up_e=0; if ($$F[2]=~/^\+$$/) {$$up_b=$$F[3] - $(UPSTREAM_EXTENSION); $$up_e=$$F[3];} else {$$up_b=$$F[4]; $$up_e=$$F[4] + $(UPSTREAM_EXTENSION);} if ($$up_b < 0) {$$up_b=0;} if ($$up_e > $$chr_len) {$$up_e=$$chr_len;} print $$F[1],$$up_b,$$up_e,$$F[0],"U";' \
	| sort -k 1,1 -k2,2n \
	> $@

%.coords.coding.nonredundant: %.coords.coding
	cut -f -4 $< | union \
		| awk 'BEGIN {FS="\t"} {OFS="\t"} {print $$1,$$2,$$3,"C"}' > $@

%.coords.5utr.nonredundant: %.coords.5utr
	cut -f -4 $< | union \
		| awk 'BEGIN {FS="\t"} {OFS="\t"} {print $$1,$$2,$$3,"5"}' > $@

%.coords.3utr.nonredundant: %.coords.3utr
	cut -f -4 $< | union \
		| awk 'BEGIN {FS="\t"} {OFS="\t"} {print $$1,$$2,$$3,"3"}' > $@

%.coords.introns.nonredundant: %.coords.introns
	cut -f -4 $< | union \
		| awk 'BEGIN {FS="\t"} {OFS="\t"} {print $$1,$$2,$$3,"I"}' > $@

%.coords.exons.nonredundant: %.coords.coding.nonredundant %.coords.5utr.nonredundant %.coords.3utr.nonredundant
	cat $^ | sort -k 1,1 -k 2,2n > $@

%.coords.upstream.nonredundant: %.coords.upstream
	cut -f -4 $< | union \
		| awk 'BEGIN {FS="\t"} {OFS="\t"} {print $$1,$$2,$$3,"U"}' > $@


%.coords_nonredundant: $(addsuffix .fa.len,$(addprefix $(SEQ_DIR)/chr,$(ALL_CHRS))) %.upstream.nonredundant %.introns.nonredundant %.3utr.nonredundant %.5utr.nonredundant %.coding.nonredundant
	rm -f $@.tmp.*;
	(for i in $(ALL_CHRS); do \
		l=`cat $(SEQ_DIR)/chr$$i.fa.len`; \
		cat $*.upstream | awk "\$$1 ~ /^$$i$$/" > $@.tmp.U; \
		cat $*.introns  | awk "\$$1 ~ /^$$i$$/" > $@.tmp.I; \
		cat $*.3utr     | awk "\$$1 ~ /^$$i$$/" > $@.tmp.3; \
		cat $*.5utr     | awk "\$$1 ~ /^$$i$$/" > $@.tmp.5; \
		cat $*.coding   | awk "\$$1 ~ /^$$i$$/" > $@.tmp.C; \
		echo -e "$$i\t0\t$$l\tN" \
			| $(BIN_DIR)/coords_nonredundant.pl -a $@.tmp.U \
			| $(BIN_DIR)/coords_nonredundant.pl -a $@.tmp.I \
			| $(BIN_DIR)/coords_nonredundant.pl -a $@.tmp.3 \
			| $(BIN_DIR)/coords_nonredundant.pl -a $@.tmp.5 \
			| $(BIN_DIR)/coords_nonredundant.pl -a $@.tmp.C; \
	done) \
	| sort -k1,1 -k2,2n \
	> $@; \
	rm -f $@.tmp.*


#%.annote: % *.coords_nonredundant
#	cut -f -3 $* | sort -k 2,2n > $@.tmp
#	# file chr_b_e ordinato per b crescente
#	chr='$*'; chr=$${chr%%_*}; chr=$${chr##chr}; \
#	intersection $@.tmp $(word 2,$^) \
#	| annote_vettorini $(NORMALIZE_VETTORINI) -l '$(COORDS_NONREDUNDANT)' > $@
#	rm $@.tmp


########## snp ############

snp.gz:
	echo "SELECT \
		chrom, chromStart, chromEnd, name, strand, refUCSC, observed, molType, class, valid, func, locType, weight \
        	FROM snp128" \
	| $(MYSQL) | sed 's/^chr//' | gzip > $@

snp.reduced: snp.gz
	zcat $< | cut -f 1-4 | sort -k 1,1 -k 2,2n | union > $@

########## IDmapping #######

known_entrez:
	echo "SELECT DISTINCT \
                name, value \
                FROM knownToLocusLink" \
        | $(MYSQL) > $@

########## ESTs ############

#la tabella est su e` la stessa cosa di all_est
est:
	echo "SELECT DISTINCT \
		qName,qSize,qStart,qEnd,strand,tName,tSize,tStart,tEnd,blockSizes,qStarts,tStarts,matches \
		FROM all_est" \
	| $(MYSQL) | sort -k1,1 -S30% > $@

mrna:
	echo 	"SELECT DISTINCT \
                	qName,qSize,qStart,qEnd,strand,tName,tSize,tStart,tEnd,blockSizes,qStarts,tStarts,matches \
                FROM all_mrna" \
	| $(MYSQL) | sort -k1,1 -S30% > $@

cdna_info.gz:
	for i in organism library sex tissue development cell keyword description geneName productName; do\
		echo $$i;\
		echo "SELECT id, name FROM $$i" | $(MYSQL) > $$i.tmp;\
	done;
	wget -O - $(DATABASE_URL)/gbCdnaInfo.txt.gz | zcat \
	| cut -f 2,5,8,9,11-14,16-19,21 > $@ 
	j=3;\
	for i in organism library sex tissue development cell keyword description geneName productName; do\
		traduci -d $$i.tmp $$j< $@ > $@.tmp;\
		mv $@.tmp $@;\
		j=$$((j+1));\
	done;
	for i in organism library sex tissue development cell keyword description geneName productName; do\
		rm -f $$i.tmp;\
	done;
	gzip < $@ > $@.tmp
	mv $@.tmp $@
	#1	acc
	#2	type
	#3	organism
	#4	library
	#5	sex
	#6	tissue
	#7	development
	#8	cell
	#9	keyword
	#10	description
	#11	geneName
	#12	productName
	#13	gi

est.coords: est
	$(call est_mrna_coord)

mrna.coords: mrna
	$(call est_mrna_coord)

est.coords.best: est
	$(call est_mrna_coord_best)

mrna.coords.best: mrna
	$(call est_mrna_coord_best)

#############################
#
#	TRASPOSONI
#
#############################

repeat.coords.gz:
	( \
	if [ `echo "show tables like '%rmsk%';" | $(MYSQL) | wc -l` != 1 ]; then \
		for i in $(ALL_CHRS); do \
			echo "SELECT genoName, genoStart, genoEnd, repName, repClass, repFamily  FROM chr$${i}_rmsk" | $(MYSQL); \
		done; \
	else \
		echo "SELECT genoName, genoStart, genoEnd, repName, repClass, repFamily  FROM rmsk" | $(MYSQL); \
	fi;\
	) \
	| sed 's/^chr//' \
	| sort -k 1,1 -k 2,2n \
	| gzip > $@

transposon.coords.gz: repeat.coords.gz
	zcat $< \
	| awk '$$5=="DNA" || $$5=="LINE" || $$5=="LTR" || $$5=="SINE"' \
	| gzip > $@


coords_nonredundant_trasposon.gz: trasposon.coords.gz
	zcat $< | sort -S40% -k 1,1 -k 2,2n | union | gzip > $@

coords_nonredundant_repeat.gz: repeat.coords.gz 
	zcat $< | sort -S40% -k 1,1 -k 2,2n | union | gzip > $@

coords_nonredundant_trasposon.%: coords_nonredundant_trasposon.gz
	zcat $< | grep -w '^$*' > $@

coords_nonredundant_trasposon.all: $(addprefix coords_nonredundant_trasposon.,$(ALL_CHRS))

#############################
#
#	STATISTICA
#
#############################

.PHONY: overlapping_genes_coding
known_genes.overlapping_genes_coding: known_genes.coding.genes
	cat $< | awk 'BEGIN{OFS="\t"} {print $$4, $$2, $$3, $$1}' \
	| sort -k 1,1 -k 2,2n | union \
	|  awk 'BEGIN{OFS="\t"}{print $$4,$$2,$$3,$$1}' | sort -k 1,1 -k 2,2n > $@.tmp
	intersection $@.tmp $@.tmp | awk '$$8!=$$9' | cut -f 8,9 | tr "\t" "\n" | count
	rm $@.tmp

%.conserved_region: conservation.gz
	zcat $< \
	| sort -k 1,1 -S80% \
	| repeat_group_pipe 'cut -f 2,3 | $(COVERAGE_BIN_DIR)/coverage | $(COVERAGE_BIN_DIR)/coverage_trigger 1 >$$1.conserved_region' 1 > $@

all_conserved_fractions_species: conservation_both.gz $(addsuffix .fa.len,$(addprefix $(SEQ_DIR)/chr,$(ALL_CHRS)))
	zcat $< \
	| repeat_group_pipe ' \
		echo -ne "$$1\t$$2\t"; \
		cut -f 2,3 \
		| awk '\''$$$$5>$(MIN_SCORE_FOR_FRACTION)'\'' \
		| $(COVERAGE_BIN_DIR)/coverage \
		| $(COVERAGE_BIN_DIR)/coverage_trigger 1 \
		| awk '\''{sum += $$$$2-$$$$1} END {printf "%i\t", sum}'\''; \
		cat $(SEQ_DIR)/chr$$2.fa.len' 4 1 \
	| repeat_group_pipe 'echo -ne "$$1\t"; cut -f 3- | sum_column' 1 \
	| awk '{print $$1 "\t" $$2/$$3}' \
	| sort -k2,2nr > $@

all_conserved_fractions: $(addsuffix .conserved_region,$(addprefix chr,$(ALL_CHRS))) \
                         $(addsuffix .fa.len,$(addprefix $(SEQ_DIR)/chr,$(ALL_CHRS)))
	(\
		for i in $(filter %.conserved_region,$^); do \
			echo -ne "$${i%%.conserved_region}\t"; \
			awk '{sum += $$2-$$1} END{printf "%i\t", sum}' $$i; \
			cat $(SEQ_DIR)/$${i%%.conserved_region}.fa.len; \
		done;\
	) > $@

%.number: %
	cut -f 1 $< | sort | uniq | wc -l > $@
	cat $@

intersection_%_ensembl_left.distrib: % $(ENSEMBL_ANNOT_DIR)/coords_gene
	intersection2 $<:2:4:5 $(word 2,$^) \
	| awk '{print $$8 "\t" ($$3-$$2)/($$5-$$4)}' \
	| find_best 1 2 \
	| cut -f 2 \
	| awk '{print 2-$$1}' \
	| binner -n 25 -l > $@

intersection_%_ensembl_left.distrib.cum: % $(ENSEMBL_ANNOT_DIR)/coords_gene
	intersection2 $<:2:4:5 $(word 2,$^) \
	| awk '{print $$8 "\t" ($$3-$$2)/($$5-$$4)}' \
	| find_best 1 2 \
	| cut -f 2 \
	| binner -n 100 -b r \
	| cumulativa > $@

intersection_%_ensembl_right.distrib.cum: % $(ENSEMBL_ANNOT_DIR)/coords_gene
	intersection2 $<:2:4:5 $(word 2,$^) \
	| awk '{print $$8 "\t" ($$3-$$2)/($$7-$$6)}' \
	| find_best 1 2 \
	| cut -f 2 \
	| binner -n 100 -b r \
	| cumulativa > $@


common_gene_%_ensembl: % $(ENSEMBL_ANNOT_DIR)/coords_gene
	intersection2 $<:2:4:5 $(word 2,$^) \
	| awk '{print $$8 "\t" ($$3-$$2)/($$5-$$4)}' \
	| find_best 1 2 \
	| awk '$$2>0.05' > $@

cross_overlap_%_ensembl: % $(ENSEMBL_ANNOT_DIR)/coords_gene
	intersection2 $<:2:4:5 $(word 2,$^) \
	| awk '{print $$8 "\t" $$18 "\t" ($$3-$$2)/($$5-$$4)}' \
	| grep -v OTT \
	| sort -k 1,1 -k 3,3n \
	| uniq >$@

cross_overlap_%_ensembl.summary: cross_overlap_%_ensembl %.number
	a=`awk '$$3>0.5' $< | wc -l`;\
	b=`cat $(word 2,$^)`;\
	perl -e "print $$a/$$b" > $@
	echo >> $@
	cat $@


missing_gene_%_ensembl: common_gene_%_ensembl %
	cut -f 1 $< > $@.tmp
	grep -F -w -v -f $@.tmp $(word 2,$^) > $@
	rm $@.tmp

intersection_%_ensembl_right.distrib: % $(ENSEMBL_ANNOT_DIR)/coords_gene
	intersection2 $<:2:4:5 $(word 2,$^) \
	| awk '{print $$8 "\t" ($$3-$$2)/($$7-$$6)}' \
	| find_best 1 2 \
	| cut -f 2 \
	| awk '{print 2-$$1}' \
	| binner -n 25 -l > $@

intersection_%_ensembl.cum.ps: intersection_%_ensembl_right.distrib.cum intersection_%_ensembl_left.distrib.cum
	(\
		echo "set terminal postscript color"; \
		echo "set logscale y"; \
		echo "set grid mxtics mytics";\
		echo "set xrange [0:1.1]"; \
		echo "plot '$<' w l ti 'intersec/ucsc', '$(word 2,$^)' w l ti 'intersec/ensembORvega'" \
	)\
	| gnuplot > $@

intersection_%_ensembl.ps: intersection_%_ensembl_right.distrib intersection_%_ensembl_left.distrib
	(\
		echo "set terminal postscript color"; \
		echo "set logscale y"; \
		echo "set logscale x"; \
		echo "set xrange[1:2]";\
		echo "set grid mxtics mytics";\
		echo "plot '$<' w l ti 'intersec/ucsc', '' w p pt 6, '$(word 2,$^)' w l ti 'intersec/ensembORvega', '' w p pt 6" \
	)\
	| gnuplot > $@

%.png: %.ps
	convert -rotate 90 $< $@
