TAXONOMY_DIR = $(TASK_ROOT)/dataset/ncbi/taxonomy/090214
GO_SPECIES_INDIPENDENT_DIR ?= $(TASK_ROOT)/dataset/GO/geneontology.org/110110
SPECIES = hsapiens mmusculus drerio

extern $(TAXONOMY_DIR)/names as TAX_NAMES
extern $(GO_SPECIES_INDIPENDENT_DIR)/go_tree 
extern $(GO_SPECIES_INDIPENDENT_DIR)/go_term

ALL += gene2go.gz $(addsuffix /gene2go.inclusive, $(SPECIES))

gene2go.gz:
	wget -O - ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/$@ \
	| zcat | unhead | gzip > $@

.META: gene2go.gz
	1 tax_id
	2 GeneID
	3 GO_ID
	4 Evidence
	5 Qualifier
	6 GO term
	7 PubMed
	8 Category

%/gene2go: gene2go.gz $(TASK_ROOT)/local/share/species-ncbi.map $(TAX_NAMES)
	mkdir -p $*
	TAXID=$$(filter_1col 3 <(bawk '$$1=="$*" {print $$2}' $^2) < $^3 | cut -f 1);\
	bawk "\$$tax_id == $$TAXID {print \$$GeneID, \$$GO_ID, \$$Evidence, \$$Category}" $< > $@

%/gene2go.inclusive: %/gene2go $(GO_SPECIES_INDIPENDENT_DIR)/go_tree $(GO_SPECIES_INDIPENDENT_DIR)/go_term
	cut -f 1,2 $< \
	| filter_1col -v 2 <(bawk '$$level==1 {print $$go_id}' $^3) \    * removing molecular_function cellular_component biological_process terms *
	| translate -a -j -d -n <(cut -f 1,2 $^2 | sort | uniq) 2 \
	| awk '{print $$1 "\t" $$2 "\n" $$1 "\t" $$3}' \
	| bsort -k1,1 | uniq > $@

%/gene2go.inclusive.sorted: %/gene2go.inclusive
	bawk '{print $$2,$$1}' $< | bsort -k1,1 >$@

#direct_annotation: $(NCBI_DIR)/gene2go.gz
#	zcat $< > $(NCBI_DIR)/gene2go.tmp
#	bawk '{if ($$1 == $(TAX_ID)) print $$2, $$3}' $(NCBI_DIR)/gene2go.tmp > $@
#	rm $(NCBI_DIR)/gene2go.tmp

%/gene2go.inclusive.altern: %/gene2go $(GO_SPECIES_INDIPENDENT_DIR)/go.term $(GO_SPECIES_INDIPENDENT_DIR)/go.anc $(GO_SPECIES_INDIPENDENT_DIR)/go.alt $(GO_SPECIES_INDIPENDENT_DIR)/go.obs
	inclusive03 -t $^2 -a $^3 -A $^4 -o $^5 <(cut -f 1,2 $<) > $@
