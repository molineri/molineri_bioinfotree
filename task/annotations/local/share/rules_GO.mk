include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

ENS_VERSION ?= 42
ENS_ANNOTE_DIR ?= $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/$(SPECIES)/$(ENS_VERSION)
BIN_DIR ?= $(BIOINFO_ROOT)/task/annotations/local/bin
SPECIES ?= hsapiens
MYSQL_MART ?= mysql -BCAN -u anonymous -h martdb.ensembl.org -P 3316 ensembl_mart_$(ENS_VERSION)
MYSQL_ENS  ?= mysql -BCAN -u anonymous -h ensembldb.ensembl.org ensembl_go_$(ENS_VERSION)
MARTSERVICE_URL = http://www.ensembl.org/biomart/martservice
EBI_MULTISPECIES_DIR = $(BIOINFO_ROOT)/task/annotations/dataset/GO/ebi/$(EBI_VERSION)

all: ensg_go.inclusive.gz


extern $(EBI_MULTISPECIES_DIR)/go_tree as GO_TREE
extern $(EBI_MULTISPECIES_DIR)/go_term as GO_TERM

ensg_go.gz:
	sed 's/__SPECIES__/$(SPECIES)/g' < $(TASK_ROOT)/local/share/queries/mart_go.xml > $@_query.xml
	wget -q -O - --post-file $@_query.xml $(MARTSERVICE_URL) \
	| bawk '$$2' \
	| gzip >$@
	rm $@_query.xml

ensg_go.inclusive.gz: ensg_go.gz $(GO_TREE) $(GO_TERM)
	zcat $< \
	| filter_1col -v 2 <(bawk '$$4==1 {print $$1}' $^3) \       * removing non translable molecular_function cellular_component biological_process *
	| cut -f 1,2 \
	| translate -v -e NA -a -j -d -n <(cut -f 1,2 $^2 | sort | uniq) 2 \     *NA are caused by ensembl and ebi version mismatch*
	| awk '{print $$1 "\t" $$2 "\n" $$1 "\t" $$3}' \
	| bsort -S 20% | uniq \
	| gzip > $@

.META: ensg_go.inclusive.gz
	1	ENSG
	2	GO

go_region: ensg_go.riannot $(ENS_ANNOTE_DIR)/coords_gene
	join3_pl -i -u -2 5 <(sort -k 1,1 $<) <(grep -F ENS $(word 2,$^) | sort -k 5,5) \
	| awk 'BEGIN{OFS="\t"}{print $$3,$$4,$$5,$$6,$$1,$$2}' \
	| sort -k 1,1 -k 2,2n -k 3,3n > $@

#per avere anche la provenienza dell'annotazione:
# cat ensg_go | join3_pl - -1 2 -2 2 -i -u -- go_tree

%.ENS_family_go: % ensg_go.riannot
	cat $* | $(BIN_DIR)/join_annotation.pl -a $(word 2,$^) -tmp $@.tmp $(DISPLAY_GENES) > $@
	rm $@.tmp

#%.family_go: % ensg_go.riannot
#	$(BIN_DIR)/join_annotation.pl -a ensg_go.riannot -n $(DISPLAY_GENES) < $< > $@ 


ENSG_EntrezGene:
	echo "SELECT DISTINCT gene_stable_id,dbprimary_id FROM $(SPECIES)_gene_ensembl__xref_entrezgene__dm WHERE dbprimary_id IS NOT NULL;" \
	| $(MYSQL_MART) \
	| sort -k1,1 \
	> $@

