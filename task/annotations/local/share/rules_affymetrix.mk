NCBI_DIR_MULTISPECIE = $(BIOINFO_ROOT)/task/annotations/dataset/ncbi/genes/pack/$(NCBI_VERSION)
NCBI_DIR = $(BIOINFO_ROOT)/task/annotations/dataset/ncbi/genes/$(SPECIES)/$(NCBI_VERSION)

ALL += probe-entrez.map

annot.csv.zip: 
	@ echo "#wget -O $@ https://www.affymetrix.com/Auth/analysis/downloads/$(ANNOT_VERSION)/ivt/$(VERSION).csv.zip"
	@ echo "It doesn't work, you must be logged"

annot.csv: annot.csv.zip
	unzip $<
	rm -f 3prime-IVT.AFFX_README.NetAffx-CSV-Files.txt
	mv $(VERSION).csv $@
	touch $@
	@ echo touch because unzipped file has an old date

probe-entrez.map: annot.csv $(NCBI_DIR_MULTISPECIE)/gene_history.gz $(NCBI_DIR)/genes.gz
	zcat $(word 2,$^) > $@.tmp.gene_history
	zcat $(word 3,$^) > $@.tmp.gene_info
	affymetrix_annotation $< $@.tmp.gene_history $@.tmp.gene_info $@.tmp $(TAXONOMY_ID)
	unhead -n 1 $@.tmp \
	| sed 's/\t/@/' | tr "\t" ";" | sed 's/@/\t/' > $@
	cat $@.tmp | sed 's/\t/@/' | tr "\t" ";" | sed 's/@/\t/' | perl -ane 'next if ($$F[1] =~ /\-/); print "$$F[0]\t$$F[1]\n"'> $@_entrez_annoted
	rm $@.tmp*

.META: probe-entrez.map
	1	ProbeSetID
	2	EntrezGene

probe-ensembl.map: annot.csv
	grep -v '^#' $< \
	| sed 's/","/\t/g; s/^"//; s| /// |;|g' \
	| cut -f 1,18 | expandsets 2 | bawk '$$2!="---"' > $@

gene_present: probe-entrez.map_entrez_annoted
	cut -f 2 $< | expandsets 1 | bsort | uniq > $@
