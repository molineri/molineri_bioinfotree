include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk
include $(BIOINFO_ROOT)/task/annotations/local/share/ensembl_common.mk

ENS_VERSION ?= 43
SPECIES ?= hsapiens
BIN_DIR ?= $(BIOINFO_ROOT)/task/annotations/local/bin

.DELETE_ON_ERROR:

Pfam_ls.gz:
	wget ftp://ftp.sanger.ac.uk/pub/databases/Pfam/current_release/Pfam_ls.gz

gene_pfam:
	echo "SELECT DISTINCT gene_stable_id,pfam_list FROM hsapiens_gene_ensembl__prot_pfam__dm WHERE pfam_list IS NOT NULL;" \
	|$(MYSQL_MART) $(ENS_MART) \
	| sort -k1,1 > $@

%.family_pfam: % gene_pfam
	cat $* | $(BIN_DIR)/join_annotation.pl -a $(word 2,$^) -tmp $@.tmp $(DISPLAY_GENES) > $@
	rm $@.tmp

pfam_acc_desc: Pfam_ls.gz
	zcat $< | $(BIN_DIR)/pfam_acc_desc.pl \
		| sort -k1,1 > $@

%.family_pfam.desc: %.family_pfam pfam_acc_desc
	cut -f -4 $< | sort -k2,2 \
		| join3_pl - -i -u -1 2 -2 1 -- $(word 2,$^) \
		| sort_pl 3 \
	> $@
