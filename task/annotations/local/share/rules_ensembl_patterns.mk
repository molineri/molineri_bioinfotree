SHELL := /bin/bash

BIN_DIR := $(BIOINFO_ROOT)/task/annotations/local/bin

SPECIES ?= hsapiens
ENS_VERSION ?= 40
ANNOTATION_DIR ?= $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/$(SPECIES)/$(ENS_VERSION)

REGIONS_DIR ?= $(BIOINFO_ROOT)/task/regions_characterisation/dataset/hsapiens_hsapiens_ensembl40_wublast_pam10/0
PARSED3 ?= regions.wublast.0.99.conn_comp.commty2.conn_comp.consensus_matrix.out.patterns.genome.wublast.0.8.parsed3
REGIONS_FILE ?= $(REGIONS_DIR)/$(PARSED3)
TARGET_PREFIX ?= regions.0.99

COORDS_NONREDUNDANT ?= C E 5 3 I U N
NORMALIZE_VETTORINI ?= -r

ALL_CHR ?= 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X Y


$(TARGET_PREFIX).annote: $(REGIONS_FILE) $(addprefix $(ANNOTATION_DIR)/coords_nonredundant.,$(ALL_CHR))
	repeat_group_pipe 'echo ">$$1"; cut -f -3' 4 < $< \
	| repeat_fasta_pipe -n '\
		echo ">$$HEADER"; sort -k 1,1 -k 2,2n \
		| repeat_group_pipe '\'' \
			intersection - $(ANNOTATION_DIR)/coords_nonredundant.$$1 \
	                | annote_vettorini $(NORMALIZE_VETTORINI) -l "$(COORDS_NONREDUNDANT)" \
		'\'' 1; \
	' > $@

$(TARGET_PREFIX).annote.bool: $(TARGET_PREFIX).annote
	repeat_fasta_pipe -n '\
		echo ">$$HEADER"; awk "BEGIN{OFS=\"\t\"} {for (i=4; i<=NF; i++) {if (\$$i>0) {\$$i=1}} print}" \
	' < $< > $@

$(TARGET_PREFIX).annote.sum: $(TARGET_PREFIX).annote.bool
	grep -v '>' $< | cut -f 4- | sum_column -n > $@

$(TARGET_PREFIX).annote.bool.overrepr: $(TARGET_PREFIX).annote.bool $(TARGET_PREFIX).annote.sum
	repeat_fasta_pipe -n "\
		echo -ne \"\$$HEADER\t\"; \
		cut -f 4- | sum_column -c | $(BIN_DIR)/annot_binomial.pl $(word 2,$^) \
	" < $< > $@

$(TARGET_PREFIX).annote.bool.overrepr.Bonferroni: $(TARGET_PREFIX).annote.bool.overrepr
	correzione=$$(($$(wc -l < $<) * 7)); \
	perl -lane "\$$\=\"\n\"; \$$,=\"\t\"; my \$$c=shift @F; @F = map {\$$_= \$$_ * $$correzione; \$$_=sprintf (\"\%.2e\",\$$_) } @F; print \$$c,@F" < $< > $@
	# per ogni label (COORDS_NONREDUNDANT): pvalue Bonferroni corretto, segno - se coda sx, segno + se coda dx
