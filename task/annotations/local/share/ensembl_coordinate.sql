SELECT DISTINCT
	a.exon_stable_id,
	a.exon_chrom_start - 1,
	a.exon_chrom_end,
	a.exon_chrom_strand,
	a.transcript_stable_id,
	a.gene_stable_id,
	a.chr_name,
	a.transcript_chrom_start - 1,
	a.transcript_chrom_end,
	a.transcript_chrom_strand,
	a.coding_start - 1,
	a.coding_end,
	a.5utr_start - 1,
	a.5utr_end,
	a.3utr_start - 1,
	a.3utr_end,
	a.biotype,
	a.mart_gene_type,
	b.gene_confidence,
	a.phase
FROM SPECIES_gene_ensembl_structure__structure__main as a
JOIN SPECIES_gene_ensembl__gene__main as b
	ON (a.gene_id_key = b.gene_id_key)
WHERE a.chr_name IN (CHRS);

