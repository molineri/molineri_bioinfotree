include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

FTP_PREFIX = ftp://ftp.informatics.jax.org/pub/reports

ANNOT_DIR = $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/hsapiens/$(ENS_VERSION)

all: annot riannot riannot.fa MGI_PhenoGenoMP.rpt MRK_Ensembl_Pheno.rpt VOC_MammalianPhenotype.rpt

clean:
	rm -f MPheno_OBO.ontology mp.dag mp.ancestors MGI_PhenoGenoMP.rpt MRK_Ensembl_Pheno.rpt VOC_MammalianPhenotype.rpt mgi2ensmus.map mp2mgi.map
clean.full: clean
	rm -f annot riannot annot.fa riannot.fa
	
MPheno_OBO.ontology:
	wget $(FTP_PREFIX)/$@

%.rpt:
	wget $(FTP_PREFIX)/$@

mp.dag: MPheno_OBO.ontology
	grep -v part_of $< \
	| awk -F" " '$$1=="id:" || $$1=="is_a:" {printf $$2 "\t"} $$1=="[Term]" {printf "\n"}' \
	| perl -lane '$$child = shift(@F); for(@F){print "$$child\t$$_"}' > $@

mp.level: mp.dag
	shortest_pats_len MP:0000001 < $< >$@

mp.first_level_projection: mp.dag mp.level
	shortest_pats_len -d -r -f <(awk '$$3==2 {print $$2}' $(word 2,$^)) < $<\
	| awk '{print $$2,$$1,$$3}' > $@

mp.ancestors: mp.dag
	dag2ancestors $< > $@

mp2mgi.map: MGI_PhenoGenoMP.rpt
	cut -f 3,5 $< | expandsets -s ',' 2 | sort | uniq >$@
	
mgi2ensmus.map: MRK_Ensembl_Pheno.rpt
	grep -F -v "no phenotypic analysis" $< | cut -f 1,4 | expandsets -s ',' 2 | sort | uniq >$@

mp2readable.map: VOC_MammalianPhenotype.rpt 
	perl -lane '$$\="\n"; s/\s+$$//; print' $< > $@

mp-description: mp.first_level_projection mp2readable.map 
	traduci -k -d -m "|" -a $< 1 < $(word 2,$^)\
	| expandsets -s ',' 2 \
	| awk '{print $$1,$$3,$$4,$$2}' \
	| tr '|' "\t" \
	| traduci -a <( cut -f -2 $(word 2,$^)) 4 > $@
#1	mp_id	MP:0000003
#2	short_desc	abnormal adipose tissue morphology
#3	long_desc	structural anomaly of the connective tissue composed of fat cells enmeshed in areolar tissue
#4      first_level_ancestor	MP:0005375
#5      first_level_ancestor_desc	adipose tissue phenotype
#6      level	2

annot: mp2mgi.map mgi2ensmus.map mp2readable.map
	traduci -k -a -d -n $< 1 < $(word 2,$^) \
	| expandsets -s ',' 2 \
	| traduci -a <(cut -f 1,2 $(word 3,$^)) 2 \
	| sort | uniq > $@
	@echo -e "\n\n\n\til primo traduci -k elimina 55 geni che hanno MGI non presenti in mp2mgi.map, non si capisce perche'\n\n"

#hsapiens_annot.pre: HMD_HumanPhenotype.rpt $(ANNOT_DIR)/ensg2any
#	perl -lane 's/\t +/\t/g; print ' $< | tr " " , \
#	| traduci -v -e NULL -a -d <(awk '$$4=="EntrezGene" {print $$5,$$1}' $(word 2,$^) | sort | uniq) 2 > $@

#hsapiens_annot: hsapiens_annot.pre mp2readable.map
#	awk '$$6' $< | expandsets -s ',' 6 | awk '{if($$3=="NULL"){$$3="NULL_" $$2} print $$5,$$6,$$3,$$2}' \
#	| traduci -a <( cut -f -2 $(word 2,$^) ) 2 > $@


riannot: annot mp.ancestors mp2readable.map
	traduci $(word 2,$^) 2 < $< \
	| expandsets -s ',' 2 \
	| cut -f -2,4- \
	| grep -F -v MP:0000001 \
	|  traduci -a <(cut -f 1,2 $(word 3,$^)) 2 > $@

mp.count: riannot
	cut -f 2,4 $< | sort | uniq | cut -f 1 | symbol_count > $@

%annot.fa: %annot
	sort -k 2,2 $< | repeat_group_pipe 'echo ">$$1 $$2"; cut -f 1,4-' 2 3 > $@
