###############
#
#	vecchia versione non piu` funzionante a causa dele modifiche alle interfacce apportate da ensebml
#
include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

ENS_VERSION ?= 42
MYSQL_ENS  ?= mysql -BCAN -u anonymous -h ensembldb.ensembl.org -P 5306 ensembl_go_$(ENS_VERSION)
#MYSQL_ENS ?= mysql -hmysql.ebi.ac.uk -ugo_select -pamigo -P4085 go_latest

all: go_tree go_term

.DELETE_ON_ERROR:

go_tree:
	echo "SELECT DISTINCT t1.acc, t2.acc FROM graph_path as g LEFT JOIN term as t1 on t1.id=g.term1_id LEFT JOIN term as t2 on t2.id=g.term2_id where g.distance>0 and t1.acc!='all' and t2.acc!='all';" \
	| $(MYSQL_ENS) \
	| bsort -k2,2 > $@

.META: go_tree
	1	go_id_ancestor
	2	go_id

go_tree.with_distance:
	echo "SELECT DISTINCT t1.acc, t2.acc, g.distance FROM graph_path as g LEFT JOIN term as t1 on t1.id=g.term1_id LEFT JOIN term as t2 on t2.id=g.term2_id where g.distance>0 and t1.acc!='all' and t2.acc!='all';" \
	| $(MYSQL_ENS) > $@

.META: go_tree
	1	go_id_ancestor
	2	go_id


go_term:
	echo "SELECT DISTINCT t2.acc, t2.name, t2.term_type, MIN(g.distance) FROM graph_path as g LEFT JOIN term as t1 on t1.id=g.term1_id LEFT JOIN term as t2 on t2.id=g.term2_id WHERE g.distance>0 AND t1.is_root=1  GROUP BY t2.acc" \
	| $(MYSQL_ENS) \
	> $@

.META: go_term
	1	go_id	GO:0000001
	2	desc	mitochondrion inheritance
	3	ontology	biological_process
	4	level	6

