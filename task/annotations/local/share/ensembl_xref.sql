select 
	gene_stable_id.stable_id, 
	xref.display_label,
	synon.synonym, 
	xref.description,
	external_db.db_name
from xref 
join object_xref on xref.xref_id = object_xref.xref_id 
JOIN external_db on external_db.external_db_id = xref.external_db_id 
JOIN gene_stable_id on gene_stable_id.gene_id = object_xref.ensembl_id 
LEFT JOIN external_synonym synon on synon.xref_id = xref.xref_id 
WHERE external_db.db_name IN(
	'CCDS',
	'EMBL',
	'EntrezGene',
	'HGNC',
	'IMGT/GENE_DB',
	'IMGT/LIGM_DB',
	'IPI',
	'MIM_GENE',
	'MIM_MORBID',
	'OTTP',
	'OTTT',
	'PDB',
	'RefSeq_dna',
	'RefSeq_dna_predicted',
	'RefSeq_peptide',
	'RefSeq_peptide_predicted',
	'UniGene',
	'Uniprot/SPTREMBL',
	'Uniprot/SWISSPROT',
	'Uniprot/Varsplic',
	'protein_id',
	'shares_CDS_and_UTR_with_OTTT',
	'shares_CDS_with_OTTT'
)
