include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

#STANDARD_SORT = sort -S80% -u -k 1,1 -k 2,2n # -u provoca un qualche problema, spariscono dei record
SHELL:=/bin/bash
BIN_DIR = $(BIOINFO_ROOT)/task/annotations/local/bin
SEQUENCE_DIR = $(BIOINFO_ROOT)/task/sequences/dataset/ensembl/$(SPECIES)/$(ENS_VERSION)
STANDARD_SORT = sort -S40% -k 1,1 -k 2,2n | uniq 
ENS2OMIM_DISORDER = ../../../ncbi/omim/070118/ens2disorders
COMMA=,

AFFY_DATASET = 'AFFY_HC_G110', 'AFFY_HG_Focus', 'AFFY_HG_U133A', 'AFFY_HG_U133A_2', 'AFFY_HG_U133B', 'AFFY_HG_U133_Plus_2', 'AFFY_HG_U95A', 'AFFY_HG_U95Av2', 'AFFY_HG_U95B', 'AFFY_HG_U95C', 'AFFY_HG_U95D', 'AFFY_HG_U95E', 'AFFY_HuGeneFL', 'AFFY_U133_X3P'

include $(BIOINFO_ROOT)/task/annotations/local/share/ensembl_common.mk

.PHONY: vedi_campi_ensembl_coordinate SECONDARY

.SECONDARY:
.DELETE_ON_ERROR:
	

all: coords_nonredundant.all

##
## Query ensembl
##

ensembl_latest_version:
	echo 'show databases like "%ensembl_mart%";' | $(MYSQL_MART) 

ensembl_coordinate.tab.gz: $(BIOINFO_ROOT)/task/annotations/local/share/ensembl_coordinate.sql $(BIOINFO_ROOT)/task/annotations/local/share/vega_coordinate.sql
	chrs="$(addprefix $(COMMA)',$(addsuffix ',$(ALL_CHR)))"; \
	chrs=`sed s/^,// <<<$$chrs`; \
	sed 's/SPECIES/$(SPECIES)/' < $(word 1, $^) \
	| sed "s/CHRS/$$chrs/" \
	| $(MYSQL_MART) $(ENS_MART)  > $@.tmp
	if [ '$(VEGA_MART)' != '' ]; then\
		sed 's/SPECIES/$(SPECIES)/' < $(word 2, $^) | $(MYSQL_MART) $(VEGA_MART) >> $@.tmp;\
	fi;
	if [ "$(NOMIRNA)" == 1 ]; then \
		grep -v -w 'miRNA' $@.tmp > $@.tmp2;\
		mv $@.tmp2 $@.tmp;\
	fi;
	sort -k7,7 -k2,2n -S80% $@.tmp | gzip > $@ 
	rm $@.tmp

ensembl_xrefs: $(BIOINFO_ROOT)/task/annotations/local/share/ensembl_xref.sql
	$(MYSQL_CORE) $(ENS_CORE) < $< >$@

exon2gene: ensembl_coordinate.tab.gz
	zcat $< | cut -f 1,6 >$@


ensg2any.pre:
	echo "SELECT\
			object_xref.ensembl_id,\
			object_xref.ensembl_object_type,\
			xref.display_label,\
			synon.synonym,\
			xref.description,\
			external_db.db_name,\
			xref.dbprimary_acc\
		FROM xref\
		INNER JOIN object_xref on xref.xref_id = object_xref.xref_id\
		LEFT JOIN external_db on external_db.external_db_id = xref.external_db_id\
		LEFT JOIN external_synonym synon on synon.xref_id = xref.xref_id \
		WHERE \
			xref.display_label is not NULL \
			AND\
			external_db.db_name not in ('GO',$(AFFY_DATASET),'Illumina_V1','Illumina_V2' )" \
	| $(MYSQL_CORE) $(ENS_CORE) \
	| bawk '{print $$1 "_" $$2,$$3,$$5,$$6,$$7; if($$4!="NULL"){print $$1 "_" $$2,$$4,$$5,$$6,$$7}}' \
	| sort | uniq > $@

stableid2any: ensg2any.pre id2stable_id
	traduci <(bawk '{print $$1"_"$$2,$$3}' $(word 2,$^)) 1 < $< > $@

ensg2ensp:
	echo "SELECT\
			gi.stable_id, \
			pi.stable_id \
		FROM gene_stable_id as gi \
		INNER JOIN transcript as t on gi.gene_id = t.gene_id\
		INNER JOIN translation as p on t.transcript_id = p.transcript_id \
		INNER JOIN translation_stable_id as pi on p.translation_id = pi.translation_id" \
	| $(MYSQL_CORE) $(ENS_CORE) > $@
	

ensg2any: stableid2any ensg2ensp ensembl_coordinate.tab.gz
	traduci -n -v $(word 2,$^) 1 < $< \
	| traduci -v <(zcat $(word 3,$^) | cut -f 5,6 | sort | uniq) 1 | sort | uniq > $@

ensg2readable: ensg2any
	filter_1col 4  <(echo -e "HGNC\nEntrezGene") < $< >$@

id2stable_id:
	(\
	for i in gene transcript translation; do\
		echo select $${i}_id, "'$$i'", stable_id FROM $${i}_stable_id WHERE $${i}_id is not NULL and stable_id is not NULL";";\
	done;\
	) | $(MYSQL_CORE) $(ENS_CORE) \
	| perl -lane '$$,="\t"; $$\="\n"; $$F[1]=ucfirst($$F[1]); print @F' > $@


homol_gene_homo_mouse:
	echo 	"SELECT DISTINCT \
			gene_stable_id,\
			homol_stable_id,\
			description \
		FROM\
			hsapiens_gene_ensembl__homologs_mmusculus__dm\
		WHERE \
			gene_stable_id is not NULL \
			and homol_stable_id is not NULL \
			and description is not NULL" \
	| $(MYSQL_MART) $(ENS_MART) > $@

paralogous_genes:
	echo 	"SELECT DISTINCT \
			gene_stable_id, \
			homol_stable_id, \
			subtype, \
			peptide_perc_pos, \
			peptide_perc_id, \
			homol_peptide_perc_pos, \
			homol_peptide_perc_id \
		FROM hsapiens_gene_ensembl__paralogs_hsapiens__dm \
		WHERE biotype = 'protein_coding'" \
	| $(MYSQL_MART) $(ENS_MART) \
	| awk -F "\t" 'BEGIN{OFS="\t"}{if($$1>$$2){print $$0;}else{print $$2,$$1,$$3,$$4,$$5,$$6,$$7}}' \
	| perl -lane '$$k=$$F[0]."_".$$F[1]; print if not defined($$a{$$k}); $$a{$$k}++' > $@

pseudogene.coords: coords_gene_biotype
	grep -F pseudogene $<  | tr "," "\t" | cut -f 1-3,5 > $@

processed_pseudogene_vega:
	echo "SELECT DISTINCT\
			gene_stable_id, \
			chr_name, \
			gene_chrom_start, \
			gene_chrom_end, \
			chrom_strand, \
			description \
		FROM hsapiens_gene_vega__gene__main \
		WHERE \
			biotype='processed_pseudogene' \
			AND description REGEXP '([[:upper:]]+[[:digit:]]*[[:alnum:]]*)' \
	" | mysql -BCAN -u anonymous -h martdb.ensembl.org -P 3316 $(VEGA_MART) \
	| perl -lne 's/\\\\n\\\\\s*$$//; print' > $@


processed_pseudogene_vega_hugo: processed_pseudogene_vega
	perl -lne '$$,="\t"; $$\="\n"; @F=split /\t/,$$_; @G = ($$F[5]=~/\(([A-Z]+\d*[A-Z]*)\)/g ); $$hugo=""; for(@G){if(length > length $$hugo){$$hugo=$$_}} print $$F[0],$$hugo,$$F[5] if $$hugo' $< \
	| sort | uniq > $@

processed_pseudogene_vega_hugo_ensg: ensg2hugo processed_pseudogene_vega_hugo
	traduci_tab2 -r  $< $(word 2,$^) \
	| perl -lne '@F=split /\t/,$$_; if ($$F[2] !~ /^ENS/) {splice @F,2,0,""} print join "\t",@F;' > $@



##
## Masks
##
.PHONY: all.masks

all.masks:
	for i in $(ALL_CHR); do \
		$(MAKE) chr$$i.masks; \
	done

chr%.masks:
	cd $(SEQUENCE_DIR)_nrm && $(MAKE) chr$*.fa;
	$(BIN_DIR)/masked_regions $(SEQUENCE_DIR)_nrm/chr$*.fa >$@ 

##
## Repeats
##
.PHONY: all.repeats coords_nonredundant_trasposon.all

all.repeats:
	for i in $(ALL_CHR); do \
		$(MAKE) chr$$i.repeats; \
	done

chr%.repeats:
	set -e;\
	source ensembl-api$(ENS_VERSION)-env; \
	chr=$*; \
	$(BIN_DIR)/fetch_repeats $(SPECIES_LONG) $${chr##chr} | \
		awk -F'\t' '{ if ($$5~/ranspo/ || $$5~/LTR/) {subtype="transposon"} else {subtype="simple"}; print $$1 "\t" $$2 "\t" subtype ":" $$3}' > $@

coords_trasposon: $(addsuffix .repeats,$(addprefix chr,$(ALL_CHR)))
	for i in $(ALL_CHR); do \
		awk "\$$3~/^transp/ {print \"$$i\t\" \$$0}" chr$$i.repeats >> $@;\
	done
	
#coords_nonredundant_trasposon: coords_trasposon
#	$(BIN_DIR)/merge_segments.pl $< > $@

coords_nonredundant_trasposon: coords_trasposon
	sort -S40% -k 1,1 -k 2,2n $< | union > $@

coords_nonredundant_trasposon.%: coords_nonredundant_trasposon
	grep -w '^$*' coords_nonredundant_trasposon > $@

coords_nonredundant_trasposon.all: $(addprefix coords_nonredundant_trasposon.,$(ALL_CHR))
	
###############

.PHONY: coords_nonredundant.all


gene_biotype_statistics: ensembl_coordinate.tab.gz
	zcat $< | awk -F "\t" '{print $$6 "\t" $$17}' | perl -e '\
		$$\="\n";\
		while(<>){\
			@F=split;\
			$$map{$$F[0]}=$$F[1]\
		}\
		for(keys(%map)){print $$map{$$_}}\
	'\
	| sort | uniq -c > $@


coords_exon: ensembl_coordinate.tab.gz
	zcat $< | awk '{print $$7 "\t" $$2 "\t" $$3 "\t" $$1}' | grep -v -F NULL | $(STANDARD_SORT) > $@

coords_nc_exon: ensembl_coordinate.tab.gz
	zcat $< | awk '$$11=="NULL" && $$12=="NULL" && $$13=="NULL" && $$14=="NULL" && $$15=="NULL" && $$16=="NULL" {print  $$7 "\t" $$2 "\t" $$3 "\t" $$1}' | $(STANDARD_SORT) > $@

coords_intron: ensembl_coordinate.tab.gz
	zcat $< | awk '{print $$7,$$2,$$3,$$5}' | $(BIN_DIR)/start_stop_introni | $(STANDARD_SORT) >  $@

coords_5utr: ensembl_coordinate.tab.gz
	zcat $< | awk '{print $$7 "\t" $$13 "\t" $$14 "\t" $$6 }' | grep -v -F NULL | $(STANDARD_SORT) > $@

coords_3utr: ensembl_coordinate.tab.gz
	zcat $< | awk '{print $$7 "\t" $$15 "\t" $$16 "\t" $$6 }' | grep -v -F NULL | $(STANDARD_SORT) > $@

coords_coding: ensembl_coordinate.tab.gz
	zcat $< | awk '{print $$7 "\t" $$11 "\t" $$12 "\t" $$6 }' | grep -v -F NULL | $(STANDARD_SORT) > $@

coding.coords: ensembl_coordinate.tab.gz
	@if [ 0 -ne `zcat ensembl_coordinate.tab.gz | awk '$$1!~/^OTT/ && $$11!="NULL" && $$18!="non_coding"' | awk '{print $$4*$$11,$$0}' | find_best -r 6 1 | awk '$$21!=-1 && $$21!=0' | wc -l` ]; then\
		echo "Fase -1 in esone coding non all'inizio di un trascritto" >&2; \
		exit 1; \
	fi
	zcat $< | awk '$$18!="non_coding" { if($$1~/^OTT/){$$20=""}else{ $$20 = $$4*( (3-$$20)%3 )} print $$7, $$11,  $$12, $$1, $$6, $$20 }' | grep -v -F NULL | $(STANDARD_SORT) > $@


coords_gene: ensembl_coordinate.tab.gz
	zcat $< | $(BIN_DIR)/start_stop_genes | $(STANDARD_SORT) >  $@

coords_upstream: coords_gene
	awk '{\
		if($$4>0){\
			if($$2>$(UPSTREAM_EXTENSION)){\
				print $$1 "\t" $$2 - $(UPSTREAM_EXTENSION) "\t" $$2 "\t" $$5;\
			}else{\
				print $$1 "\t" 0 "\t" $$2 "\t" $$5;\
			}\
		}else{\
			print $$1 "\t" $$3 "\t" $$3 + $(UPSTREAM_EXTENSION) "\t" $$5 ;\
		}\
	}' $< | $(STANDARD_SORT) > $@

coords_gene_biotype: ensembl_coordinate.tab.gz
	zcat $< | $(BIN_DIR)/start_stop_geni_biotype | $(STANDARD_SORT) >  $@

coords_%.union: coords_%
	union $< >$@


coords_coding_chr.%: coords_coding
	awk '$$1=="$*"' $< > $@;


biotype_statistics: ensembl_coordinate.tab.gz
	zcat $< | cut -f 17 | $(BIN_DIR)/frequenze_string | sort -k 2,2nr > $@

nr_coding.coords: coords_coding $(addprefix coords_nonredundant.,$(ALL_CHR))
	cat $(wordlist 2,$(words $^),$^) | awk 'BEGIN{OFS="\t"} $$4=="C" {print $$1,$$2,$$3,$$1 "_" $$2 "_" $$3}' | sort -k 1,1 -k2,2n \
	| intersection - $< \
	| cut -f 1,4,5,8,9 | collapsesets 5 > $@

nr_coding.known.coords: nr_coding.coords ensembl_coordinate.tab.gz
	expandsets 5 < $< \
	| traduci -a <(zcat $(word 2,$^) | cut -f 6,19 | sort | uniq) 5 \
	| awk '$$6=="KNOWN"' \
	| collapsesets 5 > $@

nr_intron.coords: $(addprefix coords_nonredundant.,$(ALL_CHR))
	#mkfifo tmp.pipe ; cat $^ | awk '$$4=="I" && $$2!=$$3' > tmp.pipe & make tmp.pipe.gene ; rm tmp.pipe
	#awk -F "," '{print $$1}' tmp.pipe.gene | cut -f 1,2,3,5 | $(BIN_DIR)/introns_append_id | $(STANDARD_SORT) > $@
	#rm -f tmp.pipe.gene
	cat $^ | awk 'BEGIN{OFS="\t"} $$4=="I" {print $$1,$$2,$$3,$$1 "_" $$2 "_" $$3}' | sort -k 1,1 -k2,2n > $@

.META: nr_intron.coords
	1	chr	1	il cromosoma
	2	b
	3	e
	4	id

nr_intron_gene.map: nr_intron.coords coords_gene
	intersection $^ | cut -f 8,10 > $@

ng_intron.coords: coords_exon ensembl_coordinate.tab.gz 
	traduci <(zcat $(word 2,$^) | cut -f 1,6 | sort -S20% | uniq) 4 <$< \
	| awk 'BEGIN{OFS="\t"}{print $$4, $$2, $$3, $$1}' \
	| sort -S20% -k1,1 -k2,2n -k3,3n \
	| union \
	| tr ';' '\t' \
	| awk 'BEGIN{OFS="\t"}{print $$4, $$2, $$3, $$1}' \
	| $(BIN_DIR)/start_stop_introni \
	| perl -lane '$$,="\t"; $$\="\n"; $$gene{$$F[3]}++; $$F[4]=$$F[3]; $$F[3].='_'.$$gene{$$F[3]}; print @F' \
	| sort -S20% -k1,1 -k2,2n -k3,3n >$@
#output : cromosoma start stop id_gene
#(nonredundant_intron for each gene )

ng_intron-const_altern: ng_intron.coords ensembl_coordinate.tab.gz
	cut -f 2,3,4,5 $< | grep -F ENS | $(BIN_DIR)/const_altern_introns <(zcat $(word 2, $^) | grep -F ENS | cut -f 1,2,3,5,6 ) | awk '($$3==$$5 && $$4==$$5) {print $$0, "C"} ($$3!=$$5 && $$4!=$$5) {print $$0, "A"} (($$3!=$$5 && $$4==$$5) || ($$3==$$5 && $$4!=$$5)) {print $$0, "CA"}' | sort -k2,2 -k1,1n > $@

nr_intron_ng_intron.map: nr_intron.coords ng_intron.coords
	intersection $< $(word 2,$^) \
	| cut -f8- \
	| sort -S40% >$@

#output : cromosoma start stop id_gene start stop 
#(nonredundant_intron both global and each gene)

nr_intron.coords.collapsed: nr_intron.coords
	$(BIN_DIR)/collapse_intron_id $< | $(STANDARD_SORT) > $@

coords_upstream_id: coords_upstream coords_nonredundant.all
	$(BIN_DIR)/annote_slice \
		$(addprefix -a $(BIOINFO_ROOT)/task/annotations/dataset/ensembl38/homo/coords_nonredundant., $(ALL_CHR)) \
		coords_upstream\
	| awk '$$6-$$5>0 {print $$1 "\t" $$5 "\t" $$6 "\t" $$4}' \
	>$@

chr_nomask_len: $(addprefix $(SEQUENCE_DIR)/chr,$(addsuffix .fa.len, $(ALL_CHR))) $(addsuffix .repeats,$(addprefix chr,$(ALL_CHR)))
	for i in $(ALL_CHR); do \
		echo -ne "$$i\t" >> $@; \
		tr "\n" "\t" < $(SEQUENCE_DIR)/chr$$i.fa.len >> $@; \
		union -c chr$$i.repeats | awk '{print $$2 - $$1}' | sum_column >> $@; \
	done;

coords_nonredundant.%: $(SEQUENCE_DIR)/chr%.fa.len coords_coding_chr.% coords_5utr coords_3utr coords_upstream coords_nc_exon
	#nota sull'opzione -i: pongo come introni tutta la regione genica, il coding e tutto il resto tanto sovrascrive, 
	#cosi` mi tolgo il problema di definire gli introni
	$(BIN_DIR)/annotation_non_redundant2 \
		-c coords_coding_chr.$* \
		-5u coords_5utr \
		-3u coords_3utr \
		-nce coords_nc_exon \
		-i coords_gene \
		-u coords_upstream \
	| $(STANDARD_SORT)\
	| $(BIN_DIR)/aggiungi_intergenico $< > $@;

coords_nonredundant.all: $(addprefix coords_nonredundant.,$(ALL_CHR))
	#$(BIN_DIR)/accoda --name chr_$$i "make coords_nonredundant.$$i";\

coords_nonredundant.all_accoda:
	for i in $(ALL_CHR); do\
		accoda --name chr_$$i "make coords_nonredundant.$$i";\
	done;

coords_nonredundant.all_accoda_ssh:
	for i in $(ALL_CHR); do\
		accoda_ssh -w 120 make coords_nonredundant.$$i;\
	done;
	





##
## Statistica
##

.PHONY: overlapping_genes_coding
overlapping_genes_coding: coords_coding
	cat $< | awk 'BEGIN{OFS="\t"} {print $$4, $$2, $$3, $$1}' \
	| grep 'ENS' | sort -k 1,1 -k 2,2n | union \
	|  awk 'BEGIN{OFS="\t"}{print $$4,$$2,$$3,$$1}' | sort -k 1,1 -k 2,2n > $@.tmp
	intersection $@.tmp $@.tmp | awk '$$8!=$$9' | cut -f 8,9 | tr "\t" "\n" | count
	rm $@.tmp

.PHONY: overlapping_genes_exons
overlapping_genes_exons: ensembl_coordinate.tab.gz
	zcat $< | awk 'BEGIN{OFS="\t"} {print $$6, $$2, $$3, $$7}' \
	| grep 'ENS' | sort -k 1,1 -k 2,2n | union \
	|  awk 'BEGIN{OFS="\t"}{print $$4,$$2,$$3,$$1}' | sort -k 1,1 -k 2,2n > $@.tmp
	intersection $@.tmp $@.tmp | awk '$$8!=$$9' | cut -f 8,9 | tr "\t" "\n" | count
	rm $@.tmp

coords_nonredundant_per_chr: $(addprefix, coords_nonredundant., $(ALL_CHR))
	set -e;\
	for i in coords_nonredundant.*; do\
		j=$${i#*.};\
		$(BIN_DIR)/annotation_nonredundant_sum_al_chr $$i > $@.$$j;\
		cat $@.$$j >> $@;\
	done;

coords_nonredundant_sum: coords_nonredundant_per_chr
	awk '{genome_len+=$$3; C+=$$4; E+=$$5; p5+=$$6; p3+=$$7; I+=$$8; U+=$$9; N+=$$10 } END{OFS="\t"; print genome_len,C,E,p5,p3,I,U,N}' $< > $@

coords_nonredundant_sum_norm: coords_nonredundant_sum
	awk '{OFS="\t"; print $$2/$$1,$$3/$$1,$$4/$$1,$$5/$$1,$$6/$$1,$$7/$$1,$$8/$$1}' <$< > $@

intron_distrib_lunghezze.ps: coords_intron.log_len.dist nr_intron.coords.log_len.dist
	echo -e "set terminal postscript color;\n\
		set logscale y;\n\
		plot 'coords_intron.log_len.dist' w l, 'coords_intron.log_len.dist' w p pt 6,\
		'nr_intron.coords.log_len.dist' w l, 'nr_intron.coords.log_len.dist' w p pt 6" | gnuplot > $@

all_exons_distrib_lunghezze.ps: coords_coding.log_len.dist coords_nc_exon.log_len.dist coords_5utr.log_len.dist coords_3utr.log_len.dist
	echo -e "set terminal postscript color;\n\
		set logscale y;\n\
		plot 'coords_coding.log_len.dist' w p pt 6, 'coords_nc_exon.log_len.dist' w p pt 6,\
		'coords_5utr.log_len.dist' w p pt 6, 'coords_3utr.log_len.dist' w p pt 6" | gnuplot > $@
	
%_distrib_lunghezze.ps: coords_%.log_len.dist
	echo -e "set terminal postscript color;\nset logscale y;\nplot '$<' w l, '$<' w p pt 6" | gnuplot > $@

%_distrib_lunghezze.cum.ps: coords_%.log_len.dist.norm_cum
	echo -e "set terminal postscript color; unset key; plot '$<' w l, 1-0.05, 0.05" | gnuplot - > $@
	
	
coords_%.log_len.dist: coords_%
	awk '{print log($$3-$$2+1)/log(10)}' $< | histogram -c 200 > $@

coords_%.log_len.dist.norm_cum: coords_%.log_len.dist
	cumulativa -n $< > $@;

repeat_sum: $(addsuffix .repeats,$(addprefix chr,$(ALL_CHR)))
	for i in $(ALL_CHR); do \
		echo $$i; \
		echo -ne "$$i\t" >> $@; \
		awk "{print \"$$i\t\" \$$1 \"\t\" \$$2}" chr$$i.repeats | union | awk 'BEGIN{sum=0}{sum+=($$3 - $$2)} END{print sum}' >> $@;\
	done

masks_sum: $(addsuffix .masks,$(addprefix chr,$(ALL_CHR)))
	for i in $(ALL_CHR); do \
		echo $$i; \
		echo -ne "$$i\t" >> $@; \
		awk "{print \"$$i\t\" \$$1 \"\t\" \$$2}" chr$$i.masks | union | awk 'BEGIN{sum=0}{sum+=($$3 - $$2)} END{print sum}' >> $@;\
	done


####################################3
#
#
#
#	roba vecchia
#
#

#alignments: prepara_per_database 
#	blastab2db_allineamenti.sh $(RUN_ID) $< > alignments
#	#cat alignments | psql -h to441xl  -c 'copy alignments from stdin' alignments

regioni:
	cd allineamenti; $(BIN_DIR)/get_region_from_alignments.sh
	cd allineamenti; for i in regions.*; do echo sort $$i; sort -S $$((500*1024)) -k 2,2n -k3,3n $$i | uniq > $$i.mv; mv $$i.mv $$i; done;
	touch $@;

regioni_annotazioni: regioni coords_nonredundant.all
	cd allineamenti; for i in regions.*; do echo $$i; j=$${i#regions.}; $(BIN_DIR)/annotation_nonredundant_slice ../coords_nonredundant.$${j#chr} < $$i > $@.$$i; done;
	touch $@;

%.annotated: % coords_nonredundant.all
	chr=$*; chr1=$${chr##*chr}; chr1=$${chr1%%.*};\
	chr2=$${chr#*chr}; chr2=$${chr2%%_*}; echo $$chr1 $$chr2;\
	$(BIN_DIR)/annotation_nonredundant_slice coords_nonredundant.$$chr1 <  $* > tmp;\
	$(BIN_DIR)/annotation_nonredundant_slice coords_nonredundant.$$chr2 <  tmp > $@;
	#rm tmp

%.all_annotated: % coords_nonredundant.all
	cp $* $@;
	for chr in $(ALL_CHR); do\
		echo $$chr;\
		$(BIN_DIR)/annotation_nonredundant_slice coords_nonredundant.$$chr < $@ > $@.tmp;\
		mv $@.tmp $@;\
	done;

%.gene: % coords_gene_biotype 
	$(BIN_DIR)/annotation_gene coords_gene_biotype < $< > $@
	 

%.all_annotated.gene.html: %.all_annotated.gene
	$(BIN_DIR)/html_tabellizza $< > $@

%.all_annotated.vectors: %.all_annotated
	$(BIN_DIR)/fasta_seg_annotated_2_vectors $< | awk '{i++; print i "\t" $$0}' > $@

%.all_annotated.vectors.normalized: %.all_annotated.vectors
	$(BIN_DIR)/vector_normalize $< > $@

########################################
#	Sezione relativa al database sql
#
#
#
regioni_annotazioni_in_database: regioni_annotazioni
	for i in allineamenti/regioni_annotazioni.regions.chr*; do\
		echo $$i;\
		cat $$i | psql -c 'copy region_annotations from STDIN' alignments;\
	done;
	touch regioni_annotazioni_in_database
#
#
########################################




########################################
#	Sezione dedicata al coverage
#
#
#
#
regioni_annotazioni_coverage: coords_nonredundant.all
	cd coverage_levels; for i in coverage_*_*.txt; do echo $$i; j=$${i#coverage_chr}; j=$${j%%_*}; [ -e $$i.annotated ] || $(BIN_DIR)/annotation_nonredundant_slice ../coords_nonredundant.$${j} < $$i > $$i.annotated; done;
	touch $@;


#	for chr in 1 2 3; do\
#		for score in 43 110 220 500 1000 2000; do 
coverage_level_grafico: regioni_annotazioni_coverage
	cd coverage_levels;\
	for chr in $(ALL_CHR); do\
		for score in  43 110 220 500 1000 2000; do \
			rm -f grafico_chr$${chr}_$${score}.data;\
			(for i in coverage_chr$${chr}_$${score}*.annotated; do\
				j=$${i##*_}; j=$${j%%.*};\
				echo -ne "$$j\t"; $(BIN_DIR)/sum_annotation_norm ../coords_nonredundant_sum.$$chr < $$i;\
			done;) | sort -n >> grafico_chr$${chr}_$${score}.data;\
		done;\
	done;
	touch $@

coverage_level_grafico_not_norm: regioni_annotazioni_coverage
	cd coverage_levels;\
	for chr in $(ALL_CHR_NO_M); do\
		for score in  43 110 220 500 1000 2000; do \
			rm -f grafico_chr$${chr}_$${score}.data.not_norm;\
			(for i in coverage_chr$${chr}_$${score}*.annotated; do\
				j=$${i##*_}; j=$${j%%.*};\
				echo -ne "$$j\t"; $(BIN_DIR)/sum_annotation $$i;\
			done;) | sort -n >> grafico_chr$${chr}_$${score}.data.not_norm;\
		done;\
	done;
	touch $@

coverage_level_grafico_not_norm_sum: coverage_level_grafico_not_norm
	cd coverage_levels;\
	for score in  43 110 220 500 1000 2000; do \
		cp grafico_chr1_$${score}.data.not_norm $@.$${score};\
		for chr in $(ALL_CHR_NO_M); do\
			if [ $$chr != "1" ]; then\
				$(BIN_DIR)/sum_matrix $@.$${score} < grafico_chr$${chr}_$${score}.data.not_norm > $@.$${score}.mv;\
				mv -f $@.$${score}.mv $@.$${score};\
			fi;\
		done;\
	done;
	touch $@


coverage_level_grafico.png: coverage_level_grafico
	cd coverage_levels;\
	for i in *.data; do\
		rm -f tmp.data;\
		ln $$i tmp.data;\
		echo  $${i%.*};\
		gnuplot $(BIN_DIR)/coverage_levels.gnuplot > $${i%%.*}.png;\
		rm -f tmp.data;\
	done;
#
#
#
#############################################



#############################################
#	Sezione dedicata ai palindromi
#
#
#
regioni_palindromi:
	cd palindromi; $(BIN_DIR)/get_region_from_palindromi
	cd palindromi; for i in regions.*; do echo sort $$i; sort -S $$((500*1024)) -k 2,2n -k3,3n $$i | uniq  > $$i.mv; mv $$i.mv $$i; done;
	touch $@
regioni_annotazioni_palindromi: regioni_palindromi coords_nonredundant.all
	cd palindromi; for i in regions.*; do echo $$i; j=$${i#regions.}; $(BIN_DIR)/annotation_nonredundant_slice ../coords_nonredundant.$${j#chr} < $$i > regioni_annotazioni.$$j; done;
	touch $@;
#############################################

annotazioni_backup:
	mkdir annotazioni_backup
	cp -r bin annotazioni_backup
	cp makefile annotazioni_backup


###########################################
#
# Old rules
#
###########################################
#
#ensg2omim_disorder: $(ENS2OMIM_DISORDER)
#	cp $< $@
#
#ensg2mim_morbid:
#	echo 'select DISTINCT gene_stable_id,dbprimary_id from hsapiens_gene_ensembl__xref_mim_morbid__dm where dbprimary_id is not null' \
#	| $(MYSQL_MART) ensembl_mart_$(ENS_VERSION) > $@
#
#ensg2hugo:
#	echo "SELECT\
#			gene_stable_id.stable_id,\
#			xref.display_label,\
#			xref.description\
#		FROM xref\
#		JOIN object_xref ON xref.xref_id = object_xref.xref_id\
#		JOIN external_db ON external_db.external_db_id = xref.external_db_id\
#		JOIN gene_stable_id ON gene_stable_id.gene_id = object_xref.ensembl_id \
#		WHERE external_db.db_name in ('HGNC','HUGO')" \
#	| $(MYSQL_CORE) $(ENS_CORE) > $@
