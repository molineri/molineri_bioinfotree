include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

URL_PREFIX = ftp://ftp.ncbi.nih.gov/pub/taxonomy

nucleotide-taxid.map.gz:
	wget -O $@ $(URL_PREFIX)/gi_taxid_nucl.dmp.gz

.META: taxid_nucl.gz
	gi_nucleotide_id
	tax_id

taxdump.tar.gz:
	wget -O $@ ftp://ftp.ncbi.nih.gov/pub/taxonomy/$@

%.dmp: taxdump.tar.gz
	tar -xzf $< $@

names: names.dmp
	sed 's/\t|\t/\t/g' $< | sed 's/\t|$$//' \
	| bawk '{print $$1,$$4,$$2; if($$3){print $$1,$$4,$$3}}' > $@
	
taxid-parent_taxid: nodes.dmp
	sed 's/\t|\t/\t/g' $< | sed 's/\t|$$//' | cut -f 1,2 | sort -n | uniq > $@

taxid-parent_taxid.names.gz: taxid-parent_taxid $(BIOINFO_ROOT)/task/annotations/local/share/data/ucsc_species.map
	translate -a -v -e NA <(cut -f 1,4 $^2) -f 2 1 2 < $< | gzip > $@

.META: taxid-parent_taxid.names.gz
	1	taxid	9606
	2	name	hsapiens
	3	parent_taxid	9605
	4	parent_name	NA

