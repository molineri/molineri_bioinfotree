include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

DATABASE_NAME = ipg_$(VERSION)
MYSQL ?= mysql -u $(MYSQL_USERNAME) -p$(MYSQL_PASSWRD) $(DATABASE_NAME)

go_daily-termdb-tables.tar.gz: 
	wget -O $@.tmp http://archive.geneontology.org/latest-termdb/$@
	mv $@.tmp $@

ALL += go_term go_tree

#all: go.term go.alt go.obs go.anc
#
#go.alt:	go.term
#go.obs:	go.term
#go.anc:	go.term

go_daily-termdb-tables: go_daily-termdb-tables.tar.gz
	rm -rf  go_daily-termdb-tables
	tar -xvzf go_daily-termdb-tables.tar.gz

load_database: go_daily-termdb-tables
	cat go_daily-termdb-tables/*.sql | $(MYSQL) 
	mysqlimport -u $(MYSQL_USERNAME) -p$(MYSQL_PASSWRD) -L $(DATABASE_NAME) go_daily-termdb-tables/*.txt
	touch $@

go_tables: load_database
	go_tables -u $(MYSQL_USERNAME) -P $(MYSQL_PASSWRD) -d $(DATABASE_NAME)
	touch $@

go_tree: load_database
	echo "SELECT DISTINCT t1.acc, t2.acc FROM graph_path as g LEFT JOIN term as t1 on t1.id=g.term1_id LEFT JOIN term as t2 on t2.id=g.term2_id where g.distance>0 and t1.acc!='all' and t2.acc!='all';" \
	| $(MYSQL) \
	| bsort -k2,2 > $@

.META: go_tree
	1	go_id_ancestor
	2	go_id

go_tree.with_distance: load_database
	echo "SELECT DISTINCT t1.acc, t2.acc, g.distance FROM graph_path as g LEFT JOIN term as t1 on t1.id=g.term1_id LEFT JOIN term as t2 on t2.id=g.term2_id where g.distance>0 and t1.acc!='all' and t2.acc!='all';" \
	| $(MYSQL) > $@

.META: go_tree
	1	go_id_ancestor
	2	go_id


go_term: load_database
	echo "SELECT DISTINCT t2.acc, t2.name, t2.term_type, MIN(g.distance) FROM graph_path as g LEFT JOIN term as t1 on t1.id=g.term1_id LEFT JOIN term as t2 on t2.id=g.term2_id WHERE g.distance>0 AND t1.is_root=1  GROUP BY t2.acc" \
	| $(MYSQL) \
	> $@

.META: go_term
	1	go_id	GO:0000001
	2	desc	mitochondrion inheritance
	3	ontology	biological_process
	4	level	6

