SHELL := /bin/bash

BIN_DIR := $(BIOINFO_ROOT)/task/annotations/local/bin

SPECIES ?= hsapiens
PFAM_ENS_VERSION ?= 42
ENS_VERSION ?= 40
ANNOTATION_DIR ?= $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/$(SPECIES)/$(ENS_VERSION)
PFAM_ANNOTATION_DIR ?= $(BIOINFO_ROOT)/task/annotations/dataset/pfam/$(SPECIES)/$(PFAM_ENS_VERSION)

GO_PATTERNS_ANNOTATION_DIR ?= $(BIOINFO_ROOT)/task/annotations/dataset/GO/$(SPECIES)/$(PFAM_ENS_VERSION)/patterns_hsapiens_hsapiens_ensembl40_wublast_pam10_0
TARGET_PREFIX ?= regions.0.99
FAMILY ?= $(GO_PATTERNS_ANNOTATION_DIR)/$(TARGET_PREFIX).annote_gene.family

ALL_CHR ?= 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X Y


.SECONDARY:
.DELETE_ON_ERROR:


$(TARGET_PREFIX).annote_gene.family.pfam: $(FAMILY) $(PFAM_ANNOTATION_DIR)/gene_pfam $(PFAM_ANNOTATION_DIR)/pfam_acc_desc
	$(BIN_DIR)/join_annotation.pl -a $(word 2,$^) -tmp $@.tmp -g  $< \
	| sort -k2,2 \
	| join3_pl - -i -u -1 2 -2 1 -- $(word 3,$^) \
	| sort_pl 3 \
	| awk -F"\t" 'BEGIN{OFS="\t"}{print $$1,$$2,$$3,$$6,$$4,$$5}' > $@
	rm -f $@.tmp

