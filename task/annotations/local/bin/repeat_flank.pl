#!/usr/bin/perl
use warnings;
use strict;
$,="\t";
$\="\n";

my $range=shift @ARGV;

$range=~/\d+/ or die;

while(<>){
	my ($chr,$b,$e,$s)=split;
	if($s eq '+'){
		print $chr,$b,$e,$s,
			$b,$b - $range,
			$e,$e + $range;
	}else{
		print $chr,$b,$e,$s,
			$e,$e + $range,
			$b - $range,$b;
	}
}
