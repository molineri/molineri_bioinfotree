#!/usr/bin/perl 

use warnings;
use strict;
use Getopt::Long;

$, = "\t";
$\ = "\n";

my $usage = "cat chr_b_e_id | $0 -a \$(BIOINFO_ROOT)/annotations/dataset/ensembl40/hsapiens/coords_gene";


my $annotation_file = undef;

&GetOptions (
	"gene_annot_file|a=s" => \$annotation_file
);

die ($usage) if !(defined $annotation_file);

open ANNOTATION, $annotation_file or die ("ERROR: $0: Can't open file ($annotation_file)");

my @annotations=();
my @ann_gene_pre = ();

while(<ANNOTATION>){
	chomp;
	my @ann_gene = split /\t/;
	if (
		@ann_gene_pre
			and
		$ann_gene[0] eq $ann_gene_pre[0]
			and
		$ann_gene[1] == $ann_gene_pre[1]
			and
		$ann_gene[2] == $ann_gene_pre[2]
	) {
		${$annotations[$#annotations]}[4] .= ','.$ann_gene[4];
	}else{
		push @annotations, \@ann_gene;
		@ann_gene_pre = @ann_gene;
	}

}


my $max_i = scalar(@annotations);

while(my $region = <>){
	chomp $region;
	if ($region =~ /^>/) {
		#print $region;
		next;
	}

	#my ($r_chr, $r_start, $r_stop) = split /\t/,$region;
	my ($r_chr, $r_start, $r_stop, $reg_header) = split /\t/,$region;

	$reg_header = $r_chr.'_'.$r_start.'_'.$r_stop if !defined $reg_header;
	
	my $i=undef;

	# previous gene

	my $tmp;
	for ($i=0; $i<$max_i; $i++){
		
		next if ($r_chr ne ${$annotations[$i]}[0]);
		$tmp = $annotations[$i];
		
		last if (${$annotations[$i]}[2] > $r_start);
	}
	print $reg_header, @{$tmp}, 'b' ,$r_start - ${$tmp}[2] if ( 
		($i==$max_i) and !($annotations[ $i - 1 ] == $tmp) );
	print $reg_header, @{$annotations[ $i - 1 ]}, 'b', $r_start - ${$annotations[ $i - 1 ]}[2] if ( 
		($i>0) and ($r_chr eq ${$annotations[$i - 1]}[0]) );


	# intersection
	
	for (;$i<$max_i; $i++){

		last if ($r_chr ne ${$annotations[$i]}[0]);

		last if (${$annotations[$i]}[1] >= $r_stop);

		print $reg_header, @{$annotations[ $i ]},'i','0';

	}
	
	print $reg_header, @{$annotations[ $i ]},'a',${$annotations[ $i ]}[1] - $r_stop if $i<$max_i;

	# output:
	# >chr_start_stop	chr	start	stop	?	gene	b/i/a	distance
}
