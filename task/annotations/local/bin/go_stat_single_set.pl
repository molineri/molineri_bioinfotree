#!/usr/bin/perl

use warnings;
use strict;
use constant FAM_COL  => 0;
use constant GENE_COL  => 1;
use constant ANNOT_COL => 2;
use Getopt::Long;

$, = "\t";

my $usage = "cat id_go_file | $0 -a gene_annotazione.world [-g]
	-g to visual genes related to each GO term\n";


my $annot_file = undef;
my $g = undef;

&GetOptions (
	"gene_annot_file|a=s" => \$annot_file,
	"genes|g"             => \$g
);

die ($usage) if !(defined $annot_file); 




###############################################################################################################
# Per il calcolo del PVALUE:
# 	1. conteggio del numero di geni annotati a ciascuna parola $M{word}
# 	2. conteggio del numero totale di geni annotati ad almeno una parola $N
#
open ANNOT, $annot_file or die ("ERROR: $0: Can't open file ($annot_file)");

my %annot_genes = ();
my %M = ();

while (<ANNOT>) {
	chomp;
	my @F = split /\t/;
	$M{$F[1]} = 0 if !defined $M{$F[1]};
	$M{$F[1]}++;
	$annot_genes{$F[0]} = 1;
}
my $N = scalar keys %annot_genes;
###############################################################################################################




###############################################################################################################
# Per il calcolo del PVALUE:
# 	3. conteggio del numero di geni della famiglia annotati a ciascuna parola    $x{word}
# 	4. conteggio del numero di geni della famiglia che hanno almeno un'annotazione   $n
#
my %x = ();
my $n = undef;
my $last_row_ref=undef;
my $last_id=undef;

while (my $family_ref = &read_block()){
	my $family_name = ${$family_ref}[0]->[0];
	my $n=0;
	my %genes=();
	my %x=();
	my %word_genes=();
	for(@{$family_ref}){
		$genes{$_->[GENE_COL]}=1;
		$x{$_->[ANNOT_COL]}++;
		push @{$word_genes{$_->[ANNOT_COL]}},$_->[GENE_COL];
	}
	$n=scalar keys %genes;

	for (keys %x) {
		my $pvalue = `Ipergeo $N $M{$_} $n $x{$_} | awk '{print \$10}'`;
		chomp $pvalue;
		#print STDERR "$N $M{$_} $n $x{$_}\n";
		print $family_name,$_,$pvalue,$x{$_};
		print "\t".join ',',@{$word_genes{$_}} if (defined $g);
		print "\n";
	}
}

###############################################################################################################

sub read_block
{
	
	my @block=();
	if($last_row_ref){
		push @block,$last_row_ref;
	}

	my $EOF=1;
	while(<>){
		$EOF=0;
		chomp;
		my @F=split;
		if(defined($last_id)){
			if($last_id ne $F[FAM_COL]){
				$last_row_ref=\@F;
				$last_id=$F[FAM_COL];
				return \@block;
			}
		}
		push @block,\@F;
		$last_id=$F[FAM_COL];
	}
	return undef if $EOF;
	return \@block;
}
