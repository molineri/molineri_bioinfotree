#!/usr/bin/perl

use warnings;
use strict;
use constant FAM_COL   => 0;
use constant GENE_COL  => 1;
use constant ANNOT_COL => 2;
use Getopt::Long;

$, = "\t";

my $usage = "cat famiglia_gene | $0 -a gene_annotazione [-n|-tmp tmp_filename] [-g]
	-g to visual genes related to each GO term\n";

my $already_annoted = 0;
my $annot_file = undef;
my $tmp_file = undef;
my $g = undef;

&GetOptions (
	"already_annoted|n" => \$already_annoted,
	"gene_annot_file|a=s" => \$annot_file,
	"tmp_file|tmp=s"      => \$tmp_file,
	"genes|g"             => \$g
);

die ($usage) if !(defined $annot_file);

###############################################################################################################
## join tra famiglia_gene e gene_annotazione
## risultato: 	file $tmp:
#		1. famiglia
#		2. gene
#		3. annotazione
#	NB: per ogni gene ci puo' essere piu' di una annotazione
#
if(!$already_annoted){
	die($usage) if !defined($tmp_file);
	
	my $col_id_1 = 2;
	my $col_id_2 = 1;

	open PIPE,"| join3_pl - -1 $col_id_1 -2 $col_id_2 -i -u -- $annot_file | sort -k1,1 -k3,3 > $tmp_file" or die (
		"ERROR: $0: Can't open pipe\n");

	while (<>) {
		print PIPE $_;
	}
	close PIPE;
}
###############################################################################################################



###############################################################################################################
# Per il calcolo del PVALUE:
# 	1. conteggio del numero di geni annotati a ciascuna parola $M{word}
# 	2. conteggio del numero totale di geni annotati ad almeno una parola $N
#
open ANNOT, $annot_file or die ("ERROR: $0: Can't open file ($annot_file)");

my %annot_genes = ();
my %M = ();

while (<ANNOT>) {
	chomp;
	my @F = split /\t/;
	$M{$F[1]} = 0 if !defined $M{$F[1]};
	$M{$F[1]}++;
	$annot_genes{$F[0]} = 1;
}
my $N = scalar keys %annot_genes;
###############################################################################################################




###############################################################################################################
# Per il calcolo del PVALUE:
# 	3. conteggio del numero di geni della famiglia annotati a ciascuna parola    $x{word}
# 	4. conteggio del numero di geni della famiglia che hanno almeno un'annotazione   $n
#
my $fh=undef;
if(!$already_annoted){
	open $fh, $tmp_file or die ("ERROR: $0: Can't open temporary file ($tmp_file)");
}else{
	$fh=*STDIN;
}

my %x = ();
my $n = undef;
my $last_row_ref=undef;
my $last_id=undef;

while (my $family_ref = &read_block()){
	my $family_name = ${$family_ref}[0]->[0];
	my $n=0;
	my %genes=();
	my %x=();
	my %word_genes=();
	for(@{$family_ref}){
#		print STDERR $_->[GENE_COL];
#		print STDERR $_->[ANNOT_COL];
#		die;
		$genes{$_->[GENE_COL]}=1;
		$x{$_->[ANNOT_COL]}++;
		push @{$word_genes{$_->[ANNOT_COL]}},$_->[GENE_COL];
	}
	$n=scalar keys %genes;

#	print STDERR "salto!!!";
#	die;

	for (keys %x) {
		#my $pvalue = `Ipergeo $N $M{$_} $n $x{$_} | awk '{print \$10}'`;
		#chomp $pvalue;
		#die "$N $M{$_} $n $x{$_}\n" if (!defined $pvalue or $pvalue !~ /^\d+\.\d+/);
		print $N, $M{$_}, $n, $x{$_}, $family_name, $_;
		print "\t".join ',',@{$word_genes{$_}} if (defined $g);
		print "\n";
		#print $family_name,$_,$pvalue;
		#print "\t$N\t$M{$_}\t$n\t$x{$_}";
		#print "\t".join ',',@{$word_genes{$_}} if (defined $g);
		#print "\n";
	}
}

#`rm $tmp_file`;

###############################################################################################################

sub read_block
{
	
	my @block=();
	if($last_row_ref){
		push @block,$last_row_ref;
	}

	my $EOF=1;
	while(<$fh>){
		$EOF=0;
		chomp;
		my @F=split;
		if(defined($last_id)){
			if($last_id ne $F[FAM_COL]){
				$last_row_ref=\@F;
				$last_id=$F[FAM_COL];
				print STDERR "$last_id\n";
				return \@block;
			}
		}
		push @block,\@F;
		$last_id=$F[FAM_COL];
	}
	return undef if $EOF;
	return \@block;

}
