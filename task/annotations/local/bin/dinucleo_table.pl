#!/usr/bin/perl

use warnings;
use strict;
$,="\t";
$\="\n";

my $filename_down=shift @ARGV;
my $filename_up=shift @ARGV;
my $filename_genome=shift @ARGV;

die if $filename_down !~ /down/;
die if $filename_up !~ /up/;
die if $filename_genome !~ /genome/;

open DOWN, $filename_down or die;
open UP, $filename_up or die;
open GENOME, $filename_genome or die;

my %down;
my %up;
my %genome;

while(<DOWN>){
	chomp;
	my @F=split;
	$down{$F[0]}=$F[1];
}
while(<UP>){
	chomp;
	my @F=split;
	$up{$F[0]}=$F[1];
}
while(<GENOME>){
	chomp;
	my @F=split;
	$genome{$F[0]}=$F[1];
}

my $sum_di_up=0;
my $sum_di_down=0;
my $sum_di_genome=0;
my $sum_mono_up=0;
my $sum_mono_down=0;
my $sum_mono_genome=0;

my @keys=('A','T','G','C');

foreach my $a (@keys){
	foreach my $b (@keys){
		$sum_di_up+=$up{"$a$b"};
		$sum_di_down+=$down{"$a$b"};
		$sum_di_genome+=$genome{"$a$b"};
	}
	$sum_mono_down+=$down{$a};
	$sum_mono_up+=$up{$a};
	$sum_mono_genome+=$genome{$a};
}

foreach my $a (@keys){
	foreach my $b (@keys){
		print	"$a$b",
			$down{"$a$b"}/$sum_di_down,
			$up{"$a$b"}/$sum_di_up,
			($up{"$a$b"}+$down{"$a$b"})/($sum_di_up+$sum_di_down),
			$genome{"$a$b"}/$sum_di_genome,
			$down{"$a$b"},
			$up{"$a$b"},	
			$genome{"$a$b"};
	}
}

foreach my $a (@keys){
	print STDERR $a,
		$down{$a}/$sum_mono_down,
		$up{$a}/$sum_mono_up,
		($up{$a}+$down{$a})/($sum_mono_up+$sum_mono_down),
		$genome{$a}/$sum_mono_genome,
		$down{$a},
		$up{$a},	
		$genome{$a};
}
