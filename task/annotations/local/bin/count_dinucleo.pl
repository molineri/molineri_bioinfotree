#!/usr/bin/perl

use warnings;
use strict;
$,="\t";
$\="\n";

my %count_di;
my %count;
my $pre=undef;

while(<>){
	chomp;
	if(!length or m/^>/){
		$pre=undef;
		next;
	}
	while(my $a=chop){
		
		if($a!~/[ACGT]/ or !defined($pre) or $pre!~/[ACGT]/){
			$pre=$a;
			next;
		}
		
		$count_di{"$a$pre"}++;
		$pre=$a;
		$count{$a}++;
	}
}

for (sort keys %count_di){
	print $_,$count_di{$_};
}

for (sort keys %count){
	print $_,$count{$_};
}
