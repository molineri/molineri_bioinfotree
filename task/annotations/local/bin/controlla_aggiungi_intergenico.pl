#!/usr/bin/perl

$,="\t";
$\="\n";


$usage = "$0";

@all_chr = ();
push @all_chr,(1..22);
push @all_chr,'X','Y';

foreach (@all_chr) {
	$len = `cat /raid/bioinfo/task/sequences/dataset/ensembl/hsapiens/40/chr$_.fa.len`;
	chomp $len;
	$last_rows = `tail -3 /raid/bioinfo/task/annotations/dataset/ensembl40/hsapiens/coords_nonredundant.$_`;
	chomp $last_rows;
	@lines = split /\n/,$last_rows;
	foreach $line (@lines) {
		@f=split /\t/,$line;
		print $len,$line if $f[2] > $len;
	}
}

