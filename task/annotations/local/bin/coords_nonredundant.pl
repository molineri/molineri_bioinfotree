#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;

$,="\t";
$\="\n";

$| = 1;

my $usage = "cat known_genes.nonredundant | $0 -a known_genes.label\n";

my $annot_file = undef;


&GetOptions (
	"annot_file|a=s"  => \$annot_file,
) or die ($usage);

die ($usage) if (!defined $annot_file);




if (!-s $annot_file) {
	while (<>) {
		chomp;
		print;
	}
	exit;
}



open ANNOT, $annot_file or die("Can't open file ($annot_file)");

my $eof_1=undef;
my $eof_2=undef;
my $label = undef;
my $label_base = undef;
my $label_new = undef;
my $old_label = undef;
my $x = -1;
my $old_x = 0;
my $b_base = undef;
my $e_base = undef;
my $b_new = undef;
my $e_new = undef;
my $chr = undef;
my $old_chr = undef;
my $old_b_base = undef;
my $old_b_new = undef;


#@annotations = @new_annotations;


&get_next_annot(); # faccio girare una volta per inizializzare i valori

#print STDERR "chr $chr"; ###############

while($label = &get_next_annot() ){
	#print $x,$label;
	if (defined $old_label and ($label ne $old_label) ) {
		print $chr, $old_x, $x, $old_label;
		$old_x = $x;
	}
	$old_label = $label;
}
$x--;
print $chr, $old_x, $x, $old_label;

close ANNOT;
#print STDERR "fine\n"; ###############



sub get_next_annot
{
	$x++;

#	print STDERR $x; #########

	if(!defined($e_base)) { # inizializzo $x_base

		my $line = <>;
		return undef if (!defined $line);
		chomp $line;
		($chr, $b_base, $e_base, $label_base) = split /\t/, $line;
		$x = $b_base - 1; # x della prima finestra non tutta nulla
		
		$line = <ANNOT>;
		return undef if (!defined $line);
		chomp $line;
		(my $chr_new, $b_new, $e_new, $label_new) = split /\t/, $line;
		while($chr_new ne $chr){
			($chr_new, $b_new, $e_new, $label_new) = split /\t/, $line;
		}

		$x = $b_new - 1 if $x > $b_new - 1; # x della prima finestra non tutta nulla
		return;
	}


	if ($x >= $e_base){
		my $line = <>;
		if(defined $line){
			chomp $line;
			($chr, $b_base, $e_base, $label_base) = split /\t/, $line;
			die("ERROR: more than one chr in standard input") if defined($old_chr) and $chr ne $old_chr;
			die("ERROR: disorder found in standard input") if defined($old_b_base) and $old_b_base > $b_base;
			$old_b_base = $b_base;
			$old_chr = $chr;
		}else{
			$eof_1=1
		}
	}
	if ($x >= $e_new){
		my $line = <ANNOT>;
		if(defined($line) and $line=~/^$chr\t/){
			chomp $line;
			($chr, $b_new, $e_new, $label_new) = split /\t/, $line;
			die("ERROR: disorder found in file($annot_file)") if defined($old_b_new) and $old_b_new > $b_new;
			$old_b_new = $b_new;
		}else{
			$eof_2=1
		}
	}
	
	if ($eof_1 or $eof_2){
		return undef if ($x > $e_new and $x > $e_base);
		return $label_base if ($x >= $e_new and $x < $e_base);
		return $label_new if ($x >= $e_base and $x < $e_new);
	}
	if($eof_1 and $eof_2){
		if($x<=$e_new){
			$label=$label_new;
			return $label;
		}else{
			$label=$label_base;
			return $label;
		}

	}


	# a questo punto siamo sicuri che
	# x < $e_base 
	# and
	# x < $e_new 

	return $label if $x < $b_base and $x < $b_new;

	if($x >= $b_base  and $x < $b_new){
		$label = $label_base;
		return $label;
	}
	
	if($x >= $b_new  and $x < $b_base){
		$label = $label_new;
		return $label;
	}
	
	if($x >= $b_new and $x >= $b_base){
		$label = $label_new;
		return $label;
	}

}
