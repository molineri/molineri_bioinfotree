set terminal png
set logscale x;
set logscale y;
set grid ytics mytics;
set xlabel "coverage levels" font "Helvetica,12"
set ylabel "fraction covered" 1.8,0 font "Helvetica,12"
set title "sequence biotype coverage" font "Helvetica,14"

plot 'tmp.data' using 1:2 w l title "coding",  'tmp.data' using 1:3 w l title "non coding exons",  'tmp.data' using 1:4 w l title "5' UTR",  'tmp.data' using 1:5  w l title "3' UTR", 'tmp.data' using 1:6 w l title "introns", 'tmp.data' using 1:7 w l title "upstream",  'tmp.data' using 1:8 w l  title "non coding";
