#!/usr/bin/perl
use warnings;
use strict;
use constant CHR_COL => 0;
use constant INT_B_COL => 1;
use constant INT_E_COL => 2;
use constant B_COL => 3;
use constant E_COL => 4;
use Getopt::Long;
$Getopt::Long::ignorecase=0;


my $usage = "$0 [-n|-r] [-o] [-L 7] -l 'LABEL1 LABEL2 LABEL...' intersection_file";

my $label_string;
my $ratio;
my $normalize=0;
my $ignore_ratio_overflow=0;
my $round=0;
my $LABEL_COL=8;
&GetOptions (
	"labels|l=s" => \$label_string,
	"ratio|r" => \$ratio,
	"normalize|n" => \$normalize,
	"ignore_ratio_overflow|o" => \$ignore_ratio_overflow,
	"round|f" => \$round,
	"LABEL_COL|L=i" => \$LABEL_COL
);

$LABEL_COL--;

die($usage) if ! defined($label_string);
my @labels = split /\s+/,$label_string;

my %vettorini=();
my %vettorini_ratio=();
my $last_rid=undef;
my $last_chr=undef;
my $last_b=undef;
my @reporting_cols=();
while(<>){
	chomp;
	if(!length($_)){
		warn 'WARNING: empty row';
		next;
	}
	my @F=split;

	die("ERROR: disorder found in input file") if defined($last_chr) and $F[CHR_COL] eq $last_chr and $F[B_COL] < $last_b;
	$last_chr=$F[CHR_COL];
	$last_b=$F[B_COL];

	my $rid=$F[CHR_COL]."\t".$F[B_COL]."\t".$F[E_COL];
	@reporting_cols=@F[7..$LABEL_COL] if !defined($last_rid);

	if(defined($last_rid) and $last_rid ne $rid){
		print $last_rid;
		&print_vettorino();
		%vettorini=();
		%vettorini_ratio=();
		@reporting_cols=@F[7..$LABEL_COL];
	}

	if($normalize){
		$vettorini{$F[$LABEL_COL]}=1;
	}elsif($ratio){
		$vettorini{$F[$LABEL_COL]} += ($F[INT_E_COL]-$F[INT_B_COL])/($F[E_COL]-$F[B_COL]);
	}else{
		$vettorini{$F[$LABEL_COL]} += $F[INT_E_COL]-$F[INT_B_COL];
	}
	$vettorini_ratio{$F[$LABEL_COL]} += ($F[INT_E_COL]-$F[INT_B_COL])/($F[E_COL]-$F[B_COL]);

	$last_rid=$rid;
}

if(defined($last_rid)){
	print $last_rid;
	&print_vettorino();
}

sub print_vettorino
{

	foreach my $l (@labels){
		my $v = $vettorini{$l};
		my $v_ratio = $vettorini_ratio{$l};
		$v = 0 if !defined($v);
		$v_ratio = 0 if !defined($v_ratio);
		die("ERROR: ratio overflow: $v, $v_ratio") if( !$ignore_ratio_overflow and $v_ratio > 1 );
		$v = sprintf("%.3f",$v) if $ratio and $round;
		if($ratio and $ignore_ratio_overflow){
			$v = $v < 1 ? $v : 1;
			print "\t".$v;
		}else{
			print "\t".$v;
		}
	}
	if(scalar @reporting_cols > 1){
		print "\t";
		pop @reporting_cols;
		print join "\t",@reporting_cols;
	}
	print "\n";
}
