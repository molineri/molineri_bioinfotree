#!/usr/bin/perl
use warnings;
use strict;
$,="\t";
$\="\n";

my $usage = "$0 -N 23567 -M '2273 1932 205 328 7867 4319 838' -l 'C E 5 3 I U N' file_with_id_n_x
	example of input file:
ID	n	x1	x2	x3	x4	x5	x6	x7
1002	52	13	16	1	0	14	2	9
1003	8	4	0	0	0	1	4	0
1026	53	0	0	0	0	16	3	34

	example of -M \$(grep -v '>' occurrencies.annote.bool.counted | sum_column)
";

my $N;
my $Ms;
my $labels;

use Getopt::Long;
&GetOptions (
	"N=i" => \$N,
	"M=s" => \$Ms,
	"l=s" => \$labels,
) or die($usage);

my @M = split(/\s+/, $Ms);
my @L = split(/\s+/, $labels);
my $M_max=scalar(@M);

die("#M ($#M) != #l ($#L)") if scalar(@M) != scalar(@L);

while(<>){
	my ($id,$n,@x) = split;
	for(my $i=0; $i<$M_max; $i++){
		print $N,$M[$i],$n,$x[$i],$id,$L[$i];
	}
}
