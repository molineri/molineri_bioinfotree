#!/usr/bin/env python
#
# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

from cStringIO import StringIO
from optparse import OptionParser
from subprocess import Popen, PIPE
from sys import stdin, stdout
from tempfile import TemporaryFile
from vfork.fasta.reader import MultipleBlockStreamingReader
from vfork.fasta.writer import MultipleBlockWriter
from vfork.io.util import safe_rstrip
from vfork.sequence import reverse_complement
from vfork.util import exit, format_usage


class Transcript(object):
    def __init__(self, name, chromosome, coding_start, coding_stop, strand):
        self.name = name
        self.chromosome = chromosome
        self.coding_start = coding_start
        self.coding_stop = coding_stop
        self.strand = strand
        self.exons = None

    def is_coding(self):
        return self.coding_start is not None

    def is_empty(self):
        return len(self.exons) == 0

    def trim_to_coding(self):
        self._take_range(self.coding_start, self.coding_stop)

    def trim_to_5utr(self):
        if self.strand == '+':
            self._take_range(self.exons[0].start, self.coding_start)
        else:
            self._take_range(self.coding_stop, self.exons[-1].stop)

    def trim_to_3utr(self):
        if self.strand == '+':
            self._take_range(self.coding_stop, self.exons[-1].stop)
        else:
            self._take_range(self.exons[0].start, self.coding_start)

    def _take_range(self, start, stop):
        if start == stop:
            self.exons = []
            return

        low = 0
        while low < len(self.exons) and self.exons[low].stop < start:
            low += 1

        if low >= len(self.exons) or self.exons[low].start > start:
            exit('Cut point for transcript %s is outside exons.' % self.name)
        elif self.exons[low].stop == start:
            low += 1
        else:
            self.exons[low].start = start

        high = low
        while high < len(self.exons) and self.exons[high].stop < stop:
            high += 1

        if self.exons[high].stop < stop:
            exit('Cut point for transcript %s is outside exons.' % self.name)
        elif self.exons[high].start == stop:
            high -= 1
        else:
            self.exons[high].stop = stop

        self.exons = self.exons[low:high+1]

class Exon(object):
    def __init__(self, start, stop):
        self.start = start
        self.stop = stop

    def __repr__(self):
        return '<Exon object: %d, %d>' % (self.start, self.stop)

    def __cmp__(self, o):
        return self.start - o.start

class Writer(object):
    def __init__(self, fd):
        self.last = None
        self.seq = None
        self._writer = MultipleBlockWriter(fd)

    def flush(self):
        if self.last is not None:
            self._write_seq()
        self._writer.flush()
    
    def write(self, label, strand, seq):
        if self.last is None or label != self.last[0]:
            if self.seq is not None:
                self._write_seq()

            self.last = (label, strand)
            self.seq = StringIO()

        self.seq.write(seq)
        
    def _write_seq(self):
        seq = self.seq.getvalue()
        if self.last[1] == '-':
            seq = reverse_complement(seq)
            
        self._writer.write_header(self.last[0])
        self._writer.write_sequence(seq)


def parse_structures(blocks):
    for header, body in blocks:
        tr = parse_header(header)
        tr.exons = parse_exons(tr, body)
        yield tr

def parse_exons(tr, body):
    exons = []
    for line in body:
        tokens = safe_rstrip(line).split('\t')
        
        try:
            if len(tokens) != 3:
                raise ValueError
            
            start = int(tokens[1])
            stop = int(tokens[2])
            if start >= stop:
                raise ValueError
            
            exons.append(Exon(start, stop))
            
        except ValueError:
            exit('Invalid exon coordinates for transcript %s: %s' % (tr.name, safe_rstrip(line)))

    exons.sort()
    return exons

def make_filter(gen, f):
    for tr in gen:
        if tr.is_coding():
            f(tr)
            if not tr.is_empty():
                yield tr

def print_coordinates(structures, fd):
    for tr in structures:
        for exon in tr.exons:
            print >>fd, '%s\t%s\t%d\t%d\t%s' % (tr.name, tr.chromosome, exon.start, exon.stop, tr.strand)
   
def iter_exon_sequences(fd):
    for label, seq in MultipleBlockStreamingReader(fd, join_lines=True):
        tokens = label.split('\t')
        yield tokens[0], tokens[4], seq

def parse_header(h):
    tokens = h.split('\t')
    if len(tokens) != 5:
        exit('Invalid transcript header: >%s' % h)

    if len(tokens[2]):
        try:
            start = int(tokens[2])
            stop = int(tokens[3])
            if start >= stop: raise ValueError
        except ValueError:
            exit('Invalid transcript coordinates in header: >%s' % h)
    elif len(tokens[3]):
        exit('Missing start coordinate in header: >%s' % h)
    else:
        start = None
        stop = None
        
    return Transcript(tokens[0], tokens[1], start, stop, tokens[4])


def main():
    parser = OptionParser(usage=format_usage('''
        %prog [OPTIONS] [GENOME_DIR] <TRANSCRIPT_STRUCTURES >CDNA_FA

        Extracts (spliced) messenger sequences from chromosomes and
        from the description of transcript structures.
    '''))
    parser.add_option('-c', '--coding', dest='coding', action='store_true', default=False, help='extract coding regions only')
    parser.add_option('-5', '--5utr', dest='utr5', action='store_true', default=False, help='extract 5\' UTR regions only')
    parser.add_option('-3', '--3utr', dest='utr3', action='store_true', default=False, help='extract 3\' UTR regions only')
    parser.add_option('-p', '--print-coords', dest='print_coords', action='store_true', default=False, help='print coordinates only')
    options, args = parser.parse_args()

    expected_argnum = 0 if options.print_coords else 1
    if len(args) != expected_argnum:
        exit('Unexpected argument number.')
    elif sum([options.coding, options.utr5, options.utr3]) > 1:
        exit('Use only one of --coding, --5utr, --3utr.')

    tr_gen = parse_structures(MultipleBlockStreamingReader(stdin, join_lines=False))
    if options.coding:
        tr_gen = make_filter(tr_gen, Transcript.trim_to_coding)
    elif options.utr5:
        tr_gen = make_filter(tr_gen, Transcript.trim_to_5utr)
    elif options.utr3:
        tr_gen = make_filter(tr_gen, Transcript.trim_to_3utr)

    if options.print_coords:
        print_coordinates(tr_gen, stdout)
    else:
        with TemporaryFile() as fd:
            print_coordinates(tr_gen, fd)
            fd.flush()
            fd.seek(0)
        
            proc = Popen(['seqlist2fasta', '-l', args[0]], stdin=fd, stdout=PIPE, close_fds=True)
            w = Writer(stdout)
            for label, strand, seq in iter_exon_sequences(proc.stdout):
                w.write(label, strand, seq)
            w.flush()

if __name__ == '__main__':
    main()
