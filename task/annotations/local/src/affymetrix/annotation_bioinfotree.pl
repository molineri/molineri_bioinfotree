#!/usr/bin/perl
# Zebrafish affymetrix platform annotation #

# $ARGV[0]: annotation file (affymetrix csv);
# $ARGV[1]: output file (probeset\tentrex);

$usage ="$0 annotation_file gene_history gene_info affy_probe-entrez TAXONOMY_ID";

if($ARGV[0] =~ /^-h/){
	print $usage."\n";
	exit(0);
}


open (ANN, "$ARGV[0]") || die "Unable to open annotation file!\n$!\n";
open (OUT, "> $ARGV[3]") || die "Unable to open output1 file!\n$!\n";

while (chomp ($line = <ANN>)){
   if ($line =~ /\#/) {
      next;
   } else {
	@line = split /\"\,\"/, $line;
	$line[0] =~ s/\"//g;
	$line[0] =~ s/\s//g;
	$line[18] =~ s/\s//g;
	if ($line[18] =~ /---/){	
		print OUT ("$line[0]\t$line[18]\n");
	} elsif ($line[18] =~ /\//){
		$line[18] =~ s/\/\/\//\t/g;
		print OUT ("$line[0]\t$line[18]\n");
	} elsif ($line[18] =~ /\D+/){
		print OUT ("$line[0]\t$line[18]\n");
	} elsif ($line[18] !~ /---/ && $line[18] !~ /\// && $line[18] =~ /\d+/) {
		print OUT ("$line[0]\t$line[18]\n");
	}
   }
}

close (ANN);
close (OUT);

#$tax = '7955'; #TAXONOMY CODE
$tax = $ARGV[4]; #TAXONOMY CODE
chomp $tax;
die if $tax !~ /^\d+$/;

open (IN, $ARGV[1]) || die;
open (IN2, $ARGV[2]) || die;
open (IN1, $ARGV[3]) || die;

open (OUT, "> $ARGV[3]_entrez_annoted") || die;
open (OUT2, "> $ARGV[3]_entrez_retired") || die;

while (<IN2>) {
   chomp ($_);
   next if ($_ =~ /^\#/);
   @array = split (/\t/, $_, 3);
   if ($array[0] == $tax) { # entrez gene linked to the species specific
       $species{$array[1]} = $array[0];
   }
}

while (<IN>) {
   chomp ($_);
   next if ($_ =~ /^\#/);
   @array = split (/\t/, $_);
   if ($array[0] == $tax) {
      if ($array[1] =~ /\-/) {
          $hash{$array[2]} = $array[1]; #key = retired; value = new (one if it exists) or -
      } elsif ($species{$array[1]}) {
          $hash{$array[2]} = $array[1];
      }
   }
}

while (<IN1>) {
   chomp ($_);
   @array = split (/\t/, $_);
   if ($array[1] =~ /\-\-\-/) {
      print OUT2 "$_\n";
   } elsif (!$array[2]) { # the entrez is uniq
      if (!$hash{$array[1]} && $species{$array[1]}) {
         print OUT "$_\n"; # entrez has not been retired
      } elsif (!$hash{$array[1]} && !$species{$array[1]}) {
         print OUT2 "$_\n";
      } elsif ($hash{$array[1]} =~ /[0-9]/ && $species{$hash{$array[1]}}) {
         print OUT "$array[0]\t$hash{$array[1]}\n"; # entrez has been retired and changed
      } elsif (!$species{$array[1]}) {
         print OUT2 "$_\n"; # entrez has been retired and not changed -
      }
   } elsif ($array[2]) { # there are more entrez
      $line = 'AAA';
      @entrez = ();
      @entrez_sorted = ();
      $flag = 0;
      for ($i = 1; $i <= $#array; $i++) {
          if (!$hash{$array[$i]}) {
	     if ($line =~ /AAA/) {
	        $line = $array[0]."\t".$array[$i];
		push (@entrez, $array[$i]);
	     } else {
	        $line = $line."\t".$array[$i];
	        push (@entrez, $array[$i]); # array of entrez associated to the probeset
	     }
	  } elsif ($hash{$array[$i]}) {
             if ($hash{$array[$i]} =~ /[0-9]/) {
	        if ($line =~ /AAA/) {
	           $line = $array[0]."\t".$hash{$array[$i]};
		   push (@entrez, $hash{$array[$i]});
    	        } else {
	           $line = $line."\t".$hash{$array[$i]};
		   push (@entrez, $hash{$array[$i]});
	        }
	     }
          }
       }
       if ($line ne 'AAA') {
          @entrez_sorted = sort { $a <=> $b } @entrez;
             if ($entrez_sorted[0] == $entrez_sorted[$#entrez_sorted] && $species{$entrez_sorted[0]}) {
                $line = $array[0]."\t".$entrez_sorted[0];
	        $flag = 1;
		print OUT "$line\n";
	     } else {
		$flag = 0;
		foreach $gene (@entrez_sorted) {
		   if ($species{$gene} && $flag == 0) {
                      print OUT "$array[0]\t$gene";
		      $flag = 1;
		   } elsif ($species{$gene} && $flag == 1) {
                      print OUT "\t$gene";
		   }
		}
 		if ($flag == 1) {
                   print OUT "\n";
		} elsif ($flag == 0) {
                   print OUT2 "$_\n";
		}
             }
       } else {
	   print OUT2 "$_\n";
       }
   }
}

close (IN);
close (IN1);
close (IN2);
close (OUT);
close (OUT2);
