#!/usr/bin/env python
#
# Copyright 2009-2010 Gabriele Sales <gbrsales@gmail.com>

from itertools import groupby
from operator import itemgetter
from optparse import OptionParser
from sys import stdin, stderr
from vfork.io.colreader import Reader
from vfork.util import format_usage, exit

def iter_features(fd):
    reader = Reader(fd, '13s,1s,2u,3u,4s,12s,11s', False)
    last_transcript = None
    for transcript, chrom, start, stop, strand, group, type_ in reader:
        if start < 0 or stop < start:
            exit('Invalid feature coords at line %d: %d, %d' % (reader.lineno(), start, stop))
        elif start == stop:
            continue
        elif strand not in '+-':
            exit('Invalid feature strand at line %d: %s' % (reader.lineno(), strand))
        elif transcript == '-': #or (group != 'reference' and group != 'Primary_Assembly') or type_ not in ('CDS', 'UTR'):
            continue
        elif last_transcript is not None and last_transcript > transcript:
            exit('Disorder in transcript name found at line %d.' % reader.lineno())
        else:
            last_transcript = transcript
    	    #print >>stderr, (transcript, chrom, start, stop, strand, group, type_)
            yield transcript, chrom, start-1, stop, strand, type_

def extract_cds_range(features):
    cds_features = [ f for f in features if f[5] == 'CDS' ]
    if len(cds_features):
        return str(cds_features[0][2]), str(cds_features[-1][3])
    else:
        return '', ''

def main():
    parser = OptionParser(usage=format_usage('''
        %prog [OPTIONS] <MAPVIEW_INFO >TRANSCRIPT_STRUCTURES

        Extracts transcript structures from MapView file 'seq_gene.md'.
    '''))
    options, args = parser.parse_args()
    if len(args) != 0:
        exit('Unexpected argument number.')

    for transcript, features in groupby(iter_features(stdin), itemgetter(0)):
        features = list(features)
        features.sort(key=itemgetter(2))

        chrom = features[0][1]
        strand = 1 if features[0][4] == '+' else -1
        cds_range = extract_cds_range(features)
        print '>%s\t%s\t%s\t%s\t%d' % (transcript, chrom, cds_range[0], cds_range[1], strand)

        for idx, feature in enumerate(features):
            print '%s_%d\t%d\t%d' % (transcript, idx+1, feature[2], feature[3])


if __name__ == '__main__':
    main()
