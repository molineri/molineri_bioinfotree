#!/usr/bin/env perl
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

use warnings;
use strict;
$,="\t";
$\="\n";

my $species = shift @ARGV;

use Bio::EnsEMBL::Registry;

my $registry = 'Bio::EnsEMBL::Registry';
$registry->load_registry_from_db(
	-host => 'ensembldb.ensembl.org',
	-user => 'anonymous'
);

my $slice_adaptor = $registry->get_adaptor("$species", 'Core', 'Slice');
my $transcript_adaptor  = $registry->get_adaptor("$species", 'Core', 'Transcript');

my @chromosomes = @{$slice_adaptor->fetch_all('chromosome')};
foreach my $chromosome ( @chromosomes ) {
	foreach my $transcript ( @{$chromosome->get_all_Transcripts(1)} ) {
		next unless $transcript->is_current;
		
		my $stable_id = $transcript->stable_id;
		my $length = $transcript->length;

		my $start = $transcript->cdna_coding_start;
		my $stop = $transcript->cdna_coding_end;
		my $utr5_start = "";
		my $utr3_stop = "";

		if (defined($start) and $start != 0) {
			$utr5_start = 0;
		}

		if (defined($stop) and $stop != $length) {
			$utr3_stop = $length;
		}

		if (!defined($start)) {
			$start = "";
		}
		else {
			$start -= 1;
		}
		
		if (!defined($stop)) {
			$stop = "";
		}
	
		print $stable_id, $length, $start, $stop, $utr5_start, $utr3_stop;
	}
}
