#!/usr/bin/env python
#
# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2009 Alessandro Coppe <alexcoppe@gmail.com>

from __future__ import with_statement
from optparse import OptionParser
from sys import stdin, stderr
from vfork.util import exit, format_usage, safe_import
import re

class ExactPos(object):
	def __init__(self, coord):
		self.coord = coord

class RangePos(object):
	def __init__(self, start):
		self.start = start

class BioPythonParser(object):
	def __init__(self, fd):
		with safe_import('BioPython (http://biopython.org)'):
			from Bio.GenBank import Iterator, RecordParser

		self._iter = Iterator(fd, RecordParser())
		self._simple_location_rx = re.compile(r'<?(\d+)\.\.>?(\d+)')
		self._join_location_rx = re.compile(r'join\(([^)]+)\)')
	
	def __iter__(self):
		return self._iter.__iter__()
	
	def parse_start(self, coord):
		return int(coord)-1
	
	def parse_stop(self, coord):
		return int(coord)

	def parse_pos(self, coord):
		idx = coord.find('^')
		if idx != -1:
			coord = coord[:idx]

		if coord.startswith('>'):
			klass = RangePos
			coord = coord[1:]
		else:
			klass = ExactPos
		return klass(self.parse_start(coord))

	def parse_location(self, location):
		m = self._simple_location_rx.match(location)
		if m:
			return [(self.parse_start(m.group(1)), self.parse_stop(m.group(2)))]
		
		m = self._join_location_rx.match(location)
		if m:
			parts = m.group(1).split(',')
			res = []
			for part in parts:
				m = self._simple_location_rx.match(part)
				if m is None: return None
				res.append((self.parse_start(m.group(1)), self.parse_stop(m.group(2))))
			return res
		
		else:
			print >>stderr, '[WARNING] Cannot parse location:', location
			return None

def gbparsy_records():
	with safe_import('gbparsy (http://code.google.com/p/gbfp/)'):
		from gbparsy import gbparsy

	parser = gbparsy()
	parser.parse()
	return parser.lRawResults

def gbparsy_parse_location(location):
	# TODO: start-1
	return location

def maybe_format_parts(parts):
	if parts is None:
		return ''
	else:
		return ';'.join(('%d:%d' % p) for p in parts)

def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] <GENBANK >CDNAS

		Extract mRNA information from RefSeq files.
	'''))
	parser.add_option('-p', '--parser', dest='parser', default='biopython', help='the GenBank record parser: either biopython or gbparsy')
	parser.add_option('-s', '--species', dest='species', help='keep records for SPECIES only', metavar='SPECIES')
	options, args = parser.parse_args()
	if len(args) != 0:
		exit('Unexpected argument number.')


	if options.parser == 'biopython':
		parser = BioPythonParser(stdin)
	elif options.parser == 'gbparsy':
		#records = gbparsy_records()
		#parse_location = gbparsy_parse_location
		exit('gbparsy is not working right now.')
	else:
		exit('Unknown parser: ' + option.parser)

	if options.species is not None:
		options.species = options.species.lower()
	
	for record in parser:
		if options.species is not None:
			if record.organism.lower() != options.species:
				continue

		length = int(record.size)
		start = stop = parts = None
		polyA = []
		for feature in record.features:
			if feature.key == 'CDS':
				locations = parser.parse_location(feature.location)
				if locations is None:
					continue

				start = min(l[0] for l in locations)
				stop = max(l[1] for l in locations)
				if len(locations) > 1: parts = locations

			elif feature.key == 'polyA_site':
				pos = parser.parse_pos(feature.location)
				if pos is None:
					exit('Cannot parse polyA location.')
				elif isinstance(pos, RangePos):
					# We ignore polyA sites whose position is annotated as:
					# >NUM
					# since they do not provide an exact coordinate.
					print >>stderr, '[WARNING] Ignoring polyA site whose position has form >NUM for %s.' % record.version
					continue
				else:
					polyA.append(pos.coord+1)

		if None in (start, stop):
			# Non coding transcript.
			# Print name, length and continue.
			print '\t'.join(str(v) for v in (record.version, length, '', '', '', '', ''))
			continue

		if (stop - start) % 3 != 0:
			print >>stderr, '[WARNING] The length of the coding sequence for %s is not a multiple of 3.' % record.version

		utr5_start = 0 if start != 0 else ''

		# The 3'UTR is computed as follows: it is the
		# region going from the coding stop to the first
		# annotated polyA site following (included). If such
		# region has length 0, the UTR is considered missing.
		if len(polyA) == 0:
			polyA = None
		else:
			for p in polyA:
				if p >= stop:
					polyA = p
					break
			else:
				polyA = None

		if polyA is not None and polyA != stop:
			utr3_stop = polyA
		elif polyA is None and stop != length:
			utr3_stop = length
		else:
			utr3_stop = ''

		print '\t'.join(str(v) for v in (record.version, length, start, stop, utr5_start, utr3_stop, maybe_format_parts(parts)))

if __name__ == '__main__':
	main()
