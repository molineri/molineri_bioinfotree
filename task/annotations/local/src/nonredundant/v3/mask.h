#ifndef MASK_H
#define MASK_H

typedef struct
{
	unsigned char* space;
	unsigned int size;
} mask_t;

typedef struct
{
	const mask_t* mask;
	unsigned int pos;
	
	unsigned int span_start;
	unsigned int span_stop;
	unsigned int span_label;
} mask_iterator_t;

int mask_init(mask_t* const mask, const unsigned int size);
void mask_free(mask_t* const mask);
void mask_set(mask_t* const mask, const unsigned int start, const unsigned int stop, const char label);

int mask_iterator_init(mask_iterator_t* const it, const mask_t* const mask);
void mask_iterator_free(mask_iterator_t* const it);
int mask_iterator_has_more(const mask_iterator_t* const it);
void mask_iterator_advance(mask_iterator_t* const it);
unsigned int mask_iterator_span_start(const mask_iterator_t* const it);
unsigned int mask_iterator_span_stop(const mask_iterator_t* const it);
char mask_iterator_span_label(const mask_iterator_t* const it);

#endif //MASK_H