#include "mask.h"
#include <stdio.h>

typedef struct
{
	unsigned int start;
	unsigned int stop;
	char label;
} region_t;

int read_region(FILE* stream, region_t* region)
{
	char line[4096];
	if (!fgets(line, 4096, stream))
		return 0;
	
	return parse_region(line, region);
}

int main(int argc, char** argv)
{
	if (argc != 2)
	{
		fprintf(stderr, "Unexpected argument number.\n");
		return 1;
	}
	
	unsigned int sequence_size;
	if (!parse_uint(argv[1], &sequence_size))
	{
		fprintf(stderr, "Invalid sequence size: %s\n", argv[1]);
		return 1;
	}
	
	mask_t mask;
	mask_init(&mask, )
	
	region_t region;
	
	
	mask_iterator_t it;
	
	mask_init(&m, 300);
	mask_set(&m, 0, 10, 1);
	mask_set(&m, 5, 15, 2);
	
	mask_iterator_init(&it, &m);
	while (mask_iterator_has_more(&it))
	{
		mask_iterator_advance(&it);
		printf("from %d to %d: %d\n", mask_iterator_span_start(&it), mask_iterator_span_stop(&it), mask_iterator_span_label(&it));
	}
	
	mask_iterator_free(&it);
	mask_free(&m);
	
	return 0;
}