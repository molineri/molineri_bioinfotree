#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;

my $usage="
$0 -c coord_coding -5u coord_5utr -3u coord_3utr -i coord_intron -u coord_upstream [-v]> coord_nonredundant
";

my $filename_coding;
my $filename_5utr;
my $filename_3utr;
my $filename_intron;
my $filename_upstream;
my $filename_nce;
my $verbose=0;

my ($F_CODING, $F_5UTR, $F_3UTR, $F_INTRON, $F_UPSTREAM, $F_NCE);

GetOptions (
	'coding|c=s' 	=> \$filename_coding,
	'5utr|5u=s' 	=> \$filename_5utr,
	'3utr|3u=s' 	=> \$filename_3utr,
	'intron|i=s' 	=> \$filename_intron,
	'non_coding_exon|nce=s' => \$filename_nce,
	'upstream|u=s' 	=> \$filename_upstream,
	'verbose|v+'	=> \$verbose
) or die($usage);

$filename_coding and $filename_5utr and $filename_3utr and $filename_intron and $filename_upstream and $filename_nce or die($usage);


open $F_CODING, $filename_coding or die("can't open file ($filename_coding)\n"); 	# C
open $F_5UTR, $filename_5utr or die("can't open file ($filename_5utr)\n");		# 5
open $F_3UTR, $filename_3utr or die("can't open file ($filename_3utr)\n");		# 3
open $F_INTRON, $filename_intron or die("can't open file ($filename_intron)\n");	# I
open $F_NCE, $filename_nce or die("can't open file ($filename_nce)\n");			# E
open $F_UPSTREAM, $filename_upstream or die("can't open file ($filename_upstream)\n");	# U
											# N intergenic

my %coding=();
	$verbose and print STDERR "load and merging coding\n";
&fondi_sovrapposti($F_CODING,\%coding);
my %utr5=();
	$verbose and print STDERR "load and merging utr5  \n";
&fondi_sovrapposti($F_5UTR,\%utr5);
my %utr3=();
	$verbose and print STDERR "load and merging utr3  \n";
&fondi_sovrapposti($F_3UTR,\%utr3);
my %intron=();
	$verbose and print STDERR "load and merging intron\n";
&fondi_sovrapposti($F_INTRON,\%intron);
my %nce=();
	$verbose and print STDERR "load and merging nce   \n";
&fondi_sovrapposti($F_NCE,\%nce);
my %upstream=();
	$verbose and print STDERR "load and merging upstre\n";
&fondi_sovrapposti($F_UPSTREAM,\%upstream);


my @chrs = keys %coding;
@chrs=sort @chrs;

foreach my $chr (@chrs){
	my @annotation=();
	
		$verbose and print STDERR "\n$chr  U\n";
	&aggiungi_annotazione_dal_basso(\@annotation,\@{$upstream{$chr}},'U');
		$verbose and print STDERR "\n$chr  I\n";
	&aggiungi_annotazione_dal_basso(\@annotation,\@{$intron{$chr}},'I');
		$verbose and print STDERR "\n$chr  E\n";
	&aggiungi_annotazione_dal_basso(\@annotation,\@{$nce{$chr}},'E');
		$verbose and print STDERR "\n$chr  3\n";
	&aggiungi_annotazione_dal_basso(\@annotation,\@{$utr3{$chr}}  ,'3');
		$verbose and print STDERR "\n$chr  5\n";
	&aggiungi_annotazione_dal_basso(\@annotation,\@{$utr5{$chr}}  ,'5');
		$verbose and print STDERR "\n$chr  C\n";
	&aggiungi_annotazione_dal_basso(\@annotation,\@{$coding{$chr}}  ,'C');

	for(@annotation){
		defined($_) and print $chr,"\t",join("\t",@{$_}),"\n";
	}
}



sub aggiungi_annotazione_dal_basso{
	my $annotation = shift;
	my $new_segments = shift;
	my $label = shift;

	my $i=0;
	my $j=0;
	my $imax=scalar(@{$new_segments});
	my $mill=int($imax/1000);

	foreach my $new_seg (@{$new_segments}){
		my ($start,$stop)=@{$new_seg};
		&aggiungi_segmento_dal_basso($annotation,$start,$stop,$label);
		


		if($verbose and $j > $mill){
			printf STDERR "%.1f %%\r", $i/$imax*100;
			$j=-1
		}
		$j++;
		$i++;
	}
}

sub aggiungi_segmento_dal_basso{

	my $annotation= shift;
	my $start = shift;
	my $stop = shift;
	my $label = shift;
	
	
 	my $imax=scalar(@{$annotation});

	for(my $i=0; $i<$imax; $i++){
		my $old_seg;
		if(defined(@{$annotation}[$i])){#in caso di sovrapposizione di segmenti un segmeno viene posto =undef
			$old_seg=@{$annotation}[$i];
		}else{	
			next;
		}

		my ($pre_start,$pre_stop, $pre_label) = @{$old_seg};


		next if($pre_stop<$start);
		next if($pre_start>$stop);
		


		if($start <= $pre_start and $stop>= $pre_stop){
			#il nuovo si sovrappone totalmente al vecchio:
			

			@{$annotation}[$i]=undef; #il segmento nuovo copre interamente uno vecchio


			
		}elsif($start > $pre_start and $stop < $pre_stop){ #in casodi coincidenza ricade nel caso sopra
			#il vecchio si sovrappone completamente al nuovo
			#devo spezzare il segmento vecchio in 2:
		
			@{$annotation}[$i]=undef; #tolgo quello veccio; ne aggiungo 2 nuovi
			my @tmp1 = ($pre_start, $start, $pre_label);
			push @{$annotation},\@tmp1;
			my @tmp2 = ($stop, $pre_stop, $pre_label);
			push @{$annotation},\@tmp2;


			
		}else{	#	sovrapposizione parziale
			my $pre_start_bak=$pre_start;
			my $pre_stop_bak=$pre_stop;
			if($start==$pre_start){
				$pre_start=$stop;
			}
			if($stop==$pre_stop){
				$pre_stop=$start;
			}
			if($start <= $pre_stop and $start > $pre_start){#start casca in mezzo ad un segmento
									#presistente
			
				$pre_stop = $start; 	#tronco il segmento preesistente
							#in modo che finisca un nucleotide prima
							#dell'inizio del nuovo segmento
			}elsif($stop < $pre_stop_bak and $stop >= $pre_start){#stop casca in mezzo ad un segmento preesistente
				$pre_start = $stop ; #tronco il segmento preesistente
							#in modo che inizi un nucleotide dopo
						 	#dello stop del nuovo segmento	
			}
#$pre_start<=$pre_stop or die("d $start $stop, $pre_start_bak $pre_stop_bak, $pre_start $pre_stop\n");
			@{$old_seg}=($pre_start, $pre_stop, $pre_label);
		}
	}
	
	################################
	#	i nuovi segmenti hanno priorita` sui precedenti
	#	percui il segmento nuovo sicuramente viene inserito
	#	
	my @tmp=($start,$stop,$label);
	push @{$annotation},\@tmp;
}




sub fondi_sovrapposti{
	my $FH=shift;
	my $segments=shift;

	my $pre_chr=undef;
	my $i=-1;#non conta le righe ma gli elementi di ${$segments}{$chr} che sono <= alle righe
	while(<$FH>){#mi aspetto dati ordinati per cromosoma e per start nel file di input
		#print;
		chomp;
		my ($chr,$start,$stop)=split /\t/;
		#die($start);
		
		if(!defined(${$segments}{$chr})){
			my @tmp=();
			${$segments}{$chr}=\@tmp;
		}
		
		my $pre = undef;
		if(defined($pre_chr) and $chr eq $pre_chr){
			$pre=@{${$segments}{$chr}}[$i];
			#print ${$pre}[1]."\n";
			#die  ${$pre}[1]."\n";
		}else{
			$pre_chr=undef;
			$i=-1;
		}

		if(defined($pre_chr) and $chr eq $pre_chr and $start < ${$pre}[1]){#sovrapposizione
			$stop>${$pre}[1] and ${$pre}[1]=$stop;#aggiorno lo stop solo se il novo stop e` piu` grande`
		}else{
			my @tmp=($start,$stop);
			push @{${$segments}{$chr}}, \@tmp;
			$i++
		}

		$pre_chr=$chr;
	}
	#my @chrs = keys %{$segments};
	#foreach my $chr (@chrs){
	#	for(@{${$segments}{$chr}}){
	#		print join("\t",@{$_})."\n";
	#	}
	#}
	#print "fine\n";
	#die;
}
