#!/usr/bin/perl -w

use strict;
use warnings;
use DBI;
use Getopt::Std;

our ($opt_d, $opt_u, $opt_p, $opt_P, $opt_h);
getopts('d:u:p:P:h');

my $usage = <<EOF;
usage go_tables.pl
[-d database (default "mygo")]
[-u user (default "root")]
[-P password (default "")]
[-p prefix to output files (default ./)]
EOF

if ($opt_h) {
    die $usage;
}

my $database = "ipg";
if ($opt_d) {
    $database = $opt_d;
}
my $user = "ugo";
if ($opt_u) {
    $user = $opt_u;
}
my $pass = "";
if ($opt_P) {
    $pass = $opt_P;
}
my $prefix = "./";
if ($opt_p) {
    $prefix = $opt_p;
}

my $dbh = DBI->connect("dbi:mysql:$database", $user, $pass);
my $query;
my $sth;
my $array_ref;
my $table;

# go_term.txt, go_obsolete.txt
open TERM, ">$prefix" . "go.term";
open OBS,  ">$prefix" . "go.obs";
$query = <<EOF;
SELECT
        acc, name, term_type, is_obsolete
FROM
        term
EOF

do_it($query);

foreach my $row (@$array_ref) {
    next unless (${$row}[0] =~ /^GO:\d{7}$/);
    if (${$row}[3] == 1) {
        print OBS "${ $row }[0]\n";
    } else {
        pop @{$row};
        print TERM join("\t", @{$row}), "\n";
    }
}
close TERM;
close OBS;

# go_alternate
open ALT, ">$prefix" . "go.alt";
$query = <<EOF;
SELECT
        term.acc, term_synonym.acc_synonym
FROM
        term, term_synonym
WHERE
        term_synonym.acc_synonym IS NOT NULL
AND
        term_synonym.term_id=term.id
EOF

do_it($query);
foreach my $row (@$array_ref) {
    next unless (${$row}[0] =~ /^GO:\d{7}$/ && ${$row}[1] =~ /^GO:\d{7}$/);
    print ALT ${$row}[1], "\t", ${$row}[0], "\n";
}

# go_ancestor
open ANC, ">$prefix" . "go.anc";

$query = <<EOF;
SELECT DISTINCT
        descendant.acc, ancestor.acc
FROM
        term as descendant, graph_path, term as ancestor
WHERE
        ancestor.id=graph_path.term1_id and descendant.id=graph_path.term2_id
AND
        ancestor.acc != 'all'
AND
        graph_path.distance > 0
EOF

do_it($query);
foreach my $row (@$array_ref) {
    next unless (${$row}[0] =~ /^GO:\d{7}$/ && ${$row}[1] =~ /^GO:\d{7}$/);
    print ANC ${$row}[0], "\t", ${$row}[1], "\n";
}
close ANC;

sub do_it {
    $sth = $dbh->prepare($_[0]);
    $sth->execute();
    $array_ref = $sth->fetchall_arrayref();
}
