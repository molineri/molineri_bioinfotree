#!/usr/bin/env python
from __future__ import with_statement
from sys import exit, stdin, stderr
from optparse import OptionParser
from collections import defaultdict

def load_ng_introns(stdin):
	ng_introns = {}
	for line in stdin:
		tokens = line.rstrip().split('\t')
		assert len(tokens) == 4
		ng_introns[tokens[2]]=[int(tokens[0]),int(tokens[1]),tokens[3]]
	return ng_introns

def load_ensemble_coords(filename):

	gene_transcript_map = defaultdict(lambda : [])
	transcript_exon_map = defaultdict(lambda : [])
	exon_coords_map = {}

	with file(filename, 'r') as fd: 
		for line in fd:
			tokens = line.rstrip().split('\t')
			assert len(tokens) == 5
		
			exonid = tokens[0]
			coordb = int(tokens[1])
			coorde = int(tokens[2])
			transcriptid = tokens[3]
			geneid = tokens[4]

			gene_transcript_map[geneid].append(transcriptid)
			transcript_exon_map[transcriptid].append(exonid)
			exon_coords_map[exonid]=[coordb,coorde]

	return gene_transcript_map, transcript_exon_map, exon_coords_map

def main():
	parser = OptionParser(usage='%prog ENSEMBLE_COORDINATE < NG_INTRON_COORDS')
	options, args = parser.parse_args()

	if len(args) != 1:
		exit('Unexpected argument number.')
	
	(gene_transcript_map, transcript_exon_map, exon_coords_map) = load_ensemble_coords(args[0])
	ng_introns = load_ng_introns(stdin)
		
	for intron_id, intron in ng_introns.iteritems():
		count_b = 0
		count_e = 0
		intron_b = intron[0]
		intron_e = intron[1]
		gene=intron[2]
		for t in gene_transcript_map[gene]:
			for exon in transcript_exon_map[t]:
				if intron_b == exon_coords_map[exon][1]:
					count_b += 1 
				if intron_e == exon_coords_map[exon][0]:
					count_e += 1
		print '%s\t%s\t%d\t%d\t%d' % (intron_id, gene, count_b, count_e, len(gene_transcript_map[gene]))


	#print gene_transcript_map	

if __name__ == '__main__':
	main()

