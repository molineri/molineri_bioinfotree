#!/usr/bin/env python
# encoding: utf-8

from __future__ import with_statement

from bisect import bisect_left, bisect_right
from itertools import groupby
from optparse import OptionParser
from sys import exit, stdin
from vfork.io.util import safe_rstrip

def error(msg, lineno, filename):
	msg += ' at line %d'
	if filename is not None:
		msg += ' of file %s'
		exit(msg % (lineno, filename))
	else:
		exit(msg % lineno)

def iter_regions(fd, filename=None):
	last_chr = None
	
	for lineno, line in enumerate(fd):
		tokens = safe_rstrip(line).split('\t', 3)
		if len(tokens) < 4:
			error('Insufficient token number', lineno+1, filename)
		
		chr = tokens[0]
		if last_chr is None or chr > last_chr:
			last_chr = chr
		elif chr < last_chr:
			error('Disorder found', lineno+1, filename)
		
		try:
			start = int(tokens[1])
			if start < 0:
				raise ValueError
		except ValueError:
			error('Invalid start coordinate', lineno+1, filename)
		
		try:
			stop = int(tokens[2])
			if stop <= start:
				raise ValueError
		except ValueError:
			error('Invalid stop coordinate', lineno+1, filename)
		
		yield tokens[0], start, stop, tokens[3]

def iter_genes_by_chr(path):
	with file(path, 'r') as fd:
		for chr, genes in groupby(iter_regions(fd, path), key=lambda g: g[0]):
			yield chr, [ tuple(g[1:]) for g in genes ]

def main():
	parser = OptionParser(usage='%prog GENE_COORDS <REGION_COORDS')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	try:
		gene_iterator = iter_genes_by_chr(args[0])
		gene_chr = None
	except StopIteration:
		exit('The file containing the gene coordinates is empty.')
	
	try:
		for chr, start, stop, region_id in iter_regions(stdin):
			while gene_chr is None or chr > gene_chr:
				gene_chr, genes = gene_iterator.next()
				gene_num = len(genes)
				
				genes.sort(key=lambda g: g[0])
				gene_starts = [ g[0] for g in genes ]
				gene_start_infos = list(genes)
				
				genes.sort(key=lambda g: g[1])
				gene_stops = [ g[1] for g in genes ]
				gene_stop_infos = genes # potentially unsafe; must not modify genes from here on
				
			else:
				if chr < gene_chr:
					continue
			
			gene_after_idx = bisect_left(gene_starts, stop)
			if gene_after_idx < gene_num:
				gene_info = gene_start_infos[gene_after_idx]
				print '%s\t%s\t%s\t%d\t%d\ta\t%d' % (region_id, gene_info[2], gene_chr, gene_info[0], gene_info[1], gene_info[0] - stop)
			
			gene_before_idx = bisect_right(gene_stops, start)
			if gene_before_idx > 0:
				gene_info = gene_stop_infos[gene_before_idx-1]
				print '%s\t%s\t%s\t%d\t%d\tb\t%d' % (region_id, gene_info[2], gene_chr, gene_info[0], gene_info[1], start - gene_info[1])
		
	except StopIteration:
		pass

if __name__ == "__main__":
	main()
