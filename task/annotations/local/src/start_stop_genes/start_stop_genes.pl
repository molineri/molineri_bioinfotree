#!/usr/bin/perl
use warnings;
use strict;

my $usage = "$0 < input
input:
	gene_ID
	chr
	exon_b
	exon_e
	strand
";

if($ARGV[0] and $ARGV[0]=~/^-h/){
	print $usage;
	exit(0);
}

my %gene=();
while(<>){
	chomp;
	my ($gene_id, $chr, $start, $stop,$strand) = split /\t/;
#	if($gene_id=~/ENSG00000131174/){
#		die($_);
#	}else{
#		next;
#	}
	
	if(!defined($gene{$gene_id})){
		my @tmp=($chr,$start,$stop,$strand);
		$gene{$gene_id}=\@tmp;
	}else{
		my ($p_chr,$p_start,$p_stop,$strand)=@{$gene{$gene_id}};
		die("duplicated gene id") if $p_chr ne $chr;
		if($start<$p_start){
			@{$gene{$gene_id}}[1]=$start;
		}
		if($stop>$p_stop){
			@{$gene{$gene_id}}[2]=$stop;
		}
	}
}

$,="\t";
$\="\n";

my @keys= keys(%gene);
for(@keys){
	print $_,@{$gene{$_}};
}
