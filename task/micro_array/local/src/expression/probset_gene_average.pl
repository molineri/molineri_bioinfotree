#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;

my $usage = <<EOF;
usage $0 -t probe2gene_table expression_file
  [-m missing_symbol (default 'NA')]
EOF

our($opt_h, $opt_t, $opt_m, $opt_n);
getopts('ht:m:n:');
if ($opt_h || ! $opt_t) {
    die $usage;
}

if (! defined $opt_m) {
    $opt_m = 'NA';
}

# my @NA;
# if (! defined $opt_n) {
# 	@NA = ();
# }else{
# 	for(my $i=0 ; $i<=$opt_n ; $i++){
# 		push(@NA,"NAN");
# 	}
# }

my %gene;
my %seen;
my @gene;
open PG, $opt_t  or die "Error opening $opt_t: $!\n";
while (<PG>) {
    chomp;
    my ($probe, $gene) = split("\t");
    $gene{$probe} = $gene;
    if (! defined $seen{$gene}) {
        $seen{$gene} = 1;
        push @gene, $gene;
    }
}
close PG;


my %expr;
my $head = <>;
print $head;
while (<>) {
    chomp;
    my @line = split("\t");
    my $probe = shift @line;
    next unless (defined $gene{$probe});
    push @{ $expr{$gene{$probe}}}, $_;
}

foreach my $gene (@gene) {
    next unless defined $expr{$gene};
    my @ave  = (); #@NA
    my @ndata = ();
    for (my $i = 0; $i <= $#{ $expr{$gene} }; ++$i) {
        my @line = split("\t", ${ $expr{$gene} }[$i]);
        shift @line;
        for (my $j = 0; $j <= $#line; ++$j) {
            if ($line[$j] ne $opt_m) {
                $ave[$j] += $line[$j];
                if (! defined $ndata[$j]) {
                    $ndata[$j] = 0;
                }
                ++$ndata[$j];
            }
        }
    }
    for (my $j = 0; $j <= $#ave; ++$j) {
        if ($ndata[$j]) {
            $ave[$j] /= $ndata[$j];
        }
        else {
            $ave[$j] = $opt_m;
        }
    }

my $sample= $#ave + 1;
if (defined $opt_n) {
	for (my $i=$sample ; $i < $opt_n ; $i++) {
		push @ave, "NA";
	}
}
    print $gene, "\t", join("\t", @ave), "\n";
}
