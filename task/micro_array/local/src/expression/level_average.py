#!/usr/bin/env python
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from __future__ import division
from collections import defaultdict
from optparse import OptionParser
from sys import stdin
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage

def average_levels(levels):
	n = len(levels)
	if n == 1:
		return levels[0]
	
	m = len(levels[0])
	res = []
	for i in xrange(m):
		res.append(sum(l[i] for l in levels) / n)
	return res

def main():
	parser = OptionParser(usage=format_usage('''
		%prog <LEVELS >LEVELS

		Reads an expression matrix and computes the average level for each probe
		for each sample.

		Output is sorted by probe name.
	'''))
	parser.add_option('-w', '--with-header', dest='with_header', action='store_true', default=False, help='treat the first input line as an header')
	options, args = parser.parse_args()

	if len(args) != 0:
		exit('Unexpected argument number.')

	levels = defaultdict(list)
	for line_idx, line in enumerate(stdin):
		if line_idx == 0 and options.with_header:
			print safe_rstrip(line)
			continue

		tokens = safe_rstrip(line).split('\t')

		probe = tokens[0]
		try:
			ls = [float(t) for t in tokens[1:]]
		except ValueError:
			exit('Invalid float value at line %d: %s' % (line_idx+1, safe_rstrip(line)))
		levels[probe].append(ls)
	
	probes = levels.keys()
	probes.sort()
	for probe in probes:
		avg = average_levels(levels[probe])
		print '%s\t%s' % (probe, '\t'.join(str(t) for t in avg))

if __name__ == '__main__':
	main()
