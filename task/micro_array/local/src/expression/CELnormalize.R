#!/usr/bin/env Rscript

library("affy")
library("simpleaffy")
library("getopt")

opt <- getopt(matrix(c(
	'verbose', 'v', 2, 'integer',
	'help'   , 'h', 0, 'logical',
	'CELdir' , 'd', 1, 'character',
	'method', 'm', 1, 'character',
	'presentabsent', 'p', 1, 'character',
	'outfile', 'o', 1, 'character'
), ncol=4, byrow=TRUE))

if (!is.null(opt$help)) {
  cat("Normalize CEL file in the given directory.\n\t -m|method [mas5|rma]\n\t-e|CELdir directory containing .CEL files\n\t-v|verbose\n\t-p|presentabs FILE\twrite in file the present-absent data (mas5 only)", sep="\n")
  quit()
}

if(is.null(opt$outfile)){
	write("ERROR: no output file given (-o).",stderr())
	quit(status=1)
}

if(!is.null(opt$help) && opt$method != "mas5"){
	write("ERROR: PresentAbsent calculation available only on mas5 method",stderr())
	quit(status=1)
}

cels <- list.celfiles(full.names=TRUE, path=opt$CELdir) 


if (opt$method == 'mas5'){
        eset <- ReadAffy(filenames=cels, verbose=T, compress=TRUE)#affybatch object
	gse <- justMAS(eset) 
        mas.call <- mas5calls(eset)
        data.mas.call <- exprs(mas.call)
	if(!is.null(opt$presentabsent)){
        	write.table(data.mas.call, file = opt$presentabsent, sep="\t", row.names=T, col.names=NA, quote=F)
	}
} else if (opt$method == 'rma') {
        gse <- justRMA(filenames=cels) #Antonio told me that this is better than using rma on the affybatch object
} else {
	write("ERROR: Wrong method option used!", stderr())
	quit(status=1)
}
matx <- as.table(exprs(gse))
write.table(matx, file = opt$outfile, sep="\t", row.names=T, col.names=NA, quote=F)
