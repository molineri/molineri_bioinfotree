#!/usr/bin/env Rscript
#
# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2010 Davide Risso <davide@stat.unipd.it>

library(getopt)
library(impute)
library(preprocessCore)
library(affy)

plot <- function(name,data) {
  pdf(name)
  if(!is.null(opt$log))
    boxplot(log2(as.data.frame(data)))
  else
    boxplot(as.data.frame(data))
  dev.off()
}

opt_spec <- matrix(ncol=4, byrow=TRUE,
                   c('help'     , 'h', 0, 'logical',
		     'method'   , 'm', 1, 'character',
                     'boxplot'  , 'b', 1, 'character',
                     'log'      , 'l', 0, 'logical',
                     'revertlog', 'r', 0, 'logical'))

opt = getopt(opt_spec)

if (!is.null(opt$help)) {
  usage = getopt(opt_spec, usage=TRUE)
  cat(substring(usage, 1, nchar(usage)-1), "<RAW_EXPRESSION >NORMALIZED\n\n")
  cat("Normalize the expression values of a single channel microarray set of experiments.","","The input matrix must have row and column names.","","The supported normalization methods are:","* quantile", "* loess (cyclic)","","The --boxplot option takes the prefix that will be used to name the PDF output.","","If --log data are transformed in logarithm.","","If --revertlog data are trasformed in logarithm before the normalization and then transformed back to the original scale.","", sep="\n")
  quit()
}

if (is.null(opt$method)) {
  stop("Missing normalization method.")
} else if (!(opt$method %in% c("quantile", "loess"))) {
  stop(paste("Unknown normalization method:", opt$method, sep=" "))
}
sink("/dev/null")
data <- as.matrix(read.table(file="stdin", header=TRUE, row.names=1, sep="\t"))

if (!is.null(opt$boxplot)) {
  plot(paste(opt$boxplot, "raw.pdf", sep="-"), data)
}

if (!is.null(opt$log)) {
    data <- log2(data)
    data <- impute.knn(data,k=5)$data
}

if (opt$method=="quantile") {
  data.norm <- normalize.quantiles(data)
}
if (opt$method=="loess") {
  data.norm <- impute.knn(data,k=5)$data
  data.norm <- normalize.loess(data.norm, log.it=F, verbose=F)
}

if(!is.null(opt$revertlog))
    data.norm<-2^data.norm

if (!is.null(opt$boxplot)) {
  plot(paste(opt$boxplot,"-", opt$method, ".pdf", sep=""),data.norm)
}

sink()
cat("\t")
write.table(data.norm, row.names=rownames(data), col.names=colnames(data), sep='\t', quote=F)
