#!/usr/bin/env python
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from optparse import OptionParser
from os import system
from vfork.util import exit, format_usage

def main():
	parser = OptionParser(usage=format_usage('''
		%prog NAME >LEVELS

		Downloads expression level file from GEO.
	'''))
	options, args = parser.parse_args()

	if len(args) != 1:
		exit('Unexpected argument number.')
	
	name = args[0]
	url = 'http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=%s&targ=gsm&form=text&view=full' % name
	if system("wget -O - '%s' | sed '/^[!#^]/d' | tr -d '\r' | unhead" % url) != 0:
		exit('Cannot download %s.' % name)

if __name__ == '__main__':
	main()
