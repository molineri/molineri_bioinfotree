#!/usr/bin/env python
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from commands import getstatusoutput
from optparse import OptionParser
from os import system
from vfork.util import exit, format_usage
import re

def main():
	parser = OptionParser(usage=format_usage('''
		%prog GEO_ID >TABLE

		Downloads table data from GEO.
	'''))
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	prefix = 'http://www.ncbi.nlm.nih.gov/geo/query'
	status, output = getstatusoutput("wget -q -O- '%s/acc.cgi?acc=%s'" % (prefix, args[0])) 
	if status != 0:
		exit('Cannot retrieve object %s.' % args[0])
	
	m = re.search(r"OpenLink\('([^']+)", output)
	if m is None:
		exit('Unexpected reply.')
	
	if system("wget -O - '%s/%s'" % (prefix, m.group(1))) != 0:
		exit('Cannot retrieve object %s.' % args[0])

if __name__ == '__main__':
	main()

