#!/usr/bin/env python
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from optparse import OptionParser
from os import system
from vfork.util import exit, format_usage

def main():
	parser = OptionParser(usage=format_usage('''
		%prog NAME >CEL

		Downloads a CEL file from GEO.
	'''))
	options, args = parser.parse_args()

	if len(args) != 1:
		exit('Unexpected argument number.')
	
	name = args[0]
	dirname = name[:-3] + 'nnn'
	url = 'ftp://ftp.ncbi.nih.gov/pub/geo/DATA/supplementary/samples/%s/%s/%s*.CEL.gz' % (dirname, name, name)
	if system('wget -O - %s | zcat' % url) != 0:
		exit('Cannot download %s.' % name)

if __name__ == '__main__':
	main()
