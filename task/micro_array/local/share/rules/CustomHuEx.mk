ENS_ANNOT_DIR = $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/$(SPECIES)/$(ENS_VERSION)
UCSC_ANNOTATION_DIR =  $(BIOINFO_ROOT)/task/annotations/dataset/ucsc/$(SPECIES)/$(UCSC_VERSION)
extern $(ENS_ANNOT_DIR)/exon_transctip_gene_full.gz

ALL += present_absent_gene_level_matrix.gz custom_normalization_gene_level_matrix.gz metaProbset-gene.univoc.map

metaprobset-probset.map.gz: $(MPS_FILE)
	grep -v '^#' $< \
	| unhead \
	| cut -f 1,3 | sed 's/ $$//'\
	| expandsets -s " " 2 \
	| gzip > $@

.META: metaprobset-probset.map.gz
	1	metaprobset
	2	probeset

CEL/%.gz:
	@echo ERROR manually link the CEL file

CEL/%: CEL/%.gz
	zcat $< > $@

custom_normalization: $(addprefix CEL/, $(SAMPLES))
	apt-probeset-summarize \
	-p $(PGF_FILE) \
	-c $(CLF_FILE) \
	-a rma-sketch \
	-x 15 \
	--meta-probesets $(MPS_FILE) \
	-o $@ \
	$^

present_absent: $(addprefix CEL/, $(SAMPLES))
	apt-probeset-summarize \
	-p $(PGF_FILE) \
	-c $(CLF_FILE) \
	-a dabg \
	-b $(ANTIGENOMIC_FILE) \
	-o $@ \
	$^

%_family.soft.gz: 
	wget ftp://ftp.ncbi.nih.gov/pub/geo/DATA/SOFT/by_platform/$*/$*_family.soft.gz

annotations.bed.gz: $(ANNOT_FILE)
	cat $<  | sed 's/","/\t/g; s/"//g' | grep -v '^#' | unhead \
	| bawk '$$3!="control" && $$3!="Not currently mapped to latest genome"  && $$3!="---" && $$3!="" && $$6!=0 {print $$3,$$5,$$6,$$1}' \ 
	| gzip >$@

annotations.ens.isec.gz: annotations.bed.gz $(UCSC_ANNOTATION_DIR)/ensembl_genes.exons $(ENS_ANNOT_DIR)/exon_transctip_gene_full.gz
	intersection \
	<(\
		zcat $< | sed 's/chr//' | bsort -S20% -k1,1 -k2,2n\
	) \
	<(\
		bawk '{print $$chr,$$b,$$e,$$trans_id}' $^2 | bsort -S20% -k1,1 -k2,2n\
	) | gzip > $@

metaProbset-gene.map: annotations.ens.isec.gz $(ENS_ANNOT_DIR)/transcript-gene.map.gz $(ENS_ANNOT_DIR)/genes.gz
	zcat $< | cut -f 8,9 \
	| translate <(zcat $^2) 2 \
	| sort | uniq \
	| translate -a <(bawk '{print $$gene_ID,$$biotype}' $^3) 2 >$@

.META: metaProbset-gene.map
	1	metaProbsetId
	2	ensg
	3	biotype

metaProbset-gene.deneger.map: metaProbset-gene.map
	translate -a <(cut -f 1 $< | symbol_count) 1 < $<\
	| translate -a <(cut -f 2 $< | symbol_count) 3 > $@

.META: metaProbset-gene.deneger.map
	1	metaProbsetId
	2	metaprob_count
	3	ensg
	4	ensg_count
	5	biotype

metaProbset-gene.univoc.map: metaProbset-gene.deneger.map
	bawk '$$metaprob_count==1 {print $$metaProbsetId,$$ensg}' $< \
	| sort | uniq > $@

custom_normalization_gene_level_matrix.gz: custom_normalization
	grep -v '^#' $</rma-sketch.summary.txt \
	| gzip > $@

present_absent_gene_level_matrix.gz: present_absent probeset_metadata.gz
	zgrep -v '^#' $</dabg.summary.txt \
	| bawk 'BEGIN{printf("%s",">")} {print}' \
	| filter_1col -f 1 <(bawk '$$crosshyb_type==1 {print $$probeset_id}' $^2)\   *tengo solo i probset che mappano univocamente*
	| gzip > $@

diff_exp.wilcox.gz diff_exp.limma.gz : diff_exp.%.gz: custom_normalization_gene_level_matrix.gz
	diffexpression -l `tr " " "," <<<"$(LEFT_SAMPLES)"` -r `tr " " "," <<<"$(RIGHT_SAMPLES)"` -t $* < $< \
	| bsort -S30% -k 2,2g \
	| gzip > $@

diff_exp.rankp.gz: diff_exp.%.gz: custom_normalization_gene_level_matrix.gz
	diffexpression -l `tr " " "," <<<"$(LEFT_SAMPLES)"` -r `tr " " "," <<<"$(RIGHT_SAMPLES)"` -t $* < $< \
	| bsort -S30% -k3,3g \
	| unhead -n 9 \
	| gzip > $@


diff_exp.%.annot.gz: diff_exp.%.gz metaProbset-gene.univoc.map metaProbset-gene.map
	zcat $< | translate -k -a $^2 1 \
	| translate -a <(cut -f 2,3 metaProbset-gene.map | sort | uniq) 2 \
	| gzip > $@
