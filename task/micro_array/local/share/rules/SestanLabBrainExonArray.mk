GEO_URL_PREFIX = http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi
ENS_ANNOT_DIR = $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/hsapiens/64
AFFY= $(BIOINFO_ROOT)/task/micro_array/local/share/data

%_family.soft.gz:
	wget -O $@ ftp://ftp.ncbi.nih.gov/pub/geo/DATA/SOFT/by_series/$(GEO_SERIES)/$(GEO_SERIES)_family.soft.gz

samples.txt: 
	wget -O $@ '$(GEO_URL_PREFIX)?acc=$(GEO_SERIES)&targ=self&form=text'

samples_ids: $(GEO_SERIES)_family.soft.gz
	zcat $< | perl -lne 'print $$1 if m/^\^SAMPLE\s+=\s+(.*)/' > $@


metaprobset-probset.map.gz: $(AFFY)/HuEx-1_0-st-v2.r2.dt1.hg18.full.mps
	grep -v '^#' $< \
	| unhead \
	| cut -f 1,3 | sed 's/ $$//'\
	| expandsets -s " " 2 \
	| gzip > $@

probeset_metadata.gz: $(AFFY)/HuEx-1_0-st-v2.r2.dt1.hg18.csv
	tr "," "\t" < $< \
	| tr -d '"' \
	| grep -v '^#' \
	| gzip >$@

.META: probeset_metadata.gz
	1	probeset_id
	2	psr_id
	3	exon_cluster_id
	4	transcript_cluster_id
	5	seqname
	6	strand
	7	start
	8	stop
	9	probe_count
	10	crosshyb_type
	11	number_independent_probes
	12	level
	13	bounded
	14	NoBoundedEvidence
	15	fl
	16	mrna
	17	est
	18	vegaGene
	19	vegaPseudoGene
	20	ensGene
	21	sgpGene
	22	exoniphy
	23	twinscan
	24	geneid
	25	genscan
	26	genscanSubopt
	27	mouse_fl
	28	mouse_mrna
	29	rat_fl
	30	rat_mrna
	31	microRNAregistry
	32	rnaGene
	33	mitomap
	34	has_cds
	35	is_normgene_exon
	36	is_normgene_intron

.DOC: probeset_metadata.gz
	da PMID:21498550
	## keep only probesets with a value of 1 in the crosshyb_type column
	to kepp only uniquely mapping probeset

.META: metaprobset-probset.map.gz
	1	metaprobset
	2	probeset

CEL/%.gz:
	mkdir -p CEL
	wget -O $@ ftp://ftp.ncbi.nih.gov/pub/geo/DATA/supplementary/samples/`sed s/...$$/nnn/<<<"$*"`/$*/$**

.PHONY: download_cel

download_cel: samples_ids $(addprefix CEL/, $(shell cat samples_ids))
	echo pass

CEL/%: CEL/%.gz
	zcat $< > $@

custom_normalization: $(addprefix CEL/, $(shell cat samples_ids))
	apt-probeset-summarize \
	-p $(AFFY)/HuEx-1_0-st-v2.r2.pgf \
	-c $(AFFY)/HuEx-1_0-st-v2.r2.clf \
	-a rma-sketch \
	-x 15 \
	--meta-probesets $(AFFY)/HuEx-1_0-st-v2.r2.dt1.hg18.full.mps \
	-o $@ \
	$^

present_absent: $(addprefix CEL/, $(shell cat samples_ids))
	apt-probeset-summarize \
	-p $(AFFY)/HuEx-1_0-st-v2.r2.pgf \
	-c $(AFFY)/HuEx-1_0-st-v2.r2.clf \
	-a dabg \
	-b $(AFFY)/HuEx-1_0-st-v2.r2.antigenomic.bgp \
	-o $@ \
	$^

annotations.GPL5175.gz annotations.GPL5188.gz: annotations.%.gz: $(GEO_SERIES)_family.soft.gz
	zcat $(GEO_SERIES)_family.soft.gz | sed 's/^^/>/' | tee /dev/null |get_fasta 'PLATFORM = $*' \ 
	| grep -v '^#' \
	| grep -v '^!' \
	| unhead -n 2 \
	| bawk '{sub(":","\t",$$3); sub("-","\t",$$3); print}' \
	| gzip > $@

.META: annotations.*.gz
	1	ID
	2	GB_LIST
	3	chr
	4	b
	5	e
	6	seqname
	7	RANGE_GB
	8	RANGE_STRAND
	9	RANGE_START
	10	RANGE_STOP
	11	probe_count
	12	transcript_cluster_id
	13	exon_id
	14	psr_id
	15	gene_assignment
	16	mrna_assignment
	17	level
	18	probeset_type

.DOC: annotations.*.gz
	#%genome-version=hg18

annotations.GPL5188.bed.gz: annotations.%.gz
	bawk '$$chr!="control" && $$chr!="Not currently mapped to latest genome" {print $$chr,$$b,$$e,$$ID}' $< \
	| gzip >$@

annotations.GPL5175.bed.gz: annotations.GPL5188.gz $(TASK_ROOT)/local/share/data/huex.probeset2trans.full
	bawk '$$chr!="control" && $$chr!="Not currently mapped to latest genome" {print $$chr,$$b,$$e,$$ID}' $< \
	| translate -k -a -d -j $^2 4 \
	| bawk '{print $$1,$$2,$$3, $$4 ";" $$5}' \
	| gzip > $@

.INTERMEDIATE: liftOver.chain annotations.bed

annotations.%.bed: annotations.%.bed.gz
	zcat $< > $@

liftOver.chain: $(BIOINFO_ROOT)/task/alignments/dataset/ucsc/hsapiens/hg18/hsapiens/hg19/liftOver.chain.gz
	zcat $< > $@

annotations.%.bed.hg19.gz: annotations.%.bed liftOver.chain
	liftOver $< $^2 /dev/stdout $@.mising \
	| bsort -k1,1 -k2,2n -S10% \
	| gzip > $@

annotations.GPL5175.ens.isec.gz annotations.GPL5188.ens.isec.gz: annotations.%.ens.isec.gz: annotations.%.bed.hg19.gz $(ENS_ANNOT_DIR)/exon_transctip_gene_full.gz
	intersection \
	<(\
		zcat $< | sed 's/chr//' | bsort -S20% -k1,1 -k2,2n\
	) \
	<(\
		bawk '{print $$chr,$$ENSE_b,$$ENSE_e,$$ENSE,$$ENSG,$$biotype}' $^2 | bsort -S20% -k1,1 -k2,2n\
	) | gzip > $@

metaProbset-gene.map: annotations.GPL5175.ens.isec.gz
	zcat $< | tr ";" "\t" | cut -f 9,11,12 \
	| sort | uniq >$@

.META: metaProbset-gene.map
	1	metaProbsetId
	2	ensg
	3	biotype

metaProbset-gene.deneger.map: metaProbset-gene.map
	translate -a <(cut -f 1 $< | symbol_count) 1 < $<\
	| translate -a <(cut -f 2 $< | symbol_count) 3 > $@

.META: metaProbset-gene.deneger.map
	1	metaProbsetId
	2	metaprob_count
	3	ensg
	4	ensg_count
	5	biotype

metaProbset-gene.univoc.map: metaProbset-gene.deneger.map
	bawk '$$metaprob_count==1 {print $$metaProbsetId,$$ensg}' $< \
	| sort | uniq > $@

samples_platform.map: $(GEO_SERIES)_family.soft.gz
	zcat $< | soft_series2samples_raw_meta | fasta2tab | grep Sample_platform_id | awk '{print $$1 "\t" $$4}' > $@


gene_level_matrix.gz: $(GEO_SERIES)_family.soft.gz samples_platform.map
	zcat $< | sed 's/^^/>/' \
	| cut -f -2\   		* perdo i dati di present absent se presenti *
	| soft_serie2samples \
	| get_fasta -f <(bawk '$(addsuffix " || ,$(addprefix $$2==",$(GENE_LEVEL_PLATFORMS))) 0 {print $$1}' $^2) \
	| fasta2tab \
	| map2incidence_weight > $@.tmp
	transpose $@.tmp | gzip >$@
	rm $@.tmp

custom_normalization_gene_level_matrix.gz: custom_normalization
	grep -v '^#' $</rma-sketch.summary.txt \
	| gzip > $@

present_absent_gene_level_matrix.gz: present_absent probeset_metadata.gz
	zgrep -v '^#' $</dabg.summary.txt \
	| bawk 'BEGIN{printf("%s",">")} {print}' \
	| filter_1col -f 1 <(bawk '$$crosshyb_type==1 {print $$probeset_id}' $^2)\   *tengo solo i probset che mappano univocamente*
	| gzip > $@

gene_level_matrix.annot.gz custom_normalization_gene_level_matrix.annot.gz: %matrix.annot.gz: %matrix.gz $(ENS_ANNOT_DIR)/genes.gz metaProbset-gene.univoc.map
	(:\
		zcat $< | head -n 1 | sed 's/\t/\t\t\t/';\
		zcat $< | translate -a -k <(translate -a <(bawk '{print $$gene_ID,$$biotype}' $^2) 2 < $^3) 1 ;\
	) | gzip > $@

gene-best_expression.gz: custom_normalization_gene_level_matrix.annot.gz
	zcat $<\
	| cut -f 2,3,4- | sed 's/\t/;/' | perl -lane 'BEGIN{$$,="\t"} $$id=shift @F; $$max=-1; for(@F){$$max=$$_ if $$_>$$max} print $$id,$$max' \
	| bsort -k2,2gr -S90% \
	| tr ";" "\t" \
	| find_best 1 3 \     *probeset degeenri*
	| gzip > $@

.META: gene-best_expression.gz
	1	ENSG
	2	biotype
	3	expression

