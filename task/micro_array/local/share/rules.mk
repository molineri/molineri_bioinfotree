MIN_EXPERIMENT_NUMBER_FOR_MESH ?= 5
MESH_DIR ?= $(BIOINFO_ROOT)/task/annotations/dataset/mesh/mesh2009
MYSQL ?= mysql -BCAN -u coexpr_admin -p$(MYSQL_PASSWORD) -h biother coexpr

ALL += db_mesh_tree
ALL += db_mesh_terms
ALL += db_samples_$(SPECIES)
ALL += $(CORRESPONDENT_SAMPLES)

extern $(CORRESPONDENT_DATASET)/used_mesh as CORRESPONDENT_USED_MESH 
extern $(CORRESPONDENT_DATASET)/valid_mesh.expanded as CORRESPONDENT_VALID_MESH 
extern $(CORRESPONDENT_DATASET)/db_samples_$(CORRESPONDENT_SPECIES) as CORRESPONDENT_SAMPLES

define load_table
        echo "TRUNCATE $(subst db_,,$@)" | $(MYSQL)
        echo "LOAD DATA LOCAL INFILE '$@' INTO TABLE $(subst db_,,$@)" | $(MYSQL)
endef

used_mesh: $(EXPERIMENT_MESH_FILE) $(TASK_ROOT)/local/share/data/manual_mesh_path_conversion_$(SPECIES)
	unhead $< | cut -f 10 \
	| expandsets 1 \
	| sed -f $^2 | expandsets 1\
	| tr -d "_" \
	| sort | uniq > $@

used_mesh.with_correspondent_dataset: used_mesh $(CORRESPONDENT_USED_MESH)
	cat $^ | sort | uniq > $@

valid_mesh: $(EXPERIMENT_MESH_FILE) used_mesh $(TASK_ROOT)/local/share/data/manual_mesh_path_conversion_$(SPECIES)
	cat $^2 \
	| filter_1col 1 <(\
		cut -f 10 $< \
		| unhead \
		| grep -v '#' \
		| sed -f $^3 \
		| expandsets -s ';' 1 \
		| symbol_count \
		| bawk '$$2>=$(MIN_EXPERIMENT_NUMBER_FOR_MESH) {print $$1}'\
	) > $@

valid_mesh.expanded used_mesh.expanded: %_mesh.expanded: $(MESH_DIR)/anatomy.tree %_mesh
	filter_1col 1 <(filter_1col 3 $^2 < $< | cut -f 1) < $< \
	| cut -f 3 | sort | uniq > $@

valid_mesh.expanded.with_correspondent_dataset used_mesh.expanded.with_correspondent_dataset: %_mesh.expanded.with_correspondent_dataset: %_mesh.expanded $(CORRESPONDENT_DATASET)/%_mesh.expanded
	cat $^ | sort | uniq > $@

used_mesh.expanded.inclusive valid_mesh.expanded.inclusive: %.inclusive: %.with_correspondent_dataset
	cat $< \
	|perl -lne 'print; next if length($$_)<=3; while(1){$$_ =~ s/(.*)\.\d+$$/$$1/; print $$1; last if length($$1)<=3}' > $@

db_mesh_terms: used_mesh.expanded.inclusive $(MESH_DIR)/anatomy.tree
	(echo -e "All MeSH\tNULL\t0.0.0";\
	filter_1col 3 $< < $^2 ) \
	| cut -f 1,3 | collapsesets 2 | sort | uniq \
	| enumerate_rows >$@
	$(call load_table)
	

db_samples_$(SPECIES): $(EXPERIMENT_MESH_FILE) db_mesh_terms $(TASK_ROOT)/local/share/data/manual_mesh_path_conversion_$(SPECIES)
	cut -f 1-7,9,10 $< | unhead \
	| perl -lane '$$,="\t"; $$F[8]=~s/_//g; print @F' \
	| sed -f $^3 | expandsets 9 \
	| translate -a -c -d -v -e NULL -n <(\                  * waring: rimuovere il -k appena corretti problemi con nomi obsoleti *
		cut -f 1,3 < $^2 | expandsets 2 \
	) 9 \
	| cut -f 1-8,10 \
	| sort |  uniq \					* toglie righe duplicate causate da diversi path in *Ferdi* che puntano allo stesso MeSH*
	| translate <(echo -e "1\tNORMAL\n2\tTUMOR\n3\tOTHER_DISEASE") 5\
	| translate <(echo -e "C\tCELL_LINE\nT\tTISSUE") 6\
	| translate <(echo -e "E\tEMBRIO\nA\tADULT") 7 > $@
	$(call load_table)

used_mesh.expanded.inclusive.with_name: $(MESH_DIR)/anatomy.tree used_mesh.expanded.inclusive
	filter_1col 3 $^2 < $< > $@

used_mesh.ancestor: used_mesh.expanded.inclusive.with_name
	cut -f 1,3 $< | mesh_tree2ancestor >$@

db_mesh_tree: used_mesh.ancestor db_mesh_terms
	translate -n <(cut -f 1,2 $^2) 1 2 < $< \
	| cut -f 1,2,5 | sort | uniq > $@
	$(call load_table)

table_extraction_manual:
	extractor $(CONDITION) $(TISSUES) $(STAGE) $(MESH_NUMBER) $(MESH_TERMS) $(GSM) $(FILE_EFFECTIVE) $(FILE_TEMP) $(FILE_TABLE_EXPR)

%.finale:
	$*

%.$(SPECIES)_samples: $(FILE_TABLE_EXPR)
	echo "SELECT * FROM custom_net_samples_$(SPECIES) WHERE id ='$*'" | $(MYSQL) | cut -f 2 > $(SPECIES)_tmp
	extractor_web $(SPECIES)_tmp $^ > $@
	rm $(SPECIES)_tmp
#ln -s $@ uncleaned_matrix

#table_extraction_web: FILE_WITH_SAMPLES_TO_EXTRACT $(FILE_TABLE_EXPR) NAME_TO_GIVE_TO_MATRIX
#	extractor_web $^ $^2
