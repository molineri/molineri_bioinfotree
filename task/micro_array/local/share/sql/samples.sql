CREATE TABLE `samples_hsapiens` (
  `GEO_SERIES_ACC` text NOT NULL,
  `GEO_SERIES_TITLE` text NOT NULL,
  `GEO_SAMPLE_ACC` text NOT NULL,
  `GEO_SAMPLE_TITLE` text NOT NULL,
 `condition` set('NORMAL','TUMOR','OTHER_DISEASE') NOT NULL,
  `sample` set('CELL_LINE','TISSUE') NOT NULL,
  `stage` set('EMBRIO','ADULT') NOT NULL,
  `MeSH_original_text` text NOT NULL,
  `MeSH_internal_id` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1
