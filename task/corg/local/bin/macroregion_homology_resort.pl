#!/usr/bin/perl

use warnings;
use strict;

my $usage="$0 filename.fa ordered_id";

my $filename=shift(@ARGV);
$filename or die($usage);

open FA,$filename or die("Can't open file ($filename)");

my $block=undef;
my $id=undef;
my %fasta=();
while(<FA>){
	if(m/^>/){
		if(defined($block)){
			$fasta{$id}=$block;
		}
		($id)=($_=~/^>([^\s]+)/);
		$block='';
	}else{
		$block.=$_;
	}
}
$fasta{$id}=$block;

while(<>){
	print ">$_";
	chomp;
	die ("There is no sequence for macroregion $_") if !defined($fasta{$_});
	print $fasta{$_};
}
