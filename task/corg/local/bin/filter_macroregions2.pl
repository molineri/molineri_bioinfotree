#!/usr/bin/perl
use warnings;
use strict;
$\="\n";
$,="\t";

my $tomask_file = shift;
open FH,$tomask_file or die("Can't open file ($tomask_file)");

my %tomask=();
while(<FH>){
	chomp;
	my @F=split;
	my $id=shift @F;
	push @{$tomask{$id}},\@F;
}

my $header="";
my $seq="";
while(<>){
	chomp;
	if(m/^>/){
		if($seq){
			&mask($seq,$header);
		}
		$seq="";
		$header=$_;
	}else{
		$seq.=$_;
	}
}
if($seq){
	&mask($seq,$header);
}

sub mask
{
	my $seq=shift;
	my $header=shift;
	$header=~/^>([^\t]+)/;
	my $id = $1;
	
	print $header;

	#my @s = split //, $seq;
	#die $s[0];

	for(@{$tomask{$id}}){
		my ($chr,$b,$e) = @{$_};
		print $b,$e;
		substr($seq, $b, $e-$b, 'E' x ($e-$b));
		#for(my $i=$b; $i<$e; $i++){
		#	$s[$i]='E';
		#}
	}

	#$seq = join "", @s;

	while($seq){
		print substr $seq,0,80,'';
	}
}
