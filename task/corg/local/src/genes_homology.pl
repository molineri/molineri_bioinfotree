#!/usr/bin/perl

use warnings;
use strict;
use constant GENES_COL          => 4;
use constant MACROREG_ID_COL    => 0;
use constant MACROREG_B_COL     => 2;
use constant MACROREG_1         => 0;
use constant ALIGN_B1           => 1;
use constant ALIGN_E1           => 2;
use constant MACROREG_2         => 4;
use constant ALIGN_B2           => 5;
use constant ALIGN_E2           => 6;
use Getopt::Long;

$,="\t";
$\="\n";

my $usage = "$0 -u1 upstreams1_file -u2 upstreams2_file -h homology_file -d align_dir -m1 macroreg1_file -m2 macroreg2_file";
my $upstreams1_file = undef;
my $upstreams2_file = undef;
my $homology_file = undef;
my $align_dir = undef;
my $macroreg_1_file = undef;
my $macroreg_2_file = undef;
my $verbose = 0;


GetOptions (
	'upstreams1|u1=s' => \$upstreams1_file,
	'upstreams2|u2=s' => \$upstreams2_file,
	'homology|h=s'    => \$homology_file,
	'align_dir|d=s'   => \$align_dir,
	'macroreg_1|m1=s' => \$macroreg_1_file,
	'macroreg_2|m2=s' => \$macroreg_2_file,
	'verbose|v'	  => \$verbose
) or die ($usage);



open UPSTR1,$upstreams1_file or die ("Can't open file ($upstreams1_file)\n");
open UPSTR2,$upstreams2_file or die ("Can't open file ($upstreams2_file)\n");
open HOMOL,$homology_file or die ("Can't open file ($homology_file)\n");
open M1,$macroreg_1_file or die ("Can't open file ($macroreg_1_file)\n");
open M2,$macroreg_2_file or die ("Can't open file ($macroreg_2_file)\n");


my %genes1 = ();
while (<UPSTR1>) {
	chomp;
	my ($gene_id,@tmp) = split /\t/;
	$genes1{$gene_id} = \@tmp;
}

print STDERR "letto file: $upstreams1_file" if $verbose;

my %genes2 = ();
while (<UPSTR2>) {
	chomp;
	my ($gene_id,@tmp) = split /\t/;
	$genes2{$gene_id} = \@tmp;
}

print STDERR "letto file: $upstreams2_file" if $verbose;




my %homology = ();
while (<HOMOL>) {
	chomp;
	my ($gene_sp1,$gene_sp2) = split /\t/;
	if (!defined $homology{$gene_sp1}) {
		my @aux = ($gene_sp2);
		$homology{$gene_sp1} = \@aux;
	} else {
		push @{$homology{$gene_sp1}},$gene_sp2;
	}
}

print STDERR "letto file: $homology_file" if $verbose;

my %upstream=();
my %macroreg1_b=();
while (<M1>) {
	chomp;
	my @F = split /\t/;
	my @genes_in_region = split /\s*;\s*/,$F[GENES_COL];
	foreach my $el (@genes_in_region) {
		my @tmp=($el,@{$genes1{$el}});
		my @tmp2=();
		foreach my $homol (@{$homology{$el}}) {
			next if !defined $genes2{$homol};
			my @tmp3=($homol,@{$genes2{$homol}});
			push @tmp2,\@tmp3;
		}
		push @tmp,\@tmp2;
		push @{$upstream{$F[MACROREG_ID_COL]}},\@tmp if(scalar(@tmp2));
	}
	$macroreg1_b{$F[MACROREG_ID_COL]} = $F[MACROREG_B_COL];
}

print STDERR "letto file: $macroreg_1_file" if $verbose;


my %macroreg2_b=();
while (<M2>) {
	chomp;
	my @F = split /\t/;
	$macroreg2_b{$F[MACROREG_ID_COL]} = $F[MACROREG_B_COL];
}

print STDERR "letto file: $macroreg_2_file" if $verbose;

#/home_PG/molineri/bioinfo/task/alignments/inhouse/dataset/hsapiens_mmusculus_ensembl42_corg3_wublast_high
my @files_list = <$align_dir/*.wublast.gz>;

print STDERR "inizio lettura allineamenti" if $verbose;

foreach my $file (@files_list) {
	open PIPE, "zcat $file |" or die ("ERROR: Can't open pipe");
	while (<PIPE>) {
		chomp;
		my @F = split /\t/;
		my $macroreg1 = $F[MACROREG_1];
		my $macroreg2 = $F[MACROREG_2];
		die ("ERROR: macroregion $macroreg1 not found in STDIN file\n") if (!defined $upstream{$macroreg1});

		my $align_b1 = $F[ALIGN_B1] + $macroreg1_b{$macroreg1};
		my $align_e1 = $F[ALIGN_E1] + $macroreg1_b{$macroreg1};
		my $align_b2 = $F[ALIGN_B2] + $macroreg2_b{$macroreg2};
		my $align_e2 = $F[ALIGN_E2] + $macroreg2_b{$macroreg2};

#		print $macroreg1,$align_b1,$align_e1;
#		print $macroreg2,$align_b2,$align_e2;

		foreach my $gene1_ref (@{$upstream{$F[MACROREG_ID_COL]}}) {
			my $gene1 = ${$gene1_ref}[0];
			my $chr1 = ${$gene1_ref}[1];
			my $b1 = ${$gene1_ref}[2];
			my $e1 = ${$gene1_ref}[3];
			my $homol_ref = ${$gene1_ref}[4];

#			print $gene1,$chr1,$b1,$e1;
#
			next if ($e1 <= $align_b1 or $b1 >= $align_e1);

#			print "inizio casini...";
			if (!defined $homol_ref) {
				print "non ci sono omologhi per $gene1";
				next;
			}
			my @homol_list_ref = @{$homol_ref};
			foreach my $homol (@homol_list_ref) {
				my $gene2 = ${$homol}[0];
				my $chr2 = ${$homol}[1];
				my $b2 = ${$homol}[2];
				my $e2 = ${$homol}[3];

				next if ($e2 <= $align_b2 or $b2 >= $align_e2);

				print $gene1,$chr1,$align_b1,$align_e1,$gene2,$chr2,$align_b2,$align_e2;
			}
		}
	}
	close PIPE;
}

# output: 
# per ogni regione allineata (controllare che l'allineamento vada dal gene al suo omologo)
# gene1 chr b e gene2 chr b e dell'allineamento
#
#
# in /home_PG/molineri/bioinfo/task/corg/dataset/corg3
# cat sp1.macroregions | ../../local/bin/genes_homology -u1 ../../dati_da_loredana/070123/coord_human_upstreams_ens42_corg3.txt -u2 ../../dati_da_loredana/070123/coord_mouse_upstreams_ens42_corg3.txt -h ../../dati_da_loredana/070123/homology_ens42.txt -d /home_PG/molineri/bioinfo/task/alignments/inhouse/dataset/hsapiens_mmusculus_ensembl42_corg3_wublast_high
