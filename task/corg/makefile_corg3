BIN_DIR = ../../local/bin
ENS_VERSION=42
SEQ_DIR1=$(BIOINFO_ROOT)/task/sequences/dataset/ensembl/hsapiens/$(ENS_VERSION)
SEQ_DIR2=$(BIOINFO_ROOT)/task/sequences/dataset/ensembl/mmusculus/$(ENS_VERSION)
ANNOTE_DIR1=$(BIOINFO_ROOT)/task/annotations/dataset/ensembl/hsapiens/$(ENS_VERSION)
ANNOTE_DIR2=$(BIOINFO_ROOT)/task/annotations/dataset/ensembl/mmusculus/$(ENS_VERSION)
BLOCK_NUM=50
COORD_SP1=$(BIOINFO_ROOT)/task/corg/dati_da_loredana/070123/coord_human_upstreams_ens42_corg3.txt
COORD_SP2=$(BIOINFO_ROOT)/task/corg/dati_da_loredana/070123/coord_mouse_upstreams_ens42_corg3.txt
HOMOLOGY_FILE=$(BIOINFO_ROOT)/task/corg/dati_da_loredana/070123/homology_ens42.txt
ALIGN_DIR=$(BIOINFO_ROOT)/task/alignments/inhouse/dataset/hsapiens_mmusculus_ensembl42_corg3_wublast_high_070524

.DELETE_ON_ERROR:

all: split

sp1.macroregions: $(COORD_SP1)
	awk 'OFS="\t" {print $$2,$$3,$$4,$$1}' $< \
	| sort -k 1,1 -k 2,2n \
	| union \
	| enumerate_rows > $@ 

sp2.macroregions: $(COORD_SP2)
	awk 'OFS="\t" {print $$2,$$3,$$4,$$1}' $< \
	| sort -k 1,1 -k 2,2n \
	| union \
	| enumerate_rows > $@ 

#macroregions_hsapiens.fa
sp1.macroregions.fa: sp1.macroregions
	cut -f -4 $< | seqlist2fasta -r $(SEQ_DIR1) >$@

sp2.macroregions.fa: sp2.macroregions
	cut -f -4 $< | seqlist2fasta -r $(SEQ_DIR2) >$@

macroregions_homology: $(HOMOLOGY_FILE) sp1.macroregions sp2.macroregions
	$(BIN_DIR)/macroregion_homology.pl  $^ 2>$@.missing_gene | sort | uniq > $@

sp1.macroregions.tomask: sp1.macroregions $(ANNOTE_DIR1)/coords_coding
	intersection $<:2:3:4 $(word 2,$^) | repeat_group_pipe 'awk '\''BEGIN{OFS="\t"}{print $$$$1,$$$$2-$$2,$$$$3-$$2}'\''\
	| union | append_each_row -b $$1' 8 4  | tr ";" "\t" > $@

sp2.macroregions.tomask: sp2.macroregions $(ANNOTE_DIR2)/coords_coding
	intersection $<:2:3:4 $(word 2,$^) | repeat_group_pipe 'awk '\''BEGIN{OFS="\t"}{print $$$$1,$$$$2-$$2,$$$$3-$$2}'\''\
	| union | append_each_row -b $$1' 8 4  | tr ";" "\t" > $@

sp1.macroregions.masked.fa: sp1.macroregions.tomask sp1.macroregions.fa
	$(BIN_DIR)/filter_macroregions2.pl $^ > $@

sp2.macroregions.masked.fa: sp2.macroregions.tomask sp2.macroregions.fa
	$(BIN_DIR)/filter_macroregions2.pl $^ > $@


sp%.macroregions.masked.sort.fa: sp%.macroregions.masked.fa macroregions_homology
	cut -f $* $(word 2,$^) | $(BIN_DIR)/macroregion_homology_resort.pl $< | tr "E" "N" > $@
	

split: macroregions_homology sp1.macroregions.masked.fa sp2.macroregions.masked.fa
	sort -n $< | uniq | tr "\t" "_" | xargs -n $(BLOCK_NUM)  echo | \
		(i=1; while read line; do \
			tr " " "\n" <<<"$$line" | sed 's/_.*//' | sort -n | uniq > $@.tmp1.$$i; \
			get_fasta -i -f $@.tmp1.$$i $(word 2,$^) \
			| tr 'E' 'N' >"sp1-$$i.fa"; \
			i=$$((i+1));\
		done;)
	sort -n $< | uniq | tr "\t" "_" | xargs -n $(BLOCK_NUM)  echo | \
		(i=1; while read line; do \
			tr " " "\n" <<<"$$line" | sed 's/.*_//' | sort -n | uniq > $@.tmp2.$$i; \
			get_fasta -i -f $@.tmp2.$$i $(word 3,$^) \
			| tr 'E' 'N' >"sp2-$$i.fa"; \
			i=$$((i+1));\
		done;)
	touch $@;

alignments: $(COORD_SP1) $(COORD_SP2) $(HOMOLOGY_FILE) $(ALIGN_DIR) sp1.macroregions sp2.macroregions
	$(BIN_DIR)/genes_homology \
		-u1 $(word 1,$^) \
		-u2 $(word 2,$^) \
		-h  $(word 3,$^) \
		-d  $(word 4,$^) \
	 	-m1 $(word 5,$^) \
	 	-m2 $(word 6,$^) \
	| sort -k 2,2 -k 3,3n > $@

reduced.sp1: alignments
	awk 'BEGIN{OFS="\t"}{print $$1,$$3,$$4,$$2}' $< \
	| sort -k 1,1 -k 2,2n \
	| union \
	| cut -d';' -f 1 \
	| awk 'BEGIN{OFS="\t"}{print $$1,$$4,$$2,$$3}' > $@

reduced.sp2: alignments
	awk 'BEGIN{OFS="\t"}{print $$5,$$7,$$8,$$6}' $< \
	| sort -k 1,1 -k 2,2n \
	| union \
	| cut -d';' -f 1 \
	| awk 'BEGIN{OFS="\t"}{print $$1,$$4,$$2,$$3}' > $@

reduced.%.nr: reduced.%
	awk 'BEGIN{OFS="\t"}{print $$2,$$3,$$4,$$1}' $< | sort -k 1,1 -k 2,2n \
	| union \
	| awk 'BEGIN{OFS="\t"}{print $$4,$$1,$$2,$$3}' > $@

reduced.sp1%.fa: reduced.sp1%
	seqlist2fasta -r $(SEQ_DIR1) < $< > $@

reduced.sp2%.fa: reduced.sp2%
	seqlist2fasta $(SEQ_DIR2) < $< > $@


tests:
	[ $$(grep -r -B 1 '^N\+$$' reduced.sp1.fa | wc -l) -ne 0 ] && echo "ERROR: ^N+$$ block in reduced.sp1.fa";
	[ $$(grep -r -B 1 '^N\+$$' reduced.sp2.fa | wc -l) -ne 0 ] && echo "ERROR: ^N+$$ block in reduced.sp2.fa";
###################
