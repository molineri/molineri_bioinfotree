#!/usr/bin/env python
from optparse import OptionParser
from sys import exit, stdin

def iter_coverage(fd):
	for lineno, line in enumerate(fd):
		try:
			items = line.split('\t')
			assert len(items) == 2, 'wrong item count at line %d' % (lineno+1)
			yield int(items[0]), int(items[1])
		except ValueError:
			raise ValueError, 'malformed items at line %d' % (lineno+1)

if __name__ == '__main__':
	parser = OptionParser(usage="%prog TRIGGER_LEVEL <COVERAGE")
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
		
	try:
		triggerLevel = int(args[0])
	except ValueError:
		exit('Invalid trigger level.')
	
	start = -1
	for x, y in iter_coverage(stdin):
		if start == -1:
			if y >= triggerLevel:
				start = x
		else:
			if y < triggerLevel:
				print '%d\t%d' % (start, x)
				start = -1

	if start != -1:
		exit('Unclosed block at the end of input.')
