#!/usr/bin/perl

use strict;
use warnings;

$\="\n";
$,="\t";

my $usage = "$0 < qualcosa.cliques";


my $conn_comp_num = undef;
my $clique_num = undef;
while (<>) {
	chomp;
	
	if (m/^>\d+$/) {
		$conn_comp_num = $&;
		$clique_num = 0;
	} elsif (m/^\s*size\s*=\s*\d+\s*,\s+weight\s*=\s*\d+\s*:\s+/) {
		my $id_list = $';
		my @F = split /\s+/,$id_list;
		print $conn_comp_num.'_'.$clique_num, @F;
		$clique_num++;
	} else {
		die ("ERROR in input format");
	}
}
