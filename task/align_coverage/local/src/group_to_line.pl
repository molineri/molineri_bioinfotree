#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;

$,="\t";
$\="\n";


my $usage = "$0 -s 'stringa inizio gruppo' < file_name";

my $string = undef;
GetOptions (
	'string|s=s' => \$string
) or die ($usage);
die $usage if !defined $string;


my @group = ();
while (my $line = <>) {
	if ($line =~ /^$string/) {
		print @group if (scalar @group);
		@group = ();
		next;
	}

	chomp $line;
	push @group,$line;
}

print @group if (scalar @group);
