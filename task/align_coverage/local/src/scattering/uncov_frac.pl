#!/usr/bin/perl

use warnings;
use strict;

$\="\n";
$,="\t";


my $usage = "$0 < lc_list";

my $sum;
my $peak_id;
my $old_id = undef;
my $lp;

while (<>) {
	chomp;
	
	($peak_id) = ($_ =~ /^(\d+:\w+,\d+,\d+)\t\d+$/);
	die if !defined $peak_id;

	if (!defined $old_id) {
		$sum = 0;
	} elsif ($peak_id ne $old_id) {
		$sum /= $lp;
		print $old_id, $sum;
		$sum = 0;
	}

	my ($peak_b, $peak_e, $lc) = ($_ =~ /^\d+:\w+,(\d+),(\d+)\t(\d+)$/);
	$lp = $peak_e - $peak_b;
	die if (!defined $lp or ($lp <= 0));
	$sum += ($lp - $lc);

	$old_id = $peak_id;

}

if (defined $peak_id) {
	$sum /= $lp;
	print $peak_id, $sum;
}
