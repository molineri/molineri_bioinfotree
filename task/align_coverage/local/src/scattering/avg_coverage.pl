#!/usr/bin/perl

use warnings;
use strict;

$\="\n";
$,="\t";

my $usage = "$0 1.coverage_correct < id b e";

my $cov_file = shift @ARGV;
die $usage if !defined $cov_file;

open COVERAGE,$cov_file or die "Can't open file ($cov_file)"; 

while (<>) {
	my ($id,$b,$e) = split /\t/;
	my $last_ref = &search_coverage ($b);
	my $avg = &avg_coverage ($b,$e,$last_ref);
	print $id,$avg;
}


sub search_coverage
{
	my $reg_b = shift;
	my @F;

	do {
		my $line = <COVERAGE>;
		die if !defined $line;
		chomp $line;
		@F = split /\t/,$line;
	} while ($F[0] < $reg_b);

	die if ($F[0] > $reg_b);
	return \@F;
}


sub avg_coverage
{
	my $reg_b = shift;
	my $reg_e = shift;
	my $last_F_ref = shift;
	my @last_F = @{$last_F_ref};
	my $sum = 0;
	my @F;

	do {
		my $line = <COVERAGE>;
		die if !defined $line;
		chomp $line;
		@F = split /\t/,$line;
		$sum += ($F[0] - $last_F[0]) * $last_F[1];
		@last_F = @F;
	} while ($F[0] < $reg_e);

	die if ($F[0] > $reg_e);

	$sum /= $reg_e - $reg_b;
	return $sum;
}

