#!/usr/bin/env python
# encoding: utf-8

from optparse import OptionParser
from sys import exit, stdin
from vfork.io.util import safe_rstrip

def parse_peak(peak_label, lineno):
	tokens = peak_label.split(',')
	try:
		if len(tokens) != 3:
			raise ValueError
		else:
			return peak_label, int(tokens[1]), int(tokens[2])
	except ValueError:
		exit('Invalid peak label at line %d.' % (lineno+1,))

def iter_peaks_and_chr(fd):
	last_peak = None
	last_chr = None
	last_group = None
	
	for lineno, line in enumerate(fd):
		tokens = safe_rstrip(line)
		if len(tokens) != 14:
			exit('Invalid token number at line %d.' % (lineno+1,))
		
		if tokens[0] != last_peak or tokens[6] != last_chr:
			if last_group:
				yield parse_peak(last_peak, lineno), last_group
			
			last_peak = tokens[0]
			last_chr = tokens[6]
			last_group = []
		
		try:
			last_group.append(tuple(int(c) for tokens[c] in (3,4,7,8)))
		except ValueError:
			exit('Invalid HSP coordinates at line %d.' % (lineno+1,))
	
	if last_group:
		yield parse_peak(last_peak, lineno), last_group

def main():
	parser = OptionParser(usage='%prog <HSP_BY_PEAK')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	it = iter_peaks_and_chr(stdin)
	print it.next()

if __name__ == "__main__":
	main()
