#!/usr/bin/perl

use warnings;
use strict;

$,="\t";
$\="\n";

my $usage = "$0 regions.1_50.coverage_correct_triggered.conn_comp < regions.1_50.coverage_correct_triggered.all-fc_2nd_run.groups";

my $conn_comp_file = shift @ARGV;
die $usage if !$conn_comp_file;


open CONN_COMP,$conn_comp_file or die "Can't open file $conn_comp_file!";
my @conn_comp = ();
my %conn_comp_card = ();
my $i = 0;
while (<CONN_COMP>) {
	chomp;
	my @F = split /\t/,$_;
	foreach my $node (@F) {
		$node =~ s/:.*$//g;
	}
	push @conn_comp,\@F;
	$conn_comp_card{$i} = scalar @F;
	$i++;
}

my $conn_comp_n = scalar @conn_comp;


my %count = ();
my $intest = undef;
while (my $line = <>) {
	chomp $line;
	if ($line =~ /GROUP/) {
		if (keys %count) {
			my @out = ();
			foreach (keys %count) {
				push @out, "$_,$count{$_}/$conn_comp_card{$_}" if ($count{$_}>0); 
			}
			print $intest,@out;
		}

		$intest = $line;
		for ($i=0; $i<$conn_comp_n; $i++) {
			$count{$i}=0;
		}
		next;
	}

	my $found = 0;

	for (my $idx=0; $idx<$conn_comp_n; $idx++) {
		foreach my $el (@{$conn_comp[$idx]}) {
			if ($line == $el) {
				$count{$idx}++;
				$found=1;
				last;
			}
		}
		last if $found;
	}
}

if (keys %count) {
	my @out = ();
	foreach (keys %count) {
		push @out, "$_,$count{$_}" if ($count{$_}>0); 
	}
	print $intest,@out;
}
