#!/usr/bin/perl

#use warnings;
use strict;

$\ = "\n";

my $usage = "$0 < file_patterns 1_1_50.coverage_correct_triggered";

my $pattern = shift @ARGV;
die $usage if !defined $pattern;
$pattern =~ s/\s+$//;

while (1) {
	(my $line = <>);
	last if !$line;
	next if ($line !~ /./);
	chomp $line;
	$line =~ s/\s+$//;
	
	next if ($line !~ /^>/); 
	$line = $';

	if ($pattern =~ /^$line$/) {
		while ($line = <>) {
			chomp $line;
			last if ($line =~ /^>/);
			print $line;
		}
		exit;
	}
}

print "Pattern ($pattern) not found";
