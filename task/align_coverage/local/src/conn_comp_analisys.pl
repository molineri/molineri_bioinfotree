#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Long;

$,="\t";
$\="\n";


my $usage = " $0 qualcosa.conn_comp score_connectivity [-id]
	-id if ids in file qualcosa.conn_comp don't contain chr,b,e";

my $id_only = undef;
GetOptions (
	'id_only|id' => \$id_only
) or die ($usage);
my $conn_comp_file = shift @ARGV;
die $usage if !$conn_comp_file;

open FH,$conn_comp_file or die("can't open file ($conn_comp_file)");

my $conn_comp_id=0;
my %conn_comp=();
my @all_conn_comp_id=();
while(<FH>){
	chomp;
	my @F=split;
	for(@F){
		$conn_comp{$_}=$conn_comp_id;
	}
	push @all_conn_comp_id, $conn_comp_id;
	$conn_comp_id++;
}

my %conn=();
my @score=();
while(<>){
	chomp;
	my @F=split;
	if ($id_only) {
		$F[0] =~ s/:.*//g;
		$F[2] =~ s/:.*//g;
	}
	die ("ERROR: edge between nodes from different connected components!!") if ($conn_comp{$F[0]} != $conn_comp{$F[2]});
	$conn_comp_id = $conn_comp{$F[0]};

	$conn{$conn_comp_id}{$F[0]}=$F[1];
	$conn{$conn_comp_id}{$F[2]}=$F[3];
	push @{$score[$conn_comp_id]},$F[6];
}

for(@all_conn_comp_id){
	my $score_mean = &mean(\@{$score[$_]});
	my $score_stdev = &stdev(\@{$score[$_]},$score_mean);
	my $score_ref = &median_min_max(\@{$score[$_]});
	my ($score_median, $score_min, $score_max) = @{$score_ref};

	my @a = values %{$conn{$_}};
	my $conn_mean = &mean(\@a);
	my $conn_stdev = &stdev(\@a,$conn_mean);
	my $conn_ref = &median_min_max(\@a);
	my ($conn_median, $conn_min, $conn_max) = @{$conn_ref};
	
	print $_, scalar(@a), 	$score_mean,$score_median,$score_stdev,$score_min,$score_max,
				$conn_mean, $conn_median, $conn_stdev, $conn_min, $conn_max;
}

sub mean
{
	my $sum=0;
	foreach my $value (@{$_[0]}){
		$sum += $value;
	}
	return $sum/scalar(@{$_[0]});
}

sub median_min_max
{
	my $i = int(scalar(@{$_[0]})/2);
	my @a = sort {$a<=>$b} @{$_[0]};
	my @tmp=($a[$i],$a[0],$a[scalar(@a)- 1]);
	return \@tmp;
}

sub stdev
{
	my $mean = pop @_;
	my $stdev=0;

	foreach my $value (@{$_[0]}){
		$stdev += ($value - $mean)**2;
	}

	return sqrt($stdev / scalar(@{$_[0]}));
}


