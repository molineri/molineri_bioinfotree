#!/usr/bin/perl 

use strict;
use warnings;
use Getopt::Long;


$,="\t";
$\="\n";
$| = 1;

my $usage = "cat 1_50.coverage_triggered | $0 -b \$(BIN_DIR) -f \$(BIOINFO_ROOT)/task/alignments/inhouse/dataset/homo_homo_ensembl39_wublast30/hsapiens_chr9_hsapiens_chr18.wublast.pdb.gz -s sx -chr2 22_50.coverage_triggered";
my $gap_coods_file = undef;
my $side = undef;
my $BIN_DIR = undef;

GetOptions (
        'bin_dir|b=s'		=> \$BIN_DIR,
        'gene_coods_file|f=s'	=> \$gap_coods_file,
        'side|s=s' 		=> \$side,
	'file2|chr2=s'		=> \$cov2_triggered
) or die ($usage);

die ($usage) if !$BIN_DIR;
die ($usage) if !$gap_coods_file;
die ($usage) if !$side;
die ($usage) if !$cov2_triggered;

die ("ERROR: $0: file ($cov2_triggered) is void") if (!-s $cov2_triggered);



open GAP_COORDS,$gap_coods_file or die ("ERROR: $0: Cant' open file ($gap_coods_file)\n");


my @gap_coords = ();

if ($side eq 'sx') {
	while (<GAP_COORDS>) {
	# 10   50001   50175   -   12   122345251   122345423   181   119   56   32,1;40,1        17,1;26,1
		chomp;
		my @tmp = split;
		push @gap_coords,\@tmp;
	}
} else {
	while (<GAP_COORDS>) {
		chomp;
		my @tmp = split;
		my @aux = ();
		push @aux, @tmp[4..6], $tmp[3], @tmp[0..2], @tmp[7..9];
		push @aux, $tmp[11] if defined $tmp[11];
		push @aux, $tmp[10] if defined $tmp[10];
		push @gap_coords,\@aux;
	}
}	

while (<>) {
	my ($r_chr,$r_start,$r_stop) = split;
	my @out = ($r_chr,$r_start,$r_stop,$int_start,$int_stop,$strand);

	foreach my $t (@gap_coords) {
		
		my @tmp = @{$t};
		my $align_start = $tmp[1];
		my $align_stop = $tmp [2];
		my $strand = $tmp[3];

		next if ($r_stop <= $align_start);
		next if ($align_stop <= $r_start);
		# if ( ($r_stop <= $align_start) or ($r_start >= $align_stop) ) non c'e' intersezione tra la regione e l'allineamento
	
		# coordinate assolute dell'intersezione tra regione e allineamento sul cromosoma sx
		my $int_start = $align_start < $r_start ? $r_start : $align_start; 
		my $int_stop = $align_stop < $r_stop ? $align_stop : $r_stop; 

		# coordinate di allineamento sul cromosoma sx
		my $int_align_coords_left_start;
		my $int_align_coords_left_stop;
		my $int_align_length;
		my $align_length = $align_stop - $align_start;
		

		if (defined $tmp[10]) {
			my @gaps_start  = ($tmp[10] =~ /(\d+),\d+;*/g);
			my @gaps_length = ($tmp[10] =~ /\d+,(\d+);*/g);
			$int_align_coords_left_start = &genomic2align (\@gaps_start,\@gaps_length, $align_start, $int_start); 
			$int_align_coords_left_stop  = &genomic2align (\@gaps_start,\@gaps_length, $align_start, $int_stop);
			$align_length =  &genomic2align (\@gaps_start,\@gaps_length, $align_start, $align_stop) - $align_start; 
		} else {
			$int_align_coords_left_start = $int_start; 
			$int_align_coords_left_stop  = $int_stop;
		}
		$int_align_length = $int_align_coords_left_stop - $int_align_coords_left_start;

		# coordinate di allineamento sul cromosoma dx
		my $int_align_coords_right_start;
		my $int_align_coords_right_stop;
		if ($strand eq '+') {
			my $dist = $int_align_coords_left_start - $align_start;
			$int_align_coords_right_start = $tmp[5] + $dist;
			$int_align_coords_right_stop  = $int_align_coords_right_start + $int_align_length;
		} else {
			$int_align_coords_right_stop = $tmp[5] + $align_length - ($int_align_coords_left_start - $align_start);
			$int_align_coords_right_start  = $tmp[5] + $align_length - ($int_align_coords_left_stop  - $align_start);
		}	

		# coordinate assolute sul cromosoma dx
		my $abs_coord_right_start;
		my $abs_coord_right_stop;

		if (defined $tmp[11]) {
			my @gaps_start  = ($tmp[11] =~ /(\d+),\d+;*/g);
			my @gaps_length = ($tmp[11] =~ /\d+,(\d+);*/g);
			my $ref = &align2genomic (
				\@gaps_start,
				\@gaps_length,
				$tmp[5],
				$int_align_coords_right_start,
				$int_align_coords_right_stop
			);
			$abs_coord_right_start = ${$ref}[0];
			$abs_coord_right_stop  = ${$ref}[1];
		} else {
			$abs_coord_right_start = $int_align_coords_right_start;
			$abs_coord_right_stop  = $int_align_coords_right_stop;
		}	
	
#	push @out, ($r_chr,$r_start,$r_stop,$int_start,$int_stop,$strand);
		push @out,$abs_coord_right_start,$abs_coord_right_stop;
		print @out;
	}
	
}




sub genomic2align
{
	my $gaps_start_ref = shift @_;
	my $gaps_length_ref = shift @_;
	my $align_start = shift @_;
	my $x = shift @_;

	my @gaps_start = @{$gaps_start_ref};
	my @gaps_length = @{$gaps_length_ref};
	my $l = scalar @gaps_start;

	for (my $i=0; $i<$l; $i++) {
		if ($x > $align_start + $gaps_start[$i]) {
			$x += $gaps_length[$i];
		}
	}
	
	return $x;
}





sub align2genomic
{
	my $gaps_start_ref = shift @_;
	my $gaps_length_ref = shift @_;
	my $align_start = shift @_;
	my $a = shift @_;
	my $b = shift @_;

	die("ERROR: $0: align2genomic: b must be > a") if $b <= $a;

	my @gaps_start = @{$gaps_start_ref};
	my @gaps_length = @{$gaps_length_ref};
	my $l = scalar @gaps_start;

	my $c=0;

	my $i=0;
	my $residue=0;
	while ( ($i < $l) and ($gaps_start[$i] + $align_start < $a) ) {
		if($gaps_start[$i] + $gaps_length[$i] >= $a){
			$c += $a - $gaps_start[$i];
			$residue = $gaps_start[$i] + $gaps_length[$i] - $a;
		}else{
			$c += $gaps_length[$i];
		}
		$i++;
	}

	$a -= $c;
	
	$c += $residue;
	while ( ($i < $l)  and ($gaps_start[$i] + $align_start < $b) ) {
		if($gaps_start[$i] + $gaps_length[$i] >= $b){
			$c += $b - $gaps_start[$i];
		}else{
			$c += $gaps_length[$i];
		}
		$i++;
	}

	my @out = ($a, $b-$c);

	return \@out;
	
}
