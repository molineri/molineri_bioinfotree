#!/usr/bin/perl 

use strict;
use warnings;
use constant LEFT_B_COL  => 0;
use constant LEFT_E_COL  => 1;
use constant RIGHT_B_COL => 2;
use constant RIGHT_E_COL => 3;
use Getopt::Long;


$,="\t";
$\="\n";
$| = 1;

my $usage = "cat b1_e1_b2_e2 | $0 -d 22000";

my $dist = undef;

GetOptions (
	'distance|d=i' => \$dist,
) or die ($usage);



my @list1 = ();
my $something_done = 0; 
my $old_b = undef;
my $block_end; 
my @last= ();

my $j = 0;

while (<>) {
	chomp;
	my @F = split /\t/;

	die "ERROR: file not sorted on the left_start\n" if ( (defined $old_b) and ($F[LEFT_B_COL] < $old_b) );

	$old_b = $F[LEFT_B_COL];
	my $l1 = scalar @list1;

	$block_end = 0; 
	if ($l1 == 0) {
		push @list1,\@F if ($l1 == 0);
	} else {
		$block_end = &controlla_lista($l1,\@F); 
	}

	@last = @F;
}

$j++;

my @list2 = ();

do {
	$something_done = 0;
	@list2 = @list1; 
	@list1 = ();
	$old_b = undef;

	foreach my $el (@list2) {

		my @F = @{$el};

		die "ERROR: file not sorted on the left_start\n" if ( (defined $old_b) and ($F[LEFT_B_COL] < $old_b) );

		$old_b = $F[LEFT_B_COL];
		my $l1 = scalar @list1;

		$block_end = 0; 
		if ($l1 == 0) {
			push @list1,\@F if ($l1 == 0);
		} else {
			$block_end = &controlla_lista($l1,\@F);
		}

		@last = @F;
	}
	$j++;
} while $something_done;


&print_out();

print "numero di cicli: $j";

exit;



sub controlla_lista
{
	my $l = shift;
	my $line_ref = shift;

	my @line = @{$line_ref};

	for (my $i=0; $i<$l; $i++) {
		my ($b1,$e1,$b2,$e2) = @{$list1[$i]};

		next if ($line[LEFT_B_COL] - $e1 > $dist);

		my $x_da_fondere = 1;
		$x_da_fondere = 0 if ( ($line[LEFT_E_COL] < $b1 - $dist) or ($line[LEFT_B_COL] > $e1 + $dist) );

		my $y_da_fondere = 1;
		$y_da_fondere = 0 if ( ($line[RIGHT_E_COL] < $b2 - $dist) or ($line[RIGHT_B_COL] > $e2 + $dist) );

		if ($x_da_fondere and $y_da_fondere) {
			my $x_ref = &fondi($line[LEFT_B_COL], $line[LEFT_E_COL], $b1, $e1);
			my $y_ref = &fondi($line[RIGHT_B_COL],$line[RIGHT_E_COL],$b2, $e2);
			my $new_x_b = ${$x_ref}[0];
			my $new_x_e = ${$x_ref}[1];
			my $new_y_b = ${$y_ref}[0];
			my $new_y_e = ${$y_ref}[1];
			my @new_list_line = ($new_x_b, $new_x_e, $new_y_b, $new_y_e);
			$list1[$i] = \@new_list_line;
			$something_done = 1;
			return 0;
		} else {
			push @list1,\@line;
			return 1;
		}
	}

	push @list1,\@line;
	return 1;
}


sub fondi
{
	my $lb = shift;
	my $le = shift;
	my $rb = shift;
	my $re = shift;

	my $b = ($lb < $rb) ? $lb : $rb; 
	my $e = ($le > $re) ? $le : $re;
	my @out = ($b,$e);

	return \@out;
}


sub print_out
{
	foreach my $el (@list1) {
		my @aux = @{$el};
		print @aux;
	}
}
