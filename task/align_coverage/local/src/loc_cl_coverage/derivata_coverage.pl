#!/usr/bin/perl 

use strict;
use warnings;
use Getopt::Long;

$,="\t";
$\="\n";


my $epsilon;		# raggio dell'intorno di zero
my $l;			# lunghezza della finestra in cui considero le derivate


my $usage = "cat coverage_file | derivata_coverage.pl -l windows_size\n";

GetOptions (
	'windows_size|l=i'      =>  \$l
) or die ($usage);

if ($l <= 0) {
	die "$usage\nError: windows_size must be > 0!!\n";
}

$epsilon = 1/$l + 1/(10*$l); 
# al massimo nella finestra la somma delle derivate e' 1

	
my $x =-1;
my $y =0;
my $x_change=undef;
my $y_change=undef;

my @buffer = ();
my @y_buffer = ();
my $length;
my $y_average = 0;

my $first_line = <>; # tolgo la prima linea perche' e' sempre 0\t0
&get_next_deriv(); # faccio girare una volta per inizializzare i valori

&initialize_buffer($l);

my $b_length = scalar @buffer;
my @interval = undef;
my $estr_sx = undef;
my $estr_dx = undef;

my $old_estr_dx = undef;
my $old_estr_sx = undef;
my $old_y_average = undef;

while (&sliding_window) {

	my $average = 0;
	foreach my $derivata (@buffer) {
		$average += $derivata;
	}
	$average /= $l;


	if ( abs($average) < $epsilon ) {
		if (!defined $estr_sx) {
			$estr_sx = $x - $l;	# estremo sinistro compreso
			$estr_dx = $x + 1;	# estremo destro escluso

			$length = scalar @y_buffer;
			$y_average = 0;
			my @temp = ();
			for (my $i = $length - ($l + 1); $i < $length; $i++) {
				push @temp,$y_buffer[$i];
				$y_average += $y_buffer[$i];
			}
			@y_buffer = @temp;

		} else {
			$estr_dx = $x + 1;
		}
	} else {
		
		if (defined $estr_sx) {

			$y_average -= $y_buffer[$#y_buffer];	# ho una y in piu' oltre a quelle dell'intervallo con derivata quasi zero
			$length = scalar @y_buffer - 1;
#			print "aa",$length,$y_average;
			$y_average /= $length;
			if (defined $old_estr_sx) {
				if ($old_estr_dx > $estr_sx) {
					$old_estr_dx -= int ( ($old_estr_dx - $estr_sx) / 2);
					$estr_sx = $old_estr_dx;
				}
#				print $old_estr_sx,$old_estr_dx;
				print $old_estr_sx,$old_y_average;
				print $old_estr_dx,'0';
			}
			
			$old_estr_sx = $estr_sx;
			$old_estr_dx = $estr_dx;
			$old_y_average = $y_average;

		}

		$estr_sx=undef;

		$length = scalar @y_buffer;
		$y_average = 0;
		my @temp = ();
		for (my $i = $length - $l; $i < $length; $i++) {
			push @temp,$y_buffer[$i];
			$y_average += $y_buffer[$i];
		}
		@y_buffer = @temp;
	}
}

if (defined $old_estr_dx) {
		
	if (defined $estr_sx) {
		$y_average -= $y_buffer[$#y_buffer];	# ho una y in piu' oltre a quelle dell'intervallo con derivata quasi zero
		$length = scalar @y_buffer - 1;
		$y_average /= $length;
		if ($old_estr_dx > $estr_sx) {
			$old_estr_dx -= int ( ($old_estr_dx - $estr_sx) / 2);
			$estr_sx = $old_estr_dx;
		}
#		print $old_estr_sx,$old_estr_dx;
		print $old_estr_sx,$old_y_average;
		print $old_estr_dx,'0';
#		print $estr_sx,$estr_dx;
		print $estr_sx,$y_average;
		print $estr_dx,'0';
	} else {
#		print $old_estr_sx,$old_estr_dx;
		print $old_estr_sx,$old_y_average;
		print $old_estr_dx,'0';
	}
}



sub get_next_deriv
{
	$x++;

	if(!defined($x_change)) { # inizializzo $x_change

		my $line = <>;
		return undef if !defined($line);
		chomp $line;
		($x_change, $y_change)=split /\t/, $line;
		$x = $x_change - 1; # x della prima finestra non tutta nulla

		push @y_buffer,$y_change;
		$y_average += $y_change;

	} 
	elsif ($x >= $x_change){
		my $tmp = $y_change-$y;
		$y = $y_change;
		my $line = <>;
		return undef if !defined($line);
		chomp $line;
		($x_change, $y_change)=split /\t/, $line;
		push @y_buffer,$y;
		$y_average += $y;
		return $tmp;
	} else {
		push @y_buffer,$y;
		$y_average += $y;
		return 0;
	}
}



sub sliding_window{
	shift @buffer;
	my $val = &get_next_deriv();
	return 0 if !defined($val);
	push @buffer, $val;
	return 1;
}



sub initialize_buffer
{
	my $l = shift @_;
	push @buffer,0; # va bene mettere qualunque valore perche' poi sliding_window shifta di 1
	for ( 1..($l - 1) ) {
		my $deriv = &get_next_deriv();
		die ("ERROR: no points in input file") if (!defined $deriv);
		push @buffer, $deriv;
	}
}
