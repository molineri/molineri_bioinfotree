#!/usr/bin/perl 

use strict;
use warnings;
use Getopt::Long;


$,="\t";
$\="\n";


my $usage = "cat file | stdev_col.pl
	prints out average and standard deviation of each column of the input file\n";


my @all = <>; 
my $n_lines = scalar @all;

# average
my $line = $all[0];
my @sum = split /\t/,$line;
my $n_col = scalar @sum; 
my @stdev = ();

for (my $j = 1; $j < $n_lines; $j++) {
	my @F = split /\t/,$all[$j];
	for (my $i=0; $i<$n_col; $i++) {
		$sum[$i] += $F[$i];
	}
}

for (my $j=0; $j<$n_col; $j++) {
	$sum[$j] /= $n_lines;
}


# standard deviation
my @aux = split /\t/,$line;

for (my $j = 0; $j<$n_col; $j++) {
	$stdev[$j] = ($aux[$j] - $sum[$j])**2;
}

for (my $j = 1; $j < $n_lines; $j++) {
	my @F = split /\t/,$all[$j];
	for (my $i=0; $i<$n_col; $i++) {
		$stdev[$i] += ($F[$i] - $sum[$i])**2;
	}
}

for (my $j = 0; $j<$n_col; $j++) {
	$stdev[$j] = sqrt($stdev[$j] / $n_lines);
}

# 1. average
# 2. standard deviation
print @sum;
print @stdev;
