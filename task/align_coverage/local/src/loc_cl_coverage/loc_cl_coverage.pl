#!/usr/bin/perl  

use strict;
use warnings;
use Getopt::Long;
use IO::Handle;
use PerlIO::gzip;


$,="\t";
$\="\n";


my $usage = "loc_cl.pl -d \$(DIR) -b \$(BIN_DIR) -c chromosome\n";
my $DIR = undef;
my $BIN_DIR = undef;
my $chr_n = undef;

GetOptions (
        'dir|d=s' => \$DIR,
        'bin_dir|b=s' => \$BIN_DIR,
        'chr|c=s' => \$chr_n
) or die ($usage);

die ($usage) if !$DIR;
die ($usage) if !$BIN_DIR;
die ($usage) if !$chr_n;

$DIR =~ /^\s*(\S+)\/*\s*$/;
$DIR = $1;


# lista dei files che riguardano il cromosoma $chr_n
my %sx = ();
my %dx = ();
my $ls_sx = $DIR.'/chr'.$chr_n.'_chr*.loc_cl[\.gz]*';
my $list_sx = `ls $ls_sx`;
die( "ERROR: command exited with an error ($?)") if $?!=0;
my @files_list_sx = split /\n/,$list_sx;
my %aux = ();
foreach my $file_name (@files_list_sx) {
	$sx{$file_name} = 1;
	$aux{$file_name} = 1 if -s $file_name;
}
my $list_dx = `ls $DIR/chr*_chr$chr_n.loc_cl[\.gz]*`; 
die( "ERROR: command exited with an error ($?)") if $?!=0;
my @files_list_dx = split /\n/,$list_dx; 
foreach my $file_name (@files_list_dx) {
	$dx{$file_name} = 1;
	$aux{$file_name} = 1 if -s $file_name;
}
my @files_list = sort keys %aux;

#print STDERR @files_list;


# apertura dei files del cromosoma $chr_n
my %fh = ();
foreach my $file (@files_list) {
	my $file_handle = IO::Handle->new();
	$fh{$file} = $file_handle;
	if($file!~/\.gz$/){
		open $file_handle,$file or die ("Cant' open file ($file)\n");
	}else{
		my $new_file_name = $file;
		$new_file_name =~ s/$DIR\///;
		$new_file_name =~ s/\.gz$/\.tmp/;
		`zcat $file > $new_file_name`;
		open $file_handle, $new_file_name or die ("Cant' open file ($new_file_name)\n");
		`rm $new_file_name`;
	}
}
#die join ("\n",@files_list);
# elenco dei files non vuoti contenenti location clusters


# apertura pipe per la lista dei location cluster
my $tmp_file = $chr_n.'.coverage.tmp';
open PIPE,"| $BIN_DIR/coverage | $BIN_DIR/coverage_trigger 1 > $tmp_file" or die ("Can't open pipe\n");


# da ogni file estraggo start,stop,offset degli header fasta
my %lo_cl_pos_sx = ();
my %lo_cl_pos_dx = ();
my %pos_in_starts_sx = ();
my %pos_in_starts_max_sx = ();
my %pos_in_starts_dx = ();
my %pos_in_starts_max_dx = ();

foreach my $file (@files_list) {
	my $file_handle = $fh{$file};
	my @list_sx = ();
	my @list_dx = ();
	my $is_left = defined $sx{$file};
	my $is_right = defined $dx{$file};

	while (my $line = <$file_handle>) {
		next if (!($line =~ /^>/));

		my ($start1,$stop1,$start2,$stop2) = ($line =~ /^>(\d+)_(\d+)_(\d+)_(\d+)\s*$/);
		my $offset = tell ($file_handle);
		die "$offset\t$start1\t$stop1" if ( (!defined $start1) || (!defined $stop1) );
		die "$offset\t$start2\t$stop2" if ( (!defined $start2) || (!defined $stop2) );

		if ($is_left) {
			my @temp = ($start1,$stop1,$offset);
			print PIPE $start1,$stop1;
			push @list_sx, \@temp;
		}
		if ($is_right) {
			my @temp = ($start2, $stop2,$offset);
			print PIPE $start2,$stop2;
			push @list_dx, \@temp;
		}
	}

	if ($is_left) {
		@{$lo_cl_pos_sx{$file}} = sort { ${$a}[0] <=> ${$b}[0] } @list_sx;
		$pos_in_starts_sx{$file} = 0;
		$pos_in_starts_max_sx{$file} = scalar(@list_sx);
	}
	
	if ($is_right) {
		@{$lo_cl_pos_dx{$file}} = sort { ${$a}[0] <=> ${$b}[0] } @list_dx;
		$pos_in_starts_dx{$file} = 0;
		$pos_in_starts_max_dx{$file} = scalar(@list_dx);
	}
}

close PIPE;

# apertura in lettura del file tmp 
# (regioni coperte da almeno un allineamento, ordinate per start crescente)
open TMP,$tmp_file or die ("Can't open file ($tmp_file)\n"); 
unlink($tmp_file);
# il file tmp e' uguale al file $chr_n.loc_cl creato dal makefile in $BIOINFO_ROOT/task/test_loc_cl_coverage/dataset



# per ogni location cluster cerco la regione in cui cade
while (<TMP>) {
	chomp;
	my ($r_start,$r_stop) = split /\t/;

	# apertura pipe per la lista dei location cluster
	my $PIPE;
	open $PIPE,"| $BIN_DIR/coverage" or die ("Can't open pipe\n");

	my $pos_in_starts_sx_ref = &process_side($PIPE, $r_start, $r_stop, 'sx');
	%pos_in_starts_sx = %{$pos_in_starts_sx_ref};
	my $pos_in_starts_dx_ref = &process_side($PIPE, $r_start, $r_stop, 'dx');
	%pos_in_starts_dx = %{$pos_in_starts_dx_ref};

	close $PIPE;		
}
			
close TMP;

	
sub process_side
{
	my $PIPE = shift @_;
	my $r_start = shift @_;
	my $r_stop = shift @_;
	my $side = shift @_;

	my %pos_in_starts = (); 
	my %pos_in_starts_max = (); 
	my %lo_cl_pos = ();
	
	if($side eq "sx"){
		%lo_cl_pos = %lo_cl_pos_sx;
		%pos_in_starts = %pos_in_starts_sx;
		%pos_in_starts_max = %pos_in_starts_max_sx;
	}else{
		%lo_cl_pos = %lo_cl_pos_dx;
		%pos_in_starts = %pos_in_starts_dx;
		%pos_in_starts_max = %pos_in_starts_max_dx;
	}

	foreach my $locl_file (keys %lo_cl_pos) {
		my $file_handle = $fh{$locl_file};
		my $i = $pos_in_starts{$locl_file};
		next if ($i >= $pos_in_starts_max{$locl_file});
		my $start  = ${${$lo_cl_pos{$locl_file}}[$i]}[0];
		my $stop   = ${${$lo_cl_pos{$locl_file}}[$i]}[1];
		my $offset = ${${$lo_cl_pos{$locl_file}}[$i]}[2];

		while ( ($start >= $r_start) and ($stop <= $r_stop) ) {
			die if (seek $file_handle,$offset,0) == 0;
			my $align = <$file_handle>;
		#	print STDERR $locl_file;
	#		print STDERR $start,$stop,$offset;
#			print STDERR $align;
#			$_=<$file_handle>;
#			print STDERR;
#			die;
			while ( (defined $align) and ($align !~ /^>/) ) {
				chomp $align;
				my ($start1,$stop1,$start2,$stop2) = ($align =~ /^\w+\t(\d+)\t(\d+)\t[+-]\t\w+\t(\d+)\t(\d+)\t/);
##				print STDERR $align;
#				print STDERR $start1,$stop1,$start2,$stop2;
#				die;
				die($align) if (! defined($start1));

				if($side eq 'sx'){
					print $PIPE $start1,$stop1;
				} else {
					print $PIPE $start2,$stop2;
				}

				$align = <$file_handle>;

			}
			$i++;
			if ($i >= $pos_in_starts_max{$locl_file}) {
				last; 
			}
			$start  = ${${$lo_cl_pos{$locl_file}}[$i]}[0];
			$stop   = ${${$lo_cl_pos{$locl_file}}[$i]}[1];
			$offset = ${${$lo_cl_pos{$locl_file}}[$i]}[2];
		}
		$pos_in_starts{$locl_file} = $i;
	}
	return \%pos_in_starts;
}


# per verificare che il numero di allineamenti corrisponda:
# in $BIOINFO_ROOT/task/sinteny/dataset/homo_megablast_38/loc_cl_3000
# cat chr22_chr*.loc_cl | grep -v '>' | wc -l
# cat chr*_chr22.loc_cl | grep -v '>' | wc -l
# la somma dei 2 numeri deve essere pari al numero totale di allineamenti



# seek FILEHANDLE,POSIZIONE,DA_DOVE 
# POSIZIONE 
#	numero di BYTES da aggiungere al posizionamento 
# DA_DOVE
# 	0 -> partendo dall'inizio del file
#	1 -> partendo dalla posizione corrente
# 	2 -> partendo dal fondo del file
#
# TELL da' la posizione corrente (BYTES) nella lettura del file (partendo dall'inizio)
