#!/usr/bin/perl 

use strict;
use warnings;
use constant LEFT_B_COL  => 1;
use constant LEFT_E_COL  => 2;
use constant RIGHT_B_COL => 5;
use constant RIGHT_E_COL => 6;
use Getopt::Long;


$,="\t";
$\="\n";

my $usage = "cat alignments_b1_e1_b2_e2 | $0 -l loc_cl_header_file";

my $loc_cl_file = undef;

GetOptions (
	'input_file|l=s' => \$loc_cl_file,
) or die ($usage);


open LOC_CL,$loc_cl_file or die ("Can't open file ($loc_cl_file!)\n"); 

#my $old_b = undef;
my @loc_cl_headers = ();

while (<LOC_CL>) {
	chomp;

	die "ERROR: location clusters not in tab delimited format b1\\te1\\tb2\\te2" if ($_ =~ /^>/ or $_ =~ /_/);
	my @F_loc_cl = split /\t/,$_;

	die ("ERROR: stop <= start") if (!($F_loc_cl[1] > $F_loc_cl[0]) or !($F_loc_cl[3] > $F_loc_cl[2]));
#	die "ERROR: file not sorted on the left_start\n" if ( (defined $old_b) and ($F[LEFT_B_COL] < $old_b) );
#	$old_b = $F[LEFT_B_COL];

	push @loc_cl_headers,\@F_loc_cl;
}
my $l = scalar @loc_cl_headers;



my @align_loc_cl = ();
#$old_b = undef;

while (<>) {
	chomp;
	my @F_align = split /\t/,$_;

#	die "ERROR: file not sorted on the left_start\n" if ( (defined $old_b) and ($F[LEFT_B_COL] < $old_b) );
#	$old_b = $F[LEFT_B_COL];

	die ("ERROR: stop <= start") if ( 
		!($F_align[LEFT_E_COL] > $F_align[LEFT_B_COL]) 
			or 
		!($F_align[RIGHT_E_COL] > $F_align[RIGHT_B_COL]) );

	my $ret_val = &insert_in_loc_cl(\@F_align);
	die "ERROR: alignment not inserted in location cluster\n".join "\t",@F_align if !$ret_val;
}


&print_all;



sub insert_in_loc_cl
{
	my $line_ref = shift;

	my @line = @{$line_ref};

	for (my $i=0; $i<$l; $i++) {
		my ($b1,$e1,$b2,$e2) = @{$loc_cl_headers[$i]};

		next if ($e1 < $line[LEFT_B_COL]);
		next if ($line[LEFT_E_COL] < $b1);

		next if ($e2 < $line[RIGHT_B_COL]);
		next if ($line[RIGHT_E_COL] < $b2);

		push @{$align_loc_cl[$i]}, \@line;

		return 1;
	}

	return 0;
}


sub print_all
{
	for (my $i=0; $i<$l; $i++) {
		my $header = '>'.join '_',@{$loc_cl_headers[$i]};
		print $header;
		foreach my $el (@{$align_loc_cl[$i]}) {
			my @aux = @{$el};
			print @aux;
		}
	}
}
