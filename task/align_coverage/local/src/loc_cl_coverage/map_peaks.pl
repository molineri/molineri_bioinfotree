#!/usr/bin/perl 

use strict;
use warnings;
use Getopt::Long;


$,="\t";
$\="\n";
$| = 1;

my $usage = "$0 -f loc_cl_file -b \$(BIN_DIR) 1_50.coverage_triggered 22_50.coverage_triggered";
my $loc_cl_file = undef;
my $BIN_DIR = undef;

GetOptions (
        'loc_cl_file|f=s' => \$loc_cl_file,
        'bin_dir|b=s' => \$BIN_DIR
) or die ($usage);

die ($usage) if !$loc_cl_file;
die ($usage) if !$BIN_DIR;
die ($usage) if (scalar @ARGV != 2);

die ("ERROR: $0: file ($loc_cl_file) is void") if (!-s $loc_cl_file);
open LOC_CL_FILE,$loc_cl_file or die ("ERROR: $0: Cant' open file ($loc_cl_file)\n"); 


my $region_file1 = shift @ARGV;
my $region_file2 = shift @ARGV;

open REG1,$region_file1 or die ("ERROR: $0: Cant' open file ($region_file1)\n");
open REG2,$region_file2 or die ("ERROR: $0: Cant' open file ($region_file2)\n");

my ($chr_n1) = ($region_file1 =~ /(\w+)_\d+\.coverage_triggered/);
my ($chr_n2) = ($region_file2 =~ /(\w+)_\d+\.coverage_triggered/);


my $side;
my $sx_filename = "chr$chr_n1"."_chr$chr_n2.loc_cl";
if ($loc_cl_file =~ /$sx_filename/) {
	$side = 'sx';
} else {
	$side = 'dx';
}


# dal file coi location clusters estraggo start,stop,offset degli header fasta
my @loc_cl_pos_sx = ();
my @loc_cl_pos_dx = ();
my $pos_in_starts_max_sx;
my $pos_in_starts_max_dx;

my @list_sx = ();
my @list_dx = ();

while (my $line = <LOC_CL_FILE>) {
	next if (!($line =~ /^>/));

	my ($start1,$stop1,$start2,$stop2) = ($line =~ /^>(\d+)_(\d+)_(\d+)_(\d+)\s*$/);
	my $offset = tell (LOC_CL_FILE);
	die $offset if ( (!$start1) || (!$stop1) );
	die $offset if ( (!$start2) || (!$stop2) );

	my @temp_sx = ($start1,$stop1,$offset);
	push @list_sx, \@temp_sx;

	my @temp_dx = ($start2, $stop2,$offset);
	push @list_dx, \@temp_dx;
}

@loc_cl_pos_sx = sort { ${$a}[0] <=> ${$b}[0] } @list_sx;
$pos_in_starts_max_sx = scalar(@list_sx);

@loc_cl_pos_dx = sort { ${$a}[0] <=> ${$b}[0] } @list_dx;
$pos_in_starts_max_dx = scalar(@list_dx);



my %read;
my $printed_region;
my @array;

while (<REG1>) {
	%read=();
	chomp;
	my ($chr,$r_start,$r_stop) = split /\t/;
	$printed_region = 0;


	# apertura pipe per la lista dei location cluster
	open PIPE,"| $BIN_DIR/coverage" or die ("ERROR: $0: Can't open pipe\n");


	my $pos_in_starts_sx = &process_side($r_start, $r_stop, $side);

	close PIPE;		
}



sub process_side
{
	my $r_start = shift @_;
	my $r_stop = shift @_;
	my $side = shift @_;

	my $pos_in_starts_max; 
	my @loc_cl_pos = ();
	
	if($side eq 'sx'){
		@loc_cl_pos = @loc_cl_pos_sx;
		$pos_in_starts_max = $pos_in_starts_max_sx;
	}else{
		@loc_cl_pos = @loc_cl_pos_dx;
		$pos_in_starts_max = $pos_in_starts_max_dx;
	}

	for (my $i = 0; $i < $pos_in_starts_max; $i++) {
		my $start  = ${$loc_cl_pos[$i]}[0];
		my $stop   = ${$loc_cl_pos[$i]}[1];
		my $offset = ${$loc_cl_pos[$i]}[2];

		next if ($stop <= $r_start);
		if ($start >= $r_stop) {
			last;
		}
		next if (defined $read{$offset});

		seek LOC_CL_FILE,$offset,0;
		my $align = <LOC_CL_FILE>;
		while ( (defined $align) and ($align !~ /^>/) ) {
			chomp $align;
			my ($start1,$stop1,$start2,$stop2) = ($align =~ /^\s*(\d+)\t(\d+)\t(\d+)\t(\d+)\s+/);
			die("ERROR: $0:  $align") if (! defined($start1));

			if ($side eq 'sx') {
				if ( ! ( ($stop1 <= $r_start) or ($start1 >= $r_stop) ) ){
					push @array,$start2,$stop2;
				}
			} else {
				if ( ! ( ($stop2 <= $r_start) or ($start2 >= $r_stop) ) ){
					push @array,$start1,$stop1;
				}
			}

#			if ( ! ( ($stop1 <= $r_start) or ($start1 >= $r_stop) ) ){
#				#print '>'.$chr_n,$r_start,$r_stop if (!$printed_region);
#				$printed_region = 1;
#				$start1 = $start1 >= $r_start ? $start1 : $r_start;
#				$stop1 = $stop1 <= $r_stop ? $stop1 : $r_stop;
#				print PIPE $start1,$stop1;
#				$read{$offset}=1;
#			}
#			if ( ! ( ($stop2 <= $r_start) or ($start2 >= $r_stop) ) ){
#				#print '>'.$chr_n,$r_start,$r_stop if (!$printed_region);
#				$printed_region = 1;
#				$start2 = $start2 >= $r_start ? $start2 : $r_start;
#				$stop2 = $stop2 <= $r_stop ? $stop2 : $r_stop;
#				print PIPE $start2,$stop2; 
#				$read{$offset}=1;
#			}

			$align = <LOC_CL_FILE>;

		}
	}
	
	print $r_stop,0;
	return;
}
