#!/usr/bin/perl  

use strict;
use warnings;
use Getopt::Long;


$,="\t";
$\="\n";


my $usage = "header_aligments.pl -d \$(DIR)\n";
my $DIR = undef;

GetOptions (
        'dir|d=s' => \$DIR,
) or die ($usage);

die ($usage) if !$DIR;

$DIR =~ /^\s*(\S+)\/\n*$/;
$DIR = $1;
my $list = `grep  '>' $DIR/*.loc_cl`; 
# per ogni file e per ogni location cluster grep restituisce una riga del tipo
#	chr10_chr10.loc_cl:>130370_159561_128086032_128100178
my @loc_clus_list = split /\n/,$list;
foreach my $grep_line (@loc_clus_list){
	chomp $grep_line;
	my ($chr1,$chr2,$start1,$stop1,$start2,$stop2) = ($grep_line =~ /^\s*$DIR\/chr([0-9A-Za-z]+)_chr([0-9A-Za-z]+)\.loc_cl\s*:\s*>\s*(\d+)_(\d+)_(\d+)_(\d+)\s*$/);
	#if ($start1 == 1){
	#	die "line is $grep_line";
	#}
	print $chr1,$start1,$stop1,$chr2,$start2,$stop2;
}
