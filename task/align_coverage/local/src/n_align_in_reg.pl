#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;


$,="\t";
$\="\n";

my $usage = "cat regions.align_peak_peak | $0";


my %count = ();
my $l_reg = undef;
my $r_reg = undef;

while (my $line = <>) {
	chomp $line;
	
	if ($line =~ /^>/) {
		#>10_45158385_45161583   14_37990154_37990891
		($l_reg,$r_reg) = ($line =~ /^>(\w+_\d+_\d+)\t(\w+_\d+_\d+)$/);
		die "ERROR: region non defined at line",$line if (!defined $l_reg or !defined $r_reg);
	} else {
		die "ERROR: region non defined at line",$line if (!defined $l_reg or !defined $r_reg);
	
		$count{$l_reg} = 0 if !defined $count{$l_reg};
		$count{$l_reg}++;
		next if ($l_reg eq $r_reg);

		$count{$r_reg} = 0 if !defined $count{$r_reg};
		$count{$r_reg}++;
	}
}

foreach (sort keys %count) {
	print $_,$count{$_};
}

