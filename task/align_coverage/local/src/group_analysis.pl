#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Long;

$,="\t";
$\="\n";


my $usage = " $0 qualcosa.conn_comp score_connectivity [-id] [-ccomp]
	-id if ids in file qualcosa.conn_comp don't contain chr,b,e
	-ccomp if groups are connected components (e.g. no links between nodes from 2 different groups)";

my $id_only   = undef;
my $conn_comp = undef;
GetOptions (
	'id_only|id'  => \$id_only,
	'ccomp|ccomp' => \$conn_comp
) or die ($usage);
my $group_file = shift @ARGV;
die $usage if !$group_file;

open FH,$group_file or die("can't open file ($group_file)");

my $group_id=undef;
my %group=();
my @all_group_id=();
while(<FH>){
	chomp;
	my @F=split /\t/;
	$group_id = shift @F;
	for(@F){
		$group{$_}=$group_id;
	}
	push @all_group_id, $group_id;
}

my %conn=();
my %score=();
while(<>){
	chomp;
	my @F=split;
	if ($id_only) {
		$F[0] =~ s/:.*//g;
		$F[2] =~ s/:.*//g;
	}

	if ($conn_comp) {
		die ("ERROR: edge between nodes from different connected components!!") if ($group{$F[0]} ne $group{$F[2]});

		$conn{$group_id}{$F[0]}=$F[1];
		$conn{$group_id}{$F[2]}=$F[3];
	} else {
		next if (!defined $group{$F[0]} or !defined $group{$F[2]}); # considero solo i links all'interno del gruppo
		next if ($group{$F[0]} ne $group{$F[2]}); # considero solo i links all'interno del gruppo

	}

	$group_id = $group{$F[0]};

	push @{$score{$group_id}},$F[6];
}

for(@all_group_id){
	my $score_mean = &mean(\@{$score{$_}});
	my $score_stdev = &stdev(\@{$score{$_}},$score_mean);
	my $score_ref = &median_min_max(\@{$score{$_}});
	my ($score_median, $score_min, $score_max) = @{$score_ref};

	my $node_num;
	my $conn_mean;
	my $conn_stdev;
	my $conn_median;
	my $conn_min;
	my $conn_max;

	if ($conn_comp) {
		my @a = values %{$conn{$_}};
		$node_num = scalar (@a);
		$conn_mean = &mean(\@a);
		$conn_stdev = &stdev(\@a,$conn_mean);
		my $conn_ref = &median_min_max(\@a);
		($conn_median, $conn_min, $conn_max) = @{$conn_ref};
	} else {
		my $edge_num = scalar (@{$score{$_}});
		$node_num = int (sqrt ($edge_num * 2)) + 1;
		$conn_mean = $node_num - 1;
		$conn_stdev = 0;
		$conn_median = $node_num - 1;
		$conn_min = $node_num - 1;
		$conn_max = $node_num - 1;
	}
	
	print $_, $node_num, 	$score_mean,$score_median,$score_stdev,$score_min,$score_max,
				$conn_mean, $conn_median, $conn_stdev, $conn_min, $conn_max;
}

sub mean
{
	my $sum=0;
	foreach my $value (@{$_[0]}){
		$sum += $value;
	}
	return $sum/scalar(@{$_[0]});
}

sub median_min_max
{
	my $i = int(scalar(@{$_[0]})/2);
	my @a = sort {$a<=>$b} @{$_[0]};
	my @tmp=($a[$i],$a[0],$a[scalar(@a)- 1]);
	return \@tmp;
}

sub stdev
{
	my $mean = pop @_;
	my $stdev=0;

	foreach my $value (@{$_[0]}){
		$stdev += ($value - $mean)**2;
	}

	return sqrt($stdev / scalar(@{$_[0]}));
}


