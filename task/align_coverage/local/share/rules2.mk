SPECIES ?= hsapiens
ALIGNMENT_DIR ?= $(BIOINFO_ROOT)/task/alignments/inhouse/dataset/homo_homo_ensembl38_megablast
LOC_CL_DIR ?= $(BIOINFO_ROOT)/task/align_loc_cl/dataset/homo_homo_ensembl38_megablast/3000
ANNOTATION_DIR ?= $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/$(SPECIES)/$(ENS_VERSION)
UCSC_ANNOTATION_DIR ?= $(BIOINFO_ROOT)/task/annotations/dataset/ucsc/$(SPECIES)/$(UCSC_VERSION)
GO_ANNOTATION_DIR ?= $(BIOINFO_ROOT)/task/annotations/dataset/GO/$(SPECIES)/$(GO_ENS_VERSION)
GO_ENS_VERSION ?= 42
TAXONOMY_FILE:= $(BIOINFO_ROOT)/local/share/taxonomy
COORDS_NONREDUNDANT ?= C E 5 3 I U N
COORDS_NONREDUNDANT2 ?= C 5 3 I U N
BIN_DIR = $(BIOINFO_ROOT)/task/align_coverage/local/bin
TEST_COVERAGE_DIR = $(BIOINFO_ROOT)/task/alignments/inhouse/dataset/homo_homo_ensembl38_megablast
TRIGGER_LEVEL ?= 50
AUTOCOVERAGE_TRIGGER_LEVEL ?= 1
COVERAGE_WINDOW ?= 5
COVERAGE_MIN_PEAKS ?= 60
MIN_WORD_LENGTH ?= 16
MIN_REGION_LENGTH ?= 16
ALL_TRIGGER_LEVEL ?= 1 5 10 20 30 40 50 70 100 200 400 800

BIN_DIR_SINTENY = $(BIOINFO_ROOT)/task/sinteny/local/bin
RAND_TRIAL ?= 1000
SEQ_DIR?=$(BIOINFO_ROOT)/task/sequences/dataset/ensembl/$(SPECIES)/$(ENS_VERSION)

BIN_DIR_ANNOTATION := $(BIOINFO_ROOT)/task/annotations/local/bin
UCSC_VERSION ?= hg18
ANNOTATIONS_CONS_FILE ?= $(BIOINFO_ROOT)/task/annotations/dataset/ucsc/$(SPECIES)/$(UCSC_VERSION)/conservation_both
ANNOTATIONS_CONS_SCORE ?= 0
ANNOT_RATIO_CUTOFF ?= 0.2
ALL_ANNOT_RATIO_CUTOFF ?= 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8
ANNOT_LABEL ?= C

ALL_CHR ?= 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X Y


SHELL := /bin/bash

.SECONDARY:
.DELETE_ON_ERROR:
.PRECIOUS: $(addsuffix .coverage_triggered.annote.rand,$(addsuffix _$(TRIGGER_LEVEL),$(ALL_CHR)))
.PRECIOUS: $(addsuffix .coverage_triggered.cons_intersection.rand,$(addsuffix _$(TRIGGER_LEVEL),$(ALL_CHR)))
.PRECIOUS: $(addsuffix .coverage_correct_triggered.rand.annote,$(addsuffix _$(TRIGGER_LEVEL),$(addsuffix _$(AUTOCOVERAGE_TRIGGER_LEVEL),$(ALL_CHR))))
.PRECIOUS: $(addsuffix .coverage_correct_triggered.rand.cons_intersection,$(addsuffix _$(TRIGGER_LEVEL),$(addsuffix _$(AUTOCOVERAGE_TRIGGER_LEVEL),$(ALL_CHR))))
.PHONY: all.coverage 

include taxonomy.mk


#########################################
#
#	regole all
#

all:
	@echo all_chr_$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.annote

all_chr%:
	set -e;\
	for i in $(ALL_CHR); do \
		make $${i}$*; \
	done;

accoda_all_chr%:
	set -e;\
	for i in $(ALL_CHR); do \
		accoda --name chr$${i} -- make $${i}$*; \
	done;

all.coverage : $(addsuffix .coverage, $(ALL_CHR))
all.coverage_triggered: $(addsuffix .coverage_triggered,$(addsuffix _$(TRIGGER_LEVEL),$(ALL_CHR)))
all.$(AUTOCOVERAGE_TRIGGER_LEVEL).coverage_correct: $(addsuffix _$(AUTOCOVERAGE_TRIGGER_LEVEL).coverage_correct, $(ALL_CHR))
all.coverage_correct_triggered: $(addsuffix .coverage_correct_triggered,$(addsuffix _$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL),$(ALL_CHR)))


all.annote.rand: $(addsuffix .coverage_triggered.annote.rand,$(addsuffix _$(TRIGGER_LEVEL),$(ALL_CHR)))

all.annote.rand.accoda:
	for i in $(addsuffix .coverage_triggered.annote.rand,$(addsuffix _$(TRIGGER_LEVEL),$(ALL_CHR))); do \
		accoda --name a$${i%%.*} -- make $$i; \
	done;

all.rand.annote.cutoff:
	for i in $(ALL_ANNOT_RATIO_CUTOFF); do \
		for j in $(ALL_CHR); do \
			$(MAKE) $${j}_$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.rand.annote.$$i ANNOT_RATIO_CUTOFF=$$i; \
		done; \
	done;

all.rand.annote.summary:
	for i in $(ALL_ANNOT_RATIO_CUTOFF); do \
		for j in $(ALL_CHR); do \
			$(MAKE) $${j}_$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.rand.annote.$$i.summary ANNOT_RATIO_CUTOFF=$$i; \
		done; \
	done;

all.rand.annote.summary.summary: $(addsuffix $(addprefix annote_vettorini_, $(ALL_ANNOT_RATIO_CUTOFF)),.summary.summary)

all.cons_intersection.rand: $(addsuffix .coverage_triggered.cons_intersection.rand,$(addsuffix _$(TRIGGER_LEVEL),$(ALL_CHR)))

all.cons_intersection.rand.accoda:
	for i in $(addsuffix .coverage_triggered.cons_intersection.rand,$(addsuffix _$(TRIGGER_LEVEL),$(ALL_CHR))); do \
		accoda --name c$${i%%.*} -- make $$i; \
	done; 

all.rand.cons_intersection.cutoff:
	for i in $(ALL_ANNOT_RATIO_CUTOFF); do \
		for j in $(ALL_CHR); do \
			$(MAKE) $${j}_$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.rand.cons_intersection.$$i ANNOT_RATIO_CUTOFF=$$i; \
		done; \
	done;

all.rand.cons_intersection.summary:
	for i in $(ALL_ANNOT_RATIO_CUTOFF); do \
		for j in $(ALL_CHR); do \
			$(MAKE) $${j}_$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.rand.cons_intersection.$$i.summary ANNOT_RATIO_CUTOFF=$$i; \
		done; \
	done;

all.rand.summary: $(addsuffix .coverage_triggered.cons_intersection.rand.summary, $(addsuffix _$(TRIGGER_LEVEL),$(ALL_CHR)))
all.rand.summary: $(addsuffix .coverage_triggered.annote.rand.summary, $(addsuffix _$(TRIGGER_LEVEL),$(ALL_CHR)))

all.annote_%:  
	for i in $(ALL_ANNOT_RATIO_CUTOFF); do \
		make $*_1_50.coverage_correct_triggered.annote_$$i ANNOT_RATIO_CUTOFF=$$i; \
	done; 

all.annote_vettorini:  
	for i in $(ALL_ANNOT_RATIO_CUTOFF); do \
		make annote_vettorini_$$i.summary.summary ANNOT_RATIO_CUTOFF=$$i; \
	done;

all.cons_vettorini: 
	for i in $(ALL_ANNOT_RATIO_CUTOFF); do \
		make cons_vettorini_$$i.summary.summary ANNOT_RATIO_CUTOFF=$$i; \
	done;

all.coverage_test : $(addsuffix .coverage_test, $(ALL_CHR))

all.ps : $(addsuffix .ps, $(ALL_CHR))

%.all_tigger_level:
	for i in $(ALL_TRIGGER_LEVEL); do \
		make $* TRIGGER_LEVEL=$$i; \
	done;



###############################################
################## COVERAGE ###################
###############################################

%.coverage: 	$(addprefix $(addsuffix /chr*_chr,$(LOC_CL_DIR)),$(addsuffix .loc_cl.gz, $(ALL_CHR))) \
		$(addprefix $(addsuffix /chr,$(LOC_CL_DIR)),$(addsuffix _chr*.loc_cl.gz, $(ALL_CHR)))
	echo -e "0\t0" > $@
	$(BIN_DIR)/loc_cl_coverage -d $(LOC_CL_DIR) -b $(BIN_DIR) -c $* \
	| awk '$$1 != 0 {print}' >> $@

#coverage non corretto con autocoverage
%_$(TRIGGER_LEVEL).coverage_triggered: %.coverage
	$(BIN_DIR)/coverage_trigger $(TRIGGER_LEVEL) < $< \
	| ( while read line; do\
		echo -e "$*\t$$line";\
	done; )\
	| awk '$$3 - $$2 > $(MIN_REGION_LENGTH)' \
	| sort -k 2,2n > $@;

%_$(TRIGGER_LEVEL).coverage_correct_triggered: %.coverage_correct
	chr='$*'; chr=$${chr%%_*}; chr=$${chr##chr}; \
	$(BIN_DIR)/coverage_trigger $(TRIGGER_LEVEL) < $< \
	| ( while read line; do\
		echo -e "$$chr\t$$line";\
	done; )\
	| awk '$$3 - $$2 > $(MIN_REGION_LENGTH)' \
	| sort -k 2,2n > $@;

%_$(TRIGGER_LEVEL).autocoverage:	$(addprefix $(addsuffix /chr*_chr,$(LOC_CL_DIR)),$(addsuffix .loc_cl.gz, $(ALL_CHR))) \
					$(addprefix $(addsuffix /chr,$(LOC_CL_DIR)),$(addsuffix _chr*.loc_cl.gz, $(ALL_CHR))) \
					%_$(TRIGGER_LEVEL).coverage_triggered
	echo -e '0	0' > $@ 
	$(BIN_DIR)/autocoverage -f  $(LOC_CL_DIR)/chr$*_chr$*.loc_cl.gz  -b $(BIN_DIR) -c $* $*_$(TRIGGER_LEVEL).coverage_triggered \
	| grep -v '^0	0$$' \
	| awk 'BEGIN{pre=-1} { if (pre==-1) { \
					if ($$2!=0) {print} \
				} else { \
					if ($$2!=pre){print} \
				} \
				pre=$$2}' \
	| tac \
	| awk 'BEGIN{pre=-1} { if($$1!=pre){print} pre=$$1}' \
	| tac \
	>> $@

%_$(AUTOCOVERAGE_TRIGGER_LEVEL).coverage_correct: %.coverage 
	$(MAKE) $*_$(AUTOCOVERAGE_TRIGGER_LEVEL).autocoverage TRIGGER_LEVEL=$(AUTOCOVERAGE_TRIGGER_LEVEL)
	$(BIN_DIR)/coverage_diff $< $*_$(AUTOCOVERAGE_TRIGGER_LEVEL).autocoverage >$@

regions_$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered: $(addsuffix _$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered, $(ALL_CHR))
	cat $^ > $@

coverage.ucsc_track: $(addsuffix _$(AUTOCOVERAGE_TRIGGER_LEVEL).coverage_correct, $(ALL_CHR))
	echo "browser hide all" > $@
	echo "track type=wiggle_0 name="coverage" visibility=full graphType=bar" >> $@
	for i in $(ALL_CHR); do \
		awk "BEGIN{pre=-1} { if (pre!=-1) {print \"chr$$i\t\" pre \"\t\" \$$1 \"\t\" \$$2} pre=\$$1 }" \
		$${i}_$(AUTOCOVERAGE_TRIGGER_LEVEL).coverage_correct \
		>> $@; \
	done

chr_coverage.all_trigger_level: $(addsuffix _$(AUTOCOVERAGE_TRIGGER_LEVEL).coverage_correct,$(ALL_CHR))
	for chr in $(ALL_CHR); do \
		chr_len=`cat $(SEQ_DIR)/chr$${chr}.fa.len`; \
		for j in $(ALL_TRIGGER_LEVEL); do \
			echo -ne "$$chr\t$$j\t" >> $@; \
			$(BIN_DIR)/coverage_trigger $$j < $${chr}_$(AUTOCOVERAGE_TRIGGER_LEVEL).coverage_correct \
			| append_each_row -b '$$chr' \
			| awk '$$3 - $$2 > $(MIN_REGION_LENGTH) {print ($$3 - $$2)}' \
			| sum_column \
			| awk "{ print \$$1/$$chr_len}" \
			>> $@; \
		done; \
	done;

chr_coverage.all_trigger_level.table: chr_coverage.all_trigger_level
	echo ". $(ALL_TRIGGER_LEVEL)" | sed -r 's/\s+/\t/g' > $@;
	for chr in $(ALL_CHR); do \
		echo -ne "$$chr\t" >> $@; \
		cat $< | awk "\$$1 ~ /^$$chr$$/" \
		| transpose | head -n 3 | tail -n 1 \
		>> $@; \
	done;






###############################################
#
#	regole di visualizzazione
#

%.coverage.gnuplot_interactive: %.coverage
	(\
		echo "set key left top;"; \
	        echo "plot '$<' every::1 w step lw 3;" \
	) > $@
	gnuplot $@ -
	rm $@

%.ps : %.gnuplot
	gnuplot $*.gnuplot > $*.ps





#####################################################
############   SCATTERING   #########################
#####################################################
ALIGNMENT_MIN_SCORE ?= 60

hsp_by_peak.%.gz: regions.$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.list $(wildcard $(ALIGNMENT_DIR)/*.pdb.gz)
	set -o pipefail; \
	set -e; \
	( \
		src=$(word $*,$(wordlist 2,$(words $^),$^)); \
		zcat $$src \
		| awk '$$9>=$(ALIGNMENT_MIN_SCORE)' \
		| intersection $<:2:3:4 -; \
		zcat $$src \
		| awk '$$9>=$(ALIGNMENT_MIN_SCORE)' \
		| awk 'BEGIN{OFS="\t"}{ swap(1,5); swap(2,6); swap(3,7); swap(12,13); print} function swap(i,j) {t=$$i; $$i=$$j; $$j=t}' \
		| sort -S30% -k2,2n -k3,3n \
		| intersection $<:2:3:4 -; \
	) \
	| awk 'BEGIN{OFS="\t"}{print $$8,$$1,$$6,$$7,$$9,$$10,$$11,$$12,$$13,$$14,$$15,$$16,$$17,$$18}' \
	| gzip >$@ 

hsp_by_peak.gz: $(addsuffix .gz,$(addprefix hsp_by_peak.,$(shell seq $(words $(wildcard $(ALIGNMENT_DIR)/*.pdb.gz)))))
	set -o pipefail; \
	set -e; \
	for i in $^; do \
		zcat $$i; \
	done \
	| sort -S50% -k1,1 -k6,6 -k3,3n -k4,4n -k7,7n -k8,8n \
	| gzip >$@

peaks.uncov_frac: hsp_by_peak.gz
	set -o pipefail; \
	set -e; \
	export LANG=C; \
	ulimit -v 1048576; \
	zcat $< \
	| ../../../local/src/scattering/uncov_frac.py \
	| sort -S30% -k1,1 >$@

peaks.avg_cov: regions.$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.list $(wildcard *_$(AUTOCOVERAGE_TRIGGER_LEVEL).coverage_correct)
	set -o pipefail; \
	set -e; \
	export LANG=C; \
	repeat_group_pipe 'cut -f1,3,4 | $(BIN_DIR)/avg_coverage $$1_$(AUTOCOVERAGE_TRIGGER_LEVEL).coverage_correct' 2 <$< \
	| sort -S30% -k1,1 >$@

peaks.scattering: peaks.uncov_frac peaks.avg_cov
	join3_pl -i -u -1 1 -2 1 $^ \
	| awk 'BEGIN{OFS="\t"} {print $$1,$$2/$$3.0}' \
	| sort_pl 2 >$@

#%.wublast.parsed.old.uncov_frac: %.wublast.parsed.old
#	( \
#		repeat_group_pipe '\
#			echo -ne "$$1\t"; \
#			cut -f 2,9,10 | sort -k2,2n -k3,3n | union | cut -f 2- | sum_column | awk "{print \$$$$2 - \$$$$1}" \
#		' 1 2 < $<; \
#		sort -S30% -k2,2 $< \
#		| repeat_group_pipe '\
#			echo -ne "$$1\t"; \
#			cut -f 1,7,8 | sort -k2,2n -k3,3n | union | cut -f 2- | sum_column | awk "{print \$$$$2 - \$$$$1}" \
#		' 2 1; \
#	) \
#	| sort -S30% -k1,1 \
#	| $(BIN_DIR)/uncov_frac > $@
#	
#regions.%.scattering: regions.%.list.old regions.%.wublast.parsed.old.uncov_frac all.$(AUTOCOVERAGE_TRIGGER_LEVEL).coverage_correct
#	repeat_group_pipe 'cut -f 1,3,4 | $(BIN_DIR)/avg_coverage $$1_$(AUTOCOVERAGE_TRIGGER_LEVEL).coverage_correct' 2 < $< \
#	| sort -S30% -k1,1 | join3_pl -i -1 1 -2 1 -- - $(word 2,$^) \
#	| awk 'BEGIN{OFS="\t"} {print $$1,$$2/$$3}' | sort_pl 2 > $@


###############################################
############### ANNOTATION ####################
###############################################

%.annote_gene.family: %.annote_gene
	repeat_fasta_pipe -n 'cut -f 6 | append_each_row -b $$HEADER ' < $< \
        | sed -r 's/,?OTTHUMG[0123456789]+//g' | awk '$$2' | sort -k2,2 | uniq > $@

%.annote_gene.family.go: %.annote_gene.family $(GO_ANNOTATION_DIR)/ensg_go.riannot $(GO_ANNOTATION_DIR)/keywords_term_type_level $(GO_ANNOTATION_DIR)/go_description
	$(BIN_DIR_ANNOTATION)/join_annotation.pl -a $(word 2,$^) -tmp $@.tmp -g $< \
	| sort -k2,2 \
	| join3_pl -1 1 -2 2 -i -u -- $(word 3,$^) - \
	| join3_pl -i -u -- $(word 4,$^) - \
	| sort_pl 6 \
	| awk -F "\t" 'BEGIN{OFS="\t"}{print $$3,$$1,$$2,$$4,$$5,$$6,$$7,$$8}' > $@
	rm -f $@.tmp $@.tmp1
	# formato output: 
	# 1. conn_comp_id
	# 2. GO:.....
	# 3. description
	# 4. GO_term_type (biological_process, cellular_component, molecular_function)
	# 5. livello
	# 6. pvalue
	# 7. numero di geni della componente connessa associati alla parola
	# 8. elenco dei geni della componente connessa associati alla parola

%.annote_gene.family.pfam: %.annote_gene.family $(BIOINFO_ROOT)/task/annotations/dataset/pfam/hsapiens/42/gene_pfam $(BIOINFO_ROOT)/task/annotations/dataset/pfam/hsapiens/42/pfam_acc_desc
	$(BIN_DIR_ANNOTATION)/join_annotation.pl -a $(word 2,$^) -tmp $@.tmp -g  $< \
	| sort -k2,2 \
	| join3_pl - -i -u -1 2 -2 1 -- $(word 3,$^) \
	| sort_pl 3 \
	| awk -F"\t" 'BEGIN{OFS="\t"}{print $$1,$$2,$$3,$$6,$$4,$$5}' > $@


%.annote_gene.diseases: %.annote_gene $(BIOINFO_ROOT)/task/annotations/dataset/ncbi/omim/070118/ens2disorders
	repeat_fasta_pipe -n 'cut -f 6,7,8 | append_each_row -b $$HEADER ' < $< \
	| sed -r 's/,?OTTHUMG[0123456789]+//g' | awk -F"\t" '$$2' | sort -k2,2 | uniq \
	| join3_pl -i -u -1 2 -2 1 - $(word 2,$^) > $@

regions.%.annote_sum: regions.%.list $(addprefix $(ANNOTATION_DIR)/coords_nonredundant.,$(ALL_CHR))
	cut -f 2- $< | sort -k 1,1 -k 2,2n \
	| repeat_group_pipe ' \
		intersection - $(ANNOTATION_DIR)/coords_nonredundant.$$1 \
		| annote_vettorini -n -l "$(COORDS_NONREDUNDANT)" \
		| cut -f 4- \
	' 1 \
	| sum_column -n >> $@

%.annote: % $(addprefix $(ANNOTATION_DIR)/coords_nonredundant.,$(ALL_CHR))
	cut -f -3 $* | sort -k 2,2n > $@.tmp
	# file chr_b_e ordinato per b crescente
	chr='$*'; chr=$${chr%%_*}; chr=$${chr##chr}; \
	intersection $@.tmp $(ANNOTATION_DIR)/coords_nonredundant.$$chr \
	| annote_vettorini $(NORMALIZE_VETTORINI) -l '$(COORDS_NONREDUNDANT)' > $@
	rm $@.tmp

%.rand.annote: %.annote
	set -o pipefail; \
	set -e; \
	chr='$*'; chr=$${chr%%_*}; chr=$${chr##chr}; \
	( \
		for i in `seq 1 $(RAND_TRIAL)`; do \
			$(BIN_DIR_SINTENY)/random_segments2.pl -s $(SEQ_DIR) $< | sort -S20% -k 2,2n \
			| intersection - $(ANNOTATION_DIR)/coords_nonredundant.$$chr | sort -k4,4n \
			| annote_vettorini $(NORMALIZE_VETTORINI) -l '$(COORDS_NONREDUNDANT)'; \
		done; \
	) > $@


#################################################
# annotazioni UCSC
# 	known_genes
# 	refseq_genes
# 	aceview_genes
#################################################

%.annote_known_genes: % $(UCSC_ANNOTATION_DIR)/known_genes.coords_nonredundant
	cut -f -3 $* | sort -k 2,2n > $@.tmp
	# file chr_b_e ordinato per b crescente
	intersection $@.tmp $(word 2,$^) \
	| annote_vettorini $(NORMALIZE_VETTORINI) -l '$(COORDS_NONREDUNDANT2)' > $@
	rm $@.tmp




#################################################
# distribuzioni delle annotazioni:
# per ogni chr, cfr tra
# whole_chr
# triggered
# random
#################################################

%.rand.annote.sum: %.rand.annote %.annote
	tr "\t" "_" < $< | xargs -n `cat $(word 2,$^)| wc -l` \
	| (while read line; do \
		tr " " "\n" <<< $$line | tr "_" "\t" \
		| cut -f 4- |  sum_column; \
	done) > $@

%.sum.norm: %.sum
	cat $< | normalize_row > $@

all_$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.annote.sum.norm.sum: $(addsuffix _$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.annote.sum.norm,$(ALL_CHR))
	cat $^ | sum_column | normalize_row > $@

all_$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.annote.sum.norm.log_ratio: $(addsuffix _$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.annote.sum.norm,$(ALL_CHR)) all_$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.annote.sum.norm.sum
	for i in $(addsuffix _$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.annote.sum.norm,$(ALL_CHR)); do \
		chr="$$i"; chr=$${chr%%_*}; \
		echo -ne "$$chr\t" >> $@; \
		cat $$i all_$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.annote.sum.norm.sum \
		| transpose | awk '{print log($$1/$$2)/log(10)}' | transpose >> $@;\
	done;

%.rand.annote.sum.norm.distrib: %.rand.annote.sum.norm
	rm -f $@; \
	for i in `seq 1 7`; do \
		echo ">$$i" >> $@; \
		cut -f $$i $< | binner -b m | normalizze_distrib >> $@; \
	done;

%.rand.annote.sum.norm.distrib.cfr: %.rand.annote.sum.norm.distrib %.annote.sum.norm $(addprefix $(ANNOTATION_DIR)/coords_nonredundant_per_chr.,$(ALL_CHR))
	chr='$*'; chr=$${chr%%_*}; \
	for i in `seq 1 7`; do \
		$(BIN_DIR)/annot_distrib_cfr_gnuplot -l $$i -r $< -t $(word 2,$^) -c $(ANNOTATION_DIR)/coords_nonredundant_per_chr.$$chr \			> $@.tmp; \
		gnuplot $@.tmp > $@.$$i.ps; \
	done;
	rm -f $@.tmp*
	touch $@

%.rand.annote.sum.norm.distrib.cfr2: $(addsuffix _$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.rand.annote.sum.norm.distrib,$(ALL_CHR)) $(addsuffix _$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.annote.sum.norm,$(ALL_CHR)) $(addprefix $(ANNOTATION_DIR)/coords_nonredundant_per_chr.,$(ALL_CHR))
	chr1='$*'; chr1=$${chr1%%_*}; \
	chr2='$*'; chr2=$${chr2##*_}; \
	for i in `seq 1 7`; do \
		$(BIN_DIR)/annot_distrib_cfr2_gnuplot -l $$i \
		-r1 $${chr1}_$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).*.rand.annote.sum.norm.distrib \
		-t1 $${chr1}_$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).*.annote.sum.norm \
		-c1 $(ANNOTATION_DIR)/coords_nonredundant_per_chr.$$chr1 \
		-r2 $${chr2}_$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).*.rand.annote.sum.norm.distrib \
		-t2 $${chr2}_$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).*.annote.sum.norm \
		-c2 $(ANNOTATION_DIR)/coords_nonredundant_per_chr.$$chr2 \
		> $@.tmp.$$i; \
		gnuplot $@.tmp.$$i > $@.$$i.ps; \
	done;
#	rm -f $@.tmp*
#	touch $@

%.annote.sum: %.annote
	cut -f 4- $< | sum_column > $@ 


######################################################################################################################
all_chr.rand.annote.density: $(addsuffix _$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.rand.annote, $(ALL_CHR)) $(addprefix $(SEQ_DIR)/chr, $(addsuffix .fa.len, $(ALL_CHR)))
	(\
		for i in $(ALL_CHR); do \
			echo -ne "$$i\t"; \
			cut -f 4- $${i}_$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.rand.annote \
			| sum_column; \
		done; \
	) | $(BIN_DIR)/annot_density -d $(SEQ_DIR) > $@

all_chr.rand.annote.density.ps: all_chr.rand.annote.density
	$(BIN_DIR)/annotations_rand_gnuplot -in $< $(COORDS_NONREDUNDANT) | gnuplot > $@
######################################################################################################################



###############################################
#
#	statistica
#

%.len_distrib: % 
	cat $< | awk '{print $$3 - $$2}' | binner -n 25 -l -b m > $@

%.len_distrib.gnuplot: %.len_distrib
	echo "set logscale y; set logscale x; plot '$<' w l, '' w p pt 6 , '' w histep" > $@.tmp
	gnuplot $@.tmp -
	rm $@.tmp

%.len_distrib.ps: %.len_distrib
	echo "set terminal postscript enhanced color \"Helvetica\" 10;" > $@.tmp
	echo "set logscale y; set logscale x; plot '$<' w l, '' w p pt 6 , '' w histep" >> $@.tmp
	gnuplot $@.tmp > $@
	rm $@.tmp





###############################################################
#
#	conservazione
#

taxonomy.mk: 
	echo -n "TAXONOMY=phast " >$@
	cat $(TAXONOMY_FILE) | unhead | cut -f 2 | tr '\n' ' ' | sed 's/ $$/\n/' >> $@

%.cons_intersection: % $(ANNOTATIONS_CONS_FILE)
	cut -f -3 $* | sort -k 2,2n > $@.tmp
	# file chr_b_e ordinato per b crescente
	$(BIN_DIR_ANNOTATION)/annote_comparative $(ANNOTATIONS_CONS_SCORE) $@.tmp $(word 2,$^) \
	| annote_vettorini $(NORMALIZE_VETTORINI) -l '$(TAXONOMY)' > $@
	rm $@.tmp



%.rand.cons_intersection: %.rand.annote $(ANNOTATIONS_CONS_FILE)
	cut -f -3 $< | sort -k 2,2n > $@.tmp
	# file chr_b_e ordinato per b crescente
	$(BIN_DIR_ANNOTATION)/annote_comparative $(ANNOTATIONS_CONS_SCORE) $@.tmp $(word 2,$^) \
	| annote_vettorini $(NORMALIZE_VETTORINI) -l '$(TAXONOMY)' > $@
	rm $@.tmp
	( \
		for j in $(ALL_ANNOT_RATIO_CUTOFF); do \
			cat $@ \
			| awk 'BEGIN {OFS="\t"} { for (i=4;i<=NF;i++) {if ($$i>= $$j ) {$$i=1} else {$$i=0}} print}' \
			| sum_column \
			> $*.rand.cons_intersection.$$j; \
		done; \
	)

####################################################
# distribuzioni della conservazione tra le specie:
# per ogni chr, cfr tra
# whole_chr
# triggered
# random
####################################################

%.cons_intersection.sum: %.rand.cons_intersection %.cons_intersection
	tr "\t" "_" < $< | xargs -n `cat $(word 2,$^)| wc -l` \
	| (while read line; do \
		tr " " "\n" <<< $$line | tr "_" "\t" \
		| cut -f 4- |  sum_column; \
	done) > $@

%.rand.cons_intersection.sum.norm.distrib: %.rand.cons_intersection.sum.norm
	rm -f $@; \
	for i in `seq 1 17`; do \
		echo ">$$i" >> $@; \
		cut -f $$i $< | binner -b m | normalizze_distrib >> $@; \
	done;

%.rand.cons_intersection.sum.norm.distrib.cfr: %.rand.cons_intersection.sum.norm.distrib %.cons_intersection.sum.norm $(ANNOTATIONS_CONS_FILE)
	# per ogni chr, creare file con le sue regioni conservate (analogo di coords_nonredundant_per_chr.$$chr)
	chr='$*'; chr=$${chr%%_*}; \
	for i in `seq 1 17`; do \
		cat $(word 2,$^) | awk '$$1==${chr}' > $(word 3,$^).$$chr; \
#		chr b e sum_annot
		$(BIN_DIR)/annot_distrib_cfr_gnuplot -l $$i -r $< -t $(word 2,$^) -c $(ANNOTATION_DIR)/coords_nonredundant_per_chr.$$chr \			> $@.tmp; \
		gnuplot $@.tmp > $@.$$i.ps; \
	done;
	rm -f $@.tmp*
	touch $@

%.rand.cons_intersection.sum.norm.distrib.cfr2: $(addsuffix _$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.rand.cons_intersection.sum.norm.distrib,$(ALL_CHR)) $(addsuffix _$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.cons_intersection.sum.norm,$(ALL_CHR)) $(addprefix $(ANNOTATION_DIR)/coords_nonredundant_per_chr.,$(ALL_CHR))
	chr1='$*'; chr1=$${chr1%%_*}; \
	chr2='$*'; chr2=$${chr2##*_}; \
	for i in `seq 1 17`; do \
		$(BIN_DIR)/annot_distrib_cfr2_gnuplot -l $$i \
		-r1 $${chr1}_$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).*.rand.cons_intersection.sum.norm.distrib \
		-t1 $${chr1}_$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).*.cons_intersection.sum.norm \
		-c1 $(ANNOTATION_DIR)/coords_nonredundant_per_chr.$$chr1 \
		-r2 $${chr2}_$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).*.rand.cons_intersection.sum.norm.distrib \
		-t2 $${chr2}_$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).*.cons_intersection.sum.norm \
		-c2 $(ANNOTATION_DIR)/coords_nonredundant_per_chr.$$chr2 \
		> $@.tmp.$$i; \
		gnuplot $@.tmp.$$i > $@.$$i.ps; \
	done;
#	rm -f $@.tmp*
#	touch $@

%.cons_intersection.sum: %.cons_intersection
	cut -f 4- $< | sum_column > $@ 




%.reg2reg: $(LOC_CL_DIR)/%.loc_cl.gz 
	zcat $< > $@.tmp
	$(BIN_DIR)/region2region -f $@.tmp  -r_sx 8_1_50.coverage_correct_triggered -r_dx 21_1_50.coverage_correct_triggered > $@



############################### annotazioni con ANNOT_RATIO_CUTOFF ######################################

%.annote.$(ANNOT_RATIO_CUTOFF): %.annote
	cat $< \
	| awk 'BEGIN {OFS="\t"} { for (i=4;i<=NF;i++) {if ($$i>= $(ANNOT_RATIO_CUTOFF) ) {$$i=1} else {$$i=0}} print}' \
	> $@

%.rand.annote.$(ANNOT_RATIO_CUTOFF).sum: %.rand.annote.$(ANNOT_RATIO_CUTOFF) %.annote
	tr "\t" "_" < $< | xargs -n `cat $(word 2,$^)| wc -l` \
	| (while read line; do \
		tr " " "\n" <<< $$line | tr "_" "\t" \
		| cut -f 4- |  sum_column; \
	done) > $@

%.annote.$(ANNOT_RATIO_CUTOFF).sum: %.annote.$(ANNOT_RATIO_CUTOFF)
	cut -f 4- $< | sum_column > $@ 

%.rand.annote.$(ANNOT_RATIO_CUTOFF).sum.norm.distrib: %.rand.annote.$(ANNOT_RATIO_CUTOFF).sum.norm
	for i in `seq 1 7`; do \
		echo ">$$i" >> $@; \
		cut -f $$i $< | binner -b m | normalizze_distrib >> $@; \
	done;

#%.rand.annote.$(ANNOT_RATIO_CUTOFF).sum.distrib.cfr: %.rand.annote.$(ANNOT_RATIO_CUTOFF).sum.distrib %.annote.$(ANNOT_RATIO_CUTOFF).sum
# confronto tra la distribuzione delle annotazioni dei random e le annotazioni dei segmenti reali

all.$(TRIGGER_LEVEL).coverage_correct_triggered.rand.annote.$(ANNOT_RATIO_CUTOFF).sum.norm.distrib.gnuplot: $(addsuffix _1_$(TRIGGER_LEVEL).coverage_correct_triggered.rand.annote.$(ANNOT_RATIO_CUTOFF).sum.norm.distrib, $(ALL_CHR))
	rm -f all.$(TRIGGER_LEVEL).coverage_correct_triggered.rand.annote.$(ANNOT_RATIO_CUTOFF).sum.norm.distrib.gnuplot.tmp.*
	for i in $(ALL_CHR); do \
		for j in `seq 1 7`; do \
			cat $${i}_1_$(TRIGGER_LEVEL).coverage_correct_triggered.rand.annote.$(ANNOT_RATIO_CUTOFF).sum.norm.distrib \
			| get_fasta $$j | grep -v -F '>' \
			>> $@.tmp.$$j; \
			echo -ne "\n\n" >> $@.tmp.$$j; \
		done; \
	done;
	for i in `seq 1 7`; do \
		$(BIN_DIR)/annot_distrib_gnuplot $@ -l $$i -chr '$(ALL_CHR)' > $@.tmp; \
		gnuplot $@.tmp > $@.$$i.ps; \
	done;
	rm -f $@.tmp*
	touch $@


%.$(TRIGGER_LEVEL).coverage_correct_triggered.rand.annote.$(ANNOT_RATIO_CUTOFF).sum.norm.distrib.gnuplot: $(addsuffix _1_$(TRIGGER_LEVEL).coverage_correct_triggered.rand.annote.$(ANNOT_RATIO_CUTOFF).sum.norm.distrib, $(ALL_CHR))
	chr1='$*'; chr1=$${chr1%%_*}; \
	chr2='$*'; chr2=$${chr2##*_}; \
	for j in `seq 1 7`; do \
		cat $${chr1}_1_$(TRIGGER_LEVEL).coverage_correct_triggered.rand.annote.$(ANNOT_RATIO_CUTOFF).sum.norm.distrib \
		| get_fasta $$j | grep -v -F '>' \
		> $@.tmp.$$j; \
		echo -ne "\n\n" >> $@.tmp.$$j; \
		cat $${chr2}_1_$(TRIGGER_LEVEL).coverage_correct_triggered.rand.annote.$(ANNOT_RATIO_CUTOFF).sum.norm.distrib \
		| get_fasta $$j | grep -v -F '>' \
		>> $@.tmp.$$j; \
	done; \
	for i in `seq 1 7`; do \
		$(BIN_DIR)/annot_distrib_gnuplot $@ -l $$i -chr "$$chr1 $$chr2" > $@.tmp; \
		gnuplot $@.tmp > $@.$$i.ps; \
	done;
	rm -f $@.tmp.*
	touch $@

#contenuto di %.rand.annote.$(ANNOT_RATIO_CUTOFF).summary
# 1. label
# 2. media dei delle somme per ogni ciclo di randomizzazione
# 3. deviazione standard delle medie sui random
# 4. somma dei dati reali
# 5. (somma dati reali - media somma random) / deviazione standard somma random
%.rand.annote.$(ANNOT_RATIO_CUTOFF).summary: %.rand.annote.$(ANNOT_RATIO_CUTOFF) %.annote.$(ANNOT_RATIO_CUTOFF)
	labels='$(COORDS_NONREDUNDANT)'; \
	echo $$labels | sed -r 's/\s+/\t/g' > $@.tmp
	cut -f 4- $< | block_sum_stdev -b $(RAND_TRIAL) >> $@.tmp
	cut -f 4- $(word 2,$^) | sum_column >> $@.tmp
	cat $@.tmp | transpose \
		| awk 'BEGIN {OFS="\t"} {if ($$3!=0) {$$5=($$4-$$2)/$$3;} else {$$5="NaN";} print}' > $@
	rm $@.tmp

annote_vettorini_$(ANNOT_RATIO_CUTOFF).summary.summary: $(addsuffix _$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.rand.annote.$(ANNOT_RATIO_CUTOFF).summary, $(ALL_CHR))
	# parametri: AUTOCOVERAGE_TRIGGER_LEVEL=1 TRIGGER_LEVEL=50 ANNOT_RATIO_CUTOFF=0.6
	rm -f *.coverage_correct_triggered.rand.annote.$(ANNOT_RATIO_CUTOFF).summary.tmp
	for i in *.coverage_correct_triggered.rand.annote.$(ANNOT_RATIO_CUTOFF).summary; \
		do echo $${i%%.*} >$$i.tmp; cut -f 5 $$i >> $$i.tmp;  \
	done;
	paste *.coverage_correct_triggered.rand.annote.$(ANNOT_RATIO_CUTOFF).summary.tmp >$@.tmp
	echo '. $(COORDS_NONREDUNDANT)' | sed -r 's/\s+/\n/g' | paste - $@.tmp > $@
	rm $@.tmp

annote_vettorini_$(ANNOT_RATIO_CUTOFF).summary.summary.ps: annote_vettorini_$(ANNOT_RATIO_CUTOFF).summary.summary
	cat $< | transpose > $@.tmp
	$(BIN_DIR)/annotations_gnuplot $@.tmp | gnuplot > $@
	rm $@.tmp

annote_vettorini_$(ANNOT_LABEL).summary.summary.ps: $(addsuffix .summary.summary, $(addprefix annote_vettorini_, $(ALL_ANNOT_RATIO_CUTOFF)))
	head -n 1 annote_vettorini_$(ANNOT_RATIO_CUTOFF).summary.summary > $@.tmp
	for i in $(ALL_ANNOT_RATIO_CUTOFF); do \
		echo -ne "$$i\t" >> $@.tmp; \
		cat annote_vettorini_$${i}.summary.summary | awk '$$1 ~/^$(ANNOT_LABEL)$$/ {print}' | cut -f 2- >> $@.tmp; \
	done;
	cat $@.tmp | transpose > $@.aux;
	$(BIN_DIR)/annotations_gnuplot2 -l $(ANNOT_LABEL) -in $@.aux $(ALL_ANNOT_RATIO_CUTOFF) | gnuplot > $@
	rm $@.tmp
	rm $@.aux

########################### conservazione tra le specie con ANNOT_RATIO_CUTOFF #################################

#%.rand.cons_intersection.$(ANNOT_RATIO_CUTOFF): %.cons_intersection_$(ANNOT_RATIO_CUTOFF) $(ANNOTATIONS_CONS_FILE)
#	make $*.rand.cons_intersection


%.cons_intersection.$(ANNOT_RATIO_CUTOFF): %.cons_intersection
	cat $< \
	| awk 'BEGIN {OFS="\t"} { for (i=4;i<=NF;i++) {if ($$i>= $(ANNOT_RATIO_CUTOFF) ) {$$i=1} else {$$i=0}} print}' \
	> $@

#contenuto di %.rand.cons_intersection.$(ANNOT_RATIO_CUTOFF).summary
# 1. label
# 2. media dei delle somme per ogni ciclo di randomizzazione
# 3. deviazione standard delle medie sui random
# 4. somma dei dati reali
# 5. (somma dati reali - media somma random) / deviazione standard somma random
%.rand.cons_intersection.$(ANNOT_RATIO_CUTOFF).summary: %.rand.cons_intersection.$(ANNOT_RATIO_CUTOFF) %.cons_intersection.$(ANNOT_RATIO_CUTOFF)
	labels='$(TAXONOMY)'; \
	echo $$labels | sed -r 's/\s+/\t/g' > $@.tmp
	cut -f 4- $< | stdev_col >> $@.tmp
	cut -f 4- $(word 2,$^) | stdev_col | head -1 >> $@.tmp
	cat $@.tmp | transpose \
		| awk 'BEGIN {OFS="\t"} {if ($$3!=0) {$$5=($$4-$$2)/$$3;} else {$$5="NaN";} print}' > $@
	rm $@.tmp

cons_vettorini_$(ANNOT_RATIO_CUTOFF).summary.summary: $(addsuffix _$(AUTOCOVERAGE_TRIGGER_LEVEL)_$(TRIGGER_LEVEL).coverage_correct_triggered.rand.cons_intersection.$(ANNOT_RATIO_CUTOFF).summary, $(ALL_CHR))
	rm -f *.coverage_correct_triggered.rand.cons_intersection.$(ANNOT_RATIO_CUTOFF).summary.tmp
	for i in *.coverage_correct_triggered.rand.cons_intersection.$(ANNOT_RATIO_CUTOFF).summary; \
		do echo $${i%%.*} >$$i.tmp; cut -f 5 $$i >> $$i.tmp;  \
	done;
	paste *.coverage_correct_triggered.rand.cons_intersection.$(ANNOT_RATIO_CUTOFF).summary.tmp >$@.tmp
	echo '. $(TAXONOMY)' | sed -r 's/\s+/\n/g' | paste - $@.tmp > $@
	rm $@.tmp




###############################################
########## PER LA RETE TRA PICCHI #############
###############################################

regions.%.$(SCORE_CUTOFF).list:
	suffix='$*'; \
	suffix=$${suffix%%0.*}; \
	(cat *_$$suffix \
		| awk 'BEGIN {id=0} {id++; $$1=$$1 "," $$2 "," $$3 "\t" $$1; print id ":" $$1 "\t" $$2 "\t" $$3}') \
	| sort -k2,2 -k3,3n > $@

regions.%.list:
	(cat *_$* \
		| awk 'BEGIN {id=0} {id++; $$1=$$1 "," $$2 "," $$3 "\t" $$1; print id ":" $$1 "\t" $$2 "\t" $$3}') \
	| sort -k2,2 -k3,3n > $@

%.fa.xnd: %.fa
	xdformat -n $<; 

regions.%.fa: regions.%.list
	cat $< | seqlist2fasta $(SEQ_DIR) \
	> $@

