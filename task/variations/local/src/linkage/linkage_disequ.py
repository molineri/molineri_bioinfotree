#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
from collections import defaultdict
from vfork.io.colreader import Reader
from math import sqrt
from vfork.util import safe_import

with safe_import('RPy'):
	from rpy import r as R



def main():
	usage = format_usage('''
		%prog SNP_PAIR_FILE < STDIN

		.META: STDIN
			1	SNP_ID	rs2334386      
			2	PERSON_ID
			3	allele_1
			4	allele_2
			
	''')
	parser = OptionParser(usage=usage)
	
	#parser.add_option('-c', '--cutoff', type=float, dest='cutoff', default=0.05, help='some help CUTOFF [default: %default]', metavar='CUTOFF')
	#parser.add_option('-p', '--dump-params', dest='params_file', help='some help FILE [default: %default]', metavar='FILE')
	#parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='some help [default: %default]')

	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	snp_pairs = []
	considered_snp = set()
	with file(args[0]) as snp_pairs_filename:
		for s1,s2 in Reader(snp_pairs_filename, '0s,1s', False):
			snp_pairs.append((s1,s2))
			considered_snp.add(s1)
			considered_snp.add(s2)
			

	matrix = defaultdict(dict)
	for snp_id, person_id, allele_1, allele_2 in Reader(stdin, '0s,1s,2s,3s', False):
		if snp_id in considered_snp:
			if allele_1 != allele_2:
				continue #discarging eterozigous individuals
			allele = allele_1
			if allele == "N":
				continue #discarging untypizzed individuals
			matrix[snp_id][person_id]=allele
	
	for A,B in snp_pairs:
		A_data = matrix[A]
		B_data = matrix[B]
		n = 0
		count = defaultdict(lambda: defaultdict(int))
		marginalsA = defaultdict(int)
		marginalsB = defaultdict(int)
		for person_id in A_data.iterkeys():
			allele_A = None
			allele_B = None
			if person_id in B_data:
				n+=1
				allele_A = A_data[person_id]
				allele_B = B_data[person_id]
				marginalsA[allele_A]+=1
				marginalsB[allele_B]+=1
				count[allele_A][allele_B]+=1


		if n == 0:
			print >>stderr, "Warnings: no individuals typizzed for booth SNPs in the couple (%s,%s) discarding the couple" % (A,B)
			print "%s\t%s\t%s\t%s\t%d\t%s\t%s\t%s" % (A, B, allele_A, allele_B, n, "NA", "NA" , "NA")
			continue

		allele_A = count.keys()[0]
		allele_B = count[allele_A].keys()[0]
		
		if len(marginalsA.keys()) < 2:
			print >>stderr, "Warnings: only one omozigous type seen for %s (out of %d individuals) in the couple (%s,%s) discarding the couple" % (A,n,A,B)
			print "%s\t%s\t%s\t%s\t%d\t%s\t%s\t%s" % (A, B, allele_A, allele_B, n, "NA", "NA", "NA")
			continue
		if len(marginalsB.keys()) < 2:
			print >>stderr, "Warnings: only one omozigous type seen for %s (out of %d individuals) in the couple (%s,%s) discarding the couple" % (B,n,A,B)
			print "%s\t%s\t%s\t%s\t%s\t%s" % (A, B, allele_A, allele_B, "NA", "NA")
			continue

		n = float(n)
		#non importa quali alleli scelgo per calcolare D, sto prendendo gli ultimi visti
		if allele_A is None:
			print >>stderr, 'A_%s,B_%s\t%d\t%d' % (allele_A, allele_B, count[allele_A][allele_B], n)
		D = count[allele_A][allele_B]/n - marginalsA[allele_A]/n * marginalsB[allele_B]/n
			
		normalizzing_factor = marginalsA[allele_A]/n * (1 - marginalsA[allele_A]/n) * marginalsB[allele_B]/n * (1 - marginalsB[allele_B]/n)
		#if normalizzing_factor == 0:
		#	print >>stderr, (marginalsA[allele_A], marginalsA[allele_B] marginalsB[allele_B] marginalsB[allele_B]
		r_square = D*D / normalizzing_factor
		print "%s\t%s\t%s\t%s\t%d\t%f\t%f\t%g" % (A, B, allele_A, allele_B, n, D, r_square, R.pchisq(n*r_square, 1, 0, False, False))
		
		

if __name__ == '__main__':
	main()

