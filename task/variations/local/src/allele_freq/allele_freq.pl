#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] [-g glue]]\n
	STDIN example:

	.META: stdout

";

my $help=0;
GetOptions (
	'h|help' => \$help,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my @rows=();
my $header=undef;
while(<>){
	chomp;
	next if length==0 ;
	if(/^>/){
		s/^>//;
		my $post_header=$_;
		if(defined $header){
			compute_freq($header,\@rows);
			@rows=();
		}
		$header = $post_header;
	}else{
		chomp;
		push @rows, $_;
	}
}
compute_freq($header,\@rows);

sub compute_freq
{
	my $h = shift;
	my $rows_ref = shift;
	my @rows = @{$rows_ref};
	
	$h =~ m|([ACGT])/([ACGT])$|;
	die "malformed header ($h)" if not defined($1) or not defined($2); 

	my %a=();
	for(@rows){
		my @F = split /\t/;
		$a{$F[1]}++;
	}

	my $homo1      = defined $a{"$1$1"} ? $a{"$1$1"} : 0;
	my $homo2      = defined $a{"$2$2"} ? $a{"$2$2"} : 0;
	my $hetero     = defined $a{"$1$2"} ? $a{"$1$2"} : 0;
	   $hetero    += defined $a{"$2$1"} ? $a{"$2$1"} : 0;
	my $untypizzed = defined $a{ "NN" } ? $a{ "NN" } : 0;


	print "$h\t$homo1\t$homo2\t$hetero\t$untypizzed";
	die "malformed block associated with the following header ($h)" if $homo1 + $homo2 + $hetero + $untypizzed != scalar(@rows); 
}
