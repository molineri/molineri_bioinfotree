# Copyright 2010 Ivan Molineris <ivan.molineris@gmail.com>
# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>

ARRAY_PLATFORM ?= GenomeWideSNP_6
ANNOT_VERSION  ?= na30

ANNOT_FILE     ?= http://www.affymetrix.com/Auth/analysis/downloads/$(ANNOT_VERSION)/genotyping/$(ARRAY_PLATFORM).$(ANNOT_VERSION).annot.csv.zip


ALL += annot.gz


annot.zip:
	@echo -e "\n\n[ERROR] The following procedure does not work. You need to download manually the file from affymetrix after login."; \
	echo "wget -O $@ '$(ANNOT_FILE)'"; \
	exit 1

annot.gz.head: annot.gz
	@echo

annot.gz.meta: annot.gz
	@echo

annot.gz.readme: annot.zip
	unzip -op $< AFFX_README-NetAffx-CSV-Files.txt > $@

annot.gz: annot.zip
	unzip -op $< $(ARRAY_PLATFORM).$(ANNOT_VERSION).annot.csv\
	| bawk '/^#/ {print > "$@.head"} !/^#/ {print}' \
	| perl -lnpe 's/","/\t/g; s/^"//; s/"$$//' | unhead -f >(explain_first_row > $@.meta) \
	| gzip > $@

.META: annot.gz
	1	Probe_Set_ID
	2	dbSNP_RS_ID
	3	Chromosome
	4	Physical_Position
	5	Strand
	6	ChrX_pseudo_autosomal_region_1
	7	Cytoband
	8	Flank
	9	Allele_A
	10	Allele_B
	11	Associated_Gene
	12	Genetic_Map
	13	Microsatellite
	14	Fragment_Enzyme_Type_Length_Start_Stop
	15	Allele_Frequencies
	16	Heterozygous_Allele_Frequencies
	17	Number_of_individuals/Number_of_chromosomes
	18	In_Hapmap
	19	Strand_Versus_dbSNP
	20	Copy_Number_Variation
	21	Probe_Count
	22	ChrX_pseudo_autosomal_region_2
	23	In_Final_List
	24	Minor_Allele
	25	Minor_Allele_Frequency
	26	Percent_GC
	27	OMIM

allele_strand_converted.gz: annot.gz
	bawk '{print $$Probe_Set_ID, $$dbSNP_RS_ID, $$Strand, $$Allele_A, $$Allele_B}' $< \
	| perl -lane 'BEGIN{%a=("A","T", "C","G", "G","C", "T","A"); $$,="\t"} if($$F[2] eq "-"){ $$F[3]=$$a{$$F[3]};  $$F[4]=$$a{$$F[4]};} print @F;' \
	| gzip > $@
