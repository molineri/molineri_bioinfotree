# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>

SPECIES       ?= hsapiens
VERSION       ?= hg18
TRACK         ?= snp130
FLANKING_SIZE ?= 300

MYSQL_CMD     := mysql --user=genome --host=genome-mysql.cse.ucsc.edu -BCAN
SEQUENCE_DIR  ?= $(BIOINFO_ROOT)/task/sequences/dataset/ucsc/$(SPECIES)/$(VERSION)

param dna as CHR_NAMES from $(SEQUENCE_DIR)
extern $(addprefix $(SEQUENCE_DIR)/,$(addsuffix .fa,$(CHR_NAMES))) as ALL_CHRS


ALL   += summary.gz
CLEAN += flanking.gz

summary.gz:
	$(MYSQL_CMD) $(VERSION) <<<" \
		SELECT name, class, REPLACE(chrom, 'chr', ''), \
                       chromStart, chromEnd, CONCAT(strand, '1'), observed, func \
		FROM $(TRACK) "\
	| bsort -k3,3 -k4,4n -S20%\
	| gzip >$@

.META: summary.gz
	1  rsID       72477211
	2  class      single
	3  chr        1
	4  start      259
	5  stop       260
	6  strand     +1
	7  variation  A/G
	8  function

dbSNP_function.gz:
	 $(MYSQL_CMD) $(VERSION) <<<"SELECT name, func FROM $(TRACK)" \
	 | expandsets -s ',' 2 \
	 | gzip >$@
	
snp_in_refseq_exons.gz: snp_in_%_exons.gz: summary.gz $(BIOINFO_ROOT)/task/annotations/dataset/ucsc/hsapiens/$(VERSION)/%_genes.exons
	intersection \
		-l >(gzip > $@.missing.gz)\
		<(bawk '{ if($$4==$$5){$$5=$$5+1;} print}' $<):3:4:5 \			*insertions can have 0 len*
		$^2 \
	| cut -f 1,4- \
	| gzip >$@

.META: snp_in_*_exons.gz
	1	chr     1
	2	snp_b   4580
	3       snp_e	4581
	4       exon_b	4224
	5       exon_e	4692
	6       rsID	4021645
	7       snp_type	insertion
	8       strand	-1
	9       observed	-/GGA
	10	function	unknown
	11      geneID	NR_024540
	12      exon_type	5
	13      exon_strand	-1

.META: snp_in_*_exons.gz.missing.gz
	1  chr        1
	2  start      259
	3  stop       260
	4  rsID       72477211
	5  class      single
	6  strand     +1
	7  variation  A/G

flanking.gz: summary.gz $(ALL_CHRS)
	bawk '{print $$rsID, $$chr, $$start-$(FLANKING_SIZE), $$start; print $$rsID, $$chr, $$stop, $$stop+$(FLANKING_SIZE)}' $< \
	| seqlist2fasta -c $(SEQUENCE_DIR) \
	| fasta2oneline \
	| bawk 'NR%2==1 {label=$$1; seq5=$$2} \
		NR%2==0 {if($$1!=label) {print "Label mismatch" >"/dev/stderr"; exit 1}; \
		         print label,seq5,$$2}' \
	| gzip >$@

.META: flanking.gz
	1  rsID      72477211
	2  flank5  taacccta
	3  flank3  taacccta

gwasCatalog.gz:
	$(MYSQL_CMD) $(VERSION) <<<" \
		SELECT name, REPLACE(chrom, 'chr', ''), chromStart, chromEnd, \ 
			pubMedID, author, pubDate, journal, title, trait, initSample, replSample, region, genes, riskAllele, riskAlFreq, pValue, pValueDesc, orOrBeta, ci95, platform, cnv \
		FROM gwasCatalog "\
	| bsort -k2,2 -k3,3n -S20% \
	| gzip >$@

.META: gwasCatalog.gz
	1	rsID
	2	chr
	3	b
	4	e
	5	pubMedID
	6	author
	7	pubDate
	8	journal
	9	title
	10	trait
	11	initSample
	12	replSample
	13	region
	14	genes
	15	riskAllele
	16	riskAlFreq
	17	pValue
	18	pValueDesc
	19	orOrBeta
	20	ci95
	21	platform
	22	cnv

