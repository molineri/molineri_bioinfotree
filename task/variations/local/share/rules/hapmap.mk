# Copyright 2010 Ivan Molineris <ivan.molineris@gmail.com>
# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>

SPECIES            ?= hsapiens
UCSC_VERSION       ?= hg18
REMOTE_URL         ?= http://www.hapmap.org/genotypes/2008-01/fwd_strand/non-redundant
REMOTE_FILE_PREFIX ?= genotypes
REMOTE_FILE_SUFFIX ?= r23_nr.b36_fwd.txt.gz
POPULATIONS        ?= CEU CHB JPT YRI

SEQUENCE_DIR       ?= $(BIOINFO_ROOT)/task/sequences/dataset/ucsc/$(SPECIES)/$(UCSC_VERSION)

param dna as ALL_CHRS_RAW from $(SEQUENCE_DIR)
ALL_CHRS           := $(filter-out chrM,$(ALL_CHRS_RAW))
ALL_CLASSES        := $(foreach p,$(POPULATIONS),$(foreach c,$(ALL_CHRS),$p-$c))
ALL_RAW            := $(addsuffix .raw.gz,$(ALL_CLASSES))
ALL_META           := $(addsuffix .meta,$(ALL_CLASSES))
ALL_SNP            := $(addsuffix .snp,$(ALL_CLASSES))


ALL          += snp.gz
CLEAN        += $(ALL_META)
INTERMEDIATE += $(ALL_RAW) $(ALL_SNP)

$(ALL_RAW):
	IFS=".-"; \
	t="$@"; \
	ps=( $$t ); \
	wget -O$@ $(REMOTE_URL)/$(REMOTE_FILE_PREFIX)_$${ps[1]}_$${ps[0]}_$(REMOTE_FILE_SUFFIX)

$(ALL_META): %.meta: %.raw.gz
	(zcat $< \
	| tr " " "\t" \
	| perl -lane 'BEGIN{$$,="\t"}; my @G=(); push @G, $$F[0]; push @G, $$F[2]; push @G, $$F[3]; push @G, "$*"; push @G,$$F[1]; push @G, splice(@F, 12); print @G'; \
	exit 0) \   * ignore SIGPIPE caused by explain_first_row *
	| explain_first_row >$@

$(ALL_SNP): %.snp: %.raw.gz %.meta
	zcat $< \
	| tr " " "\t" \
	| unhead \
	| perl -lane 'BEGIN{$$,="\t"}; my @G=(); push @G, $$F[0]; push @G, $$F[2]; push @G, $$F[3]; push @G, "$*"; push @G,$$F[1]; push @G, splice(@F, 12); print @G' \
	| sed 's/chr//' \
	| explain_first_row -a \
	| repeat_fasta_pl 'print ">", join("\t",map {$$_=~s/^\d\t//; $$_} splice(@rows,0,5)); print "\n"; print join "\n", @rows; print "\n"' \
	| unhead -n 2 \
	| translate -p '^>' $^2 1 >$@

snp.gz: $(ALL_SNP)
	cat $^ \
	| gzip >$@

.META: snp.gz
	1	rs#		rs1	
	2	chrom           22
	3	pos             144
	4	panel		CEU
	5	SNPalleles      C/G
	6	NA06985         CG
	7	NA06991         NN
	8	NA06993         CG
	9	NA06994         GG
	10	NA07000         GG
	11	NA07019         GG
	12	NA07022         GG
	13	NA07029         CG
	14	NA07034         GG
	15	NA07048         GG
	16	NA07055         CG
	17	NA07056         CG
	18	NA07345         CG
	19	NA07348         CG
	20	NA07357         GG
	21	NA10830         NN
	22	NA10831         CG
	23	NA10835         CG
	24	NA10838         GG
	25	NA10839         GG
	26	NA10846         CG
	27	NA10847         CG
	28	NA10851         GG
	29	NA10854         CG
	30	NA10855         GG
	31	NA10856         CG
	32	NA10857         CG
	33	NA10859         NN
	34	NA10860         CG
	35	NA10861         CG
	36	NA10863         CG
	37	NA11829         GG
	38	NA11830         GG
	39	NA11831         CG
	40	NA11832         GG
	41	NA11839         CG
	42	NA11840         CG
	43	NA11881         GG
	44	NA11882         NN
	45	NA11992         CG
	46	NA11993         GG
	47	NA11994         CG
	48	NA11995         CG
	49	NA12003         CG
	50	NA12004         GG
	51	NA12005         CG
	52	NA12006         GG
	53	NA12043         CG
	54	NA12044         GG
	55	NA12056         CG
	56	NA12057         GG
	57	NA12144         GG
	58	NA12145         GG
	59	NA12146         CG
	60	NA12154         CG
	61	NA12155         CG
	62	NA12156         GG
	63	NA12234         CG
	64	NA12236         CG
	65	NA12239         GG
	66	NA12248         NN
	67	NA12249         NN
	68	NA12264         CG
	69	NA12707         CG
	70	NA12716         CG
	71	NA12717         CG
	72	NA12740         GG
	73	NA12750         NN
	74	NA12751         CG
	75	NA12752         CG
	76	NA12753         CG
	77	NA12760         GG
	78	NA12761         GG
	79	NA12762         CG
	80	NA12763         CG
	81	NA12801         CG
	82	NA12802         CG
	83	NA12812         NN
	84	NA12813         CG
	85	NA12814         CG
	86	NA12815         CG
	87	NA12864         GG
	88	NA12865         CG
	89	NA12872         CG
	90	NA12873         GG
	91	NA12874         GG
	92	NA12875         GG
	93	NA12878         CG
	94	NA12891         GG
	95	NA12892         GG

snp_freq-population.gz: snp.gz
	zcat $< | allele_freq \
	| gzip > $@

.META: snp_freq.gz
	1	SNP_ID	rs2334386
	2	chr	22
	3	pos	14430353
	4	population	ASW	
	5	alleles	G/T
	6	AA	78	homozigous1 count
	7	BB	0	homozigous2 count
	8	AB	4	eteroziguns count
	9	NN	0	number of non typizzed persons

snp_freq.gz: snp.gz
	zcat $<\
	| fasta2tab \
	| bawk '{print $$1 ";" $$2 ";" $$3 ";" $$5, $$6, $$7}' \
	| bsort -k 1,1 -k2,2 -S40% \
	| tab2fasta \
	| tr ";" "\t" \
	| allele_freq \
	| gzip >$@

.META: snp_freq.gz
	1	SNP_ID	rs2334386
	2	chr	22
	3	pos	14430353
	4	alleles	G/T
	5	AA	78	homozigous1 count
	6	BB	0	homozigous2 count
	7	AB	4	eteroziguns count
	8	NN	0	number of non typizzed persons
