gwascatalog.gz:
	wget -O - http://www.genome.gov/admin/gwascatalog.txt \
	| unhead \
	| gzip > $@

.DOC: gwascatalog.gz
	coordinate in hg19
	altre info presso http://www.genome.gov/Pages/About/OD/OPG/GWAS%20Catalog/Tab_Delimited_Heading_Descriptions_2011-08-04.pdf 
	e http://www.genome.gov/gwastudies/

.META: gwascatalog.gz
	1	Date_Added_to_Catalog
	2	pubMedID
	3	First_Author
	4	Date
	5	Journal
	6	Link
	7	Study
	8	trait
	9	Initial_Sample_Size
	10	Replication_Sample_Size
	11	Region
	12	Chr_id
	13	Chr_pos
	14	Reported_Genes
	15	Mapped_gene
	16	Upstream_gene_id
	17	Downstream_gene_id
	18	Snp_gene_ids
	19	Upstream_gene_distance
	20	Downstream_gene_distance
	21	Strongest_SNP-Risk_Allele
	22	rsID
	23	Merged
	24	Snp_id_current
	25	Context
	26	Intergenic
	27	Risk_Allele_Frequency
	28	p-Value
	29	Pvalue_mlog
	30	p-Value
	31	OR_or_beta
	32	95perc_CI
	33	Platform
	34	CNV

