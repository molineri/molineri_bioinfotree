NON_FUNCTIONAL_CLINICALVARAINTS = Benign Likely_benign Uncertain_significance not_provided other

variant_summary.raw.gz:
	wget -O $@ ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/tab_delimited/variant_summary.txt.gz


.META: variant_summary.raw.gz
	1	AlleleID
	2	Type
	3	Name
	4	GeneID
	5	GeneSymbol
	6	ClinicalSignificance
	7	RS			(dbSNP)
	8	nsv			(dbVar)
	9	RCVaccession
	10	TestedInGTR
	11	PhenotypeIDs
	12	Origin
	13	Assembly
	14	Chromosome
	15	Start
	16	Stop
	17	Cytogenetic
	18	ReviewStatus
	19	HGVSc
	20	HGVSp
	21	NumberSubmitters
	22	LastEvaluated
	23	Guidelines
	24	OtherIDs
	25	VariantID


ClinicalSignificance.expanded.gz: variant_summary.raw.gz
	bawk '$$Chromosome {print $$Chromosome,$$Start,$$Stop,$$ClinicalSignificance, $$AlleleID, $$Type}' $<\
	| expandsets 4 \
	| unhead \
	| tr " " "_" \
	| gzip >$@

.META: ClinicalSignificance.expanded.gz functional_variants.gz
	1	chr
	2	b
	3	e
	4	ClinicalSignificance
	5	AlleleID
	6	Type

functional_variants.gz: ClinicalSignificance.expanded.gz
	zcat $< | filter_1col -v 4 <(tr " " "\n" <<<"$(NON_FUNCTIONAL_CLINICALVARAINTS)") \
	| bawk '{if($$2<$$3){a=$$2; $$3=$$3; $$3=a} if($$3==$$2){$$3++}}' | gzip > $@
	
.META: functional_variants.gz
	1	chr	1
	2	b	564405
	3	e	8597804
	4	function	Pathogenic
	5	id	72837
	6	type	copy_number_loss

