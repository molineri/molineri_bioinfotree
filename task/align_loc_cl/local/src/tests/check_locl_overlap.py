#!/usr/bin/env python
from __future__ import division
from optparse import OptionParser
from sys import exit, stdin, stderr

def load(fd):
	records = []

	for line in fd:
		if line[0] != '>':
			continue

		record = tuple(int(t) for t in line[1:].split('_'))
		assert len(record) == 4
		records.append(record)
	
	return records

def bbox(records):
	assert len(records) >= 1

	x_min, x_max, y_min, y_max = records[0]
	for x1, x2, y1, y2 in records[1:]:
		if x1 < x_min:
			x_min = x1
		if x2 > x_max:
			x_max = x2
		if y1 < y_min:
			y_min = y1
		if y2 > y_max:
			y_max = y2
	
	return x_min, x_max, y_min, y_max

#def split_point(records):
#	assert len(records) >= 2
#
#	x_min, x_max, y_min, y_max = bbox(records)
#	return (x_max+x_min)//2, (y_max+y_min)//2

def split_point(records):
	n = len(records)
	assert n >= 2

	return sum(r[0]+r[1] for r in records) // (2*n), \
	       sum(r[2]+r[3] for r in records) // (2*n)

def overlap(r1, r2):
	return not ((r1[1] <= r2[0] or r2[1] <= r1[0]) or \
	            (r1[3] <= r2[2] or r2[3] <= r1[2]))

class Split(object):
	__slots__ = ('q1', 'q2', 'q3', 'q4')

	def __init__(self, records):
		self.q1 = []
		self.q2 = []
		self.q3 = []
		self.q4 = []
		self._assign(self.q1, self.q2, self.q3, self.q4, records)
	
	def __len__(self):
		return len(self.q1) + len(self.q2) + len(self.q3) + len(self.q4)
	
	def _assign(self, q1, q2, q3, q4, records):
		xs, ys = split_point(records)
		q1_coords = (0, xs, 0, ys)
		q2_coords = (xs, 0xffffffff, 0, ys)
		q3_coords = (0, xs, ys, 0xffffffff)
		q4_coords = (xs, 0xffffffff, ys, 0xffffffff)

		for record in records:
			if overlap(record, q1_coords):
				q1.append(record)
			if overlap(record, q2_coords):
				q2.append(record)
			if overlap(record, q3_coords):
				q3.append(record)
			if overlap(record, q4_coords):
				q4.append(record)

def recursive_split(records):
	if isinstance(records, Split):
		records.q1, c1 = recursive_split(records.q1)
		records.q2, c2 = recursive_split(records.q2)
		records.q3, c3 = recursive_split(records.q3)
		records.q4, c4 = recursive_split(records.q4)
		return records, c1+c2+c3+c4
	elif len(records) > 100:
		split = Split(records)
		return split, len(split)-len(records)
	else:
		return records, 0

def max_count(records):
	if isinstance(records, Split):
		return max(max_count(records.q1), max_count(records.q2), \
		           max_count(records.q3), max_count(records.q3))
	else:
		return len(records)

def count_nonempty_splits(records):
	if isinstance(records, Split):
		return count_nonempty_splits(records.q1) + \
		       count_nonempty_splits(records.q2) + \
		       count_nonempty_splits(records.q3) + \
		       count_nonempty_splits(records.q4)
	elif len(records):
		return 1
	else:
		return 0

def check_overlap(records, min_gap, verbose):
	if isinstance(records, Split):
		check_overlap(records.q1, min_gap, verbose)
		check_overlap(records.q2, min_gap, verbose)
		check_overlap(records.q3, min_gap, verbose)
		check_overlap(records.q4, min_gap, verbose)
	
	elif len(records) > 1:
		n = len(records)
		for i in xrange(n-1):
			r = (records[i][0]-min_gap, records[i][1]+min_gap, \
			     records[i][2]-min_gap, records[i][3]+min_gap)

			for j in xrange(i+1,n):
				if overlap(r, records[j]):
					print 'Found two location clusters overlapping:'
					print '\t%d\t%d\t%d\t%d' % records[i]
					print '\t%d\t%d\t%d\t%d' % records[j]
					exit(1)

		if verbose:
			stderr.write('.')

def main():
	parser = OptionParser(usage='%prog MIN_GAP <LOC_CL')
	parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='print verbose messages')
	options, args = parser.parse_args()

	if len(args) != 1:
		exit('Unexpected argument number.')

	try:
		min_gap = int(args[0])
		if min_gap < 0:
			raise ValueError
	except ValueError:
		exit('Invalid minimum gap value: %s' % args[0])

	records = load(stdin)
	if options.verbose:
		print >>stderr, 'Loaded %d records.' % len(records)

	i = 0
	s = records
	while True:
		s, c = recursive_split(s)
		mc = max_count(s)
		if options.verbose:
			print >>stderr, 'Split level %d: biggest split %d, overhead %d.' % (i, mc, c)

		if mc < 1000 or i > 16:
			break
		
		i += 1
	
	if options.verbose:
		print 'Records distributed into %d splits.' % count_nonempty_splits(s)

	check_overlap(s, min_gap, options.verbose)
	if options.verbose:
		stderr.write('\n')

if __name__ == '__main__':
	main()

