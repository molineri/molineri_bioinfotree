#!/usr/bin/env python
# encoding: utf8

from cStringIO import StringIO
from optparse import OptionParser
from sys import exit, stdin
from vfork.fasta.reader import MultipleBlockStreamingReader

def parse_header(header):
	tokens = header.split('_')
	assert len(tokens) == 4, 'malformed header: ' + header
	return [ int(d) for d in tokens ]

def check_bounding_box(header, hsps):
	expected_bbox = parse_header(header)
	bbox = None

	for hsp in hsps:
		tokens = hsp.rstrip().split('\t')
		assert len(tokens) >= 7, 'malformed row: ' + hsp
		coords = [ int(tokens[i]) for i in (1, 2, 5, 6) ]
		
		if bbox is None:
			bbox = coords
		else:
			if bbox[0] > coords[0]:
				bbox[0] = coords[0]
			if bbox[1] < coords[1]:
				bbox[1] = coords[1]
			if bbox[2] > coords[2]:
				bbox[2] = coords[2]
			if bbox[3] < coords[3]:
				bbox[3] = coords[3]
	
	if bbox != expected_bbox:
		exit('Invalid bounding box for locl %s: expected %s, found %s.' % (header, repr(bbox), repr(expected_bbox)))

def main():
	parser = OptionParser(usage='%prog <LOC_CL')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	reader = MultipleBlockStreamingReader(stdin, False)
	for header, hsps in reader:
		check_bounding_box(header, hsps)

if __name__ == '__main__':
	main()

