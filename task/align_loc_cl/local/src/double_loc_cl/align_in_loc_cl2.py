#!/usr/bin/env python
from __future__ import with_statement

from optparse import OptionParser
from sys import exit, stdin, stdout
from vfork.io.colreader import Reader

def load_locls(filename):
	with file(filename, 'r') as fd:
		r = Reader(fd, '0u,1u,2u,3u', False)
		locls = []
		last_x = 0

		while True:
			tokens = r.readline()
			if tokens is None:
				break
			elif tokens[0] < last_x:
				exit('Disorder in location clusters.')
			else:
				last_x = tokens[0]
				locls.append(tokens)
	
	return locls

def bisect(coords, value):
	l = len(coords)
	i = 0
	j = l
	while True:
		d = max((j-1)//2, 1)
		


def by_range(starts, ends, b, e):
	paass
	l = len(locls) 
	i = min_idx
	j = l
	while True:
		d = max( (j - i)//2, 1)
		idx = i + d
		print i,j,idx,locls[idx],b,e
		if locls[idx][1] < b :
			i = idx
			assert i < l
		elif locls[idx][0] > e:
			j = idx
			assert j >= min_idx
		else:
			break
	
	res = set()
	res.add(locls[idx][2])

	i = idx -1
	while i >= min_idx:
		if locls[i][1] > b:
			res.add(locls[i][2])
		i -= 1

	i = idx +1
	while i < l and locls[i][0] < e:
		res.add(locls[i][2])
		i += 1

	return res



def print_locl(locl, hsps):
	if len(hsps) == 0:
		exit('Found an empty location cluster.')
	
	print '>%s' % ('_'.join(str(c) for c in locl))
	for hsp in hsps:
		stdout.write(hsp)

def main():
	parser = OptionParser(usage='$prog LOCL_COORDS <HSPS')
	options, args = parser.parse_args()

	if len(args) != 1:
		exit('Unexpected argument number.')
	
	locls = load_locls(args[0])
	locls_x1 = [ (l[0],i) for i,l in enumerate(locls) ]
	locls_x2 = [ (l[1],i) for i,l in enumerate(locls) ]
	locls_y1 = [ (l[2],i) for i,l in enumerate(locls) ]
	locls_y2 = [ (l[3],i) for i,l in enumerate(locls) ]
	locls_x2.sort()
	locls_y1.sort()
	locls_y2.sort()

	hsps = [[]] * len(locls)

	r = Reader(stdin, '1u,2u,5u,6u,a', False)
	
	min_idx = 0
	last_x = 0
	while True:
		tokens = r.readline()
		if tokens is None:
			break
		else:
			if tokens[0] < last_x:
				exit('Disorder in HSPs.')
			else:
				last_x = tokens[0]
			
			while True:
				if min_idx >= len(locls):
					exit('Found an HSP outside every location cluster.')
				elif locls[min_idx][1] < tokens[0]:
					print_locl(locls[min_idx], hsps[min_idx])
					hsps[min_idx] = None
					min_idx += 1
				else:
					break
			hits_x = by_range(locls_x1, locls_x2, tokens[0], tokens[1])
			hits_y = by_range(locls_y1, locls_y2, tokens[2], tokens[3])
			hits = hits_x & hits_y
			if len(hits) != 1:
				print len(hits),hits_x,hits_y,tokens[4]
				for h in hits:
					print locls[h], tokens[4]

			assert len(hits) == 1
			hsps[hits.pop()].append(tokens[4])
				
			
	for idx in xrange(min_idx, len(locls)):
		print_locl(locls[idx], hsps[idx])

if __name__ == '__main__':
	main()
