#!/usr/bin/env python
from __future__ import with_statement

from optparse import OptionParser
from sys import exit, stdin, stdout
from vfork.io.colreader import Reader

def load_locls(filename):
	with file(filename, 'r') as fd:
		r = Reader(fd, '0u,1u,2u,3u', False)
		locls = []
		last_x = 0

		while True:
			tokens = r.readline()
			if tokens is None:
				break
			elif tokens[0] < last_x:
				exit('Disorder in location clusters.')
			else:
				last_x = tokens[0]
				locls.append(tokens)
	
	return locls

def print_locl(locl, hsps):
	if len(hsps) == 0:
		exit('Found an empty location cluster.')
	
	print '>%s' % ('_'.join(str(c) for c in locl))
	for hsp in hsps:
		stdout.write(hsp)

def main():
	parser = OptionParser(usage='$prog LOCL_COORDS <HSPS')
	options, args = parser.parse_args()

	if len(args) != 1:
		exit('Unexpected argument number.')
	
	locls = load_locls(args[0])
	hsps = [[]] * len(locls)
	r = Reader(stdin, '1u,2u,5u,6u,a', False)
	min_idx = 0
	last_x = 0
	while True:
		tokens = r.readline()
		if tokens is None:
			break
		else:
			if tokens[0] < last_x:
				exit('Disorder in HSPs.')
			else:
				last_x = tokens[0]
			
			while True:
				if min_idx >= len(locls):
					exit('Found an HSP outside every location cluster.')
				elif locls[min_idx][1] < tokens[0]:
					print_locl(locls[min_idx], hsps[min_idx])
					hsps[min_idx] = None
					min_idx += 1
				else:
					break
			
			idx = min_idx
			for idx in xrange(min_idx, len(locls)):
				locl = locls[idx]
				if tokens[2] >= locl[2] and tokens[3] <= locl[3]:
					hsps[idx].append(tokens[4])
					break
			else:
				exit('Found an HSP outside every location cluster.')
	
	for idx in xrange(min_idx, len(locls)):
		print_locl(locls[idx], hsps[idx])

if __name__ == '__main__':
	main()
