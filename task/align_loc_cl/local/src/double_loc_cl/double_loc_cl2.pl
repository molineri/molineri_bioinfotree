#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
use constant ALIGN_SX_B    => 1;
use constant ALIGN_SX_E    => 2;
use constant ALIGN_STRAND  => 3;
use constant ALIGN_DX_B    => 5;
use constant ALIGN_DX_E    => 6;
use constant ALIGN_LENGTH  => 7;
use constant ALIGN_SCORE   => 8;
use constant ALIGN_SX_GAPS => 10;
use constant ALIGN_DX_GAPS => 11;


my $usage="$0 [-min_score 35] [-gap 22000] [-min_align_in_loc_clust 3] per_database_file\n
	make location cluster in 2D
";

my $min_score=0;
my $loc_clust_gap=22000;
my $verbose=0;
my $min_align_in_loc_clust=1;

GetOptions (
	'min_score|s=i' => \$min_score,
	'gap|g=i' => \$loc_clust_gap,
	'verbose|v' => \$verbose,
	'min_align_in_loc_clust|m=i' => \$min_align_in_loc_clust,
);


my %loc_clust;
my $i=-1;
my $test_x=0;
while(<>){
	
	
	printf STDERR "input row parsed: %8i\r",$. if($verbose and $.%1000==0);

	
	chomp;
	my @F=split;
	#2,3,4,6,7,10
	
	die("input must be sorted on second column\n") if($test_x>$F[1]);
	$test_x=$F[ALIGN_SX_B];
	
	if($min_score){
		next if($F[ALIGN_SCORE]<=$min_score);
	}
	
	my $first=1;
	
	my @to_merge=();
	foreach my $key (keys(%loc_clust)){
		
		my $lc=$loc_clust{$key};
		
		if(
			$F[ALIGN_SX_E] < $lc->{x_min} - $loc_clust_gap
				or
			$F[ALIGN_DX_B] < $lc->{y_min} - $loc_clust_gap
				or
			$F[ALIGN_DX_E] > $lc->{y_max} + $loc_clust_gap
		){
			next;
		}elsif(	$F[ALIGN_SX_B] > $lc->{x_max} + $loc_clust_gap ){
			&print_lc(delete($loc_clust{$key}));
			next;
		}elsif($first){
			&add_to_loc_clust($key,\@F);
			push(@to_merge,$key);
			$first=0;
		}else{
			push(@to_merge,$key);
		}
	}
	
	if($first){
		&new_loc_clust(@F);
	}elsif(scalar(@to_merge)>1){
		&merge_loc_clust(@to_merge);
	}
}


for(values(%loc_clust)){
	&print_lc($_);
}



	
sub print_lc
{
	my $lc=shift(@_);
	my @items=@{$lc->{items}};
	if($verbose and scalar(@items)>=$min_align_in_loc_clust){
		print STDERR "\nfound lc, items: ".scalar(@items)."\n";
	}
	if(scalar(@items)>=$min_align_in_loc_clust){
		print ">$lc->{x_min}_$lc->{x_max}_$lc->{y_min}_$lc->{y_max}\n";
		for(@items){
			print join("\t",@{$_}),"\n";
		}
	}
}

sub add_to_loc_clust
{
	
	my $idx=$_[0];
	my @F=@{$_[1]};
	
	my $loc_clust_R=$loc_clust{$idx};
	

	$loc_clust_R->{x_min} = $F[ALIGN_SX_B] if $loc_clust_R->{x_min} > $F[ALIGN_SX_B];
	$loc_clust_R->{x_max} = $F[ALIGN_SX_E] if $loc_clust_R->{x_max} < $F[ALIGN_SX_E];
	
	$loc_clust_R->{y_min} = $F[ALIGN_DX_B] if $loc_clust_R->{y_min} > $F[ALIGN_DX_B];
	$loc_clust_R->{y_max} = $F[ALIGN_DX_E] if $loc_clust_R->{y_max} < $F[ALIGN_DX_E];

	my @tmp = @F;
	if($F[ALIGN_STRAND] eq '+'){
	} else {
		$tmp[ALIGN_DX_B] = $F[ALIGN_DX_E];
		$tmp[ALIGN_DX_E] = $F[ALIGN_DX_B];
	}
	
	push @{$loc_clust_R->{items}},\@tmp;
}

sub new_loc_clust
{
	$i++;
	my %tmp=();
	$tmp{x_min}=$_[ALIGN_SX_B];
	$tmp{x_max}=$_[ALIGN_SX_E];
	$tmp{y_min}=$_[ALIGN_DX_B];
	$tmp{y_max}=$_[ALIGN_DX_E];
	
	my @item = @_;
	if($_[ALIGN_STRAND] eq '+'){
	} else {
		$item[ALIGN_DX_B] = $_[ALIGN_DX_E];
		$item[ALIGN_DX_E] = $_[ALIGN_DX_B];
	}
	
	$tmp{items}[0]=\@item;
	$loc_clust{$i}=\%tmp;
}

sub merge_loc_clust{
	my $first_lc_k=shift(@_);
	for(@_){
		&merge_couple($first_lc_k,$_);
	}
}

sub merge_couple
{
	my $lc1k=shift;
	my $lc2k=shift;
	my $lc1=$loc_clust{$lc1k};
	my $lc2=$loc_clust{$lc2k};

	$lc1->{x_min} = $lc2->{x_min} if $lc1->{x_min} > $lc2->{x_min};
	$lc1->{y_min} = $lc2->{y_min} if $lc1->{y_min} > $lc2->{y_min};
	
	$lc1->{x_max} = $lc2->{x_max} if $lc1->{x_max} < $lc2->{x_max};
	$lc1->{y_max} = $lc2->{y_max} if $lc1->{y_max} < $lc2->{y_max};
	
	push @{$lc1->{items}},@{$lc2->{items}};
	
	delete($loc_clust{$lc2k});
}
