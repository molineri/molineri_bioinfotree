#!/usr/bin/perl 

use strict;
use warnings;
use constant LEFT_B_COL  => 1;
use constant LEFT_E_COL  => 2;
use constant RIGHT_B_COL => 5;
use constant RIGHT_E_COL => 6;
use Getopt::Long;


$,="\t";
$\="\n";

my $usage = "cat alignments_b1_e1_b2_e2 | $0 -l loc_cl_header_file";

my $loc_cl_file = undef;

GetOptions (
	'input_file|l=s' => \$loc_cl_file,
) or die ($usage);


open LOC_CL,$loc_cl_file or die ("Can't open file ($loc_cl_file!)\n"); 

my $old_b = undef;
my @loc_cl_headers = ();

while (<LOC_CL>) {
	chomp;

	die "ERROR: $0: location clusters not in tab delimited format b1\\te1\\tb2\\te2" if ($_ =~ /^>/ or $_ =~ /_/);
	my @F_loc_cl = split /\t/,$_;

	die ("ERROR: $0: stop <= start") if (!($F_loc_cl[1] > $F_loc_cl[0]) or !($F_loc_cl[3] > $F_loc_cl[2]));
	die "ERROR: $0: file $loc_cl_file not sorted on the left_start\n" if ( (defined $old_b) and ($F_loc_cl[0] < $old_b) );
	$old_b = $F_loc_cl[0];

	push @loc_cl_headers,\@F_loc_cl;
}
my $l = scalar @loc_cl_headers;



my @align_loc_cl = ();
$old_b = undef;
my $last_idx = undef;

while (<>) {
	chomp;
	my @F_align = split /\t/,$_;

	die "ERROR: $0: input file not sorted on the left_start\n" if ( (defined $old_b) and ($F_align[LEFT_B_COL] < $old_b) );
	$old_b = $F_align[LEFT_B_COL];

	die ("ERROR: $0: stop <= start") if ( 
		!($F_align[LEFT_E_COL] > $F_align[LEFT_B_COL]) 
			or 
		!($F_align[RIGHT_E_COL] > $F_align[RIGHT_B_COL]) );

	&print_loc_cl($last_idx) if ( (defined $last_idx and $F_align[LEFT_E_COL] > ${$align_loc_cl[$last_idx]}[0]) );

	my $last_idx = &insert_in_loc_cl(\@F_align);
	die "ERROR: $0: alignment not inserted in location cluster\n".join "\t",@F_align if ($last_idx == - 1);
}


&print_all;



sub insert_in_loc_cl
{
	my $line_ref = shift;

	my @line = @{$line_ref};

	for (my $i=0; $i<$l; $i++) {
		my ($b1,$e1,$b2,$e2) = @{$loc_cl_headers[$i]};

		next if ($e1 < $line[LEFT_B_COL]);
		next if ($line[LEFT_E_COL] < $b1);

		next if ($e2 < $line[RIGHT_B_COL]);
		next if ($line[RIGHT_E_COL] < $b2);

		push @{$align_loc_cl[$i]}, \@line;

		return $i;
	}

	return -1;
}


sub print_all
{
	for (my $i=0; $i<$l; $i++) {
		my $header = '>'.join '_',@{$loc_cl_headers[$i]};
		print $header;
		foreach my $el (@{$align_loc_cl[$i]}) {
			my @aux = @{$el};
			print @aux;
		}
	}
}


sub print_loc_cl
{
	my $i = shift;

	for (my $j=0; $j<=$i; $j++) {
		my $loc_cl_ref = shift @loc_cl_headers;
		my @loc_cl = @{$loc_cl_ref};
		my $align_loc_cl_ref = shift @align_loc_cl;
		my @align_loc_cl_out = @{$align_loc_cl_ref};
		my $header = '>'.join '_',@loc_cl;
		print $header;
		foreach my $el (@align_loc_cl_out) {
			my @aux = @{$el};
			print @aux;
		}
	}
	$l = scalar @loc_cl_headers;
}
