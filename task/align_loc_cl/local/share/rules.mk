ALIGN_DIR  ?= $(BIOINFO_ROOT)/task/alignments/inhouse/dataset/mouse_mouse_ensembl40_wublast
SPECIES1   ?= mmusculus
SPECIES2   ?= mmusculus
CHRS1      ?= 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 X Y
CHRS2      ?= 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 X Y
ALIGN_SCORE_MIN         ?= 0
LOC_CLUST_MIN_ALIGN_NUM ?= 1
LOC_CLUST_MAX_GAP       ?= 22000

.PHONY: all_accoda
.DELETE_ON_ERROR:

define list_pairs
	set -o pipefail;
	if [ $(SPECIES1) == $(SPECIES2) ]; then
		chrs=( $(CHRS1) );
		for ((i=0; i<$${#chrs[*]}; i=$$i+1)); do
			for ((j=i; j<$${#chrs[*]}; j=$$j+1)); do
				echo "chr$${chrs[$$i]}_chr$${chrs[$$j]}";
			done
		done
	else
		for chr1 in $(CHRS1); do
			for chr2 in $(CHRS2); do
				echo "chr$${chr1}_chr$${chr2}";
			done
		done
	fi
endef

BIN_DIR := $(BIOINFO_ROOT)/task/align_loc_cl/local/bin
ALL_CHR_PAIRS := $(shell $(list_pairs))

all: $(addsuffix .loc_cl.gz,$(ALL_CHR_PAIRS))

all_accoda:
	for i in $(addsuffix .loc_cl.gz,$(ALL_CHR_PAIRS)); do \
		accoda --name $${i%%.*} -- make $$i; \
	done

#vecchia regola
#.SECONDEXPANSION:
#%.loc_cl.gz: $$(or $$(wildcard $(ALIGN_DIR)/$(SPECIES1)_$$(word 1,$$(subst _, ,$$(word 1,$$(subst ., ,$$@))))_$(SPECIES2)_$$(word 2,$$(subst _, ,$$(word 1,$$(subst ., ,$$@)))).*.pdb.gz),alignments_missing)
#	set -o pipefail; \
#	set -e; \
#	zcat $< \
#	| awk '$$9>$(ALIGN_SCORE_MIN)' \
#	| $(BIN_DIR)/double_loc_cl -g $(LOC_CLUST_MAX_GAP) -s $(ALIGN_SCORE_MIN) -m $(LOC_CLUST_MIN_ALIGN_NUM) \
#	| gzip >$@

#seconda vecchia regola
#.SECONDEXPANSION:
#%.loc_cl.gz: $$(or $$(wildcard $(ALIGN_DIR)/$(SPECIES1)_$$(word 1,$$(subst _, ,$$(word 1,$$(subst ., ,$$@))))_$(SPECIES2)_$$(word 2,$$(subst _, ,$$(word 1,$$(subst ., ,$$@)))).*.pdb.gz),alignments_missing)
#	set -o pipefail; \
#	set -e; \
#	zcat $< \
#	| awk '$$9>=$(ALIGN_SCORE_MIN)' | cut -f 2,3,6,7 | sort -k1,1n \
#	| $(BIN_DIR)/loc_cl_fusion -d $(LOC_CLUST_MAX_GAP) | sort -k1,1n \
#	> $@.tmp; \
#	zcat $< \
#	| awk '$$9>=$(ALIGN_SCORE_MIN)' | sort -k2,2n \
#	| $(BIN_DIR)/align_in_loc_cl -l $@.tmp \
#	| gzip > $@; \
#	rm $@.tmp

.SECONDEXPANSION:
%.loc_cl.clusters: $$(or $$(wildcard $(ALIGN_DIR)/$(SPECIES1)_$$(word 1,$$(subst _, ,$$(word 1,$$(subst ., ,$$@))))_$(SPECIES2)_$$(word 2,$$(subst _, ,$$(word 1,$$(subst ., ,$$@)))).*.pdb.gz),alignments_missing)
	set -o pipefail; \
	set -e; \
	zcat $< \
	| awk '$$9>=$(ALIGN_SCORE_MIN)' \
	| cut -f 2,3,6,7 \
	| $(BIN_DIR)/locl_fusion -g $(LOC_CLUST_MAX_GAP) \
	| sort -k1,1n -k2,2n -k3,3n -k4,4n >$@


%.loc_cl.gz: $$(or $$(wildcard $(ALIGN_DIR)/$(SPECIES1)_$$(word 1,$$(subst _, ,$$(word 1,$$(subst ., ,$$@))))_$(SPECIES2)_$$(word 2,$$(subst _, ,$$(word 1,$$(subst ., ,$$@)))).*.pdb.gz),alignments_missing) %.loc_cl.clusters
	set -o pipefail; \
	set -e; \
	zcat $< \
	| awk '$$9>=$(ALIGN_SCORE_MIN)' \
	| sort -S40% -k2,2n \
	| $(BIN_DIR)/align_in_loc_cl2 $(word 2,$^) \
	| gzip > $@
