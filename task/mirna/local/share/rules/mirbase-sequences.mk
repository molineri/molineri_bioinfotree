# Copyright 2008-2009 Gabriele Sales <gbrsales@gmail.com>

SPECIES               ?= hsapiens
SEQUENCES_VERSION     ?= 12.0
SEQUENCES_BASE_URL    ?= ftp://ftp.sanger.ac.uk/pub/mirbase/sequences/$(SEQUENCES_VERSION)/
COORDS_BASE_URL       ?= $(SEQUENCES_BASE_URL)/genomes/

SEQUENCES_SPECIES_MAP := $(TASK_ROOT)/local/share/mirbase_sequence-species.map


ALL += sequences.fa.gz seeds.fa.gz coords.gz coords.genome

INTERMEDIATE += sequences-mature.fa.gz
sequences-mature.fa.gz:
	wget -O $@ "$(SEQUENCES_BASE_URL)/mature.fa.gz"

INTERMEDIATE += sequences-minor.fa.gz
sequences-minor.fa.gz:
	wget -O $@ "$(SEQUENCES_BASE_URL)/maturestar.fa.gz"

sequences.fa.gz: sequences-mature.fa.gz sequences-minor.fa.gz
	zcat $^ \
	| sed 's/^\(>[^ \t]*\).*/\1/' \
	| get_fasta -r "$$(translate $(SEQUENCES_SPECIES_MAP) 1 <<<"$(SPECIES)")-"  \
	| gzip >$@

.META: sequences.fa.gz
	>miRNA_id
	mirna_sequence

seeds.fa.gz: sequences.fa.gz
	zcat $< \
	| mirna_seeds \
	| gzip >$@

.META: seeds.fa.gz
	>miRNA_id
	seed_sequence

INTERMEDIATE += coords.gff.download
coords.gff.download:
	wget -O $@ "$(COORDS_BASE_URL)/$$(translate $(SEQUENCES_SPECIES_MAP) 1 <<<"$(SPECIES)").gff"

coords.genome: coords.gff.download
	grep "Genome assembly:" $< \
	| at_least_rows 1 'sed -r '\''s|^.*:\s+||'\' >$@

coords.gz: coords.gff.download
	unhead -n 7 $< \
	| cut -f 1,4,5,7,9,10 \
	| sed 's|ACC="||; s|ID="||; s|";\s*|\t|; s|";||' \
	| bawk '{print $$6,$$1,$$2-1,$$3,$$4,$$5}' \
	| gzip >$@

.META: coords.gz
	1  ID         hsa-mir-1302-2
	2  chr        1
	3  start      20229
	4  stop       20366
	5  strand     +
	6  accession  MI0006363

hairpins.fa.gz:
	wget -O- 'ftp://ftp.sanger.ac.uk/pub/mirbase/sequences/$(SEQUENCES_VERSION)/hairpin.fa.gz' \
	| zcat \
	| get_fasta -r "$$(translate $(SEQUENCES_SPECIES_MAP) 1 <<<"$(SPECIES)")-" \
	| gzip >$@
