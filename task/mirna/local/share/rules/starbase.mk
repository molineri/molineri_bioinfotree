URL_PREFIX = http://starbase.sysu.edu.cn/downloads
SEQ_DIR = $(BIOINFO_ROOT)/task/sequences/dataset/ucsc/$(UCSC_SPECIES)/$(UCSC_VERSION)

ALL += ClipSeq_Prediction_isect.ALL-cluster.fa.gz 

Ago_ClipSeq_peakCluster.bed.gz:
	wget -O - $(URL_PREFIX)/$(SPECIES)_Ago_ClipSeq_peakCluster_starBase.bed \
	| gzip >$@

.META: Ago_ClipSeq_peakCluster.bed.gz:
	1	chr	11
	2	b	206974
	3	e	206994
	4	id	chr11_fcluster0
	5	ReadNum	1
	6	strand	+

Ago_ClipSeq_peakCluster.fa.gz: Ago_ClipSeq_peakCluster.bed.gz
	bawk '{print $$4,$$1~3,$$6}' $< \
	| seqlist2fasta -l $(SEQ_DIR) \
	| reverse_fasta 5 \
	| gzip > $@

$(addsuffix .gz, $(addprefix ClipSeq_Prediction_isect., $(PREDICTION_ALGO))): ClipSeq_Prediction_isect.%.gz:
	wget -O - $(URL_PREFIX)/$(SPECIES)_ClipSeq_$*_overlapResults_starBase.bed \
	| sed 's/^chr//' \
	| tr ":" "\t" \
	| tr "," ";" \
	| gzip > $@

.META: ClipSeq_Prediction_isect.miRanda.gz ClipSeq_Prediction_isect.picTar4way.gz ClipSeq_Prediction_isect.PITA.gz ClipSeq_Prediction_isect.RNA22.gz ClipSeq_Prediction_isect.TargetScan.gz 
	1	chr 	1
	2	b	6283744
	3	e	6283763
	4	miRNA	hsa-let-7a
	5	gene	ICMT
	6	cluster	chr1_rcluster695        multiple values
	7	ReadNum	2
	8	strand	-

Ago_ClipSeq_peakCluster.fa.no_prediction.fa.gz: Ago_ClipSeq_peakCluster.fa.gz $(addsuffix .gz, $(addprefix ClipSeq_Prediction_isect., $(PREDICTION_ALGO)))
	zcat $< \
	| fasta2oneline \
	| filter_1col 1 <(zcat $(addsuffix .gz, $(addprefix ClipSeq_Prediction_isect., $(PREDICTION_ALGO))) | cut -f 6) \
	| bawk '{print ">" $$1~5; print $$6}' \
	| gzip > $@


$(addsuffix .overlap.gz, $(addprefix ClipSeq_Prediction_isect., $(PREDICTION_ALGO))): ClipSeq_Prediction_isect.%.overlap.gz: Ago_ClipSeq_peakCluster.bed.gz ClipSeq_Prediction_isect.%.gz
	zcat $< | bsort -k 1,1 -k2,2n | intersection - <(\
		zcat $^2 | bsort -k1,1 -k2,2n | uniq\
	) | intersection_add_sim_diff \
	| select_columns 14 16 9 \
	| gzip > $@

.META: ClipSeq_Prediction_isect.miRanda-overlap.gz ClipSeq_Prediction_isect.picTar4way-overlap.gz ClipSeq_Prediction_isect.PITA-overlap.gz ClipSeq_Prediction_isect.RNA22-overlap.gz ClipSeq_Prediction_isect.TargetScan-overlap.gz
	1 cluster
	2 miRNA
	3 overlap ]0,1]

$(addsuffix -cluster.gz, $(addprefix ClipSeq_Prediction_isect., $(PREDICTION_ALGO))): ClipSeq_Prediction_isect.%-cluster.gz: ClipSeq_Prediction_isect.%.gz Ago_ClipSeq_peakCluster.bed.gz
	zcat $<\
	| grep -vF ';' \                                          *do not consider predicted sites overlapping two clusters *
	| translate -a -f 4 <(zcat $^2) 6 \
	| bawk '$$11==$$13 {print $$1~6,$$8,$$9,$$11}' \          *$$11==$$13 : do not consider predicted sites overlapping cluster on different strand *
	| gzip > $@

.META: ClipSeq_Prediction_isect.miRanda-cluster.gz ClipSeq_Prediction_isect.picTar4way-cluster.gz ClipSeq_Prediction_isect.PITA-cluster.gz ClipSeq_Prediction_isect.RNA22-cluster.gz ClipSeq_Prediction_isect.TargetScan-cluster.gz 
	1       chr		1
	2       mirna_b		100488305
	3       mirna_e		100488312
	4       mirna_id	hsa-miR-103
	5       gene		SLC35A3
	6       cluster		chr1_fcluster9076
	7       cluster_b	100488289
	8       cluster_e	100488313
	9       strand		+


$(addsuffix -cluster.fa.gz, $(addprefix ClipSeq_Prediction_isect., $(PREDICTION_ALGO))): ClipSeq_Prediction_isect.%-cluster.fa.gz: ClipSeq_Prediction_isect.%-cluster.gz ClipSeq_Prediction_isect.%.overlap.gz
	bawk '{print $$cluster,$$chr,$$cluster_b,$$cluster_e,$$strand,$$mirna_id}' $<\
	| filter_2col -b 1 6 <(bawk '$$overlap==1 {print $$cluster,$$miRNA}' $^2 ) \    * take only cluster overlapping completely with the predicted target site*
	| seqlist2fasta -l $(SEQ_DIR) \
	| bawk '{print $$1,$$6,$$2~5}' \
	| perl -pe 's/\t+$$//' \
	| reverse_fasta 6 \
	| gzip > $@

ClipSeq_Prediction_isect.ALL-cluster.fa.gz: $(addsuffix -cluster.fa.gz, $(addprefix ClipSeq_Prediction_isect., $(PREDICTION_ALGO)))
	( for i in $(PREDICTION_ALGO); do\
		zcat ClipSeq_Prediction_isect.$$i-cluster.fa.gz \
		| fasta2oneline \
		|  append_each_row $$i \
		| cut -f 1,2,7,8; \
	done) \
	| collapsesets 4 \
	| bawk '{print ">" $$1,$$2,$$4; print toupper($$3)}' \
	| gzip > $@

ClipSeq_Prediction_isect.ALL2-cluster.fa.gz: ClipSeq_Prediction_isect.ALL-cluster.fa.gz
	zcat $< | get_fasta -r ';' \
	| gzip >$@

