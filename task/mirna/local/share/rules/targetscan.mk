# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2011 Ivan Molineris <ivan.molineris@gmail.com>

SPECIES ?= hsapiens
VERSION ?= 50
UCSC_VERSION = 19 

SPECIES_MAP   := $(BIOINFO_ROOT)/task/annotations/local/share/ucsc_species.map
UCSC_SPECIES   = $(shell translate $(SPECIES_MAP) 1 <<<"$(SPECIES)" |cut -f 1)
UCSC_DATABASE  = $(UCSC_SPECIES)$(UCSC_VERSION)

MAF_DIR := $(BIOINFO_ROOT)/task/alignments/dataset/ucsc/hsapiens_multiz/$(UCSC_DATABASE)
SEQ_DIR = $(BIOINFO_ROOT)/task/sequences/dataset/ucsc/hsapiens/$(UCSC_DATABASE)
MAF_SPECIES := hg19 panTro2 gorGor1 ponAbe2 rheMac2 papHam1 calJac1 tarSyr1 dipOrd1 cavPor3 anoCar1 taeGut1 galGal3 bosTau4 monDom5 canFam2 rn4 mm9 loxAfr3 oryCun2 sorAra1 equCab2 speTri1 choHof1 dasNov2 echTel1 ornAna1 xenTro2 turTru1 proCap1 felCat3 pteVam1 vicPac1 ochPri2 tupBel1 otoGar1 micMur1
TARGHETSCAN_SPECIES := hg19 galGal3 mm9 rn4 canFam2

param dna as ALL_CHRS from $(SEQ_DIR)

pippo:
	echo $(ALL_CHRS)

BASE_URL       := http://www.targetscan.org/vert_$(VERSION)/vert_$(VERSION)_data_download
SPECIES_TAXON   = $(shell translate $(TASK_ROOT)/local/share/targetscan-species.map 1 <<<"$(SPECIES)")
ALL_TXT        := miR_Family_Info.txt Predicted_Targets_Info.txt
ALL_ZIP        := $(addsuffix .zip,$(ALL_TXT))

ALL += family.gz targets.gz mirna_gene.map.gz

CLEAN += $(ALL_ZIP)
%.zip:
	wget -O$@ '$(BASE_URL)/$@'


INTERMEDIATE += $(ALL_TXT)
%.txt: %.txt.zip
	unzip -p $< \
	| unhead >$@

family.gz: miR_Family_Info.txt
	bawk '$$3=="$(SPECIES_TAXON)" {$$1=tolower($$1);$$4=tolower($$4);print}' $< \
	| cut -f3 --complement \
	| bsort -k1,1 -k3,3 \
	| gzip >$@

miR_Family_info_all.txt: miR_Family_Info.txt
	sed '1,1d' $< | cut -f1,2,3 | sort -u > $@        *see the README of http://www.targetscan.org/vert_61/vert_61_data_download/targetscan_60.zip*

.META: family.gz
	1  family             mir-548d-3p
	2  seed               AAAAACC
	3  mirbase_ID         hsa-mir-548d-3p
	4  mature_sequence    CAAAAACCACAGUUUCUUUUGC  
	5  conservation       0
	6  mirbase_accession  MIMAT0003323

family.prob: family.gz
	zcat $< | cut -f -2 | collapsesets 1 | perl -lane 'BEGIN{$$,="\t"}$$F[1]=~tr/U/T/; print $$F[0],$$F[1]' >$@

targets.gz: Predicted_Targets_Info.txt family.gz
	bawk '$$4=="$(SPECIES_TAXON)" {$$1=tolower($$1);print}' $< \
	| cut -f4 --complement \
	| translate -k -d <(zcat $^2 | cut -f1,3) 1 \   * filter out mirna families lacking a representative in this species *
	| expandsets 1 \
	| bsort -k1,1 -k2,2 \
	| gzip >$@

.META: targets.gz
	1  mirna_ID     hsa-mir-320
	2  gene_ID      201626
	3  gene_symbol  2-PDE
	4  UTR_start    893
	5  UTR_end      899
	6  MSA_start    941
	7  MSA_end      947
	8  seed_match   1A
	9  PCT          0.1764

mirna_gene.map.gz: targets.gz
	zcat $< \
	| cut -f-2 \
	| bsort -u \
	| collapsesets 2 \
	| bsort -k1,1 \
	| gzip >$@

.META: mirna_target.map
	1  mirna_ID  hsa-mir-331
	2  gene_IDs  160622;23291;9256


%.targetscan_in.gz: $(MAF_DIR)/%.maf.gz
	zcat $< | maf_join -s "$(MAF_SPECIES)" \
	| gzip > $@

.META: *.targetscan_in.gz
	1	GeneID
	2	species_ID
	3	sequence

%.strandP.targetscan_out.gz: miR_Family_info_all.txt %.targetscan_in.gz $(BIOINFO_ROOT)/task/annotations/local/share/ucsc_species.map
	targetscan $< <(\
		zcat $^2 | maf_join_windows -l 1000000 -o 6 \
		| perl -pe 's/(\d+\t[A-Za-z]+)\d+/$$1/' \
		| id2count 2 -m $@.species_map\
	) /dev/stdout | unhead | gzip > $@

%.strandM.targetscan_out.gz: miR_Family_info_all.txt %.targetscan_in.gz $(BIOINFO_ROOT)/task/annotations/local/share/ucsc_species.map
	targetscan $< <(\
		zcat $^2 | maf_join_windows -l 1000000 -o 6 \
		| perl -pe 's/(\d+\t[A-Za-z]+)\d+/$$1/' \
		| id2count 2 -m $@.species_map\
		| perl -lane 'BEGIN{$$,="\t"} $$F[2] =~ tr/ATCGatcg/TAGCtagc/; $$F[2]=reverse $$F[2]; print @F' \
	) /dev/stdout | unhead | gzip > $@

.META: *.targetscan_out.gz
	1	GeneID			name/ID of gene (from UTR input file)						0
	2	miRNA_family_ID		name/ID of miRNA family (from miRNA input file)					let-7/98/4458/4500
	3	species_ID		name/ID of species (from UTR input file)					13
	4	MSA_start		starting position of site in aligned UTR (counting gaps)			384870
	5	MSA_end			ending position of site in aligned UTR (counting gaps) 				384876
	6	UTR_start		starting position of site in UTR (not counting gaps)				53665 
	7	UTR_end			ending position of site in UTR (not counting gaps)				53671
	8	Group_ID		ID (number) of site(s) (same gene, same miRNA) that overlap			1
	9	Site_type		type of site in this species (m8 [7mer-m8], 1a [7mer-1A], or m8:1a [8mer])	7mer-1a
	10	miRNA_in_this_species	if "x", then this miRNA has been annotated in this species
	11	Group_type		type of this group of sites; if 'Site_type' in a 'Group_ID' is heterogeneous, "weakest" type of the group is used	7mer-1a+8mer-1a
	12	Species_in_this_group	list of species names/IDs in which this site is found				1 13 15 18 21
	13	Species_in_this_group_with_this_site_type	for hetergeneous groups only				1 13 15 18 21

.PHONY: ALL_targetscan_out.gz
ALL_targetscan_out.gz: $(addsuffix .strandP.targetscan_out.gz, $(ALL_CHRS)) $(addsuffix .strandM.targetscan_out.gz, $(ALL_CHRS))
	@echo done

%.miR_targets.bed.gz: %.strandP.targetscan_out.gz %.strandM.targetscan_out.gz 
	bawk '{print $*, $$1+$$6, $$1+$$7, $$miRNA_family_ID, $$ }' $<

%.miR.prob_map_out.gz: family.prob /rogue/molineri/bioinfotree/task/sequences/dataset/ucsc/hsapiens/hg19/%.fa
	prob_map -v -p $< < $^2 \
	| gzip > $@

.PHONY: ALL_prob_map_out.gz
ALL_prob_map_out.gz: $(addsuffix .miR.prob_map_out.gz, $(ALL_CHRS))
	@echo done

