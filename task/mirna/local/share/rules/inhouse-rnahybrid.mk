# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

PVALUE_CUTOFF ?= 0.01

import inhouse-prediction-common

define rnahybrid_scan 
	+RNAhybrid -c -s 3utr_human -p $(PVALUE_CUTOFF) -q <(get_fasta @$1:$2 <$<) -t $^3 \
	| gzip >$@
endef

define filter_targets
	awk -F':' -v OFS='\t' 'function swap(from,to){tmp=$$from;$$from=$$to;$$to=tmp} {swap(1,3);swap(2,4);swap(2,3);print}'
endef
