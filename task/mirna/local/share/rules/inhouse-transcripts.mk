# Copyright 2008-2009 Gabriele Sales <gbrsales@gmail.com>

ANNOTATION_DIR ?= $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/mmusculus/50/

extern $(ANNOTATION_DIR)/transcript-gene.map.gz as TRANSCRIPT_GENE_MAP

ALL += transcript_groups.gz transcript-gene_groups.gz


transcript_groups.gz: ../predictions/targets.gz
	zcat $< \
	| cut -f-2 \
	| collapsesets 2 \
	| gzip >$@

.META: transcript_groups.gz
	1   mirna_ID              mmu-let-7g
	2   transcript_IDs        ENSMUST00000000080;ENSMUST00000000076

transcript-gene_groups.gz: ../predictions/targets.gz
	zcat $< \
	| cut -f-2 \
	| translate -a <(zcat $(TRANSCRIPT_GENE_MAP)) 2 \
	| bawk '{print $$1,$$3,$$2}' \
	| sort -k1,1 -k2,2 -S40% \
	| collapsesets 3 \
	| sort -k1,1 -k2,2 -S40% \
	| gzip >$@

.META: transcript-gene_groups.gz
	1   mirna_ID              mmu-let-7g
	2   gene_ID               ENSMUSG00000000001
	3   transcript_IDs        ENSMUST00000000080;ENSMUST00000000076
