# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

ALGORITHM       ?= miranda
MRNA_BLOCK_SIZE ?= 10m

extern ../sequences/mirnas.fa as MIRNAS_FA
extern ../sequences/3utrs.fa as 3UTRS_FA

ifndef BMAKE_CLEANING

ifeq ($(if $(and $(filter-out undefined,$(origin INCREMENTAL_REF)),$(shell ls ../sequences/3utrs-delta)),yes,),yes)

DO_INCREMENTAL := yes
$(ALGORITHM).mkcombo: ../sequences/mirnas.fa ../sequences/3utrs.fa ../sequences/3utrs-delta.fa ../sequences/mirnas-delta.fa makefile
	fasta_cartesian -i0 -n$(MRNA_BLOCK_SIZE) -lALL_3UTR $< $^2 3utrs-%3_%4.fa '$$(call 3utr_part,%3,%4)' >$@
	fasta_cartesian -i0 -n$(MRNA_BLOCK_SIZE) -lINCREMENTAL_3UTR $< $^3 3utrs-delta-%3_%4.fa '$$(call 3utr_part,%3,%4)' >>$@
	fasta_cartesian -i100 -n$(MRNA_BLOCK_SIZE) -lINCREMENTAL_PREDICTION_MRNA -d 3utrs-delta-%3_%4.fa $^1 $^3 $(ALGORITHM)-mrna_delta-%1_%2-%3_%4.gz '$$(call $(ALGORITHM)_scan,%1,%2,%3,%4)' >>$@
	fasta_cartesian -i100 -n$(MRNA_BLOCK_SIZE) -lINCREMENTAL_PREDICTION_MIRNA -d 3utrs-%3_%4.fa $^4 $^2 $(ALGORITHM)-mirna_delta-%1_%2-%3_%4.gz '$$(call $(ALGORITHM)_scan,%1,%2,%3,%4)' >>$@

else

$(ALGORITHM).mkcombo: ../sequences/mirnas.fa ../sequences/3utrs.fa makefile
	fasta_cartesian -i0 -n$(MRNA_BLOCK_SIZE) -lALL_3UTR $< $^2 3utrs-%3_%4.fa '$$(call 3utr_part,%3,%4)' >$@
	fasta_cartesian -i100 -n$(MRNA_BLOCK_SIZE) -lALL_PREDICTION -d 3utrs-%3_%4.fa $< $^2 $(ALGORITHM)-%1_%2-%3_%4.gz '$$(call $(ALGORITHM)_scan,%1,%2,%3,%4)' >>$@

endif
endif

CLEAN += $(ALGORITHM).mkcombo
-include $(ALGORITHM).mkcombo

define 3utr_part
	get_fasta @$1:$2 <$^2 >$@
endef

ALL += targets.gz
INTERMEDIATE += $(ALL_3UTR)
ALL_PREDICTION += $(INCREMENTAL_PREDICTION_MRNA) $(INCREMENTAL_PREDICTION_MIRNA)
CLEAN += $(ALL_PREDICTION)

ifdef DO_INCREMENTAL
targets.gz: $(INSTANCE_ROOT)/../$(INCREMENTAL_REF)/predictions/targets.gz ../sequences/mirnas-delta ../sequences/3utrs-delta $(INCREMENTAL_PREDICTION_MIRNA) $(INCREMENTAL_PREDICTION_MRNA)
	set -e; \
	( \
		zcat $< \
		| filter_1col -v 1 <(bawk '$$1!="+" {print $$2}' $^2) \
		| filter_1col -v 2 <(bawk '$$1!="+" {print $$2}' $^3); \
		zcat $(wordlist 4,$(words $^),$^) \
		| $(filter_targets); \
	) \
	| bsort -S40% \
	| gzip >$@
else
targets.gz: $(ALL_PREDICTION)
	zcat $^ \
	| $(filter_targets) \
	| bsort -S40% \
	| gzip >$@
endif
