#!/usr/bin/env python
#
# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

from optparse import OptionParser
from sys import stdin, stdout
from vfork.fasta.reader import MultipleBlockStreamingReader
from vfork.fasta.writer import MultipleBlockWriter
from vfork.util import exit, format_usage

def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] <MIRNA_FA >SEED_FA

		Reads mature miRNA sequences and outputs the corresponding seeds.
	'''))
	options, args = parser.parse_args()

	if len(args) != 0:
		exit('Unexpected argument number.')
	
	writer = MultipleBlockWriter(stdout)
	for header, sequence in MultipleBlockStreamingReader(stdin):
		writer.write_header(header)
		writer.write_sequence(sequence[1:8])
	
	writer.flush()

if __name__ == '__main__':
	main()
