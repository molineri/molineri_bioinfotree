#!/bin/bash

if [ $# -ne 2 ]; then
	echo "Unexpected argument number." >&2
	exit 1
fi

# Report start
echo "<html>"

# Head
echo "<head>"
echo "<title>genome_cmp report</title>"
echo "</head>"

# Body
echo "<body>"
echo "<h1>Chromosomes</h1>"
echo "<p>"
echo "Genome 1: <code>$1</code><br>"
echo "Genome 2: <code>$2</code>"
echo "</p>"

for png in chr*.png; do
	name="${png%.png}"
	name="${name#chr}"
	echo "<h2>$name</h2>"
	echo "<center><img src=\"$png\"></img></center>"
done

echo "</body>"

# Report end
echo "</html>"
