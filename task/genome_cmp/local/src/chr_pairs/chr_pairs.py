#!/usr/bin/env python
# encoding: utf-8

from glob import glob
from optparse import OptionParser
from os.path import basename, isdir, join
from sys import exit

def list_fasta(path):
	return glob(join(path, '*.fa'))

def list_pairs(chr_list1, chr_list2):
	names1 = [ basename(p).lower() for p in chr_list1 ]
	names2 = [ basename(p).lower() for p in chr_list2 ]
	
	for idx1, name1 in enumerate(names1):
		try:
			idx2 = names2.index(name1)
			yield (chr_list1[idx1], chr_list2[idx2])
		except ValueError:
			pass

def main():
	parser = OptionParser(usage='%prog GENOME_DIR1 GENOME_DIR2')
	options, args = parser.parse_args()

	if len(args) != 2:
		exit('Unexpected argument number.')
	
	for path in args:
		if not isdir(path):
			exit('%s is not a directory.' % path)
	
	chr_list1 = list_fasta(args[0])
	chr_list2 = list_fasta(args[1])	
	for chr1, chr2 in list_pairs(chr_list1, chr_list2):
		print chr1.replace(' ', '\\ '), chr2.replace(' ', '\\ ')

if __name__ == '__main__':
	main()
