#!/usr/bin/perl
use warnings;
use strict;
$,="\t";
$\="\n";

my $reverse;
my $chr1;
my $chr2;
my $start1;
my $start2;
my $stop1;
my $stop2;
my $offset;

while(<>){
	if(m/^h/){
		$chr1=<>;
		$chr2=<>;
		$chr1 =~ s/^\s+\">//;
		$chr2 =~ s/^\s+\">//;
		$chr1 =~ s/\"\s+$//;
		$chr2 =~ s/\"\s+$//;
		if($chr2 =~/reverse/){
			$reverse=1;
		}else{
			$reverse=0;
		}
		$chr1=~/(\d+)$/;
		$offset=$1;
		$chr2 =~ s/\s+.*$//;
	}elsif(m/^\s+b/){
		($start1,$start2) = (m/\sb\s(\d+)\s(\d+)/);
	}elsif(m/^\s+e/){
		($stop1,$stop2) = (m/\se\s(\d+)\s(\d+)/);
		if($reverse){
			print $chr1, $offset+$start1, $offset+$stop1, $chr2, $stop2, $start2;
		}else{
			print $chr1, $offset+$start1, $offset+$stop1, $chr2, $start2, $stop2;
		}
	}
}

