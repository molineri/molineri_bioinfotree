#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;


$,="\t";
$\="\n";

my $usage = "$0 -m 8 < qualcosa.chain_format
	output:	1. query_chr
		2. query_align_start
		3. query_align_stop
		4. target_chr
		5. target_align_start
		6. target_align_stop
		7. strand
		8. gaps_sum
		9. chain_score
		10.chain_id";

my $max_gap = undef;
GetOptions (
	'max_gap|m=i' => \$max_gap,
) or die ($usage);

die $usage if (!defined $max_gap);


my $q_chr = undef;
my $q_b = undef;
my $q_e = undef;
my $t_chr = undef;
my $t_b = undef;
my $t_e = undef;
my $strand = undef;
my $chain_id = undef;

my $q_sum_gaps = undef;
my $t_sum_gaps = undef;
my $length = undef;
my $q_gaps = undef;
my $t_gaps = undef;

while (my $line = <>) {
	next if ($line=~/^##/);
	chomp $line;
	next if !$line;

	if ($line=~/^chain/) {
		# chain 418999670 chrY 57772954 + 2 6162616 chrX 154913754 + 2 92262165 1
		my @chain_int = split /\s+/,$line;
		$q_chr = $chain_int[2];
		$q_b = $chain_int[5];
		$t_chr = $chain_int[7];
		$t_b = $chain_int[10];
		$t_e = $chain_int[11];
		$strand = ($chain_int[4] eq $chain_int[9]) ? '+' : '-';
		$chain_id = $chain_int[12];
	
		$q_e = $q_b;
		$t_e = $t_b;
		$q_sum_gaps = 0;
		$t_sum_gaps = 0;
		next;
	}

	($length, $q_gaps, $t_gaps) = split /\t/,$line;
	$q_e += $length;
	$t_e += $length;

	if (!defined $q_gaps or !defined $t_gaps) {
		&print_align;
		next;
	}

	if ( ($q_gaps > $max_gap) or ($t_gaps > $max_gap) ) {
		&print_align;
		next;
	}

	$q_e += $q_gaps;
	$t_e += $t_gaps;
	$q_sum_gaps += $q_gaps;
	$t_sum_gaps += $t_gaps;

}


sub print_align {

	print $q_chr,$q_b,$q_e, $t_chr,$t_b,$t_e, $strand, $q_sum_gaps,$t_sum_gaps, $chain_id;
	$q_sum_gaps = 0;
	$t_sum_gaps = 0;
	$q_b = (defined $q_gaps) ? $q_e + $q_gaps : $q_e;
	$t_b = (defined $t_gaps) ? $t_e + $t_gaps : $t_e;
	$q_e = $q_b;
	$t_e = $t_b;
}

