
SPECIES            ?= hsapiens
VERSION            ?= 18

SPECIES_MAP   := $(BIOINFO_ROOT)/task/annotations/local/share/ucsc_species.map
UCSC_SPECIES   = $(shell translate $(SPECIES_MAP) 1 <<<"$(SPECIES)" | cut -f 1)
UCSC_DATABASE  = $(UCSC_SPECIES)$(VERSION)
UCSC_ALIGN_SPECIES   = $(shell translate $(SPECIES_MAP) 1 <<<"$(ALIGN_SPECIES)" | cut -f -1)
UCSC_ALIGN_DATABASE  = $(UCSC_ALIGN_SPECIES)$(ALIGN_VERSION)
DATABASE_URL   = http://hgdownload.cse.ucsc.edu/goldenPath/$(UCSC_DATABASE)
SEQUENCE_DIR = $(BIOINFO_ROOT)/task/sequences/dataset/ucsc/$(SPECIES)/$(UCSC_DATABASE)
SEQUENCE_DIR_FOR_ALIGN_SPECIES = $(BIOINFO_ROOT)/task/sequences/dataset/ucsc/$(ALIGN_SPECIES)/$(UCSC_ALIGN_DATABASE)

include $(BIOINFO_ROOT)/task/sequences/local/share/species/ucsc/$(SPECIES).mk

include $(BIOINFO_ROOT)/task/sequences/local/share/species/ucsc/$(SPECIES).mk


ALL+= $(addsuffix .axt.gz, $(addprefix chr,$(SPECIES_CHRS))) $(addsuffix .map.gz, $(addprefix chr,$(SPECIES_CHRS)))
all_map: $(addsuffix .axt.gz, $(addprefix chr,$(SPECIES_CHRS))) $(addsuffix .map.gz, $(addprefix chr,$(SPECIES_CHRS)))

all_chr.len:
	wget -O - http://hgdownload.cse.ucsc.edu/goldenPath/$(UCSC_ALIGN_DATABASE)/database/chromInfo.txt.gz \
	| zcat | cut -f -2 \
	| sed 's/^chr//' > $@

liftOver.chain.gz:
	wget -O $@ http://hgdownload.cse.ucsc.edu/goldenPath/$(UCSC_DATABASE)/liftOver/$(UCSC_DATABASE)To$(shell perl -e 'print ucfirst("$(UCSC_ALIGN_DATABASE)")').over.chain.gz

all.chain.gz net.gz: %.gz:
	db=$(UCSC_ALIGN_DATABASE);\
	db=`perl -ne 'if($$_ ne "$(UCSC_DATABASE)"){ print ucfirst} else{ print "Self"};' <<<$$db`;\
	wget -O $@ $(DATABASE_URL)/vs$$db/$(UCSC_DATABASE).$(UCSC_ALIGN_DATABASE).$*.gz

chain.gz: all.chain.gz
	cp $< $@

chain.axt.gz: chain.gz $(SEQUENCE_DIR) $(SEQUENCE_DIR_FOR_ALIGN_SPECIES)
	/usr/local/stow/kent/bin/chainToAxt \			* chainToAxt is a jkent software *
	<(zcat $<) $^2 $^3 >(gzip > $@)

#chr1.atx.gz
%.axt.gz:
	db=$(UCSC_ALIGN_DATABASE);\
	db=`perl -ne 'if($$_ ne "$(UCSC_DATABASE)"){ print ucfirst} else{ print "Self"};' <<<$$db`;\
	wget -O $@ $(DATABASE_URL)/vs$$db/axtNet/$*.$(UCSC_DATABASE).$(UCSC_ALIGN_DATABASE).net.axt.gz

uniq.axt.gz:
	db=$(UCSC_ALIGN_DATABASE);\
	db=`perl -e "print ucfirst("\""$$db"\"")"`;\
	wget -O $@ $(DATABASE_URL)/vs$$db/$(UCSC_DATABASE).$(UCSC_ALIGN_DATABASE).net.axt.gz

.DOC: uniq.axt.gz
	some species do not have the axt files separed for each chromosomes but a single file.

%.map.gz: %.axt.gz all_chr.len
	zcat $< \
	| sed 's/chr//g' \
	| axtmap -f $^2 \
	| sed 's/chr//g' \
	| gzip > $@

.META: *.map.gz
	1	chrL	chromosome on genome L
	2	bL	start of region on genome L
	3	eL	end of region on genome L
	4	strand	strand of the alignment
	5	chrR	chromosome on genome R
	6	bR	start of aligned region on genome R
	7	eR	end of aligned region on genome R
	8	gap	sequence on genome R: 0=sequence 1=gap

.DOC: *.map.gz
	Each line of the output referes to a region of genome L.
	In order to be present in the output a region must be included in the corresponding axt input file.
	The meaning of columns 6 and 7 depends on the value of column 8:
	    * if column 8 = 0, the region on genome L aligns without gaps to the region in genome R, 
		and the length of the regions in the genomes are the same
	    * if column 8 = 1, the region on genome L aligns to a gap in genome R. The length of the 
		gap is equal to the length of the region in genome L, and columns 6 and 7 refer to
		the bases immediately before and immediately after the gap in genome R
	~
	An example might help:
	~
	Suppose the first 20 bases of chr1 in genome L are aligned to the first 12 bases of chrX in genome R in the following way.
	~
	0 chr1 1 20 chrX 1 12 + 1000
	AAA--AAAAA--AA
	AAATTAA--AAAAA 
	~
	The output of axtmap would be:
	~
	chr1    0       3       +       chrX    0       3       0
	chr1    3       5       +       chrX    5       7       0
	chr1    5       7       +       chrX    6       8       1
	chr1    7       8       +       chrX    7       8       0
	chr1    8       10      +       chrX    10      12      0
	~
	Note that coordinates in output are given in UCSC convention (start from 0, end not included). 
	When the alignment strand is "-", the coordinates are given on the + strand. 
