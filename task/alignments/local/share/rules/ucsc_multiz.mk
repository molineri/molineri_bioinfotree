URL_PREFIX=ftp://hgdownload.cse.ucsc.edu/goldenPath/$(UCSC_VERSION)/$(ALIGN_VERSION)
SEQUENCE_DIR = $(BIOINFO_ROOT)/task/sequences/dataset/ucsc/$(SPECIES)/$(UCSC_VERSION)

param dna as ALL_CHRS_RAW from $(SEQUENCE_DIR)

ALL+=$(addsuffix .maf.gz,$(ALL_CHRS_RAW))

$(addsuffix .maf.gz,$(ALL_CHRS_RAW)):%.maf.gz:
		wget -O $@ $(URL_PREFIX)/maf/$*.maf.gz

multi.maf.gz: $(addsuffix .maf.gz,$(ALL_CHRS_RAW))
	(for i in $^ ; do \
		zcat $$i | grep "^s " \
		| sed 's/\s\{1,\}/\t/g' \
		| awk '{if($$0 ~ /^s/ && $$0 ~ /$(UCSC_VERSION)/ && NF==7){\
				split($$2, name, ".") ; split($$0, data, "\t") ; sub(/chr/,"", name[2]) ;\
				print name[2],$$3,$$4,$$5,$$6,$$1"\t"name[1]"#"$$7\
			}else if($$0 ~ /^s/ && $$0 !~ /$(UCSC_VERSION)/ && NF==7){\
				split($$2, corr, ".") ; sub(/chr/,"", name[2]) ; print name[2],data[3],data[4],data[5],data[6],data[1]"\t"corr[1]"#"$$7\
			} }' \
		| sed 's/\s\{1,\}/:/g' | sed 's/:s:/\t/g' | sed 's/#/\t/' ;\
	done) \
	| tr ":" "\t" \
	| gzip >$@
