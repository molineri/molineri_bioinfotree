#!/usr/bin/perl

use warnings;
use strict;
use IPC::Open2;
$,="\t";
$\="\n";

my $seq_dir = shift @ARGV;

die("ERROR: $0") if !defined($seq_dir);

while(<>){
	chomp;
	print ">$_";
	my @F=split /\t/;
	print ">$F[0]<\n";
	my $RDR;
	my $WTR;
	my $pid;
	$pid = open2($RDR, $WTR, "seqlist2fasta -i $seq_dir");
	print $WTR @F[0..2];
	print $WTR @F[4..6];
	close $WTR;
	my $seq=0;
	my $seq1='';
	my $seq2='';
	while(<$RDR>){
		if(m/^>/){
			$seq++;
			next;
		}
		if($seq==1){
			$seq1.=$_;
		}else{
			$seq2.=$_;
		}
	}
	
	$seq1=~s/\n//g;
	$seq2=~s/\n//g;

	if($F[3] eq '-'){
		$seq1 = reverse $seq1;
		$seq1 =~ tr/ACGT/TGCA/;
	}
	
	print "$seq1\n";
	print "$seq2\n";

	$seq1 = &insert_gap($seq1,$F[10]) if defined($F[10]);
	$seq2 = &insert_gap($seq2,$F[11]) if defined($F[11]);

	die("ERROR: $0: sequence differ in lengtht (". (length($seq1) - length($seq2)) .")") if length($seq1) !=  length($seq2);
	
	while(length($seq1)){
		my $a = substr($seq1,0,120,'');
		my $b = substr($seq2,0,120,'');
		my @a_= split //,$a;
		my @b_= split //,$b;
		my $c='';
		for(my $i=0; $i<length($a); $i++){
			if($a_[$i] eq $b_[$i]){
				$c .= '|';
			}elsif(
				($a_[$i] eq 'A' and $b_[$i] eq 'G')
				or 
				($a_[$i] eq 'T' and $b_[$i] eq 'C')
			){
				$c.=':';
			}else{
				$c.=' ';
			}
		}
		print $a;
		print $c;
		print $b;
		print '';
	}
}

sub insert_gap
{
	my $a=shift;
	my $g=shift;
	my @gaps_start  = ($g =~ /(\d+),\d+;*/g);
	my @gaps_length = ($g =~ /\d+,(\d+);*/g);
	for(my $i=0; $i<scalar(@gaps_start); $i++){
		my $r='';
		for(1..$gaps_length[$i]){
			$r.='-';
		}
		my $pos = $gaps_start[$i];
		substr($a,$pos,0) = $r;
	}
	return $a;
}

