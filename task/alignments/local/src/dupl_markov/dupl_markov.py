from optparse import OptionParser
from pyPgSQL import PgSQL as pgsql
from sys import exit, stderr
from vfork.config.mapconfig import Configuration
from vfork.sql.databaseparams import collectDatabaseParams

LABELS = ('c', 'e', '5', '3', 'i', 'u', 'n')

def getLabelInfo():
	global LABELS
	
	lst = []
	map = {}
	idx = 0
	for x in LABELS:
		lst.append(x)
		map[x] = idx
		idx += 1
	
	for x in LABELS:
		for y in LABELS:
			if x != y:
				lst.append(x+y)
				map[x+y] = idx
				idx += 1
	
	return lst, map

def getLabel(vect, threshold):	
	sumAll = sum(vect)
	c1, c2 = sorted(zip(vect, LABELS), reverse=True)[:2]
	
	if c1[0] / sumAll > threshold:
		return c1[1]
	else:
		return c1[1]+c2[1]

def main():
	parser = OptionParser(usage='%prog [options] ALIGNMENT_RUN_ID')
	parser.add_option('-c', '--conf', dest='conf', default='db.conf', help='read database configuration from FILE', metavar='FILE')
	parser.add_option('-f', '--filter', dest='filter', default='', help='a FILTER to add to the SQL WHERE clause', metavar='FILTER')
	parser.add_option('-t', '--threshold', dest='threshold', type='int', help='non ne ho voglia')
	parser.add_option('-v', '--verbose', action='store_true', dest='verbose', help='verbose output')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		print >>stderr, 'Wrong argument number.'
		exit(1)
	
	try:
		alignmentRunId = int(args[0])
	except ValueError:
		print >>stderr, 'Invalid alignment run ID.'
		exit(1)
	
	conf = Configuration().load(options.conf)
	databaseParams = collectDatabaseParams(options, conf)
	
	labelList, labelMap = getLabelInfo()
	labelCounts = [[0]*len(labelMap)]*len(labelMap)
	threshold = options.threshold
	
	connection = pgsql.connect(**databaseParams)
	cursor = connection.cursor()
	
	try:	
		query = '''
			SELECT r1.c, r1.e, r1.utr5, r1.utr3, r1.i, r1.u, r1.stop-r1.start-r1.c-r1.e-r1.utr5-r1.utr3-r1.i-r1.u,
			       r2.c, r2.e, r2.utr5, r2.utr3, r2.i, r2.u, r2.stop-r2.start-r2.c-r2.e-r2.utr5-r2.utr3-r2.i-r2.u
			FROM alignments, region_annotations as r1, region_annotations as r2
			WHERE
				alignments.run_id = %s AND
		''' % args[0]
		clauses = '''
				r1.chr = alignments.qchr AND
				r1.start = alignments.qstart AND
				r1.stop = alignments.qstop AND
				r2.chr = alignments.tchr AND
				r2.start = alignments.tstart AND
				r2.stop = alignments.tstop
		'''
		if len(options.filter):
			query = '%s %s AND %s' % (query, options.filter, clauses)
		else:
			query += clauses
		
		cursor.execute(query)
		while True:
			row = cursor.fetchone()
			if row is None:
				break
			
			x = labelMap[getLabel(row[:7], threshold)]
			y = labelMap[getLabel(row[7:], threshold)]
			labelCounts[y][x] += 1
			labelCounts[x][y] += 1
	
	finally:
		cursor.close()
		connection.close()
	
	print '\t'.join(labelList)
	for row in labelCounts:
		print '\t'.join(row)

if __name__ == '__main__':
	main()
	
	#import pdb
	#pdb.run('main()')