#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;

my $usage = <<EOF;
usage axtmap -f chrominfo_file axt_file

-w	if a chr len is not defined in chrominfo_file warn instead of die

OUTPUT
	1	chrL	chromosome on genome L
	2	bL	start of region on genome L
	3	eL	end of region on genome L
	4	strand	strand of the alignment
	5	chrR	chromosome on genome R
	6	bR	start of aligned region on genome R
	7	eR	end of aligned region on genome R
	8	gap	sequence on genome R: 0=sequence 1=gap

	Each line of the output referes to a region of genome L.
	In order to be present in the output a region must be included in the corresponding axt input file.
	The meaning of columns 6 and 7 depends on the value of column 8:
	    * if column 8 = 0, the region on genome L aligns without gaps to the region in genome R, 
		and the length of the regions in the genomes are the same
	    * if column 8 = 1, the region on genome L aligns to a gap in genome R. The length of the 
		gap is equal to the length of the region in genome L, and columns 6 and 7 refer to
		the bases immediately before and immediately after the gap in genome R
	~
	An example might help:
	~
	Suppose the first 10 bases of chr1 in genome L are aligned to the first 12 bases of chrX in genome R in the following way.
	~
	0 chr1 1 20 chrX 1 12 + 1000
	AAA--AAAAA--AA
	AAATTAA--AAAAA 
	~
	The output of axtmap would be:
	~
	chr1    0       3       +       chrX    0       3       0
	chr1    3       5       +       chrX    5       7       0
	chr1    5       7       +       chrX    6       8       1
	chr1    7       8       +       chrX    7       8       0
	chr1    8       10      +       chrX    10      12      0
	~
	Note that coordinates in output are given in UCSC convention (start from 0, end not included). 
	When the alignment strand is "-", the coordinates are given on the + strand. 
EOF

our($opt_h, $opt_w, $opt_f) = (0, 0, undef);
getopts('hwf:');
if ($opt_h || !defined $opt_f) {
   die $usage;
}

my %len;
open CI, $opt_f or die "Error opening $opt_f";
while (<CI>) {
    chomp;
    my ($chr, $len) = split;
    $len{$chr} = $len;
}
close CI;

my ($state, @start_a, @start_b, @end_a, @end_b, @gap, $loc_a, $loc_b);

while (<>) {
    next if /^#/;
    chomp;
    my ($n, $chr_a, $start_a, $end_a, $chr_b, $start_b, $end_b, $strand, $score) = split;
    my $seq_a = <>;
    chomp $seq_a;
    my $seq_b = <>;
    chomp $seq_b;
    my $dummy = <>;
    my @seq_a = split("", $seq_a);
    my @seq_b = split("", $seq_b);
    if ($#seq_a != $#seq_b) {
        die "Sequence length does not match\n";
    }
    @start_a = ();
    @end_a = ();
    @start_b = ();
    @end_b = ();
    @gap = ();
    $state = 0;

# state = 0: no gaps
# state = 1: gap on sequence B
# state = 2: gap on sequence A

    $loc_a = $start_a;
    $loc_b = $start_b;
    start(0);
    for (my $i = 0; $i <= $#seq_a; ++$i) {
        if (($seq_a[$i] ne "-") && ($seq_b[$i] ne "-")) {
            if ($state != 0) {
                end($state);
                start(0);
            }
            ++$loc_a;
            ++$loc_b;
        }
        elsif (($seq_a[$i] ne "-") && ($seq_b[$i] eq "-")) {
            if ($state != 1) {
                end($state);
                start(1);
            }
            ++$loc_a;
        }
        elsif (($seq_a[$i] eq "-") && ($seq_b[$i] ne "-")) {
            if ($state !=  2) {
                end($state);
                start(2);
            }
            ++$loc_b;
        }
    }
    end($state);
    for (my $j = 0; $j <= $#start_a; ++$j) {
        if ($strand eq "-") {
            my $start = $start_b[$j];
            my $end = $end_b[$j];
	    if(!defined $len{$chr_b}){
		    my $msg = "Undefined len for chr ($chr_b)";
		    die($msg) if !$opt_w;
		    warn("Undefined len for chr ($chr_b)");
		    next;
	    }
            $start_b[$j] = $len{$chr_b} - $end + 1;
            $end_b[$j] = $len{$chr_b} - $start + 1;
        }
        print join("\t", $chr_a, $start_a[$j]-1, $end_a[$j], $strand, $chr_b, $start_b[$j]-1, $end_b[$j], $gap[$j]), "\n";
    }
}

sub start {

    my $x = $_[0];
    $state = $x;
    return if ($x == 2);
    if ($x == 0) {
        push @start_a, $loc_a;
        push @start_b, $loc_b;
    }
    elsif ($x == 1) {
        push @start_a, $loc_a;
        push @start_b, $loc_b-1;
    }
    push @gap, $x;
}

sub end {

    my $x = $_[0];
    return if ($x == 2);
    if ($x == 0) {
        push @end_a, $loc_a - 1;
        push @end_b, $loc_b - 1;
    }
    elsif ($x == 1) {
        push @end_a, $loc_a - 1;
        push @end_b, $loc_b;
    }
}
