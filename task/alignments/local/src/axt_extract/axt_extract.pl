#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use lib $ENV{'BIOINFO_ROOT'}.'/task/binding_sites/local/src/tfbs/';
use Tfbs;

$SIG{__WARN__} = sub {die @_};

# extract from axt files based on coordinates

my $usage = <<EOF;
usage axt_extract -c chromosome -s start -e end axt_file
EOF

our ($opt_s, $opt_e, $opt_h, $opt_c);
getopts('s:e:hc:');
if ($opt_h || ! defined $opt_s || ! defined $opt_e || ! defined $opt_c) {
    die $usage;
}

while (1) {
    my $id_line = <>;
    last unless $id_line;
    next if $id_line =~ /^#/;
    chomp $id_line;
    my ($n, $primary_chr, $primary_start, $primary_end, $aligned_chr, $aligned_start, $aligned_end, $strand, $blast) =
      split(/\s+/, $id_line);
    if ($primary_chr ne $opt_c || $primary_end < $opt_s || $primary_start > $opt_e) {
        <>;
        <>;
        <>;
        next;
    }

    my $primary_seq = <>;
    chomp $primary_seq;
    my @invmap = inverse_mapping($primary_seq);
    my $aligned_seq = <>;
    my @alt_map = mapping($aligned_seq);
    chomp $aligned_seq;
    if (length($primary_seq) != length($aligned_seq)) {
        die "Primary and aligned sequences have different length\n";
    }
    my $empty = <>;
    if ($empty !~ /^\s*$/) {
        die "Empty line expected after $n\n";
    }
    my $start;
    my $end;
    # $start and $end refer to chromosomal coordinates

    if ($primary_start > $opt_s) {
        $start = $primary_start;
    }
    else {
        $start = $opt_s;
    }

    if ($primary_end < $opt_e) {
        $end = $primary_end;
    }
    else {
        $end = $opt_e;
    }


    my $seq_start = $invmap[$start - $primary_start];
    my $seq_end = $invmap[$end - $primary_start];
    $aligned_end = $aligned_start + $alt_map[$seq_end];
    $aligned_start += $alt_map[$seq_start];
    print join(" ", ($n, $primary_chr, $start, $end, $aligned_chr, $aligned_start, $aligned_end, $strand, $blast)), "\n";
    print substr($primary_seq, $seq_start, $seq_end - $seq_start + 1), "\n";
    print substr($aligned_seq, $seq_start, $seq_end - $seq_start + 1), "\n";
    print "\n";
}
