General::argnum = "Wrong argument number.";

(* command line *)
arguments = Select[$CommandLine, !StringMatchQ[#, RegularExpression["-.*"]]& ];
If[
	Length[arguments] != 3,
	Message[General::argnum]; Exit[1];
];

lengthsFile=arguments[[2]];
outputPrefix=arguments[[3]];

(* packages *)
<<Graphics`Graphics`

(* load data *)
lengthDistribution = Map[Reverse, Import[lengthsFile, "Table"]];

(* full dataset *)
fig = LogLogListPlot[lengthDistribution, DisplayFunction -> Identity, Background -> White, PlotJoined -> True, ImageSize -> 1000];
Export[outputPrefix <> "-complete.png", fig];

(* first 1000 points *)
reducedDistribution = Select[lengthDistribution, #[[1]] < 1000& ];
fig = LogLogListPlot[reducedDistribution, DisplayFunction -> Identity, Background -> White, PlotJoined -> True, ImageSize -> 1000];
Export[outputPrefix <> "-partial.png", fig];
