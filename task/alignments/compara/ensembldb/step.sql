use ensembl_compara_33;

select *
	from genomic_align
	where
		# filter by genome and chromosomes
		genomic_align.method_link_species_set_id in
			( select method_link_species_set.method_link_species_set_id
				from genome_db, method_link, method_link_species_set
				where
					# select genome
					genome_db.name like "Homo sapiens" and
				
					# select chromosomes
					method_link.type like "BLASTZ_NET" and
					method_link_species_set.method_link_id = method_link.method_link_id and
					method_link_species_set.genome_db_id = genome_db.genome_db_id
			)
	limit 10;
