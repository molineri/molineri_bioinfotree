from os import system
from time import time

if __name__ == '__main__':
	limitBottom =     10
	limitTop    = 200000
	limit = limitBottom
	
	print 'elapsed = {'
	
	while limit <= limitTop:
		startTime = time()
		system("sed -e 's|limit 10|limit %d|' step.sql | mysql -u anonymous -h ensembldb.ensembl.org >&/dev/null" % limit)
		delta = time() - startTime
		
		print '\t{ %d, %f },' % (limit, delta)
		limit *= 2
	
	print '}'