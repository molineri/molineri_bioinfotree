use ensembl_compara_33;

select genomic_align_block.*
	from dnafrag, genome_db, genomic_align, genomic_align_block, method_link
	where
		genome_db.name like "Homo sapiens" and
		method_link.type like "BLASTZ_NET" and
		
		dnafrag.genome_db_id = genome_db.genome_db_id and
		dnafrag.name not like "%NT%" and dnafrag.name not like "DR%" and
		
		genomic_align.dnafrag_id = dnafrag.dnafrag_id and
		genomic_align_block.genomic_align_block_id = genomic_align_block.genomic_align_block_id
	
	group by genomic_align_block.genomic_align_block_id
	limit 30;

