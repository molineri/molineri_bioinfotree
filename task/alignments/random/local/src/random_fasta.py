#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr, stdout
from optparse import OptionParser
from vfork.fasta.reader import MultipleBlockStreamingReader
from vfork.fasta.writer import MultipleBlockWriter
from vfork.util import exit, format_usage
from random import shuffle
from array import array

def main():
	usage = format_usage('''
		%prog < FASTA
	''')
	parser = OptionParser(usage=usage)
	
	#parser.add_option('-c', '--cutoff', type=float, dest='cutoff', default=0.05, help='some help CUTOFF', metavar='CUTOFF')

	options, args = parser.parse_args()

	if len(args) != 0:
		exit('Unexpected argument number.')
	
	reader = MultipleBlockStreamingReader(stdin)
	writer = MultipleBlockWriter(stdout)

	for header, content in reader:
		a = array('c', content)
		shuffle(a)
		writer.write_header(header)
		writer.write_sequence(a.tostring())
	
	writer.flush()

if __name__ == '__main__':
	main()
