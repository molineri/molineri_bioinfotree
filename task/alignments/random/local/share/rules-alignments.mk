include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

SEQDIR1 := ../sequences
SEQDIR2 := ../sequences

all: alignments.$(ALIGN_TYPE).gz

clean:
	rm -f align.combo accoda_gentile.tmp accoda_gentile.tmp.tac
	rm -f $(ALL_ALIGN)
	rm -f $(ALL_ALIGN:.gz=.log.gz)
clean.full: clean
	rm -f alignments.$(ALIGN_TYPE).gz

layout: rules-alignments-common.mk rules-algo.mk

rules-alignments-common.mk: ../../../../local/share/rules-alignments-common.mk rules-algo.mk
	ln -sf $< $@

rules-algo.mk: ../../../../../../task/alignments/inhouse/local/share/rules-algo2.mk
	ln -sf $< $@


align.combo: makefile ../sequences/random.fa.splitted.len ../sequences/target.fa.splitted.len ../sequences/random.fa.splitted ../sequences/random.fa.splitted.idx ../sequences/random.fa.splitted.xns ../sequences/target.fa.splitted ../sequences/target.fa.splitted.idx ../sequences/target.fa.splitted.xns
	set -e; \
	( \
		$(ALIGN_BIN_DIR)/wublast_mkcombo $(SINGLE_SPECIES) $(QUERY_LABEL) ../sequences random $(TARGET_LABEL) ../sequences target 20M $(ALIGN_TYPE)_single_run; \
		echo -ne 'INTRON_CUMULATIVE_LENGTH1:='; \
		cat $(word 2,$^); \
		echo -ne 'INTRON_CUMULATIVE_LENGTH2:='; \
		cat $(word 3,$^); \
	) >$@

include align.combo

WUBLAST_FLAGS += Y=$(INTRON_CUMULATIVE_LENGTH1) Z=$(INTRON_CUMULATIVE_LENGTH2)
include rules-algo.mk

.PHONY: align.accoda
align.accoda: align.combo
	$(ALIGN_BIN_DIR)/queue_helper -c 'echo -- make $$target' -d1 $< ALL_ALIGN $(SPECIES1) $(SPECIES2) $(ALIGN_TYPE)

.PHONY: align.accoda_gentile
align.accoda_gentile: accoda_gentile.tmp
	accoda_gentile $<

.INTERMEDIATE: accoda_gentile.tmp.tac
accoda_gentile.tmp.tac: accoda_gentile.tmp
	tac $< >$@

.PHONY: align.accoda_gentile.tac
align.accoda_gentile.tac: accoda_gentile.tmp.tac
	accoda_gentile $<

.INTERMEDIATE: accoda_gentile.tmp
accoda_gentile.tmp: align.combo
	$(ALIGN_BIN_DIR)/queue_helper -c '--name $$name -- make $$target' -n $< ALL_ALIGN $(SPECIES1) $(SPECIES2) $(ALIGN_TYPE) >$@

.PHONY: align.bsub
align.bsub: align.combo
	$(ALIGN_BIN_DIR)/queue_helper -d1 -c 'bsub -- make $$target' $< ALL_ALIGN $(SPECIES1) $(SPECIES2) $(ALIGN_TYPE)

# symalign simmetrizza gli allineamenti e toglie quelli interni ad un singolo introne
alignments.$(ALIGN_TYPE).gz: $(ALL_ALIGN)
	set -e; \
	find . -name '$(SPECIES1)_*'.$(ALIGN_TYPE).gz \
	| xargs zcat \
	| $(BIN_DIR)/symalign \
	| sort -S40% -k1,1 -k2,2n -k3,3n -k5,5 -k6,6n -k7,7n \
	| gzip >$@

