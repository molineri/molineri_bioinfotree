include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

all: random.fa.splitted random.fa.splitted.idx random.fa.splitted.len random.fa.splitted.xns target.fa.splitted target.fa.splitted.idx  target.fa.splitted.len target.fa.splitted.xns

clean.full:
	rm -f random.fa.splitted random.fa.splitted.idx random.fa.splitted.len random.fa.splitted.xns target.fa.splitted target.fa.splitted.idx  target.fa.splitted.len target.fa.splitted.xns

random.fa.splitted: $(SEQUENCE_QUERY_FILE)
	$(BIN_DIR)/random_fasta < $< > $@

target.fa.splitted: $(TARGET_SEQUENCE_FILE)
	cp $< $@

%.fa.splitted.idx: %.fa.splitted
	fastalen $< >$@

%.fa.splitted.len: %.fa.splitted.idx
	cut -f2 $< \
	| sum_column	>$@

%.fa.splitted.xns: %.fa.splitted
	xdformat -n -o $< $<

