#!/usr/bin/perl
use warnings;
use strict;
use Data::Dumper;

$\="\n";

my $id_col = shift @ARGV;
my $group_col = shift @ARGV;
my $val_col = shift @ARGV;

die if !$id_col or !$val_col or !$group_col;
$id_col--;
$group_col--;
$val_col--;

my %data=();
my %meta_data=();
while(<>){
	chomp;
	my @F=split;
	my $id=$F[$id_col];
	my $group=$F[$group_col];
	my $val=$F[$val_col];
	
	push @{$data{$id}{$group}}, $_;
	
	$meta_data{$id}{$group} = $val if(!defined($meta_data{$id}{$group}) or $meta_data{$id}{$group} < $val);
}

foreach my $id (keys(%meta_data)){
	my $group=&find_max($meta_data{$id});
	foreach my $r (@{$data{$id}{$group}}){
		print $r;
	}
}


sub find_max
{
	my $ref = shift;
	my $retval=undef;
	my $val=undef;
	for(keys(%{$ref})){
		if(!defined($val) or $val > ${$ref}{$_}){
			$val=${$ref}{$_};
			$retval=$_;
		}
	}
	return $retval;
}
