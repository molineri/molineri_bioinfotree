#!/bin/env python
from optparse import OptionParser
from os import popen
from sys import stdout

class Reader(object):
	def __init__(self,sources):
		self.sources = sources
	
	def get_block_single_file(fd):
		max_stop=None
		out = []
		for line in fd:
			f = parse(line)
			if max_stop is None or f.start <= max_stop:
				out.append(f)
			else:
				yield out
				out=[f]
			max_stop = max(max_stop, f.stop)
	
	def get_block():
		out=[]
		while 1:
			sn = 0
			for s in self.sources:
				for line in s:
					if :
						out.append(line)
						sources_last_line[sn] = None
					else:
						sources_last_line[sn] = line
						break
				print
				sn  = sn + 1
				
			if len(out) == 0:
				return
			else:
				yield out
				for l in sources_last_line:
					if l is not None:
						out.append(l)

if __name__ == '__main__':
	parser = OptionParser(usage='%prog [DATA_FILES]')
	options, args = parser.parse_args()

	if len(args) < 1:
		exit('Unexpected argument number.')
	
	cmds = [ 'zcat ' + x for x in args ]
	sources = [ popen(x) for x in cmds ]

	i=0
	for block in get_block(sources):
		if i>5:
			break;
		print '\n---------- Block %i ---------\n' % i
		print ''.join(block)
		i=i+1
