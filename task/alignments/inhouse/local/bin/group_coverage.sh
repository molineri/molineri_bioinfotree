#!/bin/bash
cut -f 4-6 | sort -t"	" -k 2,2n -k 3,3n | merge_segments | awk -F "\t" '{sum+=$3-$2} END{print sum}'
