#!/usr/bin/perl

use warnings;
use strict;
use IPC::Open2;
$,="\t";
$\="\n";

my $seq_dir = shift @ARGV;

die("ERROR: seqdir not defined") if !defined($seq_dir);

while(<>){
	chomp;
	print ">$_";
	my @F=split /\t/;
	my $RDR;
	my $WTR;
	my $pid;
	$pid = open2($RDR, $WTR, "seqlist2fasta -i $seq_dir");
	print $WTR @F[1..3];
	close $WTR;
	my $seq='';
	while(<$RDR>){
		chomp;
		next if m/^>/;
		$seq1.=$_;
	}
	
	if($F[2]>$F[3]){
		$seq = reverse $seq1;
		$seq =~ tr/ACGT/TGCA/;
	}
	
	$seq = &insert_gap($seq,$F[4]) if defined($F[4]);
	print @F,$seq;
	
}

sub insert_gap
{
	my $a=shift;
	my $g=shift;
	my @gaps_start  = ($g =~ /(\d+),\d+;*/g);
	my @gaps_length = ($g =~ /\d+,(\d+);*/g);
	for(my $i=0; $i<scalar(@gaps_start); $i++){
		my $r='';
		for(1..$gaps_length[$i]){
			$r.='-';
		}
		my $pos = $gaps_start[$i];
		substr($a,$pos,0) = $r;
	}
	return $a;
}
