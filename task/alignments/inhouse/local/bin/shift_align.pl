#!/usr/bin/perl
use warnings;
use strict;
$,="\t";
$\="\n";

my $up_coords = shift @ARGV;

open UP_COORDS , $up_coords or die("Can't open file ($up_coords)");

my %coords=();
while(<UP_COORDS>){
	chomp;
	my @F=split;
	my ($id1, $chr1, $b1, $e1, $id2, $chr2, $b2, $e2 ) = split;
	my @tmp1=($chr1, $b1, $e1);
	my @tmp2=($chr2, $b2, $e2);


	$coords{$id1}=\@tmp1;
	$coords{$id2}=\@tmp2;
}

while(<>){
	chomp;
	my ($id1, $b1, $e1, $id2, $b2, $e2, $s1, $s2, $s3, $g1, $g2 ) = split;
	
	die("No coordinates for id ($id1)") if !defined($coords{$id1});
	die("No coordinates for id ($id2)") if !defined($coords{$id2});
	my ($chr1, $offset1, $ceck1)=@{$coords{$id1}};
	my ($chr2, $offset2, $ceck2)=@{$coords{$id2}};

	die("Overflow in coordinates") if $e1 + $offset1 > $ceck1;
	die("Overflow in coordinates") if $e2 + $offset2 > $ceck2;
	
	print 	$id1, $chr1, $b1 + $offset1, $e1 + $offset1, 
		$id2, $chr2, $b2 + $offset2, $e2 + $offset2,
		$s1, $s2, $s3,
		&shift_gap($g1, $offset1),
		&shift_gap($g2, $offset2);
}

sub shift_gap
{
	my $g=shift;
	return if !$g;
	my $offset=shift;
	my @gaps=split /;/, $g;
	my @retval=();
	for(@gaps){
		my ($b, $l)=split /,/;
		$b+=$offset;
		push @retval, "$b,$l";
	}
	return join(';',@retval);
}
