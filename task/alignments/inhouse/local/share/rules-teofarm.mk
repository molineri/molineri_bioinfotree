SHELL = /bin/bash
BIN_DIR := $(BIOINFO_ROOT)/task/alignments/inhouse/local/bin
BIN_DIR_PARENT := $(BIOINFO_ROOT)/task/alignments/local/bin

MEGA_FLAGS ?= -W 11 -t 16 -N 2 -g T -F T -f T -m 8 -e 10000000 -s 16 -z 1 -U -M 3000000000 -v 1000000000 -b 1000000000 -H 0
megablast_DEPS=$(SEQ_DIR1)/$${chrs[$$i]}.fa $(SEQ_DIR1)/$${chrs[$$i1]}.fa.splitted $(SEQ_DIR2)/$${chrs[$$j]}.fa $(SEQ_DIR2)/$${chrs[$$j]}.fa.nsq

CHAOS_FLAGS ?= -b
chaos_DEPS=$(SEQ_DIR1)/$${chrs[$$i]}.fa $(SEQ_DIR2)/$${chrs[$$j]}.fa

WUBLAST_FLAGS ?= W=10 matrix=pam10 Q=2046 R=18 S2=2232 gapS2=2232 S=2232 Y=3080436051 Z=3080436051 kap hspmax=0 cpus=1
wublast_DEPS=$(SEQ_DIR1)/$${chrs[$$i]}.fa $(SEQ_DIR1)/$${chrs[$$i]}.fa.splitted $(SEQ_DIR2)/$${chrs[$$j]}.fa $(SEQ_DIR2)/$${chrs[$$j]}.fa.xns

COVERAGE_TRIGGER_LEVEL ?= 1
COVERAGE_TRIGGER_LEVELS = 1 2 3 4 5 10 15 20 25 50

.PHONY: all clean \
        all.$(ALIG_TYPE) clean.$(ALIGN_TYPE) \
        all.coverage all.$(SPECIES1).coverage all.$(SPECIES2).coverage \
            clean.coverage clean.$(SPECIES1).coverage clean.$(SPECIES2).coverage \
        all.coverage.triggered all.$(SPECIES1).coverage.triggered all.$(SPECIES2).coverage.triggered \
            clean.coverage.triggered clean.$(SPECIES1).coverage.triggered clean.$(SPECIES2).coverage.triggered \
        all.coverage.triggered_multi clean.coverage_triggered.multi \
        all.coverage.triggered.annote all.$(SPECIES1).coverage.triggered.annote all.$(SPECIES2).coverage.triggered.annote \
            clean.coverage.triggered.annote clean.$(SPECIES1).coverage.triggered.annote clean.$(SPECIES2).coverage.triggered.annote

.SECONDARY:
.DELETE_ON_ERROR:

all : all.$(ALIGN_TYPE)
clean : clean.$(ALIGN_TYPE)
distclean : clean
	rm -f chrs.combo chromosomes-1.list chromosomes-2.list
include chrs.combo

chromosomes-%.list : makefile rules.mk $(SEQ_DIR1)/makefile $(SEQ_DIR2)/makefile
	grep '^ALL_CHROMOSOMES\s*.*=' $($(addsuffix $*, SEQ_DIR))/makefile | sed 's|^ALL_CHROMOSOMES\s*.*=\s*||' | sed 's|\.fa||g' >$@

chrs.combo : rules.mk chromosomes-1.list chromosomes-2.list
	( \
		targets=""; \
		pdb_targets=""; \
		coverage_targets=""; \
		if [ $(SEQ_DIR1) == $(SEQ_DIR2) ]; then \
			chrs=( $$(cat chromosomes-1.list) ); \
			for ((i=0; i<$${#chrs[*]}; i=$$i+1)); do \
				for ((j=$$i; j<$${#chrs[*]}; j=$$j+1)); do \
					target="$(SPECIES1)_$${chrs[$$i]}_$(SPECIES1)_$${chrs[$$j]}.$(ALIGN_TYPE).gz"; \
					targets="$$targets $$target"; \
					pdb_targets="$$pdb_targets $${target%%.gz}.pdb.gz"; \
					echo -n "$$target : "; \
					echo "$($(ALIGN_TYPE)_DEPS)"; \
					echo -e "\t\$$(call $(ALIGN_TYPE)_single_run)"; \
				done; \
			done; \
		else \
			for chr1 in $$(cat chromosomes-1.list); do \
				for chr2 in $$(cat chromosomes-2.list); do \
					target="$(SPECIES1)_$${chr1}_$(SPECIES2)_$${chr2}.$(ALIGN_TYPE).gz"; \
					targets="$$targets $$target"; \
					pdb_targets="$$pdb_targets $${target%%.gz}.pdb.gz"; \
					echo -n "$$target : "; \
					echo "$($(ALIGN_TYPE)_DEPS)"; \
					echo -e "\t\$$(call $(ALIGN_TYPE)_single_run)"; \
				done; \
			done; \
			for chr in $$(cat chromosomes-2.list); do \
				coverage_targets2="$$coverage_targets $(SPECIES2)_$${chr}.coverage"; \
				echo -n "$(SPECIES2)_$${chr}.coverage : "; \
				tr " " "\n" <<<$${targets} | grep "$(SPECIES2)_$${chr}[_.]" | tr "\n" " "; \
				echo; \
			done; \
		fi; \
		for chr in $$(cat chromosomes-1.list); do \
			coverage_targets="$$coverage_targets $(SPECIES1)_$${chr}.coverage"; \
			echo -n "$(SPECIES1)_$${chr}.coverage : "; \
			tr " " "\n" <<<$${targets} | grep "$(SPECIES1)_$${chr}[_.]" | tr "\n" " "; \
			echo; \
		done; \
		coverage_triggered_multi_targets=""; \
		for coverage_target in $$coverage_targets; do \
			for level in $(COVERAGE_TRIGGER_LEVELS); do \
				target="$$coverage_target.triggered_$$level"; \
				coverage_triggered_multi_targets="$$coverage_triggered_multi_targets $$target"; \
				echo "$$target: $$coverage_target"; \
				echo -e "\t\$$(call coverage_trigger_by_level $${level})"; \
			done; \
		done; \
		echo "all.$(ALIGN_TYPE) : $$targets"; \
		echo "clean.$(ALIGN_TYPE) :"; \
		echo -e "\trm -f $$targets"; \
		echo -e "\trm -f $$(echo $$targets | sed 's|.gz|.log|g')";\
		echo "all.pdb : $$pdb_targets"; \
		echo "clean.pdb :"; \
		echo -e "\trm -f $$pdb_targets"; \
		echo "all.coverage.triggered_multi : $$coverage_triggered_multi_targets"; \
		echo "clean.coverage.triggered_multi :"; \
		echo -e "\trm -f $$coverage_triggered_multi_targets"; \
		echo "ALL_CHRS1=$$(cat chromosomes-1.list)"; \
		echo "ALL_CHRS2=$$(cat chromosomes-2.list)"; \
	) >$@

%.fa %.splitted %.fa.nsq %.fa.xns:
	cd $$(dirname $@) && $(MAKE) $$(basename $@)

define megablast_single_run
	logname=$(patsubst %.gz,%.log,$@); \
	( \
		set -o pipefail; \
		set -e; \
		if [ "$(word 4,$^)" == "" ]; then \
			database=$(word 1,$^); \
		else \
			database=$(word 3,$^); \
		fi; \
		echo "starting at $$(date) on host $$(hostname --fqdn)"; \
		echo "running megablast on $(word 2,$^) and $$database"; \
		echo "using the following flags: $(MEGA_FLAGS)"; \
		( \
			repeat_fasta_pipe "megablast $(MEGA_FLAGS) -i /dev/stdin -d $$database" <$(word 2,$^) \
			| gzip >$@; \
		) 2>&1 \
		| head -n50 \
		| while read line; do echo "[ERR] $$line"; done; \
		if [ -e error.log ]; then \
			cat error.log | while read line; do echo "[ERR] $$line"; done; \
			rm -f error.log; \
		fi; \
		exit_code=$$?; \
		if [ $$exit_code -ne 0 ]; then \
			echo "aborted at $$(date) with code $$exit_code"; \
		else \
			echo "completed at $$(date)"; \
		fi; \
		exit $$exit_code; \
	) >$$logname; \
	exit $$?
endef

define wublast_single_run
	logname=$(patsubst %.gz,%.log,$@); \
	( \
		set -o pipefail; \
		set -e; \
		if [ "$(word 4,$^)" == "" ]; then \
			database=$(word 1,$^); \
		else \
			database=$(word 3,$^); \
		fi; \
		echo "starting at $$(date) on host $$(hostname --fqdn)"; \
		echo "running wublast on $(word 2,$^) and $$database"; \
		echo "using the following flags: $(WUBLAST_FLAGS)"; \
		( \
			source $$(which wublast-env); \
			echo "matrix path: $$WUBLASTMAT"; \
			blastn $$database $(word 2,$^) $(WUBLAST_FLAGS) \
			| $(BIN_DIR)/wublast_parser_pipe \
			| gzip >$@; \
		) 2>&1 \
		| while read line; do echo "[ERR] $$line"; done; \
		exit_code=$$?; \
		if [ $$exit_code -ne 0 ]; then \
			echo "aborted at $$(date) with code $$exit_code"; \
		else \
			echo "completed at $$(date)"; \
		fi; \
		exit $$exit_code; \
	) >$$logname; \
	exit $$?
endef

define chaos_single_run
	(source /home_PG/molineri/local/bin/chaos_env; /home_PG/molineri/local/bin/chaos $^ $(CHAOS_FLAGS) | gzip >$@ ) 2>$@.err
endef

%.$(ALIGN_TYPE).pdb.gz: %.$(ALIGN_TYPE).gz
	set -o pipefail; \
	set -e; \
	pdb_opt=""; \
	if [ "$(SPECIES1)" == "$(SPECIES2)" ]; then \
		pdb_opt="$$pdb_opt -s"; \
	fi; \
	zcat $< | sed 's|^$(SPECIES1)_||' | $(BIN_DIR)/per_database $$pdb_opts | gzip >$@

########################################
#
#	STATISTICA
#
########################################

ALL_COVERAGE1=$(addprefix $(SPECIES1)_, $(addsuffix .coverage, $(ALL_CHRS1)))
ALL_COVERAGE2=$(addprefix $(SPECIES2)_, $(addsuffix .coverage, $(ALL_CHRS2)))

all.$(SPECIES1).coverage: $(ALL_COVERAGE1)
all.$(SPECIES2).coverage: $(ALL_COVERAGE2)
all.coverage : all.$(SPECIES1).coverage all.$(SPECIES2).coverage
clean.$(SPECIES1).coverage:
	rm -f $(ALL_COVERAGE1)
clean.$(SPECIES2).coverage:
	rm -f $(ALL_COVERAGE2)
clean : clean.$(SPECIES1).coverage clean.$(SPECIES2).coverage

%.coverage:
	set -e; \
	( \
		for i in $*_*.$(ALIGN_TYPE).per_database.gz; do \
			[ "$$i" != "$*_*.per_database.gz" ] || break; \
			echo "Processing $$i" >&2; \
			zcat $$i | cut -f 2,3,10; \
		done; \
		for i in *_$*.$(ALIGN_TYPE).per_database.gz; do \
			[ "$$i" != "*_$*.per_database.gz" ] || break; \
			echo "Processing $$i" >&2; \
			zcat $$i | cut -f 6,7,10; \
		done; \
	) \
		| awk '$$3>50 {print $$1 "\t" $$2}' \
		| $(BIN_DIR_PARENT)/coverage > $@

ALL_COVERAGE_TRIGGERED1=$(addsuffix .triggered, $(ALL_COVERAGE1))
ALL_COVERAGE_TRIGGERED2=$(addsuffix .triggered, $(ALL_COVERAGE2))

all.$(SPECIES1).coverage.triggered: $(ALL_COVERAGE_TRIGGERED1)
all.$(SPECIES2).coverage.triggered: $(ALL_COVERAGE_TRIGGERED2)
all.coverage.triggered : all.$(SPECIES1).coverage.triggered all.$(SPECIES2).coverage.triggered
clean.$(SPECIES1).coverage.triggered:
	rm -f $(ALL_COVERAGE_TRIGGERED1)
clean.$(SPECIES2).coverage.triggered:
	rm -f $(ALL_COVERAGE_TRIGGERED2)
clean: clean.$(SPECIES1).coverage.triggered clean.$(SPECIES2).coverage.triggered

%.coverage.triggered : %.coverage
	$(BIN_DIR_PARENT)/coverage_trigger $(COVERAGE_TRIGGER_LEVEL) <$< >$@

ALL_COVERAGE_TRIGGERED_ANNOTE1=$(addsuffix .annote, $(ALL_COVERAGE_TRIGGERED1))
ALL_COVERAGE_TRIGGERED_ANNOTE2=$(addsuffix .annote, $(ALL_COVERAGE_TRIGGERED2))

all.$(SPECIES1).coverage.triggered.annote: $(ALL_COVERAGE_TRIGGERED_ANNOTE1)
all.$(SPECIES2).coverage.triggered.annote: $(ALL_COVERAGE_TRIGGERED_ANNOTE2)
all.coverage.triggered.annote: all.$(SPECIES1).coverage.triggered.annote all.$(SPECIES2).coverage.triggered.annote
clean.$(SPECIES1).coverage.triggered.annote:
	rf -f $(ALL_COVERAGE_TRIGGERED_ANNOTE1)
clean.$(SPECIES2).coverage.triggered.annote:
	rf -f $(ALL_COVERAGE_TRIGGERED_ANNOTE2)
clean.coverage.triggered.annote: clean.$(SPECIES1).coverage.triggered.annote clean.$(SPECIES2).coverage.triggered.annote

%.coverage.triggered.annote : %.coverage.triggered
	chr=$*; chr=$${chr##*_chr}; perl -ne "print \"$$chr\t\",\$$_" $< > $@.tmp
	$(BIOINFO_ROOT)/task/annotations/local/bin/annote \
		$(addprefix -a $(BIOINFO_ROOT)/task/annotations/dataset/ensembl38/homo/coords_nonredundant., $(subst chr,, $(ALL_CHRS1))) \
		-g \
		$@.tmp\
	> $@
	rm -f $@.tmp

clean: clean.coverage.triggered_multi

define coverage_trigger_by_level
	$(BIN_DIR_PARENT)/coverage_trigger $1 <$< >$@
endef

# coverage_level.stats: all.coverage.triggered_multi
# 	(for i in $(COVERAGE_TRIGGER_LEVELS); do\
# 		echo -ne "$$i\t";\
# 		awk '{print $$2-$$1}' $(SPECIES1)_*.coverage.triggered_$$i | sum_column;\
# 	done) > $@
