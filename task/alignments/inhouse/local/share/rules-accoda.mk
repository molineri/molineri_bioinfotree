
all_accoda_ssh:
	for i in $$(grep -w all.$(ALIGN_TYPE) chrs.combo  | cut -d' ' -f 3-); do\
		accoda_ssh make $$i;\
	done;

all_accoda:
	for i in $$(grep -w all.$(ALIGN_TYPE) chrs.combo  | cut -d' ' -f 3-); do\
		accoda --name `sed s/$(SPECIES1)// <<<$$i \
			| sed s/$(SPECIES2)// \
			| sed s/$(ALIGN_TYPE)// \
			| sed s/_//g\
			| sed s/..gz//`\
		make $$i;\
	done;
