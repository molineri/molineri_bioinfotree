QUERY_COORDS  ?= $(BIOINFO_ROOT)/task/sinteny/dataset/homo_megablast_38/new_gene_exons
QUERY_SEQ_DIR ?= $(BIOINFO_ROOT)/task/sequences/dataset/ensembl/hsapiens/38
DATABASE      ?= $(BIOINFO_ROOT)/task/sequences_prot/dataset/swissprot/human_protein.fa.xps
ALIGN_TYPE    ?=
LABEL_FILTER  ?= | sed 's|^>\(\S*\).*|>\1|'

.PHONY: all clean distclean
.DELETE_ON_ERROR:
.SECONDARY: alignments.$(ALIGN_TYPE).gz
.INTERMEDIATE: query.fa.len

COVERAGE_BIN_DIR := $(BIOINFO_ROOT)/task/align_coverage/local/bin

all: alignments.$(ALIGN_TYPE).gz superpositions.txt
clean:
	rm -f query.fa
distclean: clean
	rm -f alignments.$(ALIGN_TYPE).gz superpositions.txt

include rules-algo.mk

%.fa.len: %.fa
	fastalen $< \
	| sort -k1,1 >$@

query.fa: $(QUERY_COORDS)
	sort -k2,2 $< \
	| seqlist2fasta $(QUERY_SEQ_DIR) \
	$(LABEL_FILTER) >$@

alignments.megablast.gz: query.fa $(DATABASE)
	$(call megablast_single_run)

alignments.wublast.gz: query.fa $(DATABASE)
	$(call wublast_single_run)

alignments.wublastNoparse.gz: query.fa $(DATABASE)
	$(call wublastNoparse_single_run)

alignments.wublastParse.gz: alignments.wublastNoparse.gz
	zcat $< | grep -vF Links \
	| $(BIN_DIR)/wublast_parser_pipe -g -s\
	| sort -k 1,1n -k 6,6 -k 5,5n -k 2,2n \
	$(OUTPUT_FILTER) \
	| gzip >$@; \

alignments.wublastx.gz: query.fa $(DATABASE)
	$(call wublastx_single_run)

alignments.lalign.gz: query.fa $(DATABASE)
	$(call lalign_single_run)

superpositions.txt: alignments.$(ALIGN_TYPE).gz query.fa.len
	zcat $< \
	| sort -S40% -k1,1 -k5,5 \
	| repeat_group_pipe 'echo -ne "$$1\t$$2\t"; \
	                     cut -f2,3 \
			     | sort -S40% -k1,1n \
			     | union -c \
			     | awk '\''BEGIN{sum=0}{ sum += ($$$$2-$$$$1) } END{print sum}'\' 1 5 \
	| join3_pl -i -u -1 1 -2 1 - $(word 2,$^) >$@

