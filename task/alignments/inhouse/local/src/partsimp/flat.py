from sys import argv, stdout

def readPalindromi(filename):
	map = {}

	fd = file(filename)
	for line in fd:
		items = line.split()
		assert len(items) == 3
		
		lst = map.get(items[0], [])
		lst.append( (int(items[1]), int(items[2])) )
		map[items[0]] = lst
	
	fd.close()
	
	return map

if __name__ == '__main__':
	palindromi = readPalindromi(argv[1])
	
	fd = file(argv[2])
	for line in fd:
		items = line.split()
		
		qchr, qstart, qstop = items[0], int(items[1]), int(items[2])
		for start, stop in palindromi.get(qchr, []):
			if qstart <= start and qstop >= stop:
				info = [qchr, str(start), str(stop)] + items
				print '\t'.join(info)
				stdout.flush()
		
		tchr, tstart, tstop = items[4], int(items[5]), int(items[6])
		for start, stop in palindromi.get(tchr, []):
			if tstart <= start and tstop >= stop:
				info = [tchr, str(start), str(stop)] + items
				print '\t'.join(info)
				stdout.flush()
	
	fd.close()
