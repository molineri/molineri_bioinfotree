#!/bin/bash

for i in tables/*; do
	job=partsimp-${i##*/}
	accoda.pl --name $job "python flat.py palindromi $i > $job.out"
done

