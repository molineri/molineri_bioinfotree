#!/usr/bin/env python
# encoding: utf-8

from optparse import OptionParser
from sys import exit, stdin

def iter_fasta(fd):
	start_lineno = 0
	aux = None
	for lineno, line in enumerate(fd):
		if line.startswith('>'):
			if aux is not None:
				yield start_lineno+1, aux
				start_lineno = lineno
			aux = []
		else:
			aux.append(line.rstrip())
	yield aux

def parse_strand(line):
	tokens = line.split('\t')
	if len(tokens) <= 4:
		raise ValueError
	
	y_start = int(tokens[2])
	y_stop = int(tokens[3])
	if y_start > y_stop:
		return -1
	else:
		return +1

def print_counters(prefix, counters):
	items = counters.items()
	items.sort()
	for k, v in items:
		print '%s%d\t%d' % (prefix, k, v)

def main():
	parser = OptionParser(usage='%prog <LOC_CL')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	plus_counters = {}
	minus_counters = {}

	for start_lineno, lines in iter_fasta(stdin):
		try:
			strands = [ parse_strand(l) for l in lines ]
		except ValueError:
			exit('Malformed input in fasta block starting at line %d.' % start_lineno)
		
		for block_len in xrange(1, len(strands)+1):
			idx_max = len(strands) - block_len + 1
			for idx in xrange(idx_max):
				first_strand = strands[idx]
				for idx2 in xrange(idx+1, len(strands)):
					if strands[idx2] != first_strand:
						break
				else:
					if first_strand > 0:
						plus_counters[block_len] = plus_counters.get(block_len, 0) + 1
					else:
						minus_counters[block_len] = minus_counters.get(block_len, 0) + 1
	
	print_counters('+\t', plus_counters)
	print_counters('-\t', minus_counters)

if __name__ == '__main__':
	main()
