#!/usr/bin/env python
from glob import glob
from gzip import GzipFile
from optparse import OptionParser
from os import chdir, popen, system
from sys import argv, exit, stdin
from vfork.io.util import safe_rstrip

def main():
        parser = OptionParser(usage='%prog SEQ_ID ALIGN_DIR')
	parser.add_option('-S', '--buffer-size', dest='sort_buffer', default='10%', help='value passed verbatim to sort (default: 10%)')
        options, args = parser.parse_args()

	if len(args) != 2:
		exit('%s: unexpected argument number' % argv[0])
		
	chdir(args[1])
	sort_fd = popen('sort -k2,2n -k3,3n -S%s' % options.sort_buffer, 'w')

	processed_files = []
	for filename in glob('*_chr%s_*.pdb.gz' % args[0]):
		processed_files.append(filename)

		fd = GzipFile(filename,'r')
		for lineno, line in enumerate(fd):
			tokens = safe_rstrip(line).split('\t')
			if len(tokens) != 13:
				exit('%s: wrong column number at line %d in file %s' % (argv[0], filename, lineno+1))

			print >>sort_fd, '\t'.join(tokens[:11])

	for filename in glob('*_chr%s.*.pdb.gz' % args[0]):
		if filename in processed_files:
			continue
		
		fd = GzipFile(filename,'r')
		for lineno, line in enumerate(fd):
			tokens = safe_rstrip(line).split('\t')
			if len(tokens) != 13:
				exit('%s: wrong column number at line %d in file %s' % (argv[0], filename, lineno+1))

			tokens[0:3],tokens[4:7] = tokens[4:7],tokens[0:3]
			#tokens[11], tokens[12] = tokens[12], tokens[11]
			print >>sort_fd, '\t'.join(tokens[:11])
	
	if sort_fd.close() is not None:
		exit('%s: error sorting output' % argv[0])

if __name__ == '__main__':
	main()
