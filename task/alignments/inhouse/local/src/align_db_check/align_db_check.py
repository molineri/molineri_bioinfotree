#!/usr/bin/env python
from optparse import OptionParser
from os import listdir, popen, system
from os.path import isdir, isfile, join
from sys import exit, stderr
import re

def same_chr(dirname):
	m = re.match('(\S+)chr([^_]+)_(\S+)chr([^_]+)', dirname)
	return m.group(1) == m.group(3) and m.group(2) == m.group(4)

def detect_zcat():
	template = 'which %s >&/dev/null'
	
	for name in ('gzcat', 'zcat'):
		res = system(template % name)
		if res == 0:
			return name
	
	raise RuntimeError, "can't find zcat"

def cmp_rownum(src, dst, same_chr):
	zcat_cmd = detect_zcat()

	src_fd = popen('%s %s 2>/dev/null' % (zcat_cmd, src))
	dst_fd = popen('%s %s 2>/dev/null' % (zcat_cmd, dst))
	
	src_eof = False
	res = True
	
	while not src_eof:
		src_eof = len(src_fd.readline()) == 0
		dst_eof = len(dst_fd.readline()) == 0
		if src_eof != dst_eof:
			res = same_chr and not src_eof and dst_eof
			break
	
	src_close = src_fd.close()
	dst_close = dst_fd.close()
	return res and (not src_eof or src_close is None) and (not dst_eof or dst_close is None)

if __name__ == '__main__':
	parser = OptionParser(usage='%prog ALIGNMENT_DIR')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit("Unexpected argument number.")
	
	for item in listdir(args[0]):
		if not isdir(join(args[0], item)):
			continue
		
		src_file = join(args[0], item, 'blast.gz')
		if not isfile(src_file):
			continue
		
		dst_file = join(args[0], item, 'per_database.gz')
		if not isfile(dst_file):
			print >>stderr, "Warning: the directory %s is missing per_database.gz" % item
			continue

		if not cmp_rownum(src_file, dst_file, same_chr(item)):
			print item
