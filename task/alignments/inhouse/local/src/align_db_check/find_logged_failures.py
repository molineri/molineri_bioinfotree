#!/usr/bin/env python
from glob import glob

def main():
	for name in glob('*.log'):
		fd = file(name, 'r')
		content = fd.read()
		fd.close()
		if 'code 0' not in content:
			print name

if __name__ == '__main__':
	main()
