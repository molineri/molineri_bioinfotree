#!/usr/bin/env python

from optparse import OptionParser
from os import environ, link
from os.path import isdir, isfile, join
from regions import ConservedRegions, RegionList, RegionMap
from sys import exit
import re

def slurp_file(path):
	try:
		fd = file(path, 'r')
		content = fd.read()
		fd.close()
		return content
	except IOError:
		exit('Error reading file %s' % path)

env_var_rx = re.compile(r'\$\(([a-zA-Z_]+)\)')
def expand_env_vars(value):
	def expand_var(m):
		return environ.get(m.group(1), '')
	return env_var_rx.sub(expand_var, value)

def get_var(varname, source, source_path):
	m = re.search(r'^%s\s*:?=\s*(.*)$' % varname, source, re.M)
	if m is None:
		exit('Error searching variable %s in file %s' % (varname, source_path))
	else:
		return expand_env_vars(m.group(1))

def scan_align_dir(path):
	makefile_path = join(path, 'makefile')
	makefile = slurp_file(makefile_path)
	
	species1 = get_var('SPECIES1', makefile, makefile_path)
	species2 = get_var('SPECIES2', makefile, makefile_path)
	seqdir1 = get_var('SEQDIR1', makefile, makefile_path)
	seqdir2 = get_var('SEQDIR2', makefile, makefile_path)
	align_type = get_var('ALIGN_TYPE', makefile, makefile_path)

	return (species1, seqdir1, species2, seqdir2, align_type)

def check_align_info(align_info1, align_info2):
	if align_info1[0] == align_info1[2] and align_info1[1] != align_info1[3]:
		exit('Inconsistency between species declaration and sequence directories in first alignment directory.')
	if align_info2[0] == align_info2[2] and align_info2[1] != align_info2[3]:
		exit('Inconsistency between species declaration and sequence directories in second alignment directory.')

	for idx, label in [(0, 'first species'), (2, 'second species'), (4, 'alignment type')]:
		if align_info1[idx] != align_info2[idx]:
			exit('Inconsistency between %s declaration in alignment dirs.' % label)
	
	for path in (align_info1[1], align_info1[3], align_info2[1], align_info2[3]):
		if not isdir(path):
			exit('Missing sequence directory %s' % path)

def scan_sequence_dir(path):
	makefile_path = join(path, 'makefile')
	makefile = slurp_file(makefile_path)
	
	sequences = get_var('ALL_DNA', makefile, makefile_path).split(None)
	return (path, sequences)

def check_sequence_info(info):
	for seq in info[1]:
		path = join(info[0], seq + '.fa.splitted')
		if not isfile(path):
			exit('Missing sequence file %s' % path)
		if not isfile(path + '.idx'):
			exit('Missing sequence index file %s' % path)

class SequencePairs(object):
	def __init__(self, seqs11, seqs12, seqs21, seqs22, same_species):
		self.same_species = same_species
		self.first = self._find_common_subset(seqs11, seqs12, 'first species')
		if not self.same_species:
			self.second = self._find_common_subset(seqs21, seqs22, 'second species')
	
	def __iter__(self):
		if self.same_species:
			for idx, seq1 in enumerate(self.first):
				for seq2 in self.first[idx:]:
					yield seq1, seq2
		else:
			for seq1 in self.first:
				for seq2 in self.second:
					yield seq1, seq2
	
	def _find_common_subset(self, a, b, label):
		sa = set(a)
		for seq in b:
			if seq not in a:
				exit('Inconsistency detected between sequence sets of %s.' % label)
		return b

def check_pdbs(align_dir, sequence_pairs, build_pdb_name):
	for seq1, seq2 in sequence_pairs:
		pdb_path = join(align_dir, build_pdb_name(seq1, seq2))
		if not isfile(pdb_path):
			exit('Missing PDB file %s' % pdb_path)

def load_conservation(sequence_name, sequence_path1, sequence_path2, conservation_map_path):
	try:
		m = RegionMap()
		m.load(join(conservation_map_path, sequence_name + '.blocks'))
		if m.identity:
			return 'identity'
		
		r1 = RegionList()
		r1.load(join(sequence_path1, sequence_name + '.fa.splitted.idx'))
		r2 = RegionList()
		r2.load(join(sequence_path2, sequence_name + '.fa.splitted.idx'))
		
		return ConservedRegions(r1, r2, m)
		
	except ValueError, e:
		exit('Error loading conservation info: %s' % e.msg[0])

def main():
	parser = OptionParser(usage='%prog ALIGNDIR1 ALIGNDIR2 GENOME_CMP_DIR')
	options, args = parser.parse_args()
	
	if len(args) != 3:
		exit('Unexpected argument number.')
	
	align_info1 = scan_align_dir(args[0])
	align_info2 = scan_align_dir(args[1])
	check_align_info(align_info1, align_info2)
	
	sequence_info11 = scan_sequence_dir(align_info1[1])
	check_sequence_info(sequence_info11)
	sequence_info12 = scan_sequence_dir(align_info1[3])
	check_sequence_info(sequence_info12)
	sequence_info21 = scan_sequence_dir(align_info2[1])
	check_sequence_info(sequence_info21)
	sequence_info22 = scan_sequence_dir(align_info2[3])
	check_sequence_info(sequence_info22)
	
	sequence_pairs = SequencePairs(sequence_info11[1], sequence_info12[1], sequence_info21[1], sequence_info22[1], align_info2[0] == align_info2[2])
	def build_pdb_name(seq1, seq2):
		return '%s_%s_%s_%s.%s.pdb.gz' % (align_info2[0], seq1, align_info2[2], seq2, align_info2[4])
	check_pdbs(args[1], sequence_pairs, build_pdb_name)
	
	for seq1, seq2 in sequence_pairs:
		conserv1 = load_conservation(seq1, sequence_info11[0], sequence_info12[0], args[2])
		conserv2 = load_conservation(seq2, sequence_info21[0], sequence_info22[0], args[2])
		
		pdb_name = build_pdb_name(seq1, seq2)
		src_path = join(args[0], pdb_name)
		if conserv1 == conserv2 == 'identity':
			#link(src_path, join(args[1], pdb_name)
			print 'linking %s with %s' % (src_path, join(args[1], pdb_name))

if __name__ == '__main__':
	main()
