from __future__ import with_statement

class RegionList(object):
	def load(self, filename):
		self.seq = None
		self.regions = []

		with file(filename, 'r') as fd:
			last_start = 0
			for idx, line in enumerate(fd):
				lineno = idx + 1
				chr, start, stop = self._parse_entry(line, lineno, filename)
				
				if self.seq is None:
					self.seq = chr
				elif chr != self.seq:
					raise ValueError, 'invalid chromosome in region list at line %d of file %s' % (lineno, filename)
				
				if start < last_start:
					raise ValueError, 'disorder in region list at line %d of file %s' % (lineno, filename)
				else:
					last_start = start

				self.regions.append((start, stop, idx))
	
	def _parse_entry(self, line, lineno, filename):
		tokens = line.split(None)
		if len(tokens) != 4:
			raise ValueError, 'invalid token number found at line %d of file %s' % (lineno, filename)

		label = tokens[0].split('_')
		if len(label) != 2:
			raise ValueError, 'invalid token number found at line %d of file %s' % (lineno, filename)

		chr = label[0]

		try:
			start = int(label[1])
			if start < 0:
				raise ValueError
		except ValueError:
			raise ValueError, 'invalid region start coordinate at line %d of file %s' % (lineno, filename)

		try:
			length = int(tokens[1])
			if length <= 0:
				raise ValueError
		except ValueError:
			raise ValueError, 'invalid region length at line %d of file %s' % (lineno, filename)

		return chr, start, start+length

class RegionMap(object):
	def load(self, filename):
		self.regions = []

		with file(filename, 'r') as fd:
			len1, len2 = self._load_sizes(fd.readline(), filename)
			
			lineno = 1
			last_start = 0
			for line in fd:
				lineno += 1
				entry = self._parse_entry(line, filename, lineno)
				
				if entry[0] < last_start:
					raise ValueError, 'disorder in region map at line %d of file %s' % (lineno, filename)
				else:
					last_start = entry[0]
				
				self.regions.append(entry)
		
		self.identity = len1 == len2 and len(self.regions) == 1 and self.regions[0] == (0, len1, 0)
	
	def remap(self, region_iter):
		idx = 0
		last_start = 0

		for region in region_iter:
			start = region[0]
			stop = region[1]

			if last_start > start:
				raise ValueError, 'disorder in region iterator or invalid start coordinate'
			else:
				last_start = start

			if stop <= start:
				raise ValueError, 'invalid stop coordinate'

			while idx < len(self.regions) and self.regions[idx][1] <= start:
				idx += 1

			if idx >= len(self.regions):
				break
			elif start >= self.regions[idx][0] and stop <= self.regions[idx][1]:
				offset = self.regions[idx][2]
				yield (start, stop, offset) + region[2:]
			else:
				yield None

		for region in region_iter:
			yield None
	
	def _load_sizes(self, line, filename):
		tokens = line.split(None)
		if len(tokens) != 2:
			raise ValueError, 'invalid token number at line 1 of file %s' % filename
		
		try:
			len1 = int(tokens[0])
			if len1 < 0:
				raise ValueError
		except ValueError:
			raise ValueError, 'invalid first sequence length at line 1 of file %s' % filename
		
		try:
			len2 = int(tokens[1])
			if len2 < 0:
				raise ValueError
		except ValueError:
			raise ValueError, 'invalid second sequence length at line 1 of file %s' % filename
		
		return len1, len2

	def _parse_entry(self, line, filename, lineno):
		tokens = line.split(None)
		if len(tokens) != 4:
			raise ValueError, 'invalid token number at line %d of file %s' % (lineno, filename)

		try:
			start = int(tokens[0])
			if start < 0:
				raise ValueError
		except ValueError:
			raise ValueError, 'invalid source start coordinate at line %d of file %s' % (lineno, filename)

		try:
			stop = int(tokens[1])
			if stop <= start:
				raise ValueError
		except ValueError:
			raise ValueError, 'invalid source stop coordinate at line %d of file %s' % (lineno, filename)

		try:
			dest_start = int(tokens[2])
			if dest_start < 0:
				raise ValueError
		except ValueError:
			raise ValueError, 'invalid destination start coordinate at line %d of file %s' % (lineno, filename)

		try:
			dest_stop = int(tokens[3])
			if dest_stop <= dest_start:
				raise ValueError
		except ValueError:
			raise ValueError, 'invalid destination stop coordinate at line %d of file %s' % (lineno, filename)

		if dest_stop - dest_start != stop - start:
			raise ValueError, 'inconsistency in region length detected at line %d of file %s' % (lineno, filename)

		return start, stop, dest_start - start

class ConservedRegions(object):
	def __init__(self, old_regions, new_regions, region_map):
		if old_regions.seq != new_regions.seq:
			raise ValueError, 'mismatch in region chromosome'

		self.seq = old_regions.seq
		self._build(old_regions, new_regions, region_map)

	def __iter__(self):
		return self.regions.__iter__()
	
	def _build(self, old_regions, new_regions, region_map):
		self.regions = []

		new_regions = dict( ((start, stop), idx) for start, stop, idx in new_regions.regions)
		for res in region_map.remap( r[:2] for r in old_regions.regions ):
			if res is None:
				continue

			start, stop, offset = res
			new_region_idx = new_regions.get((start, stop), None)
			if new_region_idx is None:
				continue

			self.regions.append((start, stop, offset, new_region_idx))

