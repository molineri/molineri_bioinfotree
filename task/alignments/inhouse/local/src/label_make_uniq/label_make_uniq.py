#!/usr/bin/env python

from optparse import OptionParser
from sys import exit, stdin, stdout

def main():
	parser = OptionParser(usage='%prog <FASTA')
	options, args = parser.parse_args()

	if len(args) != 0:
		exit('Unexpected argument number.')
	
	last_header = None
	last_content = []
	header_serial = 1

	for line in stdin:
		if line[0] == '>':
			idx = line.find(' ')
			if idx == -1:
				line = line.rstrip()
			else:
				line = line[idx]
			
			if line == last_header:
				print '%s_%d' % (line, header_serial)
				header_serial += 1
			else:
				if last_header is not None:
					if header_serial > 1:
						print '%s_%d' % (last_header, header_serial)
					else:
						print last_header
				last_header = line
				header_serial = 1

			for line in last_content:
				stdout.write(line)
			last_content = []

		else:
			last_content.append(line)
	
	for line in last_content:
		stdout.write(line)

if __name__ == '__main__':
	main()
