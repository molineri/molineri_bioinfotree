#!/usr/bin/env python -O
from optparse import OptionParser
from sys import exit, stderr
import re

def readSeq(fd):
	ttbl = list(chr(i) for i in xrange(256))
	for c in 'acgt':
		ttbl[ord(c)] = 's'
	ttbl[ord('N')] = 'h'
	
	return fd.read().translate(''.join(ttbl), '\r\n')

if __name__ == '__main__':
	parser = OptionParser(usage='%prog SEQ_FASTA')
	parser.add_option('-n', '--name', dest='name', help='sequence NAME', metavar='NAME')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		print >>stderr, 'Unexpected argument number.'
		exit(1)
	
	fd = file(args[0])
	
	header = fd.readline().rstrip()
	if header[0] != '>':
		print >>stderr, 'Malformed FASTA header (or missing).'
		exit(1)
	if options.name is None:
		options.name = header[1:]
	
	seq = readSeq(fd)
	fd.close()
	
	softRx = re.compile('[s]+')
	hardRx = re.compile('[h]+')
	lst = []
	
	for m in softRx.finditer(seq):
		lst.append((m.start(), m.end(), 's'))
	for m in hardRx.finditer(seq):
		lst.append((m.start(), m.end(), 'h'))
	
	lst.sort()
	for mask in lst:
		print '\t'.join(str(i) for i in mask)
