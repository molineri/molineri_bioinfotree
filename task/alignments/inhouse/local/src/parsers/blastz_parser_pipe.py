#!/usr/bin/env python
from optparse import OptionParser
from sys import exit, stdin
from vfork.alignment.parser import BlastzParser
from wublast_parser_pipe import format_gaps

if __name__ == '__main__':
	opt_parser = OptionParser(usage='%prog')
	options, args = opt_parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	parser = BlastzParser()
	for alignment in parser.parse(stdin):
		print '\t'.join((
			alignment.query_label,
			str(alignment.query_start),
			str(alignment.query_stop),
			alignment.strand,
			alignment.target_label,
			str(alignment.target_start),
			str(alignment.target_stop),
			str(alignment.length),
			'',
			'',
			'',
			format_gaps(alignment.query_gaps),
			format_gaps(alignment.target_gaps)
		))
