#!/usr/bin/perl -lan
use warnings;
use strict;
$,="\t";
$\="\n";

while(<>){
	chomp;
	my @F=split;
	my $b=$F[17];
	my $e=$F[18];
	my $strand="+";
	if($b>$e){
		$strand='-';
		my $tmp=$e;
		$e=$b;
		$b=$tmp;
	}
	$b--;
	$F[20]--;
	print $F[0],$b,$e,$strand,$F[1],$F[20],$F[21],$F[6],$F[7],$F[10],$F[2],'ND','ND';
}
