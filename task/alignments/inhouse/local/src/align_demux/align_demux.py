#!/usr/bin/env python
# encoding: utf-8

from __future__ import with_statement

from bisect import bisect
from optparse import OptionParser
from sys import exit, stdin
from vfork.io.colreader import Reader

class BlockIndex(object):
	def __init__(self, filename):
		self.blocks = {}
		self._load(filename)
	
	def remap(self, label, start, stop):
		labels, offsets = self.blocks[label]

		idx = bisect(offsets, start) - 1
		assert 0 <= idx < len(offsets)
		
		offset = offsets[idx]
		return labels[idx], start - offset, stop - offset
	
	def _load(self, filename):
		block_name = None
		labels = []
		offsets = []
		
		with file(filename, 'r') as fd:
			reader = Reader(fd, '0s,1s,2u', False)
			while True:
				info = reader.readline()
				if info is None:
					break
				
				if info[0] != block_name:
					if len(labels):
						self.blocks[block_name] = (labels, offsets)
						
					if info[2] != 0:
						exit('First element of block has offset != 0 in index %s at line %d.' % (filename, reader.lineno()))
						
					block_name = info[0]
					labels = [info[1]]
					offsets = [info[2]]
					last_offset = 0
					
				else:
					if last_offset >= info[2]:
						exit('Disorder found in index %s at line %d.' % (filename, reader.lineno()))
					
					labels.append(info[1])
					offsets.append(info[2])
					last_offset = info[2]
		
		if len(labels):
			self.blocks[block_name] = (labels, offsets)
		else:
			exit('Empty index %s' % filename)

def main():
	parser = OptionParser(usage='%prog QUERY_BLOCK_INDEX TARGET_BLOCK_INDEX <PDB')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	query_block_index = BlockIndex(args[0])
	target_block_index = BlockIndex(args[1])
	
	reader = Reader(stdin, '0s,1u,2u,3s,4s,5u,6u,7s,8s,9s,10s,11s,12s', False)
	while True:
		info = reader.readline()
		if info is None:
			break
		
		try:
			query_label, query_start, query_stop = query_block_index.remap(info[0], info[1], info[2])
		except KeyError:
			exit('Unknown block name at line %d.' % reader.lineno())
		
		try:
			target_label, target_start, target_stop = target_block_index.remap(info[4], info[5], info[6])
		except KeyError:
			exit('Unexpected block name at line %d.' % reader.lineno())
	
		print '%s\t%d\t%d\t%s\t%s\t%d\t%d\t%s\t%s\t%s\t%s\t%s\t%s' % ( \
			query_label, query_start, query_stop, info[3], target_label, target_start, target_stop,
			info[7], info[8], info[9], info[10], info[11], info[12]
		)

if __name__ == "__main__":
	main()
