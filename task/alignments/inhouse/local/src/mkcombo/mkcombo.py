#!/usr/bin/env python
# encoding: utf-8

from __future__ import with_statement

from optparse import OptionParser
from os.path import isdir, join
from sys import exit
from vfork.io.colreader import Reader

class BlockPartitions(object):
	def __init__(self, filename, label, target_size):
		self.label = label
		self.target_size = target_size
		
		try:
			self.idx = self._load_idx(filename)
		except IOError:
			exit('Error reading %s' % filename)
	
	def __iter__(self):
		start = 0
		idx = 0
		cum_size = 0
		target = self.target_size
		while idx < len(self.idx):
			updated_size = cum_size + self.idx[idx]
			
			if updated_size > target:
				delta_low = target - cum_size
				delta_high = updated_size - target
				
				if start != idx and delta_low <= delta_high:
					yield start, idx-1
					start = idx
				else:
					yield start, idx
					start = idx + 1
					cum_size = updated_size
				
				target += self.target_size
			
			else:
				cum_size = updated_size
			
			idx += 1
		
		yield start, idx-1
	
	def _load_idx(self, filename):
		with file(filename, 'r') as fd:
			reader = Reader(fd, '1u', False)
			res = []
			
			while True:
				entry = reader.readline()
				if entry is None:
					break
				else:
					res.append(entry[0])
		
		return res

def parse_target_size(s):
	if len(s) == 0:
		raise ValueError
	
	scale = 1
	last_char = s[-1].upper()
	if last_char == 'K':
		s = s[:-1]
		scale = 1024
	elif last_char == 'M':
		s = s[:-1]
		scale = 1024*1024
	
	target_size = int(s)
	if target_size < 0:
		raise ValueError
	else:
		return target_size * scale
		
def print_half_splitted_targets(species1, s1, species2, s2, align_func, format_suffix):
	alignment = '%s_%s_%s_%s.$(ALIGN_TYPE).gz' % (species1, s1, species2, s2)
	print '%s: $(SEQDIR1)/%s.fa.splitted $(SEQDIR2)/%s.fa.%s' % (alignment, s1, s2, format_suffix)
	print '\t$(call %s)' % align_func
	
	pdb = '%s_%s_%s_%s.$(ALIGN_TYPE).pdb.gz' % (species1, s1, species2, s2) 
	print '%s: %s' % (pdb, alignment)
	
	return [alignment], pdb

def print_splitted_targets(species1, p1, species2, p2, align_func, format_suffix):
	alignments = []
	pdbs = []
	
	for b1_start, b1_stop in p1:
			b1_start += 1
			b1_stop  += 1
	
			for b2_start, b2_stop in p2:
				b2_start += 1
				b2_stop  += 1
				
				alignment = '%s_%s_%d_%d_%s_%s_%d_%d.$(ALIGN_TYPE).gz' % (species1, p1.label, b1_start, b1_stop, species2, p2.label, b2_start, b2_stop)
				alignments.append(alignment)
				print '%s: $(SEQDIR1)/%s.fa.splitted $(SEQDIR2)/%s.fa.splitted.%s' % (alignment, p1.label, p2.label, format_suffix)
				print '\t$(call %s,%d,%d,%d,%d)' % (align_func, b1_start, b1_stop, b2_start, b2_stop)
				
	pdb = '%s_%s_%s_%s.$(ALIGN_TYPE).pdb.gz' % (species1, p1.label, species2, p2.label) 
	print '%s: %s' % (pdb, ' '.join(alignments))
	
	return alignments, pdb

def main():
	parser = OptionParser(usage='%prog SPECIES1 SEQDIR1 QUOTED_SEQLIST1 SPECIES2 SEQDIR2 QUOTED_SEQLIST2 TARGET_SIZE ALIGN_FUNC')
	parser.add_option('-2', '--half-split', dest='half_split', action='store_true', default=False, help='split one side only')
	parser.add_option('-s', '--format-suffix', dest='format_suffix', default='xns', help='the SUFFIX of formatted sequence files (default: xns)', metavar='SUFFIX')
	options, args = parser.parse_args()
	
	if len(args) != 8:
		exit('Unexpected argument number.')
	elif not isdir(args[1]):
		exit('Invalid sequence directory 1.')
	elif not isdir(args[4]):
		exit('Invalid sequence directory 2.')
	
	try:
		target_size = parse_target_size(args[6])
	except ValueError:
		exit('Invalid target size.')
	
	seqlist1 = args[2].split()
	seqlist2 = args[5].split()
	if args[0] == args[3]:
		if args[1] != args[4]:
			exit('Same species, but different sequence directories.')
		elif seqlist1 != seqlist2:
			exit('Same species, but different sequences.')
	
	alignments = []
	pdbs = []
	
	if options.half_split:
		if args[0] == args[3]:
			for idx, s1 in enumerate(seqlist1):
				for s2 in seqlist2[idx:]:
					res = print_half_splitted_targets(args[0], s1, args[3], s2, args[7], options.format_suffix)
					alignments += res[0]
					pdbs.append(res[1])
		else:
			for s1 in seqlist1:
				for s2 in seqlist2:
					res = print_half_splitted_targets(args[0], s1, args[3], s2, args[7], options.format_suffix)
					alignments += res[0]
					pdbs.append(res[1])
	else:
		partitions2 = []
		for sequence in seqlist2:
			filename = join(args[4], sequence + '.fa.splitted.idx')
			partitions2.append(BlockPartitions(filename, sequence, target_size))
	
		if args[0] == args[3]:
			for idx, p1 in enumerate(partitions2):
				for p2 in partitions2[idx:]:
					res = print_splitted_targets(args[0], p1, args[3], p2, args[7], options.format_suffix)
					alignments += res[0]
					pdbs.append(res[1])
		else:
			for sequence in seqlist1:
				filename = join(args[1], sequence + '.fa.splitted.idx')
				p1 = BlockPartitions(filename, sequence, target_size)
				for p2 in partitions2:
					res = print_splitted_targets(args[0], p1, args[3], p2, args[7], options.format_suffix)
					alignments += res[0]
					pdbs.append(res[1])
	
	print 'ALL_ALIGN:=%s' % ' '.join(alignments)
	print 'ALL_PDB:=%s' % ' '.join(pdbs)

if __name__ == "__main__":
	main()
