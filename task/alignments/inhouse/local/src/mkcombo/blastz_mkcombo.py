#!/usr/bin/env python
# encoding: utf-8

from __future__ import with_statement

from optparse import OptionParser
from os.path import isdir, join
from sys import exit

def block_num(seqdir, seq):
	num = 0
	with file(join(seqdir, seq + '.fa.splitted.idx'), 'r') as fd:
		while True:
			line = fd.readline()
			if len(line) == 0:
				break
			else:
				num += 1
	return num
		
def print_targets(species1, seqdir1, s1, species2, s2, align_func):
	alignments = []
	for block_idx in xrange(block_num(seqdir1, s1)):
		block_idx += 1
		alignment = '%s_%s_%s_%s_%d.$(ALIGN_TYPE).gz' % (species1, s1, species2, s2, block_idx)
		alignments.append(alignment)
		
		print '%s: $(SEQDIR1)/%s.fa.splitted $(SEQDIR2)/%s.fa.splitted' % (alignment, s1, s2)
		print '\t$(call %s,%d)' % (align_func, block_idx)
	
	pdb = '%s_%s_%s_%s.$(ALIGN_TYPE).pdb.gz' % (species1, s1, species2, s2) 
	print '%s: %s' % (pdb, ' '.join(alignments))
	
	return alignments, pdb

def main():
	parser = OptionParser(usage='%prog SPECIES1 SEQDIR1 QUOTED_SEQLIST1 SPECIES2 SEQDIR2 QUOTED_SEQLIST2 ALIGN_FUNC')
	options, args = parser.parse_args()
	
	if len(args) != 7:
		exit('Unexpected argument number.')
	elif not isdir(args[1]):
		exit('Invalid sequence directory 1.')
	elif not isdir(args[4]):
		exit('Invalid sequence directory 2.')
	
	seqlist1 = args[2].split()
	seqlist2 = args[5].split()
	if args[0] == args[3]:
		if args[1] != args[4]:
			exit('Same species, but different sequence directories.')
		elif seqlist1 != seqlist2:
			exit('Same species, but different sequences.')
	
	alignments = []
	pdbs = []
	
	if args[0] == args[3]:
		for idx, s1 in enumerate(seqlist1):
			for s2 in seqlist2[idx:]:
				res = print_targets(args[0], args[1], s1, args[3], s2, args[6])
				alignments += res[0]
				pdbs.append(res[1])
	else:
		for s1 in seqlist1:
			for s2 in seqlist2:
				res = print_targets(args[0], args[1], s1, args[3], s2, args[6])
				alignments += res[0]
				pdbs.append(res[1])
	
	print 'ALL_ALIGN:=%s' % ' '.join(alignments)
	print 'ALL_PDB:=%s' % ' '.join(pdbs)

if __name__ == "__main__":
	main()
