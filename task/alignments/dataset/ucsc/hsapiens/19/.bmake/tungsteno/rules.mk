ifndef __BMAKE_DO_LAYOUT
SPECIES_MAP   := $(BIOINFO_ROOT)/task/annotations/local/share/ucsc_species.map
UCSC_SPECIES   = $(shell translate $(SPECIES_MAP) 1 <<<"$(SPECIES)" | cut -f 1)
UCSC_DATABASE  = $(UCSC_SPECIES)$(VERSION)
UCSC_ALIGN_SPECIES   = $(shell translate $(SPECIES_MAP) 1 <<<"$(ALIGN_SPECIES)" | cut -f -1)
UCSC_ALIGN_DATABASE  = $(UCSC_ALIGN_SPECIES)$(ALIGN_VERSION)
DATABASE_URL   = ftp://hgdownload.cse.ucsc.edu/goldenPath/$(UCSC_DATABASE)
SEQUENCE_DIR = $(BIOINFO_ROOT)/task/sequences/dataset/ucsc/$(SPECIES)/$(UCSC_DATABASE)
SEQUENCE_DIR_FOR_ALIGN_SPECIES = $(BIOINFO_ROOT)/task/sequences/dataset/ucsc/$(ALIGN_SPECIES)/$(UCSC_ALIGN_DATABASE)

ALL+=build_tree

ftp_index:
	wget -O $@ $(DATABASE_URL)/

species_list: ftp_index
	perl -lne 'print lcfirst($$1) if(m|vs([\D]+)\d+/</a>\s+$$|)' $< \
	| translate -a -k -n <(cut -f 1,2 $(SPECIES_MAP)) 1 \
	| sort | uniq > $@

build_tree: ftp_index species_list
	repeat_group_pipe '\
		grep -i "vs$$1" $< \
		| perl -ne '\''m|(\d+)/</a>\s+$$/|; print "$$$$1\n" '\'' \
		| bsort -nr | head -n 1 \
		| append_each_row -f "/" -B $$2\
	' 1 2 < $(word 2,$^) \
	| for i in `cat`; do\
		mkdir -p $$i;\
		link_install ../../../../local/share/makefiles/autospecies_$(UCSC_DATABASE) $$i/makefile;\
		link_install ../../../../local/share/rules/ucsc.mk $$i/rules.mk;\
	done;
	touch $@

all_axt:
	for i in */*; do cd $$i; bmake; cd -; done;

endif
