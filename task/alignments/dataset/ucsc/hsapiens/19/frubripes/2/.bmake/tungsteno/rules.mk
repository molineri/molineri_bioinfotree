ifndef __BMAKE_DO_LAYOUT

SPECIES            ?= hsapiens
VERSION            ?= 18

SPECIES_MAP   := $(BIOINFO_ROOT)/task/annotations/local/share/ucsc_species.map
UCSC_SPECIES   = $(shell translate $(SPECIES_MAP) 1 <<<"$(SPECIES)" | cut -f 1)
UCSC_DATABASE  = $(UCSC_SPECIES)$(VERSION)
UCSC_ALIGN_SPECIES   = $(shell translate $(SPECIES_MAP) 1 <<<"$(ALIGN_SPECIES)" | cut -f -1)
UCSC_ALIGN_DATABASE  = $(UCSC_ALIGN_SPECIES)$(ALIGN_VERSION)
DATABASE_URL   = http://hgdownload.cse.ucsc.edu/goldenPath/$(UCSC_DATABASE)
SEQUENCE_DIR = $(BIOINFO_ROOT)/task/sequences/dataset/ucsc/$(SPECIES)/$(UCSC_DATABASE)
SEQUENCE_DIR_FOR_ALIGN_SPECIES = $(BIOINFO_ROOT)/task/sequences/dataset/ucsc/$(ALIGN_SPECIES)/$(UCSC_ALIGN_DATABASE)

include $(BIOINFO_ROOT)/task/sequences/local/share/species/ucsc/$(SPECIES).mk

include $(BIOINFO_ROOT)/task/sequences/local/share/species/ucsc/$(SPECIES).mk


ALL+= $(addsuffix .axt.gz, $(addprefix chr,$(SPECIES_CHRS))) $(addsuffix .map.gz, $(addprefix chr,$(SPECIES_CHRS)))
all_map: $(addsuffix .axt.gz, $(addprefix chr,$(SPECIES_CHRS))) $(addsuffix .map.gz, $(addprefix chr,$(SPECIES_CHRS)))

all_chr.len:
	wget -O - http://hgdownload.cse.ucsc.edu/goldenPath/$(UCSC_ALIGN_DATABASE)/database/chromInfo.txt.gz \
	| zcat | cut -f -2 \
	| sed 's/^chr//' > $@

liftOver.chain.gz:
	wget -O $@ http://hgdownload.cse.ucsc.edu/goldenPath/$(UCSC_DATABASE)/liftOver/$(UCSC_DATABASE)To$(shell perl -e 'print ucfirst("$(UCSC_ALIGN_DATABASE)")').over.chain.gz

all.chain.gz net.gz: %.gz:
	db=$(UCSC_ALIGN_DATABASE);\
	db=`perl -ne 'if($$_ ne "$(UCSC_DATABASE)"){ print ucfirst} else{ print "Self"};' <<<$$db`;\
	wget -O $@ $(DATABASE_URL)/vs$$db/$(UCSC_DATABASE).$(UCSC_ALIGN_DATABASE).$*.gz

chain.gz: all.chain.gz
	cp $< $@

chain.axt.gz: chain.gz $(SEQUENCE_DIR) $(SEQUENCE_DIR_FOR_ALIGN_SPECIES)
	/usr/local/stow/kent/bin/chainToAxt \
	<(zcat $<) $(word 2,$^) $(word 3,$^) >(gzip > $@)

#chr1.atx.gz
%.axt.gz:
	db=$(UCSC_ALIGN_DATABASE);\
	db=`perl -ne 'if($$_ ne "$(UCSC_DATABASE)"){ print ucfirst} else{ print "Self"};' <<<$$db`;\
	wget -O $@ $(DATABASE_URL)/vs$$db/axtNet/$*.$(UCSC_DATABASE).$(UCSC_ALIGN_DATABASE).net.axt.gz

uniq.axt.gz:
	db=$(UCSC_ALIGN_DATABASE);\
	db=`perl -e "print ucfirst("\""$$db"\"")"`;\
	wget -O $@ $(DATABASE_URL)/vs$$db/$(UCSC_DATABASE).$(UCSC_ALIGN_DATABASE).net.axt.gz


%.map.gz: %.axt.gz all_chr.len
	zcat $< \
	| sed 's/chr//g' \
	| axtmap -f $(word 2,$^) \
	| sed 's/chr//g' \
	| gzip > $@



endif
