ifndef __BMAKE_DO_LAYOUT
$(SEQ_DIR)/hsapiens/$(SEQ_VERSION)/cdna.fa.gz: ; bmake --external $@
$(SEQ_DIR)/ggallus/$(SEQ_VERSION)/cdna.fa.gz: ; bmake --external $@
$(SEQ_DIR)/ggorilla/$(SEQ_VERSION)/cdna.fa.gz: ; bmake --external $@
$(SEQ_DIR)/mdomestica/$(SEQ_VERSION)/cdna.fa.gz: ; bmake --external $@
$(SEQ_DIR)/mmulatta/$(SEQ_VERSION)/cdna.fa.gz: ; bmake --external $@
$(SEQ_DIR)/mmusculus/$(SEQ_VERSION)/cdna.fa.gz: ; bmake --external $@
$(SEQ_DIR)/oanatinus/$(SEQ_VERSION)/cdna.fa.gz: ; bmake --external $@
$(SEQ_DIR)/pabelii/$(SEQ_VERSION)/cdna.fa.gz: ; bmake --external $@
$(SEQ_DIR)/ptroglodytes/$(SEQ_VERSION)/cdna.fa.gz: ; bmake --external $@

MART_QUERY_TOOL = $(BIOINFO_ROOT)/task/annotations/local/share/queries/mart_query.pl

%.fa: %.fa.gz
	zcat $< > $@

%.gene-transcript.map.gz:
	sed 's/__SPECIES__/$*/g' < $(BIOINFO_ROOT)/task/annotations/local/share/queries/mart_gene-transcript_map.xml \
	| $(MART_QUERY_TOOL) \
	| bsort -S20% | uniq \
	| gzip > $@

hsapiens-%.mart.homolog.gz:
	sed 's/__SPECIES__/hsapiens/g; s/__HOMOLOG_SPECIES__/$*/g' < $(BIOINFO_ROOT)/task/annotations/local/share/queries/mart_hortologous.xml \
	| $(MART_QUERY_TOOL) \
	| bsort -S20% | uniq \
	| gzip > $@

%.mart.gene_biotype.gz:
	sed 's/__SPECIES__/$*/g;' < $(BIOINFO_ROOT)/task/annotations/local/share/queries/mart_gene_biotype.xml \
	| $(MART_QUERY_TOOL) \
	| gzip > $@

%.standard_blast.transcript_score.gz: %.cdna.psl.gz
	bawk 	'{\
			score=$$match - $$mismatch*3 - $$Q_gap_count*5 - $$T_gap_count*5 - ($$Q_gap_tot_bases-$$Q_gap_count)*2 - ($$T_gap_tot_bases-$$T_gap_count)*2; \
			print $$Q_id, $$T_id,score\
		}' $< \
	| unhead -n 5 \
	| gzip > $@

%.standard_blat.transcript_score.gz: %.cdna.psl.gz
	bawk 	'{\
		score=$$match - $$mismatch - $$Q_gap_count -$$T_gap_count; \
			print $$Q_id, $$T_id,score\
		}' $< \
	| unhead -n 5 \
	| gzip > $@

%.cdna.psl.nogaplen.transcript_score.gz: %.cdna.psl.gz
	bawk 	'{\
			score=$$match - $$mismatch*3 - $$Q_gap_count*5 - $$T_gap_count*5;\
			print $$Q_id, $$T_id,score\
		}' $< \
	| unhead -n 5 \
	| gzip > $@



%.gene_score.best_q.gz: %.gene_score.gz
	bawk '{print $$Q_gene,$$T_gene,$$5}' $<\
	| find_best 1 3 \
	| gzip > $@

%.gene_score.best_t.gz: %.gene_score.gz
	bawk '{print $$Q_gene,$$T_gene,$$5}' $<\
	| find_best 2 3 \
	| gzip > $@

%.gene_score.one2one.gz: %.gene_score.best_q.gz %.gene_score.best_t.gz
	zcat $< | filter_2col 1 2 <(zcat $(word 2,$^) | cut -f -2) | gzip > $@

hsapiens-mmusculus.cdna_psl_blat-mart.comparison: hsapiens-mmusculus.mart.homolog.gz  hsapiens_mmusculus.cdna.psl.nogaplen.gene_score.one2one.gz
	(\
		bawk '$$3=="ortholog_one2one" {print $$1,$$2}' $< \
		| translate -a -v -e missing_in_blat <(zcat $(word 2,$^) | cut -f 1,2) 1;
		zcat $(word 2,$^) \
		| translate -a -v -e missing_in_mart <(bawk '$$3=="ortholog_one2one" {print $$1,$$2}' $<) 1;
	)  gzip > $@



.SECONDEXPANSION:

%.cdna.psl.gz: $$(SEQ_DIR)/$$(word 1,$$(subst _, ,%))/$$(SEQ_VERSION)/cdna.fa $$(SEQ_DIR)/$$(word 2,$$(subst _, ,%))/$$(SEQ_VERSION)/cdna.fa
	blat $(word 2,$^) $< /dev/stdout -minIdentity=50 | unhead -n 5 | untail -n 2 | gzip > $@


%.gene_score.gz: %.transcript_score.gz $$(word 1,$$(subst _, ,%)).gene-transcript.map.gz $$(word 2,$$(subst ., ,$$(subst _, ,%))).gene-transcript.map.gz
	zcat $< \
	| translate -a -n <(zcat $(word 2,$^)) 1\
	| translate -a -n <(zcat $(word 3,$^)) 3\
	| gzip > $@

endif
