.DELETE_ON_ERROR:

all: symbols.occurrencies.wublast.parsed


.PHONY: iter_clean all clean
iter_clean:
	rm -f symbols.iter.*;
	rm -f sequences.len.fa

clean: iter_clean
	rm -f sequences.len.fa sequences.len.map
	

.PHONY: symbols.iter.fa.add
symbols.iter.fa.add: symbols.iter.fa.tmp symbols.fa 
	echo "aggiunta dei patterns rimanenti nel file di quelli definitivi";
	pattern_next_id=`grep '>' $(word 2,$^) | wc -l`; \
	perl -e "\$$\=\"\n\"; \$$,=\"\t\"; \$$id=$$pattern_next_id; while (<>) {chomp; if (\$$_=~/^>/) {my (\$$len) = (\$$_=~/^>\d+;(\d+)$$/); \$$_='>'.\$$id.';'.\$$len; \$$id++;}  print \$$_}" < $< >> symbols.fa;
	rm -f *wublast*;

sequences.len.fa: sequences.len.map $(STARTING_SEQUENCES)
	traduci -w sequences.len.map < $(STARTING_SEQUENCES) > $@
	touch symbols.iter.fa.changed.tmp

.INTERMEDIATE: sequences.len.map
sequences.len.map: $(STARTING_SEQUENCES)
	fastalen $< | awk '{print $$1 "\t" $$1 ";" $$2}' > $@

symbols.iter.fa.tmp: sequences.len.fa
	        perl -e '$$\="\n"; $$count=0; while (<>) {chomp; if ($$_=~/^>/) {($$len) = ($$_=~/^>\d+.*;(\d+)$$/); print ">$$count;$$len"; $$count++;} else {print;} }' < $< > $@

symbols.fa: symbols.iter.fa.tmp symbols.iter.fa.changed.tmp
	echo; \
	echo '# 0. controllo sull`elenco dei patterns da allineare tra loro'; \
	echo; \
	if [ `wc -l < $<` -eq 0 ]; then exit 0; fi; \
	if [ `grep '>' $< | wc -l` -eq 1 ]; then \
		make symbols.iter.fa.add; \
		exit 0; \
	fi; \
	rm $(word 2,$^); \
	echo; \
	echo '# 1. ricerca di allineamenti tra i patterns'; \
	echo; \
	wublast_parsed=symbols.iter.fa.tmp.wublast.parsed; \
	make $$wublast_parsed; \
	if [ `wc -l < $$wublast_parsed` -eq 0 ]; then \
		make symbols.iter.fa.add; \
		exit 0; \
	fi; \
	echo; \
	echo '# 2. costruzione della rete tra i patterns'; \
	echo; \
	net="$$wublast_parsed.$(SCORE_CUTOFF).score"; \
	make $$net; \
	if [ `wc -l < $$net` -eq 0 ]; then \
		make symbols.iter.fa.add; \
		exit 0; \
	fi; \
	echo; \
	echo '# 3. ricerca delle comunita` di patterns'; \
	echo; \
	net_commty="$$net.conn_comp.commty2"; \
	make $$net_commty; \
	if [ ! -s "$$net_commty" ]; then \
		make symbols.iter.fa.add; \
		exit 0; \
	fi; \
	net_commty2="$$net_commty.conn_comp"; \
	make $$net_commty2; \
	if [ ! -s "$$net_commty2" ]; then \
		make symbols.iter.fa.add; \
		exit 0; \
	fi; \
	echo; \
	echo '# 4. i patterns che non compaiono nelle comunita` vengono aggiunti alla lista definitiva dei patterns'; \
	echo; \
	cut -f 2- $$net_commty2 | tr "\t" "\n" | sort > $$net_commty2.in_comm; \
	grep '>' $< | tr -d '>' | sort > $$net_commty2.initial; \
	comm -3 $$net_commty2.initial $$net_commty2.in_comm > $$net_commty2.excluded; \
	touch $@; \
	pattern_next_id=`grep '>' $@ | wc -l` || echo "hack to give exit vale 0"; \
	perl -e "\$$\=\"\n\"; \$$,=\"\t\"; \$$id=$$pattern_next_id; while (<>) {chomp; my (\$$len) = (\$$_=~/^\d+;(\d+)$$/); print 'echo -e \'>'.\$$id.';'.\$$len.'\''; \$$id++; print 'get_fasta -n \''.\$$_.'\' $<'}" < $$net_commty2.excluded | bash >> $@; \
	rm -f $$commty.in_comm $$net_commty2.initial $$net_commty2.excluded; \
	echo; \
	echo '# 5. per ciascuna comunita` si cerca il multiallineamento tra i suoi patterns'; \
	echo; \
	multialign="$$net_commty2.multialign" || echo "hack to give exit vale 0"; \
	make $$net_commty2.strand; \
	rm -f $$multialign.dnd $$multialign.out $$multialign.err; \
	repeat_fasta_pipe -n "\
		rm -f $$multialign.tmp; \
		echo '>\$$HEADER' >> $$multialign; \
		perl -lane "\""print 'get_fasta_strand \''.(join ' ',@F).'\''.' $<' "\"" | bash | tr ';' '_' > $$multialign.tmp; \
		clustalw -INFILE=$$multialign.tmp -TYPE=DNA -OUTPUT=GDE -CASE=UPPER -OUTORDER=ALIGNED -OUTFILE=$$multialign.out >> $$multialign.err;\
		perl -lne 'if (\$$_=~/^#.*_(\d+)\$$/) { \$$_=~s/_(\$$1)$$/;\$$1/;} print ' < $$multialign.out >> $$multialign; \
		rm -f $$multialign.out; \
	" < $$net_commty2.strand; \
	rm -f $$multialign.tmp $$multialign.dnd; \
	echo; \
	echo '# 6. si estraggono i nuovi patterns a partire dal multiallineamento'; \
	echo; \
	make $$net_commty2.consensus_matrix.patterns; \
	echo; \
	echo '# 7. i patterns che non hanno un buon multiallineamento (sono presenti nelle comunita` ma non nei nuovi patterns) vengono aggiunti alla lista definitiva dei symbols;' \
	echo; \
	repeat_fasta_pipe -n '\
		if [ `wc -l` -eq 0 ]; then echo $$HEADER; fi \
	' < $$net_commty2.consensus_matrix.patterns > $$net_commty2.consensus_matrix.patterns.lost ; \
	symbol_next_id=`grep '>' $@ | wc -l` || echo "hack to give exit vale 0"; \
	grep -w -f $$net_commty2.consensus_matrix.patterns.lost $$net_commty2 \
		| perl -e "\$$\=\"\n\"; \$$,=\"\t\"; \$$id=$$symbol_next_id; while (<>) {chomp; my @F=split/\t/,\$$_; shift @F; foreach (@F) {my (\$$len) = (\$$_=~/^\d+;(\d+)$$/); print 'echo -e \'>'.\$$id.';'.\$$len.'\''; \$$id++; print 'get_fasta -n \''.\$$_.'\' $<'} }" | bash >> $@; \
	echo; \
	echo '# 8. si prendono i nuovi patterns come punto di partenza per l`iterazione successiva (si ridefiniscono gli ids)'; \
	echo; \
	perl -e '$$\="\n"; $$count=0; $$,="\t"; while (<>) {chomp; next if ($$_=~/^>/); $$_=~s/#//; my $$len=length($$_); print ">$$count;$$len"; print $$_; $$count++; }' < $$net_commty2.consensus_matrix.patterns > $<; \
	echo; \
	echo '# 9. si eliminano tutti files intermedi del ciclo di iterazione'; \
	echo; \
	rm -f *wublast*; \
	echo '# 10. se e` necessario continuare ad iterare (il numero di patterns nuovi e` > 1) tocco il file $(word 2,$^)'; \
	new_patterns_n=`grep '>' $< | wc -l` || echo "hack to give exit vale 0"; \
	if [ "$$new_patterns_n" -gt 1 ]; then \
		touch $(word 2,$^); \
		echo -e "\n\n\n\n\t\t\tnuova iterazione\n\n\n\n"; sleep 5; \
		make $@; \
	fi;


symbols.occurrencies.pdb: sequences.len.fa symbols.fa
	source `which wublast-env`; \
	xdformat -n $<
	repeat_fasta_pipe 'blastn $< - $(WUBLAST_PARAM) mformat=2' < $(word 2,$^) 2> $@.err \
	| /home/bioinfo/task/alignments/inhouse/local/bin/wublastab2pdb > $@ 
	rm $<.xn[dst]

	
	

symbols.iter.fa.tmp.wublast: symbols.iter.fa.tmp
	source `which wublast-env`; \
	xdformat -n $<
	repeat_fasta_pipe 'blastn $< - $(WUBLAST_PARAM) mformat=2' < $< > $@ 2> $@.err
	rm $<.xn[dst]

symbols.iter.fa.tmp.wublast.parsed: symbols.iter.fa.tmp.wublast
	cut -f -2,7,11,14,16,18-19,21-22 $< \
	| perl -lane '$$,="\t"; $$\="\n"; next if ($$F[0] eq $$F[1]); ($$left_id,$$left_len)=($$F[0]=~/^(\d+);(\d+)$$/); ($$right_id,$$right_len)=($$F[1]=~/^(\d+);(\d+)$$/); next if ($$left_id > $$right_id); $$F[10]="+"; if ($$F[6] > $$F[7]) {$$tmp=$$F[6]; $$F[6]=$$F[7]; $$F[7]=$$tmp; $$F[10]="-"} $$F[6]--; $$F[8]--; print @F' \
	| sort -k1,1 -k2,2 | uniq \
	> $@
# .parsed per gli allineamenti dei patterns di consensi (no chr,b,e sul genoma) nelle iterazioni
# formato output:
#	1. left_region_id ( 0;174     (pattern_id);(left_reg_length) )
#	2. right_region_id ( 1;281     (pattern_id);(right_reg_length) )
#	3. overall_alignment_length
#	4. percent identity over the alignment length (nel vecchio formato non c'era questa colonna)
#	5. query_gaps_length
#	6. target_gaps_length
#	7. query_begin
#	8. query_end
#	9. target_begin
#	10.target_end
#	11.strand

%.parsed.double_score: %.parsed
	$(BIN_DIR)/score_align_peak_peak4 < $< > $@

%.parsed.score: %.parsed.double_score
	cat $< \
		| perl -lane '$$,="\t"; $$\="\n"; ($$left_len)=($$F[0]=~/^\d+;(\d+)$$/); ($$right_len)=($$F[1]=~/^\d+;(\d+)$$/); $$score=( $$F[2]/($$left_len) + $$F[3]/($$right_len) )/2; splice @F,2,1; $$F[2]=$$score; print @F;' \
		>$@

%.$(SCORE_CUTOFF).score: %.score
	cat $< | awk '$$5 > $(SCORE_CUTOFF)' > $@

%.score.conn_comp: %.score
	cut -f -2 $< | conn_comp | perl -lne '$$,="\t"; $$\="\n"; $$n = $$. - 1; print ">$$n",$$_' > $@

%.score.connectivity: %.score
	cut -f -2 $< | connectivity > $@

%.score_connectivity: %.connectivity %
	traduci_tab $^ > $@

%.conn_comp.stats: %.conn_comp %.score_connectivity
	$(BIN_DIR)/group_analysis -ccomp $^ > $@
	# 1.  conn_component_id (row number - 1)
	# 2.  number of nodes in conn_comp
	# 3.  score_mean
	# 4.  score_median
	# 5.  score_stdev
	# 6.  score_min
	# 7.  score_max
	# 8.  conn_mean
	# 9.  conn_median
	# 10. conn_stdev
	# 11. conn_min
	# 12. conn_max

%.conn_comp.commty2: %.conn_comp.stats %.conn_comp %
	for i in `cut -f 1 $< | tr -d '>'`; do \
		rm -f $@.tmp $@.tmp2; \
		$(BIN_DIR)/extract_conn_comp -n $$i -c $(word 2,$^) -tmp $@.tmp -line \
		$(word 3,$^) > $@.tmp2; \
		echo ">$$i" >> $@; \
		cut -f -2 $@.tmp2 | commty2 >> $@; \
	done
	rm -f $@.tmp $@.tmp2

%.conn_comp.commty2.conn_comp: %.conn_comp.commty2
	$(BIN_DIR)/commty2conn_comp -n 2 < $< > $@

%.conn_comp.commty2.conn_comp.strand: % %.conn_comp.commty2.conn_comp
	$(BIN_DIR)/group_net -c $(word 2,$^) $< > $@

%.consensus_matrix: %.multialign
	repeat_fasta_pipe -n '\
		echo ">$$HEADER"; \
		$(BIN_DIR)/consensus -m -s \
	' < $< > $@

%.consensus_matrix.patterns: %.consensus_matrix
	repeat_fasta_pipe -n '\
		echo ">$$HEADER"; \
		transpose | $(BIN_DIR)/extract_patterns \
		-c $(CONSENSUS_CUTOFF) -drop $(CONSENSUS_DROPOFF) -n $(CONSENSUS_MATCH_VALUE) -m $(CONSENSUS_MISMATCH_VALUE) \
		-g $(CONSENSUS_GAP_VALUE) -l $(CONSENSUS_MIN_LEN) -s $(CONSENSUS_MIN_SCORE) -t ; \
	' < $< > $@

