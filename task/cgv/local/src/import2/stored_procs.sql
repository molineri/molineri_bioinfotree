DROP PROCEDURE IF EXISTS regions_by_region;
DROP PROCEDURE IF EXISTS segments_by_region;

delimiter //

CREATE PROCEDURE regions_by_region (seq_id INT UNSIGNED, region_start INT UNSIGNED, region_stop INT UNSIGNED)
BEGIN
	DECLARE current_span INT UNSIGNED;
	DECLARE span_done INT DEFAULT 0;
	DECLARE span_cursor CURSOR FOR SELECT id FROM spans WHERE sequence_id=seq_id AND NOT (region_start > stop OR region_stop < start);
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET span_done = 1;
	
	OPEN span_cursor;
	FETCH span_cursor INTO current_span;
	WHILE NOT span_done DO
		SELECT type_id, start, stop FROM regions WHERE span_id=current_span AND NOT (region_start > stop OR region_stop < start) ORDER BY type_id DESC;
		FETCH span_cursor INTO current_span;
	END WHILE;
	CLOSE span_cursor;
END;
//

CREATE PROCEDURE segments_by_region (seq1_id INT UNSIGNED, seq2_id INT UNSIGNED, region_x_start INT UNSIGNED, region_x_stop INT UNSIGNED, region_y_start INT UNSIGNED, region_y_stop INT UNSIGNED)
BEGIN
	DECLARE current_locl INT UNSIGNED;
	DECLARE locl_done INT DEFAULT 0;
	DECLARE locl_cursor CURSOR FOR SELECT id FROM locls WHERE seuence_pair_id=@current_sequence_pair AND NOT (region_x_start > x_stop OR region_x_stop < x_start) AND NOT (region_y_start > y_stop OR region_y_stop < y_start);
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET locl_done = 1;
	
	SELECT id INTO @current_sequence_pair FROM sequence_pairs WHERE sequence_pairs.sequence1_id=seq1_id and sequence_pairs.sequence2_id=seq2_id;

	OPEN locl_cursor;
	FETCH locl_cursor INTO current_locl;
	WHILE NOT locl_done DO
		SELECT type_id, x_start, x_stop, y_start, y_stop FROM segments WHERE locl_id=current_locl AND NOT (region_x_start > GREATEST(x_start, x_stop) OR region_x_stop < LEAST(x_start, x_stop)) AND NOT (region_y_start > GREATEST(y_start, y_stop) OR region_y_stop < LEAST(y_start, y_stop)) ORDER BY type_id DESC;
		FETCH locl_cursor INTO current_locl;
	END WHILE;
	CLOSE locl_cursor;
END;
//
