from gzip import GzipFile
from string import maketrans

class LoclScanner(object):
	''' A scanner for FASTA files having location cluster
	    as sections.
	
	    Implement the L{process_label} and L{process_line]}
	    to handle labels and content lines, then call the
	    L{scan} method.
	
	    This class provides the following member variables during
	    the scan:
	     - filename
	     - lineno (0 indexed)
	'''
	def process_label(self, label):
		raise NotImplemented
	
	def process_line(self, line):
		raise NotImplemented
	
	def scan(self, filename):
		self.filename = filename
		label = None
		
		try:
			fd = GzipFile(filename, 'r')
			for self.lineno, line in enumerate(fd):
				line = line.rstrip()
				
				if len(line) == 0:
					continue
				elif line[0] == '>':
					label = line[1:]
					self.process_label(label)
				elif label is not None:
					self.process_line(line)
				else:
					exit('Spurious content at line %d of file %s' % (self.lineno+1, filename))
		finally:
			fd.close()

def parse_locl_filename(filename):
	try:
		prefix = filename[:filename.index('.')]
	except ValueError:
		exit('Invalid location cluster filename: %s' % filename)
	
	chrs = prefix.split('_')
	if len(chrs) != 2:
		exit('Invalid location cluster filename: %s' % filename)
	
	return tuple(c.replace('chr', '') for c in chrs)

def parse_locl_label(label, filename, lineno):
	tokens = label.split('_')
	try:
		if len(tokens) != 4:
			raise ValueError
		return tuple(int(d) for d in tokens)
	except ValueError:
		exit('Invalid location cluster label at line %d of file %s: %s' % (lineno, filename, label))

def parse_segment(line, filename, lineno):
	tokens = line.split('\t')
	try:
		if len(tokens) < 10:
			raise ValueError
		return tuple(int(d) for d in (tokens[i] for i in (1, 2, 5, 6)))
	except ValueError:
		exit('Invalid segment at line %d of file %s: %s' % (lineno, filename, line))
