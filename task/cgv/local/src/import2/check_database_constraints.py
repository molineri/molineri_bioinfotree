#!/usr/bin/env python
# encoding: utf-8

from getpass import getpass
from MySQLdb import connect as database_connect
from optparse import OptionParser
from sys import exit

def check_foreign_keys(cursor, source_key, source_table, foreign_key, foreign_tables):
	for foreign_table in foreign_tables:
		cursor.execute('SELECT COUNT(*) FROM %s' % foreign_table)
		total_count, = cursor.fetchone()
		
		cursor.execute('SELECT COUNT(*) FROM %s a JOIN %s b ON a.%s = b.%s' % (foreign_table, source_table, foreign_key, source_key))
		joined_count, = cursor.fetchone()
		
		if total_count != joined_count:
			exit('%d rows in the table %s have a %s not matching any %s in the table %s.' % (total_count-joined_count, foreign_table, foreign_key, source_key, source_table))

def main():
	parser = OptionParser(usage='%prog DB USERNAME')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	db = database_connect(user=args[1], passwd=getpass(), db=args[0])
	cursor = db.cursor()
	
	check_foreign_keys(cursor, 'id', 'types', 'type_id', ('regions', 'segments'))
	check_foreign_keys(cursor, 'id', 'chrs', 'chr_id', ('spans',))
	check_foreign_keys(cursor, 'id', 'chr_pairs', 'chr_pair_id', ('locls',))
	check_foreign_keys(cursor, 'id', 'spans', 'span_id', ('regions',))
	check_foreign_keys(cursor, 'id', 'locls', 'locl_id', ('segments',))

if __name__ == '__main__':
	main()
