from __future__ import with_statement
from sys import exit

class ChrTable(object):
	def __init__(self, filename):
		self.fd = file(filename, 'w')
		self.id = 0
	
	def add(self, chromosome):
		self.id += 1
		print >>self.fd, '%d\t%s' % (self.id, chromosome)
		return self.id

class ChrPairTable(object):
	def __init__(self, filename):
		self.fd = file(filename, 'w')
		self.id = 0
	
	def add(self, chr1, chr2):
		self.id += 1
		print >>self.fd, '%d\t%s\t%s' % (self.id, chr1, chr2)
		return self.id

class SpanTable(object):
	def __init__(self, filename):
		self.fd = file(filename, 'w')
		self.id = 0
	
	def next_id(self):
		return self.id + 1
	
	def add(self, chr_id, start, stop):
		self.id += 1
		print >>self.fd, '\t'.join(str(d) for d in (self.id, chr_id, start, stop))
		return self.id

class LoclTable(object):
	def __init__(self, filename):
		self.fd = file(filename, 'w')
		self.id = 0
	
	def add(self, chrs_pair_id, x_start, x_stop, y_start, y_stop):
		self.id += 1
		print >>self.fd, '\t'.join(str(d) for d in (self.id, chrs_pair_id, x_start, x_stop, y_start, y_stop))
		return self.id

class RegionTable(object):
	def __init__(self, filename):
		self.fd = file(filename, 'w')
	
	def add(self, span_id, start, stop, tp):
		print >>self.fd, '\t'.join(str(d) for d in (span_id, tp, start, stop))

class HspTable(object):
	def __init__(self, filename, type_id):
		self.fd = file(filename, 'w')
		self.type_id = type_id
	
	def add(self, locl_id, x_start, x_stop, y_start, y_stop):
		record = '\t'.join(str(d) for d in (locl_id, self.type_id, x_start, x_stop, y_start, y_stop))
		print >>self.fd, record

class DiagonalTable(object):
	def __init__(self, filename, type_id):
		self.fd = file(filename, 'w')
		self.type_id = type_id
	
	def add(self, locl_id, x_start, x_stop, y_start, y_stop):
		record = '\t'.join(str(d) for d in (locl_id, self.type_id, x_start, x_stop, y_start, y_stop))
		print >>self.fd, record

class SpGapTable(object):
	def __init__(self, filename, type_id):
		self.fd = file(filename, 'w')
		self.type_id = type_id
	
	def add(self, locl_id, x_start, x_stop, y_start, y_stop):
		record = '\t'.join(str(d) for d in (locl_id, self.type_id, x_start, x_stop, y_start, y_stop))
		print >>self.fd, record

class TypeMap(object):
	def __init__(self, filename):
		self.type_map = self._load_type_map(filename)
	
	def get_id(self, tp):
		return self.type_map[tp]
	
	def _load_type_map(self, filename):
		type_map = {}
		with file(filename, 'r') as fd:
			for lineno, line in enumerate(fd):
				line = line.rstrip()
				tokens = line.split('\t')
				try:
					if len(tokens) != 2:
						raise ValueError
					
					type_id = int(tokens[0])
					if type_id in type_map:
						raise ValueError
					else:
						type_map[tokens[1]] = type_id
				
				except ValueError:
					exit('Invalid type at line %d of file %s: %s' % (lineno+1, filename, line))
		
		return type_map

class LoclMap(object):
	def __init__(self):
		self.chr_pair_map = {}
		self.locl_map = {}
	
	def add_chr_pair(self, chr_pair_id, chr1, chr2):
		self.chr_pair_map[chr_pair_id] = (chr1, chr2)
	
	def add_locl(self, chr_pair_id, locl_id, x_start, x_stop, y_start, y_stop):
		k = self.chr_pair_map[chr_pair_id]
		self.locl_map[k + (x_start, x_stop, y_start, y_stop)] = locl_id
	
	def __getitem__(self, key):
		assert len(key) == 6, 'invalid key'
		return self.locl_map[key]
