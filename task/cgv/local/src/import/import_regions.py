#!/usr/bin/env python
# encoding: utf-8

from commands import getstatusoutput
from glob import glob
from optparse import OptionParser
from os.path import basename, join, isfile
from sys import exit
from tables import *

def count_rows(*filenames):
	total = 0
	
	for filename in filenames:
		status, output = getstatusoutput('wc -l "%s"' % filename)
		try:
			if status != 0:
				raise ValueError
			total += int(output[:output.index(' ')])
		except IndexError, ValueError:
			exit('Can\'t determine %s length.' % filename)
	
	return total

class MultipleRegionReader(object):
	def __init__(self, *filenames):
		self.fds = [ file(filename) for filename in filenames ]
		self.linenos = [ 1 ] * len(filenames)
		
		self.buffer = []
		for idx, fd in enumerate(self.fds):
			line = fd.readline()
			self.buffer.append(self._parse_line(line.rstrip(), fd.name, 1) if len(line) else None)
	
	def __iter__(self):
		return self
	
	def next(self):
		smallest = None
		for idx, region in enumerate(self.buffer):
			if region is not None:
				if smallest is None or region < smallest[1]:
					smallest = (idx, region)
		
		if smallest is None:
			for fd in self.fds:
				fd.close()
			raise StopIteration
		else:
			idx = smallest[0]
			
			line = self.fds[idx].readline()
			if len(line):
				self.linenos[idx] += 1
				self.buffer[idx] = self._parse_line(line, self.fds[idx].name, self.linenos[idx])
			else:
				self.buffer[idx] = None
			
			return smallest[:1] + smallest[1]
	
	def _parse_line(self, line, filename, lineno):
		tokens = line.rstrip().split('\t')
		try:
			if len(tokens) != 3:
				raise ValueError
			return (int(tokens[0]), int(tokens[1]), tokens[2])
		except ValueError:
			exit('Malformed input at line %d of file %s' % (lineno, filename))

def extract_chromosome(filename):
	name = basename(filename)
	try:
		idx = name.index('.')
		return name[:idx].replace('chr', '')
	except IndexError:
		exit('Invalid annotation file: %s' % filename)

def check_file(filename):
	if not isfile(filename):
		exit('Missing file: %s' % filename)

def parse_repeat_label(label, filename, lineno):
	idx = label.find(':')
	try:
		if idx == -1:
			raise ValueError
		
		label = label[:idx]
		if label == 'simple':
			return 'repeat'
		elif label == 'transposon':
			return label
		else:
			raise ValueError
	
	except ValueError:
		exit('Invalid repeat label at line %d of file %s: %s' % (lineno, filename, label))

def parse_mask_label(label, filename, lineno):
	if label.lower() == 'h':
		return 'hard_mask'
	else:
		exit('Invalid mask label at line %d of file %s: %s' % (lineno, filename, label))

def parse_nr_label(label, filename, lineno):
	if label.lower() not in 'ce53uin':
		exit('Invalid non redundant annotation label at line %d of file %s: %s' % (lineno, filename, label))
	else:
		return label

def main():
	parser = OptionParser(usage='%prog ANNOTATION_DIR')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	chrs_table = ChrTable('chrs.tbl')
	span_table = SpanTable('spans.tbl')
	region_table = RegionTable('regions.tbl')
	type_map = TypeMap('types.tbl')
	
	for repeats_filename in glob(join(args[0], '*.repeats')):
		chromosome = extract_chromosome(repeats_filename)
		masks_filename = join(args[0], 'chr%s.masks' % chromosome)
		non_redundant_filename = join(args[0], 'chr%s.non_redundant' % chromosome)
		check_file(masks_filename)
		check_file(non_redundant_filename)
		
		record_num = count_rows(repeats_filename, masks_filename, non_redundant_filename)
		if record_num == 0:
			continue
		
		chr_id = chrs_table.add(chromosome)
		record_by_span = (record_num // 100) if record_num > 100 else 100
		reader = MultipleRegionReader(repeats_filename, masks_filename, non_redundant_filename)
		type_label_parsers = (parse_repeat_label, parse_mask_label, parse_nr_label)
		
		span_id = None
		for idx, (file_idx, start, stop, tp) in enumerate(reader):
			if idx % record_by_span == 0:
				if span_id is not None:
					span_table.add(chr_id, span_start, span_stop)
				span_id = span_table.next_id()
				span_start = start
				span_stop = 0
			else:
				filename = reader.fds[file_idx].name
				lineno = reader.linenos[file_idx]
				type_name = type_label_parsers[file_idx](tp, filename, lineno)
				
				try:
					region_table.add(span_id, start, stop, type_map.get_id(type_name))
					span_stop = max(stop, span_stop)
				except KeyError:
					exit('Invalid type at line %d of file %s: %s' % (lineno, filename, type_name))
		
		span_table.add(chr_id, span_start, span_stop)

if __name__ == '__main__':
	main()
