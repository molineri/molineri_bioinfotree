function paint_canvas(id)
{
	var canvas = document.getElementById(id);
	if (canvas.getContext)
	{
		var ctx = canvas.getContext('2d');
		ctx.fillRect(100, 100, 200, 200);
		
		var img = new Image();
		img.onload = function() {
			ctx.save();
			ctx.translate((400 - img.height) / 2 + img.height, (400 - img.width) / 2);
			ctx.rotate(Math.PI / 2);
			ctx.drawImage(img, 0, 0);
			ctx.restore();
		}
		img.src = "http://static.reddit.com/redditheaderProgramming.png";
	}
}
