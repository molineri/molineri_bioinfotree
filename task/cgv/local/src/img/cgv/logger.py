from __future__ import with_statement

from os import getpid
from os.path import join
from time import ctime

class ProcessLogger(object):
	def __init__(self, path, prefix):
		self.path = path
		self.prefix = prefix
	
	def log(self, msg):
		filename = join(self.path, '%s%d.log' % (self.prefix, getpid()))
		with file(filename, 'a') as fd:
			print >>fd, 'At %s:' % ctime()
			print >>fd, msg

