from MySQLdb import connect as mysql_connect
from time import time

class ConnectionPool(object):
	def __init__(self, user, passwd, size):
		self.__user = user
		self.__passwd = passwd
		self.size = size
		self.pool = {}
	
	def get_cursor(self, db):
		def refresh_cursor():
			connection = mysql_connect(db=db, user=self.__user, passwd=self.__passwd)
			self.pool[db] = [connection, time()]
			return connection.cursor()
		
		try:
			info = self.pool[db]
			cursor = info[0].cursor()
			info[1] = time()
			return Cursor(cursor, refresh_cursor)
		
		except KeyError:
			if len(self.pool) == self.size:
				self._drop_oldest_connection()
			return Cursor(refresh_cursor(), refresh_cursor)
	
	def _drop_oldest_connection(self):
		oldest_db = None
		oldest_timestamp = None
		
		for db, (connection, timestamp) in self.pool.iter_items():
			if oldest_timestamp is None or timestamp < oldest_timestamp:
				oldest_db = db
				oldest_timestamp = timestamp
		
		connection, timestamp = self.pool.pop(db)
		connection.close()

class Cursor(object):
	def __init__(self, cursor, refresh_cursor):
		self.cursor = cursor
		self.refresh_cursor = refresh_cursor
	
	def execute(self, query, args):
		try:
			return self.cursor.execute(query, args)
		except:
			self.cursor = self.refresh_cursor()
			return self.cursor.execute(query, args)
	
	def fetchone(self):
		return self.cursor.fetchone()
	
	def iter_results(self):
		while True:
			row = self.cursor.fetchone()
			if row is None:
				if not self.cursor.nextset():
					break
			else:
				yield row

class CgvDatabase(object):
	def __init__(self, user, passwd, connection_pool_size):
		self.pool = ConnectionPool(user, passwd, connection_pool_size)
	
	def iter_types(self, db):
		cursor = self.pool.get_cursor(db)
		cursor.execute('SELECT id, name FROM types ORDER BY name')
		return cursor.iter_results()
	
	def iter_regions(self, db, chromosome, start, stop):
		cursor = self.pool.get_cursor(db)
		cursor.execute('CALL regions_by_region(%s, %s, %s)', (chromosome, start, stop))
		return cursor.iter_results()
	
	def iter_segments(self, db, chr1, chr2, x_start, x_stop, y_start, y_stop):
		cursor = self.pool.get_cursor(db)
		cursor.execute('SELECT id FROM chr_pairs WHERE chr1=%s AND chr2=%s', (chr1, chr2))
		row = cursor.fetchone()
		reverse = row is None
		
		query = 'CALL segments_by_region(%s, %s, %s, %s, %s, %s)'
		if reverse:
			cursor.execute(query, (chr2, chr1, y_start, y_stop, x_start, x_stop))
			return self._reverse_fields(cursor.iter_results())
		else:
			cursor.execute(query, (chr1, chr2, x_start, x_stop, y_start, y_stop))
			return cursor.iter_results()
	
	def _reverse_fields(self, it):
		for type_id, y_start, y_stop, x_start, x_stop in it:
			yield type_id, x_start, x_stop, y_start, y_stop
