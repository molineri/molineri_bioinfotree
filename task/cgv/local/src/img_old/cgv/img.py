from __future__ import division
from __future__ import with_statement

from database import CgvDatabase
from cStringIO import StringIO
from itertools import izip
from math import pi
from paste.request import parse_formvars
import cairo

def app_factory(global_conf, db_names, db_user, db_passwd, db_pool_size, type_spec):
	def load_type_spec(filename):
		type_spec = []
		with file(filename, 'r') as fd:
			for lineno, line in enumerate(fd):
				tokens = line.rstrip().split('\t')
				try:
					if len(tokens) != 5:
						raise ValueError
					type_spec.append( (int(tokens[0]),) + tuple(float(f) for f in tokens[1:]) )
				except ValueError:
					raise ValueError, 'invalid type spec at line %d of file %s' % (lineno+1, filename)
		return type_spec
	
	db_names = tuple(n for n in db_names.split() if len(n))
	if len(db_names) == 0:
		raise ValueError, 'invalid database name list'
	
	try:
		db_pool_size = int(db_pool_size)
		if db_pool_size < 1 or db_pool_size > 20:
			raise ValueError
	except ValueError:
		raise ValueError, 'invalid database pool size'
	
	return ImgApp(db_names, db_user, db_passwd, db_pool_size, load_type_spec(type_spec))

class ImgApp(object):
	REQUIRED = object()
	
	def __init__(self, db_names, db_user, db_passwd, db_pool_size, types):
		self.cgv_db = CgvDatabase(db_user, db_passwd, db_pool_size)
		self.cgv_db_names = db_names
		self.types = types
		self.param_spec = (('db',      's', self.REQUIRED),
		                   ('x_chr',   's', self.REQUIRED),
		                   ('y_chr',   's', self.REQUIRED),
		                   ('x_start', 'i', self.REQUIRED),
		                   ('x_stop',  'i', self.REQUIRED),
		                   ('y_start', 'i', self.REQUIRED),
		                   ('y_stop',  'i', self.REQUIRED),
		                   ('tracks',  's', self.REQUIRED),
		                   ('width',   'i', 600),
		                   ('height',  'i', 600)) + tuple(('color%d' % t[0], 'c', t[1:]) for t in self.types)
	
	def __call__(self, environ, start_response):
		params = self._process_params(parse_formvars(environ))
		if params:
			color_map = self._build_color_map(params)
			start_response('200 OK', [('Content-Type', 'image/png')])
			return [ self._draw(color_map, *self._extract_draw_params(params)) ]
		else:
			start_response('400 Bad Request', [('Content-Type', 'text/plain')])
			return [ 'Malformed query.' ]
	
	def _process_params(self, vars):
		params = {}
		
		for name, tp, default_value in self.param_spec:
			value = vars.get(name, default_value)
			if value == self.REQUIRED:
				return None
			elif value != default_value:
				try:
					if tp == 'i':
						value = int(value)
						if value < 0:
							raise ValueError
					elif tp == 'c':
						if len(value) != 8:
							raise ValueError
						else:
							value = tuple([ int(value[:2], 16)  / 255,
							                int(value[2:4], 16) / 255,
							                int(value[4:6], 16) / 255,
							                int(value[6:], 16)  / 255 ])
				except ValueError:
					return None
			
			params[name] = value
		
		if params['db'] not in self.cgv_db_names:
			return None
		elif len(params['tracks']) != len(self.types):
			return None
		elif params['width'] < 16 or params['width'] > 1024:
			return None
		elif params['height'] < 16 or params['height'] > 1024:
			return None
		elif params['x_stop'] <= params['x_start']:
			return None
		elif params['y_stop'] <= params['y_start']:
			return None
		
		return params
	
	def _build_color_map(self, params):
		color_map = {}
		for state, info in izip(params['tracks'], self.types):
			if state == '1':
				color_map[info[0]] = params['color%d' % info[0]]
		return color_map
	
	def _extract_draw_params(self, params):
		return tuple(params[n] for n in ('width', 'height', 'db', 'x_chr', 'y_chr', 'x_start', 'x_stop', 'y_start', 'y_stop'))
	
	def _draw(self, color_map, width, height, db, x_chr, y_chr, x_start, x_stop, y_start, y_stop):
		import time
		start_time = time.time()
		
		ctr = CoordinateTransformer(x_start, x_stop - x_start, y_start, y_stop - y_start, width, height)
		
		surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, width, height)
		ctx = cairo.Context(surface)
		line_width = 5
		ctx.set_line_width(line_width)
		ctx.set_line_cap(cairo.LINE_CAP_ROUND)
		
		region_counter = 0
		segment_counter = 0
		
		# vertical stripes
		for type_id, start, stop in self.cgv_db.iter_regions(db, x_chr, x_start, x_stop):
			if type_id not in color_map:
				continue
			
			x, w = ctr.transform_horizontal_region(start, stop - start)
			ctx.set_source_rgba(*color_map[type_id])
			ctx.rectangle(x, 0, w, height)
			ctx.fill()
			
			region_counter += 1
		
		# horizonal stripes
		for type_id, start, stop in self.cgv_db.iter_regions(db, y_chr, y_start, y_stop):
			if type_id not in color_map:
				continue
			
			y, h = ctr.transform_vertical_region(start, stop - start)
			ctx.set_source_rgba(*color_map[type_id])
			ctx.rectangle(0, height - y, width, h)
			ctx.fill()
			
			region_counter += 1
		
		# segments
		for type_id, sx_start, sx_stop, sy_start, sy_stop in self.cgv_db.iter_segments(db, x_chr, y_chr, x_start, x_stop, y_start, y_stop):
			if type_id not in color_map:
				continue
			
			sx_start, sx_stop, sy_start, sy_stop = ctr.transform_segment(sx_start, sx_stop, sy_start, sy_stop)
			ctx.set_source_rgba(*color_map[type_id])
			if abs(sx_stop-sx_start) < line_width and abs(sy_stop-sy_start) < line_width:
				ctx.arc(sx_start, height - sy_start, line_width/2, 0, 2*pi)
				ctx.fill()
			else:
				ctx.move_to(sx_start, height - sy_start)
				ctx.line_to(sx_stop, height - sy_stop)
				ctx.stroke()
			
			segment_counter += 1
		
		# rendering
		png_buffer = StringIO()
		surface.write_to_png(png_buffer)
		
		elapsed_time = time.time() - start_time
		print '* Processed %d regions and %d segments in %f seconds.' % (region_counter, segment_counter, elapsed_time)
		
		return png_buffer.getvalue()

class CoordinateTransformer(object):
	TOP    = 1
	BOTTOM = 2
	RIGHT  = 4
	LEFT   = 8
	
	def __init__(self, original_x, original_w, original_y, original_h, space_w, space_h):
		self.original_x = original_x
		self.original_w = original_w
		self.original_y = original_y
		self.original_h = original_h
		
		self.space_w = space_w
		self.space_h = space_h
		
		self.scale_w = self.space_w / self.original_w
		self.scale_h = self.space_h / self.original_h
	
	def transform_horizontal_region(self, x, w):
		# This function assumes that the transformed region at least
		# partially overlaps the target space. If that's not the case,
		# function behaviour is undefined.
		
		x = (x - self.original_x) * self.scale_w
		w = w * self.scale_w
		
		if x < 0:
			w += x
			x = 0
		if x + w > self.space_w:
			w = self.space_w - x
		
		return x, w
	
	def transform_vertical_region(self, y, h):
		# This function assumes that the transformed region at least
		# partially overlaps the target space. If that's not the case,
		# function behaviour is undefined.
		
		y = (y - self.original_y) * self.scale_h
		h = h * self.scale_h
		
		if y < 0:
			h += y
			y = 0
		if y + h > self.space_h:
			h = self.space_h - y
		
		return y, h
	
	def transform_segment(self, x1, x2, y1, y2):
		# This function assumes that the transformed segment at least
		# partially overlaps the target space. If that's not the case,
		# function behaviour is undefined.
		
		# translation & scaling
		w = (x2 - x1) * self.scale_w
		h = (y2 - y1) * self.scale_h
		
		x1 = (x1 - self.original_x) * self.scale_w
		x2 = x1 + w
		y1 = (y1 - self.original_y) * self.scale_h
		y2 = y1 + h
		
		# clipping (Cohen & Sutherland)
		region_code1 = self._comp_region_code(x1, y1)
		region_code2 = self._comp_region_code(x2, y2)
		
		while True:
			if not (region_code1 | region_code2):
				return x1, x2, y1, y2
			else:
				outside_code = region_code1 if region_code1 != 0 else region_code2
				if outside_code & self.TOP:
					x = x1 + (x2 - x1) * (self.space_h - y1) / (y2 - y1)
					y = self.space_h
				elif outside_code & self.BOTTOM:
					x = x1 + (x2 - x1) * (-y1) / (y2 - y1)
					y = 0
				elif outside_code & self.RIGHT:
					y = y1 + (y2 - y1) * (self.space_w - x1) / (x2 - x1)
					x = self.space_w
				else:
					y = y1 + (y2 - y1) * (-x1) / (x2 - x1)
					x = 0
				
				if outside_code == region_code1:
					x1 = x
					y1 = y
					region_code1 = self._comp_region_code(x1, y1)
				else:
					x2 = x
					y2 = y
					region_code2 = self._comp_region_code(x2, y2)
	
	def _comp_region_code(self, x, y):
		code = 0
		
		if y > self.space_h:
			code |= self.TOP
		elif y < 0:
			code |= self.BOTTOM
		
		if x > self.space_w:
			code |= self.RIGHT
		elif x < 0:
			code |= self.LEFT
		
		return code
