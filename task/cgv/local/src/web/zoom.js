Zoom = {
	callback: null,
	_target: null,
	_target_x_min: null,
	_target_x_max: null,
	_target_y_min: null,
	_target_y_max: null,
	_start_point: null,
	_region_tl: null,
	_region_dimensions: null,
	_stretch_handler: null,
	_stop_handler: null,
	
	install: function(e) {
		connect(e, 'onmousedown', Zoom.start);
	},
	
	start: function(e) {
		if (e.mouse().button.left)
		{
			e.stop();
			
			Zoom._target = e.target();
			var target_pos = getElementPosition(Zoom._target);
			Zoom._target_x_min = target_pos.x;
			Zoom._target_y_min = target_pos.y;
			var target_dims = getElementDimensions(Zoom._target);
			Zoom._target_x_max = Zoom._target_x_min + target_dims.w;
			Zoom._target_y_max = Zoom._target_y_min + target_dims.h;
			
			var mouse_pos = e.mouse().page;
			Zoom._start_point = new Coordinates(mouse_pos.x, mouse_pos.y);
			
			Zoom._stretch_handler = connect(document, 'onmousemove', Zoom._stretch);
			Zoom._stop_handler = connect(document, 'onmouseup', Zoom._stop);
		}
	},
	
	_stretch: function(e) {
		e.stop();
		
		var mouse_pos = e.mouse().page;
		var x_min = Math.min(Zoom._start_point.x, mouse_pos.x);
		x_min = Math.max(x_min, Zoom._target_x_min);
		var x_max = Math.max(Zoom._start_point.x, mouse_pos.x);
		x_max = Math.min(x_max, Zoom._target_x_max);
		var y_min = Math.min(Zoom._start_point.y, mouse_pos.y);
		y_min = Math.max(y_min, Zoom._target_y_min);
		var y_max = Math.max(Zoom._start_point.y, mouse_pos.y);
		y_max = Math.min(y_max, Zoom._target_y_max);
		
		Zoom._region_tl = new Coordinates(x_min, y_min);
		Zoom._region_dimensions = new Dimensions(x_max-x_min, y_max-y_min);
		
		setElementPosition('zoom_region', Zoom._region_tl);
		setElementDimensions('zoom_region', Zoom._region_dimensions);
	},
	
	_stop: function(e) {
		setElementDimensions('zoom_region', new Dimensions(0, 0));
		disconnect(Zoom._stretch_handler);
		disconnect(Zoom._stop_handler);
		
		var mouse_pos = e.mouse().page;
		if ((mouse_pos.x != Zoom._start_point.x || mouse_pos.y != Zoom._start_point.y) && Zoom.callback != null)
		{
			var start_point = new Coordinates(Zoom._region_tl.x - Zoom._target_x_min, Zoom._region_tl.y - Zoom._target_y_min);
			Zoom.callback(Zoom._target, start_point, Zoom._region_dimensions);
		}
		
		Zoom._target = null;
	}
};

addLoadEvent(
	function() {
		appendChildNodes(currentDocument().body, DIV({'id': 'zoom_region'}));
	});
