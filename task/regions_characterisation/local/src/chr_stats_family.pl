#!/usr/bin/perl

use warnings;
use strict;
$,="\t";
$\="\n";

my @R=<>;
my $N=scalar(@R);
my %chr_M;

for(@R){
	my ($chr) = m/^(\w+)\s+/;
	$chr_M{$chr}++;
}

my @all_chr=sort keys %chr_M;
my %chr_x;
for(@all_chr){
	$chr_x{$_}=0;
}

my $Bonferroni = $N * scalar(@all_chr);

my $pre_fam=undef;
my $n=0;
for(@R){
	chomp;
	my @F=split;
	my $fam = $F[3];
	my $chr = $F[0];
	die("ERROR: unsorted input (family)")  if defined($pre_fam) and ($pre_fam > $fam);
	
	if(defined($pre_fam) and $pre_fam != $fam){
		for(@all_chr){
			#my $out=`Ipergeo $N $chr_M{$_} $n $chr_x{$_}`;
			#$out =~ s/\w+:\s//g;
			#chomp $out;
			#$out =~ s/\t([^\s]+)$//;
			print $N, $chr_M{$_}, $n, $chr_x{$_}, $Bonferroni, $pre_fam, $_; 
			$chr_x{$_}=0;
		}
		$n=0;
	}
	
	$chr_x{$chr}++;
	$n++;
	$pre_fam = $fam;

}
if(defined($pre_fam)){
	for(@all_chr){
		#my $out=`Ipergeo $N $chr_M{$_} $n $chr_x{$_}`;
		#$out =~ s/\w+:\s//g;
		#chomp $out;
		#$out =~ s/\t([^\s]+)$//;
		print $N, $chr_M{$_}, $n, $chr_x{$_}, $Bonferroni, $pre_fam, $_;
	}
}
