#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Long;

$\="\n";

my $usage = " $0 -c 0.9 -drop 5 -n 1 -m 1.2 -g 1.5 -l 16 -s 6 -t < qualcosa.consensus_matrix_c
	-c cutoff on nucleotide frequency
	-drop dropoff
	-n nucleotide match value (to sum)
	-m nucleotide mismatch value (to subtract)
	-g gap value (to subtract)
	-l min output sequence length (>= 1)
	-s min output sequence score  (>= 0)
	-t if file in STDIN is already transposed";

my $cutoff  = undef;
my $dropoff = undef;
my $n_value = undef;
my $m_value = undef;
my $g_value = undef;
my $min_len = undef;
my $score_cutoff = undef;
my $transpose = undef;
GetOptions (
	'cutoff|c=f'   => \$cutoff,
	'drop|drop=f'  => \$dropoff,
	'n_value|n=f'  => \$n_value,
	'm_value|m=f'  => \$m_value,
	'g_value|g=f'  => \$g_value,
	'min_len|l=i'  => \$min_len,
	's_cutoff|s=f' => \$score_cutoff,
	'transp|t'   => \$transpose
) or die ($usage);

die $usage if (!defined $cutoff or !defined $dropoff or !defined $n_value or !defined $m_value or !defined $g_value);

$min_len = 1 if !defined $min_len;
die $usage if ($min_len <= 0);

$score_cutoff = 0 if !defined $score_cutoff;
die $usage if ($score_cutoff <= 0);


my $FH=undef;
if (!$transpose) {
	my $consensus_file = shift @ARGV;
	die $usage if (!defined $consensus_file);
	open $FH, "transpose $consensus_file |" or die "Can't open file ($consensus_file)";
} else {
	$FH=*STDIN;
}


chomp (my $intest = <$FH>);
my @letters = split /\t/,$intest;
my $l = scalar @letters;

my @seq = ();
my $old_score = undef;
my $new_score = undef;
my $max_score = undef;
my $max_idx   = undef;

while (my $line = <$FH>) {
	chomp $line;
	my @F = split /\t/,$line;

	my $max = 0;
	my $idx = 0;
	for (my $i=0; $i<$l; $i++) {
		if ($F[$i] > $max) {
			$max = $F[$i];
			$idx = $i;
		}
	}

	if (!defined $new_score) {
		if ($max >= $cutoff) {
			if ($letters[$idx] ne '-') {
				push @seq,$letters[$idx];
				$new_score = $n_value;
				$max_score = $new_score;
				$max_idx   = 0;
			}
		}
		next;
	}

	$old_score = $new_score;

	if ($max >= $cutoff) {
		if ($letters[$idx] ne '-') {
			$new_score += $n_value;
			if ($new_score >= $max_score) {
				$max_score = $new_score;
				$max_idx   = scalar @seq;
			}
		}
	} else {
		if ($letters[$idx] eq '-') {
			$new_score -= $g_value;
		} else {
			$new_score -= $m_value;
		}
	}

	if ($max_score - $new_score <= $dropoff) {
		push @seq,$letters[$idx];
		next;
	}

	# ($max_score - $new_score > $dropoff)
	splice @seq, $max_idx + 1;
	@seq = map {$_ = $_ eq '-' ? '' : $_} @seq;
	my $out = join ('',@seq); 
	print '#', $out if ( (length $out >= $min_len) and ($max_score >= $score_cutoff) );
	@seq = ();
	$out = '';
	$old_score = undef;
	$new_score = undef;
	$max_score = undef;
	$max_idx   = undef;
}

if (defined $max_idx) {
	splice @seq, $max_idx + 1;
	@seq = map {$_ = $_ eq '-' ? '' : $_} @seq;
	my $out = join ('',@seq); 
	print '#', $out if ( (length $out >= $min_len) and ($max_score >= $score_cutoff) );
}
