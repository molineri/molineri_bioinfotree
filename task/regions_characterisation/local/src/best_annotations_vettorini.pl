#!/usr/bin/perl

use warnings;
use strict;

$\="\n";
$,="\t";

my $usage = "$0 1e-4 *.annote.overrepr.Bonferroni";

my $cutoff = shift;
die $usage if $cutoff > 1;

my @label=('C','E','5','3','I','U','N');

while(<>){
	chomp;
	my @F=split;
	my $id = shift @F;
	my $i=0;
	for(@F){
		print $id,$label[$i],$_ if $_ < $cutoff;
		$i++;
	}
}
