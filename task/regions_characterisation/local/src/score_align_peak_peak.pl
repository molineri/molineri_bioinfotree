#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;

use constant ALIGN_LEN   => 2;
use constant GAPS_SX_LEN => 4;
use constant GAPS_DX_LEN => 5;
use constant ALIGN_SX_B  => 6;
use constant ALIGN_SX_E  => 7;
use constant ALIGN_DX_B  => 8;
use constant ALIGN_DX_E  => 9;
use constant STRAND      => 10;


$,="\t";
$\="\n";

my $usage = "cat regions_1_50.coverage_correct_triggered.wublast.parsed | $0";


my $rid      = undef; 
my $rid_last = undef;
my @block_rows=();

while (my $line = <>) {
	chomp $line;
	my @F = split /\t/,$line;

	$rid = $F[0]. "\t" .$F[1];

	if (defined($rid_last) and ($rid ne $rid_last) ) {
		my $len_sx = &process_block(\@block_rows, ALIGN_SX_B, ALIGN_SX_E);
		my $len_dx = &process_block(\@block_rows, ALIGN_DX_B, ALIGN_DX_E);
		my $strand = &process_strand(\@block_rows, ALIGN_LEN, STRAND);
		my $ratio = &process_ratio(\@block_rows, $rid_last, ALIGN_LEN, GAPS_SX_LEN, GAPS_DX_LEN);
		print $rid_last,$len_sx,$len_dx,$strand,@{$ratio};
		@block_rows = (\@F);
		$rid_last = $rid;
	} else {
		push @block_rows, \@F;
		$rid_last = $rid;
	}
}

if (scalar(@block_rows)) {
	my $len_sx = &process_block(\@block_rows, ALIGN_SX_B, ALIGN_SX_E);
	my $len_dx = &process_block(\@block_rows, ALIGN_DX_B, ALIGN_DX_E);
	my $strand = &process_strand(\@block_rows, ALIGN_LEN, STRAND);
	my $ratio = &process_ratio(\@block_rows, $rid_last, ALIGN_LEN, GAPS_SX_LEN, GAPS_DX_LEN);
	print $rid,$len_sx,$len_dx,$strand,@{$ratio};
}



sub process_block
{
	my $array_ref = shift;
	my $col_b = shift;
	my $col_e = shift;

	my @array = sort {${$a}[$col_b] <=> ${$b}[$col_b]} @{$array_ref};

	my @union = ();
	my $union_len=undef;
	foreach (@array) {
		my $b = ${$_}[$col_b];
		my $e = ${$_}[$col_e];
		if (!@union) {
			@union = ($b, $e);
			$union_len = $union[1] - $union[0];
		}elsif ($b > $union[1]) {
			@union=($b, $e);
			$union_len += $union[1] - $union[0];
		}else{
			if ($e > $union[1]) {
				$union_len += ($e - $union[1]);
				$union[1] = $e;
			}
		}
	}
	if(!defined($union[1]) or !defined($union[0])){
		for(@array){
			print "ERR:", @{$_};
		}
		die;
	}

	return $union_len;
}


sub process_strand
{
	my $array_ref = shift;
	my $align_len_col = shift;
	my $strand_col = shift;

	my $strand = undef;
	my $max_len = 0;
	foreach (@{$array_ref}) {
		if (${$_}[$align_len_col] > $max_len) {
			$max_len = ${$_}[$align_len_col];
			$strand = ${$_}[$strand_col];
		}
	}

	return $strand;
}

sub process_ratio
{
	 my $rows_ref = shift;
	 my $rid = shift;
	 my $align_len_col = shift;
	 my $gap_sx_col = shift;
	 my $gap_dx_col = shift;

	 my ($q_b,$q_e,$t_b,$t_e)=($rid=~/^\d+:\w+,(\d+),(\d+)\t\d+:\w+,(\d+),(\d+)$/); 
	 my $reg_len_max = undef;
	 my $reg_len_min = undef;
	 if ( ($q_e - $q_b) > ($t_e - $t_b) ) {
		$reg_len_max = ($q_e - $q_b);
		$reg_len_min = ($t_e - $t_b);
	 } else {
		$reg_len_max = ($t_e - $t_b);
		$reg_len_min = ($q_e - $q_b);
	 }

	 my $max_len = 0;
	 for (@{$rows_ref}) {
#		$gap_col = (${$_}[$gap_sx_col] > ${$_}[$gap_dx_col]) ? $gap_sx_col : $gap_dx_col if !defined $gap_col;
		my $align_len = ${$_}[$align_len_col]; # - ${$_}[$gap_col];
		$max_len = $align_len if ($align_len > $max_len);
	}

	my @out = ($max_len/$reg_len_max, $max_len/$reg_len_min);
	return \@out;
}


