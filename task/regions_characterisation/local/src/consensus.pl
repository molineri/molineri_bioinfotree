#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;


$,="\t";
$\="\n";

my $usage = "$0 [-m] [-s] < qualcosa.multialign
	with -m option prints a consensus matrix
		default output: foreach position, the most frequent char
	-s if input file only contains the multialignment of a single group of regions";

my $matrix = undef;
my $single_group = undef;
GetOptions (
	'matrix|m' => \$matrix,
	'single|s' => \$single_group
) or die ($usage);



my %consesus_matrix = ();
my $pos = 0;
my $seq_num = 0;
while (my $line = <>) {
	chomp $line;
	next if (!$line);

	if (defined $single_group) {
		if ($line =~ /^#/) {
			$pos = 0;
			$seq_num++;
			next;
		}
	} else {
		if ($line =~ /^#/) {
			print $line;
			next;
		}
		if ($line =~ /^>/) {
			$pos = 0;
			$seq_num++;
			next;
		}
	}

	while (my $char = substr $line,0,1,'') {
		if (!defined $consesus_matrix{$char}[$pos]) {
			$consesus_matrix{$char}[$pos] = 1;
		} else {
			$consesus_matrix{$char}[$pos]++;
		}
		$pos++;
	}
}

foreach (sort keys %consesus_matrix) {
	for (my $i=0; $i<$pos; $i++) {
		if (!defined $consesus_matrix{$_}[$i]) {
			$consesus_matrix{$_}[$i] = 0;
		} else {
			$consesus_matrix{$_}[$i] /= $seq_num;
		}
	}

	if (defined $matrix) {
		print $_,@{$consesus_matrix{$_}};
	}
}

if (!defined $matrix) {
	my $sequence = '';
	for (my $i=0; $i<$pos; $i++) {
		my $cons_char = undef;
		my $max = undef;
		foreach (sort keys %consesus_matrix) {
			if ( (!defined $max) or ($consesus_matrix{$_}[$i] > $max) ) {
				$max = $consesus_matrix{$_}[$i];
				$cons_char = $_;
			}
		}
		$sequence .= $cons_char;
	}
	print $sequence;
}


