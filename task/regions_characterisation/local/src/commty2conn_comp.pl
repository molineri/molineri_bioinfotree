#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;

$\="\n";
$,="\t";

my $usage = "$0 < qualcosa.commty";

my $n_min = undef;
GetOptions (
	'n_min|n=i' => \$n_min
) or die ($usage);
$n_min = 3 if !defined $n_min;


my $conn_comp_pre = undef;
my $community_pre = undef;
my @commty = ();
while (<>) {
	chomp;
	if (m/^>(\d+)$/) {
		print ">$community_pre"."_".$conn_comp_pre, @commty if (scalar @commty >= $n_min);
		$community_pre = $1;
		$conn_comp_pre = undef;
		next;
	}
	next if m/^#/;

	my @F = split /\t/;

	if (!defined $conn_comp_pre) {
		$conn_comp_pre = $F[1];
		@commty = ($F[0]);
		next;
	}

	if ($F[1] == $conn_comp_pre) {
		push @commty, $F[0];
	} else {
		print ">$community_pre"."_".$conn_comp_pre, @commty if (scalar @commty >= $n_min);
		@commty = ($F[0]);
		$conn_comp_pre = $F[1];
	}
}

print ">$community_pre"."_".$conn_comp_pre, @commty if (scalar @commty >= $n_min);
