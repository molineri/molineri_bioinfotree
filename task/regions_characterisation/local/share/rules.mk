SHELL := /bin/bash

BIN_DIR := $(BIOINFO_ROOT)/task/regions_characterisation/local/bin
BIN_DIR_ANNOTATION := $(BIOINFO_ROOT)/task/annotations/local/bin

SPECIES ?= hsapiens
UCSC_VERSION ?= hg18
ENS_VERSION ?= 40
GO_ENS_VERSION ?= 42
PFAM_ENS_VERSION ?= 42
PFAM_ANNOTATION_DIR ?= $(BIOINFO_ROOT)/task/annotations/dataset/pfam/$(SPECIES)/$(PFAM_ENS_VERSION)
ANNOTATION_DIR ?= $(BIOINFO_ROOT)/task/annotations/dataset/ensembl/$(SPECIES)/$(ENS_VERSION)
SEQ_DIR?=$(BIOINFO_ROOT)/task/sequences/dataset/ensembl/$(SPECIES)/$(ENS_VERSION)
UCSC_ANNOTATION_DIR ?= $(BIOINFO_ROOT)/task/annotations/dataset/ucsc/$(SPECIES)/$(UCSC_VERSION)

BACKGROUND ?= $(BIOINFO_ROOT)/task/align_coverage/dataset/hsapiens_hsapiens_ensembl40_wublast_pam10/0/regions_1_1.coverage_correct_triggered
REGIONS_LIST ?= $(BIOINFO_ROOT)/task/align_coverage/dataset/hsapiens_hsapiens_ensembl40_wublast_pam10/0/regions.1_50.coverage_correct_triggered.list
REGIONS_FA ?= $(BIOINFO_ROOT)/task/align_coverage/dataset/hsapiens_hsapiens_ensembl40_wublast_pam10/0/regions.1_50.coverage_correct_triggered.fa
REGIONS_FA_XND ?= $(BIOINFO_ROOT)/task/align_coverage/dataset/hsapiens_hsapiens_ensembl40_wublast_pam10/0/regions.1_50.coverage_correct_triggered.fa.xnd

COORDS_NONREDUNDANT ?= C E 5 3 I U N
NORMALIZE_VETTORINI ?= -r
COORDS_NONREDUNDANT2 ?= C 5 3 I U N
VETTORINI_CUTOFF ?= 1e-5 

NODES_NUMBER_CUTOFF ?= 20
PARSED_SCORE_SCRIPT ?= score_align_peak_peak
PARSED2_SCORE_SCRIPT ?= score_align_peak_peak2
PARSED4_SCORE_SCRIPT ?= score_align_peak_peak4
CONSENSUS_CUTOFF ?= 0.8
CONSENSUS_DROPOFF ?= 7.3
CONSENSUS_MATCH_VALUE ?= 1
CONSENSUS_MISMATCH_VALUE ?= 1.2
CONSENSUS_GAP_VALUE ?= 1.5
CONSENSUS_MIN_LEN ?= 30
CONSENSUS_MIN_SCORE ?= 6
PATTERNS_RATIO_CUTOFF ?= 0.8

ALL_CHR ?= 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X Y

SCORE_CUTOFF ?= 0.52
ALL_SCORE_CUTOFF ?= 0.5 0.7 0.9
CONN_COMP_NUMBER ?= 1

WUBLAST_REGION_OPTIONS ?= W=10 matrix=pam10 Q=2046 R=18 S2=2232 gapS2=5580 S=5580 V=2147483647 Y=3080436051 Z=3080436051 kap hspmax=0 gspmax=0 cpus=1 mformat=2 B=2147483647
WUBLAST_GENOME_OPTIONS ?= matrix=pam10 Q=2046 R=18 S2=2232 gapS2=5580 S=5580 W=10 mformat=2 hspmax=0 gspmax=0 V=2147483647 B=2147483647 kap cpus=1
WUBLAST_GENOME_STANDARD_OPTIONS ?= E=0.000001 mformat=2 hspmax=0 V=2147483647 B=2147483647 hspsepSmax=10000
SCORE_CUTOFF_GENOME ?= 0.8


.SECONDARY:
.DELETE_ON_ERROR:


include rules_db.mk
include rules_annote.mk


###############################################
#
#	regole di visualizzazione
#

%.ps : %.gnuplot
	gnuplot $*.gnuplot > $*.ps



###############################################
############## RETE TRA PICCHI ################
###############################################

#blastn $< $< W=3 Y=1 Z=1 hspmax=0 gspmax=0 cpus=1 E 0.00001 seqtest mformat=2 V=2147483647 B=2147483647
regions.wublast_all: $(REGIONS_FA) $(REGIONS_FA_XND)
	source `which wublast-env`; \
	blastn $< $< $(WUBLAST_REGION_OPTIONS) \
	> $@ 2> $@.err

cancellami:
	echo  $(REGIONS_FA)

#repeat_fasta_pipe 'blastn $< - Y=1 Z=1 E=0.0001 mformat=2 V=2147483647 B=2147483647' < $< 
regions.wublast: $(REGIONS_FA) $(REGIONS_FA_XND)
	source `which wublast-env`; \
	repeat_fasta_pipe 'blastn $< - $(WUBLAST_REGION_OPTIONS)' < $< \
	> $@ 2> $@.err

%.conn_comp.list: %.conn_comp
	cat $< | enumerate_rows -s -1 \
		| perl -lane '$$,="\t"; $$\="\n"; $$id=shift @F; for(@F){s/\d+://; print $$id,split(",",$$_)}' \
	> $@

%.list.ens_nearest_genes.coords: %.list $(ANNOTATION_DIR)/coords_gene
	cut -f 2- $< \
		| $(BIN_DIR_ANNOTATION)/region_nearest_genes.pl \
		-a $(word 2,$^) \
	> $@

%.ucsc_nearest_genes.coords: %.list $(UCSC_ANNOTATION_DIR)/known_genes.coords
	cut -f 2- $< \
		| $(BIN_DIR_ANNOTATION)/region_nearest_genes.pl \
		-a $(word 2,$^) \
	> $@

%_nearest_genes: %_nearest_genes.coords
	cut -f 1,6 $< \
	perl -lane '$$,="\t"; $$\="\n"; @id_list=split /,/,$$F[1]; foreach (@id_list) {print $$F[0],$$_}' \
	| sort -k2,2 > $@

%.patterns.fa: %.patterns
	perl -lane 'if(m/^>/){chomp; $$header=$$_; $$i=0;}else{ s/^#//; $$len = length($$_) - 1; print $$header."_".$$i.";".$$len; print $$_; $$i++}' $< > $@


%.out.patterns.len_distrib: %.out.patterns.fa
	grep '>' $< | tr ';' "\t" | cut -f 2 \
	| binner -n 100 -l -b m > $@

%.patterns.len_distrib.png: %.patterns.len_distrib
	(\
		echo "set terminal png;"; \
		echo "set logscale x; plot '$<' w l, '' w p pt 6 , '' w histep"; \
	) | gnuplot > $@

%.iter.patterns.fa: %.patterns.fa
	perl -e '$$\="\n"; $$count=0; while (<>) {chomp; if ($$_=~/^>/) {($$len) = ($$_=~/^>\d+.*;(\d+)$$/); print ">$$count;$$len"; $$count++;} else {print;} }' < $< > $@

%.iterate: %.patterns.fa
	if [ -s "$*.out.patterns.fa" ]; then \
		echo "file $*.out.patterns.fa already exists"; \
		exit 0; \
	fi; \
	make $@.clean; \
	make $*.iter.patterns.fa; \
	touch $*.iter.patterns.fa.changed; \
	make $*.out.patterns.fa; \
	if [ ! `echo $$?` -eq 0 ]; then \
		echo; \
		echo "ERRORS IN ITERATION PROCESS"; \
		echo; \
	else \
		rm -f $*.iter.patterns.fa*; \
	fi

%.iter.patterns.fa.add: %.iter.patterns.fa %.out.patterns.fa
	echo "aggiunta dei patterns rimanenti nel file di quelli definitivi";
	pattern_next_id=`grep '>' $(word 2,$^) | wc -l`; \
	perl -e "\$$\=\"\n\"; \$$,=\"\t\"; \$$id=$$pattern_next_id; while (<>) {chomp; if (\$$_=~/^>/) {my (\$$len) = (\$$_=~/^>\d+;(\d+)$$/); \$$_='>'.\$$id.';'.\$$len; \$$id++;}  print \$$_}" < $< >> $(word 2,$^);
	rm -f $*.iter.patterns.wublast*;

%.iterate.clean:
	rm -f $*.iter.patterns.wublast*;
	rm -f $*.iter.patterns.fa*;
	rm -f $*.out.patterns.fa;

%.out.patterns.fa: %.iter.patterns.fa %.iter.patterns.fa.changed
	echo; \
	echo '# 0. controllo sull`elenco dei patterns da allineare tra loro'; \
	echo; \
	if [ `wc -l < $<` -eq 0 ]; then exit 0; fi; \
	if [ `grep '>' $< | wc -l` -eq 1 ]; then \
		make $*.iter.patterns.fa.add; \
		exit 0; \
	fi; \
	rm $(word 2,$^); \
	echo; \
	echo '# 1. ricerca di allineamenti tra i patterns'; \
	echo; \
	make $*.iter.patterns.wublast.parsed4; \
	if [ `wc -l < $*.iter.patterns.wublast.parsed4` -eq 0 ]; then \
		make $*.iter.patterns.fa.add; \
		exit 0; \
	fi; \
	echo; \
	echo '# 2. costruzione della rete tra i patterns'; \
	echo; \
	make $*.iter.patterns.wublast.parsed4.$(SCORE_CUTOFF).score; \
	if [ `wc -l < $*.iter.patterns.wublast.parsed4.$(SCORE_CUTOFF).score` -eq 0 ]; then \
		make $*.iter.patterns.fa.add; \
		exit 0; \
	fi; \
	echo; \
	echo '# 3. ricerca delle comunita` di patterns'; \
	echo; \
	make $*.iter.patterns.wublast.parsed4.$(SCORE_CUTOFF).conn_comp.commty2; \
	if [ ! -s "$*.iter.patterns.wublast.parsed4.$(SCORE_CUTOFF).conn_comp.commty2" ]; then \
		make $*.iter.patterns.fa.add; \
		exit 0; \
	fi; \
	commty2=$*.iter.patterns.wublast.parsed4.$(SCORE_CUTOFF).conn_comp.commty2.conn_comp; \
	make $$commty2; \
	if [ ! -s "$$commty2" ]; then \
		make $*.iter.patterns.fa.add; \
		exit 0; \
	fi; \
	echo; \
	echo '# 4. i patterns che non compaiono nelle comunita` vengono aggiunti alla lista definitiva dei patterns'; \
	echo; \
	cut -f 2- $$commty2 | tr "\t" "\n" | sort > $$commty2.in_comm; \
	grep '>' $< | tr -d '>' | sort > $$commty2.initial; \
	comm -3 $$commty2.initial $$commty2.in_comm > $$commty2.excluded; \
	touch $@; \
	pattern_next_id=`grep '>' $@ | wc -l`; \
	perl -e "\$$\=\"\n\"; \$$,=\"\t\"; \$$id=$$pattern_next_id; while (<>) {chomp; my (\$$len) = (\$$_=~/^\d+;(\d+)$$/); print 'echo -e \'>'.\$$id.';'.\$$len.'\''; \$$id++; print 'get_fasta -n \''.\$$_.'\' $<'}" < $$commty2.excluded | bash >> $@; \
	rm -f $$commty.in_comm $$commty2.initial $$commty2.excluded; \
	echo; \
	echo '# 5. per ciascuna comunita` si cerca il multiallineamento tra i suoi patterns'; \
	echo; \
	target=$*.iter.patterns.wublast.parsed4.$(SCORE_CUTOFF).conn_comp.commty2.conn_comp.multialign; \
	make $$commty2.strand; \
	rm -f $$target.dnd $$target.out $$target.err; \
	repeat_fasta_pipe -n "\
		rm -f $$target.tmp; \
		echo '>\$$HEADER' >> $$target; \
		perl -lane "\""print 'get_fasta_strand \''.(join ' ',@F).'\''.' $<' "\"" | bash | tr ';' '_' > $$target.tmp; \
		clustalw -INFILE=$$target.tmp -TYPE=DNA -OUTPUT=GDE -CASE=UPPER -OUTORDER=ALIGNED -OUTFILE=$$target.out >> $$target.err;\
		perl -lne 'if (\$$_=~/^#.*_(\d+)\$$/) { \$$_=~s/_(\$$1)$$/;\$$1/;} print ' < $$target.out >> $$target; \
		rm -f $$target.out; \
	" < $$commty2.strand; \
	rm -f $$target.tmp $$target.dnd; \
	echo; \
	echo '# 6. si estraggono i nuovi patterns a partire dal multiallineamento'; \
	echo; \
	make $$commty2.consensus_matrix.patterns; \
	echo; \
	echo '# 7. i patterns che non hanno un buon multiallineamento (sono presenti nelle comunita` ma non nei nuovi patterns) vengono aggiunti alla lista definitiva dei patterns;' \
	echo; \
	repeat_fasta_pipe -n '\
		if [ `wc -l` -eq 0 ]; then echo $$HEADER; fi \
	' < $$commty2.consensus_matrix.patterns > $$commty2.consensus_matrix.patterns.lost ; \
	pattern_next_id=`grep '>' $@ | wc -l`; \
	grep -w -f $$commty2.consensus_matrix.patterns.lost $$commty2 \
		| perl -e "\$$\=\"\n\"; \$$,=\"\t\"; \$$id=$$pattern_next_id; while (<>) {chomp; my @F=split/\t/,\$$_; shift @F; foreach (@F) {my (\$$len) = (\$$_=~/^\d+;(\d+)$$/); print 'echo -e \'>'.\$$id.';'.\$$len.'\''; \$$id++; print 'get_fasta -n \''.\$$_.'\' $<'} }" | bash >> $@; \
	echo; \
	echo '# 8. si prendono i nuovi patterns come punto di partenza per l`iterazione successiva (si ridefiniscono gli ids)'; \
	echo; \
	perl -e '$$\="\n"; $$count=0; $$,="\t"; while (<>) {chomp; next if ($$_=~/^>/); $$_=~s/#//; my $$len=length($$_); print ">$$count;$$len"; print $$_; $$count++; }' < $$commty2.consensus_matrix.patterns > $<; \
	echo; \
	echo '# 9. si eliminano tutti files intermedi del ciclo di iterazione'; \
	echo; \
	rm $*.iter.patterns.wublast* $<.*; \
	echo '# 10. se e` necessario continuare ad iterare (il numero di patterns nuovi e` > 1) tocco il file $(word 2,$^)'; \
	new_patterns_n=`grep '>' $< | wc -l`; \
	if [ "$$new_patterns_n" -gt 1 ]; then touch $(word 2,$^); fi; \
	make $@


%.patterns.genome.wublast: %.patterns.fa $(SEQ_DIR)/genome.fa.xns
	source `which wublast-env`; \
	repeat_fasta_pipe 'blastn $(SEQ_DIR)/genome.fa - $(WUBLAST_GENOME_OPTIONS)' < $< \
	> $@ 2> $@.err

%.patterns.genome.standard_wublast: %.patterns.fa $(SEQ_DIR)/genome.fa.xns
	source `which wublast-env`; \
	repeat_fasta_pipe 'blastn $(SEQ_DIR)/genome.fa - $(WUBLAST_GENOME_STANDARD_OPTIONS)' < $< \
	> $@ 2> $@.err


%.patterns.wublast: %.patterns.fa %.patterns.fa.xnd
	source `which wublast-env`; \
	repeat_fasta_pipe 'blastn $< - E=0.00001 mformat=2' < $< \
	> $@ 2> $@.err

#%.fa.xnd: %.fa
#	xdformat -n $<; 

#blastn $< $< W=3 Y=1 Z=1 hspmax=0 gspmax=0 cpus=1 E 0.00001 seqtest mformat=2 V=2147483647 B=2147483647
#regions.%.wublast_all: regions.%.fa regions.%.fa.xnd
#	source `which wublast-env`; \
#	blastn $< $< $(WUBLAST_REGION_OPTIONS) \
#	> $@ 2> $@.err

#repeat_fasta_pipe 'blastn $< - Y=1 Z=1 E=0.0001 mformat=2 V=2147483647 B=2147483647' < $< 
regions.%.wublast: regions.%.fa regions.%.fa.xnd
	source `which wublast-env`; \
	repeat_fasta_pipe 'blastn $< - $(WUBLAST_REGION_OPTIONS)' < $< \
	> $@ 2> $@.err


%.wublast.parsed: %.wublast
	cut -f -2,7,11,14,16,18-19,21-22 $< \
		| perl -lane '$$,="\t"; $$\="\n"; next if ($$F[0] eq $$F[1]); ($$l_chr,$$l_b,$$l_e)=($$F[0]=~/^\d+:(\w+),(\d+),(\d+)$$/); ($$r_chr,$$r_b)=($$F[1]=~/^\d+:(\w+),(\d+),\d+$$/); next if ($$l_chr gt $$r_chr); next if ( ($$l_chr eq $$r_chr) and ($$l_b > $$r_b) ); $$F[10]="+"; if ($$F[6] > $$F[7]) {$$tmp=$$F[6]; $$F[6]=$$F[7]; $$F[7]=$$tmp; $$F[10]="-"} $$F[6]--; $$F[8]--; print @F' \
		| tr ':' "\t" | tr ',' "\t" | sort -k2,2 -k3,3n | uniq \
		| perl -lane '$$,="\t"; $$\="\n"; $$left_reg=$$F[0].":".$$F[1].",".$$F[2].",".$$F[3]; $$right_reg=$$F[4].":".$$F[5].",".$$F[6].",".$$F[7]; splice @F,0,8; print $$left_reg, $$right_reg, @F' \
	> $@
# formato output:
#	1. left_region_id ( 7316:1,124853,125263         (reg_id):(chr),(b),(e) )
#	2. right_region_id ( 13958:7,60402,60812         (reg_id):(chr),(b),(e) )
#	3. overall_alignment_length
#	4. percent identity over the alignment length (nel vecchio formato non c'era questa colonna)
#	5. query_gaps_length
#	6. target_gaps_length
#	7. query_begin
#	8. query_end
#	9. target_begin
#	10.target_end
#	11.strand

%.wublast.parsed2: %.wublast
	cut -f -2,7,11,14,16,18-19,21-22 $< \
		| perl -lane '$$,="\t"; $$\="\n"; next if ($$F[0] eq $$F[1]); ($$left_id,$$left_n,$$left_len)=($$F[0]=~/^(\d+(?:_\d+)*)_(\d+);(\d+)$$/); ($$right_id,$$right_n,$$right_len)=($$F[1]=~/^(\d+(?:_\d+)*)_(\d+);(\d+)$$/); next if ($$left_id gt $$right_id); next if ( ($$left_id eq $$right_id) and ($$left_n gt $$right_n) ); $$F[10]="+"; if ($$F[6] > $$F[7]) {$$tmp=$$F[6]; $$F[6]=$$F[7]; $$F[7]=$$tmp; $$F[10]="-"} $$F[6]--; $$F[8]--; print @F' \
		| sort -k1,1 -k2,2 | uniq \
		> $@
# .parsed2 per gli allineamenti dei patterns di consensi (no chr,b,e sul genoma)
# formato output:
#	1. left_region_id ( 0_1;174     (conn_comp)_(consensus_number);(left_reg_length) )
#	2. right_region_id ( 877_1;281     (conn_comp)_(consensus_number);(right_reg_length) )
#	3. overall_alignment_length
#	4. percent identity over the alignment length (nel vecchio formato non c'era questa colonna)
#	5. query_gaps_length
#	6. target_gaps_length
#	7. query_begin
#	8. query_end
#	9. target_begin
#	10.target_end
#	11.strand

%.wublast.parsed4: %.wublast
	cut -f -2,7,11,14,16,18-19,21-22 $< \
		| perl -lane '$$,="\t"; $$\="\n"; next if ($$F[0] eq $$F[1]); ($$left_id,$$left_len)=($$F[0]=~/^(\d+);(\d+)$$/); ($$right_id,$$right_len)=($$F[1]=~/^(\d+);(\d+)$$/); next if ($$left_id > $$right_id); $$F[10]="+"; if ($$F[6] > $$F[7]) {$$tmp=$$F[6]; $$F[6]=$$F[7]; $$F[7]=$$tmp; $$F[10]="-"} $$F[6]--; $$F[8]--; print @F' \
		| sort -k1,1 -k2,2 | uniq \
		> $@
# .parsed4 per gli allineamenti dei patterns di consensi (no chr,b,e sul genoma) nelle iterazioni
# formato output:
#	1. left_region_id ( 0;174     (pattern_id);(left_reg_length) )
#	2. right_region_id ( 1;281     (pattern_id);(right_reg_length) )
#	3. overall_alignment_length
#	4. percent identity over the alignment length (nel vecchio formato non c'era questa colonna)
#	5. query_gaps_length
#	6. target_gaps_length
#	7. query_begin
#	8. query_end
#	9. target_begin
#	10.target_end
#	11.strand

%.patterns.genome.wublast.$(SCORE_CUTOFF_GENOME).parsed3: %.patterns.genome.wublast
	cut -f -2,7,14,16,18-19,21-22 $< \
	| perl -lane '$$,="\t"; $$\="\n"; $$F[9]="+"; if ($$F[5] > $$F[6]) {$$tmp=$$F[5]; $$F[5]=$$F[6]; $$F[6]=$$tmp; $$F[9]="-"} $$F[5]--; $$F[7]--; print @F' \
	| sort -k1,1 -k2,2 | uniq \
	| tr ';' '\t' | awk 'BEGIN{OFS="\t"} ($$10-$$9)/$$2 > $(SCORE_CUTOFF_GENOME) {print $$3,$$9,$$10,$$1}' \
	| sed 's/chr//g' > $@
# .parsed3 per gli allineamenti dei patterns di consensi (no chr,b,e sul genoma) contro il genoma (chr,b,e)
# formato output:
# 	1. genome_chr
# 	2. genome_b
# 	3. genome_e
# 	4. pattern_id

%.parsed3.copy_distrib: %.parsed3
	repeat_group_pipe '\
		wc -l \
	' 4 < $< \
	| binner -l -n 100 > $@

%.copy_distrib.png: %.copy_distrib
	(\
		echo "set terminal png;"; \
		echo "set logscale x; plot '$<' w l, '' w p pt 6 , '' w histep"; \
	) | gnuplot > $@

%.patterns.genome.standard.wublast.$(SCORE_CUTOFF_GENOME).parsed3: %.patterns.genome.standard.wublast
	cut -f -2,7,14,16,18-19,21-22 $< \
	| perl -lane '$$,="\t"; $$\="\n"; $$F[9]="+"; if ($$F[5] > $$F[6]) {$$tmp=$$F[5]; $$F[5]=$$F[6]; $$F[6]=$$tmp; $$F[9]="-"} $$F[5]--; $$F[7]--; print @F' \
	| sort -k1,1 -k2,2 | uniq \
	| tr ';' '\t' | awk 'BEGIN{OFS="\t"} ($$10-$$9)/$$2 > $(SCORE_CUTOFF_GENOME) {print $$3,$$9,$$10,$$1}' \
	| sed 's/chr//g' > $@

%.reg_overlap: %.parsed3
	cut -f -3 $< | sort -k1,1 -k2,2n -k3,3n | enumerate_rows -r > $@.tmp
	cat $@.tmp | intersection -l $@.not - $@.tmp \
	| awk '$$8<$$9' | intersection_add_sim_diff >$@
	rm $@.tmp*
# formato output:
# 	1. chr
# 	2. intersection_b
# 	3. intersection_e
# 	4. left_region_b
# 	5. left_region_e
# 	6. right_region_b
# 	7. right_region_e
# 	8. (intersection_len) / (left_region_len)
# 	9. (intersection_len) / (left_region_len)
# 	10. 1 - (intersection_len / union_len)
# 	11. left_region_id
# 	12. right_region_id

%.reg_overlap.not: %.reg_overlap
	echo

%.reg_overlap.distrib: %.reg_overlap %.parsed3
	( \
		awk '{print $$11; print $$12}' $< \
		| sort | uniq -c | awk '{print $$1}'; \
		for i in $$(seq `cut -f 11,12 $< | tr "\t" "\n" | sort | uniq | wc -l` `wc -l < $(word 2,$^)`); do echo 1; done;\
	) | binner -n 50 > $@ 

%.simm.parsed: %.parsed
	awk 'BEGIN{OFS="\t"} {print $$0; print $$2,$$1,$$3,$$4,$$6,$$5,$$9,$$10,$$7,$$8,$$11}' $< > $@

%.len_ratio: %.parsed
	cat $< \
		| perl -lane '$$,="\t"; $$\="\n"; ($$t_b,$$t_e)=($$F[0]=~/^\d+:\w+,(\d+),(\d+)$$/); ($$q_b,$$q_e)=($$F[1]=~/^\d+:\w+,(\d+),(\d+)$$/); if ( ($$t_e - $$t_b) > ($$q_e - $$q_b) ) { $$ratio=($$F[2]-$$F[5])/($$q_e - $$q_b);} else {$$ratio=($$F[2]-$$F[4])/($$t_e - $$t_b);} print $$F[0],$$F[1],$$ratio;' \
	> $@

%.len_ratio.ps: %.len_ratio
	cat $< \
		| binner -n 20 \
	> $<.binner
	echo "set terminal postscript enhanced color 'Helvetica' 10; \
	set logscale y; plot '$<.binner' w p pt 6, '' w l, '' w histep" \
	| gnuplot > $@

%.double_score: %.parsed
	$(BIN_DIR)/$(PARSED_SCORE_SCRIPT) < $< > $@

%.parsed2.double_score: %.parsed2
	$(BIN_DIR)/$(PARSED2_SCORE_SCRIPT) < $< > $@

%.parsed2.score: %.parsed2.double_score
	cat $< \
		| perl -lane '$$,="\t"; $$\="\n"; ($$left_len)=($$F[0]=~/^(?:\d+_)+\d+;(\d+)$$/); ($$right_len)=($$F[1]=~/^(?:\d+_)+\d+;(\d+)$$/); $$score=( $$F[2]/($$left_len) + $$F[3]/($$right_len) )/2; splice @F,2,1; $$F[2]=$$score; print @F;' \
		>$@

%.parsed4.double_score: %.parsed4
	$(BIN_DIR)/$(PARSED4_SCORE_SCRIPT) < $< > $@

%.parsed4.score: %.parsed4.double_score
	cat $< \
		| perl -lane '$$,="\t"; $$\="\n"; ($$left_len)=($$F[0]=~/^\d+;(\d+)$$/); ($$right_len)=($$F[1]=~/^\d+;(\d+)$$/); $$score=( $$F[2]/($$left_len) + $$F[3]/($$right_len) )/2; splice @F,2,1; $$F[2]=$$score; print @F;' \
		>$@

%.conn_comp.cliques.conn_comp.score: %.conn_comp.cliques.conn_comp %.score
	$(BIN_DIR)/group_score -c $< < $(word 2,$^) > $@

%.conn_comp.commty2.conn_comp.score: %.conn_comp.commty2.conn_comp %.score
	$(BIN_DIR)/group_score -c $< < $(word 2,$^) > $@

%.$(SCORE_CUTOFF).score: %.score
	cat $< | awk '$$5 > $(SCORE_CUTOFF)' > $@

%.score: %.double_score
	cat $< \
		| perl -lane '$$,="\t"; $$\="\n"; ($$l_b,$$l_e)=($$F[0]=~/^\d+:\w+,(\d+),(\d+)$$/); ($$r_b,$$r_e)=($$F[1]=~/^\d+:\w+,(\d+),(\d+)$$/); $$score=( $$F[2]/($$l_e-$$l_b) + $$F[3]/($$r_e-$$r_b) )/2; splice @F,2,1; $$F[2]=$$score; print @F;' \
		>$@

%.best.score: %.score 
	find_best_multi 1 5 $<  \
	| perl -lane '$$,="\t"; $$\="\n"; $$a=shift @F; $$b=shift @F; if ($$a lt $$b) {print $$a,$$b,@F;} else {print $$b,$$a,@F}' \
	| sort | uniq > $@

%.connectivity: %.score
	cut -f -2 $< | connectivity > $@

%.id: %.connectivity
	cat $< \
	| tr ":," "\t\t" | awk '{print $$1 "\t" $$1 ":" $$2 "," $$3 "," $$4}' \
	> $@

%.score_connectivity: %.connectivity %.score
	traduci_tab $^ > $@

%.scores.ps: %.score
	cut -f 5 $< \
		| binner -n 20 \
	> $*.scores.binner
	echo "set terminal postscript enhanced color 'Helvetica' 10; \
	set logscale y; plot '$*.scores.binner' w p pt 6, '' w l, '' w histep" \
	| gnuplot > $@

%.connectivity.ps: %.connectivity
	cut -f 2 $< \
		| binner -l -n 20 \
	> $*.connectivity.binner
	echo "set terminal postscript enhanced color 'Helvetica' 10; \
	set logscale x; set logscale y; plot '$*.connectivity.binner' w p pt 6, '' w l, '' w histep" \
	| gnuplot > $@

%.connectivity_nonlog.ps: %.connectivity
	cut -f 2 $< \
		| binner -n 20 \
	> $*.connectivity_nonlog.binner
	echo "set terminal postscript enhanced color 'Helvetica' 10; \
	set logscale y; plot '$*.connectivity_nonlog.binner' w p pt 6, '' w l, '' w histep" \
	| gnuplot > $@

#################################### COMUNITA' CON COMMTY (PROVERO) #############################################

%.conn_comp.commty2: %.conn_comp.stats %.conn_comp %.score
	for i in `cut -f 1 $< | tr -d '>'`; do \
		rm -f $@.tmp $@.tmp2; \
		$(BIN_DIR)/extract_conn_comp -n $$i -c $(word 2,$^) -tmp $@.tmp -line \
		$(word 3,$^) > $@.tmp2; \
		echo ">$$i" >> $@; \
		cut -f -2 $@.tmp2 | commty2 >> $@; \
	done
	rm -f $@.tmp $@.tmp2

%.parsed4.$(SCORE_CUTOFF).conn_comp.commty2.conn_comp: %.parsed4.$(SCORE_CUTOFF).conn_comp.commty2
	$(BIN_DIR)/commty2conn_comp -n 2 < $< > $@

%.conn_comp.commty2.conn_comp: %.conn_comp.commty2
	$(BIN_DIR)/commty2conn_comp < $< > $@

%.conn_comp.commty2.conn_comp.stats: %.conn_comp.commty2.conn_comp %.score_connectivity
	$(BIN_DIR)/group_analysis $^ > $@

%.conn_comp.commty2.conn_comp.clique_mis: %.conn_comp.commty2.conn_comp %.score
	$(BIN_DIR)/conn_comp_clique_mis $< < $(word 2,$^) > $@

%.conn_comp.commty2.conn_comp.strand: %.score %.conn_comp.commty2.conn_comp
	$(BIN_DIR)/group_net -c $(word 2,$^) $< > $@

#################################################################################################################

########################################### RICERCA DELLE CLIQUES ###############################################

%.conn_comp.cliques: %.conn_comp.score
	repeat_fasta_pipe -n '\
		echo ">$$HEADER"; \
		cut -f -2 \
		| $(BIN_DIR)/ricerca_cliques -tmp $@.tmp \
	' < $< > $@

%.conn_comp.cliques.conn_comp: %.conn_comp.cliques
	$(BIN_DIR)/cliques2conncomp < $< > $@

%.conn_comp.cliques.conn_comp.stats: %.conn_comp.cliques.conn_comp %.score_connectivity
	$(BIN_DIR)/group_analysis $^ > $@

%.conn_comp.cliques.conn_comp.strand: %.score %.conn_comp.cliques.conn_comp
	$(BIN_DIR)/group_net -c $(word 2,$^) $< > $@

#################################################################################################################

%.conn_comp: %.score
	cut -f -2 $< | conn_comp | perl -lne '$$,="\t"; $$\="\n"; $$n = $$. - 1; print ">$$n",$$_' > $@


#	$(BIN_DIR)/conn_comp_analisys $^ > $@
%.conn_comp.stats: %.conn_comp %.score_connectivity
	$(BIN_DIR)/group_analysis -ccomp $^ > $@
	# 1.  conn_component_id (row number - 1)
	# 2.  number of nodes in conn_comp
	# 3.  score_mean
	# 4.  score_median
	# 5.  score_stdev
	# 6.  score_min
	# 7.  score_max
	# 8.  conn_mean
	# 9.  conn_median
	# 10. conn_stdev
	# 11. conn_min
	# 12. conn_max

%.conn_comp.clique_mis: %.conn_comp %.score
	$(BIN_DIR)/conn_comp_clique_mis -ccomp $< < $(word 2,$^) > $@
	# 1.  conn_component_id (row number - 1)
	# 2.  number of nodes in conn_comp
	# 3.  number of edges N
	# 4.  number of edges if clique N_CLIQUE
	# 5.  ratio N / N_CLIQUE

%.conn_comp.stats.size.distrib: %.conn_comp.stats
	cut -f 2 $< | binner -l > $@

%.conn_comp.clique_mis.distrib: %.conn_comp.clique_mis
	cut -f 5 $< | binner > $@

%.conn_comp.clique_mis.$(NODES_NUMBER_CUTOFF).distrib: %.conn_comp.clique_mis
	awk "\$$2 >= $(NODES_NUMBER_CUTOFF) {print \$$5}" $< | binner > $@

%.conn_comp.score: %.conn_comp %.score
	repeat_fasta_pipe -n '\
		echo ">$$HEADER"; \
		$(BIN_DIR)/extract_conn_comp -n $$HEADER -c $< -tmp $@.tmp -line \
		$(word 2,$^) \
	' < $< > $@

%.conn_comp.strand: %.score %.conn_comp
	$(BIN_DIR)/group_net -c $(word 2,$^) $< > $@

%.conn_comp.multialign: %.conn_comp.strand %.conn_comp.stats
	rm -f $@.dnd $@.out $@.err
	for i in `cut -f 1 $(word 2,$^) | tr -d '>'`; do \
		rm -f $@.tmp; \
		echo ">$$i" >> $@; \
		get_fasta -n $$i $< \
			| perl -lane '$$,="\t"; $$\="\n"; ($$chr,$$b,$$e)=($$F[0]=~/^\d+:(\w+),(\d+),(\d+)$$/); print shift @F,$$chr,$$b,$$e,@F' \
			| seqlist2fasta -r $(SEQ_DIR) \
			| reverse_fasta > $@.tmp; \
		clustalw -INFILE=$@.tmp -TYPE=DNA -OUTPUT=GDE -CASE=UPPER -OUTORDER=ALIGNED -OUTFILE=$@.out >> $@.err;\
		cat $@.out >> $@; \
		rm -f $@.out; \
	done;
	rm -f $@.tmp $@.dnd

%.consensus_matrix: %.multialign
	repeat_fasta_pipe -n '\
		echo ">$$HEADER"; \
		$(BIN_DIR)/consensus -m -s \
	' < $< > $@

%.consensus_matrix.$(CONSENSUS_CUTOFF): %.consensus_matrix
	repeat_fasta_pipe -n '\
		echo ">$$HEADER"; \
		transpose | $(BIN_DIR)/colora_consensus -c $(CONSENSUS_CUTOFF) -t; \
	' < $< > $@

%.consensus_matrix.patterns: %.consensus_matrix
	repeat_fasta_pipe -n '\
		echo ">$$HEADER"; \
		transpose | $(BIN_DIR)/extract_patterns \
		-c $(CONSENSUS_CUTOFF) -drop $(CONSENSUS_DROPOFF) -n $(CONSENSUS_MATCH_VALUE) -m $(CONSENSUS_MISMATCH_VALUE) \
		-g $(CONSENSUS_GAP_VALUE) -l $(CONSENSUS_MIN_LEN) -s $(CONSENSUS_MIN_SCORE) -t ; \
	' < $< > $@

%.conn_comp.test_bonta: %.conn_comp.consensus_matrix %.conn_comp.consensus_matrix.patterns
	(repeat_fasta_pipe -n '\
		multi_len=`get_fasta -n $$HEADER $< | head -n 1 | awk "{print (NF - 1)}"`; \
		perl -lne "\$$,=\"\t\"; \$$\=\"\n\"; s/^>//; s/^#//; \$$ratio=(length \$$_) / $${multi_len}; {print '\''$$HEADER'\'',\$$ratio}"; \
	' < $(word 2,$^) ) \
	| find_best 1 2 > $@

%.conn_comp.test_bonta.$(PATTERNS_RATIO_CUTOFF): %.conn_comp.consensus_matrix %.conn_comp.consensus_matrix.patterns
	(repeat_fasta_pipe -n '\
		multi_len=`get_fasta -n $$HEADER $< | head -n 1 | awk "{print (NF - 1)}"`; \
		perl -lne "\$$,=\"\t\"; \$$\=\"\n\"; s/^>//; s/^#//; \$$ratio=(length \$$_) / $${multi_len}; if (\$$ratio >= $(PATTERNS_RATIO_CUTOFF)) {print '\''$$HEADER'\'',\$$ratio}"; \
	' < $(word 2,$^) ) > $@



regions.%.fa: regions.%.list $(REGIONS_LIST)
	cat $< | seqlist2fasta $(SEQ_DIR) \
	> $@

#regions.%$(SCORE_CUTOFF).list: $(REGIONS_LIST)
#	suffix='$*'; \
#	suffix=$${suffix%%0.*}; \
#	(cat *_$$suffix \
#		| awk 'BEGIN {id=0} {id++; $$1=$$1 "," $$2 "," $$3 "\t" $$1; print id ":" $$1 "\t" $$2 "\t" $$3}') \
#	| sort -k2,2 -k3,3n > $@

#regions.%.list: $(REGIONS_LIST)
#	(cat *_$* \
#		| awk 'BEGIN {id=0} {id++; $$1=$$1 "," $$2 "," $$3 "\t" $$1; print id ":" $$1 "\t" $$2 "\t" $$3}') \
#	| sort -k2,2 -k3,3n > $@

regions.%.fa.xnd: regions.%.fa $(REGIONS_LIST)
	xdformat -n $<; 



###############################################
#
#	statistica
#

%.len_distrib: % 
	cat $< | awk '{print $$3 - $$2}' | binner -n 25 -l -b m > $@

%.len_distrib.gnuplot: %.len_distrib
	echo "set logscale y; set logscale x; plot '$<' w l, '' w p pt 6 , '' w histep" > $@.tmp
	gnuplot $@.tmp -
	rm $@.tmp

%.len_distrib.ps: %.len_distrib
	echo "set terminal postscript enhanced color \"Helvetica\" 10;" > $@.tmp
	echo "set logscale y; set logscale x; plot '$<' w l, '' w p pt 6 , '' w histep" >> $@.tmp
	gnuplot $@.tmp > $@
	rm $@.tmp

%.overlap: %
	sort -k1,1 -k2,2n -k3,3n -S15% $* > $@.tmp
	cp $@.tmp $@.tmp2
	intersection $@.tmp $@.tmp2 | intersection_add_sim_diff |  awk '$$11!=$$12 && $$10<0.5' > $@


########## REGOLE PER RETE CON CYTOSCAPE #############
%.sif: %.score
	cat $< \
		| awk '{FS="\t"} {print $$1 "\ta\t" $$2}' > $@

%.eda: %.score
	#InteractionStrength
	#YAL001C (pp) YBR043W = 0.82
	#YMR022W (pd) YDL112C = 0.441
	#YDL112C (pd) YMR022W = 0.9013
	echo "Score" > $@;
	cat $< \
		| awk '{FS="\t"} {print $$1 " (a) " $$2 " = " $$3}' >> $@




