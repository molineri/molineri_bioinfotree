DATABASE?=symbols_070730
MYSQL?=mysql -BCAN -u symbols -p'pippo123' -h to441xl.to.infn.it $(DATABASE) 
#MYSQL?= cat >/dev/null
PREFIX?=regions.wublast.0.99.conn_comp.commty2.conn_comp.consensus_matrix.out.patterns.genome.wublast.0.8.parsed3

db_all: db_annot db_chr_anomalies db_pfam db_genes db_occurrencies

db_annot: $(PREFIX).annote.bool.overrepr.Bonferroni
	perl -lane '$$,="\t",$$\="\n"; my $$id=shift @F; @F=map{$$_ = $$_>0.05 ? 1 : $$_ } @F; print $$id,@F' $< > $@
	$(call load_table)

db_chr_anomalies: $(PREFIX).chr_distrib.overrepr
	cut -f 1,2,6,7 $< >$@
	$(call load_table)

db_go: $(PREFIX).annote_gene.family.go
	perl -F"\t" -lane '$$,="\t"; $$\="\n"; splice @F,4,1; splice @F,5,1; my $$M=splice @F,5,1; my $$genes=pop @F; $$F[3]=~s/molecular_function/mol_func/; $$F[3]=~s/biological_process/biol_proc/; $$F[3]=~s/cellular_component/cell_loc/; print @F, $$M, $$genes' < $< > $@
	$(call load_table)
	# contenuto colonne:
	# +-------------+----------------------------------------+------+-----+---------+-------+
	# | Field       | Type                                   | Null | Key | Default | Extra |
	# +-------------+----------------------------------------+------+-----+---------+-------+
	# | id          | int(11)                                | NO   |     |         |       |
	# | key         | varchar(20)                            | NO   |     |         |       |
	# | description | text                                   | NO   |     |         |       |
	# | ontology    | enum('mol_func','biol_proc','cell_loc')| NO   |     |         |       |
	# | p_value     | double                                 | NO   |     |         |       |
	# | n           | int(11)                                | NO   |     |         |       |
	# | x           | int(11)                                | NO   |     |         |       |
	# | M           | int(11)                                | NO   |     |         |       |
	# | genes       | text                                   | NO   |     |         |       |
	# +-------------+----------------------------------------+------+-----+---------+-------+

db_pfam: $(PREFIX).annote_gene.family.pfam
	perl -F"\t" -lane '$$,="\t"; $$\="\n"; splice @F,4,1; my $$M=splice @F,4,1; my $$genes=pop @F; print @F, $$M, $$genes' < $< > $@
	$(call load_table)
	# contenuto colonne:
	# +-------------+-------------+------+-----+---------+-------+
	# | Field       | Type        | Null | Key | Default | Extra |
	# +-------------+-------------+------+-----+---------+-------+
	# | id          | int(11)     | NO   | PRI |         |       |
	# | key         | varchar(20) | NO   |     |         |       |
	# | description | text        | NO   |     |         |       |
	# | p_value     | double      | NO   |     |         |       |
	# | n           | int(11)     | NO   |     |         |       |
	# | x           | int(11)     | NO   |     |         |       |
	# | M           | int(11)     | NO   |     |         |       |
	# | genes       | text        | NO   |     |         |       |
	# +-------------+-------------+------+-----+---------+-------+

db_genes: $(PREFIX).annote_gene
	grep -v '>' $< | cut -f 1,6,7,8 > $@
	$(call load_table)
	# +----------+-------------------+------+-----+---------+-------+
	# | Field    | Type              | Null | Key | Default | Extra |
	# +----------+-------------------+------+-----+---------+-------+
	# | id       | int(11)           | NO   |     |         |       |
	# | gene     | varchar(30)       | NO   |     |         |       |
	# | position | enum('i','b','a') | NO   |     |         |       |
	# | distance | int(11)           |      |     |         |       |
	# +----------+-------------------+------+-----+---------+-------+

db_occurrencies: $(PREFIX)
	awk 'BEGIN{OFS="\t"}{print $$4,$$1,$$2,$$3}' $< > $@
	$(call load_table)


define load_table
	echo "TRUNCATE $(subst db_,,$@)" | $(MYSQL)
	echo "LOAD DATA LOCAL INFILE '$@' INTO TABLE $(subst db_,,$@)" | $(MYSQL)
endef
