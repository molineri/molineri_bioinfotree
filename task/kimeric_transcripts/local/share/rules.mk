DATASET ?= ensembl
SPECIES ?= hsapiens
VERSION ?= 45
SEQ_DIR ?= $(BIOINFO_ROOT)/task/sequences/dataset/$(DATASET)/$(SPECIES)/$(VERSION)
ANNOTATION_DIR ?= $(BIOINFO_ROOT)/task/annotations/dataset/$(DATASET)/$(SPECIES)/$(VERSION)
ALL_CHR ?= 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X Y


chr%.to_mask: $(ANNOTATION_DIR)/coords_coding_chr.%
	sort -k2,2n -k3,3n $< | union | cut -f 2,3 > $@

chr%.coding_masked: chr%.to_mask $(SEQ_DIR)/chr%.fa
	mask_fa $< < $(word 2,$^) > $@
