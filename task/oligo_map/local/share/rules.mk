# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>
#
include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

OLIGO_SEQUENCES ?= oligos.fa
CDNA_SEQUENCES  ?= $(BIOINFO_ROOT)/task/sequences/dataset/ensembl/mmusculus/50/cdna.fa

all: exact_matches oligo-transcript.map
clean.full:
	rm -f exact_matches oligo-transcript.map

exact_matches: $(OLIGO_SEQUENCES) $(CDNA_SEQUENCES)
	fasta_exact_match -p $(word 2,$^) <$< \
	| sort -k1,1 -k2,2 -k3,3n -S40% >$@

.META: exact_matches
	1  oligo_ID        M000002_01
	2  transcript_ID   ENSMUST00000000962
	3  start_position  1943

oligo-transcript.map: exact_matches
	cut -f1,2 $< \
	| sort -S40% >$@

.META: oligo-transcript.map
	1  oligo_ID       M000002_01
	2  transcript_ID  ENSMUST00000000962
