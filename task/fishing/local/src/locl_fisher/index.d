module index;
import reader;
import tango.core.Exception;

class RegionBruteForceIndex
{
	Region[] regions;
	
	this(Region[] regions)
	{
		this.regions = regions;
	}
	
	Region[] get_overlapping(uint start, uint stop)
	{
		Region[] res;
		foreach (r; regions)
		{
			if (!(r.stop <= start || r.start >= stop))
				res ~= r;
		}
		return res;
	}
}

struct HeapEntry
{
	uint start;
	uint stop;
	uint child_start_idx;
}

class RegionHeapIndex
{
	const Region[] regions;
	const uint granularity;
	HeapEntry[] heap;
	
	this(Region[] regions, uint granularity)
	{
		if (granularity < 2)
			throw new IllegalArgumentException("invalid granularity: must be >= 2");
		
		this.regions = regions;
		this.granularity = granularity;
		build_heap();
	}
	
	Region[] get_overlapping(uint start, uint stop)
	{
		uint[] matches = new uint[2];
		matches[0] = 0;
		matches[1] = 1;
		
		while (matches.length > 0 && matches[0] < heap.length / 2)
		{
			uint[] new_matches;
			foreach (m; matches)
			{
				uint i = heap[m].child_start_idx;
				if (overlapping_heap_entry(heap[i], start, stop))
					new_matches ~= i;
				
				i++;
				if (overlapping_heap_entry(heap[i], start, stop))
					new_matches ~= i;
			}
			
			matches = new_matches;
		}
		
		Region[] res = new Region[matches.length*granularity];
		uint used = 0;
		foreach (m; matches)
		{
			for (uint j = 0; j < granularity; j++)
			{
				Region r = regions[heap[m].child_start_idx+j];
				if (overlapping_region(r, start, stop))
					res[used++] = r;
			}
		}
		
		res = res[0..used];
		return res;
	}
	
	private void build_heap()
	{
		uint child_num = regions.length / granularity;
		heap = new HeapEntry[child_num * 2 - 2];
		
		uint i = heap.length - child_num - 1;
		foreach (j, region; regions)
		{
			if (j % granularity == 0)
			{
				i++;
				heap[i].start = region.start;
				heap[i].child_start_idx = j;
			}
			
			heap[i].stop = region.stop;
		}
		while (i < heap.length)
		{
			heap[i].start = uint.max;
			heap[i].stop = uint.max;
			heap[i].child_start_idx = 0;
		}
		
		uint child_base_idx = heap.length - child_num;
		uint level_size = child_num / 2;
		while (level_size > 1)
		{
			uint j;
			for (i = child_base_idx - level_size, j = child_base_idx; i < child_base_idx; i++, j+= 2)
			{
				heap[i].start = heap[j].start;
				heap[i].stop = heap[j+1].stop;
				heap[i].child_start_idx = j;
			}
			
			child_base_idx -= level_size;
			level_size /= 2;
		}
	}
	
	private bool overlapping_heap_entry(HeapEntry e, uint start, uint stop)
	{
		return !(e.start >= stop || e.stop <= start);
	}
	
	private bool overlapping_region(Region r, uint start, uint stop)
	{
		return !(r.stop <= start || r.start >= stop);
	}
}
