module reader;
import tango.core.Exception;
import tango.io.Conduit;
import tango.text.convert.Integer;
import tango.text.convert.Sprint;
import tango.text.stream.LineIterator;
import tango.text.String;
import tango.text.Util;

struct Region
{
	uint start;
	uint stop;
	char[] label;
}

struct Locl
{
	uint query_start;
	uint query_stop;
	uint target_start;
	uint target_stop;
}

struct HSP
{
	uint query_start;
	uint query_stop;
	uint target_start;
	uint target_stop;
	char[] raw;
}

Region[] load_regions(Conduit src, char[] sequence_name)
{
	const uint step = 4096;
	Region[] res;
	res.length = step;
	
	uint lineno = 0;
	uint used = 0;
	foreach (line; new LineIterator!(char)(src))
	{
		lineno++;
		if (used == res.length)
			res.length = res.length + step;
		
		if (parse_region(line, lineno, sequence_name, res[used]))
		{
			if (used > 0 && res[used-1].start > res[used].start)
				throw new IOException((new Sprint!(char)).format("disorder found at line {}", lineno));
			used++;
		}
	}
	
	return res[0 .. used];
}

void parse_locl_header(char[] line, inout Locl locl)
{
	char[][] tokens = split(line[1..line.length], "_");
	if (tokens.length != 4)
		throw new IOException((new Sprint!(char)).format("malformed locl header"));
	
	try
	{
		locl.query_start = toUint(tokens[0]);
	}
	catch (IllegalArgumentException e)
	{
		throw new IOException((new Sprint!(char)).format("malformed locl query start coordinate"));
	}

	try
	{
		locl.query_stop = toUint(tokens[1]);
		if (locl.query_start >= locl.query_stop)
			throw new IllegalArgumentException(null);
	}
	catch (IllegalArgumentException e)
	{
		throw new IOException((new Sprint!(char)).format("malformed locl query stop coordinate"));
	}

	try
	{
		locl.target_start = toUint(tokens[2]);
	}
	catch (IllegalArgumentException e)
	{
		throw new IOException((new Sprint!(char)).format("malformed locl target start coordinate"));
	}

	try
	{
		locl.target_stop = toUint(tokens[3]);
		if (locl.target_start >= locl.target_stop)
			throw new IllegalArgumentException(null);
	}
	catch (IllegalArgumentException e)
	{
		throw new IOException((new Sprint!(char)).format("malformed locl target stop coordinate"));
	}
}

void parse_hsp(char[] line, inout HSP hsp)
{
	char[][] tokens = split(line, "\t");
	/*if (tokens.length != 13)
		throw new IOException((new Sprint!(char)).format("malformed HSP spec"));*/
	
	try
	{
		hsp.query_start = toUint(tokens[1]);
	}
	catch (IllegalArgumentException e)
	{
		throw new IOException((new Sprint!(char)).format("malformed query start coordinate"));
	}
	
	try
	{
		hsp.query_stop = toUint(tokens[2]);
		if (hsp.query_start >= hsp.query_stop)
			throw new IllegalArgumentException(null);
	}
	catch (IllegalArgumentException e)
	{
		throw new IOException((new Sprint!(char)).format("malformed query stop coordinate"));
	}
	
	try
	{
		hsp.target_start = toUint(tokens[5]);
	}
	catch (IllegalArgumentException e)
	{
		throw new IOException((new Sprint!(char)).format("malformed target start coordinate"));
	}
	
	try
	{
		hsp.target_stop = toUint(tokens[6]);
		if (hsp.target_start >= hsp.target_stop)
			throw new IllegalArgumentException(null);
	}
	catch (IllegalArgumentException e)
	{
		throw new IOException((new Sprint!(char)).format("malformed target stop coordinate"));
	}
	
	hsp.raw = line.dup;
}

private bool parse_region(char[] line, uint lineno, char[] sequence_name, inout Region r)
{
	char[][] tokens = split(line, "\t");
	if (tokens.length != 4)
		throw new IOException((new Sprint!(char)).format("malformed region spec at line {}", lineno));
	
	if (tokens[0] != sequence_name)
		return false;
	
	try
	{
		r.start = toUint(tokens[1]);
	}
	catch (IllegalArgumentException e)
	{
		throw new IOException((new Sprint!(char)).format("malformed start coordinate at line {}", lineno));
	}
	
	try
	{
		r.stop = toUint(tokens[2]);
	}
	catch (IllegalArgumentException e)
	{
		throw new IOException((new Sprint!(char)).format("malformed stop coordinate at line {}", lineno));
	}
	
	if (r.start >= r.stop)
		throw new IOException((new Sprint!(char)).format("invalid stop coordinate at line {}", lineno));
	
	r.label = tokens[3].dup;
	return true;
}

private uint toUint(char[] str)
{
	long value = toLong(str);
	if (value < 0 || value > uint.max)
		throw new IllegalArgumentException(null);
	else
		return cast(uint)value;
}
