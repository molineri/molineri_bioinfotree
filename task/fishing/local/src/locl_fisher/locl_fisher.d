import index;
import reader;
import tango.core.Exception;
import tango.io.Console;
import tango.io.FileConduit;
import tango.io.Stdout;
import tango.stdc.stdlib;
import tango.text.stream.LineIterator;

private void parse_args(char[][] args)
{
	foreach (arg; args)
	{
		if (arg == "-h" || arg == "--help")
		{
			Cerr("Usage: ")(args[0])(" SEQ1 BAITS1 SEQ2 BAITS2 <LOCL").newline;
			exit(1);
		}
	}
	
	if (args.length != 5)
	{
		Cerr("Unexpected argument number.").newline;
		exit(1);
	}
}


/*struct IndexEntry
{
	uint pos;
	Region* region;
}

private extern(C) int compare_regions(void* a, void* b)
{
	Region* c = (cast(IndexEntry*)a).region;
	Region* d = (cast(IndexEntry*)b).region;
	if (c.stop == d.stop)
		return (c.start > d.start) - (c.start < d.start);
	else
		return (c.stop > d.stop) - (c.stop < d.stop);
}

private class RegionIndex
{
	Region[] regions;
	IndexEntry[] stop_index;
	
	this(Region[] regions)
	{
		this.regions = regions;
		build_stop_index();
	}
	
	Region[] get_overlapping(uint start, uint stop)
	{
		uint l = index_lbound(start);
		uint m = 0, n = 0xffffffff, o = 0;
		for (; m < regions.length; m++)
		{
			if (regions[m].stop > start && regions[m].stop < n)
			{
				o = m;
				n = regions[m].stop;
			}
		}
		
		if (l != o)
		{
			Stderr.format("found: {}; correct: {}", l, o).newline;
			Stderr.format("found stop: {}; correct stop: {}", regions[l].stop, regions[o].stop).newline;
		}
		
		Region[] res;
		foreach (region; regions[l..regions.length])
		{
			if (region.start >= stop)
				break;
			else if (!(region.stop <= start || region.start >= stop))
				res ~= region;
		}
		
		return res;
	}
	
	private void build_stop_index()
	{
		stop_index = new IndexEntry[regions.length];
		for (uint i = 0; i < stop_index.length; i++)
		{
			stop_index[i].pos = i;
			stop_index[i].region = &regions[i];
		}
		
		qsort(stop_index.ptr, stop_index.length, IndexEntry.sizeof, &compare_regions);
	}
	
	private uint index_lbound(uint start)
	{
		uint searched = 0;
		uint pos = stop_index.length / 2;
		uint step = pos / 2;
		while (step >= 1)
		{
			if (stop_index[pos].region.stop > start)
				pos -= step;
			else
				pos += step;
			step /= 2;
			//searched++;
		}
		
		while (pos > 0 && stop_index[pos-1].region.stop > start)
		{
			pos--;
			//searched++;
		}
		
		//Stderr.format("{} searched", searched).newline;
		return stop_index[pos].pos;
	}
}*/

void print_overlapping(Region[] baits1, Region[] baits2, HSP hsp)
{
	foreach (bait1; baits1)
	{
		foreach (bait2; baits2)
		{
			if ((hsp.query_stop > bait1.start || hsp.query_start < bait1.stop) && (hsp.target_stop > bait2.start || hsp.target_start < bait2.stop))
				Stdout.format("{}\t{}\t{}", bait1.label, bait2.label, hsp.raw).newline;
		}
	}
}

int main(char[][] args)
{
	parse_args(args);
	
	auto baits1_index = new RegionHeapIndex(load_regions(new FileConduit(args[2]), args[1]), 16);
	auto baits2_index = new RegionHeapIndex(load_regions(new FileConduit(args[4]), args[3]), 16);
	
	Locl locl;
	HSP hsp;
	uint lineno = 0;
	Region[] baits1 = null;
	Region[] baits2 = null;
	//foreach (line; new LineIterator!(char)(Cin.conduit))
	foreach (line; new LineIterator!(char)(new FileConduit("chr5_chr12.loc_cl")))
	{
		lineno++;
		if (line[0] == '>')
		{
			baits1 = null;
			baits2 = null;
			
			try
			{
				parse_locl_header(line, locl);
			}
			catch (IOException e)
			{
				Stderr.format("Error: {} at line {}.", e.msg, lineno).newline;
				return 1;
			}
			
			baits1 = baits1_index.get_overlapping(locl.query_start, locl.query_stop);
			if (baits1.length == 0)
			{
				baits1 = null;
				continue;
			}
			
			baits2 = baits2_index.get_overlapping(locl.target_start, locl.target_stop);
			if (baits2.length == 0)
			{
				baits1 = null;
				baits2 = null;
				continue;
			}
		}
		else if (baits1 !is null)
		{
			try
			{
				parse_hsp(line, hsp);
			}
			catch (IOException e)
			{
				Stderr.format("Error: {} at line {}.", e.msg, lineno).newline;
				return 1;
			}
			
			print_overlapping(baits1, baits2, hsp);
		}
	}
	
	return 0;
}
