#!/usr/bin/env python

from optparse import OptionParser
from sys import exit, stdin
from vfork.io.colreader import Reader

from sys import stderr

def main():
	parser = OptionParser(usage='%prog <FISHED')
	options, args = parser.parse_args()

	if len(args) > 0:
		exit('Unexpected argument number.')
	
	reader = Reader(stdin, '0u,1u,3u,4u,a', False)
	while True:
		tokens = reader.readline()
		if tokens is None:
			break
		
		if not (tokens[1] <= tokens[2] or tokens[0] >= tokens[3]):
			print tokens[4]

if __name__ == '__main__':
	main()

