#!/usr/bin/env python
from optparse import OptionParser
from os import chdir, mkdir, symlink, system
from os.path import abspath, exists, join
from random import randint
from shutil import rmtree
from sys import exit

def translate_database(from_file, to_file):
	in_fd = file(from_file, 'r')
	out_fd = file(to_file, 'w')

	# header
	out_fd.write(in_fd.readline())

	# build translation table
	trtbl = [ chr(i) for i in xrange(256) ]
	for c in 'acgtn':
		trtbl[ord(c)] = 'N'
	trtbl = ''.join(trtbl)

	# copy all lines
	for line in in_fd:
		out_fd.write(line.translate(trtbl))
	
	in_fd.close()
	out_fd.close()

if __name__ == '__main__':
	parser = OptionParser(usage='%prog QUERY_FASTA DATABASE_FASTA')
	parser.add_option('-s', '--seed', type='int', dest='seed', default=11, help='seed SIZE', metavar='SIZE')
	parser.add_option('-i', '--min-score', type='int', dest='min_score', default=32, help='discard alignments with a score lower than SCORE; defaults to 32', metavar='SCORE') 
	parser.add_option('-o', '--output', dest='output', default='blast.res', help='the FILENAME for the list of computed alignments', metavar='FILENAME')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	if exists('work'):
		exit("Can't create directory 'work' because something else with that name already exists.")
	mkdir('work')
	
	# link the query
	symlink(abspath(args[0]), join('work', 'query.fa'))
	
	# select and link a random database
	translate_database(abspath(args[1]), join('work', 'database.fa'))
	
	# move into the work directory
	chdir('work')
	
	# format both the query and the database
	for filename in ('database.fa', 'query.fa'):
		res = system('formatdb -p F -o T -v 500000000 -i %s' % filename)
		if res != 0:
			raise RuntimeError, 'error running formatdb on %s' % filename
	
	# run blast
	blastall_opts = '-p blastn -e 9e-05 -W %d -F F -S 3 -m 8 -b 10000000 -v 10000000 -z 1' % options.seed
	print 'blastall %s -i query.fa -d database.fa | awk \'$12>%d\' > %s' % (blastall_opts, options.min_score, join('..', options.output))
	res = system('blastall %s -i query.fa -d database.fa | awk \'$12>%d\' > %s' % (blastall_opts, options.min_score, join('..', options.output)))
	if res != 0:
		raise RuntimeError, 'error running blastall'

	# clean up the mess
	chdir('..')
	rmtree('work')
