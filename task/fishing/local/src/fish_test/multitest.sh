set -e
rm -f count_multitest;
for i in `seq 1 100`; do 
	sh homo_homo.sh >/dev/null;
	mv blast.res blast.res.$i;
	mv fishing.res fishing.res.$i;
	mv baits.txt baits.txt.$i;
	wc -l *.res.$i >> count_multitest;
	echo '------' >> count_multitest;
	wc -l *.res.$i;
done;
 
