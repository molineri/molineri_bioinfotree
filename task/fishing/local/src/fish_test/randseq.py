#!/usr/bin/env python
from optparse import OptionParser
from os import linesep
from random import randint
from sys import exit
from vfork.fasta import SingleBlockReader
import re

''' Reads a sequence of a random length from the input FASTA files (each
    one holding a single chromosome), chosen at random.
'''

class SingleBlockFastaWriter(object):
	def __init__(self, filename, header, width):
		self.fd = file(filename, 'w')
		self.write_header(header)
		self.width = width
		self.pending = ''
	
	def close(self):
		print >>self.fd, self.pending
		self.pending = ''
		self.fd.close()
	
	def write(self, seq):
		self.pending += seq
		
		content_width = self.width - len(linesep)
		while len(self.pending) >= content_width:
			line = self.pending[:content_width]
			self.pending = self.pending[content_width:]
			print >>self.fd, line
	
	##
	## Internal use only
	##
	def write_header(self, header):
		if header[0] != '>':
			header = '>' + header
		print >>self.fd, header

if __name__ == '__main__':
	parser = OptionParser(usage='%prog [OPTIONS] CHROMOSOME_FASTA_FILE...')
	parser.add_option('-i', '--min-length', dest='min_length', type='int', default=50, help='lower bound to sequence SIZE', metavar='SIZE')
	parser.add_option('-a', '--max-length', dest='max_length', type='int', default=100, help='upper bound to sequence SIZE', metavar='SIZE')
	parser.add_option('-o', '--output', dest='output', default='sequence.fa', help='output FILENAME', metavar='FILENAME')
	parser.add_option('-w', '--width', dest='width', type='int', default=80, help='maximum line WIDTH for the output file', metavar='WIDTH')
	options, args = parser.parse_args()
	
	if len(args) == 0:
		exit('Unexpected argument number.')
	
	arg_idx = randint(0, len(args) - 1)
	reader = SingleBlockReader(args[arg_idx])
	if reader.content_size < options.max_length:
		exit('Input file %s is smaller than max_length.' % args[arg_idx])

	trtbl = [ chr(i) for i in xrange(256) ]
	for c in 'acgtn':
		trtbl[ord(c)] = 'N'
	trtbl = ''.join(trtbl)
	
	only_masked = True
	while only_masked:
		seq_size = randint(options.min_length, options.max_length)
		seq_pos = randint(0, reader.content_size - seq_size)		
		seq = ''.join(reader.get(seq_pos, seq_size)).translate(trtbl)
		
		for c in seq:
			if c != 'N':
				only_masked = False
				break
	
	reader.close()
		
	header = '%s:%d:%d' % (reader.header, seq_pos, seq_pos + seq_size)
	writer = SingleBlockFastaWriter(options.output, header, options.width)
	writer.write(seq)
	writer.close()
