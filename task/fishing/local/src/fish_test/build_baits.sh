#!/bin/bash

query=$(head -n1 $1 | tr -d ">" | awk -F: '{print $1 "\t" $2 "\t" $3}')
target_chr=$(basename ${2%.fa})
target_size=$(wc -c $2 | cut -d' ' -f1)
target="$target_chr	0	$target_size"

echo -e "ID1\t$query\tID2\t$target"
