#!/bin/bash

if [ "$1" == "" ]; then
	echo "Query sequences directory missing."
	exit 1
fi
query_sequences_dir=$1

if [ "$2" == "" ]; then
	echo "Database sequences directory missing."
	exit 1
fi
database_sequences_dir=$2

if [ "$3" == "" ]; then
	echo "Alignments directory missing."
	exit 1
fi
alignments_dir=$3

if [ "$4" != "" ]; then
	echo "Excess arguments detected."
	exit 1
fi

set -e

echo "Picking sequences..."
find $1 -name '*.fa' | xargs -n500 -x ./randseq.py -o query.fa -i 5000 -a 6000
database=$(find $2 -name '*.fa' | xargs -n500 -x ./pick_random.py)

echo "Blasting..."
./single_blast.py -o blast.res -i 40 query.fa $database

echo "Fishing..."
./build_baits.sh query.fa $database > baits.txt
# rm -f query.fa

chr1=$(awk '{print $2}' baits.txt)
chr2=$(awk '{print $6}' baits.txt)
if [ $1 != $2 ]; then
	for align_arch in $(find $3 -name '*'$chr1'_*'$chr2 -type d); do
		zcat $align_arch/per_database.gz | awk '$10>40' | ../../bin/fish_align -c baits.txt >fishing.res
	done
else
	rm -f fishing.res
	for align_arch in $(find $3 -name '*'$chr1'_*'$chr2 -type d); do
		echo "zcat $align_arch/per_database.gz | awk '\$10>40' | ../../bin/fish_align -c -s baits.txt >>fishing.res"
		zcat $align_arch/per_database.gz | awk '$10>40' | ../../bin/fish_align -c -s baits.txt >>fishing.res
	done
	if [ $chr1 != $chr2 ]; then
		for align_arch in $(find $3 -name '*'$chr2'_*'$chr1 -type d); do
			echo "zcat $align_arch/per_database.gz | awk '\$10>40' | ../../bin/fish_align -c -s baits.txt >>fishing.res"
			zcat $align_arch/per_database.gz | awk '$10>40' | ../../bin/fish_align -c -s baits.txt >>fishing.res
		done	
	fi
fi
