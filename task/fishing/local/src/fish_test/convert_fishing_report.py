#!/usr/bin/env python
from optparse import OptionParser
from os import popen
from sys import exit

def need_swap(fd):
	tokens = fd.readline().rstrip().split()[10:]
	fd.seek(0)
	
	chr1 = int(tokens[0])
	chr2 = int(tokens[4])
	return chr1 > chr2

if __name__ == '__main__':
	parser = OptionParser(usage='%prog FISHING_REPORT')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	fd = file(args[0], 'r')
	swap = need_swap(fd)
	
	aux = []
	for line in fd:
		tokens = line.rstrip().split()[10:]
		if swap:
			hold = tokens[0:3]
			tokens[0:3] = tokens[4:7]
			tokens[4:7] = hold
		
		aux.append(tokens)
		
	fd.close()
	
	fd = popen('sort -k2,3n -k6,7n > %s' % args[0], 'w')
	for record in aux:
		print >>fd, '\t'.join(record)
	if fd.close() is not None:
		raise RuntimeError, 'error running sort'
