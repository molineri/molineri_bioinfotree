#!/usr/bin/env python
from random import randint
from sys import argv, exit

if __name__ == '__main__':
	if len(argv) < 2:
		exit('Unexpected argument number.')
	
	print argv[randint(1, len(argv) - 1)]
