#!/usr/bin/env python
# encoding: utf-8
from __future__ import division
from __future__ import with_statement

from math import ceil, log
from optparse import OptionParser
from sys import exit, stdin, stderr
from vfork.fasta.reader import MultipleBlockStreamingReader
from vfork.io.colreader import Reader

class RegionIndex(object):
	def __init__(self, filename, seq):
		self.regions = self._load_regions(filename, seq)
	
	def get_overlapping(self, start, stop):
		res = []
		for region in self.regions:
			if region[1] <= start:
				continue
			elif region[0] < stop:
				res.append(region)
		return res
	
	def _load_regions(self, filename, seq):
		try:
			res = []
			last_start = None
			
			with file(filename, 'r') as fd:
				r = Reader(fd, '0s,1u,2u,3s', False)
				while True:
					tokens = r.readline()
					if tokens is None:
						break
					elif tokens[0] == seq:
						if last_start is None:
							last_start = tokens[1]
						elif last_start > tokens[1]:
							exit('Disorder found at line %d of file %s' % (r.lineno, filename))
						else:
							last_start = tokens[1]
						
						res.append(tuple(tokens[1:]))
		
			if len(res) == 0:
				print >>stderr, '[WARNING] No regions found.'

			return res
	
		except IOError:
			exit('Error opening %s' % filename)
		except ValueError, e:
			exit('Error reading %s: %s' % (filename, str(e)))

class RegionHeapIndex(RegionIndex):
	def __init__(self, filename, seq, granularity):
		self.regions = self._load_regions(filename, seq)
		assert len(self.regions) > 0
		self.level_num, self.granularity, self.heap = self._build_heap(granularity)
		# self._check_heap()
	
	def get_overlapping(self, start, stop):
		horizon = []
		if self.heap[0] is not None and self._is_node_overlapping(self.heap[0], start, stop):
			horizon.append(self.heap[0])
		if self.heap[1] is not None and self._is_node_overlapping(self.heap[1], start, stop):
			horizon.append(self.heap[1])
		
		level = 2
		while len(horizon) and level < self.level_num:
			new_horizon = []
			for node in horizon:
				child = self.heap[node[2]]
				if child is not None and self._is_node_overlapping(child, start, stop):
					new_horizon.append(child)
				
				child = self.heap[node[2]+1]
				if child is not None and self._is_node_overlapping(child, start, stop):
					new_horizon.append(child)
			
			horizon = new_horizon
			level += 1
		
		res = []
		for node in horizon:
			j = node[2]
			j2 = j + self.granularity
			while j < len(self.regions) and j < j2:
				region = self.regions[j]
				if self._is_region_overlapping(region, start, stop):
					res.append(region)
				j += 1
		
		return res
	
	def _build_heap(self, granularity):
		region_num = len(self.regions)
		block_num, level_num, granularity = self._compute_levels(region_num, granularity)
		heap_size = max(2, 2**level_num)
		heap = [None] * heap_size
		
		# fill the last level with region indexes
		i = 2**(level_num-1) - 2
		j = 0
		while j < region_num:
			start = self.regions[j][0]
			stop = self.regions[j][1]
			idx = j
			
			j2 = min(region_num, j+granularity)
			j += 1
			while j < j2:
				if stop < self.regions[j][1]:
					stop = self.regions[j][1]
				j += 1
			
			heap[i] = (start, stop, idx)
			i += 1
		
		# fill other levels with span informations
		level = level_num - 2
		while level > 0:
			base_idx = 2**level - 2
			children_base_idx = 2**(level+1) - 2
			
			i = base_idx
			j = children_base_idx
			while i < children_base_idx:
				if heap[j] is None:
					break
				elif heap[j+1] is None:
					heap[i] = (heap[j][0], heap[j][1], j)
				else:
					heap[i] = (heap[j][0], max(heap[j][1], heap[j+1][1]), j)
				i += 1
				j += 2
			
			level -= 1
		
		return level_num, granularity, heap
	
	def _compute_levels(self, record_num, granularity):
		block_num = record_num // granularity
		if record_num % granularity > 0:
			block_num += 1
		
		level_num = int(ceil(log(block_num, 2))) + 1
		block_num = 2**(level_num-1)
		granularity = int(ceil(record_num / block_num))
		
		return block_num, level_num, granularity
	
	def _is_node_overlapping(self, node, start, stop):
		return not (node[0] >= stop or node[1] <= start)
	
	def _is_region_overlapping(self, region, start, stop):
		return not (region[0] >= stop or region[1] <= start)
	
	def _check_heap(self):
		heap_size = len(self.heap)
		
		horizon = self.heap[:2]
		level = 2
		while level < self.level_num:
			new_horizon = []
			
			for node in horizon:
				child1 = self.heap[node[2]]
				if child1 is not None:
					assert node[0] == child1[0]
					new_horizon.append(child1)
				
				child2 = self.heap[node[2]+1]
				if child1 is None:
					assert child2 is None
				elif child2 is not None:
					assert node[1] == max(child1[1], child2[1])
					new_horizon.append(child2)
			
			level += 1
			horizon = new_horizon
		
		for idx, node in enumerate(horizon):
			j = node[2]
			if idx > 0:
				assert j - horizon[idx-1][2] == self.granularity
			
			start = self.regions[j][0]
			stop = self.regions[j][1]
			
			j2 = min(len(self.regions), j+self.granularity)
			j += 1
			while j < j2:
				if stop < self.regions[j][1]:
					stop = self.regions[j][1]
				j += 1
			
			assert node[0] == start
			assert node[1] == stop

def parse_header(header):
	try:
		tokens = header.split('_')
		if len(tokens) != 4:
			raise ValueError
		return tuple(int(t) for t in tokens)
	except ValueError:
		exit('Invalid location cluster header found.')

def parse_hsp(hsp):
	try:
		tokens = hsp.split('\t')
		if len(tokens) < 7:
			raise ValueError
		return tuple(int(tokens[i]) for i in (1, 2, 5, 6))
	except ValueError:
		exit('Invalid HSP spec found.')

def main():
	parser = OptionParser(usage='%prog REGIONS1 SEQ1 REGIONS2 SEQ2 <LOCL')
	parser.add_option('-s', '--safe-mode', dest='safe_mode', action='store_true', help='enable a slightly safer, but slower, indexing scheme')
	options, args = parser.parse_args()
	
	if len(args) != 4:
		exit('Unexpected argument number.')
	
	if options.safe_mode:
		regions1 = RegionIndex(args[0], args[1])
		regions2 = RegionIndex(args[2], args[3])
	else:
		regions1 = RegionHeapIndex(args[0], args[1], 16)
		regions2 = RegionHeapIndex(args[2], args[3], 16)
	
	reader = MultipleBlockStreamingReader(stdin, join_lines=False)
	for header, content in reader:
		locl = parse_header(header)
		
		rs1 = regions1.get_overlapping(locl[0], locl[1])
		rs2 = regions2.get_overlapping(locl[2], locl[3])
		if len(rs1) == 0 or len(rs2) == 0:
			continue
		
		hsps = [ (parse_hsp(line), line) for line in content ]	
		hsps.sort()

		for hsp, line in hsps:
			for r1 in rs1:
				if r1[1] <= hsp[0]:
					continue
				elif r1[0] >= hsp[1]:
					break

				for r2 in rs2:
					if r2[1] <= hsp[2]:
						continue
					elif r2[0] >= hsp[3]:
						break
					else:
						print '\t'.join(str(d) for d in (r1 + r2 + (line,)))

if __name__ == "__main__":
	main()
