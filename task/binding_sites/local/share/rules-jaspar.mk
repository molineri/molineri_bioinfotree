include $(BIOINFO_ROOT)/task/sequences/local/share/species/ensembl/$(SPECIES).mk

URL_PREFIX      = http://jaspar.genereg.net/html/DOWNLOAD
URL_PREFIX_CORE = $(URL_PREFIX)/jaspar_CORE/non_redundant/by_tax_group/vertebrates
URL_DIR_MATRIX_CONVERSION = $(URL_PREFIX)/all_data/sql_tables

UNIPROT_DIR = $(BIOINFO_ROOT)/task/annotations/dataset/uniprot/090324

clean:
	rm -f $(addprefix core.pfms.occurrencies.chr,$(SPECIES_CHRS)) 
.PHONY: core
core.pfms.raw: core.pfms.annot
	wget -O - $(URL_PREFIX)/matrix_only/matrix_only.txt \
	| tr -d '[]' \
	| perl -pne 'BEGIN{$$\="\n";} s/\s+$$//; s/^\s+//; s/\s+/\t/g; if(!m/^>/){ s/[ACGT]\t// }' > $@

.DOC: core.pfms.raw
	fdas
	
all.pfms.raw:
	wget -O - $(URL_PREFIX)/all_data/matrix_only/matrix_only.txt \
	| tr -d '[]' \
	| perl -pne 'BEGIN{$$\="\n";} s/\s+$$//; s/^\s+//; s/\s+/\t/g; if(!m/^>/){ s/[ACGT]\t// }' > $@

matices.mk: all.pfms.raw core.count
	grep '^>' $< \
	| perl -lane '$$_=$$F[0]; s/^>//; $$m=$$_; s/\d+.*//; print "$$_\t$$m"' \      * isolo il prefisso*
	| translate -n $(PRJ_ROOT)/local/share/data/collections2id.map 1 | bsort | tab2fasta | fasta2oneline -t | sed 's/\t/=/' \
	| perl -lane 'print; s/(^[^=]+)=.*//; print "\necho.$$1:\n\t\@echo \$$($$1)\n";'> $@
	(\
		echo CORE_VERTEBRATE = `cut -f 1 $^2 | sort | uniq | tr "\n" " "`;\
		echo -e "echo.CORE_VERTEBRATE:\n\techo \$$(CORE_VERTEBRATE)";\
	) >> $@

include matices.mk

cne.pfms.raw philofacts.pfms.raw splice_a.pfms.raw splice_d.pfms.raw pol.pfms.raw fam.pfms.raw pmb.pfms.raw pmb_homeo.pfms.raw pmb_hlh.pfms.raw: %.pfms.raw: all.pfms.raw
	fasta2tab $< \
	| bawk '$$1~/^$(shell bawk '$$1=="$*" {print $$2}' $(PRJ_ROOT)/local/share/data/collections2id.map)/' \
	| cut -f 1,3- \
	| tab2fasta > $@


all.pfms.annot:
	wget -O - $(URL_PREFIX)/all_data/matrix_only/matrix_only.txt \
	| grep '^>' | tr -d '>' | perl -pne 's/\s+/\t/g' > $@
	
core.pfms.annot:
	wget -q -O - $(URL_PREFIX)/FlatFileDir/matrix_list.txt \
	| sed -r 's/ +\t/\t/g' \
	| sed 's/Vertebrate/vertebrate/g' \                                       * correct typo     *
	| sed 's/PLANT/plant/g' \                                                 * correct typo     *
	| sed 's/veterbrate/vertebrate/g' \                                       * correct typo     *
	| sed 's/BRCA1\(.*\)sysgroup ""/BRCA1\1sysgroup "vertebrate"/' > $@       * add missing data *

core.matrix2accession.map.fuzy: core.pfms.annot
	perl -lanwe '$$,="\t"; $$\="\n";		m/acc\s+"([^"]+)"/;		$$a=$$1;		$$a=$$F[2] if not $$a or $$a eq "-";		$$a=~s/\s+,\s+/;/; 		print $$F[0],$$a' $< \
	| perl -lane 'print uc' \
	| sed -f $(PRJ_ROOT)/local/share/data/accession-manual-conversion.sed  > $@

core.matrix2ensembl.map.fuzy: core.matrix2accession.map.fuzy $(UNIPROT_DIR)/idmapping.dat.gz
	expandsets 2 < $< | translate -d -v -j -e NULL -a <(bawk '$$ID_type=="Ensembl" {print $$1,$$3}' $^2) 2 > $@


core.matrix2name.map.fuzy: core.pfms.annot
	cut -f 1,3 $< \
        | sed 's/-\([A-Z]..\)/;\1/' \
	| perl -lane 'print uc' > $@

core.accession2entrez: core.matrix2accession.map.fuzy
	(for i in `cut -f 2 $< | expandsets 1`; do\
		wget -O - "http://www.ncbi.nlm.nih.gov/sites/entrez?db=gene&cmd=search&term=$$i" \
		| perl -lane 'if(m/GeneID:\s+(\d+)/){print $$1; exit(0)}' | append_each_row -B $$i;\
	done;) >$@

core.pfms.occurrencies.class: fake_meme_id-motif.map core.pfms.occurrencies core.pfms.annot
	translate $^1 1 < $^2\
	| translate -a <(cut -f 1,4 $^3) 1 \
	| cut -f 2 | symbol_count >$@

.META: core.pfms.occurrencies.class
	1	class	AP2
	2	occurrencies	2376	Occurrencies of motifs of given class in the whole genome


.META: core.pfms.annot
	1	ID	a unique identifier for each model. CORE matrices always have a MAnnnn IDs
	2	Name	The name of the transcription factor. Based on Entrez gene symbols. In the case of hetero-dimer, two names are concatenated, such as RXR-VDR. In a few cases, different splice forms of the same gene have different binding specificity: in this case the splice form information is added to the name, based on the relevant literature.
	1	ID	MA0062
	2	total_ic	13.8895030261926	The information content of the matrix, based on Shannon. See Schnedier's excellent primer for a review
	3	name	GABPA
	4	class	ETS	Structural class of the transcription factor, based on PFAM and Interpro
	5	other	; acc "Q06546" ; medline "8383622" ; species "Homo sapiens" ; sysgroup "vertebrate" ; total_ic "13.8920" ; type "COMPILED"
			Acc	A representative protein accession number in Genbank for the transcription factor. 
			Type	Methodology used for matrix construction  - currently either by in-vitro selection assays(SELEX) or by comiling in-vivo sites (COMPILED)
			medline Medline	a link to the relevant publication reporting the sites used in the mode building
			Species	The species source for the sequences, in Latin. “-“ generally signifies that several species were used in the model construction
			Sysgroup	Group of species, currently consisting of 4 larger groups: vertebrate, insect, plant, chordate
			Comment	For some matrices, a curator comment is added
		

%.count: %.pfms.raw
	cat $< \
	| sed -r 's/^\s+//' | sed -r 's/\s+/\t/g' \
	| sed 's/\b0\b/1/g' \					* pseudocounts *
	| repeat_fasta_pipe -n '\
		transpose \
		| enumerate_rows -s -1\
		| append_each_row -b $$HEADER\
	' > $@

%.pfms: %.pfms.raw
	cat $< \
	| sed -r 's/^\s+//' | sed -r 's/\s+/\t/g' \
	| sed 's/\b0\b/1/g' \					* pseudocounts *
	| repeat_fasta_pipe -n '\
		normalize_columns | transpose \
		| enumerate_rows -s -1\
		| append_each_row -b $$HEADER\
	' \
	| sed 's/\t0.0\t//'> $@
	
.META: %.pfms
	1	ID	MA0001
	2	position	0
	3	A_frequency	0.0
	4	C_frequency	0.969072164948
	5	G_frequency	0.0103092783505
	6	T_frequency	0.020618556701

%.ss: %.pfms ../../../local/share/data/background.txt 
	pfms2ss -s -c $(TFBS_ABSOLUTE_CUTOFF) -C $(TFBS_RELATIVE_CUTOFF) -m $< -b $^2 >$@

.DOC: %.ss
	SiteSiker Format
	Ogni 4 righe indicano una posizione della matrice (A C G T), poi la prima colonna
	e` il punteggio (-1000 appunto quando la % di occorrenza del dato carattere e` zero), 
	poi il max totale per quella matrice (non so bene perche` Francesco la tenesse ma ho
	deciso per ora di tenerle cosi` per non stare ad avere un altro formato	da gestire)
	e poi il cutoff da considerare.


%.fake_meme: %.pfms ../../../local/share/data/background.txt 
include ../../../local/share/rules-fake_meme.mk
	#(\
	#	echo MEME version 4.0.0;\
	#	echo ALPHABET= ACGT;\
	#	echo strands: + -;\
	#	echo 'Background letter frequencies (from dataset with add-one prior applied):';\
	#	tr "\t" "\n" < $^2 | transpose\
	#) > $@
	#id2count 1 < $< | sort -k 1,1\
	#| repeat_group_pipe '\
	#	echo "MOTIF  $$1";\
	#	echo "BL   MOTIF $$1 width=0 seqs=0";\
	#	echo letter-probability matrix: alength= 4 w= len_$$1 nsites= 1 E= 1;\
	#	cut -f 3-\
	#' 1 \
	#| translate -v -w <(id2count 1 < $< | find_best 1 2 | bawk '{print "len_" $$1,$$2+1}') >> $@

%.fake_meme_id-motif.map: %.pfms
	id2count 1 $< | cut -f 1 | paste - $< | cut -f 1,2 | sort -k 2,2 | uniq > $@

%.entropy: %.pfms
	bsort -k1,1 $< | repeat_group_pipe 'echo -ne "$$1\t"; cut -f 3- | matrix_entropy' 1 > $@

%.minmax_score: %.count $(TASK_ROOT)/local/share/data/background_human_hg18.txt
	best_score $^2 < $< > $@ 

.META: *.minmax_score
	1	id
	2	min_score
	3	max_score

%.optim_cutoff: %.minmax_score
	bawk '{x=$$max_score; print $$id, $(CUTOFF_MODEL_PARAM_A)*x*x + $(CUTOFF_MODEL_PARAM_B)*x + $(CUTOFF_MODEL_PARAM_C) }' $< > $@

%.pfms.occurrencies.chr%: %.pfms.occurrencies.chr%.xml
	fimo_xml_parser $< \
	| bawk '{strand = "+"; if($$3>$$4){strand = "-"; tmp=$$3; $$3=$$4; $$4=tmp}; $$4 = $$4 "\t" strand; print }' > $@

.META: *.pfms.occurrencies.chr*
	1	motif_id	100
	2	seq_id	22
	3	b	14441507
	4	e	14441516
	5	strand	+
	6	score	9.99515
	7	pvalue	8.94e-05
	8	qvalue	0.426
	9	match-sequence	TGGGACTTCC

%.pfms.occurrencies.gz: $(addprefix %.pfms.occurrencies.chr,$(SPECIES_CHRS))
	cat $^ | gzip > $@

.META: *.pfms.occurrencies.gz
	1	motif_id	100
	2	seq_id	22
	3	b	14441507
	4	e	14441516
	5	strand	+
	6	score	9.99515
	7	pvalue	8.94e-05
	8	qvalue	0.426
	9	match-sequence	TGGGACTTCC

####
#
# files for the translation of pwm in uniprot ids and then in entrez genes
#
####

matrix_xref:
	wget -O - $(URL_DIR_MATRIX_CONVERSION)/MATRIX.txt > $@

.META: matrix_xref
	1	internal_ID	9229
	2	belonging_db	CORE
	3	matrix_ID	MA0001
	4	??	1
	5	hugo_name	AGL3

matrix_swissprot:
	wget -O - $(URL_DIR_MATRIX_CONVERSION)/MATRIX_PROTEIN.txt > $@

.META: matrix_protein
	1	internal_ID	9229
	2	swissprot_ID	P29383
