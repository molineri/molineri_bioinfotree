TAX_GRUP = $(shell pwd | perl -lne '@F=split(/\//,$$_); print $$F[-1]')

URL_PREFIX_CORE = http://jaspar.genereg.net/html/DOWNLOAD/jaspar_CORE/non_redundant/by_tax_group/$(TAX_GRUP)

ALL+= core.count core.pfms.annot

core.pfms.raw: core.pfms.annot
	wget -O - $(URL_PREFIX_CORE)/matrix_only/matrix_only.txt \
	| tr -d '[]' \
	| perl -pne 'BEGIN{$$\="\n";} s/\s+$$//; s/^\s+//; s/\s+/\t/g; if(!m/^>/){ s/[ACGT]\t// }' > $@

core.pfms.annot:
	wget -q -O - $(URL_PREFIX_CORE)/FlatFileDir/matrix_list.txt \
	| sed -r 's/ +\t/\t/g' \
	| sed 's/Vertebrate/vertebrate/g' \                                       * correct typo     *
	| sed 's/PLANT/plant/g' \                                                 * correct typo     *
	| sed 's/veterbrate/vertebrate/g' \                                       * correct typo     *
	| sed 's/BRCA1\(.*\)sysgroup ""/BRCA1\1sysgroup "vertebrate"/' > $@       * add missing data *

%.uniprotHS.annot: %.pfms.annot
	grep 'species "$(UNIPROT_ANNOTATION_REF_SPECIE)"' < $< | \
        perl -F"\t" -ane 'if ($$F[4] =~ m/acc "([^\s]+)"/) { print $$F[0] . "\t" . $$F[2] . "\t" . $$1 . "\n"; }' | \
 	expandsets 3 -s, > $@

.META:
	1       id	MA0059.1
	2       name	MYC::MAX
	3       accession	AH36092

AB_ANNOT := /rogue/bioinfotree/prj/bind_coop/dataset/0.1/Yale/antibody_annotation

encode_ab_uniprot: $(AB_ANNOT)
	getEncodeUniprotAnnotation < $< > $@

.META:
	1       AbName  NFKB
	2       AbHsUniprotID     P19838

.SECONDARY:
%.encode_ab_uniprot.annot: %.uniprotHS.annot encode_ab_uniprot %.pfms.annot
	translate -a -v -d -j -c -f 3 $< 2 < $^2 > $@

.META:
	1	AbName	NFKB
	2	AbHsUniprotID     P19838
	3	jasparID	
	4	jasparName

%.encode_ab_uniprot_names.annot: %.encode_ab_uniprot.annot %.pfms.annot
	bawk '$$3 == "" {print $$2,$$1}' < $< | translate -f 2 -a -d -j -v -c <(bawk '{print $$1,$$3,$$3}' < $^2) 2 > $@
	bawk '$$3 != "" {print $$2,$$1,$$3,$$4}' < $< >> $@

.META:
	1	AbHsUniprotID     P19838
	2	AbName	NFKB
	3	jasparID	
	4	jasparName


%.count: %.pfms.raw
	cat $< \
	| sed -r 's/^\s+//' | sed -r 's/\s+/\t/g' \
	| sed 's/\b0\b/1/g' \					* pseudocounts *
	| repeat_fasta_pipe -n '\
		transpose \
		| enumerate_rows -s -1\
		| append_each_row -b $$HEADER\
	' > $@

%.pfms: %.pfms.raw
	cat $< \
	| sed -r 's/^\s+//' | sed -r 's/\s+/\t/g' \
	| sed 's/\b0\b/1/g' \					* pseudocounts *
	| repeat_fasta_pipe -n '\
		normalize_columns | transpose \
		| enumerate_rows -s -1\
		| append_each_row -b $$HEADER\
	' \
	| sed 's/\t0.0\t//'> $@
	
.META: %.pfms
	1	ID	MA0001
	2	position	0
	3	A_frequency	0.0
	4	C_frequency	0.969072164948
	5	G_frequency	0.0103092783505
	6	T_frequency	0.020618556701

%.ss: %.pfms ../../../local/share/data/background.txt 
	pfms2ss -s -c $(TFBS_ABSOLUTE_CUTOFF) -C $(TFBS_RELATIVE_CUTOFF) -m $< -b $^2 >$@

.DOC: %.ss
	SiteSiker Format
	Ogni 4 righe indicano una posizione della matrice (A C G T), poi la prima colonna
	e` il punteggio (-1000 appunto quando la % di occorrenza del dato carattere e` zero), 
	poi il max totale per quella matrice (non so bene perche` Francesco la tenesse ma ho
	deciso per ora di tenerle cosi` per non stare ad avere un altro formato	da gestire)
	e poi il cutoff da considerare.


%.entropy: %.pfms
	repeat_group_pipe -s 'echo -ne "$$1\t"; cut -f 3- | matrix_entropy' 1 < $< > $@


.PHONY: %.matlign

#using options from matlign readme
#exits with 1, why?
%.matlign: %.count
	matlign -input=<(cut -f 2 --complement < $< | tab2fasta -s) -transv=-4 -transi=-4 -match=5 -gopen=-10 -gext=-1 -term=5 -norm=235 \
	-out=$* -spacer=1  -mode=1 -zscore=1 -random=0 -pseudo=0 -freqat=0.5 -freqcg=0.5
