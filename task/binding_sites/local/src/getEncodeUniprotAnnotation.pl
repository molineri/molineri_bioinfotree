#!/usr/bin/perl -w
#gets uniprot id starting from antibody terms (Encode data) using cv.ra
use LWP::Simple;

if ($#ARGV != -1) {
	die "Usage: $0 < cv.ra > name_and_uniprot_ids";
}

#we get the uniprot id passing from the genecard url

my $term;
my $status = 0;
while(<STDIN>) {
	if (/^#/) {
		next;
	}
	chomp();
	if ($status == 0 && /^term\s+([^\s]+)/) {
		$term = $1;
	} elsif ($status == 0 && /^type\s+Antibody/) {
		$status = 1;
	} elsif ($status == 1 && /^targetUrl\s+([^\s]+)/) {
		my $u = &getUniprotID($1);
		print $term . "\t" . $u . "\n";
		$status = 0;	
	}
}

sub getUniprotID {
	my $url = $_[0];
	my $content = get $url;
	return "NA" unless defined $content;
	if ($content =~ /http:\/\/www\.uniprot\.org\/uniprot\/([^\s"]+)"/) {
		return $1;
	}
	else {
		return "NA";
	}
}



