sub sum {

    my $sum = 0;
    foreach (@_) {
        $sum += $_;
    }
    return $sum;
}


sub score {

    my $seq = shift @_;
    my @mat =  @_;
    my $score = 0;
    my @seq = split("", $seq);
    for (my $pos = 0; $pos < length($seq); ++$pos) {
        my @line = split("\t", $mat[$pos]);
        if ($seq[$pos] eq 'A') {
            $score += $line[0];
#            next;
        }
        elsif ($seq[$pos] eq 'C') {
            $score += $line[1];
#            next;
        }
        elsif ($seq[$pos] eq 'G') {
            $score += $line[2];
#            next;
        }
        elsif ($seq[$pos] eq 'T') {
            $score += $line[3];
#            next;
        }
    }
    return $score;
}


sub revc {

    my $string = $_[0];
    $string =~ tr/ACGT/TGCA/;
    return join("",reverse split("",$string));
}

sub log2 {

    my @out = ();
    foreach (@_) {
        push @out, log($_)/log(2.0);
    }
    return @out;
}

sub maxscore {

    my $s1 = score(@_);
    my $seq = shift @_;
    my $s2 = score(revc($seq), @_);
#    print STDERR "$s1 $s2\n";
    if ($s1 >= $s2) {
        return ($s1, '+');
    }
    else {
        return ($s2, '-');
    }
}

sub normalized {

    my @background = splice(@_,0,4);
    my @norm = @_;
    foreach my $line (@norm) {
        my @line = split("\t", $line);
        my $tot = sum(@line);
        @line = log2(@line);
        for (my $i = 0; $i <= $#line; ++$i) {
            $line[$i] -= log($tot)/log(2);
            $line[$i] -= $background[$i];
        }
        $line = join("\t", @line);
    }
    return @norm;
}


sub mapping {

    # map between position in string and actual position in aligned sequence (i.e. not counting gaps)

    my ($seq) = @_;
    my @out = ();
    my @seq = split('',$seq);
    my $j = -1;
    for (my $i = 0; $i <= $#seq; ++$i) {
        if ($seq[$i] ne '-') {
            ++$j;
        }
        $out[$i] = $j;
    }
    return @out;
}

sub inverse_mapping {

    # inverse map wrt the one defined by mapping

    my ($seq) = @_;
    my @out = ();
    my @seq = split('',$seq);
    my $j = -1;
    for (my $i = 0; $i <= $#seq; ++$i) {
        if ($seq[$i] ne '-') {
            ++$j;
            $out[$j] = $i;
        }
    }
    return @out;
}

sub best_score {

    my $best_score = 0;
    foreach my $pos (@_) {
        my @line = split("\t", $pos);
        $best_score += max(@line);
    }
    return $best_score;

}


sub max {

    my $max = $_[0];
    for (my $i = 1; $i <= $#_; ++$i) {
        if ($_[$i] > $max) {
            $max = $_[$i];
        }
    }
    return $max;
}


1;

