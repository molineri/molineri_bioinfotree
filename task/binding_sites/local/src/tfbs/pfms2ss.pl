#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use lib $ENV{'BIOINFO_ROOT'}.'/task/binding_sites/local/src/tfbs/';
use Tfbs;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

our($opt_h, $opt_m, $opt_c, $opt_b, $opt_M, $opt_o, $opt_C, $opt_s);
getopts('hm:c:b:Mo:C:s');

my $usage = <<EOF;
usage $0 -m matrix_file -c cutoff -C fractional_cutoff -b background_file 
 [-s use -c and -C at the same time]
EOF

if ($opt_h || ! defined $opt_m || !defined $opt_b) {
    die $usage;
}

if (! defined $opt_c && ! defined $opt_C) {
    die $usage;
}

if ( defined $opt_c && defined $opt_C && ! defined $opt_s) {
    die "Options -c and -C are mutually exclusive without -s option\n";
}

if (! defined $opt_o) {
    $opt_o = 0;
}

my @base = ('A', 'C', 'G', 'T');

open BKG, $opt_b or die "Error opening $opt_b: $!\n";
my @background = ();
my @bkg = <BKG>;
chomp @bkg;
my @check = ();
for (my $i = 0; $i <= 3; ++$i) {
    my @line = split("\t", $bkg[$i]);
    push @check, $line[0];
    push @background, $line[1];
}
for (my $i = 0; $i <= 3; ++$i) {
    if ($check[$i] ne $base[$i]) {
        die "Error in $opt_b: $bkg[$i]\n";
    }
}



my $tot = sum(@background);
foreach (@background) {
    $_ /= $tot;
}


# load matrices
my %matrix;
open MAT, $opt_m or die "Error opening $opt_m";
while (<MAT>) {
    chomp;
    my @line = split("\t");
    my $mat_id = shift @line;
    my $pos = shift @line;
    ${ $matrix{$mat_id} }[$pos] = join("\t", @line);
}
close MAT;

foreach my $mat_id (keys %matrix) {
    #print STDERR join("\t",@{ $matrix{$mat_id} }),"\n";
    #@background = log2(@background);
    #my @mat = normalized(@background,  @{ $matrix{$mat_id} });
    my @mat = log_likeliood($matrix{$mat_id});
    #print STDERR $mat_id;
    #print STDERR @background;
    #print STDERR @{ $matrix{$mat_id} };
    #die;
    my $best_score =  best_score( @mat );
    my $cutoff = undef;
    if (defined $opt_C and defined $opt_c) {
	die("error in checking options") if not defined $opt_s;
	$cutoff = $opt_C * $best_score;
	if ($cutoff < $opt_c){
		$cutoff = $opt_c;
	}
    }
    elsif (defined $opt_C) {
        $cutoff = $opt_C * $best_score;
    }
    else {
        $cutoff = $opt_c;
    }
    foreach my $line(@mat){
    	   my @F=split /\t/, $line;
	   for(@F){
	   	print $mat_id,$_,$best_score,$cutoff;
	}
    }
}

sub log_likeliood
{
	my $mat_Array_ref = shift;
	my @retval=();
	foreach my $position (@{$mat_Array_ref}){
		my $i=0;
		my @tmp=();
		for(split /\t/,$position){
			push @tmp,log($_/$background[$i])/log(2);
			$i++;
		}
		push @retval,join("\t",@tmp);

	}
	return @retval;
}
