/***************************************************************************
 *   Copyright (C) 2007 by Ari Loytynoja and Matti Kankainen               *
 *   ari@ebi.ac.uk / matti.kankainen@helsinki.fi                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "intmatrix.h"
#include "flmatrix.h"
#include "zscore.h"
#include "filereader.h"
#include "viterbi.h"

using namespace std;

extern float match;
extern float transi;
extern float transv;
extern float gOpen;
extern float gExt;
extern int tLength;
extern int noise;
extern bool FIRST;
extern bool SECOND;
extern std::string norm;
extern int zscore;
extern int spacer;

//*********************************************************************************************************//
Zscore::Zscore(int orig_tlength) {
	tlength = orig_tlength;

	initialize_array(zscore1);
	initialize_array(zscore2);
};
//*********************************************************************************************************//
Zscore::~Zscore() {
	zscore1.clear();
	zscore2.clear();
};
//*********************************************************************************************************//
void Zscore::initialize_array(vector<string>& zscoreX) {
	zscoreX.push_back("1\t0\t0\t0\n"); 
	zscoreX.push_back("0\t1\t0\t0\n"); 
	zscoreX.push_back("0\t0\t1\t0\n"); 
	zscoreX.push_back("0\t0\t0\t1\n"); 
	zscoreX.push_back("1\t1\t0\t0\n"); 
	zscoreX.push_back("1\t0\t1\t0\n"); 
	zscoreX.push_back("1\t0\t0\t1\n"); 
	zscoreX.push_back("0\t1\t1\t0\n");
	zscoreX.push_back("0\t1\t0\t1\n"); 
	zscoreX.push_back("0\t0\t1\t1\n"); 
	zscoreX.push_back("1\t1\t1\t0\n"); 
	zscoreX.push_back("1\t1\t0\t1\n"); 
	zscoreX.push_back("1\t0\t1\t1\n"); 
	zscoreX.push_back("0\t1\t1\t1\n");
	zscoreX.push_back("1\t1\t1\t1\n"); 
};
//*********************************************************************************************************//
int Zscore::calculate_zscore(IntMatrix* c1, IntMatrix* c2, float &mean, float &std) {
	FlMatrix* m1;
	FlMatrix* m2;
	m1 = new FlMatrix(c1->X(),4,"frequencies");
	int i,j;
	float sum, score;

	FOR(i,c1->X()){
		sum = 0;
		FOR(j,4) {
			m1->s(c1->g(i,j),i,j);
			sum+=c1->g(i,j);
		}
		FOR(j,4) {
			m1->d(sum,i,j);
		}
	}

	m2 = new FlMatrix(c2->X(),4,"frequencies");
	FOR(i,c2->X()){
		sum = 0;
		FOR(j,4) {
			m2->s(c2->g(i,j),i,j);
			sum+=c2->g(i,j);
		}
		FOR(j,4) {
			m2->d(sum,i,j);
		}
	}
	
	Viterbi* vit = new Viterbi(m1,m2);
	vit->fill(&mean, &std);

	delete m1;
	delete m2;
	delete vit;
	return(0);
};
//*********************************************************************************************************//
void Zscore::get_zscore(float &n0_mean, float &n0_std, float &n1_mean, float &n1_std, float &n2_mean, float &n2_std, 
		        float &n3_mean, float &n3_std, float &n4_mean, float &n4_std, float &n5_mean, float &n5_std) {
	int tlength_temp(0);
	int min_l(0);
	int max_l(0);
	int min_length (0);
	string temp_norm;
	float* norm_mean(0);
	float* norm_std(0);

	IntMatrix* p0;
	IntMatrix* p1;
	FileReader* fr = new FileReader();    
 
	p0 = fr->readMatrix(zscore1, min_length, "zscore", 0, 0.5, 0.5);
	p1 = fr->readMatrix(zscore2, min_length, "zscore", 0, 0.5, 0.5);
                
	tLength= get_tlength(((*p0).X()-1), ((*p1).X()-1), min_l, max_l);

	temp_norm = norm;
	for(int i = 0; i < temp_norm.size(); ++i) {
		norm = temp_norm.substr(i, 1).c_str();

		if(atoi(norm.c_str()) == 0) { norm_mean = &n0_mean; norm_std = &n0_std; }
		if(atoi(norm.c_str()) == 1) { norm_mean = &n1_mean; norm_std = &n1_std; }
		if(atoi(norm.c_str()) == 2) { norm_mean = &n2_mean; norm_std = &n2_std; }
		if(atoi(norm.c_str()) == 3) { norm_mean = &n3_mean; norm_std = &n3_std; }
		if(atoi(norm.c_str()) == 4) { norm_mean = &n4_mean; norm_std = &n4_std; }
		if(atoi(norm.c_str()) == 5) { norm_mean = &n5_mean; norm_std = &n5_std; }

		if(atoi(norm.c_str()) < 6 && atoi(norm.c_str()) >= 0) {
			if(*norm_mean != 0)     { *norm_mean = 0; }
			if(*norm_std  != 1)     { *norm_std  = 1; }
			calculate_zscore(p0, p1, *norm_mean, *norm_std);
		}
	}
	norm = temp_norm;
	
	delete p0;
	delete p1;
	delete fr;
};
//*********************************************************************************************************//
int Zscore::get_tlength(int length_pat1, int length_pat2, int &min, int &max) {
	int return_length(0);
	int length_diff(0);

	if (length_pat1 >= length_pat2) { length_diff = length_pat1 - 1; min = length_pat2; max = length_pat1; }
	if (length_pat1 <  length_pat2) { length_diff = length_pat2 - 1; min = length_pat1; max = length_pat2; }

	if((spacer == 1) && (tlength > length_diff)) {
		return_length = length_diff;
	} else if ((spacer == 1) && (tlength <= length_diff)) {
		return_length = tlength;
	} else if (spacer == 0) {
		return_length = length_diff;
	} else {
		return_length = 0;
	}

	return(return_length);
};
//*********************************************************************************************************//
