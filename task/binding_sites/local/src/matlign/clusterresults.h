/***************************************************************************
 *   Copyright (C) 2007 by Ari Loytynoja and Matti Kankainen               *
 *   ari@ebi.ac.uk / matti.kankainen@helsinki.fi                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef clust_res_h
#define clust_res_h

#include <string>

class ClusterResults {
public:
	void SetElement(std::string, std::string, std::string, std::string, double, float, unsigned int);
	void GetElement(std::string&, std::string&, std::string&, std::string&, double&, float&, unsigned int&);

	ClusterResults();
	~ClusterResults();
private:
	std::string new_element;
	std::string element1;
	std::string element2;
	std::string consensus;
	float element_distance;
	float element_fdr;
	unsigned int node_nro;
};

#endif
