/***************************************************************************
 *   Copyright (C) 2007 by Ari Loytynoja and Matti Kankainen               *
 *   ari@ebi.ac.uk / matti.kankainen@helsinki.fi                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <string>
#include "clusterresults.h"

using namespace std;

//*********************************************************************************************************//
ClusterResults::ClusterResults() {
	new_element 		= "";
	element1 		= "";
	element2 		= "";
	element_distance 	= 0;	
	consensus		= "";
	element_fdr		= 0;
	node_nro		= 0;
};
//*********************************************************************************************************//
ClusterResults::~ClusterResults() {
	new_element 		= "";
	element1 		= "";
	element2 		= "";
	element_distance 	= 0;
	consensus		= "";
	element_fdr		= 0;
	node_nro		= 0;
};
//*********************************************************************************************************//
void ClusterResults::SetElement(string name_new, string name_element1, string name_element2, string name_consensus, double value, float fdr, unsigned int nro) {
	new_element 		= name_new;
	element1 		= name_element1;
	element2 		= name_element2;
	element_distance 	= value;
	consensus		= name_consensus;
	element_fdr		= fdr;
	node_nro		= nro;
};
//*********************************************************************************************************//
void ClusterResults::GetElement(string &name_new, string &name_element1, string &name_element2, string &name_consensus, double &value, float &fdr, unsigned int &nro) {
	name_new 		= new_element;
	name_element1 		= element1;
	name_element2 		= element2;
	value 			= element_distance;
	name_consensus		= consensus;
	fdr 			= element_fdr;
	nro			= node_nro;
};
//*********************************************************************************************************//
