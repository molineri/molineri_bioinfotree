/***************************************************************************
 *   Copyright (C) 2006 by Ari Loytynoja   *
 *   ari@ebi.ac.uk   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <cmath>
#include <fstream>
#include "flmatrix.h"
#include "intmatrix.h"
#include "intmatrixrow.h"
#include "viterbi.h"
#include "filereader.h"
#include "distancematrix.h"
#include "clusterresults.h"
#include "clusterinfo.h"
#include "zscore.h"
#include "random.h"

using namespace std;

float match;
float transi;
float transv;
float gOpen;
float gExt;

float pseudo_count;
float genome_fra;
float genome_frc;

int tLength;
int noise;
int this_norm;
string norm;
int spacer;
int mode;
int zscore;
int random_perm;
int maxsize;

float norm0_mean;
float norm0_std;
float norm1_mean;
float norm1_std;
float norm2_mean;
float norm2_std;
float norm3_mean;
float norm3_std;
float norm4_mean;
float norm4_std;
float norm5_mean;
float norm5_std;

string file;
string out;
bool FIRST;
bool SECOND;
//*********************************************************************************************************************//

void calc_alignment_score(IntMatrix*, IntMatrix*, IntMatrix**, IntMatrix*, IntMatrix*, IntMatrix**, float&, float&);
int calc_all_scores();
int get_tLength(int, int, int, int, int&, int&);
int calculate_silhouette_value(vector<ClusterInfo>&, vector<float>&, DistanceMatrix&, unsigned long, float, FILE*, unsigned long);
void convert_int_to_fl_matrix (IntMatrix*, FlMatrix*);
void normalize_silhouette_matrix(int, float, float, DistanceMatrix&, DistanceMatrix&);
void print_cluster_variance(vector<ClusterInfo>&, FILE*);
void print_cluster_silhouette(vector<float>&, FILE*, unsigned long, float, unsigned long);
void clear_memory (DistanceMatrix&, DistanceMatrix&, vector<string>&, vector<IntMatrix*>&, vector<IntMatrix*>&, 
		   vector<ClusterResults>&, vector<ClusterInfo>&, vector<float>&, FILE*, FILE*, FILE*, FILE*, FILE*, FILE*);

//*********************************************************************************************************************//
int main(int argc, char *argv[]) {
	bool e		= 0;
	int r		= 0;
	match 		= 10;		//DEFAULT VALUE
	transi 		= -4;		//DEFAULT VALUE
	transv 		= -4;		//DEFAULT VALUE
	gOpen 		= -10;		//DEFAULT VALUE
	gExt 		= -1;		//DEFAULT VALUE
	tLength 	= 5;		//DEFAULT VALUE
	noise 		= 0;		//DEFAULT VALUE
	out 		= "outfile";	//DEFAULT VALUE
	norm		= 5;		//0=Kendall's, 1=Spearman's uncor, 2=Spearman's cor, 3=Pearson's, 4=Euclidean, 5=Dynamic, 6=YSRA
	spacer		= 1;		//DEFAULT VALUE (spacer is enabled)
	mode		= 0;		//DEFAULT VALUE (do not cluster patterns)
	this_norm	= 5;
	zscore		= 0;		//DEFAULT VALUE (do not use z-scores)
	random_perm	= 0;
	pseudo_count	= 0;
	genome_fra	= 0.5;
	genome_frc	= 0.5;
	maxsize		= -1;

	norm0_mean 	= 0;
	norm0_std 	= 1;
	norm1_mean 	= 0;
	norm1_std 	= 1;
	norm2_mean	= 0;
	norm2_std	= 1;
	norm3_mean 	= 0;
	norm3_std 	= 1;
	norm4_mean 	= 0;
	norm4_std 	= 1;
	norm5_mean 	= 0;
	norm5_std 	= 1;

	int seed 	= time(NULL);
	srand ( seed );	
	//cout << seed << "\n";
	for(int i=1;i<argc;i++) {
		string s = string(argv[i]);

		if(s.substr(0,7)=="-input=") {
			file = s.substr(7);
			e = 1;
		} else if(s.substr(0,7)=="-match=") {
			match = atof(s.substr(7).c_str());
		} else if(s.substr(0,8)=="-transi=") {
			transi = atof(s.substr(8).c_str());
		} else if(s.substr(0,8)=="-transv=") {
			transv = atof(s.substr(8).c_str());
		} else if(s.substr(0,7)=="-gopen=") {
			gOpen = atof(s.substr(7).c_str());
		} else if(s.substr(0,6)=="-gext=") {
			gExt = atof(s.substr(6).c_str());
		} else if(s.substr(0,6)=="-term=") {
			tLength = atoi(s.substr(6).c_str());
		} else if(s.substr(0,5)=="-out=") {
			out = s.substr(5);
		} else if(s.substr(0,7)=="-noise=") {
			noise = atoi(s.substr(7).c_str());
		} else if(s.substr(0,6)=="-norm=") {
			norm = (s.substr(6).c_str());
		} else if(s.substr(0,8)=="-spacer=") {
			spacer = atoi(s.substr(8).c_str());
		} else if(s.substr(0,6)=="-mode=") {
			mode = atoi(s.substr(6).c_str());
		} else if(s.substr(0,8)=="-zscore=") {
			zscore = atoi(s.substr(8).c_str());
		} else if(s.substr(0,8)=="-random=") {
			random_perm = atoi(s.substr(8).c_str());
                } else if(s.substr(0,8)=="-pseudo=") {
                        pseudo_count = atof(s.substr(8).c_str());
                } else if(s.substr(0,8)=="-freqat=") {
                        genome_fra = atof(s.substr(8).c_str());
                } else if(s.substr(0,8)=="-freqcg=") {
                        genome_frc = atof(s.substr(8).c_str());
		} else if(s.substr(0,9)=="-maxsize=") {
			maxsize	= atoi(s.substr(9).c_str());
		} else {
			cout<<"unrecognised argument: "<<s<<endl;
		}
	}

	if (spacer == 0) {
		FIRST = 1; SECOND = 0;  //spacer is disabled
	} else if (spacer == 1) {
		FIRST = 1; SECOND = 1;  //spacer is enabled
	} else if (spacer == 2) {
		FIRST = 0; SECOND = 1;  //spacer is required
	} else {
		FIRST = 1; SECOND = 1;  //spacer is enabled
	}

	if (e == 1) {
		r = calc_all_scores();
	}
	return(1);
}
//*********************************************************************************************************************//
int calc_all_scores () {
	IntMatrix* p0;
	IntMatrix* i1;
	FlMatrix* fltemp;

	FileReader* fr = new FileReader();
	RandomMatrix* random_matrix = new RandomMatrix();

	char buffer[10];

	int min_length(0), min_temp_len(0), min_l(0), max_l(0), n_mats(0);
	int parameter_tlength = tLength;	
	int file_ok(0);

	unsigned long row_max_dist(0);
	unsigned long col_max_dist(0);
	unsigned long cluster_size(0);
	unsigned long clusters_tot(0);

	double pair_score(0);
	float score(0), align_cols(0);
	float temp_max(0), temp_min(0), temp_score(0);
	float fdr(1);

	string element1;
	string element2;
	string new_element;
	string consensus;

	vector <string>                         mat_names;
	vector <ClusterResults>                 cluster_results;
	vector <IntMatrix*>                     input_mat_vector;
	vector <IntMatrix*>			one_zero_mat_vector;
	vector <ClusterInfo>                    cluster_info;
	vector <float>                          silhouette_value;

        DistanceMatrix dist_mat;
        DistanceMatrix silhouette;

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //File handling. Open the print streams

	string pswm_file = out + ".pswm"; FILE *pswm_stream; pswm_stream = fopen(pswm_file.c_str(), "w+");
	string mtrx_file = out + ".mtrx"; FILE *mtrx_stream; mtrx_stream = fopen(mtrx_file.c_str(), "w+");
	string tree_file = out + ".tree"; FILE *tree_stream; tree_stream = fopen(tree_file.c_str(), "w+");
	string vara_file = out + ".vara"; FILE *vara_stream; vara_stream = fopen(vara_file.c_str(), "w+");
	string fdrs_file = out + ".fdrs"; FILE *fdrs_stream; fdrs_stream = fopen(fdrs_file.c_str(), "w+");
	string freq_file = out + ".freq"; FILE *freq_stream; freq_stream = fopen(freq_file.c_str(), "w+");

	if (pswm_stream == NULL) { fprintf (stderr, "ERROR 1: FILE NOT FOUND %s\n", pswm_file.c_str()); exit(0); }
	if (ferror(pswm_stream)) { fprintf (stderr, "ERROR 2: FILE NOT FOUND %s\n", pswm_file.c_str()); exit(0); }

	if (mtrx_stream == NULL) { fprintf (stderr, "ERROR 1: FILE NOT FOUND %s\n", mtrx_file.c_str()); exit(0); }
	if (ferror(mtrx_stream)) { fprintf (stderr, "ERROR 2: FILE NOT FOUND %s\n", mtrx_file.c_str()); exit(0); }

	if (tree_stream == NULL) { fprintf (stderr, "ERROR 1: FILE NOT FOUND %s\n", tree_file.c_str()); exit(0); }
	if (ferror(tree_stream)) { fprintf (stderr, "ERROR 2: FILE NOT FOUND %s\n", tree_file.c_str()); exit(0); }

	if (vara_stream == NULL) { fprintf (stderr, "ERROR 1: FILE NOT FOUND %s\n", vara_file.c_str()); exit(0); }
	if (ferror(vara_stream)) { fprintf (stderr, "ERROR 2: FILE NOT FOUND %s\n", vara_file.c_str()); exit(0); }

        if (fdrs_stream == NULL) { fprintf (stderr, "ERROR 1: FILE NOT FOUND %s\n", fdrs_file.c_str()); exit(0); }
        if (ferror(fdrs_stream)) { fprintf (stderr, "ERROR 2: FILE NOT FOUND %s\n", fdrs_file.c_str()); exit(0); }

        if (freq_stream == NULL) { fprintf (stderr, "ERROR 1: FILE NOT FOUND %s\n", freq_file.c_str()); exit(0); }
        if (ferror(freq_stream)) { fprintf (stderr, "ERROR 2: FILE NOT FOUND %s\n", freq_file.c_str()); exit(0); }

	//fprintf(freq_stream, "#THE LAST COLUMN SHOWS THE TOTAL NUMBER OF MATRICES THAT WERE ALIGNED TO THE POSITION");
	fprintf(freq_stream, "#This file shows for each matrix on how many matrices/consensus sequences it has been constructed.\n");
	fprintf(freq_stream, "#Please keep in mind that the matrices of this file are not given in any proper formatand should not\n");
	fprintf(freq_stream, "#as such be further analyzed. PFM are given in a separate file.\n\n");
	fprintf(freq_stream, "#The first four columns show the number of combined matrices/consensus sequences that had a nucleotide\n");
	fprintf(freq_stream, "#in the corresponding position. In other words, it is the alignment of matrices, in which positive nucleotide\n");
	fprintf(freq_stream, "#counts are marked as one). The last column (|tot: X) is the number matrices/consensus sequences in total combined.\n"); 
	fprintf(freq_stream, "#Examples: If the row is 2,0,0,1|tot: 3, it means that this position of the new matrix was constructed \n");
	fprintf(freq_stream, "#from three matrices/consensus sequences, of which two had A in the position and one had T. If the row is\n");
	fprintf(freq_stream, "#2,0,0,1|tot: 2, it means that this position of the new matrix was constructed from two matrices/consensus\n");
	fprintf(freq_stream, "#sequences, of which one had A and the other one W. If the row is 4,3,4,3|tot: 4 it means that this position\n");
	fprintf(freq_stream, "#of the new matrix was constructed from four matrices/consensus sequences, of which three had N and one had Y.\n");
	fprintf(freq_stream, "#If the row is 2,3,2,3|tot: 4 it means that this position of the new matrix was constructed from four matrices/\n");
	fprintf(freq_stream, "#consensus sequences, of which A) two had N, one had C and one had T, B) one had N, one had Y, one had M and\n");
	fprintf(freq_stream, "#one had K. The last example illustrates that one cannot always figure out the combination by only looking at \n");
	fprintf(freq_stream, "this file. In case, you did not understood me, please contact: matti.kankainen@helsinki.fi\n\n\n"); 

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Calculates the Z-scores by matching two IUPAC vectors against each other

	if(zscore == 1) { 
		Zscore* zscore = new Zscore(tLength);	
		(*zscore).get_zscore(norm0_mean, norm0_std, norm1_mean, norm1_std, norm2_mean, norm2_std,
				     norm3_mean, norm3_std, norm4_mean, norm4_std, norm5_mean, norm5_std);
		delete zscore;
	}

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Read the inputfile 

	file_ok = (*fr).readFile(&file, input_mat_vector, min_length, pseudo_count, genome_fra, genome_frc);
	//fr->readFile(&file, original_mat_vector, min_temp_len);
	//if((file_ok == 0) || (input_mat_vector.size() < 2)) {
	//cerr << maxsize << " " << input_mat_vector.size() << "\n";

	if((file_ok == 0) || (input_mat_vector.size() < 2) || ((maxsize > 0) && (input_mat_vector.size() > maxsize))  ) {
		delete fr;
		delete random_matrix;
		dist_mat.SetSize(1, 1);
		silhouette.SetSize(1, 1);
		clear_memory(dist_mat, silhouette, mat_names, input_mat_vector, one_zero_mat_vector, 
			cluster_results, cluster_info, silhouette_value,
			tree_stream, mtrx_stream, pswm_stream, vara_stream, fdrs_stream, freq_stream);
		cerr << "Error in input file\n";
		if(maxsize > 0) {
			cerr << "Input contains too many patterns/PFM in comparison to maxsize parameter\n";
		}

		return(1);
   	}
	clusters_tot = input_mat_vector.size();

	n_mats = input_mat_vector.size();
	dist_mat.SetSize(n_mats, n_mats);
	silhouette.SetSize(n_mats, n_mats);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Create matrices which each cell has one as the value
	//Elements are deleted after they are printed

	for(int i = 0; i < input_mat_vector.size(); ++i) {
        	i1 = new IntMatrix((*input_mat_vector[i]).X(), 5, "frequencies");
        	i1->initialise(0);

		for(int j = 0; j < (*i1).X(); ++j) {
			int inp_sum(0);
			inp_sum = (*input_mat_vector[i]).g(j,0)+(*input_mat_vector[i]).g(j,1)+(*input_mat_vector[i]).g(j,2)+(*input_mat_vector[i]).g(j,3);

			if((*input_mat_vector[i]).g(j,0) > int(float(inp_sum)*(pseudo_count*(genome_fra/2))) ) { (*i1).s(1,j,0); }
			if((*input_mat_vector[i]).g(j,1) > int(float(inp_sum)*(pseudo_count*(genome_frc/2))) ) { (*i1).s(1,j,1); }
			if((*input_mat_vector[i]).g(j,2) > int(float(inp_sum)*(pseudo_count*(genome_frc/2))) ) { (*i1).s(1,j,2); }
			if((*input_mat_vector[i]).g(j,3) > int(float(inp_sum)*(pseudo_count*(genome_fra/2))) ) { (*i1).s(1,j,3); }
			(*i1).s(1,j,4);
		}
		one_zero_mat_vector.push_back(i1);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Loop that calculates the first pair-wise distance matrix

	for(int i = 0; i < input_mat_vector.size(); ++i) {
		(*input_mat_vector[i]).get_element_name(element1);
		mat_names.push_back(element1);

		consensus = (*input_mat_vector[i]).get_consensus(pseudo_count, genome_fra, genome_frc);

		fltemp = new FlMatrix((*input_mat_vector[i]).X(),4,"frequencies");
		convert_int_to_fl_matrix((input_mat_vector[i]), fltemp);
		(*fltemp).print(element1, consensus, pswm_stream);
		delete fltemp;

		(*one_zero_mat_vector[i]).print_one_zero(element1, consensus, freq_stream);

		for(int j = (i+1); j < input_mat_vector.size(); ++j) {
			tLength = get_tLength(((*input_mat_vector[i]).X()-1), ((*input_mat_vector[j]).X()-1), spacer, 
				  parameter_tlength, min_l, max_l);
			
			calc_alignment_score(input_mat_vector[i], input_mat_vector[j], &p0, 
					     one_zero_mat_vector[i], one_zero_mat_vector[j], &i1, 
					     score, align_cols);
			
			delete (p0);
			delete (i1);

			align_cols = 1;	//comment this, if you wish to normalize scores //
			dist_mat.SetElement(i, j, (score/align_cols));
			dist_mat.SetElement(j, i, (score/align_cols));

			if (i == 0 && j == 1) { temp_max = score; temp_min = score; }
			if (score >= temp_max) { temp_max = score; }
			if (score <= temp_min) { temp_min = score; }
		}

		{
			ClusterInfo this_cluster_info;
			this_cluster_info.SetElement(i, -1);
			cluster_info.push_back(this_cluster_info);
		}

		dist_mat.AddElementSize(1);
		silhouette_value.push_back(0);
	}

	if (random_perm > 0) {
		fdr = (*random_matrix).randomize_gene(random_perm, input_mat_vector, 
						      dist_mat, fdrs_stream);
	}

	dist_mat.PrintMatrixAvg(mtrx_stream);

	if(mode == 0) { 
		delete fr;
		delete random_matrix;
		clear_memory(dist_mat, silhouette, mat_names, input_mat_vector, one_zero_mat_vector, 
			     cluster_results, cluster_info, silhouette_value,
                             tree_stream, mtrx_stream, pswm_stream, vara_stream, fdrs_stream, freq_stream);
		return(1);
	}

	//NORMALIZE SILHOUETTE TABLE//
	normalize_silhouette_matrix(input_mat_vector.size(), temp_min, temp_max, dist_mat, silhouette);
	print_cluster_silhouette(silhouette_value, vara_stream, input_mat_vector.size(), temp_max, clusters_tot);
	//print_cluster_variance(cluster_info, vara_stream);

	//loop that performs hierarchical clustering
	for (unsigned int i = 0; i < (n_mats -1); ++i) {
		sprintf(buffer, "%i", (i+1));
		new_element = ">node_nro" + string(buffer);

		pair_score = dist_mat.FindMax(row_max_dist, col_max_dist);

		for(int j = 0; j < input_mat_vector.size(); ++j) {
			if(j != row_max_dist) {
				score = dist_mat.GetElement(row_max_dist, j) + dist_mat.GetElement(col_max_dist, j);
				dist_mat.SetElement(row_max_dist, j, score);
				dist_mat.SetElement(j, row_max_dist, score);
			}
		}

		dist_mat.MergeElementSize(row_max_dist, col_max_dist);
		dist_mat.RemoveElement(col_max_dist);
		dist_mat.PrintMatrixAvg(mtrx_stream);		

		tLength = get_tLength(((*input_mat_vector[row_max_dist]).X()-1), ((*input_mat_vector[col_max_dist]).X()-1), 
			  spacer, parameter_tlength, min_l, max_l);
		
		calc_alignment_score(input_mat_vector[row_max_dist], input_mat_vector[col_max_dist], &p0, 
				     one_zero_mat_vector[row_max_dist], one_zero_mat_vector[col_max_dist], &i1, 
				     score, align_cols);
		consensus = (*p0).get_consensus(pseudo_count, genome_fra, genome_frc);

		fltemp = new FlMatrix((*p0).X(),4,"frequencies");
		convert_int_to_fl_matrix(p0, fltemp);
		(*fltemp).print(new_element, consensus, pswm_stream);
		delete fltemp;

		(*i1).print_one_zero(new_element, consensus, freq_stream);
		
		delete input_mat_vector[row_max_dist];
		delete input_mat_vector[col_max_dist];
		input_mat_vector[row_max_dist] = p0;
		input_mat_vector.erase(input_mat_vector.begin() + col_max_dist, input_mat_vector.begin() + col_max_dist + 1);

		delete one_zero_mat_vector[row_max_dist];
		delete one_zero_mat_vector[col_max_dist];
		one_zero_mat_vector[row_max_dist] = i1;
		one_zero_mat_vector.erase(one_zero_mat_vector.begin() + col_max_dist, one_zero_mat_vector.begin() + col_max_dist + 1);

		element1 = mat_names[row_max_dist];
		element2 = mat_names[col_max_dist];
		mat_names.erase(mat_names.begin() + col_max_dist, mat_names.begin() + col_max_dist + 1);
		mat_names[row_max_dist] = new_element;

		{
			ClusterResults this_cluster_result;
			this_cluster_result.SetElement(new_element, element1, element2, consensus, pair_score, fdr, (i+1));
			cluster_results.push_back(this_cluster_result);
		}

		if (random_perm > 0) {
			fdr = (*random_matrix).randomize_gene(random_perm, input_mat_vector, dist_mat, fdrs_stream);
		}

		for(unsigned int j = 0; j < cluster_info[col_max_dist].GetSize(); ++j ) {
			cluster_info[row_max_dist].SetElement(cluster_info[col_max_dist].GetElement(j));
		}
		cluster_info.erase(cluster_info.begin() + col_max_dist, cluster_info.begin() + col_max_dist + 1);

		//CALCULATE SILHOUETTE VALUE//
		calculate_silhouette_value(cluster_info, silhouette_value, silhouette, input_mat_vector.size(), pair_score, vara_stream, clusters_tot);

		//CALCULATE MIN, MAX, MEAN AND VARIANCE WITHIN THE CLUSTER WHEN COMPARED TO THE CENTER OF CLUSTER (pT)//
		//cluster_info[row_max_dist].ClearElementStat();
		//for(unsigned int j = 0; j < cluster_info[row_max_dist].GetSize(); ++j ) {
		//	pT = calc_alignment_score(p0, original_mat_vector[cluster_info[row_max_dist].GetElement(j)], score, align_cols);
		//	cluster_info[row_max_dist].SetElementStat(score, score);
		//}
		//print_cluster_variance(cluster_info, vara_stream);
	}

	unsigned int cluster_num = 0;
	string unique_depth_string;
	for(unsigned int i = 0; i < cluster_results.size(); ++i) {
		cluster_results[i].GetElement(new_element, element1, element2, consensus, pair_score, fdr, cluster_num);

		char buffer [50];
		cluster_num = clusters_tot - cluster_num;
		if(cluster_num >= 0 && cluster_num < 10) {
                	sprintf(buffer, "%.5f0000%u", pair_score, cluster_num);
		} else if(cluster_num >= 10 && cluster_num < 100) {
			sprintf(buffer, "%.5f000%u", pair_score, cluster_num);
		} else if(cluster_num >= 100 && cluster_num < 1000) {
			sprintf(buffer, "%.5f00%u", pair_score, cluster_num);
		} else if(cluster_num >= 1000 && cluster_num < 10000) {
			sprintf(buffer, "%.5f0%u", pair_score, cluster_num);
		} else {
			sprintf(buffer, "%.5f%u", pair_score, cluster_num);
		}

		unique_depth_string =  buffer;
		//fprintf (outstream, "%u\t%.10f\t%s\n", cluster_number, silhouette_temp, unique_depth_string.c_str());
		//fprintf (outstream, "%u\t%.10f\t%.5f\n", cluster_number, silhouette_temp, depth );

		if(random_perm > 0) {
			fprintf(tree_stream, "%s\t%s\t%s\t%s\t%.4f\t%s\n", new_element.c_str(), element1.c_str(), element2.c_str(), 
				unique_depth_string.c_str(), fdr, consensus.c_str());
		} else {
			fprintf(tree_stream, "%s\t%s\t%s\t%s\tnull\t%s\n", new_element.c_str(), element1.c_str(), element2.c_str(),
				unique_depth_string.c_str(), consensus.c_str());
		}
	}

	delete fr;
	delete random_matrix;
	clear_memory(dist_mat, silhouette, mat_names, input_mat_vector, one_zero_mat_vector, 
		     cluster_results, cluster_info, silhouette_value,
		     tree_stream, mtrx_stream, pswm_stream, vara_stream, fdrs_stream, freq_stream);
	return(1);
};
//*********************************************************************************************************************//
//close me
void clear_memory (DistanceMatrix& dist_mat, DistanceMatrix& silhouette, vector<string>& mat_names,
                   vector<IntMatrix*>& input_mat_vector, vector<IntMatrix*>& one_zero_mat, 
		   vector<ClusterResults>& cluster_results, vector<ClusterInfo>& cluster_info, vector<float>&
                   silhouette_value, FILE* tree_stream,  FILE* mtrx_stream, FILE* pswm_stream, FILE* vara_stream, 
		   FILE* fdrs_stream, FILE* freq_stream) {
	
	for(unsigned int i = 0; i < input_mat_vector.size(); ++i) {
		delete input_mat_vector[i];
	}

	for(unsigned int i = 0; i < one_zero_mat.size(); ++i) {
		delete one_zero_mat[i];
	}

	dist_mat.FreeMemory();
	silhouette.FreeMemory();

	fclose(tree_stream);
	fclose(mtrx_stream);
	fclose(pswm_stream);
	fclose(vara_stream);
	fclose(fdrs_stream);
	fclose(freq_stream);

	input_mat_vector.clear();
	cluster_results.clear();
	cluster_info.clear();
	silhouette_value.clear();
};
//*********************************************************************************************************************//
//normalize silhouette matrix
void normalize_silhouette_matrix(int input_size, float temp_min, float temp_max, DistanceMatrix& dist_mat, DistanceMatrix& slh_dist) {
	float temp_score(0);

	for(int i = 0; i < input_size; ++i) {
		for(int j = (i+0); j < input_size; ++j) { // +0 allow matches to itself, +1 do not match itself (does not matter, as normalization factor)
			temp_score = dist_mat.GetElementAvg(i, j);
			temp_score = (temp_score - temp_min)/(temp_max - temp_min); // 1 = max/perfect correlation; 0 = min/no correlation
			temp_score = (1 - temp_score); 				    // 1 - 1 (max) = 0 (perfect corr); 1 - 0 (min) = 1 (no corr)

			slh_dist.SetElement(j, i, temp_score);
			slh_dist.SetElement(i, j, temp_score);
		}
	}
}
//*********************************************************************************************************************//
//calculate_silhouette_value(cluster_info, silhouette_value, silhouette, input_mat_vector.size(), pair_score, vara_stream);
//silhouettes demands that distances goes from small to big, in here this is achieved by normalizing the z-score table 
//small (0) must indicate that two elements are identical whereas inf means that they are different (i.e. must be like euc-distance)
//however this might only work well when the initial matrix contains enough many elements

int calculate_silhouette_value(vector<ClusterInfo>& clst_inf, vector<float>& silhouette_vector, DistanceMatrix& slh_dist, 
			       unsigned long cluster_number, float pair_score, FILE *outstream, unsigned long clusters_tot) {
	float a1(0), b1(0), b1t(0);	


	for(unsigned int j = 0; j < clst_inf.size(); ++j ) {
		for(unsigned int k = 0; k < clst_inf[j].GetSize(); ++k ) {
			a1 = 0;
			for(unsigned int m = 0; m < clst_inf[j].GetSize(); ++m ) {
				if (k != m) {
					a1 = a1 + slh_dist.GetElement(clst_inf[j].GetElement(k), clst_inf[j].GetElement(m));
				}
			}
			//an average distance of the element and of the rest of the elements of the cluster
			a1 = (a1 / clst_inf[j].GetSize());

			b1 = HUGE_VAL;
			//loop all clusters, but DONOT choose the j, which is the current cluster
			for(unsigned int m = 0; m < clst_inf.size(); ++m ) {
				if (m != j) {
					b1t = 0;
					for(unsigned int n = 0; n < clst_inf[m].GetSize(); ++n ) {
						b1t = b1t + slh_dist.GetElement(clst_inf[j].GetElement(k), clst_inf[m].GetElement(n));
					}
					b1t = (b1t / clst_inf[m].GetSize());
					//if b1t (current cluster) is closer, save the value
					if (b1t <=  b1) { b1 = b1t; }
				}
			}
			
			if ((a1 == 0) && (b1 == 0)) {
				silhouette_vector[(clst_inf[j].GetElement(k))] = 0;
			} else if (clst_inf[j].GetSize() == 1) {
				silhouette_vector[(clst_inf[j].GetElement(k))] = 0;
			} else if (a1 >= b1) {
				silhouette_vector[(clst_inf[j].GetElement(k))] = ((b1 - a1) / a1);
			} else {
				silhouette_vector[(clst_inf[j].GetElement(k))] = ((b1 - a1) / b1);
			}
		}
	}
	print_cluster_silhouette(silhouette_vector, outstream, cluster_number, pair_score, clusters_tot);
	return(1);
};
//*********************************************************************************************************************//
int get_tLength(int length_pat1, int length_pat2, int spacer, int parameter, int &min, int &max) {
	int length(0);      
	int length_diff(0);

	if (length_pat1 >= length_pat2) { length_diff = length_pat1 - 1; min = length_pat2; max = length_pat1; }
	if (length_pat1 <  length_pat2) { length_diff = length_pat2 - 1; min = length_pat1; max = length_pat2; }

	if((spacer == 1) && (parameter > length_diff)) {
		length = length_diff;
	} else if ((spacer == 1) && (parameter <= length_diff)) {
		length = parameter;
	} else if (spacer == 0) {
		length = length_diff;
	} else {
		length = 0;
	}

	//cout << length_diff << " " << parameter << " " << length << "\n";
	return(length);
};
//*********************************************************************************************************************//
void print_cluster_silhouette(vector<float>& silhouette_value, FILE *outstream, unsigned long cluster_number, float depth, unsigned long cluster_tot) {
	float silhouette_temp(0);
	unsigned long cluster_temp(0);
	string unique_depth_string;
	char buffer [50];

	for(unsigned int j = 0; j < silhouette_value.size(); ++j ) {
		silhouette_temp = silhouette_temp + silhouette_value[j];
		//fprintf (outstream, "%.3f ",  silhouette_value[j] );
	}
	//fprintf (outstream, "\n\n");	

	silhouette_temp = silhouette_temp / silhouette_value.size();
	if (cluster_number == 1) { silhouette_temp = -1.00; }

	cluster_temp = cluster_tot - cluster_number;
	if(cluster_temp >= 0 && cluster_temp < 10) {
		sprintf(buffer, "%.5f0000%u", depth, cluster_temp);
	} else if(cluster_temp >= 10 && cluster_temp < 100) {
		sprintf(buffer, "%.5f000%u", depth, cluster_temp);
	} else if(cluster_temp >= 100 && cluster_temp < 1000) {
		sprintf(buffer, "%.5f00%u", depth, cluster_temp);
	} else if(cluster_temp >= 1000 && cluster_temp < 10000) {
		sprintf(buffer, "%.5f0%u", depth, cluster_temp);
	} else {
		sprintf(buffer, "%.5f%u", depth, cluster_temp);
	}

	unique_depth_string =  buffer;
	fprintf (outstream, "%u\t%.10f\t%s\n", cluster_number, silhouette_temp, unique_depth_string.c_str());
	//fprintf (outstream, "%u\t%.10f\t%.5f\n", cluster_number, silhouette_temp, depth );
};
//*********************************************************************************************************************//
void print_cluster_variance(vector<ClusterInfo>& cluster_info, FILE *outstream) {
	float cluster_min(0), cluster_max(0), cluster_mean(0), cluster_var(0);
	float cluster_mean_tot(0), cluster_var_tot(0);
	unsigned long cluster_size(0);

	for(unsigned int i = 0; i < cluster_info.size(); ++i) {
		cluster_info[i].CalcElementStat(cluster_min, cluster_max, cluster_mean, cluster_var, cluster_size);
		//cluster_info[i].PrintElement(outstream, 1);
		cluster_mean_tot = cluster_mean_tot + cluster_mean;
		cluster_var_tot = cluster_var_tot + cluster_var;
	}
	
	fprintf (outstream, "%u\t%.3e\t%.3e\n", cluster_info.size(), cluster_mean_tot, cluster_var_tot );	
};
//*********************************************************************************************************************//
void convert_int_to_fl_matrix (IntMatrix* i1, FlMatrix* f1) {
	float sum;
	int i,j;
	FOR(i,i1->X()){
		sum = 0;
		FOR(j,4) {
			f1->s(i1->g(i,j),i,j);
			sum+=i1->g(i,j);
		}
		FOR(j,4) {
			f1->d(sum,i,j);
		}
	}
}
//*********************************************************************************************************************//
void calc_alignment_score (IntMatrix* c1, IntMatrix* c2, IntMatrix** r1, IntMatrix* i1, IntMatrix* i2, IntMatrix** r2, float &score, float &align_cols) {
	FlMatrix* m1;
	FlMatrix* m2;
	IntMatrix* path;
	Viterbi* vit;

	int i,j;
	float sum;

	m1 = new FlMatrix(c1->X(),4,"frequencies");
	m1->initialise(0);
	convert_int_to_fl_matrix(c1, m1);

	m2 = new FlMatrix(c2->X(),4,"frequencies");
	m2->initialise(0);
	convert_int_to_fl_matrix(c2, m2);

	//m2->print();
	
	vit = new Viterbi(m1,m2);
	path = new IntMatrix(m1->X()+m2->X(),"path");
	path->initialise(-1);
	vit->align(path, &score);

	int l=1,k,m;
	FOR(i,path->X()){
		if(path->g(i)>=0)
			l++;
	}
	
	*r1 = new IntMatrix(l, 4, "parent");
	(**r1).initialise(0);
	*r2 = new IntMatrix(l, 5, "parent");
	(**r2).initialise(0);

	j = k = l = 1;
	align_cols = 0;
	RFOR(i,path->X()-1){
		if(path->g(i)<0) {
			continue;
		} else if(path->g(i)==0 || path->g(i)==3) {
			FOR(m,4) {
				(**r1).a(c1->g(l,m),j,m);
				(**r1).a(c2->g(k,m),j,m);

				(**r2).a(i1->g(l,m),j,m);
				(**r2).a(i2->g(k,m),j,m);
			}
		
			(**r2).a(i1->g(l,4),j,4);
			(**r2).a(i2->g(k,4),j,4);

			j++; l++; k++; 
			align_cols++;
		} else if(path->g(i)==1) {
			FOR(m,4) {
				(**r1).a(c1->g(l,m),j,m);
				
				(**r2).a(i1->g(l,m),j,m);
			}
			(**r2).a(i1->g(l,4),j,4);
	
			j++; l++;
		} else if(path->g(i)==2) {
			FOR(m,4) {
				(**r1).a(c2->g(k,m),j,m);
				
				(**r2).a(i2->g(k,m),j,m);
			}
			(**r2).a(i2->g(k,4),j,4);

			j++; k++;
		}
	}

	if(noise>0) {
		j = k = l = 1;
		string alpha = "ACGT";
		cout<<"Alignment"<<endl;
		RFOR(i,path->X()-1){
			if(path->g(i)<0) {
				continue;
			} else if(path->g(i)==0 || path->g(i)==3) {
				//C1=TRUE 	C2=TRUE
				FOR(j,4)
					cout<<c1->g(l,j)<<" ";
				cout<<";";
				FOR(j,4)
					cout<<" "<<c2->g(k,j);
				cout<<endl;
				l++; k++;
			} else if(path->g(i)==1) {
				//C1=TRUE 	C2=FALSE
				FOR(j,4)
					cout<<c1->g(l,j)<<" ";
				cout<<"; - - - -"<<endl;
				l++;
			} else if(path->g(i)==2) {
				//C1=FALSE 	C2=TRUE
				cout<<"- - - - ;";
				FOR(j,4)
					cout<<" "<<c2->g(k,j);
				cout<<endl;
				k++;
			}
		}	
		cout<<endl<<"SCORE:"<<score<<endl;

		cout<<endl<<"Matrix"<<endl;
		for(i=1; i < (**r1).X();i++) {
			FOR(j,4){
				cout<<(**r1).g(i,j)<<" ";
			}
			cout<<endl;
		}
	}

	delete m1;
	delete m2;
	delete path;
	delete vit;
}
//*********************************************************************************************************************//

