/***************************************************************************
 *   Copyright (C) 2007 by Ari Loytynoja and Matti Kankainen               *
 *   ari@ebi.ac.uk / matti.kankainen@helsinki.fi                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef clust_info_h
#define clust_info_h

#include <vector>

class ClusterInfo {
public:
	void PrintElement(FILE*, bool prnt_elements = 0);
	void ClearElementStat();
	void CalcElementStat(float&, float&, float&, float&, unsigned long&);
	void SetElementStat(float, float);
	void SetElement(unsigned long, long element = -1);
	unsigned long GetElement(unsigned long);
	unsigned long GetSize();

	ClusterInfo();
	~ClusterInfo();
private:
	std::vector <unsigned long> cluster_elements;
	float min;
	float max;
	float sum;
	float var;
	float temp_sum;

	float calc_sum;
	float calc_var;
	float calc_min;
	float calc_max;
};

#endif
