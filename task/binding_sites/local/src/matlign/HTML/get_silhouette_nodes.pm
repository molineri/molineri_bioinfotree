#!/afs/bi/p/bin/perl
use strict;
use warnings;
use Getopt::Long;

###################################################################################################
#Get commandline options

my %opt = ();
my %legend = (
	"input=s"		=>	"prefix of the input files",
	"silh=s"		=>	"print either silhouette cluster or all",
	"fdr=s"			=>	"cutoff value to choose patterns to be printed",
);

GetOptions(\%opt, "help!", keys %legend);

my($input) 	         					= "";
my($fdr)							= "null";
my($silh)							= 1;
my($runmain)							= 1;

if (exists($opt{'input'}))		{ $input	  	= $opt{'input'};		} else { $runmain = 0; }
if (exists($opt{'fdr'}))		{ $fdr			= $opt{'fdr'};			}
if (exists($opt{'silh'}))		{ $silh			= $opt{'silh'};			}

if(($runmain == 0) || (scalar(keys(%opt)) == 0) || (exists($opt{h})) || (exists($opt{help})) ) {
	print STDERR "USAGE perl $0 --input --fdr --silh\n";
	print STDERR "STDOUT is used as an output\n\n";
	print STDERR "input  = input file prefix\n";
	print STDERR "fdr    = fdr-cutoff (default is null)\n";
	print STDERR "silh   = print silhouette nodes (1=default) (0 = get \'best\' pairwise nodes | 1 = get silhouette nodes)\n";
} else {
	&main($input, $silh, $fdr);
}

exit;
###################################################################################################
#Main program that calls everything else (no params)
sub main {
        my($input_prefix, $silhouette, $fdr_cutoff)						= @_;
	my(%exp_tree)										= ();	
	my(%fdrok)										= ();
	my(%fdrfalse)										= ();
	my(%pattern_headers)									= ();

	my($root_ypos)										= 0;
	my($root)										= "";
	my($uppery)										= 0;
	my($lowery)										= 0;
	my($max_silhouette)									= 0;
	my($silhouette_depth)									= 0;
	my($number_of_clusters)									= 0;
	my($maxnode)										= 0;

	if($silhouette == 1) {
		($max_silhouette, $silhouette_depth, $number_of_clusters, $maxnode)	= &FindMaxValue("$input_prefix\.vara");
		($uppery, $lowery)							= &ReadTreeInput("$input_prefix\.tree", \%exp_tree, ($maxnode+1));
		($root, $root_ypos)							= &FindRoot(\%exp_tree);

		&CheckSilh($root, $lowery, \%exp_tree, $silhouette_depth, $max_silhouette, $fdr_cutoff, \%fdrok, \%fdrfalse);
		&GetPatterns("$input_prefix\.pswm", \%pattern_headers);

		print "#accepted nodes\n";
		for my $key (sort (keys %fdrok)) {
			print "$key\t$fdrok{$key}\t$pattern_headers{$key}\n";
		}
		print "\n##########################\n";
		print "#rejected nodes\n";
		for my $key (sort (keys %fdrfalse)) {
			print "$key\t$fdrfalse{$key}\t$pattern_headers{$key}\n";
		}		
	} else {
		&GetBestMatches("$input_prefix\.pswm", "$input_prefix\.mtrx", "$input_prefix\.fdrs", $fdr_cutoff, \%fdrok, \%fdrfalse);
		&GetPatterns("$input_prefix\.pswm", \%pattern_headers);

		print "#accepted nodes\n";
		for my $key (sort (keys %fdrok)) {
			for my $key2 (sort (keys %{$fdrok{$key}} )) {
				print "$key\t$pattern_headers{$key}\t$key2\t$pattern_headers{$key2}\t";	
				print "$fdrok{$key}{$key2}{'score'}\t$fdrok{$key}{$key2}{'fdr'}\n";
				#$pattern_headers{$key}\t$match\t$pattern_headers{$match}\t$fdrok{$key}{'score'}\n";
			}
		}
		print "\n##########################\n";
		print "#rejected nodes\n";
		for my $key (sort (keys %fdrfalse)) {
			for my $key2 (sort (keys %{$fdrfalse{$key}} )) {
				print "$key\t$pattern_headers{$key}\t$key2\t$pattern_headers{$key2}\t";
				print "$fdrfalse{$key}{$key2}{'score'}\t$fdrfalse{$key}{$key2}{'fdr'}\n";
				#print "$key\t$key2\t$fdrfalse{$key}{$key2}{'score'}\t$fdrfalse{$key}{$key2}{'fdr'}\n";
			}
		}	
	}
};
###################################################################################################
#get best patterns
sub GetBestMatches {
	my($pswm, $mtrx, $fdrs, $fdrcut, $fdrok, $fdrfalse)						= @_;
	my(@cols)											= ();
	my(@mat_cols)											= (0);
	my(@fdr_cols)											= (0);
	my($line)											= "";
	my(@header)											= ();
	my($max)											= 0;
	my($row)											= 0;
	my($is_first)											= 1;
	
	open(PSWM, $pswm);
	while($line = <PSWM>) {
		$line =~ s/^\s+|\s+$//g;
		if($line =~ m/^>/) {
			push(@header, $line);
		}
	}
	close PSWM;

	open(MTRX, $mtrx);
	open(FDRS, $fdrs);
	$row = 0;
	while(scalar(@mat_cols) > 0) {
		$line = <MTRX>;
		$line =~ s/^\s+|\s+$//g;
		@cols = split(/\|/, $line);
		@mat_cols = ();
		for(my($i) = 0; $i < @cols; ++$i) {
			$cols[$i] =~  s/^\s+|\s+$//g;
			if(length($cols[$i]) > 0) {
				push(@mat_cols, $cols[$i]);
			}
		}

		$line = <FDRS>;
		$line =~ s/^\s+|\s+$//g;
		@cols = split(/\|/, $line);
		@fdr_cols = ();
		for(my($i) = 0; $i < @cols; ++$i) {
			$cols[$i] =~  s/^\s+|\s+$//g;
			if(length($cols[$i]) > 0) {
				push(@fdr_cols, $cols[$i]);
			}
		}

		$max 		= 0;
		$is_first	= 1;
		if(scalar(@mat_cols) > 0) {
			for(my($i) = 0; $i < @mat_cols; ++$i) {
				if( (!($mat_cols[$i] =~ m/inf/)) ) {
					if($is_first == 1) { 
						$max		= $mat_cols[$i]; 
						$is_first	= 0;
					}
					if($mat_cols[$i] >= $max) { 
						$max		= $mat_cols[$i];
					}
				}
			}
			
			for(my($i) = 0; $i < @mat_cols; ++$i) {
				if( (!($mat_cols[$i] =~ m/inf/)) ) {
					if($mat_cols[$i] >= $max) {
						if($fdr_cols[$i] <= $fdrcut) {
							${$fdrok}{$header[$row]}{$header[$i]}{'score'}		= $max;
							${$fdrok}{$header[$row]}{$header[$i]}{'fdr'}		= $fdr_cols[$i];
						} else {
							${$fdrfalse}{$header[$row]}{$header[$i]}{'score'}	= $max;
							${$fdrfalse}{$header[$row]}{$header[$i]}{'fdr'}		= $fdr_cols[$i];
						}
					}
				}
			}
		}
		++$row;
	}
	close MTRX;
	close FDRS;
};
###################################################################################################
#get patterns
sub GetPatterns { 
	my($file, $pat_ptr)										= @_;
	my($line)											= "";
	my($header)											= "";
	my($consensus)											= "";

	open(PSWM, $file);
	while($line = <PSWM>) {
		$line =~ s/^\s+|\s+$//g;
		if($line =~ m/^>/) {
			$header = $line;
		}
		if($line =~ m/^#/) {
			$consensus = $line;
			$consensus =~ s/^#consensus:\s+//;
			${$pat_ptr}{$header} = $consensus;
			$header 	= "";
			$consensus	= "";
		}
	}
	close PSWM;
};
###################################################################################################
#Writes coordinates for Gene expression tree
sub CheckSilh {
        my($oldnode, $ypos, $exp_ptr, $silhouette_depth, $max_silhouette, $fdrcut, $fdrok, $fdrfalse)	= @_;
        my($xpos1)                                                                                      = 0;
        my($xpos2)                                                                                      = 0;
        my($oldy)                                                                                       = 0;
        my($geneid)                                                                                     = "";
        my($xposition)                                                                                  = 0;

        if(exists(${$exp_ptr}{$oldnode}{'yposition'})) { $oldy = ${$exp_ptr}{$oldnode}{'yposition'}; }

        if(exists(${$exp_ptr}{$oldnode}{'l_child'})) {
                my($l_child) = ${$exp_ptr}{$oldnode}{'l_child'};
                ($xpos1) = &CheckSilh($l_child, $oldy, $exp_ptr, $silhouette_depth, $max_silhouette, $fdrcut, $fdrok, $fdrfalse);
        } if(exists(${$exp_ptr}{$oldnode}{'r_child'})) {
                my($r_child) = ${$exp_ptr}{$oldnode}{'r_child'};
                ($xpos2) = &CheckSilh($r_child, $oldy, $exp_ptr, $silhouette_depth, $max_silhouette, $fdrcut, $fdrok, $fdrfalse);
        }

        #For labels and for special dots use xposition and oldy
        if(exists(${$exp_ptr}{$oldnode}{'id'})) 	{ $geneid = ${$exp_ptr}{$oldnode}{'id'}; }
        if(exists(${$exp_ptr}{$oldnode}{'xposition'})) 	{ $xposition = ${$exp_ptr}{$oldnode}{'xposition'}; }

        if (${$exp_ptr}{$oldnode}{'is_gene'} != 1) {
                $geneid = "";
        }

        #$oldy down (high), $ypos up (low)#
        if(($ypos < $silhouette_depth) && ($silhouette_depth <= $oldy)) {
		if($fdrcut =~ m/null/) {
			#print "$oldnode\t${$exp_ptr}{$oldnode}{'fdr'}\t${$exp_ptr}{$oldnode}{'id'}\n";
			${$fdrok}{$oldnode} = "${$exp_ptr}{$oldnode}{'fdr'}";
		} else {
			if(${$exp_ptr}{$oldnode}{'fdr'} =~ m/NaN/) {
				${$fdrok}{$oldnode} = "${$exp_ptr}{$oldnode}{'fdr'}";
			} elsif(${$exp_ptr}{$oldnode}{'fdr'} <= $fdrcut) {
				#print "$oldnode\t${$exp_ptr}{$oldnode}{'fdr'}\t${$exp_ptr}{$oldnode}{'id'}\n";
				${$fdrok}{$oldnode} = "${$exp_ptr}{$oldnode}{'fdr'}";
			} else {
				${$fdrfalse}{$oldnode} = "${$exp_ptr}{$oldnode}{'fdr'}";
			}
		}
	}
        return($xposition);
};
###################################################################################################
#Reads the gtr (gene tree) file
sub ReadTreeInput {
        my($gtr_file, $exp_ptr, $uppery)			                       = @_;
        my(@gtrdata)                                                                    = ();
        my(@rows)                                                                       = ();
        my($line)                                                                       = "";
        my($root)                                                                       = "";
        my($node)                                                                       = "";
        my($l_child)                                                                    = "";
        my($r_child)                                                                    = "";
        my($yposition)                                                                  = 0;
        my($checkline)                                                                  = "";
        my($node_consensus)                                                             = "";
        my($fdr)                                                                        = 0;

	my($is_first)									= 1;
        my($min_yposition)                                                              = 0;
        my($max_yposition)                                                              = 0;

        open (GTR, $gtr_file) or die "gtr-file $gtr_file was not found\n";
        @gtrdata = <GTR>;
        close GTR;

        #Check that the format is correct#
        $checkline = $gtrdata[0];
        $checkline =~ s/^\s*|\s*$//i;
        @rows = split(/\s+/, $checkline);
        if (@rows < 3) { die "gtr-file must have at least tree columns\n"; }

        while (@gtrdata > 0) {
                $node = ""; $l_child = ""; $r_child = ""; $yposition = 0;

                $line = shift(@gtrdata);
                $line =~ s/^\s*|\s*$//i;
                @rows = split(/\s+/, $line);

                if ((length($line) > 0) && (scalar(@rows) >= 2)) {
                        $node    = $rows[0];
                        $l_child = $rows[1];
                        $r_child = $rows[2];
                        if (@rows > 3) { $yposition = $rows[3]; }
                        else { $yposition = 0; }                                        #Correlation row is not obligatory
                        if (@rows > 4) { $fdr = $rows[4]; }
                        else { $fdr = "null"; }

                        if (!(exists(${$exp_ptr}{$node}{'mother'}))) {${$exp_ptr}{$node}{'mother'} = 'root'; }

                        ${$exp_ptr}{$node}{'l_child'}           = $l_child;             #Travels down   (ROOT --> LEAF)
                        ${$exp_ptr}{$node}{'r_child'}           = $r_child;             #Travels down   (ROOT --> LEAF)
                        ${$exp_ptr}{$node}{'yposition'}         = ($yposition);         #Positio in the tree
                        ${$exp_ptr}{$node}{'is_gene'}           = 0;                    #Is not gene
                        ${$exp_ptr}{$node}{'fdr'}               = $fdr;
                        ${$exp_ptr}{$l_child}{'mother'}         = $node;                #Travels up     (LEAF --> ROOT)
                        ${$exp_ptr}{$r_child}{'mother'}         = $node;                #Travels up     (LEAF --> ROOT)

			if($is_first == 1) {
				$max_yposition = $yposition;
				$min_yposition = $yposition;
				$is_first = 0;
			}

                        if ($yposition >= $max_yposition) { $max_yposition = $yposition; }
                        if ($yposition <= $min_yposition) { $min_yposition = $yposition; }

                        if(!($l_child =~ m/node/i)) {
                                my($gtr_id)                             = $l_child;

                                ${$exp_ptr}{$l_child}{'yposition'}      = $uppery;      	#Position in the tree picture
				${$exp_ptr}{$l_child}{'fdr'}		= "NaN";
                                ${$exp_ptr}{$l_child}{'is_gene'}        = 1;            	#Flag to check, is the node is gene
                                ${$exp_ptr}{$l_child}{'genes'}          = 1;
                        } if(!($r_child =~ m/node/i)) {
                                my($gtr_id)                             = $r_child;

                                ${$exp_ptr}{$r_child}{'yposition'}      = $uppery;
				${$exp_ptr}{$r_child}{'fdr'}		= "NaN";
                                ${$exp_ptr}{$r_child}{'is_gene'}        = 1;
                                ${$exp_ptr}{$r_child}{'genes'}          = 1;
                        }
                }        
	}

        $max_yposition = ($max_yposition + (($max_yposition - $min_yposition) * 0.01));
        $min_yposition = ($min_yposition - (($max_yposition - $min_yposition) * 0.01));
        return($max_yposition, $min_yposition);
};
###################################################################################################
#Check file existing
sub FindMaxValue {
        my($input_file)                                                                         = @_;
        my(@data)                                                                               = ();
        my(@columns)                                                                            = ();
        my($line)                                                                               = "";
        my($max_value)                                                                          = 0;
        my($return_value_clust)                                                                 = 0;
        my($return_value_depth)                                                                 = 0;
        my($is_first)                                                                           = 1;
        my($tot_clust)                                                                          = 0;
	my($max_node)										= 0;

        open(DATA, $input_file);
        @data = <DATA>;
        close DATA;

        while(@data > 0) {
                $line = shift(@data);
                $line =~ s/^\s+|\s+$//g;
                if(length($line) > 0) {
                        @columns = split(/\s/, $line);
                        if(@columns >= 1) {
                                if($is_first == 1) {
                                        $max_value      = $columns[1];
                                        $is_first       = 0;
                                        $tot_clust      = $columns[0];
					$max_node	= $columns[2];			
                                }
                                if($columns[1] >= $max_value) {
                                        $max_value              = $columns[1];
                                        $return_value_clust     = $columns[0];
                                        $return_value_depth     = $columns[2];
				}
				if($columns[2] >= $max_node) {
					$max_node	= $columns[2];
				}
                        }
                }
        }
        return($return_value_clust, $return_value_depth, $tot_clust, $max_node);
};
###################################################################################################
#Finds the root of exp_tree and returns it
sub FindRoot {
        my($exp_ptr)                                                            = @_;
        my($root)                                                               = "";
        my($root_ypos)                                                          = 0;

        for my $node(keys(%{$exp_ptr})) {
                if (${$exp_ptr}{$node}{'mother'} eq 'root') { $root = $node; }
        }

        if(exists(${$exp_ptr}{$root}{'yposition'})) { $root_ypos = ${$exp_ptr}{$root}{'yposition'};  }
        return($root, $root_ypos);
};
###################################################################################################


