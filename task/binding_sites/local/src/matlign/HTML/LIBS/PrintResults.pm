use strict;
use warnings;


###################################################################################################
#Interface
sub PrintBestpats {
	my($input_prefix_pats, $input_prefix_mtrx, $input_prefix_fdrs, $fdr_cutoff)		= @_;
	my(@data)										= ();
	my(@pats)										= ();
	my(@seqs)										= ();
	my(@temp_pats)										= ();
	my(@temp_fdrs)										= ();
	my(@cols)										= ();
	my(@cols_fdrs)										= ();
	my($line)										= "";
	my($max)										= 0;
	my($maxcol)										= 0;
	my(%bestpats)										= ();
	my($pats_size)										= 0;
	my($hit)										= "";
	my($con_pattern)									= "";
	my($button) = "<INPUT TYPE=\"button\" name=\"b";
	my($counter)										= 0;

	open(DATA, "$input_prefix_pats") or die "output file pats $input_prefix_pats was not found";
	@temp_pats = <DATA>;
	close DATA;

	while (@temp_pats > 0) {
                $line = shift(@temp_pats);
		if($line =~ m/^>/) {		
			push(@pats, $line);
		} elsif($line =~ m/consensus/i) {
			$line =~ s/^#consensus:\s+//i;
			push(@seqs, $line);
		}
	}

	open(DATA, "$input_prefix_fdrs") or die "output file pats $input_prefix_fdrs was not found";
	@temp_fdrs = <DATA>;
	close DATA;

	open(DATA, "$input_prefix_mtrx") or die "output file mtrx $input_prefix_mtrx was not found";
	@data = <DATA>;
	close DATA;

	for(my($i) = 0; $i < @data; ++$i) {
		if(@temp_fdrs > $i) {
			$line = $temp_fdrs[$i];
			$line =~ s/^\s+|\s+$//g;
			$line =~ s/^\|+|\|+$//g;
			@cols_fdrs = split(/\|/, $line);
		}

		$line = $data[$i];
		$line =~ s/^\s+|\s+$//g;
		$line =~ s/^\|+|\|+$//g;
		@cols = split(/\|/, $line);

		if(length($line) > 0) {
			$cols[0] =~ s/\+//i; 
			$max = $cols[0];
			$maxcol = -1;
			for(my($j) = 0; $j < @cols; ++$j) {
				$cols[$j] =~ s/\+//i;
				if($cols[$j] > $max) { $max = $cols[$j]; $maxcol = $j; }
			}

			$counter = 0;

			if(@cols_fdrs > 0) {
				for(my($j) = 0; $j < @cols_fdrs; ++$j) {
					if(($cols_fdrs[$j] <= $fdr_cutoff) && ($j != $i)) {
						$bestpats{$pats[$i]}{$counter}{'best_pat'}	= $pats[$j];
						$bestpats{$pats[$i]}{$counter}{'score'}		= $cols[$j];
						$bestpats{$pats[$i]}{$counter}{'sequence'}	= $seqs[$i];	
						$bestpats{$pats[$i]}{$counter}{'row_num'}	= $i;
						$bestpats{$pats[$i]}{$counter}{'fdr'}		= $cols_fdrs[$j];
						++$counter;
					}
				}
			} else {
				for(my($j) = 0; $j < @cols; ++$j) {
					if($cols[$j] ==  $max) {
						$bestpats{$pats[$i]}{$counter}{'best_pat'} 	= $pats[$j];
						$bestpats{$pats[$i]}{$counter}{'score'} 	= $cols[$j];
						$bestpats{$pats[$i]}{$counter}{'sequence'} 	= $seqs[$i];
						$bestpats{$pats[$i]}{$counter}{'row_num'}	= $i;
						$bestpats{$pats[$i]}{$counter}{'fdr'}		= "null";
						++$counter;
					}
				}
			}
			#if($counter > 0) { $bestpats{$pats[$i]}{'counter'}{'counter'} = $counter; }
		}
	}

	#print HTML "".&tablestart(0, 1, 100, 3)."\nBEST MATCHES:\n</td></tr>\n";

	for my $pat_name (sort { $bestpats{$a}{'0'}{'row_num'} <=> $bestpats{$b}{'0'}{'row_num'}; } (keys( %bestpats ))) {
		my($first) = 0;
		
		for my $ord( sort { $bestpats{$pat_name}{$b}{'score'} <=> $bestpats{$pat_name}{$a}{'score'}; } (keys( %{$bestpats{$pat_name}} ))) {
			if(exists($bestpats{$pat_name}{$ord}{'score'})) {
				$hit = $bestpats{$pat_name}{$ord}{'best_pat'};

				if($first == 0) {
					print HTML "".&tablestart(0, 1, 100, 3)."$pat_name vs. $bestpats{$pat_name}{$ord}{'best_pat'}"; 
					print HTML "</b></td></tr><tr bgcolor ='#B0C4DE'\n".&td(100, 1)."";
					print HTML "<b>Score = $bestpats{$pat_name}{$ord}{'score'}, ";
					print HTML "FDR = $bestpats{$pat_name}{$ord}{'fdr'} </b></td></tr>\n";
					++$first;
				} else 	{ 
					print HTML "<tr bgcolor ='#B0C4DE'>\n";
					print HTML "".&td(100, 1)."<b>$pat_name vs. $bestpats{$pat_name}{$ord}{'best_pat'}";
					print HTML "</td></tr><tr bgcolor ='#B0C4DE'\n".&td(100, 1)."";
					print HTML "<b>Score = $bestpats{$pat_name}{$ord}{'score'}, ";
					print HTML "FDR = $bestpats{$pat_name}{$ord}{'fdr'} </b></td></tr>\n";
				}
	
				for(my($i) = 1; $i < 3; ++$i) {
					if($i == 1) { $con_pattern = $bestpats{$pat_name}{$ord}{'sequence'}; }
					if($i == 2) { $con_pattern = $bestpats{$hit}{0}{'sequence'}; }
					$con_pattern =~ s/^\s+|\s+$//g;
	
					print HTML "<tr bgcolor ='#B0C4DE'>\n".&td(100, 1)."";
					print HTML "<font color='#000000'>Pattern $i: $con_pattern</font></td>\n";
					print HTML "</tr>\n";

					#print HTML "<font color='#000000'>$bestpats{$pat_name}{'sequence'}</font></td>\n";
					#print HTML "".&td(33, 1)."<font color='#000000'>$bestpats{$hit}{'sequence'}</font></td>\n";
					#print HTML "".&td(33, 1)."<font color='#000000'>$bestpats{$pat_name}{'score'}</font></td>\n";
					#if($i == 0) { $con_pattern = $bestpats{$pat_name}{'sequence'}; }
					#if($i == 1) { $con_pattern = $bestpats{$hit}{'sequence'}; }

					#print HTML "<tr bgcolor='708090'>\n";
					#print HTML "".&td(100, 1)."";
					#print HTML "".$button."a\" value=\"POBO\" ";
					#print HTML "onClick=\"call_tools('$con_pattern', 'pobo/pobo')\">";
					#print HTML "".$button."b\" value=\"POCO2nd\" ";
					#print HTML "onClick=\"call_tools('$con_pattern', 'poco/poco2nd')\">";
					#print HTML "".$button."c\" value=\"Visualize\" ";
					#print HTML "onClick=\"call_tools('$con_pattern', 'visualize/visualize')\">";
					#print HTML "".$button."d\" value=\"Evolution\" ";
					#print HTML "onClick=\"call_tools('$con_pattern', 'evolution/evolution')\">";
					#print HTML "".$button."e\" value=\"Screener\" ";
					#print HTML "onClick=\"call_tools('$con_pattern', 'screener/screener')\">";
				}		
			}
		}

		print HTML "".&tableend(0, 1, 100, 1)."<br>\n";
	}
	print HTML "".&printfooters()."\n";
};
###################################################################################################
#Interface
sub PrintClustering {
	my($input_prefix_pats, $input_prefix_tree, $silhouette_ptr, $print_silhouette, $fdr_cutoff )	= @_;
	my(%score_hash)											= ();
	my(@score_data)											= ();
	my(@temp_data)											= ();
	my(@row)											= ();
	my($line)											= "";	
	my($col_width)											= 0;
	my($col_count)											= 0;
	my($con_pattern)										= "";
	my($temp_line)											= "";
	my($score_text)											= "";
	my($header_text)										= "";
	my($score_text_node1)										= "";
	my($score_text_node2)										= "";
	my($score_value)										= "";
	my($score_compare)										= "";
	my($button) = "<INPUT TYPE=\"button\" name=\"b";

	open(SCORE, "$input_prefix_tree")  or die "output file html $input_prefix_tree was not found";
	@score_data = <SCORE>;
	close SCORE;
	
	open(DATA, "$input_prefix_pats") or die "output file html $input_prefix_pats was not found";	
	@temp_data = <DATA>;
	close DATA;

	while (@score_data > 0) {
		$line = shift(@score_data);
		$line =~ s/^\s+|\s+$//;
		if(length($line) > 0) {
			#$line =~ s/\>//g;
			@row = split(/\s/, $line);
			$score_text_node1 = "";
			$score_text_node2 = "";
			$row[3] = sprintf("%.2f", $row[3]);
			$score_hash{$row[0]}{'text_score'}	= "Score = $row[3], FDR = $row[4]";
			$score_hash{$row[0]}{'text_pats'}	= "(patterns: $row[1] $score_text_node1 and $row[2] $score_text_node2)";
			$score_hash{$row[0]}{'child_r'}		= $row[1];
			$score_hash{$row[0]}{'child_l'}		= $row[2];
			$score_hash{$row[0]}{'score'}		= $row[3];
			$score_hash{$row[0]}{'fdr'}		= $row[4];
		}
	}
	
	while (@temp_data > 0) {
		$line = shift(@temp_data);
		$line =~ s/^\s+|\s+$//;
	
		#PRINT ONLY PATTERNS CROSSING THE SILHOUETTE VALUE#
	
		if(($print_silhouette == 0) && ($line =~ m/^>/) && (!($line =~ m/node/)) &&  (!($line =~ m/consensus/))) {
			while (length($line) > 0) {				#unclusterd pattterns
				$line = shift(@temp_data);
				$line =~ s/^\s+|\s+$//;
			}
		} elsif(($print_silhouette == 1) && ($line =~ m/^>/) &&  (!($line =~ m/consensus/)) && (!(exists(${$silhouette_ptr}{$line})))) {
			while (length($line) > 0) {                             #unclusterd pattterns
				$line = shift(@temp_data);
				$line =~ s/^\s+|\s+$//;
			}

		} elsif ((($print_silhouette == 0) && (length($line) > 0) && ($line =~ m/^>/) && (($line =~ m/node/i)) ||
			($print_silhouette == 1) && (length($line) > 0) && ($line =~ m/^>/))) {
			#$line =~ s/\>//g;
			
			if (exists($score_hash{$line}{'fdr'})) {
				if(!($score_hash{$line}{'fdr'} =~ m/null/g)) {
					$score_value = $score_hash{$line}{'fdr'};	
					$score_compare = 0;
				} else {
					$score_compare = 1;
					if (exists($score_hash{$line}{'score'})) {
						if(!($score_hash{$line}{'fdr'} =~ m/null/g)) {
							$score_value = $score_hash{$line}{'score'}
						} else {
							$score_value = 0;
						}
					} else {
						$score_value = 0;
					}
				}
			} else {
				#IF THE NODE IS A PATTERN
				$score_compare	= 1;
				$score_value	= 1;
			}

			if(($score_compare == 0 && $score_value <= $fdr_cutoff) || ($score_compare == 1 && $score_value > 0)) {
				$temp_line = $temp_data[1];
				$temp_line =~ s/^\s+|\s+$//;

				if (exists($score_hash{$line}{'text_score'})) {
					$score_text = $score_hash{$line}{'text_score'}; 
				} else {
					$score_text = "Score = null, FDR = null";
				}
				
				if (exists($score_hash{$line}{'text_pats'})) {
					$header_text = $score_hash{$line}{'text_pats'};
				} else {
					$header_text = "";
				}

				print HTML "".&tablestart(0, 1, 100, 4)."\n";
				print HTML "".$line." ".$header_text."</b></td></tr>\n";
				print HTML "<tr bgcolor ='#B0C4DE'>".&td(100, 4)."";
				print HTML "<b>$score_text</b></td></tr>\n";

				
				print HTML "<tr bgcolor ='#B0C4DE'>\n".&td(25, 1)."";
				print HTML "<font color='#000000'>A</font></td>\n";
				print HTML "".&td(25, 1)."<font color='#000000'>C</font></td>\n";
				print HTML "".&td(25, 1)."<font color='#000000'>G</font></td>\n";
				print HTML "".&td(25, 1)."<font color='#000000'>T</font></td>\n";
				print HTML "</tr>\n";				
			} else {
				if (exists($score_hash{$line}{'text'})) {
					$score_text = $score_hash{$line}{'text'};
				} else {
					$score_text = "(no score)";
				}

				print HTML "".&tablestart(0, 1, 100, 4)."\n";
				print HTML "".$line." ".$score_text."</td></tr>\n";
				print HTML "<tr bgcolor ='#B0C4DE'>\n<td colspan ='4' align='center'>\n";
				print HTML "<font color='#000000'>Matrix was not drawn (score = 0.0). To view the ";
				print HTML "matrix see results files below</font></td></tr>\n";
				print HTML "".&tableend(0, 1, 100, 4)."<br>\n";
				
				while (length($line) > 0) {
					$line = shift(@temp_data);
					$line =~ s/^\s+|\s+$//;
				}
			}
		}  elsif ((length($line) > 0) && ($line =~ m/consensus/i)) {
                        $line =~ s/^#//;
			                        
			$con_pattern = $line;
			$con_pattern =~ s/consensus://i;
			$con_pattern =~ s/\s//g;
			print HTML "<tr bgcolor='#B0C4DE'>\n";
			print HTML "".&td(100, 4)."<font color='#000000'>\n";
			print HTML "".$line."</font></td></tr>\n";

			print HTML "<tr bgcolor='708090'>\n";
			print HTML "".&td(100, 4)."";
			print HTML "".$button."a\" value=\"POBO\" ";
			print HTML "onClick=\"call_tools('$con_pattern', 'pobo/pobo')\">";
			print HTML "".$button."b\" value=\"POCO2nd\" ";
			print HTML "onClick=\"call_tools('$con_pattern', 'poco/poco2nd')\">";
			print HTML "".$button."c\" value=\"Visualize\" ";
			print HTML "onClick=\"call_tools('$con_pattern', 'visualize/visualize')\">";
			print HTML "".$button."d\" value=\"Evolution\" ";
			print HTML "onClick=\"call_tools('$con_pattern', 'evolution/evolution')\">";
			print HTML "".$button."e\" value=\"Screener\" ";
			print HTML "onClick=\"call_tools('$con_pattern', 'screener/screener')\">";

			print HTML "".&tableend(0, 1, 100, 4)."\n";
		} elsif (length($line) > 0) {
			@row = split(/\s/, $line);

			print HTML "<tr bgcolor ='#B0C4DE'>\n";
			for (my($j) = 0; $j < @row; ++$j) {
				print HTML "".&td(25, 1)."<font color='#000000'>".$row[$j]."</font></td>\n";
			}
			print HTML "</tr>\n";
		} elsif (length($line) == 0) {
			print HTML "<br>\n";
		}
	}

	print HTML "".&printfooters()."\n";
};
###################################################################################################
#Print start rows
sub printheaders {
	my($picture_file, $sequences1, $sequences2, $input_number, $sequence_length, $pat_overlap,
	   $bootstrap_sequence, $bootstrap_cluster, $background_organism, $draw_pictures)		= @_;
	my($line)                                                                                       = "";

	$line .= "<dtml-unless picture>";
	$line .= "<dtml-call \"REQUEST.set('picture', '$picture_file')\"></dtml-unless>\n";
	$line .= "<dtml-unless input_number>";
	$line .= "<dtml-call \"REQUEST.set('input_number', '$input_number')\"></dtml-unless>\n";
	$line .= "<dtml-unless sequence_length>";
	$line .= "<dtml-call \"REQUEST.set('sequence_length', '$sequence_length')\"></dtml-unless>\n";
	$line .= "<dtml-unless pat_overlap>";
	$line .= "<dtml-call \"REQUEST.set('pat_overlap','$pat_overlap')\"></dtml-unless>\n";
	$line .= "<dtml-unless bootstrap_sequence>";
	$line .= "<dtml-call \"REQUEST.set('bootstrap_sequence', '$bootstrap_sequence')\"></dtml-unless>\n";
	$line .= "<dtml-unless bootstrap_cluster>";
	$line .= "<dtml-call \"REQUEST.set('bootstrap_cluster', '$bootstrap_cluster')\"></dtml-unless>\n";
	$line .= "<dtml-unless background_organism>";
	$line .= "<dtml-call \"REQUEST.set('background_organism','$background_organism')\"></dtml-unless>\n";
	$line .= "<dtml-unless search_pattern>";
	$line .= "<dtml-call \"REQUEST.set('search_pattern','')\"></dtml-unless>\n";
	
	$line .= "<dtml-var matlign_header>\n";
	$line .= "<form name=\"cluster_hidden\" method=\"post\" enctype=\"multipart/form-data\">\n";
	$line .= "<dtml-var matlign_script>\n";
	$line .= "<Input type=\"hidden\" name=\"sequence_set1\" size=\"0\" value=\"$sequences1\">\n";
	$line .= "<Input type=\"hidden\" name=\"sequence_set2\" size=\"0\" value=\"$sequences2\">\n";
	$line .= "</form>\n";

	if($draw_pictures == 1) {
		$line .= "<dtml-var matlign_picture>\n";
	}

	return($line);
};
###################################################################################################
#Print end rows
sub printfooters {
	return("<dtml-var matlign_result_files>\n<dtml-var matlign_footer>");
};
###################################################################################################
#Print table row start
sub tablestart {
	my($type_switch, $bold, $width, $colspan)						= @_;
	my($line)										= "";

	$line = "<table border='1' cellpadding='4' cellspacing='0' bordercolor = '#00008B' align='center' width='95%'>"; 

	if($type_switch == 0) {
		$line  .= "<tr bgcolor='#B0C4DE'>";
		$line .= "".&td($width, $colspan)."";
		$line .= "<font color='#000000'>";
	} elsif ($type_switch == 1) {
		$line  .= "<tr bgcolor='#B0C4DE'>";
		$line .= "".&td($width, $colspan)."";
		$line .= "<font color='#000000'>";
	}
	if ($bold == 1) {
		$line .= "<b>";
	}
	return($line);
};
###################################################################################################
#Print table row start
sub tableend {
	my($type_switch, $bold, $width, $colspan)						= @_;
	my($line)										= "";

	if ($bold == 1) {
		$line  = "</b></font></td></tr>"; 
	} else {
		$line  = "</font></td></tr>";
	}
	$line .= "</table>\n";
	return($line);
};
###################################################################################################
#Print table td tag
sub td {
	my($width, $colspan)									= @_;
	return("<td align='center' width='$width%' colspan='$colspan'>");
};
###################################################################################################
1;
