use strict;
use warnings;

###################################################################################################
#Check file existing
sub Checkfile {
	my($input_prefix, $html_mode, $output_file) 				= @_;
	my(@test)								= ();
	
	if ($html_mode == 1) {
		if ((!(-e "$input_prefix\.tree")) || (!(-e "$input_prefix\.mtrx")) || (!(-e "$input_prefix\.pswm"))) {
			open(HTML, ">$output_file") or die "output file html $output_file was not found\n";
			print HTML "<dtml-var matlign_header>\n";

			print HTML "<table border='1' cellpadding='4' cellspacing='0' align='center' ";
			print HTML "bordercolor='#00008B' bgcolor='#B0C4DE' width='95%'>\n";
			print HTML "<tr bgcolor='#B0C4DE'>\n";
			print HTML "<td align='center' class='tswhite'>\n";
			print HTML "<b><font color = '#000000'>An error has occurred while performing the analysis. ";
			print HTML "Please, check that your input sequences are in FASTA-format and that you have ";
			print HTML "submitted at least two patterns that will be clustered. Clustered patterns can ";
			print HTML "be either in raw-format (the pattern input box contain only pattern sequences) ";
			print HTML "or in FASTA-format.</font></b>\n";
			print HTML "</td></tr></table>\n";
			print HTML "<br><br>\n";

			print HTML "<dtml-var matlign_footer>\n";
			close HTML;		
		}
	}
	
	if (!(-e "$input_prefix\.tree")) { die "5: $input_prefix\.tree was not found\n"; };
	if (!(-e "$input_prefix\.mtrx")) { die "6: $input_prefix\.mtrx was not found\n"; };
	if (!(-e "$input_prefix\.pswm")) { die "7: $input_prefix\.pswm was not found\n"; };
	if (!(-e "$input_prefix\.fdrs")) { die "7: $input_prefix\.fdrs was not found\n"; };

	#open(TEST, "$input_prefix\.tree") or die "tree"; @test = <TEST>; if(@test == 0) { die "tree size 0"; }
	open(TEST, "$input_prefix\.mtrx") or die "mtrx"; @test = <TEST>; if(@test == 0) { die "tree mtrx 0"; }
	open(TEST, "$input_prefix\.pswm") or die "pswm"; @test = <TEST>; if(@test == 0) { die "tree pswm 0"; }
	
};
###################################################################################################
1;
