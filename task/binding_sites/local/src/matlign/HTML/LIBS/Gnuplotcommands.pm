use strict;
use warnings;

###################################################################################################
#Common commands to set up gnuplot view
sub Gnuplotcommon {
	my($width, $height, $output)						= @_;
	print TEMP "set terminal png small size $width,$height\n";

        print TEMP "set output '$output'\n";
        print TEMP "set pointsize 1.25\n";

	print TEMP "set key off\n";
	print TEMP "unset colorbox\n";
	print TEMP "unset border\n";
	print TEMP "unset surface\n";
	print TEMP "unset xtics\n";
};
###################################################################################################
#Plot tree
sub Plotexpressiontree {
	my($total, $input, $yupper, $ylower, $silhouette_depth)			= @_;

	#print TEMP "set xrange[0:$total]\n";
	#print TEMP "set yrange[-1:1]\n";
	print TEMP "set yrange[$yupper:$ylower]\n";	
	print TEMP "set tmargin 2\n";
	print TEMP "set bmargin 2\n";
        #print TEMP "set lmargin 8\n";
        #print TEMP "set rmargin 8\n";
	print TEMP "set arrow from -10, $silhouette_depth to ".($total+10).", $silhouette_depth lt 1\n";
	print TEMP "plot '$input' w lines -1\n";
};
###################################################################################################
#Plot vara
sub Plotexpressionvara {
        my($input, $output, $width, $height, $xcoord, $number_of_clusters)	= @_;

	print TEMP "set key off\n";
	print TEMP "set yrange[-1:1]\n";
	print TEMP "set xrange[0:$number_of_clusters]\n";
	print TEMP "set terminal png small size $width,$height\n";
	print TEMP "set output '$output'\n";
	print TEMP "set pointsize 1.25\n";
	print TEMP "set arrow from $xcoord, -2 to $xcoord, 2 lt 1\n";
	print TEMP "set arrow from 0, 0 to $number_of_clusters, 0 lt 0\n";
	print TEMP "set label 'MAXIMUM SILHOUETTE ($xcoord PATTERNS)' at $xcoord, 0.1 center front textcolor lt 0\n";	
	print TEMP "plot '$input' u 1:2 w line -1\n";

};
###################################################################################################
#Call GnuPlot
sub Drawselectednode {
	my($inputfile_tree, $inputfile_vara, $input_temp1, $input_temp2, $output_tree, 
	    $output_vara, $yupper, $ylower, $gene_ptr, $total, $max_silhouette,
	    $silhouette_depth, $number_of_clusters) 				= @_;
	my($height)								= 500;
	my($width)								= 1100;
	my($execute_gp)								= 0;
	#$yupper 								= $yupper * 1.01;

	open(TEMP, ">$input_temp1") or die "output file $input_temp1 was not found\n";
	&Gnuplotcommon($width, $height, $output_tree);
	&Writetemptreesymbols($gene_ptr, ($yupper*0.05));
	&Plotexpressiontree(($total-1), $inputfile_tree, $yupper, $ylower, $silhouette_depth);
	close TEMP;

	open(TEMP, ">$input_temp2") or die "output file $input_temp2 was not found\n";
	&Plotexpressionvara($inputfile_vara, $output_vara, $width, 200, $max_silhouette, $number_of_clusters);
	close TEMP;

        $execute_gp = system("gnuplot $input_temp1");
	if($execute_gp != 0) { die "Problems performing Gnuplot\n"; }
	$execute_gp = system("gnuplot $input_temp2");
	if($execute_gp != 0) { die "Problems performing Gnuplot\n"; }

};
###################################################################################################
sub Writetemptreesymbols {
	my($gene_ptr, $label_shift)						= @_; 
	my($is_gene)								= 0;
	my($xcoord)								= 0;
	my($ycoord)								= 0;
	my($coords)								= "";
	my($gene_name)								= "";
	my($even_counter)							= 0;

	for my $nodeid (keys( %{$gene_ptr} ))  {
		if (exists(${$gene_ptr}{$nodeid}{'is_gene'}))	{ $is_gene = ${$gene_ptr}{$nodeid}{'is_gene'}; }
		if (exists(${$gene_ptr}{$nodeid}{'xposition'}))	{ $xcoord  = ${$gene_ptr}{$nodeid}{'xposition'};  }
		if (exists(${$gene_ptr}{$nodeid}{'yposition'}))	{ $ycoord  = ${$gene_ptr}{$nodeid}{'yposition'};  }
		if (($is_gene == 1) && (($xcoord % 2) == 1)  ) { $ycoord = $ycoord - $label_shift; }
		#$ycoord = $ycoord - 0.015;
		$ycoord = $ycoord - 0.011;
		$coords = $xcoord.", ".$ycoord;

		$gene_name = ${$gene_ptr}{$nodeid}{'id'};
		if(length($gene_name) > 20) { $gene_name =~ s/\(.*\)//; }

		#$gene_name .= "(".${$gene_ptr}{$nodeid}{'yposition'}.")";
		print TEMP "set label '$gene_name' at $coords center front textcolor lt 1\n";
	}
};
###################################################################################################
1;
