use strict;
use warnings;

###################################################################################################
#Reads the gtr (gene tree) file
sub ReadTreeInput {
	my($gtr_file, $exp_ptr, $asso_ptr, $random_mode, $uppery) 			= @_;
	my(@gtrdata) 									= ();
	my(@rows) 									= ();
	my($line) 									= "";
	my($root) 									= "";
	my($node) 									= "";
	my($l_child) 									= "";
	my($r_child) 									= "";
	my($yposition) 									= 0;
	my($checkline)									= "";
	my($node_consensus)								= "";
	my($fdr)									= 0;

	my($is_first)									= 1;
	my($min_yposition)								= 0;
	my($max_yposition)								= 0;

	open (GTR, $gtr_file) or die "gtr-file $gtr_file was not found\n";
	@gtrdata = <GTR>;
	close GTR;

	#Check that the format is correct#
	$checkline = $gtrdata[0];
	$checkline =~ s/^\s*|\s*$//i;
	@rows = split(/\s+/, $checkline);
	if (@rows < 3) { die "gtr-file must have at least tree columns\n"; }

	while (@gtrdata > 0) {
		$node = ""; $l_child = ""; $r_child = ""; $yposition = 0;

		$line = shift(@gtrdata);
		$line =~ s/^\s*|\s*$//i;
		@rows = split(/\s+/, $line);

		if ((length($line) > 0) && (scalar(@rows) >= 2)) {
			$node 	 = $rows[0];
			$l_child = $rows[1];
			$r_child = $rows[2];
			if (@rows > 3) { $yposition = $rows[3]; } 
			else { $yposition = 0; }					#Correlation row is not obligatory
			if (@rows > 4) { $fdr = $rows[4]; }
			else { $fdr = 0; }
			if (@rows > 5) { $node_consensus = "(".$rows[5].")"; }
                        else { $node_consensus = ""; }

			if (!(exists(${$exp_ptr}{$node}{'mother'}))) {${$exp_ptr}{$node}{'mother'} = 'root'; }
	
			${$exp_ptr}{$node}{'l_child'} 		= $l_child;		#Travels down	(ROOT --> LEAF)
			${$exp_ptr}{$node}{'r_child'} 		= $r_child;		#Travels down	(ROOT --> LEAF)
			${$exp_ptr}{$node}{'yposition'}		= ($yposition);		#Positio in the tree
			${$exp_ptr}{$node}{'is_gene'}		= 0;			#Is not gene
			${$exp_ptr}{$node}{'id'}		= $node.$node_consensus;
			${$exp_ptr}{$node}{'fdr'}		= $fdr;
			${$exp_ptr}{$l_child}{'mother'} 	= $node;		#Travels up 	(LEAF --> ROOT)
			${$exp_ptr}{$r_child}{'mother'} 	= $node;		#Travels up 	(LEAF --> ROOT)
			
			if($is_first == 1) {
				$max_yposition 	= $yposition;
				$min_yposition 	= $yposition;
				$is_first 	= 0;
			}

			if ($yposition >= $max_yposition) { $max_yposition = $yposition; }
			if ($yposition <= $min_yposition) { $min_yposition = $yposition; }

			if(!($l_child =~ m/node/i)) {
				my($gtr_id) 				= $l_child; 

				${$exp_ptr}{$l_child}{'id'} 		= $gtr_id;	#Name of the gene
				${$exp_ptr}{$l_child}{'yposition'} 	= $uppery;	#Position in the tree picture
				${$exp_ptr}{$l_child}{'is_gene'} 	= 1;		#Flag to check, is the node is gene
				${$exp_ptr}{$l_child}{'genes'}		= 1;
			} if(!($r_child =~ m/node/i)) {
				my($gtr_id) 				= $r_child;

				${$exp_ptr}{$r_child}{'id'} 		= $gtr_id;
				${$exp_ptr}{$r_child}{'yposition'} 	= $uppery;
				${$exp_ptr}{$r_child}{'is_gene'} 	= 1;
				${$exp_ptr}{$r_child}{'genes'}          = 1;
        		}
		}
	} 
	$max_yposition = ($max_yposition + (($max_yposition - $min_yposition) * 0.01));
	$min_yposition = ($min_yposition - (($max_yposition - $min_yposition) * 0.01));
	return($max_yposition, $min_yposition);
};
###################################################################################################
1;
