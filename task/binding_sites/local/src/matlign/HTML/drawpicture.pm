#!/afs/bi/p/bin/perl
use strict;
use warnings;
use Getopt::Long;
use FindBin qw($Bin);

use lib "$Bin/LIBS";
use Checkfile;
use Expressiontree;
use Gnuplotcommands;
use PrintResults;
use PrintPicturefiles;
use ReadExpressiondata;
use MaxDepth;

###################################################################################################
#Get commandline options

my %opt = ();
my %legend = (
	"input_prefix=s"	=>	"prefix of the input files",
	"output_prefix=s"	=>	"prefix for the output file",
	"html_mode=s"		=>	"html mode",
	"html_output=s"         =>      "link to html result folder",
	"search_pattern=s"	=>	"file that has the original matrices",
	"sequence_set1=s"       =>      "file of the first sequence set",
	"sequence_set2=s"       =>      "file of the second sequence set",
	"input_number=s"        =>      "number of sequence sets",
	"sequence_length=s"     =>      "length of the sequences",
	"pat_overlap=s"         =>      "can patterns overlap (yes)",
	"bootstrap_sequence=s"  =>      "number of sequences selected in bootstrap repeats",
	"bootstrap_cluster=s"   =>      "number of bootstrap repeats",
	"background_organism=s" =>      "organism where sequences are compared to",
	"silhouette=s"		=>	"print either silhouette cluster or all",
	"mode=s"		=> 	"cluster patterns or not",
	"fdr_cutoff=s"		=>	"cutoff value to choose patterns to be printed",
);

GetOptions(\%opt, "help!", keys %legend);

my($input_prefix)          					= "";
my($output_prefix)						= "";
my($html_output)						= "";
my($html_mode)							= "";

my($sequence_set1)                                              = "";
my($sequence_set2)                                              = "";
my($search_pattern)                                             = "";
my($input_number)                                               = "";
my($sequence_length)                                            = "";
my($pat_overlap)                                                = "";
my($bootstrap_sequence)                                         = "";
my($bootstrap_cluster)                                          = "";
my($background_organism)                                        = "";
my($print_silhouette)						= "";
my($draw_pictures)						= 1;
my($cluster_patterns)						= "";
my($fdr_cutoff)							= 1;

if (exists($opt{'input_prefix'}))	{ $input_prefix	  	= $opt{'input_prefix'};		} else { die "No input files\n";  }
if (exists($opt{'output_prefix'}))	{ $output_prefix  	= $opt{'output_prefix'};	} else { die "No output files\n"; }
if (exists($opt{'html_output'}))	{ $html_output    	= $opt{'html_output'};		} else { $html_output = $input_prefix; }
if (exists($opt{'html_mode'}))		{ $html_mode	  	= $opt{'html_mode'};		} else { $html_mode = 0; }
if (exists($opt{'mode'}))		{ $cluster_patterns 	= $opt{'mode'};			} else { $cluster_patterns = 1; }

if (exists($opt{'sequence_set1'}))      { $sequence_set1   	= $opt{'sequence_set1'};	} else { $sequence_set1 = "null"; }
if (exists($opt{'sequence_set2'}))      { $sequence_set2   	= $opt{'sequence_set2'};	} else { $sequence_set2 = "null"; }

if (exists($opt{'search_pattern'}))     { $search_pattern  	= $opt{'search_pattern'};       }
if (exists($opt{'input_number'}))       { $input_number    	= $opt{'input_number'};         }
if (exists($opt{'sequence_length'}))    { $sequence_length 	= $opt{'sequence_length'};      }
if (exists($opt{'pat_overlap'}))        { $pat_overlap          = $opt{'pat_overlap'};          }
if (exists($opt{'bootstrap_sequence'})) { $bootstrap_sequence   = $opt{'bootstrap_sequence'};   }
if (exists($opt{'bootstrap_cluster'}))  { $bootstrap_cluster    = $opt{'bootstrap_cluster'};    }
if (exists($opt{'background_organism'})){ $background_organism  = $opt{'background_organism'};  }
if (exists($opt{'fdr_cutoff'}))		{ $fdr_cutoff		= $opt{'fdr_cutoff'};		} else { $fdr_cutoff = 1; }

if ($html_mode == 1) {
	if (exists($opt{'silhouette'}))	{ $print_silhouette	= $opt{'silhouette'};		} else { $print_silhouette = 1; }
}

if ($cluster_patterns == 0) {
	$print_silhouette 	= 0;
	$draw_pictures 		= 0;
}

&main($input_prefix, $output_prefix, $html_output, $html_mode, $sequence_set1, $sequence_set2, $search_pattern, 
$input_number, $sequence_length, $pat_overlap, $bootstrap_sequence, $bootstrap_cluster, $background_organism, 
$print_silhouette, $cluster_patterns );

eval 'use lib "/data/backup/zope_data/poxo/COMMON"';
eval 'use CreateCheckfile';
eval '&CreateCheckfile("$output_prefix\.checkme")';

exit;
###################################################################################################
#Main program that calls everything else (no params)
sub main {
        my($input_prefix, $output_prefix, $html_output, $html_mode, $sequence_set1, $sequence_set2, 
	$search_pattern, $input_number, $sequence_length, $pat_overlap, $bootstrap_sequence, 
	$bootstrap_cluster, $background_organism, $print_silhouette, $cluster_patterns  ) 	= @_;
	my(%silhouette_nodes)									= ();	
	my(%exp_tree)										= ();	
	my(%null_hash)										= ();
	my(@delete_file)									= ();
	my(@temp_seqs)										= ();	

	my($line)										= "";
	my($min_ypos)										= 0;
	my($max_ypos)										= 0;
	my($root_ypos)										= 0;
	my($total)										= 0;
	my($root)										= "";
	my($uppery)										= 0;
	my($lowery)										= 0;
	my($small_factor)									= 0;
	my($sequences1)                                                                         = "";
	my($sequences2)                                                                         = "";
	my($max_silhouette)									= 0;
	my($silhouette_depth)									= 0;
	my($number_of_clusters)									= 0;
	
	&Checkfile($input_prefix, $html_mode, "$output_prefix\.html");

	if($draw_pictures == 1) {
		($lowery, $uppery) = &FindMinAndMaxDepth("$input_prefix\.tree");	
		($max_silhouette, $silhouette_depth, $number_of_clusters) = &FindMaxValue("$input_prefix\.vara");

		#$uppery = $uppery * 1.1;
		#$lowery = $lowery * 1.1;
		if (($uppery - $lowery) > 0) {
			$small_factor = (($uppery - $lowery) * 0.01);
		} elsif (($uppery - $lowery) < 0) {
			$small_factor = (($lowery - $uppery) * 0.01);
		} else {
			$small_factor = 0.01;
		}
	
		$uppery = $uppery + $small_factor;
		$lowery = $lowery - $small_factor;
	
		($min_ypos, $max_ypos) 	= &ReadTreeInput("$input_prefix\.tree", \%exp_tree, \%null_hash, 0, $uppery);
		($root, $root_ypos)	= &FindRoot(\%exp_tree);
		($total)		= &GetTreeInformation($root, 0, $min_ypos, $max_ypos, -1, \%exp_tree);
 
		&PictureCaller($root, \%exp_tree, "$output_prefix\.temp", $lowery, \%silhouette_nodes, $silhouette_depth, $max_silhouette);
		&Drawselectednode($output_prefix."\.temp", "$output_prefix\.vara", $output_prefix."1\.gnu", $output_prefix."2\.gnu", 
			  $output_prefix."1\.png", $output_prefix."2\.png", $uppery, $lowery, \%exp_tree, $total, 
			  $max_silhouette, $silhouette_depth, $number_of_clusters);
	}

	if ($html_mode == 1) {
		if (!($sequence_set1 eq "null")) {
			open (SEQUENCE_SET1, "$sequence_set1") or die "1: output file $sequence_set1 was not found\n";
			for (<SEQUENCE_SET1>) { $sequences1 .= $_; }
			close SEQUENCE_SET1;
												
			$sequences1 =~ s/\'/ /gi;
			$sequences1 =~ s/\"/ /gi;
			$sequences1 =~ s/\r/ /gi;
			$sequences1 =~ s/\t/ /gi;
		}
		if (!($sequence_set2 eq "null")) {
			open (SEQUENCE_SET2, "$sequence_set2") or die "2: output file $sequence_set2 was not found\n";
			for (<SEQUENCE_SET2>) { $sequences2 .= $_; }
			close SEQUENCE_SET2;

			$sequences2 =~ s/\'/ /gi;
			$sequences2 =~ s/\"/ /gi;
			$sequences2 =~ s/\r/ /gi;
			$sequences2 =~ s/\t/ /gi;
		}

		open(HTML, ">$output_prefix\.html") or die "3: output file html $output_prefix was not found\n";
		$html_output =~ s/\.html//i;
		
		$line = &printheaders($html_output, $sequences1, $sequences2, $input_number, $sequence_length, $pat_overlap,
		$bootstrap_sequence, $bootstrap_cluster, $background_organism, $draw_pictures);
				
		print HTML "$line\n";

		if ($cluster_patterns == 1) {
			&PrintClustering("$input_prefix\.pswm", "$input_prefix\.tree", \%silhouette_nodes, $print_silhouette, $fdr_cutoff );
		} elsif ($cluster_patterns == 0) { 
			&PrintBestpats("$input_prefix\.pswm", "$input_prefix\.mtrx", "$input_prefix\.fdrs", $fdr_cutoff);	
		}

		close HTML;
	}
	push(@delete_file, "$output_prefix\.temp");
	push(@delete_file, $output_prefix."1\.gnu");
	push(@delete_file, $output_prefix."2\.gnu");
	push(@delete_file, "$sequence_set1");
	push(@delete_file, "$sequence_set2");

	if (!($search_pattern =~ m/null/gi)) {
		push(@delete_file, "$search_pattern");
	}
	#unlink(@delete_file);
};
###################################################################################################
