/***************************************************************************
 *   Copyright (C) 2007 by Ari Loytynoja and Matti Kankainen               *
 *   ari@ebi.ac.uk / matti.kankainen@helsinki.fi                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "intmatrix.h"
#include "random.h"
#include "intmatrixrow.h"
#include "distancematrix.h"

using namespace std;

extern void calc_alignment_score(IntMatrix*, IntMatrix*, IntMatrix**, IntMatrix*, IntMatrix*, IntMatrix**, float&, float&);
extern int get_tLength(int, int, int, int, int&, int&);
extern int spacer;
extern int tLength;

//*********************************************************************************************************//
RandomMatrix::RandomMatrix() {

};
//*********************************************************************************************************//
RandomMatrix::~RandomMatrix() {

};
//*********************************************************************************************************//
int RandomMatrix::clear_vector(vector<IntMatrix*>& input_vec) {
	for(unsigned int i = 0; i < input_vec.size(); ++i) {
		delete input_vec[i];
	}
	input_vec.clear();
	return(1);
};
//*********************************************************************************************************//
float RandomMatrix::randomize_gene(int random_perm, vector<IntMatrix*>& input_mat_vec,
				   DistanceMatrix& dist_mat, FILE* fdrs_stream ) {
	vector <IntMatrixRow>                   pooled_values;
	vector <IntMatrix*>			one_zero_mat_vec;
	vector <int>                            random_values;
	vector <float>                          random_scores;

	IntMatrix* i1;
	
	float   sum(0);
        int     cell(0);
	int     rand_pos(0);
	int     rand_val(0);
	int 	min_l(0);
	int	max_l(0);
	int 	parameter_tlength = tLength;
	int 	return_val;
		
	float score(0);
	float align_cols(0);
	float fdr(0);

	IntMatrix* p0;
	IntMatrix* p1;

	for(unsigned int h = 0; h < random_perm; ++h) {
		IntMatrixRow* int_row;
		for(unsigned int i = 0; i < input_mat_vec.size(); ++i) {
			for(unsigned int j = 1; j < ((*input_mat_vec[i]).X()); ++j){
				int_row = new IntMatrixRow;
				(*int_row).i0 = (*input_mat_vec[i]).g(j,0);
				(*int_row).i1 = (*input_mat_vec[i]).g(j,1);
				(*int_row).i2 = (*input_mat_vec[i]).g(j,2);
				(*int_row).i3 = (*input_mat_vec[i]).g(j,3);
				pooled_values.push_back(*int_row);

				delete int_row;
			}
		}

		for(unsigned int i = 0; i < input_mat_vec.size(); ++i) {
			p0 = new IntMatrix( ((*input_mat_vec[i]).X()), 4, "counts");
			(*p0).initialise(0);
			
			for(unsigned int j = 1; j < ((*input_mat_vec[i]).X()); ++j){
				int_row = new IntMatrixRow;

				rand_pos = rand()%pooled_values.size();
				*int_row = pooled_values.at(rand_pos);
				pooled_values.erase( pooled_values.begin() + rand_pos );

				random_values.push_back((*int_row).i0);
				random_values.push_back((*int_row).i1);
				random_values.push_back((*int_row).i2);
				random_values.push_back((*int_row).i3);

				for(unsigned int k = 0; k < 4; ++k) {
					rand_pos = rand()%random_values.size();
					rand_val = random_values.at(rand_pos);
					random_values.erase( random_values.begin() + rand_pos );
					p0->s(rand_val,j,k);
				}

				delete int_row;
				random_values.clear();
			}
			input_rnd_vector.push_back(p0);
		}
	}              

	for(int i = 0; i < input_mat_vec.size(); ++i) {
		i1 = new IntMatrix((*input_mat_vec[i]).X(), 5, "frequencies");
		i1->initialise(1);
		one_zero_mat_vec.push_back(i1);
	}
	
	for(int i = 0; i < random_perm; ++i) {
		for(int j = (i*input_mat_vec.size()); j < (i*input_mat_vec.size())+input_mat_vec.size(); ++j) {
			for(int k = (j+1); k < (i*input_mat_vec.size())+input_mat_vec.size(); ++k ) {
				tLength = get_tLength(((*input_rnd_vector[j]).X()-1), ((*input_rnd_vector[k]).X()-1), 
							spacer, parameter_tlength, min_l, max_l);
				//this function returns the correct viterbi-path score
				calc_alignment_score(input_rnd_vector[j], input_rnd_vector[k], &p0, 
						     one_zero_mat_vec[j-(i*input_mat_vec.size())], 
						     one_zero_mat_vec[k-(i*input_mat_vec.size())], &p1, 
						     score, align_cols);
				delete p0;
				delete p1;
				align_cols = 1;
				random_scores.push_back(score/align_cols);
			}
		}
	}

	return_val = clear_vector(input_rnd_vector);
	return_val = clear_vector(one_zero_mat_vec);
	
	sort(random_scores.rbegin(), random_scores.rend());
	fdr = calculate_fdr(random_perm, random_scores, input_mat_vec.size(), dist_mat, fdrs_stream);
	return(fdr);
};
//*********************************************************************************************************//
float RandomMatrix::calculate_fdr(int random_perm, vector<float>& random_scores, int input_size, DistanceMatrix& dist_mat, FILE* fdrs_stream ) {
	vector<float> positives;

	unsigned long row_max_dist(0);
	unsigned long col_max_dist(0);

	float fdr_false;
	float fdr_positive;
	float temp_score;
	float fdr;
	float this_fdr;
	float pair_score(0);

	DistanceMatrix fdr_values;
	fdr_values.SetSize(input_size, input_size);

	pair_score = dist_mat.FindMax(row_max_dist, col_max_dist);

	for(int i = 0; i < input_size; ++i) {
		for(int j = (i+1); j < input_size; ++j) {
			positives.push_back(dist_mat.GetElementAvg(i, j));	
		}
	}

	sort(positives.rbegin(), positives.rend());

	for(int i = 0; i < input_size; ++i) {
		for(int j = (i+1); j < input_size; ++j) {
			fdr_false       = 0;
			fdr_positive    = 0;
			//dist_mat might contain merged elments, value must be divided by
			temp_score = dist_mat.GetElementAvg(i, j);

			for(int k = 0; k <  positives.size(); ++k) {
				if(positives.at(k) >= temp_score) { ++fdr_positive; }
			}

			for(int k = 0; k <  random_scores.size(); ++k) {
				if(random_scores.at(k) >= temp_score) { ++fdr_false; }
			}

			fdr_false = fdr_false / random_perm;
			fdr = fdr_false/fdr_positive;
			if(fdr > 1) { fdr = 1; }
			if(fdr < 0) { fdr = 0; }

			fdr_values.SetElement(i, j, fdr);
			fdr_values.SetElement(j, i, fdr);

			if(i == row_max_dist && j == col_max_dist) { this_fdr = fdr; }
		}
	}

	fdr_values.PrintMatrix(fdrs_stream);
	fdr_values.FreeMemory();

	return(this_fdr);
};
//*********************************************************************************************************//
