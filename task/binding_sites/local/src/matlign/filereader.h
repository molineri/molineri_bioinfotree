/***************************************************************************
 *   Copyright (C) 2006 by Ari Loytynoja   *
 *   ari@ebi.ac.uk   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef FOR
#define FOR(i,n) for(i=0; i<n; i++)
#endif

#ifndef FILEREADER_H
#define FILEREADER_H

#include "intmatrix.h"
#include <vector>

class FileReader{
public:
    FileReader();
    IntMatrix* readSequence(std::string, int &, std::string, float, float, float);
    IntMatrix* readMatrix(std::vector<std::string>& f, int &, std::string, float, float, float);
    int readFile(std::string* f, std::vector<IntMatrix*>&, int &, float, float, float);

    ~FileReader();

};

#endif
