/***************************************************************************
 *   Copyright (C) 2007 by Ari Loytynoja and Matti Kankainen               *
 *   ari@ebi.ac.uk / matti.kankainen@helsinki.fi                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef random_h
#define random_h
#include <string>
#include <vector>
#include "intmatrix.h"
#include "distancematrix.h"

class RandomMatrix {
public:
	float randomize_gene(int, std::vector<IntMatrix*>&, DistanceMatrix&, FILE* );

	RandomMatrix ();
	~RandomMatrix();
private:
	std::vector <IntMatrix*> input_rnd_vector;
	int clear_vector(std::vector<IntMatrix*>&);
	float calculate_fdr(int, std::vector<float>&, int, DistanceMatrix&, FILE*);
};

#endif


