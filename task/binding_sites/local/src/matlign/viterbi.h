/***************************************************************************
 *   Copyright (C) 2006 by Ari Loytynoja   *
 *   ari@ebi.ac.uk   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef FOR
#define FOR(i,n) for(i=0; i<n; i++)
#endif

#ifndef VITERBI_H
#define VITERBI_H

#include <vector>
#include <utility>
#include "flmatrix.h"
#include "intmatrix.h"

class Viterbi{

    bool s1Single;
    bool s2Single;

    IntMatrix* s1;
    IntMatrix* s2;

    FlMatrix* m1;
    FlMatrix* m2;

    int l1;
    int l2;

    FlMatrix* sScore;
    float small;
    int pt;
public:
    ~Viterbi();
    Viterbi(IntMatrix* s1, IntMatrix* s2);
    Viterbi(IntMatrix* s1, FlMatrix* m2);
    Viterbi(FlMatrix* m1, FlMatrix* m2);

    void defineModel();
    void align(IntMatrix* path, float* score);
    void fill(float* mean, float* std);
    float score(int i,int j);

    float max(float a,float b);
    float max(float a,float b,float c);
    bool rndBool();
    int rndInt(int i);
};

#endif
