#ifndef clust_res_h
#define clust_res_h

#include <string>

class ClusterResults {
public:
	float element_distance;

	void SetElement(std::string, std::string, std::string, std::string, float);
	void GetElement(std::string&, std::string&, std::string&, std::string&, float&);

	ClusterResults();
	~ClusterResults();

private:
	std::string new_element;
	std::string element1;
	std::string element2;
	std::string consensus;
};

#endif
