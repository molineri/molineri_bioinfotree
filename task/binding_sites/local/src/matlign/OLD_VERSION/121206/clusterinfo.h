#ifndef clust_info_h
#define clust_info_h

#include <vector>

class ClusterInfo {
public:
	void PrintElement(FILE*, bool prnt_elements = 0);
	void ClearElementStat();
	void CalcElementStat(float&, float&, float&, float&, unsigned long&);
	void SetElementStat(float, float);
	void SetElement(unsigned long, long element = -1);
	unsigned long GetElement(unsigned long);
	unsigned long GetSize();

	ClusterInfo();
	~ClusterInfo();
private:
	std::vector <unsigned long> cluster_elements;
	float min;
	float max;
	float sum;
	float var;
	float temp_sum;

	float calc_sum;
	float calc_var;
	float calc_min;
	float calc_max;
};

#endif
