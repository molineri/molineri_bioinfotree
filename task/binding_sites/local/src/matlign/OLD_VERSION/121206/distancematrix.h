#ifndef dist_mat_h
#define dist_mat_h
#include <vector>

class DistanceMatrix {
public:
	float ** distance_matrix;	

	void FreeMemory();
	void SetSize( unsigned long, unsigned long );
	void PrintMatrix( FILE* );
	float FindMax( unsigned long&, unsigned long& );
	float FindMaxPair( unsigned long, unsigned long& );
	void RemoveElement( unsigned long );
	void RemoveRow( unsigned long );
	void SetSmallRow( unsigned long );

	float SetElement( unsigned long, unsigned long, float);
	float GetElement( unsigned long, unsigned long);

	unsigned long SetElementSize (unsigned long, unsigned long);
	unsigned long GetElementSize (unsigned long);
	unsigned long AddElementSize (unsigned long);
	unsigned long MergeElementSize(unsigned long, unsigned long);

	DistanceMatrix();
	~DistanceMatrix();
private:
	bool has_data;
	std::vector <unsigned long> element_size;

	unsigned long row;
	unsigned long col;

};

#endif
