#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "clusterinfo.h"

using namespace std;

//*********************************************************************************************************//
ClusterInfo::ClusterInfo() {
	sum = 0;
	var = 0;
	min = HUGE_VAL;
	max = -HUGE_VAL;
};
//*********************************************************************************************************//
ClusterInfo::~ClusterInfo() {
	cluster_elements.clear();	
};
//*********************************************************************************************************//
unsigned long ClusterInfo::GetSize() {
	return (cluster_elements.size());
};
//*********************************************************************************************************//
void ClusterInfo::PrintElement(FILE *outstream, bool prnt_elements) {
	if (prnt_elements == 1) {
		for(unsigned int i = 0; i < cluster_elements.size(); ++i) {
			fprintf (outstream, "%u ", cluster_elements[i]);
		}
		fprintf (outstream, "\n");
	}

	fprintf (outstream, "%.3e\t%.3e\t%.3e\t%.3e\t%u\n", calc_min, calc_max, calc_sum, calc_var,  cluster_elements.size());

};
//*********************************************************************************************************//
void ClusterInfo::SetElement(unsigned long value, long element) {
	if (element == -1) {
		cluster_elements.push_back(value);
	} else {
		cluster_elements[element] = value;
	}
};
//*********************************************************************************************************//
unsigned long ClusterInfo::GetElement(unsigned long element) {
	if(element < cluster_elements.size()) {
		return (cluster_elements[element]);
	} else {
		return (999999);
	}
};
//*********************************************************************************************************//
void ClusterInfo::ClearElementStat() {
        sum = 0;
        var = 0;
        min = HUGE_VAL;
        max = -HUGE_VAL;
};
//*********************************************************************************************************//
void ClusterInfo::SetElementStat(float real_score, float temp_score) {
	//if (real_score < 0) { real_score = 0; }
	//if (temp_score < 0) { temp_score = 0; }
	
	sum = sum + real_score;
	temp_sum = temp_sum + temp_score;
	var = var + pow(temp_score, 2);
	if (real_score <= min) { min = real_score; }
	if (real_score >= max) { max = real_score; }
};
//*********************************************************************************************************//
void ClusterInfo::CalcElementStat(float &get_min, float &get_max, float &get_sum, float &get_var, unsigned long &get_size) {
	get_size = cluster_elements.size();

	if (min == HUGE_VAL) { get_min = 0; } else { get_min = min; }
	if (max = -HUGE_VAL) { get_max = 0; } else { get_max = max; }

	//if ((sum != 0) && (cluster_elements.size() > 0)) { get_sum = (sum / cluster_elements.size()); } else { get_sum = 0; }
	get_sum = sum;
	
	if ((sum != 0) && (var != 0) && ((cluster_elements.size() -1) > 0)) {
		get_var = ((cluster_elements.size() * var) - pow(temp_sum, 2));
		get_var = (get_var /(cluster_elements.size() * (cluster_elements.size() - 1)));
	} else {
		get_var = 0; 
	}

	calc_min = get_min;
	calc_max = get_max;
	calc_var = get_var;
	calc_sum = get_sum;
};
//*********************************************************************************************************//



