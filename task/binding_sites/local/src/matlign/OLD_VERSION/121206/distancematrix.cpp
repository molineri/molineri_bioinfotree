#include <iostream>
#include <stdio.h>
#include <string>
#include "distancematrix.h"
#include <vector>
#include <cmath>

using namespace std;

//*********************************************************************************************************//
DistanceMatrix::DistanceMatrix() {
        has_data = 1;
	//distance_matrix = NULL;
};
//*********************************************************************************************************//
DistanceMatrix::~DistanceMatrix() {
	has_data = 0;
	if (distance_matrix != NULL) {
		for(unsigned int i = 0; i < row; ++i) {
			if (distance_matrix[i] != NULL) {
				delete [] distance_matrix[i];
				distance_matrix[i] = NULL;
			}
		}
		distance_matrix = NULL;
	}
	element_size.clear();
};
//*********************************************************************************************************//
void DistanceMatrix::FreeMemory() {
	has_data = 0;

	for(unsigned int i = 0; i < row; ++i) {
		delete [] distance_matrix[i];
		distance_matrix[i] = NULL;
	}

	delete [] distance_matrix;
	distance_matrix = NULL;
};
//*********************************************************************************************************//
void DistanceMatrix::SetSize(unsigned long n_row, unsigned long n_col) {
	has_data = 1;
	row = n_row;
	col = n_col;
	distance_matrix = new float * [row];
	
	if (distance_matrix != NULL) {
		for(unsigned int i = 0; i < row; ++i) {
			distance_matrix[i] = new float[col];
		}
	}

	for(unsigned int i = 0; i < row; ++i) {
		for(unsigned int j = 0; j < col; ++j) {
			distance_matrix[i][j] = -HUGE_VAL;
		}
	}
};
//*********************************************************************************************************//
void DistanceMatrix::SetSmallRow(unsigned long row_element) {
	for(unsigned int j = 0; j < col; ++j) {
		distance_matrix[row_element][j] = -HUGE_VAL;
	}
};
//*********************************************************************************************************//
void DistanceMatrix::RemoveRow(unsigned long remove_element) {
	unsigned long temp_i(0);
	unsigned long temp_j(0);

	float ** distance_matrix_temp;

	if ((remove_element < row ) && (remove_element < col)) {
		distance_matrix_temp = new float * [row-1];
		for(unsigned int i = 0; i < row -1; ++i) {
			distance_matrix_temp[i] = new float[col];
		}
	}
	
	for(unsigned int i = 0; i < row; ++i) {
		temp_j = 0;
		for(unsigned int j = 0; j < col; ++j) {
			if ((i < remove_element || i > remove_element)) {
				distance_matrix_temp[temp_i][temp_j] = distance_matrix[i][j];
				++temp_j;
			}
		}
		if (i < remove_element || i > remove_element) { ++temp_i; }
	}

        FreeMemory();
        SetSize(row - 1, col);

	for(unsigned int i = 0; i < row; ++i) {
		for(unsigned int j = 0; j < col; ++j) {
			distance_matrix[i][j] = distance_matrix_temp[i][j];
		}
	}

	for(unsigned int i = 0; i < row; ++i) {
		delete [] distance_matrix_temp[i];
		distance_matrix_temp[i] = NULL;
	}

	delete [] distance_matrix_temp;
	distance_matrix_temp = NULL;
};
//*********************************************************************************************************//
void DistanceMatrix::RemoveElement(unsigned long remove_element) {
	unsigned long temp_i(0);
	unsigned long temp_j(0);

	float ** distance_matrix_temp;

	if ((remove_element < row ) && (remove_element < col)) {
		distance_matrix_temp = new float * [row-1];
		for(unsigned int i = 0; i < row -1; ++i) {
			distance_matrix_temp[i] = new float[col -1];
		}
	}
        
	for(unsigned int i = 0; i < row; ++i) {
		temp_j = 0;
		for(unsigned int j = 0; j < col; ++j) {
			if ((i < remove_element || i > remove_element) && (j < remove_element || j > remove_element)) {
				distance_matrix_temp[temp_i][temp_j] = distance_matrix[i][j];
				++temp_j;
			}
		}
		if (i < remove_element || i > remove_element) { ++temp_i; }
	}

	FreeMemory();
	SetSize(row - 1, col - 1);
	//distance_matrix = distance_matrix_temp;	
	//Should work since address is copied (???)
	//But how to delete temp matrix (???)

	for(unsigned int i = 0; i < row; ++i) {
		for(unsigned int j = 0; j < col; ++j) {
			distance_matrix[i][j] = distance_matrix_temp[i][j];
		}
	}

        for(unsigned int i = 0; i < row; ++i) {
                delete [] distance_matrix_temp[i];
                distance_matrix_temp[i] = NULL;
        }
        delete [] distance_matrix_temp;
        distance_matrix_temp = NULL;
};
//*********************************************************************************************************//
float DistanceMatrix::FindMaxPair(unsigned long max_row, unsigned long& max_col) {
	float max_value(-1000);
	float max_temp(0);

	for(unsigned long j = 0; j < col; ++j) {
		max_temp = distance_matrix[max_row][j] / (element_size[max_row] * element_size[j]);
		if ((max_temp >= max_value) || (j == 0)) {
			max_value = max_temp;
			max_col = j;
		}
	}
        
	return (max_value);

};
//*********************************************************************************************************//
float DistanceMatrix::FindMax(unsigned long& max_row, unsigned long& max_col) {
	unsigned long temp(0);
	float max_value(-1000);
	float max_temp(0);

	for(unsigned long i = 0; i < row; ++i) {
		for(unsigned long j = 0; j < col; ++j) {
			//max_temp = distance_matrix[i][j];
			max_temp = distance_matrix[i][j] / (element_size[i] * element_size[j]);
			if ((max_temp >= max_value) || (i == 0 && j == 0)) { 
				max_value = max_temp; 
				max_row = i;
				max_col = j;
			}
		}
	}
	
	if(max_row > max_col) {
		temp = max_col;
		max_col = max_row;
		max_row = temp;
	}
	return (max_value);
};
//*********************************************************************************************************//
void DistanceMatrix::PrintMatrix(FILE *outstream) {
	float prnt_value(0);	

	for(unsigned int i = 0; i < row; ++i) {
		fprintf (outstream, "|");
		for(unsigned int j = 0; j < col; ++j) {
			prnt_value = distance_matrix[i][j] / (element_size[i] * element_size[j]);	
			if( distance_matrix[i][j] > 0 ) { fprintf (outstream, "+%.3f|", prnt_value); }
			else { fprintf (outstream, "%.3f|", prnt_value); }
		}
		fprintf (outstream, "\n");
	}
	fprintf (outstream, "\n");
};
//*********************************************************************************************************//
float DistanceMatrix::SetElement(unsigned long s_row, unsigned long s_col, float set_value) {
	float value_set(0);
	if (s_row > row) { value_set = 0; }
	if (s_col > col) { value_set = 0; }

	if ((s_row <= row) && (s_col <= col)) {
		distance_matrix[s_row][s_col] = set_value;
		value_set = 1;
	}
	return(value_set);
};
//*********************************************************************************************************//
float DistanceMatrix::GetElement(unsigned long g_row, unsigned long g_col) {
	float get_value(0);
	if (g_row > row) { get_value = 0; }
	if (g_col > col) { get_value = 0; }

	if ((g_row <= row) && (g_col <= col)) {
		get_value = distance_matrix[g_row][g_col];
	}
	return(get_value);
};
//*********************************************************************************************************//
unsigned long DistanceMatrix::SetElementSize(unsigned long element, unsigned long size) {
	element_size[element] = size;
};
//*********************************************************************************************************//
unsigned long DistanceMatrix::AddElementSize(unsigned long size) {
        element_size.push_back(size);
};
//*********************************************************************************************************//
unsigned long DistanceMatrix::GetElementSize(unsigned long element) {
	return element_size[element];
};
//*********************************************************************************************************//
unsigned long DistanceMatrix::MergeElementSize(unsigned long element1, unsigned long element2) {
	unsigned long size1(0);
	unsigned long size2(0);

	size1 = element_size[element1];
	size2 = element_size[element2];
	element_size[element1] = size1 + size2;
	element_size.erase(element_size.begin() + element2, element_size.begin() + element2 + 1);
	return(size1 + size2);
};
//*********************************************************************************************************//


