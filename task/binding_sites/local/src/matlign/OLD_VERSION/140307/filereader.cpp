/***************************************************************************
 *   Copyright (C) 2006 by Ari Loytynoja   *
 *   ari@ebi.ac.uk   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "filereader.h"
#include <iostream>
#include <fstream>
#include <stdio.h>

using namespace std;

FileReader::FileReader()
{
}


FileReader::~FileReader()
{
}

IntMatrix* FileReader::readSequence(string s, int &min_length, string element_name)
{
	string t;
	string alpha = "ACGTRYMKWSBDHVN";
	int s_length(0);
	
	for(int i = 0; i < s.length(); ++i) {
		int ca = alpha.find(toupper(s.at(i)));	
		if (ca != string::npos) { ++s_length; }
	}
	min_length = s_length;
	
	if(s_length < 1) {
		cout<<"length zero!"<<endl;
		exit(-1);
	}

	IntMatrix* mat = new IntMatrix(s_length + 1,4,"counts");
	mat->initialise(0);
	mat->set_element_name(element_name);
	
	int i;
	FOR(i,s.length()) {
		int ci = alpha.find(toupper(s.at(i)));
		if(ci>=0 && ci<4) {
			mat->s(1,(i+1),ci);
		} else if(ci == 4) {
			mat->s(1,(i+1),0); mat->s(1,(i+1),2);
		} else if(ci == 5) {
			mat->s(1,(i+1),1); mat->s(1,(i+1),3);
		} else if(ci == 6) {
			mat->s(1,(i+1),1); mat->s(1,(i+1),0);
		} else if(ci == 7) {
			mat->s(1,(i+1),3); mat->s(1,(i+1),2);
		} else if(ci == 8) {
			mat->s(1,(i+1),3); mat->s(1,(i+1),0);
		} else if(ci == 9) {
			mat->s(1,(i+1),1); mat->s(1,(i+1),2);
		} else if(ci == 10) {
			mat->s(1,(i+1),1); mat->s(1,(i+1),3); mat->s(1,(i+1),2);
		} else if(ci == 11) {
			mat->s(1,(i+1),0); mat->s(1,(i+1),3); mat->s(1,(i+1),2);
		} else if(ci == 12) {
			mat->s(1,(i+1),0); mat->s(1,(i+1),3); mat->s(1,(i+1),1);
		} else if(ci == 13) {
			mat->s(1,(i+1),0); mat->s(1,(i+1),1); mat->s(1,(i+1),2);
		} else if(ci == 14) {
			mat->s(1,(i+1),0); mat->s(1,(i+1),1); mat->s(1,(i+1),2); mat->s(1,(i+1),3); 
		} else {
			//cout<<"unrecognised character: "<<s.at(i)<<endl;
			//exit(-1);
		}
	}
	return mat;
}

IntMatrix* FileReader::readMatrix(vector<string>& matrix, int &min_length, string element_name) 
{
	if(matrix.size() == 1) {
		cout<<"length zero!"<<endl;
		exit(-1);
	}
	min_length = matrix.size();
	
	IntMatrix* mat = new IntMatrix((matrix.size()+1),4,"counts");
	mat->initialise(0);
	mat->set_element_name(element_name);
	
	int i = 0;
	while (matrix.size() > 0) {
		string t = matrix.front();
		matrix.erase(matrix.begin());
		int e = t.find_first_of("\t",0);
		int v = atoi(t.substr(0,e).c_str());
		mat->s(v,(i+1),0);
		int s = e+1;
		e = t.find_first_of("\t",s);
		v = atoi(t.substr(s,e).c_str());
		mat->s(v,(i+1),1);
		s = e+1;
		e = t.find_first_of("\t",s);
		v = atoi(t.substr(s,e).c_str());
		mat->s(v,(i+1),2);
		s = e+1;
		v = atoi(t.substr(s).c_str());
		mat->s(v,(i+1),3);
		++i;
	}
	return mat;
}

IntMatrix* FileReader::readFile(string* file, vector<IntMatrix*>& input_mat_vector, int &min_length)
{
	int r 			= 1;
	int min_length_temp 	= 0;
	int is_string 		= 0;
	int is_first		= 1;
	int is_header		= 0;
	unsigned int j		= 0;
	unsigned int ci 	= 0;
	int matrix_counter	= 0;
	
	string element_name	= "";
	string previous_name	= "null";
	char buffer[10];
	
	vector <string> seq_mat;
	string t;
	string alpha = "ACGTMRWSYKVHDBN";
	string wildcards = "\n\t\r ";
	
	ifstream in(file->c_str());
	
	while(getline(in,t)){
		is_string = 0;
		unsigned int e = t.find_first_of(">", 0);
		
		if (e != string::npos) {
			ci = 0;	
			++matrix_counter;
			is_header = 1;
			
			element_name = "";
			for(unsigned int k = 0; k < t.length(); ++k) {
				ci = wildcards.find(t.at(k), 0);
				if(ci == string::npos) {
					element_name = element_name + t.substr(k, 1);
				}	
			}
			sprintf(buffer, "%i", (matrix_counter));
			element_name = element_name + "_nro" + string(buffer);
			if (previous_name == "null") { previous_name = element_name; }
			
			if ((seq_mat.size() > 1) && (is_string == 0)) { 
				if(previous_name.size() < 1) {
					sprintf(buffer, "%i", (matrix_counter+1));
					previous_name = ">matrix_nro" + string(buffer);
				}

				input_mat_vector.push_back(readMatrix(seq_mat, min_length_temp, previous_name)); 
			}
			seq_mat.clear(); ci = 0;
			previous_name = element_name;

		} else if (e == string::npos) {
			j = 0; ci = 0;
			while ((j < t.length()) && (ci != string::npos)) {
				ci = wildcards.find(t.at(j), 0);
				j++;
			}

			for(int i = 0; i < t.length(); ++i) {
				int ca = alpha.find(toupper(t.at(i)));
				if (ca != string::npos) { is_string = 1; }
			}
			
			if(j != t.length()) {
				if(is_string == 0) {
					seq_mat.push_back(t);
				} else {
					
					if(element_name.size() < 1) {
						sprintf(buffer, "%i", (matrix_counter+1));
						element_name = ">pattern_nro" + string(buffer);
					}
					
					seq_mat.push_back(t);
					input_mat_vector.push_back(readSequence(seq_mat.back(), min_length_temp, element_name));
			
					seq_mat.clear();
					if(is_header == 0) {
						++matrix_counter;
					}
					element_name = "";
					is_header = 0;
				}
			}
		}
	
		if ((min_length_temp < min_length) || (is_first == 1 && min_length_temp > 0)) {
			min_length = min_length_temp;
			is_first = 0;
		}
	}

	if((seq_mat.size() > 1) && (is_string == 0)) { 
		if(previous_name.size() < 1) {
			sprintf(buffer, "%i", (matrix_counter+1));
			element_name = ">matrix_nro" + string(buffer);
		}
		
		input_mat_vector.push_back(readMatrix(seq_mat, min_length_temp, previous_name)); 
		++matrix_counter;
		previous_name = element_name;
	}

	if ((min_length_temp < min_length) || (is_first == 1 && min_length_temp > 0)) {
		min_length = min_length_temp;
		is_first = 0;
	}
	//in.close();	
};


