/***************************************************************************
 *   Copyright (C) 2006 by Ari Loytynoja   *
 *   ari@ebi.ac.uk   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <iostream>
#include <vector>
#include <utility>
#include "viterbi.h"
#include "distance_array.h"
#include <math.h>

extern float match;
extern float transi;
extern float transv;
extern float gOpen;
extern float gExt;
extern int tLength;
extern int noise;
extern bool FIRST;
extern bool SECOND;
extern std::string norm;
extern int zscore;

extern float norm0_mean;
extern float norm0_std;
extern float norm1_mean;
extern float norm1_std;
extern float norm2_mean;
extern float norm2_std;
extern float norm3_mean;
extern float norm3_std;
extern float norm4_mean;
extern float norm4_std;
extern float norm5_mean;
extern float norm5_std;

using namespace std;
//*********************************************************************************************************//
Viterbi::~Viterbi()
{
    delete sScore;
}
//*********************************************************************************************************//
Viterbi::Viterbi(IntMatrix* st1, IntMatrix* st2)
{
    s1Single = true;
    s2Single = true;
    s1 = st1;
    s2 = st2;
    l1 = s1->X();
    l2 = s2->X();

    defineModel();
}
//*********************************************************************************************************//
Viterbi::Viterbi(IntMatrix* st1, FlMatrix* mt2)
{
    s1Single = true;
    s2Single = false;
    s1 = st1;
    m2 = mt2;
    l1 = s1->X();
    l2 = m2->X();

    defineModel();
}
//*********************************************************************************************************//
Viterbi::Viterbi(FlMatrix* mt1, FlMatrix* mt2)
{
    s1Single = false;
    s2Single = false;
    m1 = mt1;
    m2 = mt2;
    l1 = m1->X();
    l2 = m2->X();

    defineModel();
}

//*********************************************************************************************************//
void Viterbi::defineModel()
{
    sScore = new FlMatrix(4,4,"substScores");
    sScore->initialise(0);

    int i;
    FOR(i,4){
	sScore->s(match,i,i);
    }

    if(transi!=0) {
	sScore->s(transi,0,2);
	sScore->s(transi,1,3);
	sScore->s(transi,2,0);
	sScore->s(transi,3,1);
    }

    if(transv!=0) {
	sScore->s(transv,0,1);
	sScore->s(transv,0,3);
	sScore->s(transv,1,0);
	sScore->s(transv,1,2);
	sScore->s(transv,2,1);
	sScore->s(transv,2,3);
	sScore->s(transv,3,0);
	sScore->s(transv,3,2);
    }

    if(noise>0) {
	cout<<endl<<"Substitution scores"<<endl;
	sScore->print();
    }
}
//*********************************************************************************************************//
void Viterbi::fill(float* mean, float* std) {
	float mean_temp(0), std_temp(0);
	float sc;

	for(int i = 0; i < l1; ++i) {
		for(int j = 0; j < l2; ++j) {
			if(i>0 && j>0) {
				sc = this->score(i,j);
				mean_temp = mean_temp + sc;
				std_temp  = std_temp + pow(sc, 2);
			}
		}
	}

	*mean = mean_temp / ((l1-1) * (l2-1));
	*std = sqrt( (std_temp - (((l1-1)*(l2-1))*pow((*mean),2))) / ((l1-1)*(l2-1))  );
};
//*********************************************************************************************************//
void Viterbi::align(IntMatrix* path, float* score){
	FlMatrix* M1 = new FlMatrix(l1,l2,"M1");
	FlMatrix* X1 = new FlMatrix(l1,l2,"X1");
	FlMatrix* Y1 = new FlMatrix(l1,l2,"Y1");
	FlMatrix* M2 = new FlMatrix(l1,l2,"M2");

	IntMatrix* pX1 = new IntMatrix(l1,l2,"pX1");
	IntMatrix* pY1 = new IntMatrix(l1,l2,"pY1");
	IntMatrix* pM2 = new IntMatrix(l1,l2,"pM2");

	small = -HUGE_VAL;
	M1->initialise(small);
	X1->initialise(small);
	Y1->initialise(small);
	M2->initialise(small);

	pX1->initialise(-1);
	pY1->initialise(-1);
	pM2->initialise(-1);

	int i,j;
	FOR(i,min(tLength+1,l1)){
		M1->s(0,i,0);
	}
	FOR(j,min(tLength+1,l2)){
		M1->s(0,0,j);
	}

	float sc;
	FOR(i,l1) {
		FOR(j,l2) {
			if(i>0 && j>0) {
				X1->s( max(M1->g(i-1,j)+gOpen, X1->g(i-1,j)+gExt) , i, j );
				pX1->s( pt, i, j );

				Y1->s( max(M1->g(i,j-1)+gOpen, Y1->g(i,j-1)+gExt) , i, j );
				pY1->s( pt*2, i, j );
		
				sc = this->score(i,j);
				M1->s( M1->g(i-1,j-1)+sc, i, j );
				//M2->s( max(X1->g(i-1,j-1), Y1->g(i-1,j-1), M2->g(i-1,j-1)+sc), i, j );
				M2->s( max(X1->g(i-1,j-1), Y1->g(i-1,j-1), M2->g(i-1,j-1))+sc, i, j );
				pM2->s( pt+1, i, j );
			}
		}
	}

	if(noise>0) {
		cout<<"M1"<<endl;
		M1->print();
		cout<<"X1"<<endl;
		X1->print();
		cout<<"Y1"<<endl;
		Y1->print();
		cout<<"M2"<<endl;
		M2->print();
	}
    
	if(noise>1) {
		cout<<"pX1"<<endl;
		pX1->print();
		cout<<"pY1"<<endl;
		pY1->print();
		cout<<"pM2"<<endl;
		pM2->print();
	}

	sc = small;
	pt = -1;
	int mi = -1;
	int mj = -1;

	j = l2-1;
	for(i = l1-1;i>=l1-tLength-1 && i>0;i--){
		if((FIRST) && (M1->g(i,j)>sc)){
			//if(M1->g(i,j)>sc){
			sc = M1->g(i,j);
			pt=0;
			mi=i;
			mj=j;
		}
		if((SECOND) && (M2->g(i,j)>sc && pM2->g(i,j)==3)){
			//if(M2->g(i,j)>sc && pM2->g(i,j)==3){
			sc = M2->g(i,j);
			pt=3;
			mi=i;
			mj=j;
		}
	}

	i = l1-1;
	for(j = l2-1;j>=l2-tLength-1 && j>0;j--){
		if((FIRST) && (M1->g(i,j)>sc)){
			//if(M1->g(i,j)>sc){
			sc = M1->g(i,j);
			pt=0;
			mj=j;
			mi=i;
		}
		if((SECOND) && (M2->g(i,j)>sc && pM2->g(i,j)==3)){
			//if(M2->g(i,j)>sc && pM2->g(i,j)==3){
			sc = M2->g(i,j);
			pt=3;
			mj=j;
			mi=i;
		}
	}

	i = l1-1;
	j = l2-1;
	if((SECOND) && (M2->g(i-1,j-1)+this->score(i,j)>sc)){
		//if(M2->g(i-1,j-1)+this->score(i,j)>sc){
		sc = M2->g(i-1,j-1)+this->score(i,j);
		pt=3;
		mj=j;
		mi=i;
	}

	*score = sc;
	int k = 0;

	for(i=l1-1;mi>0 && i>mi;i--)
		path->s(1,k++);

	for(j=l2-1;mj>0 && j>mj;j--)
		path->s(2,k++);

	for(;i>=0,j>=0;){
		if(i==0 || j==0)
			break;

		path->s(pt,k++);

		if(pt==0) {
			i--;
			j--;
		} else if(pt==1) {
			pt = pX1->g(i,j);
			i--;
		} else if(pt==2) {
			pt = pY1->g(i,j);
			j--;
		} else if(pt==3) {
			pt = pM2->g(i,j);
			i--;
			j--;
		}
	}

	for(;i>0;i--)
		path->s(1,k++);

	for(;j>0;j--)
		path->s(2,k++);

	delete M1;
	delete X1;
	delete Y1;
	delete M2;

	delete pX1;
	delete pY1;
	delete pM2;
}
//*********************************************************************************************************//
bool SortDistanceArrayByValue(const DistanceArray &a, const DistanceArray &b) {
	return a.value > b.value;
};
//*********************************************************************************************************//
bool SortDistanceArrayByOrigRank(const DistanceArray &a, const DistanceArray &b) {
	return a.orig_rank < b.orig_rank;
};
//*********************************************************************************************************//
bool SortDistanceArrayByRealRank(const DistanceArray &a, const DistanceArray &b) {
        return a.real_rank < b.real_rank;
};
//*********************************************************************************************************//
float Viterbi::score(int i, int j) {
	float mScore = 0;
	float total_count = 0;
	float m1_itself_score = 0;
	float m2_itself_score = 0;
	float scale_score = 0;

	float euclidean = 0, spearman = 0, kendall = 0, pearson = 0, ysra = 0;
	float pearson_avg1 = 0, pearson_avg2 = 0, pearson_std1 = 0, pearson_std2 = 0;
	float spearman_avg1 = 0, spearman_avg2 = 0, spearman_std1 = 0, spearman_std2 = 0;
	float rank_counter = 0;
	int this_norm;

	double qval(0), pval(0), comb_zscore(0);

	//this_norm == 0 = Kendall's tau correlation
	//this_norm == 1 = Spearman's rank correlation (un-corrected)
	//this_norm == 2 = Spearman's rank correlation (corrected)
	//this_norm == 3 = Pearson's correlation
	//this_norm == 4 = Euclidean correlation
	//this_norm == 5 = Dynamic programming
	//this_norm == 6 = YSRA

	float total_score 	= 1;
	float added_score 	= 0;

	float neg_counter 	= 0;
	float pos_counter 	= 0;
	float neg_sum 		= 0;
	float pos_sum		= 0;

	for(int k = 0; k < norm.size(); ++k) {
		this_norm = atoi(norm.substr(k, 1).c_str());

		if(this_norm == 0) { //Kendall's tau distance (number of sort operations)
			vector <DistanceArray> matrix1_diag;
			vector <DistanceArray> matrix2_diag;
			{
				DistanceArray temp_cell;
				for(int(m) = 0; m < 4; ++m) {
					temp_cell.value		= m1->g(i,m);
					temp_cell.orig_rank	= m;
					matrix1_diag.push_back(temp_cell);
				}

				sort(matrix1_diag.begin(), matrix1_diag.end(), SortDistanceArrayByValue);

				for(int(m) = 0; m < 4; ++m) {
					temp_cell.value		= m2->g(j, int(matrix1_diag[m].orig_rank));
					temp_cell.orig_rank	= 0;
					matrix2_diag.push_back(temp_cell);
				}
			}

			for(int(m) = 0; m < 4; ++m) {
				for(int(n) = (m+1); n < 4; ++n) {
					if ((matrix2_diag[m].value >= matrix2_diag[n].value)) { kendall = kendall + 1; }
				}
			}

			//*** The above counter is not exactly the one given by Wikipedia, since sort-function was overwritten !  ***//	
			kendall = kendall / ((4*3)/2);	//This value is between 0 and 1 [0...1] where 0 means identical vectors//
			kendall = kendall * 2;		//[0...2]
			kendall = kendall - 1;		//[-1...1]
	
			matrix1_diag.clear();
			matrix2_diag.clear();
			added_score = ((kendall - norm0_mean) / norm0_std);

		} else if (this_norm == 1 || this_norm == 2 ) {	//Spearman's rank correlation
			vector <DistanceArray> matrix1_diag;
			vector <DistanceArray> matrix2_diag;

    			{
    				DistanceArray temp_cell;
    				for(int(m) = 0; m < 4; ++m) {
        				temp_cell.value		= m1->g(i,m);
					temp_cell.orig_rank	= m;
					matrix1_diag.push_back(temp_cell);
	
					temp_cell.value 	= m2->g(j,m);
					temp_cell.orig_rank  	= m;
					matrix2_diag.push_back(temp_cell);
    				}

				sort(matrix1_diag.begin(), matrix1_diag.end(), SortDistanceArrayByValue);
				sort(matrix2_diag.begin(), matrix2_diag.end(), SortDistanceArrayByValue);
    			}    

			for(int(m) = 0; m < 2; ++m) {
				rank_counter = 0;
				for(int(n) = 0; n < 4; ++n) {
					if ((m == 0) && (n > 0) && (matrix1_diag[n].value != matrix1_diag[n-1].value)) { rank_counter = n; }
					if ((m == 1) && (n > 0) && (matrix2_diag[n].value != matrix2_diag[n-1].value)) { rank_counter = n; }

					if (m == 0) { matrix1_diag[n].real_rank = rank_counter; }
					if (m == 1) { matrix2_diag[n].real_rank = rank_counter; }
				}
			}

			sort(matrix1_diag.begin(), matrix1_diag.end(), SortDistanceArrayByOrigRank);
			sort(matrix2_diag.begin(), matrix2_diag.end(), SortDistanceArrayByOrigRank);
		
			//THIS CAN BE USED WHEN THERE ARE NO TIES (ACCORDING TO WIKIPEDIA)///
			//Spearman's correlation is defined as [-1...1] where 1 is identical//
			if (this_norm == 1) {
				for(int(m) = 0; m < 4; ++m) {
					spearman = spearman + pow((matrix1_diag[m].real_rank - matrix2_diag[m].real_rank),2);
				}
				spearman = 1 - ((6 * spearman) / 60); 
				added_score = ((spearman - norm1_mean) / norm1_std);

			} else if (this_norm == 2) {		
				for(int(m) = 0; m < 4; ++m) {
					spearman_avg1 = spearman_avg1 + matrix1_diag[m].real_rank;
					spearman_avg2 = spearman_avg2 + matrix2_diag[m].real_rank;
				}
				spearman_avg1 = spearman_avg1 / 4;
				spearman_avg2 = spearman_avg2 / 4;
			
				spearman = 0;
				for(int(m) = 0; m < 4; ++m) {
					spearman += ((matrix1_diag[m].real_rank - spearman_avg1) * (matrix2_diag[m].real_rank - spearman_avg2));
					spearman_std1 += pow((matrix1_diag[m].real_rank - spearman_avg1),2);
					spearman_std2 += pow((matrix2_diag[m].real_rank - spearman_avg2),2);
				}
				if (spearman_std1 == 0) { spearman_std1 = 1; }
				if (spearman_std2 == 0) { spearman_std2 = 1; }

				spearman = spearman / sqrt(spearman_std1*spearman_std2);
				added_score = ((spearman - norm2_mean) / norm2_std);
			}

			matrix1_diag.clear();
			matrix2_diag.clear();

		} else if (this_norm == 3) { // Pearson's correlation, Pietrokovski NAR 1996
			for(int(m) = 0; m < 4; ++m) {
				pearson_avg1 = pearson_avg1 + (m1->g(i,m));
				pearson_avg2 = pearson_avg2 + (m2->g(j,m));	
			}
			pearson_avg1 = pearson_avg1 / 4;
			pearson_avg2 = pearson_avg2 / 4;	

			for(int(m) = 0; m < 4; ++m) {
				pearson = pearson + ((m1->g(i,m) - pearson_avg1) * (m2->g(j,m) - pearson_avg2));
				pearson_std1 = pearson_std1 + pow((m1->g(i,m) - pearson_avg1),2);
				pearson_std2 = pearson_std2 + pow((m2->g(j,m) - pearson_avg2),2);
			}
			if (pearson_std1 == 0) { pearson_std1 = 1; }
			if (pearson_std2 == 0) { pearson_std2 = 1; }

			pearson = pearson / sqrt(pearson_std1*pearson_std2);
			added_score = ((pearson - norm3_mean) / norm3_std);

		} else if (this_norm == 4) { //Euclidean correlation [0...sqrt(max)], normalized to be between [-1...1]
			for(int(m) = 0; m < 4; ++m) {
				euclidean = euclidean + pow((m1->g(i,m) - (m2->g(j,m))), 2);
			}
			euclidean = sqrt(euclidean);
			//euclidean = euclidean * -1;
			euclidean = euclidean / sqrt(2.0)* 2;	//max of euc distance, in here it is sqrt(2.0)
			euclidean = (euclidean - 1) * -1;	//normalization
			added_score = ((euclidean - norm4_mean) / norm4_std);
    	
		} else if (this_norm == 5) {
			if(s1Single && s2Single) {
				mScore = sScore->g(s1->g(i),s2->g(j));
			} else if(s1Single) {
				for(int(m) = 0; m < 4; ++m) {
					mScore += sScore->g(s1->g(i),m)*m2->g(j,m);
				}
			} else {
				for(int(m) = 0; m < 4; ++m) {
					for(int(n) = 0; n < 4; ++n) {
						mScore += sScore->g(n,m) * m1->g(i,n) *  m2->g(j,m);
					}
				}
			}

			added_score = ((mScore - norm5_mean) / norm5_std);

		} else if (this_norm == 6) {	//YSRA-method, used in JASPAR database
			for(int(m) = 0; m < 4; ++m) {
				ysra += pow((m1->g(i,m) - m2->g(j,m)),2);
			}
			return(2 - ysra);
		} else {
			return(1);
		}

		//make this using abs-function, it is more elegant//
		total_score = total_score * added_score;
		if (added_score < 0) {
			++neg_counter; 
			neg_sum = neg_sum + (added_score * -1); 
		} else if ( added_score >= 0) {
			++pos_counter;
			pos_sum = pos_sum + added_score;
		} 
	}
	
	if (zscore == 1) {
	if (neg_counter > pos_counter) {
		if (total_score > 0) { total_score = total_score * -1; }
	} else if (neg_counter < pos_counter) {
		if (total_score < 0) { total_score = total_score * -1; }
	} else if (neg_counter == pos_counter) {
		if (neg_sum > pos_sum) { 
			if (total_score > 0) { total_score = total_score * -1; }
		} else if (neg_sum < pos_sum) {
			if (total_score < 0) { total_score = total_score * -1; }
		}
	}
	}

	return(total_score);
};
//*********************************************************************************************************//
float Viterbi::max(float a,float b)
{
    if(a==small && b==small) {
	pt = 0;
	return a;
    } else if(a>b) {
	pt = 0;
	return a;
    } else if(a<b) {
	pt = 1;
	return b;
    } else {
	if(rndBool()) {
	    pt = 0;
	    return a;
	} else {
	    pt = 1;
	    return b;
	}
    }
};
//*********************************************************************************************************//
float Viterbi::max(float a,float b, float c) 
{
    if(a==small && b==small && c==small) {
	pt = 0;
	return a;
    } else if(a>b && a>c) {
	pt = 0;
	return a;
    } else if(a<b && b>c) {
	pt = 1;
	return b;
    } else if(a<c && b<c) {
	pt = 2;
	return c;
    } else if(a>b && a==c) {
	if(rndBool()) {
	    pt = 0;
	    return a;
	} else {
	    pt = 2;
	    return c;
	}
    } else if(a>c && a==b) {
	if(rndBool()) {
	    pt = 0;
	    return a;
	} else {
	    pt = 1;
	    return b;
	}
    } else if(a<b && b==c) {
	if(rndBool()) {
	    pt = 1;
	    return b;
	} else {
	    pt = 2;
	    return c;
	}
    } else {
	int i = rndInt(3);
	pt = i;
	if(i==0 || i==3){
	    return a;
	} else if(i==1){
	    return b;
	} else if(i==2){
	    return c;
	} else {
	    cout <<"random number error: i="<<i<<endl;
	    exit(1);
	}
    }
};
//*********************************************************************************************************//
bool Viterbi::rndBool() 
{
    double p = (double)rand()/(double)RAND_MAX;
    if(p>0.5)
	return true;
    else 
	return false;
};
//*********************************************************************************************************//
int Viterbi::rndInt(int i) 
{
    return (int)(rand()/(double)RAND_MAX*(double)i);
};
//*********************************************************************************************************//
