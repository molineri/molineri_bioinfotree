#include <string>
#include "clusterresults.h"

using namespace std;

//*********************************************************************************************************//
ClusterResults::ClusterResults() {
	new_element 		= "";
	element1 		= "";
	element2 		= "";
	element_distance 	= 0;	
	consensus		= "";
};
//*********************************************************************************************************//
ClusterResults::~ClusterResults() {
	new_element 		= "";
	element1 		= "";
	element2 		= "";
	element_distance 	= 0;
	consensus		= "";
};
//*********************************************************************************************************//
void ClusterResults::SetElement(string name_new, string name_element1, string name_element2, string name_consensus, double value) {
	new_element 		= name_new;
	element1 		= name_element1;
	element2 		= name_element2;
	element_distance 	= value;
	consensus		= name_consensus;
};
//*********************************************************************************************************//
void ClusterResults::GetElement(string &name_new, string &name_element1, string &name_element2, string &name_consensus, double &value) {
	name_new 		= new_element;
	name_element1 		= element1;
	name_element2 		= element2;
	value 			= element_distance;
	name_consensus		= consensus;
};
//*********************************************************************************************************//
