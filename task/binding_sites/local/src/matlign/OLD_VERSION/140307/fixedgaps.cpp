/***************************************************************************
 *   Copyright (C) 2006 by Ari Loytynoja   *
 *   ari@ebi.ac.uk   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <cmath>
#include <fstream>
#include "flmatrix.h"
#include "intmatrix.h"
#include "viterbi.h"
#include "filereader.h"
#include "distancematrix.h"
#include "clusterresults.h"
#include "clusterinfo.h"

using namespace std;

float match;
float transi;
float transv;
float gOpen;
float gExt;
int tLength;
int noise;
int this_norm;
string norm;
int spacer;
int mode;
int zscore;

float norm0_mean;
float norm0_std;
float norm1_mean;
float norm1_std;
float norm2_mean;
float norm2_std;
float norm3_mean;
float norm3_std;
float norm4_mean;
float norm4_std;
float norm5_mean;
float norm5_std;

string file;
string out;
bool FIRST;
bool SECOND;
//*********************************************************************************************************************//
IntMatrix* calc_alignment_score(IntMatrix*, IntMatrix*, float&, float&);
int calc_z_score(IntMatrix*, IntMatrix*, float&, float&);
int calc_all_scores();
int get_tLength(int, int, int, int, int&, int&);
int calculate_silhouette_value(vector<ClusterInfo>& clst_inf, vector<float>& slh_val, DistanceMatrix& slh_dist, unsigned long, float, FILE*);
void print_cluster_variance(vector<ClusterInfo>& cluster_info, FILE*);
void print_cluster_silhouette(vector<float>& silhouette_value, FILE*, unsigned long, float);
//*********************************************************************************************************************//
int main(int argc, char *argv[]) {
	bool e		= 0;
	int r		= 0;
	match 		= 10;		//DEFAULT VALUE
	transi 		= -4;		//DEFAULT VALUE
	transv 		= -4;		//DEFAULT VALUE
	gOpen 		= -10;		//DEFAULT VALUE
	gExt 		= -1;		//DEFAULT VALUE
	tLength 	= 5;		//DEFAULT VALUE
	noise 		= 0;		//DEFAULT VALUE
	out 		= "outfile";	//DEFAULT VALUE
	norm		= 5;		//DEFAULT VALUE (no normalization)
	spacer		= 1;		//DEFAULT VALUE (spacer is enabled)
	mode		= 0;		//DEFAULT VALUE (do not cluster patterns)
	this_norm	= 5;
	zscore		= 0;		//DEFAULT VALUE (do not use z-scores)

	norm0_mean 	= 0;
	norm0_std 	= 1;
	norm1_mean 	= 0;
	norm1_std 	= 1;
	norm2_mean	= 0;
	norm2_std	= 1;
	norm3_mean 	= 0;
	norm3_std 	= 1;
	norm4_mean 	= 0;
	norm4_std 	= 1;
	norm5_mean 	= 0;
	norm5_std 	= 1;

	int seed 	= time(NULL);
	srand ( seed );	
	//cout << seed << "\n";
	for(int i=1;i<argc;i++) {
		string s = string(argv[i]);

		if(s.substr(0,7)=="-input=") {
			file = s.substr(7);
			e = 1;
		} else if(s.substr(0,7)=="-match=") {
			match = atof(s.substr(7).c_str());
		} else if(s.substr(0,8)=="-transi=") {
			transi = atof(s.substr(8).c_str());
		} else if(s.substr(0,8)=="-transv=") {
			transv = atof(s.substr(8).c_str());
		} else if(s.substr(0,7)=="-gopen=") {
			gOpen = atof(s.substr(7).c_str());
		} else if(s.substr(0,6)=="-gext=") {
			gExt = atof(s.substr(6).c_str());
		} else if(s.substr(0,6)=="-term=") {
			tLength = atoi(s.substr(6).c_str());
		} else if(s.substr(0,5)=="-out=") {
			out = s.substr(5);
		} else if(s.substr(0,7)=="-noise=") {
			noise = atoi(s.substr(7).c_str());
		} else if(s.substr(0,6)=="-norm=") {
			norm = (s.substr(6).c_str());
		} else if(s.substr(0,8)=="-spacer=") {
			spacer = atoi(s.substr(8).c_str());
		} else if(s.substr(0,6)=="-mode=") {
			mode = atoi(s.substr(6).c_str());
                } else if(s.substr(0,8)=="-zscore=") {
                        zscore = atoi(s.substr(8).c_str());
		} else {
			cout<<"unrecognised argument: "<<s<<endl;
		}
    	}

	if (spacer == 0) {
		FIRST = 1; SECOND = 0;  //spacer is disabled
	} else if (spacer == 1) {
		FIRST = 1; SECOND = 1;  //spacer is enabled
	} else if (spacer == 2) {
		FIRST = 0; SECOND = 1;  //spacer is required
	} else {
		FIRST = 1; SECOND = 1;  //spacer is enabled
	}

	if (e == 1) {
		r = calc_all_scores();
	}
}
//*********************************************************************************************************************//
int calc_all_scores () {
	IntMatrix* p0;
	IntMatrix* p1;
	//IntMatrix* pT;

	char buffer[10];

	int min_length(0), min_temp_len(0), min_l(0), max_l(0), n_mats(0);
	int parameter_tlength = tLength;	
	
	unsigned long row_max_dist(0);
	unsigned long col_max_dist(0);
	unsigned long cluster_size(0);

	double pair_score(0);
	float score(0), added_score(0);
	float cluster_min(0), cluster_max(0);
	float cluster_mean(0), cluster_mean_tot(0);
	float cluster_var(0), cluster_var_tot(0);
	float temp_max(0), temp_min(0), temp_score(0);
	float align_cols(0);
	float* norm_mean(0); 
	float* norm_std(0);

	FILE *pswm_stream;
	FILE *mtrx_stream;
	FILE *tree_stream;
	FILE *vara_stream;

	string mtrx_file = out + ".mtrx";
	string tree_file = out + ".tree";
	string pswm_file = out + ".pswm";
	string vara_file = out + ".vara";

	string zscore_pattern;
	string element1;
	string element2;
	string new_element;
	string consensus;
	string temp_norm;	

	vector <string> mat_names;
	vector <string> zscore_patterns1;
	vector <string> zscore_patterns2;
	vector <ClusterResults> cluster_results;
	vector <IntMatrix*> input_mat_vector;
	//vector <IntMatrix*> original_mat_vector;
	vector <ClusterInfo> cluster_info;
	vector <float> silhouette_value;
	vector <int> zscore_rand;

	FileReader* fr = new FileReader();

	if(zscore == 1) {
		zscore_patterns1.push_back("1\t0\t0\t0\n"); zscore_patterns2.push_back("1\t0\t0\t0\n");
		zscore_patterns1.push_back("0\t1\t0\t0\n"); zscore_patterns2.push_back("0\t1\t0\t0\n");
		zscore_patterns1.push_back("0\t0\t1\t0\n"); zscore_patterns2.push_back("0\t0\t1\t0\n");
		zscore_patterns1.push_back("0\t0\t0\t1\n"); zscore_patterns2.push_back("0\t0\t0\t1\n");
		zscore_patterns1.push_back("1\t1\t0\t0\n"); zscore_patterns2.push_back("1\t1\t0\t0\n");
		zscore_patterns1.push_back("1\t0\t1\t0\n"); zscore_patterns2.push_back("1\t0\t1\t0\n");
		zscore_patterns1.push_back("1\t0\t0\t1\n"); zscore_patterns2.push_back("1\t0\t0\t1\n");
		zscore_patterns1.push_back("0\t1\t1\t0\n"); zscore_patterns2.push_back("0\t1\t1\t0\n");
		zscore_patterns1.push_back("0\t1\t0\t1\n"); zscore_patterns2.push_back("0\t1\t0\t1\n");
		zscore_patterns1.push_back("0\t0\t1\t1\n"); zscore_patterns2.push_back("0\t0\t1\t1\n");
		zscore_patterns1.push_back("1\t1\t1\t0\n"); zscore_patterns2.push_back("1\t1\t1\t0\n");
		zscore_patterns1.push_back("1\t1\t0\t1\n"); zscore_patterns2.push_back("1\t1\t0\t1\n");
		zscore_patterns1.push_back("1\t0\t1\t1\n"); zscore_patterns2.push_back("1\t0\t1\t1\n");
		zscore_patterns1.push_back("0\t1\t1\t1\n"); zscore_patterns2.push_back("0\t1\t1\t1\n");
		zscore_patterns1.push_back("1\t1\t1\t1\n"); zscore_patterns2.push_back("1\t1\t1\t1\n");

		//p0 = fr->readSequence("ACGTRYSWKMBDHVN", min_length, "zscore");
		//tLength = get_tLength(((*p0).X()-1), ((*p0).X()-1), spacer, parameter_tlength, min_l, max_l);

		p0 = fr->readMatrix(zscore_patterns1, min_length, "zscore");
		p1 = fr->readMatrix(zscore_patterns2, min_length, "zscore");
		tLength = get_tLength(((*p0).X()-1), ((*p1).X()-1), spacer, parameter_tlength, min_l, max_l);

		temp_norm = norm;
		for(int i = 0; i < temp_norm.size(); ++i) {
			norm = temp_norm.substr(i, 1).c_str();
	
			if(atoi(norm.c_str()) == 0) { norm_mean = &norm0_mean; norm_std = &norm0_std; }
			if(atoi(norm.c_str()) == 1) { norm_mean = &norm1_mean; norm_std = &norm1_std; }
			if(atoi(norm.c_str()) == 2) { norm_mean = &norm2_mean; norm_std = &norm2_std; }
			if(atoi(norm.c_str()) == 3) { norm_mean = &norm3_mean; norm_std = &norm3_std; }
			if(atoi(norm.c_str()) == 4) { norm_mean = &norm4_mean; norm_std = &norm4_std; }
			if(atoi(norm.c_str()) == 5) { norm_mean = &norm5_mean; norm_std = &norm5_std; }
		
			if(atoi(norm.c_str()) < 6 && atoi(norm.c_str()) >= 0) {
				if(*norm_mean != 0)	{ *norm_mean = 0; } 
				if(*norm_std  != 1)	{ *norm_std  = 1; } 
				calc_z_score(p0, p0, *norm_mean, *norm_std); 
			}
		}
		norm = temp_norm;
	}

	fr->readFile(&file, input_mat_vector, min_length);
	//fr->readFile(&file, original_mat_vector, min_temp_len);
	if(input_mat_vector.size() < 2) { return (0); }
	
	n_mats = input_mat_vector.size();
	DistanceMatrix dist_mat;
	DistanceMatrix silhouette;
	dist_mat.SetSize(n_mats, n_mats);
	silhouette.SetSize(n_mats, n_mats);

	pswm_stream = fopen(pswm_file.c_str(), "w+");
	mtrx_stream = fopen(mtrx_file.c_str(), "w+");
	tree_stream = fopen(tree_file.c_str(), "w+");
	vara_stream = fopen(vara_file.c_str(), "w+");

	if (pswm_stream == NULL) { fprintf (stderr, "ERROR 1: FILE NOT FOUND %s\n", pswm_file.c_str()); exit(0); }
	if (ferror(pswm_stream)) { fprintf (stderr, "ERROR 2: FILE NOT FOUND %s\n", pswm_file.c_str()); exit(0); }
	
	if (mtrx_stream == NULL) { fprintf (stderr, "ERROR 1: FILE NOT FOUND %s\n", mtrx_file.c_str()); exit(0); }
	if (ferror(mtrx_stream)) { fprintf (stderr, "ERROR 2: FILE NOT FOUND %s\n", mtrx_file.c_str()); exit(0); }
	
	if (tree_stream == NULL) { fprintf (stderr, "ERROR 1: FILE NOT FOUND %s\n", tree_file.c_str()); exit(0); }
	if (ferror(tree_stream)) { fprintf (stderr, "ERROR 2: FILE NOT FOUND %s\n", tree_file.c_str()); exit(0); }
        
	if (vara_stream == NULL) { fprintf (stderr, "ERROR 1: FILE NOT FOUND %s\n", vara_file.c_str()); exit(0); }
	if (ferror(vara_stream)) { fprintf (stderr, "ERROR 2: FILE NOT FOUND %s\n", vara_file.c_str()); exit(0); }

	for(int i = 0; i < input_mat_vector.size(); ++i) {
		(*input_mat_vector[i]).get_element_name(element1);
		mat_names.push_back(element1);

		consensus = (*input_mat_vector[i]).get_consensus();
		(*input_mat_vector[i]).print(element1, consensus, pswm_stream);

		for(int j = (i+1); j < input_mat_vector.size(); ++j) {
			tLength = get_tLength(((*input_mat_vector[i]).X()-1), ((*input_mat_vector[j]).X()-1), spacer, 
						parameter_tlength, min_l, max_l);

			p0 = calc_alignment_score(input_mat_vector[i], input_mat_vector[j], added_score, align_cols);
			score = added_score;
			delete p0;

			align_cols = 1;	//comment this, if you wish to normalize scores //
			dist_mat.SetElement(i, j, (score/align_cols));
			dist_mat.SetElement(j, i, (score/align_cols));

			if (i == 0 && j == 1) { temp_max = score; temp_min = score; }
			if (score >= temp_max) { temp_max = score; }
			if (score <= temp_min) { temp_min = score; }
		}
		
		{
			ClusterInfo this_cluster_info;
			this_cluster_info.SetElement(i, -1);
			cluster_info.push_back(this_cluster_info);
		}

		dist_mat.AddElementSize(1);
		silhouette_value.push_back(0);
	}

	dist_mat.PrintMatrix(mtrx_stream);

	if(mode == 0) { 
		dist_mat.FreeMemory();
		silhouette.FreeMemory();

		fclose(tree_stream);
		fclose(mtrx_stream);
		fclose(pswm_stream);
		fclose(vara_stream);
		
		delete fr;

		input_mat_vector.clear();
		cluster_results.clear();
		cluster_info.clear();
		silhouette_value.clear();
		return(1);
	}

	//NORMALIZE SILHOUETTE TABLE//
	for(int i = 0; i < input_mat_vector.size(); ++i) {
		for(int j = (i+1); j < input_mat_vector.size(); ++j) {
			temp_score = dist_mat.GetElement(i, j);
			temp_score = (temp_score - temp_min)/(temp_max - temp_min);
			temp_score = (1 - temp_score); //0 is a perfect correlation (distance is null) whereas 1 is no corr at all//

			silhouette.SetElement(j, i, temp_score);
			silhouette.SetElement(i, j, temp_score);
		}
	}

        print_cluster_silhouette(silhouette_value, vara_stream, input_mat_vector.size(), temp_max);
        //print_cluster_variance(cluster_info, vara_stream);

	for (unsigned int i = 0; i < (n_mats -1); ++i) {
		sprintf(buffer, "%i", (i+1));
		new_element = ">node_nro" + string(buffer);

		pair_score = dist_mat.FindMax(row_max_dist, col_max_dist);

		for(int j = 0; j < input_mat_vector.size(); ++j) {
			if(j != row_max_dist) {
				score = dist_mat.GetElement(row_max_dist, j) + dist_mat.GetElement(col_max_dist, j);
				dist_mat.SetElement(row_max_dist, j, score);
				dist_mat.SetElement(j, row_max_dist, score);
			}
		}

		dist_mat.MergeElementSize(row_max_dist, col_max_dist);
		dist_mat.RemoveElement(col_max_dist);
		dist_mat.PrintMatrix(mtrx_stream);		

		tLength = get_tLength(((*input_mat_vector[row_max_dist]).X()-1), ((*input_mat_vector[col_max_dist]).X()-1), 
					spacer, parameter_tlength, min_l, max_l);
		p0 = calc_alignment_score(input_mat_vector[row_max_dist], input_mat_vector[col_max_dist], score, align_cols);
		consensus = (*p0).get_consensus();
		(*p0).print(new_element, consensus, pswm_stream);

		delete input_mat_vector[row_max_dist];
		delete input_mat_vector[col_max_dist];
		input_mat_vector[row_max_dist] = p0;
		input_mat_vector.erase(input_mat_vector.begin() + col_max_dist, input_mat_vector.begin() + col_max_dist + 1);
		
		element1 = mat_names[row_max_dist];
		element2 = mat_names[col_max_dist];
		mat_names.erase(mat_names.begin() + col_max_dist, mat_names.begin() + col_max_dist + 1);
		mat_names[row_max_dist] = new_element;

		{
			ClusterResults this_cluster_result;
			this_cluster_result.SetElement(new_element, element1, element2, consensus, pair_score);
			cluster_results.push_back(this_cluster_result);
		}
		
		for(unsigned int j = 0; j < cluster_info[col_max_dist].GetSize(); ++j ) {
			cluster_info[row_max_dist].SetElement(cluster_info[col_max_dist].GetElement(j));
		}
		cluster_info.erase(cluster_info.begin() + col_max_dist, cluster_info.begin() + col_max_dist + 1);

		//CALCULATE SILHOUETTE VALUE//
		calculate_silhouette_value(cluster_info, silhouette_value, silhouette, input_mat_vector.size(), pair_score, vara_stream);

		//CALCULATE MIN, MAX, MEAN AND VARIANCE WITHIN THE CLUSTER WHEN COMPARED TO THE CENTER OF CLUSTER (pT)//
		//cluster_info[row_max_dist].ClearElementStat();
		//for(unsigned int j = 0; j < cluster_info[row_max_dist].GetSize(); ++j ) {
		//	pT = calc_alignment_score(p0, original_mat_vector[cluster_info[row_max_dist].GetElement(j)], score, align_cols);
		//	cluster_info[row_max_dist].SetElementStat(score, score);
		//}
		//print_cluster_variance(cluster_info, vara_stream);
	}

	for(unsigned int i = 0; i < cluster_results.size(); ++i) {
		cluster_results[i].GetElement(new_element, element1, element2, consensus, pair_score);
		fprintf(tree_stream, "%s\t%s\t%s\t%.20f\t%s\n", new_element.c_str(), element1.c_str(), element2.c_str(), 
							       pair_score, consensus.c_str());
	}

	//for(unsigned int i = 0; i < original_mat_vector.size(); ++i) {
	//	delete original_mat_vector[i];
	//}

	dist_mat.FreeMemory();
	silhouette.FreeMemory();

	fclose(tree_stream);
	fclose(mtrx_stream);
	fclose(pswm_stream);
	fclose(vara_stream);
	
 	delete fr;
	delete p0;
	delete p1;
	//delete pT;

	//original_mat_vector.clear();
	input_mat_vector.clear();
	cluster_results.clear();
	cluster_info.clear();
	silhouette_value.clear();
	return(1);
};
//*********************************************************************************************************************//
//calculate_silhouette_value(cluster_info, silhouette_value, silhouette, input_mat_vector.size(), pair_score, vara_stream);
int calculate_silhouette_value(vector<ClusterInfo>& clst_inf, vector<float>& slh_val, DistanceMatrix& slh_dist, unsigned long cluster_number, float pair_score, FILE *outstream) {
	float a1(0), b1(0), b1t(0);	

	for(unsigned int j = 0; j < clst_inf.size(); ++j ) {
		for(unsigned int k = 0; k < clst_inf[j].GetSize(); ++k ) {
			a1 = 0;
			for(unsigned int m = 0; m < clst_inf[j].GetSize(); ++m ) {
				if (k != m) {
					a1 = a1 + slh_dist.GetElement(clst_inf[j].GetElement(k), clst_inf[j].GetElement(m));
				}
			}
			a1 = (a1 / clst_inf[j].GetSize());

			b1 = HUGE_VAL;
			for(unsigned int m = 0; m < clst_inf.size(); ++m ) {
				if (m != j) {
					b1t = 0;
					for(unsigned int n = 0; n < clst_inf[m].GetSize(); ++n ) {
						b1t = b1t + slh_dist.GetElement(clst_inf[j].GetElement(k), clst_inf[m].GetElement(n));
					}
					b1t = (b1t / clst_inf[m].GetSize());

					//if (m == 0) { b1 = b1t; }
					//if (j == 0 && m == 1) { b1 = b1t; }
					if (b1t <=  b1) { b1 = b1t; }
				}
			}

			if ((a1 == 0) && (b1 == 0)) {
				slh_val[(clst_inf[j].GetElement(k))] = 0;
			} else if (clst_inf[j].GetSize() == 1) {
				slh_val[(clst_inf[j].GetElement(k))] = 0;
			} else if (a1 >= b1) {
				slh_val[(clst_inf[j].GetElement(k))] = ((b1 - a1) / a1);
			} else {
				slh_val[(clst_inf[j].GetElement(k))] = ((b1 - a1) / b1);
			}
		}
	}
	print_cluster_silhouette(slh_val, outstream, cluster_number, pair_score);
	return(1);
};
//*********************************************************************************************************************//
int get_tLength(int length_pat1, int length_pat2, int spacer, int parameter, int &min, int &max) {
	int length(0);      
	int length_diff(0);

	if (length_pat1 >= length_pat2) { length_diff = length_pat1 - 1; min = length_pat2; max = length_pat1; }
	if (length_pat1 <  length_pat2) { length_diff = length_pat2 - 1; min = length_pat1; max = length_pat2; }

	if((spacer == 1) && (parameter > length_diff)) {
		length = length_diff;
	} else if ((spacer == 1) && (parameter <= length_diff)) {
		length = parameter;
	} else if (spacer == 0) {
		length = length_diff;
	} else {
		length = 0;
	}

//cout << length_diff << " " << parameter << " " << length << "\n";
	return(length);
};
//*********************************************************************************************************************//
void print_cluster_silhouette(vector<float>& silhouette_value, FILE *outstream, unsigned long cluster_number, float depth) {
	float silhouette_temp(0);
                
	for(unsigned int j = 0; j < silhouette_value.size(); ++j ) {
		silhouette_temp = silhouette_temp + silhouette_value[j];
		//fprintf (outstream, "%.3f ",  silhouette_value[j] );
	}
	//fprintf (outstream, "\n\n");	

	silhouette_temp = silhouette_temp / silhouette_value.size();
	if (cluster_number == 1) { silhouette_temp = -1.00; }
	fprintf (outstream, "%u\t%.20f\t%.20f\n", cluster_number, silhouette_temp, depth );
};
//*********************************************************************************************************************//
void print_cluster_variance(vector<ClusterInfo>& cluster_info, FILE *outstream) {
	float cluster_min(0), cluster_max(0), cluster_mean(0), cluster_var(0);
	float cluster_mean_tot(0), cluster_var_tot(0);
	unsigned long cluster_size(0);

	for(unsigned int i = 0; i < cluster_info.size(); ++i) {
		cluster_info[i].CalcElementStat(cluster_min, cluster_max, cluster_mean, cluster_var, cluster_size);
		//cluster_info[i].PrintElement(outstream, 1);
		cluster_mean_tot = cluster_mean_tot + cluster_mean;
		cluster_var_tot = cluster_var_tot + cluster_var;
	}
	
	fprintf (outstream, "%u\t%.3e\t%.3e\n", cluster_info.size(), cluster_mean_tot, cluster_var_tot );	
};
//*********************************************************************************************************************//
int calc_z_score (IntMatrix* c1, IntMatrix* c2, float &mean, float &std) {
	FlMatrix* m1;
	FlMatrix* m2;
	m1 = new FlMatrix(c1->X(),4,"frequencies");
	int i,j;
	float sum, score;

	FOR(i,c1->X()){
		sum = 0;
		FOR(j,4) {
			m1->s(c1->g(i,j),i,j);
			sum+=c1->g(i,j);
		}
		FOR(j,4) {
			m1->d(sum,i,j);
		}
	}

	m2 = new FlMatrix(c2->X(),4,"frequencies");
	FOR(i,c2->X()){
		sum = 0;
		FOR(j,4) {
			m2->s(c2->g(i,j),i,j);
			sum+=c2->g(i,j);
		}
		FOR(j,4) {
			m2->d(sum,i,j);
		}
        }

	Viterbi* vit = new Viterbi(m1,m2);
	vit->fill(&mean, &std);

	delete m1;
	delete m2;
	delete vit;
	return(0);
};
//*********************************************************************************************************************//
IntMatrix* calc_alignment_score (IntMatrix* c1, IntMatrix* c2, float &score, float &align_cols) {
	FlMatrix* m1;
	FlMatrix* m2;
	m1 = new FlMatrix(c1->X(),4,"frequencies");
	int i,j;
	float sum;
	FOR(i,c1->X()){
		sum = 0;
		FOR(j,4) {
			m1->s(c1->g(i,j),i,j);

			sum+=c1->g(i,j);
		}
		FOR(j,4) {
			m1->d(sum,i,j);
		}
	}

	m2 = new FlMatrix(c2->X(),4,"frequencies");
	FOR(i,c2->X()){
		sum = 0;
		FOR(j,4) {
			m2->s(c2->g(i,j),i,j);
			sum+=c2->g(i,j);
		}
		FOR(j,4) {
			m2->d(sum,i,j);
		}
	}

	Viterbi* vit = new Viterbi(m1,m2);
	IntMatrix* path = new IntMatrix(m1->X()+m2->X(),"path");
	path->initialise(-1);
	vit->align(path, &score);

	int l=1,k,m;
	FOR(i,path->X()){
		if(path->g(i)>=0)
			l++;
	}
	
	FlMatrix* parent = new FlMatrix(l,4,"parent");
	parent->initialise(0);
	
	j = k = l = 1;
	align_cols = 0;
	RFOR(i,path->X()-1){
		if(path->g(i)<0) {
			continue;
		} else if(path->g(i)==0 || path->g(i)==3) {
			FOR(m,4) {
				parent->a(c1->g(l,m),j,m);
				parent->a(c2->g(k,m),j,m);
			}
			j++; l++; k++; 
			align_cols++;
		} else if(path->g(i)==1) {
			FOR(m,4) {
				parent->a(c1->g(l,m),j,m);
			}
			j++; l++;
		} else if(path->g(i)==2) {
			FOR(m,4) {
				parent->a(c2->g(k,m),j,m);
			}
			j++; k++;
		}
	}

	//parent p0 to be stored into the vector
	IntMatrix* p0 = new IntMatrix(parent->X(),4,"counts");
	(*p0).initialise(0);
	for(i=0;i<parent->X();i++) {
		FOR(j,4){
			(*p0).s(int(parent->g(i,j)),i,j);
		}
	}

	if(noise>0) {
		j = k = l = 1;
		string alpha = "ACGT";
		cout<<"Alignment"<<endl;
		RFOR(i,path->X()-1){
			if(path->g(i)<0) {
				continue;
			} else if(path->g(i)==0 || path->g(i)==3) {
				FOR(j,4)
					cout<<c1->g(l,j)<<" ";
				cout<<";";
				FOR(j,4)
					cout<<" "<<c2->g(k,j);
				cout<<endl;
				l++; k++;
			} else if(path->g(i)==1) {
				FOR(j,4)
					cout<<c1->g(l,j)<<" ";
				cout<<"; - - - -"<<endl;
				l++;
			} else if(path->g(i)==2) {
				cout<<"- - - - ;";
				FOR(j,4)
					cout<<" "<<c2->g(k,j);
				cout<<endl;
				k++;
			}
		}	
		cout<<endl<<"Matrix"<<endl;
		for(i=1;i<parent->X();i++) {
			FOR(j,4){
				cout<<parent->g(i,j)<<" ";
			}
			cout<<endl;
		}
	}
	
	delete m1;
	delete m2;
	delete path;
	delete parent;
	delete vit;

	return(p0);
}
//*********************************************************************************************************************//

