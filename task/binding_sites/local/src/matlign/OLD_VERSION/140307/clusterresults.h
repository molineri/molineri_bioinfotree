#ifndef clust_res_h
#define clust_res_h

#include <string>

class ClusterResults {
public:
	void SetElement(std::string, std::string, std::string, std::string, double);
	void GetElement(std::string&, std::string&, std::string&, std::string&, double&);

	ClusterResults();
	~ClusterResults();
private:
	std::string new_element;
	std::string element1;
	std::string element2;
	std::string consensus;
	float element_distance;
};

#endif
