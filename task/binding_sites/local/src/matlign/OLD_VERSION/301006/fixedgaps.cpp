/***************************************************************************
 *   Copyright (C) 2006 by Ari Loytynoja   *
 *   ari@ebi.ac.uk   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <string>
#include <vector>
#include <cmath>
#include "flmatrix.h"
#include "intmatrix.h"
#include "viterbi.h"
#include "filereader.h"
#include "distancematrix.h"
#include "clusterresults.h"
#include "clusterinfo.h"

using namespace std;

float match;
float transi;
float transv;
float gOpen;
float gExt;
int tLength;
int noise;
int norm;
int spacer;
string file;
string out;
bool FIRST;
bool SECOND;
//*********************************************************************************************************************//
IntMatrix* calc_alignment_score(IntMatrix*, IntMatrix*, float&);
int calc_all_scores();
int get_tLength(int, int, int, int);
void print_cluster_variance(vector<ClusterInfo>& cluster_info, FILE*);
void print_cluster_silhouette(vector<float>& silhouette_value, FILE*, unsigned long, float);
//*********************************************************************************************************************//
int main(int argc, char *argv[]) {
	bool e		= 0;
	int r		= 0;
	match 		= 5;		//DEFAULT VALUE
	transi 		= -4;		//DEFAULT VALUE
	transv 		= -4;		//DEFAULT VALUE
	gOpen 		= -10;		//DEFAULT VALUE
	gExt 		= -1;		//DEFAULT VALUE
	tLength 	= 5;		//DEFAULT VALUE
	noise 		= 0;		//DEFAULT VALUE
	out 		= "outfile";	//DEFAULT VALUE
	norm		= 0;		//DEFAULT VALUE (no normalization)
	spacer		= 1;		//DEFAULT VALUE (spacer is enabled)

	for(int i=1;i<argc;i++) {
		string s = string(argv[i]);

		if(s.substr(0,7)=="-input=") {
			file = s.substr(7);
			e = 1;
		} else if(s.substr(0,7)=="-match=") {
			match = atof(s.substr(7).c_str());
		} else if(s.substr(0,8)=="-transi=") {
			transi = atof(s.substr(8).c_str());
		} else if(s.substr(0,8)=="-transv=") {
			transv = atof(s.substr(8).c_str());
		} else if(s.substr(0,7)=="-gopen=") {
			gOpen = atof(s.substr(7).c_str());
		} else if(s.substr(0,6)=="-gext=") {
			gExt = atof(s.substr(6).c_str());
		} else if(s.substr(0,6)=="-term=") {
			tLength = atoi(s.substr(6).c_str());
		} else if(s.substr(0,5)=="-out=") {
			out = s.substr(5);
		} else if(s.substr(0,7)=="-noise=") {
			noise = atoi(s.substr(7).c_str());
		} else if(s.substr(0,6)=="-norm=") {
			norm = atoi(s.substr(6).c_str());
		} else if(s.substr(0,8)=="-spacer=") {
			spacer = atoi(s.substr(8).c_str());
		} else {
			cout<<"unrecognised argument: "<<s<<endl;
		}
    	}

	if (spacer == 0) {
		FIRST = 1; SECOND = 0;  //spacer is disabled
	} else if (spacer == 1) {
		FIRST = 1; SECOND = 1;  //spacer is enabled
	} else if (spacer == 2) {
		FIRST = 0; SECOND = 1;  //spacer is required
	} else {
		FIRST = 1; SECOND = 1;  //spacer is enabled
	}


	if (e == 1) {
		r = calc_all_scores();
	}
}
//*********************************************************************************************************************//
int calc_all_scores () {
	IntMatrix* p0;
	IntMatrix* pT;

	char buffer[10];
	float score(0);
	int min_length(0);
	int min_temp_len(0);
	int parameter_tlength = tLength;
	double pair_score(0);
	unsigned long row_max_dist(0);
	unsigned long col_max_dist(0);
	unsigned long n_mats(0);

	float cluster_min(0), cluster_max(0);
	float cluster_mean(0), cluster_mean_tot(0);
	float cluster_var(0), cluster_var_tot(0);
	float a1(0), b1(0), b1t(0);
	float temp_max(0), temp_min(0), temp_score(0);
	unsigned long cluster_size(0);

	FILE *pswm_stream;
	FILE *mtrx_stream;
	FILE *tree_stream;
	FILE *vara_stream;

	string mtrx_file = out + ".mtrx";
	string tree_file = out + ".tree";
	string pswm_file = out + ".pswm";
	string vara_file = out + ".vara";

	string element1;
	string element2;
	string new_element;
	string consensus;
	
	vector <string> mat_names;
	vector <ClusterResults> cluster_results;
	vector <IntMatrix*> input_mat_vector;
	//vector <IntMatrix*> original_mat_vector;
	vector <ClusterInfo> cluster_info;
	vector <float> silhouette_value;

	FileReader* fr = new FileReader();
	fr->readFile(&file, input_mat_vector, min_length);
	//fr->readFile(&file, original_mat_vector, min_temp_len);
	if(input_mat_vector.size() < 2) { return (0); }
	
	n_mats = input_mat_vector.size();
	DistanceMatrix dist_mat;
	DistanceMatrix silhouette;
	dist_mat.SetSize(n_mats, n_mats);
	silhouette.SetSize(n_mats, n_mats);

	pswm_stream = fopen(pswm_file.c_str(), "w+");
	mtrx_stream = fopen(mtrx_file.c_str(), "w+");
	tree_stream = fopen(tree_file.c_str(), "w+");
	vara_stream = fopen(vara_file.c_str(), "w+");

	if (pswm_stream == NULL) { fprintf (stderr, "ERROR 1: FILE NOT FOUND %s\n", pswm_file.c_str()); exit(0); }
	if (ferror(pswm_stream)) { fprintf (stderr, "ERROR 2: FILE NOT FOUND %s\n", pswm_file.c_str()); exit(0); }
	
	if (mtrx_stream == NULL) { fprintf (stderr, "ERROR 1: FILE NOT FOUND %s\n", mtrx_file.c_str()); exit(0); }
	if (ferror(mtrx_stream)) { fprintf (stderr, "ERROR 2: FILE NOT FOUND %s\n", mtrx_file.c_str()); exit(0); }
	
	if (tree_stream == NULL) { fprintf (stderr, "ERROR 1: FILE NOT FOUND %s\n", tree_file.c_str()); exit(0); }
	if (ferror(tree_stream)) { fprintf (stderr, "ERROR 2: FILE NOT FOUND %s\n", tree_file.c_str()); exit(0); }
        
	if (vara_stream == NULL) { fprintf (stderr, "ERROR 1: FILE NOT FOUND %s\n", vara_file.c_str()); exit(0); }
	if (ferror(vara_stream)) { fprintf (stderr, "ERROR 2: FILE NOT FOUND %s\n", vara_file.c_str()); exit(0); }

	for(int i = 0; i < input_mat_vector.size(); ++i) {
		(*input_mat_vector[i]).get_element_name(element1);
		mat_names.push_back(element1);

		consensus = (*input_mat_vector[i]).get_consensus();
		(*input_mat_vector[i]).print(element1, consensus, pswm_stream);

		for(int j = (i+1); j < input_mat_vector.size(); ++j) {
			tLength = get_tLength(((*input_mat_vector[i]).X()-1), ((*input_mat_vector[j]).X()-1), spacer, parameter_tlength);

			p0 = calc_alignment_score(input_mat_vector[i], input_mat_vector[j], score);
			delete p0;
			
			dist_mat.SetElement(i, j, score);
			dist_mat.SetElement(j, i, score);

			if (i == 0) { temp_max = score; temp_min = score; }
			if (score >= temp_max) { temp_max = score; }
			if (score <= temp_min) { temp_min = score; }
		}
		
		{
			ClusterInfo this_cluster_info;
			this_cluster_info.SetElement(i, -1);
			cluster_info.push_back(this_cluster_info);
		}

		dist_mat.AddElementSize(1);
		silhouette_value.push_back(0);
	}

	//NORMALIZE SILHOUETTE TABLE//
	for(int i = 0; i < input_mat_vector.size(); ++i) {
		for(int j = (i+1); j < input_mat_vector.size(); ++j) {
			temp_score = dist_mat.GetElement(i, j);
			temp_score = (temp_score - temp_min)/(temp_max - temp_min);	
			temp_score = (1 - temp_score);
			silhouette.SetElement(j, i, temp_score);
			silhouette.SetElement(i, j, temp_score);
		}
	}

	dist_mat.PrintMatrix(mtrx_stream);
	//print_cluster_variance(cluster_info, vara_stream);
	print_cluster_silhouette(silhouette_value, vara_stream, input_mat_vector.size(), temp_max);

	for (unsigned int i = 0; i < (n_mats -1); ++i) {
		sprintf(buffer, "%i", (i+1));
		new_element = ">node_nro" + string(buffer);

		pair_score = dist_mat.FindMax(row_max_dist, col_max_dist);

		for(int j = 0; j < input_mat_vector.size(); ++j) {
			if(j != row_max_dist) {
				score = dist_mat.GetElement(row_max_dist, j) + dist_mat.GetElement(col_max_dist, j);
				dist_mat.SetElement(row_max_dist, j, score);
				dist_mat.SetElement(j, row_max_dist, score);
			}
		}
		dist_mat.MergeElementSize(row_max_dist, col_max_dist);
		dist_mat.RemoveElement(col_max_dist);
		dist_mat.PrintMatrix(mtrx_stream);		

		tLength = get_tLength(((*input_mat_vector[row_max_dist]).X()-1), ((*input_mat_vector[col_max_dist]).X()-1), spacer, parameter_tlength);
		p0 = calc_alignment_score(input_mat_vector[row_max_dist], input_mat_vector[col_max_dist], score);
		consensus = (*p0).get_consensus();
		(*p0).print(new_element, consensus, pswm_stream);

		delete input_mat_vector[row_max_dist];
		delete input_mat_vector[col_max_dist];
		input_mat_vector[row_max_dist] = p0;
		input_mat_vector.erase(input_mat_vector.begin() + col_max_dist, input_mat_vector.begin() + col_max_dist + 1);
		
		element1 = mat_names[row_max_dist];
		element2 = mat_names[col_max_dist];
		mat_names.erase(mat_names.begin() + col_max_dist, mat_names.begin() + col_max_dist + 1);
		mat_names[row_max_dist] = new_element;

		{
			ClusterResults this_cluster_result;
			this_cluster_result.SetElement(new_element, element1, element2, consensus, pair_score);
			cluster_results.push_back(this_cluster_result);
		}
		
		for(unsigned int j = 0; j < cluster_info[col_max_dist].GetSize(); ++j ) {
			cluster_info[row_max_dist].SetElement(cluster_info[col_max_dist].GetElement(j));		
		}
		cluster_info.erase(cluster_info.begin() + col_max_dist, cluster_info.begin() + col_max_dist + 1);

		//CALCULATE SILHOUETTE VALUE//
		for(unsigned int j = 0; j < cluster_info.size(); ++j ) {
			for(unsigned int k = 0; k < cluster_info[j].GetSize(); ++k ) {
				a1 = 0;
				for(unsigned int m = 0; m < cluster_info[j].GetSize(); ++m ) {
					if (k != m) {
						a1 = a1 + silhouette.GetElement(cluster_info[j].GetElement(k), cluster_info[j].GetElement(m));
					}
				}
				a1 = (a1 / cluster_info[j].GetSize());

				b1 = 0;
				for(unsigned int m = 0; m < cluster_info.size(); ++m ) {
					if (m != j) {
						b1t = 0;
						for(unsigned int n = 0; n < cluster_info[m].GetSize(); ++n ) {
							b1t = b1t + silhouette.GetElement(cluster_info[j].GetElement(k), cluster_info[m].GetElement(n));
						}
						b1t = (b1t / cluster_info[m].GetSize());
					}
					if (m == 0) { b1 = b1t; }
					if (b1t <=  b1) { b1 = b1t; }	
				}
				if ((a1 == 0) && (b1 == 0)) {
					silhouette_value[(cluster_info[j].GetElement(k))] = 0;
				} else if (cluster_info[j].GetSize() == 1) {
					silhouette_value[(cluster_info[j].GetElement(k))] = 0;
				} else if (a1 >= b1) {
					silhouette_value[(cluster_info[j].GetElement(k))] = ((b1 - a1) / a1);
				} else {
					silhouette_value[(cluster_info[j].GetElement(k))] = ((b1 - a1) / b1);
				}
			}
		}
		print_cluster_silhouette(silhouette_value, vara_stream, input_mat_vector.size(), pair_score);		

		//CALCULATE MIN, MAX, MEAN AND VARIANCE WITHIN THE CLUSTER WHEN COMPARED TO THE CENTER OF CLUSTER (pT)//
		//cluster_info[row_max_dist].ClearElementStat();
		//for(unsigned int j = 0; j < cluster_info[row_max_dist].GetSize(); ++j ) {
		//	pT = calc_alignment_score(p0, original_mat_vector[cluster_info[row_max_dist].GetElement(j)], score);
		//	cluster_info[row_max_dist].SetElementStat(score, score);
		//}
		//print_cluster_variance(cluster_info, vara_stream);
	}

	for(unsigned int i = 0; i < cluster_results.size(); ++i) {
		cluster_results[i].GetElement(new_element, element1, element2, consensus, score);
		fprintf(tree_stream, "%s\t%s\t%s\t%.2f\t%s\n", new_element.c_str(), element1.c_str(), element2.c_str(), 
							       score, consensus.c_str());
	}

	//for(unsigned int i = 0; i < original_mat_vector.size(); ++i) {
	//	delete original_mat_vector[i];
	//}

	dist_mat.FreeMemory();
	silhouette.FreeMemory();

	fclose(tree_stream);
	fclose(mtrx_stream);
	fclose(pswm_stream);
	fclose(vara_stream);
	delete p0;
	//delete pT;
 	delete fr;
	input_mat_vector.clear();
	//original_mat_vector.clear();
	cluster_results.clear();
	cluster_info.clear();
	silhouette_value.clear();
	return(1);
}; 
//*********************************************************************************************************************//
int get_tLength(int length_pat1, int length_pat2, int spacer, int termgap) {
	int length(0);      
	int length_diff(0);

	if (length_pat1 >= length_pat2) { length_diff = length_pat1 - 1; }
	if (length_pat1 <  length_pat2) { length_diff = length_pat2 - 1; }

	if((spacer == 1) && (termgap > length_diff)) {
		length = length_diff;
	} else if ((spacer == 1) && (termgap <= length_diff)) {
		length = termgap;
	} else if (spacer == 0) {
		length = length_diff;
	} else {
		length = 0;
	}

cout << length << "\n";	
	return(length);
};
//*********************************************************************************************************************//
void print_cluster_silhouette(vector<float>& silhouette_value, FILE *outstream, unsigned long cluster_number, float depth) {
	float silhouette_temp(0);
                
	for(unsigned int j = 0; j < silhouette_value.size(); ++j ) {
		silhouette_temp = silhouette_temp + silhouette_value[j];
		//fprintf (outstream, "%.3f ",  silhouette_value[j] );
	}
	//fprintf (outstream, "\n\n");	

	silhouette_temp = silhouette_temp / silhouette_value.size();
	if (cluster_number == 1) { silhouette_temp = 0; }
	fprintf (outstream, "%u\t%.3f\t%.3f\n", cluster_number, silhouette_temp, depth );
};
//*********************************************************************************************************************//
void print_cluster_variance(vector<ClusterInfo>& cluster_info, FILE *outstream) {
	float cluster_min(0), cluster_max(0), cluster_mean(0), cluster_var(0);
	float cluster_mean_tot(0), cluster_var_tot(0);
	unsigned long cluster_size(0);

	for(unsigned int i = 0; i < cluster_info.size(); ++i) {
		cluster_info[i].CalcElementStat(cluster_min, cluster_max, cluster_mean, cluster_var, cluster_size);
		//cluster_info[i].PrintElement(outstream, 1);
		cluster_mean_tot = cluster_mean_tot + cluster_mean;
		cluster_var_tot = cluster_var_tot + cluster_var;
	}
	
	fprintf (outstream, "%u\t%.3e\t%.3e\n", cluster_info.size(), cluster_mean_tot, cluster_var_tot );	
};
//*********************************************************************************************************************//
IntMatrix* calc_alignment_score (IntMatrix* c1, IntMatrix* c2, float &score) {
	FlMatrix* m1;
	FlMatrix* m2;
	m1 = new FlMatrix(c1->X(),4,"frequencies");
	int i,j;
	float sum;
	FOR(i,c1->X()){
		sum = 0;
		FOR(j,4) {
			m1->s(c1->g(i,j),i,j);
			sum+=c1->g(i,j);
		}
		FOR(j,4) {
			m1->d(sum,i,j);
		}
	}

	m2 = new FlMatrix(c2->X(),4,"frequencies");
	FOR(i,c2->X()){
		sum = 0;
		FOR(j,4) {
			m2->s(c2->g(i,j),i,j);
			sum+=c2->g(i,j);
		}
		FOR(j,4) {
			m2->d(sum,i,j);
		}
	}

	Viterbi* vit = new Viterbi(m1,m2);
	IntMatrix* path = new IntMatrix(m1->X()+m2->X(),"path");
	path->initialise(-1);
	vit->align(path, &score, norm);
	
	int l=1,k,m;
	FOR(i,path->X()){
		if(path->g(i)>=0)
			l++;
	}
	
	FlMatrix* parent = new FlMatrix(l,4,"parent");
	parent->initialise(0);
	
	j = l = k = 1;
	RFOR(i,path->X()-1){
		if(path->g(i)<0) {
			continue;
		} else if(path->g(i)==0 || path->g(i)==3) {
			FOR(m,4) {
				parent->a(c1->g(l,m),j,m);
				parent->a(c2->g(k,m),j,m);
			}
			j++; l++; k++;
		} else if(path->g(i)==1) {
			FOR(m,4) {
				parent->a(c1->g(l,m),j,m);
			}
			j++; l++;
		} else if(path->g(i)==2) {
			FOR(m,4) {
				parent->a(c2->g(k,m),j,m);
			}
			j++; k++;
		}
	}

	//parent p0 to be stored into the vector
	IntMatrix* p0 = new IntMatrix(parent->X(),4,"counts");
	(*p0).initialise(0);
	for(i=0;i<parent->X();i++) {
		FOR(j,4){
			(*p0).s(int(parent->g(i,j)),i,j);
		}
	}

	if(noise>0) {
		j = l = k = 1;
		string alpha = "ACGT";
		cout<<"Alignment"<<endl;
		RFOR(i,path->X()-1){
			if(path->g(i)<0) {
				continue;
			} else if(path->g(i)==0 || path->g(i)==3) {
				FOR(j,4)
					cout<<c1->g(l,j)<<" ";
				cout<<";";
				FOR(j,4)
					cout<<" "<<c2->g(k,j);
				cout<<endl;
				l++; k++;
			} else if(path->g(i)==1) {
				FOR(j,4)
					cout<<c1->g(l,j)<<" ";
				cout<<"; - - - -"<<endl;
				l++;
			} else if(path->g(i)==2) {
				cout<<"- - - - ;";
				FOR(j,4)
					cout<<" "<<c2->g(k,j);
				cout<<endl;
				k++;
			}
		}	
		cout<<endl<<"Matrix"<<endl;
		for(i=1;i<parent->X();i++) {
			FOR(j,4){
				cout<<parent->g(i,j)<<" ";
			}
			cout<<endl;
		}
	}
	
	delete m1;
	delete m2;
	delete path;
	delete parent;
	delete vit;

	return(p0);
}
//*********************************************************************************************************************//

