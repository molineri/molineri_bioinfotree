use strict;
use warnings;
###################################################################################################
#Call all the methods that are neede to generate the tree and matrix pictures
sub PictureCaller {
        my($root, $exp_ptr, $outputfile, $lowery, $silhouette_ptr, $silhouette_depth)		= @_;

	open (TREE, ">$outputfile") or die "output file $outputfile was not found\n";;
        &PrintExpressionTreeFile($root, $lowery, $exp_ptr, 0, $silhouette_ptr, $silhouette_depth);
	close TREE;
};
###################################################################################################
#Writes coordinates for Gene expression tree
sub PrintExpressionTreeFile {
	my($oldnode, $ypos, $exp_ptr, $xposition, $silhouette_ptr, $silhouette_depth) = @_;
	my($xpos1) 										= 0;
	my($xpos2) 										= 0;
	my($oldy) 										= 0;
	my($geneid)										= "";

	if(exists(${$exp_ptr}{$oldnode}{'yposition'})) { $oldy = ${$exp_ptr}{$oldnode}{'yposition'}; }

	if(exists(${$exp_ptr}{$oldnode}{'l_child'})) { 
		my($l_child) = ${$exp_ptr}{$oldnode}{'l_child'};
		($xpos1) = &PrintExpressionTreeFile($l_child, $oldy, $exp_ptr, ($xposition + 1), $silhouette_ptr, $silhouette_depth); 
	} if(exists(${$exp_ptr}{$oldnode}{'r_child'})) { 
		my($r_child) = ${$exp_ptr}{$oldnode}{'r_child'};
		($xpos2) = &PrintExpressionTreeFile($r_child, $oldy, $exp_ptr, ($xposition + 1), $silhouette_ptr, $silhouette_depth); 
	}

	#For labels and for special dots use xposition and oldy
	if(exists(${$exp_ptr}{$oldnode}{'id'})) { $geneid = ${$exp_ptr}{$oldnode}{'id'}; }
	if(exists(${$exp_ptr}{$oldnode}{'xposition'})) { $xposition = ${$exp_ptr}{$oldnode}{'xposition'}; }
	print TREE "#$oldnode\t$geneid\n";

	if (${$exp_ptr}{$oldnode}{'is_gene'} == 1) {
		my($gtr_id) = $oldnode;
		$gtr_id =~ s/\D|\s//gi;
	} else {
		print TREE "$xpos1\t$oldy\n";
		print TREE "$xpos2\t$oldy\n\n";
		$geneid = "";
	}

	if(($ypos <= $silhouette_depth) && ($silhouette_depth < $oldy)) {
		${$silhouette_ptr}{$oldnode} = 1;
	}

	print TREE "$xposition\t$oldy\n";
	print TREE "$xposition\t$ypos\n\n";
	return($xposition);
};
###################################################################################################
1;
