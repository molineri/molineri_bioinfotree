use strict;
use warnings;

###################################################################################################
#Check file existing
sub FindMinAndMaxDepth {
	my($input_file)										= @_;
	my(@data)										= ();
	my(@columns)										= ();
	my($line)										= "";
	my($max_value)										= 0;
	my($min_value)										= 0;
	my($is_first)										= 1;
	
	open(TREE, $input_file);
	@data = <TREE>;
	close TREE;

	while(@data > 0) {
                $line = shift(@data);
		$line =~ s/^\s+|\s+$//g;
		if(length($line) > 0) {
			@columns = split(/\s/, $line);
			if(@columns >= 3) {
				if($is_first == 1) { 
					$max_value = $columns[3]; 
					$min_value = $columns[3];
					$is_first = 0;
				}
				if($columns[3] >= $max_value) { $max_value = $columns[3]; }
				if($columns[3] <= $min_value) { $min_value = $columns[3]; }
			}
		}
	}
	return($min_value, $max_value);
};
###################################################################################################
#Check file existing
sub FindMaxValue {
	my($input_file)										= @_;
	my(@data)										= ();
	my(@columns)										= ();
	my($line)										= "";
	my($max_value)										= 0;
	my($return_value_clust)									= 0;
	my($return_value_depth)									= 0;
	my($is_first)										= 1;
       
	open(DATA, $input_file);
	@data = <DATA>;
	close DATA;

	while(@data > 0) {
		$line = shift(@data);
		$line =~ s/^\s+|\s+$//g;
		if(length($line) > 0) {
			@columns = split(/\s/, $line);
			if(@columns >= 1) {
				if($is_first == 1) {
					$max_value = $columns[1];
					$is_first = 0;
				}
				if($columns[1] >= $max_value) { 
					$max_value 		= $columns[1]; 
					$return_value_clust 	= $columns[0];
					$return_value_depth	= $columns[2];
				}
			}
		}
	}
	return($return_value_clust, $return_value_depth);
};
###################################################################################################
1

