#ifndef dist_mat_h
#define dist_mat_h

class DistanceMatrix {
public:
	float ** distance_matrix;	

	void FreeMemory();
	void SetSize( unsigned long, unsigned long );
	void PrintMatrix( FILE* );
	float FindMax( unsigned long&, unsigned long& );
	void RemoveElement( unsigned long );

	float SetElement( unsigned long, unsigned long, float);
	float GetElement( unsigned long, unsigned long);

	DistanceMatrix();
	~DistanceMatrix();
private:
	bool has_data;
	unsigned long row;
	unsigned long col;

};

#endif
