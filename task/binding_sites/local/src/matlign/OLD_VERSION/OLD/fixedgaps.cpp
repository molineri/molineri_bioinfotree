/***************************************************************************
 *   Copyright (C) 2006 by Ari Loytynoja   *
 *   ari@ebi.ac.uk   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <vector>
#include "flmatrix.h"
#include "intmatrix.h"
#include "viterbi.h"
#include "filereader.h"
#include "distancematrix.h"
#include "clusterresults.h"

using namespace std;

float match;
float transi;
float transv;
float gOpen;
float gExt;
int tLength;
int noise;
int norm;
string file;
string out;
//*********************************************************************************************************************//
IntMatrix* calc_alignment_score(IntMatrix*, IntMatrix*, float&);
int calc_all_scores();
//*********************************************************************************************************************//
int main(int argc, char *argv[]) {
	bool e		= 0;
	int r		= 0;
	match 		= 5;		//DEFAULT VALUE
	transi 		= -4;		//DEFAULT VALUE
	transv 		= -4;		//DEFAULT VALUE
	gOpen 		= -10;		//DEFAULT VALUE
	gExt 		= -1;		//DEFAULT VALUE
	tLength 	= 5;		//DEFAULT VALUE
	noise 		= 0;		//DEFAULT VALUE
	out 		= "outfile";	//DEFAULT VALUE
	norm		= 0;		//DEFALUT VALUE (no normalization)

	for(int i=1;i<argc;i++) {
		string s = string(argv[i]);

		if(s.substr(0,7)=="-input=") {
			file = s.substr(7);
			e = 1;
		} else if(s.substr(0,7)=="-match=") {
			match = atof(s.substr(7).c_str());
		} else if(s.substr(0,8)=="-transi=") {
			transi = atof(s.substr(8).c_str());
		} else if(s.substr(0,8)=="-transv=") {
			transv = atof(s.substr(8).c_str());
		} else if(s.substr(0,7)=="-gopen=") {
			gOpen = atof(s.substr(7).c_str());
		} else if(s.substr(0,6)=="-gext=") {
			gExt = atof(s.substr(6).c_str());
		} else if(s.substr(0,6)=="-term=") {
			tLength = atoi(s.substr(6).c_str());
		} else if(s.substr(0,5)=="-out=") {
			out = s.substr(5);
		} else if(s.substr(0,7)=="-noise=") {
			noise = atoi(s.substr(7).c_str());
		} else if(s.substr(0,6)=="-norm=") {
			norm = atoi(s.substr(6).c_str());
		} else {
			cout<<"unrecognised argument: "<<s<<endl;
		}
    	}
	if (e == 1) {
		r = calc_all_scores();
	}
}
//*********************************************************************************************************************//
int calc_all_scores () {
	IntMatrix* p0;
	IntMatrix* p1;

	char buffer[10];
	float score(0);
	int min_length(0);
	double combine_pair(0);
	unsigned long row_max_dist(0);
	unsigned long col_max_dist(0);
	unsigned long n_mats(0);

	FILE *pswm_stream;
	FILE *mtrx_stream;
	FILE *tree_stream;

	string mtrx_file = out + ".mtrx";
	string tree_file = out + ".tree";
	string pswm_file = out + ".pswm";
	string element1;
	string element2;
	string new_element;
	string consensus;
	
	vector <string> mat_names;
	vector <ClusterResults> combination_order;
	vector <IntMatrix*> input_mat_vector;

	FileReader* fr = new FileReader();
	fr->readFile(&file, input_mat_vector, min_length);
	if(tLength > min_length) { tLength = min_length; }	
	
	n_mats = input_mat_vector.size();
	DistanceMatrix dist_mat;
	dist_mat.SetSize(n_mats, n_mats);

	//cout<<"Analyzing "<<input_mat_vector.size()<<" matrices/patterns\n";
	if(input_mat_vector.size() < 2) { return (0); }

	pswm_stream = fopen(pswm_file.c_str(), "w+");
	mtrx_stream = fopen(mtrx_file.c_str(), "w+");
	tree_stream = fopen(tree_file.c_str(), "w+");

	if (pswm_stream == NULL) { fprintf (stderr, "ERROR 1: FILE NOT FOUND %s\n", pswm_file.c_str()); exit(0); }
	if (ferror(pswm_stream)) { fprintf (stderr, "ERROR 2: FILE NOT FOUND %s\n", pswm_file.c_str()); exit(0); }
	if (mtrx_stream == NULL) { fprintf (stderr, "ERROR 1: FILE NOT FOUND %s\n", mtrx_file.c_str()); exit(0); }
	if (ferror(mtrx_stream)) { fprintf (stderr, "ERROR 2: FILE NOT FOUND %s\n", mtrx_file.c_str()); exit(0); }
	if (tree_stream == NULL) { fprintf (stderr, "ERROR 1: FILE NOT FOUND %s\n", tree_file.c_str()); exit(0); }
	if (ferror(tree_stream)) { fprintf (stderr, "ERROR 2: FILE NOT FOUND %s\n", tree_file.c_str()); exit(0); }

	for(int i = 0; i < input_mat_vector.size(); ++i) {
		(*input_mat_vector[i]).get_element_name(element1);

		//sprintf(buffer, "%i", (i+1));
		//element1 = ">matrix" + string(buffer);
		mat_names.push_back(element1);

		consensus = (*input_mat_vector[i]).get_consensus();
		(*input_mat_vector[i]).print(element1, consensus, pswm_stream);

		for(int j = (i+1); j < input_mat_vector.size(); ++j) {
			p0 = calc_alignment_score(input_mat_vector[i], input_mat_vector[j], score);
			delete p0;

			dist_mat.SetElement(i, j, score);
			dist_mat.SetElement(j, i, score);
		}
	}
	
	dist_mat.PrintMatrix(mtrx_stream);
	
	for (unsigned int i = 0; i < (n_mats -1); ++i) {
		combine_pair = dist_mat.FindMax(row_max_dist, col_max_dist);
		dist_mat.RemoveElement(col_max_dist);

		p0 = calc_alignment_score(input_mat_vector[row_max_dist], input_mat_vector[col_max_dist], score);
		delete input_mat_vector[row_max_dist];
		delete input_mat_vector[col_max_dist];
		input_mat_vector[row_max_dist] = p0;
		input_mat_vector.erase(input_mat_vector.begin() + col_max_dist, input_mat_vector.begin() + col_max_dist + 1);
		
		element1 = mat_names[row_max_dist];
		element2 = mat_names[col_max_dist];
		sprintf(buffer, "%i", (i+1));
		new_element = ">node_nro" + string(buffer);
		mat_names.erase(mat_names.begin() + col_max_dist, mat_names.begin() + col_max_dist + 1);
		mat_names[row_max_dist] = new_element;
		consensus = (*p0).get_consensus();
		(*p0).print(new_element, consensus, pswm_stream);

		{
			ClusterResults this_result;
			this_result.SetElement(new_element, element1, element2, consensus, score);
			combination_order.push_back(this_result);
		}

		for(int j = 0; j < input_mat_vector.size(); ++j) {
			if(j != row_max_dist) {
				p0 = calc_alignment_score(input_mat_vector[row_max_dist], input_mat_vector[j], score);
				dist_mat.SetElement(row_max_dist, j, score);
				dist_mat.SetElement(j, row_max_dist, score);
				delete p0;
			}
		}

		dist_mat.PrintMatrix(mtrx_stream);
	}
	
	for(unsigned int i = 0; i < combination_order.size(); ++i) {
		combination_order[i].GetElement(new_element, element1, element2, consensus, score);
		fprintf(tree_stream, "%s\t%s\t%s\t%.2f\t%s\n", new_element.c_str(), element1.c_str(), element2.c_str(), 
							       score, consensus.c_str());
	}

	dist_mat.FreeMemory();

	fclose(tree_stream);
	fclose(mtrx_stream);
	fclose(pswm_stream);
	delete p0;
 	delete fr;
	input_mat_vector.clear();
	return(1);
};
//*********************************************************************************************************************//
IntMatrix* calc_alignment_score (IntMatrix* c1, IntMatrix* c2, float &score) {
	FlMatrix* m1;
	FlMatrix* m2;

	m1 = new FlMatrix(c1->X(),4,"frequencies");
	int i,j;
	float sum;
	FOR(i,c1->X()){
		sum = 0;
		FOR(j,4) {
			m1->s(c1->g(i,j),i,j);
			sum+=c1->g(i,j);
		}
		FOR(j,4) {
			m1->d(sum,i,j);
		}
	}

	m2 = new FlMatrix(c2->X(),4,"frequencies");
	FOR(i,c2->X()){
		sum = 0;
		FOR(j,4) {
			m2->s(c2->g(i,j),i,j);
			sum+=c2->g(i,j);
		}
		FOR(j,4) {
			m2->d(sum,i,j);
		}
	}

	Viterbi* vit = new Viterbi(m1,m2);
	IntMatrix* path = new IntMatrix(m1->X()+m2->X(),"path");
	path->initialise(-1);
	vit->align(path, &score, norm);
	
	int l=1,k,m;
	FOR(i,path->X()){
		if(path->g(i)>=0)
			l++;
	}
	
	FlMatrix* parent = new FlMatrix(l,4,"parent");
	parent->initialise(0);
	
	j = l = k = 1;
	RFOR(i,path->X()-1){
		if(path->g(i)<0) {
			continue;
		} else if(path->g(i)==0 || path->g(i)==3) {
			FOR(m,4) {
				parent->a(c1->g(l,m),j,m);
				parent->a(c2->g(k,m),j,m);
			}
			j++; l++; k++;
		} else if(path->g(i)==1) {
			FOR(m,4) {
				parent->a(c1->g(l,m),j,m);
			}
			j++; l++;
		} else if(path->g(i)==2) {
			FOR(m,4) {
				parent->a(c2->g(k,m),j,m);
			}
			j++; k++;
		}
	}

	//parent p0 to be stored into the vector
	IntMatrix* p0 = new IntMatrix(parent->X(),4,"counts");
	(*p0).initialise(0);
	for(i=0;i<parent->X();i++) {
		FOR(j,4){
			(*p0).s(int(parent->g(i,j)),i,j);
		}
	}

	if(noise>0) {
		j = l = k = 1;
		string alpha = "ACGT";
		cout<<"Alignment"<<endl;
		RFOR(i,path->X()-1){
			if(path->g(i)<0) {
				continue;
			} else if(path->g(i)==0 || path->g(i)==3) {
				FOR(j,4)
					cout<<c1->g(l,j)<<" ";
				cout<<";";
				FOR(j,4)
					cout<<" "<<c2->g(k,j);
				cout<<endl;
				l++; k++;
			} else if(path->g(i)==1) {
				FOR(j,4)
					cout<<c1->g(l,j)<<" ";
				cout<<"; - - - -"<<endl;
				l++;
			} else if(path->g(i)==2) {
				cout<<"- - - - ;";
				FOR(j,4)
					cout<<" "<<c2->g(k,j);
				cout<<endl;
				k++;
			}
		}	
		cout<<endl<<"Matrix"<<endl;
		for(i=1;i<parent->X();i++) {
			FOR(j,4){
				cout<<parent->g(i,j)<<" ";
			}
			cout<<endl;
		}
	}
	
	delete m1;
	delete m2;
	delete path;
	delete parent;
	delete vit;

	return(p0);
}
//*********************************************************************************************************************//

