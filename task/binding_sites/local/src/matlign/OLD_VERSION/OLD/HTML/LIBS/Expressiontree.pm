use strict;
use warnings;

###################################################################################################
#Finds the root of exp_tree and returns it
sub FindRoot {
        my($exp_ptr)                                            		= @_;
        my($root)                                               		= "";
	my($root_ypos)								= 0;

        for my $node(keys(%{$exp_ptr})) {
                if (${$exp_ptr}{$node}{'mother'} eq 'root') { $root = $node; }
        }

	if(exists(${$exp_ptr}{$root}{'yposition'})) { $root_ypos = ${$exp_ptr}{$root}{'yposition'};  }
        return($root, $root_ypos);
};
###################################################################################################
#Gets the node information (gene name and number, and creates background values) 
sub GetTreeInformation {
	my($oldnode, $level, $min_val, $max_val, $xpos, $exp_ptr) 		= @_;
	my($total) 								= 0;		#Total number of genes
	my($l_name)								= "";		#Name of the left child
	my($r_name)								= "";		#Name of the right child
	my($ypos)								= 0;
	my($xposition)								= 0;
	my($xpos1)								= 0;
	my($xpos2)								= 0;
	my($gene_count)								= 0;

	++$level;

	if (exists(${$exp_ptr}{$oldnode}{'l_child'})) {
		$l_name = ${$exp_ptr}{$oldnode}{'l_child'}; 
		($total, $xpos, $xpos1) = &GetTreeInformation($l_name, $level, $min_val, $max_val, $xpos, $exp_ptr); 
	} if (exists(${$exp_ptr}{$oldnode}{'r_child'})) {
		$r_name = ${$exp_ptr}{$oldnode}{'r_child'}; 
		($total, $xpos, $xpos2) = &GetTreeInformation($r_name, $level, $min_val, $max_val, $xpos, $exp_ptr); 
	}
	
	if(${$exp_ptr}{$oldnode}{'is_gene'} == 1) {
		$xpos 					= $xpos + 1;
		$xposition 				= $xpos;
		${$exp_ptr}{$oldnode}{'xposition'} 	= ($xposition);	
	} else {
		$xposition 				= ($xpos1 + $xpos2)/2;
		$gene_count 				= ${$exp_ptr}{$r_name}{'genes'} + ${$exp_ptr}{$l_name}{'genes'};
		${$exp_ptr}{$oldnode}{'xposition'} 	= $xposition;
		${$exp_ptr}{$oldnode}{'genes'} 		= $gene_count;
	}

	${$exp_ptr}{$oldnode}{'level'} 			= $level;
	return($gene_count, $xpos, $xposition);
};
###################################################################################################
1;
