use strict;
use warnings;

###################################################################################################
#Common commands to set up gnuplot view
sub Gnuplotcommon {
	my($width, $height, $output)						= @_;
	print TEMP "set terminal png small size $width,$height\n";

        print TEMP "set output '$output'\n";
        print TEMP "set pointsize 1.25\n";

	print TEMP "set key off\n";
	print TEMP "unset colorbox\n";
	print TEMP "unset border\n";
	print TEMP "unset surface\n";
	print TEMP "unset xtics\n";
};
###################################################################################################
#Plot tree
sub Plotexpressiontree {
	my($total, $file, $yupper, $ylower)					= @_;

	#print TEMP "set xrange[0:$total]\n";
	#print TEMP "set yrange[$yupper:0]\n";
	print TEMP "set yrange[$yupper:$ylower]\n";	
	print TEMP "set tmargin 2\n";
	print TEMP "set bmargin 2\n";
        #print TEMP "set lmargin 8\n";
        #print TEMP "set rmargin 8\n";
	print TEMP "plot '$file' w lines -1\n";
};
###################################################################################################
#Call GnuPlot
sub Drawselectednode {
	my($input_tree, $input_temp, $output, $yupper, $ylower, $gene_ptr, $total) 	= @_;
	my($height)								= 500;
	my($width)								= 1100;
	my($execute_gp)								= 0;
	#$yupper 								= $yupper * 1.01;

	open(TEMP, ">$input_temp") or die "output file $input_temp was not found\n";

	&Gnuplotcommon($width, $height, $output);
	&Writetemptreesymbols($gene_ptr, ($yupper*0.05));
	&Plotexpressiontree(($total-1), $input_tree, $yupper, $ylower);

	close TEMP;

        $execute_gp = system("gnuplot $input_temp");
	#$execute_gp = system("/afs/bi/bioinfo/group/bin/gnuplot $input_temp");
	if($execute_gp != 0) { die "Problems performing Gnuplot\n"; }
};
###################################################################################################
sub Writetemptreesymbols {
	my($gene_ptr, $label_shift)						= @_; 
	my($is_gene)								= 0;
	my($xcoord)								= 0;
	my($ycoord)								= 0;
	my($coords)								= "";
	my($gene_name)								= "";
	my($even_counter)							= 0;

	for my $nodeid (keys( %{$gene_ptr} ))  {
		if (exists(${$gene_ptr}{$nodeid}{'is_gene'}))	{ $is_gene = ${$gene_ptr}{$nodeid}{'is_gene'}; }
		if (exists(${$gene_ptr}{$nodeid}{'xposition'}))	{ $xcoord  = ${$gene_ptr}{$nodeid}{'xposition'};  }
		if (exists(${$gene_ptr}{$nodeid}{'yposition'}))	{ $ycoord  = ${$gene_ptr}{$nodeid}{'yposition'};  }
		if (($is_gene == 1) && (($xcoord % 2) == 1)  ) { $ycoord = $ycoord - $label_shift; }
		#$ycoord = $ycoord - 0.015;
		$ycoord = $ycoord - 0.011;
		$coords = $xcoord.", ".$ycoord;

		$gene_name = ${$gene_ptr}{$nodeid}{'id'};
		if(length($gene_name) > 20) { $gene_name =~ s/\(.*\)//; }

		#$gene_name .= "(".${$gene_ptr}{$nodeid}{'yposition'}.")";
		print TEMP "set label '$gene_name' at $coords center front textcolor lt 1\n";
	}
};
###################################################################################################
1;
