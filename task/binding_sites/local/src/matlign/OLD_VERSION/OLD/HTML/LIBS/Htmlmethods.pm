use strict;
use warnings;

###################################################################################################
#Interface
sub Printhtmloutput {
	my($input_prefix_pats, $input_prefix_tree)						= @_;
	my(%score_hash)										= ();
	my(@score_data)										= ();
	my(@temp_data)										= ();
	my(@row)										= ();
	my($line)										= "";	
	my($col_width)										= 0;
	my($col_count)										= 0;
	my($con_pattern)									= "";
	my($temp_line)										= "";
	my($score_text)										= "";
	my($score_text_node1)									= "";
	my($score_text_node2)									= "";
	my($score_value)									= "";
	my($button) = "<INPUT TYPE=\"button\" name=\"b";

	open(SCORE, "$input_prefix_tree")  or die "output file html $input_prefix_tree was not found";
	@score_data = <SCORE>;
	close SCORE;
	
	open(DATA, "$input_prefix_pats") or die "output file html $input_prefix_pats was not found";	
	@temp_data = <DATA>;
	close DATA;

	while (@score_data > 0) {
		$line = shift(@score_data);
		$line =~ s/^\s+|\s+$//;
		if(length($line) > 0) {
			#$line =~ s/\>//g;
			@row = split(/\s/, $line);
			$score_text_node1 = "";
			$score_text_node2 = "";
			$score_hash{$row[0]}{'text'}	= "(score $row[3]) (combined patterns: $row[1]";
			$score_hash{$row[0]}{'text'}   .= "$score_text_node1 and $row[2]".$score_text_node2.")";
			$score_hash{$row[0]}{'child_r'} = $row[1];
			$score_hash{$row[0]}{'child_l'} = $row[2];
			$score_hash{$row[0]}{'score'} 	= $row[3];
		}
	}
	
	while (@temp_data > 0) {
		$line = shift(@temp_data);
		$line =~ s/^\s+|\s+$//;
		
		if(($line =~ m/^>/) && (!($line =~ m/node/)) &&  (!($line =~ m/consensus/))) {
			while (length($line) > 0) {				#unclusterd pattterns
				$line = shift(@temp_data);
				$line =~ s/^\s+|\s+$//;
			}
		} elsif ((length($line) > 0) && ($line =~ m/^>/) && (($line =~ m/node/i))) {	
			#$line =~ s/\>//g;
			if (exists($score_hash{$line}{'text'})) {
				$score_value = $score_hash{$line}{'score'}	
			} else {
				$score_value = 1.00;
			}

			if ($score_value > 0) {
				$temp_line = $temp_data[1];
				$temp_line =~ s/^\s+|\s+$//;

				if (exists($score_hash{$line}{'text'})) {
					$score_text = $score_hash{$line}{'text'}; 
				} else {
					$score_text = "(no score)";
				}
				
				print HTML "".&tablestart(0, 1, 100, 4)."\n";
				print HTML "".$line." ".$score_text."</td></tr>\n";
				
				print HTML "<tr bgcolor ='#B0C4DE'>\n".&td(25, 1)."";
				print HTML "<font color='#000000'>A</font></td>\n";
				print HTML "".&td(25, 1)."<font color='#000000'>C</font></td>\n";
				print HTML "".&td(25, 1)."<font color='#000000'>G</font></td>\n";
				print HTML "".&td(25, 1)."<font color='#000000'>T</font></td>\n";
				print HTML "</tr>\n";				
			} else {
				if (exists($score_hash{$line}{'text'})) {
					$score_text = $score_hash{$line}{'text'};
				} else {
					$score_text = "(no score)";
				}

				print HTML "".&tablestart(0, 1, 100, 4)."\n";
				print HTML "".$line." ".$score_text."</td></tr>\n";
				print HTML "<tr bgcolor ='#B0C4DE'>\n<td colspan ='4' align='center'>\n";
				print HTML "<font color='#000000'>Matrix was not drawn (score = 0.0). To view the ";
				print HTML "matrix see results files below</font></td></tr><br>\n";
				print HTML "".&tableend(0, 1, 100, 4)."\n";
				
				while (length($line) > 0) {
					$line = shift(@temp_data);
					$line =~ s/^\s+|\s+$//;
				}
			}
		}  elsif ((length($line) > 0) && ($line =~ m/consensus/i)) {
                        $line =~ s/^#//;
			                        
			$con_pattern = $line;
			$con_pattern =~ s/consensus://i;
			$con_pattern =~ s/\s//g;
			print HTML "<tr bgcolor='#B0C4DE'>\n";
			print HTML "".&td(100, 4)."<font color='#000000'>\n";
			print HTML "".$line."</font></td></tr>\n";

			print HTML "<tr bgcolor='708090'>\n";
			print HTML "".&td(100, 4)."";
			print HTML "".$button."a\" value=\"POBO\" ";
			print HTML "onClick=\"call_tools('$con_pattern', 'pobo/pobo')\">";
			print HTML "".$button."b\" value=\"POCO2nd\" ";
			print HTML "onClick=\"call_tools('$con_pattern', 'poco/poco2nd')\">";
			print HTML "".$button."c\" value=\"Visualize\" ";
			print HTML "onClick=\"call_tools('$con_pattern', 'visualize/visualize')\">";
			print HTML "".$button."d\" value=\"Evolution\" ";
			print HTML "onClick=\"call_tools('$con_pattern', 'evolution/evolution')\">";
			print HTML "".$button."e\" value=\"Screener\" ";
			print HTML "onClick=\"call_tools('$con_pattern', 'screener/screener')\">";

			print HTML "".&tableend(0, 1, 100, 4)."\n";
		} elsif (length($line) > 0) {
			@row = split(/\s/, $line);

			print HTML "<tr bgcolor ='#B0C4DE'>\n";
			for (my($j) = 0; $j < @row; ++$j) {
				print HTML "".&td(25, 1)."<font color='#000000'>".$row[$j]."</font></td>\n";
			}
			print HTML "</tr>\n";
		} elsif (length($line) == 0) {
			print HTML "<br>\n";
		}
	}

	print HTML "".&printfooters()."\n";
};
###################################################################################################
#Print start rows
sub printheaders {
	my($picture_file, $sequences1, $sequences2, $input_number, $sequence_length, $pat_overlap,
	   $bootstrap_sequence, $bootstrap_cluster, $background_organism)                                  = @_;

	my($line)                                                                                       = "";
	$line .= "<dtml-unless picture>";
	$line .= "<dtml-call \"REQUEST.set('picture', '$picture_file')\"></dtml-unless>\n";
	$line .= "<dtml-unless input_number>";
	$line .= "<dtml-call \"REQUEST.set('input_number', '$input_number')\"></dtml-unless>\n";
	$line .= "<dtml-unless sequence_length>";
	$line .= "<dtml-call \"REQUEST.set('sequence_length', '$sequence_length')\"></dtml-unless>\n";
	$line .= "<dtml-unless pat_overlap>";
	$line .= "<dtml-call \"REQUEST.set('pat_overlap','$pat_overlap')\"></dtml-unless>\n";
	$line .= "<dtml-unless bootstrap_sequence>";
	$line .= "<dtml-call \"REQUEST.set('bootstrap_sequence', '$bootstrap_sequence')\"></dtml-unless>\n";
	$line .= "<dtml-unless bootstrap_cluster>";
	$line .= "<dtml-call \"REQUEST.set('bootstrap_cluster', '$bootstrap_cluster')\"></dtml-unless>\n";
	$line .= "<dtml-unless background_organism>";
	$line .= "<dtml-call \"REQUEST.set('background_organism','$background_organism')\"></dtml-unless>\n";
	$line .= "<dtml-unless search_pattern>";
	$line .= "<dtml-call \"REQUEST.set('search_pattern','')\"></dtml-unless>\n";
	
	$line .= "<dtml-var matlign_header>\n";
	$line .= "<form name=\"cluster_hidden\" method=\"post\" enctype=\"multipart/form-data\">\n";
	$line .= "<dtml-var matlign_script>\n";
	$line .= "<Input type=\"hidden\" name=\"sequence_set1\" size=\"0\" value=\"$sequences1\">\n";
	$line .= "<Input type=\"hidden\" name=\"sequence_set2\" size=\"0\" value=\"$sequences2\">\n";
	$line .= "</form>\n";
	$line .= "<dtml-var matlign_picture>\n";
	return($line);
};
###################################################################################################
#Print end rows
sub printfooters {
	return("<dtml-var matlign_result_files>\n<dtml-var matlign_footer>");
};
###################################################################################################
#Print table row start
sub tablestart {
	my($type_switch, $bold, $width, $colspan)						= @_;
	my($line)										= "";

	$line = "<table border='1' cellpadding='4' cellspacing='0' bordercolor = '#00008B' align='center' width='95%'>"; 

	if($type_switch == 0) {
		$line  .= "<tr bgcolor='#B0C4DE'>";
		$line .= "".&td($width, $colspan)."";
		$line .= "<font color='#000000'>";
	} elsif ($type_switch == 1) {
		$line  .= "<tr bgcolor='#B0C4DE'>";
		$line .= "".&td($width, $colspan)."";
		$line .= "<font color='#000000'>";
	}
	if ($bold == 1) {
		$line .= "<b>";
	}
	return($line);
};
###################################################################################################
#Print table row start
sub tableend {
	my($type_switch, $bold, $width, $colspan)						= @_;
	my($line)										= "";

	if ($bold == 1) {
		$line  = "</b></font></td></tr>"; 
	} else {
		$line  = "</font></td></tr>";
	}
	$line .= "</table>\n";
	return($line);
};
###################################################################################################
#Print table td tag
sub td {
	my($width, $colspan)									= @_;
	return("<td align='center' width='$width%' colspan='$colspan'>");
};
###################################################################################################
1;
