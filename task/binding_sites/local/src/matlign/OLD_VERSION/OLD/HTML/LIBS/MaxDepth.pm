use strict;
use warnings;

###################################################################################################
#Check file existing
sub FindMinAndMaxDepth {
	my($input_file)										= @_;
	my(@data)										= ();
	my(@columns)										= ();
	my($line)										= "";
	my($max_value)										= 0;
	my($min_value)										= 0;
	my($is_first)										= 1;
	
	open(TREE, $input_file);
	@data = <TREE>;
	close TREE;

	while(@data > 0) {
                $line = shift(@data);
		$line =~ s/^\s+|\s+$//g;
		if(length($line) > 0) {
			@columns = split(/\s/, $line);
			if(@columns >= 3) {
				if($is_first == 1) { 
					$max_value = $columns[3]; 
					$min_value = $columns[3];
					$is_first = 0;
				}
				if($columns[3] >= $max_value) { $max_value = $columns[3]; }
				if($columns[3] <= $min_value) { $min_value = $columns[3]; }
			}
		}
	}
	return($min_value, $max_value);
};
###################################################################################################
1

