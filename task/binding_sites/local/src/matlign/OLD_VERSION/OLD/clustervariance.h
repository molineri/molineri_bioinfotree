#ifndef clust_var_h
#define clust_var_h

#include <string>
#include <vector>

class ClusterVariance {
public:
	void SetElement(unsigned long, float, unsigned long);
	void GetElement(unsigned long, float&, float&, unsigned long&);
	void MergeElement(unsigned long, unsigned long, float);
	void GetVariance(unsigned long, float&, float&);
	void PrintStatistics(FILE*, bool header = 0);

	ClusterVariance( unsigned long );
	~ClusterVariance( );
private:
	std::vector <float> cluster_sum;
	std::vector <float> cluster_var;
	std::vector <unsigned long> cluster_size;
};

#endif
