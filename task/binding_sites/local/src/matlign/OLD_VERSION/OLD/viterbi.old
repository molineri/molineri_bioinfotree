/***************************************************************************
 *   Copyright (C) 2006 by Ari Loytynoja   *
 *   ari@ebi.ac.uk   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <iostream>
#include <vector>
#include <utility>
#include "viterbi.h"

extern float match;
extern float transi;
extern float transv;
extern float gOpen;
extern float gExt;
extern int tLength;
extern int noise;

using namespace std;

Viterbi::~Viterbi()
{
    delete sScore;
}

Viterbi::Viterbi(IntMatrix* st1, IntMatrix* st2)
{
    s1Single = true;
    s2Single = true;
    s1 = st1;
    s2 = st2;
    l1 = s1->X();
    l2 = s2->X();

    defineModel();
}

Viterbi::Viterbi(IntMatrix* st1, FlMatrix* mt2)
{
    s1Single = true;
    s2Single = false;
    s1 = st1;
    m2 = mt2;
    l1 = s1->X();
    l2 = m2->X();

    defineModel();
}

Viterbi::Viterbi(FlMatrix* mt1, FlMatrix* mt2)
{
    s1Single = false;
    s2Single = false;
    m1 = mt1;
    m2 = mt2;
    l1 = m1->X();
    l2 = m2->X();

    defineModel();
}


void Viterbi::defineModel()
{
    sScore = new FlMatrix(4,4,"substScores");
    sScore->initialise(0);

    int i;
    FOR(i,4){
	sScore->s(match,i,i);
    }

    if(transi!=0) {
	sScore->s(transi,0,2);
	sScore->s(transi,1,3);
	sScore->s(transi,2,0);
	sScore->s(transi,3,1);
    }

    if(transv!=0) {
	sScore->s(transv,0,1);
	sScore->s(transv,0,3);
	sScore->s(transv,1,0);
	sScore->s(transv,1,2);
	sScore->s(transv,2,1);
	sScore->s(transv,2,3);
	sScore->s(transv,3,0);
	sScore->s(transv,3,2);
    }

    if(noise>0) {
	cout<<endl<<"Substitution scores"<<endl;
	sScore->print();
    }
}

void Viterbi::align(IntMatrix* path, float* score, int norm){
	
    FlMatrix* M1 = new FlMatrix(l1,l2,"M1");
    FlMatrix* X1 = new FlMatrix(l1,l2,"X1");
    FlMatrix* Y1 = new FlMatrix(l1,l2,"Y1");
    FlMatrix* M2 = new FlMatrix(l1,l2,"M2");

    IntMatrix* pX1 = new IntMatrix(l1,l2,"pX1");
    IntMatrix* pY1 = new IntMatrix(l1,l2,"pY1");
    IntMatrix* pM2 = new IntMatrix(l1,l2,"pM2");

    small = -HUGE_VAL;
    M1->initialise(small);
    X1->initialise(small);
    Y1->initialise(small);
    M2->initialise(small);

    pX1->initialise(-1);
    pY1->initialise(-1);
    pM2->initialise(-1);

    int i,j;
    FOR(i,tLength+1){
	M1->s(0,i,0);
	M1->s(0,0,i);
    }

    float sc;

    FOR(i,l1) {
	FOR(j,l2) {
	    if(i>0) {
		X1->s( max(M1->g(i-1,j)+gOpen, X1->g(i-1,j)+gExt) , i, j );
		pX1->s( pt, i, j );
	    }
	    if(j>0) {
		Y1->s( max(M1->g(i,j-1)+gOpen, Y1->g(i,j-1)+gExt) , i, j );
		pY1->s( pt*2, i, j );
	    }
	    if(i>0 && j>0) {
		sc = this->score(i,j, norm);
		M1->s( M1->g(i-1,j-1)+sc, i, j );
		M2->s( max(X1->g(i-1,j-1), Y1->g(i-1,j-1), M2->g(i-1,j-1)+sc), i, j );
		pM2->s( pt+1, i, j );
	    }
	}
    }

    if(noise>0) {
	cout<<"M1"<<endl;
	M1->print();
	cout<<"X1"<<endl;
	X1->print();
	cout<<"Y1"<<endl;
	Y1->print();
	cout<<"M2"<<endl;
	M2->print();
    }
    if(noise>1) {
	cout<<"pX1"<<endl;
	pX1->print();
	cout<<"pY1"<<endl;
	pY1->print();
	cout<<"pM2"<<endl;
	pM2->print();
    }

    sc = small;
    pt = -1;
    int mi = -1;
    int mj = -1;

    j = l2-1;
    for(i = l1-1;i>=l1-tLength,i>0;i--){
	if(M1->g(i,j)>sc){
	    sc = M1->g(i,j);
	    pt=0;
	    mi=i;
	    mj=j;
	}
	if(M2->g(i,j)>sc && pM2->g(i,j)==3){
	    sc = M2->g(i,j);
	    pt=3;
	    mi=i;
	    mj=j;
	}
    }

    i = l1-1;
    for(j = l2-1;j>=l2-tLength,j>0;j--){
	if(M1->g(i,j)>sc){
	    sc = M1->g(i,j);
	    pt=0;
	    mj=j;
	    mi=i;
	}
	if(M2->g(i,j)>sc && pM2->g(i,j)==3){
	    sc = M2->g(i,j);
	    pt=3;
	    mj=j;
	    mi=i;
	}
    }

    *score = sc;

    int k = 0;

    for(i=l1-1;mi>0 && i>mi;i--)
	path->s(1,k++);

    for(j=l2-1;mj>0 && j>mj;j--)
	path->s(2,k++);

    for(;i>=0,j>=0;){

	if(i==0 || j==0)
	    break;

	path->s(pt,k++);

	if(pt==0) {
	    i--;
	    j--;
	} else if(pt==1) {
	    pt = pX1->g(i,j);
	    i--;
	} else if(pt==2) {
	    pt = pY1->g(i,j);
	    j--;
	} else if(pt==3) {
	    pt = pM2->g(i,j);
	    i--;
	    j--;
	}
    }

    for(;i>0;i--)
	path->s(1,k++);

    for(;j>0;j--)
	path->s(2,k++);


    delete M1;
    delete X1;
    delete Y1;
    delete M2;

    delete pX1;
    delete pY1;
    delete pM2;

}

float Viterbi::score(int i, int j, int norm)
{
    float mScore = 0;
    float max_val_score = 0;
    float meanm1m2 = 0;
    float m1_ss = 0, m2_ss = 0;

    if(s1Single && s2Single) {
	mScore = sScore->g(s1->g(i),s2->g(j));
    } else if(s1Single) {
	int m;
	FOR(m,4) {
	    mScore += sScore->g(s1->g(i),m)*m2->g(j,m);
	}
    } else {
	int m,n;
	FOR(m,4) {
	    FOR(n,4) {
		mScore += sScore->g(n,m)*m1->g(i,n)*m2->g(j,m);

		if((m == 0) && (n == 0)) { max_val_score = sScore->g(n,m); }
                if(sScore->g(n,m) >= max_val_score) { max_val_score = sScore->g(n,m); }

		m1_ss += sScore->g(n,m) * m1->g(i,n) * m1->g(i,m);
		m2_ss += sScore->g(n,m) * m2->g(j,n) * m2->g(j,m);

	    }
	}
    }

    if(norm == 0) {			//no normalization
	return mScore;
    } else if(norm == 1) {
	int round_score;
	round_score = int(mScore + 0.5);
	mScore = float(round_score);
    } else if(norm == 2) {		//normalize using the max-score value
	mScore /= max_val_score;
	return mScore;
    } else if(norm == 3) {		//normalize using the max-mean
	mScore = mScore / ((m1_ss + m2_ss) / 2);
	return mScore;
    } else {
	return mScore;
    }
}

float Viterbi::max(float a,float b)
{
    if(a==small && b==small) {
	pt = 0;
	return a;
    } else if(a>b) {
	pt = 0;
	return a;
    } else if(a<b) {
	pt = 1;
	return b;
    } else {
	if(rndBool()) {
	    pt = 0;
	    return a;
	} else {
	    pt = 1;
	    return b;
	}
    }
}

float Viterbi::max(float a,float b, float c) 
{
    if(a==small && b==small && c==small) {
	pt = 0;
	return a;
    } else if(a>b && a>c) {
	pt = 0;
	return a;
    } else if(a<b && b>c) {
	pt = 1;
	return b;
    } else if(a<c && b<c) {
	pt = 2;
	return c;
    } else if(a>b && a==c) {
	if(rndBool()) {
	    pt = 0;
	    return a;
	} else {
	    pt = 2;
	    return c;
	}
    } else if(a>c && a==b) {
	if(rndBool()) {
	    pt = 0;
	    return a;
	} else {
	    pt = 1;
	    return b;
	}
    } else if(a<b && b==c) {
	if(rndBool()) {
	    pt = 1;
	    return b;
	} else {
	    pt = 2;
	    return c;
	}
    } else {
	int i = rndInt(3);
	pt = i;
	if(i==0 || i==3){
	    return a;
	} else if(i==1){
	    return b;
	} else if(i==2){
	    return c;
	} else {
	    cout <<"random number error: i="<<i<<endl;
	    exit(1);
	}
    }
}

bool Viterbi::rndBool() 
{
    double p = (double)rand()/(double)RAND_MAX;
    if(p>0.5)
	return true;
    else 
	return false;
}

int Viterbi::rndInt(int i) 
{
    return (int)(rand()/(double)RAND_MAX*(double)i);
}
