#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "clustervariance.h"

using namespace std;

//*********************************************************************************************************//
ClusterVariance::ClusterVariance(unsigned long cluster_element) {
	for(int i = 0; i < cluster_element; ++i) {
		cluster_sum.push_back(0);
		cluster_var.push_back(0);
		cluster_size.push_back(1);		
	}
};
//*********************************************************************************************************//
ClusterVariance::~ClusterVariance() {
	cluster_sum.clear();
	cluster_var.clear();
	cluster_size.clear();
};
//*********************************************************************************************************//
void ClusterVariance::SetElement(unsigned long element, float distance_sum, unsigned long size) {
	cluster_sum[element] = distance_sum;
	cluster_var[element] = pow(distance_sum, 2);
	cluster_size[element] = size;
};
//*********************************************************************************************************//
void ClusterVariance::MergeElement(unsigned long element1, unsigned long element2, float distance) {
	float sum(0);
	float var(0);
	unsigned long size(0);

	size = cluster_size[element1] + cluster_size[element2];
	sum  = cluster_sum[element1] + cluster_sum[element2];
	var  = cluster_var[element1] + cluster_var[element2];

	cluster_sum[element1] = sum + distance;
	cluster_var[element1] = var + pow(distance, 2);
	cluster_size[element1] = size;

	cluster_sum.erase(cluster_sum.begin() + element2, cluster_sum.begin() + element2 + 1);
	cluster_var.erase(cluster_var.begin() + element2, cluster_var.begin() + element2 + 1);
	cluster_size.erase(cluster_size.begin() + element2, cluster_size.begin() + element2 + 1);
};
//*********************************************************************************************************//
void ClusterVariance::GetElement(unsigned long element, float &distance_sum, float &distance_var, unsigned long &element_size) {
	distance_sum = cluster_sum[element];
	distance_var = cluster_var[element];
	element_size = cluster_size[element];
};
//*********************************************************************************************************//
void ClusterVariance::GetVariance(unsigned long element, float &distance_mean, float &distance_stdev) {
        float distance_sum(0);
	float distance_var(0);
	unsigned long element_size(0);

	distance_sum = cluster_sum[element];
        distance_var = cluster_var[element];
        element_size = ((cluster_size[element] * (cluster_size[element] - 1))/2);

	if (element_size == 0) { element_size = 1; }

	distance_mean = distance_sum / element_size;
	//distance_mean = distance_var;
	
	if(element_size > 1) {
		distance_stdev = ((element_size * distance_var) - pow(distance_sum, 2))/(element_size * (element_size - 1));
	} else {
		distance_stdev = ((element_size * distance_var) - pow(distance_sum, 2))/(element_size * (element_size - 0));
	}
};
//*********************************************************************************************************//
void ClusterVariance::PrintStatistics(FILE *outstream, bool header) {
	unsigned long cluster_number(0);
	float mean(0);
	float stdev(0);
	float mean_tot(0);
	float stdev_tot(0);

	cluster_number = cluster_sum.size();

	if (header == 1) { fprintf (outstream, "n\tmean\tvar\n"); }

	for(unsigned int i = 0; i < cluster_number; ++i) {
		GetVariance(i, mean, stdev);
		mean_tot = mean_tot + mean;
		stdev_tot = stdev_tot + stdev;
//cout << "ZZZ " << i << " " <<  mean <<  " " << stdev << " " << mean_tot << " " << stdev_tot << "\n";
//fprintf (outstream, "XXX%.u\t%.3f\t%.3f\t", cluster_number, mean_tot, stdev_tot);
//fprintf (outstream, "%.u\t%.3f\t%.3f\n", cluster_number, mean, stdev);
	}
		
	//mean_tot = mean_tot / cluster_number;
	fprintf (outstream, "%.u\t%.3e\t%.3e\n", cluster_number, mean_tot, stdev_tot);
};
//*********************************************************************************************************//

