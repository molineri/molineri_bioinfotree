/***************************************************************************
 *   Copyright (C) 2006 by Ari Loytynoja   *
 *   ari@ebi.ac.uk   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <iostream>
#include <vector>
#include <utility>
#include "viterbi.h"

extern float match;
extern float transi;
extern float transv;
extern float gOpen;
extern float gExt;
extern int tLength;
extern int noise;
extern bool FIRST;
extern bool SECOND;
extern int norm;

using namespace std;

Viterbi::~Viterbi()
{
    delete sScore;
}

Viterbi::Viterbi(IntMatrix* st1, IntMatrix* st2)
{
    s1Single = true;
    s2Single = true;
    s1 = st1;
    s2 = st2;
    l1 = s1->X();
    l2 = s2->X();

    defineModel();
}

Viterbi::Viterbi(IntMatrix* st1, FlMatrix* mt2)
{
    s1Single = true;
    s2Single = false;
    s1 = st1;
    m2 = mt2;
    l1 = s1->X();
    l2 = m2->X();

    defineModel();
}

Viterbi::Viterbi(FlMatrix* mt1, FlMatrix* mt2)
{
    s1Single = false;
    s2Single = false;
    m1 = mt1;
    m2 = mt2;
    l1 = m1->X();
    l2 = m2->X();

    defineModel();
}


void Viterbi::defineModel()
{
    sScore = new FlMatrix(4,4,"substScores");
    sScore->initialise(0);

    int i;
    FOR(i,4){
	sScore->s(match,i,i);
    }

    if(transi!=0) {
	sScore->s(transi,0,2);
	sScore->s(transi,1,3);
	sScore->s(transi,2,0);
	sScore->s(transi,3,1);
    }

    if(transv!=0) {
	sScore->s(transv,0,1);
	sScore->s(transv,0,3);
	sScore->s(transv,1,0);
	sScore->s(transv,1,2);
	sScore->s(transv,2,1);
	sScore->s(transv,2,3);
	sScore->s(transv,3,0);
	sScore->s(transv,3,2);
    }

    if(noise>0) {
	cout<<endl<<"Substitution scores"<<endl;
	sScore->print();
    }
}

void Viterbi::align(IntMatrix* path, float* score){
	
    FlMatrix* M1 = new FlMatrix(l1,l2,"M1");
    FlMatrix* X1 = new FlMatrix(l1,l2,"X1");
    FlMatrix* Y1 = new FlMatrix(l1,l2,"Y1");
    FlMatrix* M2 = new FlMatrix(l1,l2,"M2");

    IntMatrix* pX1 = new IntMatrix(l1,l2,"pX1");
    IntMatrix* pY1 = new IntMatrix(l1,l2,"pY1");
    IntMatrix* pM2 = new IntMatrix(l1,l2,"pM2");

    small = -HUGE_VAL;
    M1->initialise(small);
    X1->initialise(small);
    Y1->initialise(small);
    M2->initialise(small);

    pX1->initialise(-1);
    pY1->initialise(-1);
    pM2->initialise(-1);

    int i,j;
    FOR(i,min(tLength+1,l1)){
        M1->s(0,i,0);
    }
    FOR(j,min(tLength+1,l2)){
        M1->s(0,0,j);
    }

    float sc;

    FOR(i,l1) {
	FOR(j,l2) {
	    if(i>0 && j>0) {

		X1->s( max(M1->g(i-1,j)+gOpen, X1->g(i-1,j)+gExt) , i, j );
		pX1->s( pt, i, j );

		Y1->s( max(M1->g(i,j-1)+gOpen, Y1->g(i,j-1)+gExt) , i, j );
		pY1->s( pt*2, i, j );
		
		sc = this->score(i,j);
		M1->s( M1->g(i-1,j-1)+sc, i, j );
		//M2->s( max(X1->g(i-1,j-1), Y1->g(i-1,j-1), M2->g(i-1,j-1)+sc), i, j );
		M2->s( max(X1->g(i-1,j-1), Y1->g(i-1,j-1), M2->g(i-1,j-1))+sc, i, j );
		pM2->s( pt+1, i, j );
	    }
	}
    }

    if(noise>0) {
	cout<<"M1"<<endl;
	M1->print();
	cout<<"X1"<<endl;
	X1->print();
	cout<<"Y1"<<endl;
	Y1->print();
	cout<<"M2"<<endl;
	M2->print();
    }
    if(noise>1) {
	cout<<"pX1"<<endl;
	pX1->print();
	cout<<"pY1"<<endl;
	pY1->print();
	cout<<"pM2"<<endl;
	pM2->print();
    }

    sc = small;
    pt = -1;
    int mi = -1;
    int mj = -1;

    j = l2-1;
    for(i = l1-1;i>=l1-tLength-1 && i>0;i--){
	if((FIRST) && (M1->g(i,j)>sc)){
	//if(M1->g(i,j)>sc){
	    sc = M1->g(i,j);
	    pt=0;
	    mi=i;
	    mj=j;
	}
	if((SECOND) && (M2->g(i,j)>sc && pM2->g(i,j)==3)){
	//if(M2->g(i,j)>sc && pM2->g(i,j)==3){
	    sc = M2->g(i,j);
	    pt=3;
	    mi=i;
	    mj=j;
	}
    }

    i = l1-1;
    for(j = l2-1;j>=l2-tLength-1 && j>0;j--){
	if((FIRST) && (M1->g(i,j)>sc)){
	//if(M1->g(i,j)>sc){
	    sc = M1->g(i,j);
	    pt=0;
	    mj=j;
	    mi=i;
	}
	if((SECOND) && (M2->g(i,j)>sc && pM2->g(i,j)==3)){
	//if(M2->g(i,j)>sc && pM2->g(i,j)==3){
	    sc = M2->g(i,j);
	    pt=3;
	    mj=j;
	    mi=i;
	}
    }

    i = l1-1;
    j = l2-1;
    if((SECOND) && (M2->g(i-1,j-1)+this->score(i,j)>sc)){
    //if(M2->g(i-1,j-1)+this->score(i,j)>sc){
	sc = M2->g(i-1,j-1)+this->score(i,j);
	pt=3;
	mj=j;
	mi=i;
    }

    *score = sc;

    int k = 0;

    for(i=l1-1;mi>0 && i>mi;i--)
	path->s(1,k++);

    for(j=l2-1;mj>0 && j>mj;j--)
	path->s(2,k++);

    for(;i>=0,j>=0;){

	if(i==0 || j==0)
	    break;

	path->s(pt,k++);

	if(pt==0) {
	    i--;
	    j--;
	} else if(pt==1) {
	    pt = pX1->g(i,j);
	    i--;
	} else if(pt==2) {
	    pt = pY1->g(i,j);
	    j--;
	} else if(pt==3) {
	    pt = pM2->g(i,j);
	    i--;
	    j--;
	}
    }

    for(;i>0;i--)
	path->s(1,k++);

    for(;j>0;j--)
	path->s(2,k++);

    delete M1;
    delete X1;
    delete Y1;
    delete M2;

    delete pX1;
    delete pY1;
    delete pM2;

}

float Viterbi::score(int i, int j)
{
    float mScore = 0;
    float total_count = 0;
    float max_val_score = 0;
    float min_val_score = 0;
    float meanm1m2 = 0;
    float m1_ss = 0, m2_ss = 0, mm1 = 0, mm2 = 0, mm3 = 0, aa = 0, bb = 0, kaa = 0, kbb = 0;
    float scale_score = 0;
    float max_m1_m2 = 0;

    //MUTUAL INFORMATION (MI) [-1...1]  //mScore += sScore->g(n,m)	*	m1->g(i,n)	*	m2->g(j,m);
    /*
    
    float h_1 = 0, h_2 = 0, h_1_2 = 0, mi = 0;
    float contingency_tab[4][4] = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
    float min = 0;
    float row_v = 0, col_v = 0;

    int m,n,o;
    FOR(m ,4) {
	if ((m1->g(i,m)) > 0) { h_1 = h_1 + ((m1->g(i,m))*log2(m1->g(i,m))); }
	if ((m2->g(j,m)) > 0) { h_2 = h_2 + ((m2->g(j,m))*log2(m2->g(j,m))); }
    }
    h_1 = h_1 * -1;
    h_2 = h_2 * -1;

    m = n = 0;
    FOR(m ,4) {
	FOR(n ,4) {		
		col_v = m1->g(i,m);
		row_v = m2->g(j,n);

		for(o=0; o < (m); ++o) {
			row_v = row_v - contingency_tab[o][n];
		}

		for(o=0; o < (n); ++o) {
			col_v = col_v - contingency_tab[m][o];
		}
		if (row_v < col_v) { contingency_tab[m][n] = row_v; }
		else { contingency_tab[m][n] = col_v; }
	}
    }

    m = n = 0;
    FOR(m ,4) {
	FOR(n ,4) {
		if (contingency_tab[m][n] > 0) { h_1_2 = h_1_2 + ((contingency_tab[m][n])*(log2(contingency_tab[m][n]))) ;}
	}
    }
    h_1_2 = h_1_2 * -1;
    return(h_1 + h_2 - h_1_2);
    return((h_1 + h_2 - h_1_2)/(h_1+h_2));
    */
    
    vector <float> arr1;
    vector <float> arr2;

    vector <float> arr1_org;
    vector <float> arr2_org;

    vector <float> rank1;
    vector <float> rank2;
    
    int m, n;
    FOR(m ,4) {
        arr1_org.push_back(m1->g(i,m));
	arr2_org.push_back(m2->g(j,m));
	
	arr1.push_back(m1->g(i,m));
	arr2.push_back(m2->g(j,m));
    }
    sort(arr1.begin(), arr1.end());
    sort(arr2.begin(), arr2.end());
    
    
    //KENDAL'S TAU[0...1]
    /* 
    FOR(m ,4) {
	mm1 = arr1_org[m];
	FOR(n ,4) {
		if(arr1[n] == mm1) { arr2[n] = arr2_org[m]; break; }
	}

    }

    for (m = 0; m < 4; ++m) {
	for (n = (m+1); n < 4; ++n) {
		if ((arr1[m] < arr1[n]) && (arr2[m] > arr2[n])) { mm2 = mm2 + 1; }
	}
    }
    mm2 = 1 - (mm2 / 6);
    mm2 = mm2 * 2;
    mm2 = mm2 - 1;
    return(mm2);
    */

    // SPEARMAN'S RANK CORRELATION [-1...1]
      	
    FOR(m ,4) {
	mm1 = arr1_org[m];
	mm2 = arr2_org[m];
	FOR(n ,4) {
		if(arr1[n] == mm1) { rank1.push_back(n); break; }
	} FOR(n ,4) {
		if(arr2[n] == mm2) { rank2.push_back(n); break; }
	}
    }
   
    FOR(m ,4) {    
	mm2 = rank1[m] - rank2[m];
	mm3 = mm3 + pow(mm2,2);        
    }

    mm3 = 1 - ((6 * mm3) / 60);
    return(mm3); 
    

    //Mahalanobis distance variance
    /*
    int m;
    float var1=0, var2=0, var3=0, var4=0;
    FOR(m, 4) {
	mm2 = (m1->g(i,m) + m2->g(j,m));
	mm3 = pow(m1->g(i,m),2) + pow(m2->g(j,m),2);

	if(m == 0) { var1 = (((2 * mm3) - pow(mm2,2))/2) ; if (var1 > 0) { var1 = pow(var1, 2); } else { var1 = 1; } }
	if(m == 1) { var2 = (((2 * mm3) - pow(mm2,2))/2) ; if (var2 > 0) { var2 = pow(var2, 2); } else { var2 = 1; } }
	if(m == 2) { var3 = (((2 * mm3) - pow(mm2,2))/2) ; if (var3 > 0) { var3 = pow(var3, 2); } else { var3 = 1; } }
	if(m == 3) { var4 = (((2 * mm3) - pow(mm2,2))/2) ; if (var4 > 0) { var4 = pow(var4, 2); } else { var4 = 1; } }
    }
    */

    char buffer[100];

    //ENTROPY FOR PEARSON SCALING//
    /*
    float h_1 = 0, h_2 = 0, h_1_2 = 0, h_diff = 0;
    float avg_h12 = 0;
    int m;
    FOR(m ,4) {
        if ((m1->g(i,m)) > 0) { h_1 = h_1 + ((m1->g(i,m))*log2(m1->g(i,m))); }
        if ((m2->g(j,m)) > 0) { h_2 = h_2 + ((m2->g(j,m))*log2(m2->g(j,m))); }
	avg_h12 = ((m1->g(i,m) + m2->g(j,m)) / 2);
	if (avg_h12 > 0) { h_1_2 = h_1_2 + (avg_h12)*log2(avg_h12); }
    }
    h_1 = 2-(h_1 * -1);
    h_2 = 2-(h_2 * -1);
    h_1_2 = 2-(h_1_2 * -1);
    h_diff = ((2 * h_1_2) - (h_1 + h_2));
    h_diff = h_diff + 1;
    return(h_diff);
    */

    //Combine spearman's rank corr and Entropy
    //mm3 = (h_1 + h_2) * mm3;
    //return(mm3);
    //Kendal's Tau
    //mm2 = (h_1 + h_2) * mm2;
    //return(mm2);
    
    //int m;
    FOR(m,4) {               
	aa = (m1->g(i,m));
        sprintf(buffer, "%.2f", aa);
        aa = float(atof(buffer));

	bb = (m2->g(j,m));
	sprintf(buffer, "%.2f", bb);
	bb = float(atof(buffer));

	//if (aa == 0) { aa = 0.001; } else if (aa == 1) { aa = 0.999; } 
	//aa = log(aa) - log(1-aa);
	//if (bb == 0) { bb = 0.001; } else if (bb == 1) { bb = 0.999; } 
	//bb = log(bb) - log(1-bb);

        kaa = kaa + aa;
        kbb = kbb + bb;
    }
    kaa = kaa / 4;
    kbb = kbb / 4;
 
    if(s1Single && s2Single) {
	mScore = sScore->g(s1->g(i),s2->g(j));
    } else if(s1Single) {
	int m;
	FOR(m,4) {
	    mScore += sScore->g(s1->g(i),m)*m2->g(j,m);
	}
    } else {
	int m,n;
	FOR(m,4) {
	    FOR(n,4) {
		//mScore += sScore->g(n,m)*m1->g(i,n)*m2->g(j,m);
                //if((m == 0) && (n == 0)) { max_val_score = sScore->g(n,m);  min_val_score = sScore->g(n,m); }
		//if(sScore->g(n,m) >= max_val_score) { max_val_score = sScore->g(n,m); }
		//if(sScore->g(n,m) <= min_val_score) { min_val_score = sScore->g(n,m); }
		//m1_ss += sScore->g(n,m) * m1->g(i,n) * m1->g(i,m);
		//m2_ss += sScore->g(n,m) * m2->g(j,m) * m2->g(j,n);

		//EMBOSS
		//mScore += sScore->g(n,m)*m1->g(i,n)*m2->g(j,m);

		//YSRA
		//if (m == n) {
		//	mScore += pow((m1->g(i,n) - m2->g(j,m)),2);
		//}

		//Pietrokovski NAR 1996, PCC
		//if (m == n) {
		//	mm1 = mm1 + ((m1->g(i,n) - 0.25) * (m2->g(j,m) - 0.25));
		//	mm2 = mm2 + pow((m1->g(i,n) - 0.25),2);
		//	mm3 = mm3 + pow((m2->g(j,m) - 0.25),2);
		//}

		//Dot product
		//if (m == n) {
		//      	aa = (m1->g(i,n));
		//      	bb = (m2->g(j,m));

        	//	sprintf(buffer, "%.2f", aa);
        	//	aa = float(atof(buffer));
        	//	sprintf(buffer, "%.2f", bb);
        	//	bb = float(atof(buffer));

		      	//if (aa == 0) { aa = 0.001; } else if (aa == 1) { aa = 0.999; } 
			//aa = log(aa) - log(1-aa);
		      	//if (bb == 0) { bb = 0.001; } else if (bb == 1) { bb = 0.999; } 
			//bb = log(bb) - log(1-bb);

		//      	aa = aa - kaa;
		//      	bb = bb - kbb;

		//      	mm1 = mm1 + (aa * bb);
                //      	mm2 = mm2 + pow(aa, 2);
                //      	mm3 = mm3 + pow(bb, 2);
		//}

		//Pietrokovski NAR 1996, Euc and // if-sentences are for Maha-distance
		if (m == n) {
			//if (m == 0) { mm1 = mm1 + (pow((m1->g(i,n) - (m2->g(j,m))), 2)/ var1); }
			//if (m == 1) { mm1 = mm1 + (pow((m1->g(i,n) - (m2->g(j,m))), 2)/ var2); }
			//if (m == 2) { mm1 = mm1 + (pow((m1->g(i,n) - (m2->g(j,m))), 2)/ var3); }
			//if (m == 3) { mm1 = mm1 + (pow((m1->g(i,n) - (m2->g(j,m))), 2)/ var4); }
			mm1 = mm1 + pow((m1->g(i,n) - (m2->g(j,m))), 2);
		}
	    }
	}
    }

    if(norm == 0) {                     //no normalization
        mm1 = sqrt(mm1);
        mm1 = mm1 / sqrt(2.0)* 2;
        mm1 = (mm1 - 1) * -1;
        return(mm1);
	

	//return(mScore);
    } else if (norm == 10) {
	mm1 = (mm1 / sqrt(mm2*mm3));
	//mm1 = ((mm1 - -1)/(1 - -1));
	//mm1 = (mm1 * (max_val_score - min_val_score)) + min_val_score;
	return(mm1);
	//return(mScore + 7);
	
	//YSRA
    	//return (2-mScore);

	//Pietrokovski, PCC
	return((mm1 / sqrt(mm2*mm3)));

	mm1 = (mm1 / sqrt(mm2*mm3));
	mm1 = ((((mm1 - -1)/(1 - -1))*(max_val_score - min_val_score))-min_val_score);
	return(mm1);

	mm1 = (mm1 / sqrt(mm2*mm3));
	mm1 = ((mm1 + 1) * 9);
	//mm1 = (h_1 * h_2) * mm1;
	//mm1 = mm1 + h_diff;
	return(mm1);
	
	//Pietrokovski, Euc
	//mm1 = sqrt(mm1);
        //mm1 = mm1 / sqrt(2.0)* 2;
        //mm1 = (mm1 - 1) * -1;
        //return(mm1);

	//Dot product
	//return(mm1);	

	//MAHA
	//mm1 = sqrt(mm1);
	//if(mm1 > 0) {
	//	mm1 = 1/mm1;
	//} else {
	//	mm1 = 1;
	//}
	//return(mm1);

    } else if(norm == 1) {		//EMBOSS
	int round_me;

	if(mScore >= 0) {
		round_me = (int)(mScore + 0.5);
	} else {
		round_me = (int)(mScore - 0.5);
	}
	
	mScore = float(round_me);
	return mScore;
    } else if(norm == 2) {              //normalize using the max-score value
	mScore /= max_val_score;
	return mScore;
    } else if(norm == 3) {              //normalize using the min-max-mean
	if (m1_ss >= m2_ss) { max_m1_m2 = m1_ss; }
	else { max_m1_m2 = m2_ss; }

	scale_score = (mScore - min_val_score) / (max_m1_m2 - min_val_score);
	scale_score = scale_score * (max_val_score - min_val_score);
	scale_score = scale_score + min_val_score;
    	//rounded_sum = m1_ss + m2_ss;
	//sprintf(buffer, "%.5f", (m1_ss + m2_ss));	
	//rounded_sum = float(atof(buffer));
	//mScore = mScore / (rounded_sum / 2);
	
	return scale_score;
    } else {
	return mScore;
    }
}

float Viterbi::max(float a,float b)
{
    if(a==small && b==small) {
	pt = 0;
	return a;
    } else if(a>b) {
	pt = 0;
	return a;
    } else if(a<b) {
	pt = 1;
	return b;
    } else {
	if(rndBool()) {
	    pt = 0;
	    return a;
	} else {
	    pt = 1;
	    return b;
	}
    }
}

float Viterbi::max(float a,float b, float c) 
{
    if(a==small && b==small && c==small) {
	pt = 0;
	return a;
    } else if(a>b && a>c) {
	pt = 0;
	return a;
    } else if(a<b && b>c) {
	pt = 1;
	return b;
    } else if(a<c && b<c) {
	pt = 2;
	return c;
    } else if(a>b && a==c) {
	if(rndBool()) {
	    pt = 0;
	    return a;
	} else {
	    pt = 2;
	    return c;
	}
    } else if(a>c && a==b) {
	if(rndBool()) {
	    pt = 0;
	    return a;
	} else {
	    pt = 1;
	    return b;
	}
    } else if(a<b && b==c) {
	if(rndBool()) {
	    pt = 1;
	    return b;
	} else {
	    pt = 2;
	    return c;
	}
    } else {
	int i = rndInt(3);
	pt = i;
	if(i==0 || i==3){
	    return a;
	} else if(i==1){
	    return b;
	} else if(i==2){
	    return c;
	} else {
	    cout <<"random number error: i="<<i<<endl;
	    exit(1);
	}
    }
}

bool Viterbi::rndBool() 
{
    double p = (double)rand()/(double)RAND_MAX;
    if(p>0.5)
	return true;
    else 
	return false;
}

int Viterbi::rndInt(int i) 
{
    return (int)(rand()/(double)RAND_MAX*(double)i);
}
