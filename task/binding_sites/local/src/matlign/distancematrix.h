/***************************************************************************
 *   Copyright (C) 2007 by Ari Loytynoja and Matti Kankainen               *
 *   ari@ebi.ac.uk / matti.kankainen@helsinki.fi                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef dist_mat_h
#define dist_mat_h
#include <vector>

class DistanceMatrix {
public:
	float ** distance_matrix;	

	void FreeMemory();
	void SetSize( unsigned long, unsigned long );
	void PrintMatrix( FILE* );
	void PrintMatrixAvg( FILE* );
	float FindMax( unsigned long&, unsigned long& );
	void RemoveElement( unsigned long );

	float SetElement( unsigned long, unsigned long, float);
	float GetElement( unsigned long, unsigned long);
	float GetElementAvg( unsigned long, unsigned long);
	
	unsigned long SetElementSize (unsigned long, unsigned long);
	unsigned long GetElementSize (unsigned long);
	unsigned long AddElementSize (unsigned long);
	unsigned long MergeElementSize(unsigned long, unsigned long);

	DistanceMatrix();
	~DistanceMatrix();
private:
	bool has_data;
	std::vector <unsigned long> element_size;

	unsigned long row;
	unsigned long col;

};

#endif
