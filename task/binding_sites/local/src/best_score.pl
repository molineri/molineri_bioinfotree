#!/usr/bin/perl
use strict;
use warnings;

$,="\t";
$\="\n";

if (scalar(@ARGV)!=1) {
	die "Usage: $0 bg_file < pwm_file\noutput the min and max possible score for each matrix";
}

#we get the bg scores
open(my $bg, $ARGV[0]) or die $!;
my $b = <$bg>;
chomp($b);
my @bgs = split("\t", $b); #A C G T
close($bg);

#we get the max score foreach pwm
my %minmax_score;
while(<STDIN>) {
	chomp;
	my @line = split("\t", $_);
	my $name = shift(@line);
	shift(@line); #we get rid of the position
	my ($min, $max) = &minmax_logRatio(\@bgs, \@line);
	$minmax_score{$name}[0] += $min; 
	$minmax_score{$name}[1] += $max; 
}

for(keys(%minmax_score)){
	print $_,$minmax_score{$_}[0],$minmax_score{$_}[1]
}

sub minmax_logRatio {
	#evaluates log2ratios for a position (from A C T G counts and bg) and return the max
	my $bgR = $_[0];
	my $countsR = $_[1];
	my $sum = 0;
	for (my $i = 0; $i < scalar( @{ $countsR } ); $i++) {
		$sum += $countsR->[$i];
	}
	for (my $i = 0; $i < scalar( @{ $countsR } ); $i++) {
		$countsR->[$i] = log(($countsR->[$i] / $sum)/ $bgR->[$i]) / log(2);
	}
	my $max = $countsR->[0];
	my $min = $countsR->[0];
	for (my $i = 1; $i < scalar( @{ $countsR } ); $i++) {
		if ($countsR->[$i] > $max) {
			$max = $countsR->[$i];
		} elsif ($countsR->[$i] < $min) {
			$min = $countsR->[$i];
		}
		
	}
	return ($min, $max);
}

