#!/usr/bin/env python
from database import Adaptor, Cursor, TypeCache
from optparse import OptionParser
from sys import exit, stdin
from vfork.sql.databaseparams import add_database_options, collect_database_params

def parse_line(line):
	tokens = line.strip().split()
	if len(tokens) != 4:
		raise ValueError, 'unexpected token number'
	
	try:
		tokens[0] = int(tokens[0])
		tokens[1] = int(tokens[1])
	except ValueError:
		raise ValueError, 'invalid integer value'
	
	return tokens

if __name__ == '__main__':
	parser = OptionParser(usage='%prog SPECIES CHROMOSOME <REGIONS')
	add_database_options(parser)
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	database_params = collect_database_params(options, {'db': 'alignment_viewer', 'passwd': 'nopass'})
	
	cursor = Cursor(**database_params)
	type_cache = TypeCache(cursor)
	adaptor = Adaptor(cursor, type_cache, partitions=False)
	
	chromosome_id = adaptor.get_chromosome_id(args[0], args[1])
	if chromosome_id is None:
		chromosome_id = adaptor.insert_chromosome(args[0], args[1])
	
	try:
		for lineno, line in enumerate(stdin):
			try:
				region_info = parse_line(line)
			except ValueError, e:
				exit('Error: %s at line %d' % (e.args[0], lineno+1))
			
			adaptor.insert_region(chromosome_id, *region_info)
	except:
		adaptor.rollback()
		raise
	else:
		adaptor.commit()

