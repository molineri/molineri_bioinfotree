from MySQLdb import connect

class Cursor(object):
	def __init__(self, user, passwd, db, host):
		self.connection = None
		self.connection = connect(user=user, passwd=passwd, db=db, host=host)
		self.cursor = self.connection.cursor()
	
	def __del__(self):
		self.close()
	
	def close(self):
		if self.connection is not None:
			self.cursor.close()
			self.connection.close()
			self.cursor = self.connection = None
	
	def execute(self, *args):
		return self.cursor.execute(*args)
	
	def fetchone(self):
		return self.cursor.fetchone()
	
	def commit(self):
		return self.connection.commit()

	def rollback(self):
		return self.connection.rollback()
	
	@property
	def lastrowid(self):
		return self.cursor.lastrowid
	
	@property
	def rowcount(self):
		return self.cursor.rowcount

class TypeCache(object):
	def __init__(self, cursor):
		self.name_to_id = {}
		self.id_to_name = {}
		self.load_type_cache(cursor)
	
	def get_by_name(self, type, subtype):
		return self.name_to_id[(type, subtype)]
	
	def get_by_id(self, id):
		return self.id_to_name[id]
	
	def update(self, id, type, subtype):
		self.name_to_id[(type, subtype)] = id
		self.id_to_name[id] = (type, subtype)
	
	##
	## Internal use only
	##
	def load_type_cache(self, cursor):
		cursor.execute('SELECT id, type, subtype FROM types')
		
		while True:
			row = cursor.fetchone()
			if row is None:
				break
			else:
				self.update(*row)

class Adaptor(object):
	def __init__(self, cursor, type_cache, max_rownum=10240, partitions=False):
		self.cursor = cursor
		self.type_cache = type_cache
		self.max_rownum = max_rownum
		self.partitions = partitions

	def commit(self):
		self.cursor.commit()

	def rollback(self):
		self.cursor.rollback()
	
	def get_chromosome_id(self, species, chromosome):
		self.cursor.execute('SELECT id FROM chromosomes WHERE species=%s AND chromosome=%s', (species, chromosome))
		row = self.cursor.fetchone()
		if row is None:
			return None
		else:
			return row[0]
	
	def insert_chromosome(self, species, chromosome):
		self.cursor.execute('INSERT INTO chromosomes (species, chromosome) VALUES(%s, %s)', (species, chromosome))
		chromosome_id = self.cursor.lastrowid
		
		if self.partitions:
			self.cursor.execute('ALTER TABLE regions ADD PARTITION (PARTITION partition%d VALUES IN (%d))' % (chromosome_id, chromosome_id))
		
		return chromosome_id
	
	def get_chromosome_pair_id(self, species1, chromosome1, species2, chromosome2):
		query = '''
			SELECT p.id
				FROM chromosome_pairs p
				JOIN (chromosomes c1, chromosomes c2) ON (p.chromosome1_id = c1.id AND p.chromosome2_id = c2.id)
				WHERE
					c1.species=%s AND
					c1.chromosome=%s AND
					c2.species=%s AND
					c2.chromosome=%s
		'''
		self.cursor.execute(query, (species1, chromosome1, species2, chromosome2))
		row = self.cursor.fetchone()
		if row is None:
			return None
		else:
			return row[0]
	
	def insert_chromosome_pair(self, species1, chromosome1, species2, chromosome2):
		self.cursor.execute('SELECT id FROM chromosomes WHERE species=%s AND chromosome=%s', (species1, chromosome1))
		row = self.cursor.fetchone()
		if row is None:
			raise ValueError, 'chromosome %s of species %s is missing' % (chromosome1, species1)
		chromosome1_id = row[0]
		
		self.cursor.execute('SELECT id FROM chromosomes WHERE species=%s AND chromosome=%s', (species2, chromosome2))
		row = self.cursor.fetchone()
		if row is None:
			raise ValueError, 'chromosome %s of species %s is missing' % (chromosome2, species2)
		chromosome2_id = row[0]
		
		self.cursor.execute('INSERT INTO chromosome_pairs (chromosome1_id, chromosome2_id) VALUES(%s, %s)', (chromosome1_id, chromosome2_id))
		chromosome_pair_id = self.cursor.lastrowid
		
		if self.partitions:
			self.cursor.execute('ALTER TABLE segments ADD PARTITION (PARTITION partition%d VALUES IN (%d))' % (chromosome_pair_id, chromosome_pair_id))
		
		return chromosome_pair_id
	
	def insert_region(self, chromosome_id, start, stop, type, subtype):
		type_id = self.get_type_id(type, subtype)
		self.cursor.execute('INSERT INTO regions (chromosome_id, start, stop, type_id) VALUES(%s, %s, %s, %s)', (chromosome_id, start, stop, type_id))
	
	def insert_segment(self, chromosome_pair_id, x_start, x_stop, y_start, y_stop, type, subtype):
		type_id = self.get_type_id(type, subtype)
		self.cursor.execute('INSERT INTO segments (chromosome_pair_id, x_start, x_stop, y_start, y_stop, type_id) VALUES(%s, %s, %s, %s, %s, %s)', (chromosome_pair_id, x_start, x_stop, y_start, y_stop, type_id))
	
	def get_region_by_range(self, species, chromosome, start, stop):
		query = '''
			SELECT r.start, r.stop, t.id
				FROM regions r
				JOIN (chromosomes c, types t) ON (r.chromosome_id = c.id AND r.type_id = t.id)
				WHERE
					c.species = %s AND
					c.chromosome = %s AND
					r.start <= %s AND
					r.stop >= %s
				ORDER BY t.id
		'''
		self.cursor.execute(query, (species, chromosome, stop, start))
		if self.max_rownum and self.cursor.rowcount > self.max_rownum:
			raise RuntimeError, 'too many results'
		
		return self.build_type_dict()
	
	def get_segment_by_box(self, x_species, x_chromosome, y_species, y_chromosome, x_start, x_stop, y_start, y_stop):
		query = '''
			SELECT s.x_start, s.x_stop, s.y_start, s.y_stop, t.id
				FROM segments s
				JOIN (chromosome_pairs p, chromosomes c1, chromosomes c2, types t) ON (
					s.chromosome_pair_id = p.id AND
					p.chromosome1_id = c1.id AND
					p.chromosome2_id = c2.id AND
					s.type_id = t.id
				)
				WHERE
					c1.species = %s AND
					c1.chromosome = %s AND
					c2.species = %s AND
					c2.chromosome = %s AND
					s.x_start <= %s AND
					s.x_stop >= %s AND
					s.y_start <= %s AND
					s.y_stop >= %s
				ORDER BY t.id
		'''
		self.cursor.execute(query, (x_species, x_chromosome, y_species, y_chromosome, x_stop, x_start, y_stop, y_start))
		if self.max_rownum and self.cursor.rowcount > self.max_rownum:
			raise RuntimeError, 'too many results'
		
		return self.build_type_dict()
	
	##
	## Internal use only
	##
	def get_type_id(self, type, subtype):
		try:
			return self.type_cache.get_by_name(type, subtype)
		except KeyError:
			type_id = self.insert_type(type, subtype)
			self.type_cache.update(type_id, type, subtype)
			return type_id
	
	def insert_type(self, type, subtype):
		self.cursor.execute('INSERT INTO types (type, subtype) VALUES(%s, %s)', (type, subtype))
		return self.cursor.lastrowid
	
	def build_type_dict(self):
		type_dict = {}
		last_type_id = -1
		type_list = None
		while True:
			row = self.cursor.fetchone()
			if row is None:
				break
			else:
				type_id = row[-1]
				if type_id != last_type_id:
					last_type_id = type_id
					type_list = self.get_type_list(type_dict, type_id)
				
				type_list.append( tuple(row[:-1]) )
		
		return type_dict
	
	def get_type_list(self, type_dict, type_id):
		type, subtype = self.type_cache.get_by_id(type_id)
		
		try:
			subtype_dict = type_dict[type]
		except KeyError:
			subtype_dict = {}
			type_dict[type] = subtype_dict
		
		try:
			type_list = subtype_dict[subtype]
		except KeyError:
			type_list = []
			subtype_dict[subtype] = type_list

		return type_list
