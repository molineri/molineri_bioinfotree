include $(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

ENS_VERSION ?= 49
VEGA_MART ?= vega_mart_$(ENS_VERSION)
ENS_MART ?= ensembl_mart_$(ENS_VERSION)

MYSQL_MART ?= mysql -u anonymous -h martdb.ensembl.org -P 5316 -BCAN
MYSQL_CORE ?= mysql -u anonymous -h ensembldb.ensembl.org -P 5306 -BCAN
ENS_MART = ensembl_mart_$(ENS_VERSION)

all: hsapiens2mmusculus-homolog.map

%-homolog.map:
	species="$*"; \
	if [[ $$species =~ ([a-z]*)2([a-z]*) ]]; then \
		spec1=$${BASH_REMATCH[1]};\
		spec2=$${BASH_REMATCH[2]};\
		if [[ $$spec1 != $$spec2 ]]; then \
			echo "selecting hortologous between $$spec1 and $$spec2";\
			$(call HORTOLOGOUS) > $@; \
		else \
			echo "selecting paralogous for $$spec1";\
			$(call PARALOGOUS) > $@; \
		fi; \
	else \
		echo "ERROR: malformed target name ($@). Correct example: hsapiens2mmusculus-homolog.map)";\
	fi;\

PARALOGOUS = echo "SELECT DISTINCT \
			gene_stable_id, \
			homol_stable_id, \
			subtype, \
			peptide_perc_pos, \
			peptide_perc_id, \
			homol_peptide_perc_pos, \
			homol_peptide_perc_id, \
			biotype \
		FROM $${spec1}_gene_ensembl__paralogs_$${spec2}__dm" \
	| $(MYSQL_MART) $(ENS_MART) \
	| awk -F "\t" 'BEGIN{OFS="\t"}{if($$1>$$2){print $$0;}else{print $$2,$$1,$$3,$$4,$$5,$$6,$$7,$$8}}' \
	| perl -lane '$$k=$$F[0]."_".$$F[1]; print if not defined($$a{$$k}); $$a{$$k}++'

HORTOLOGOUS = echo "SELECT DISTINCT \
			gene_stable_id,\
			homol_stable_id,\
			description \
		FROM\
			$${spec1}_gene_ensembl__homologs_$${spec2}__dm\
		WHERE \
			gene_stable_id is not NULL \
			and homol_stable_id is not NULL \
			and description is not NULL" \
	| $(MYSQL_MART) $(ENS_MART)
