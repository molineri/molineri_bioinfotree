NCBI_ANNOT_DIR = $(BIOINFO_ROOT)/task/annotations/dataset/ncbi/genes/hsapiens/$(NCBI_ANNOT_DIR_VERSION)
URL_PREFIX=http://www.drugbank.ca/system/downloads/current/
DB_URL=$(URL_PREFIX)/drugbank.txt.zip
ENS_DIR=$(BIOINFO_ROOT)/task/annotations/dataset/ensembl/hsapiens/$(ENS_VERSION)

drugbank.txt.zip:
	wget $(DB_URL)

drugbank.xml.zip:
	wget $(URL_PREFIX)/$@

$(addsuffix _links.csv.zip, target enzyme carriers transporters drug): %_links.csv.zip: 
	wget -O $@ http://www.drugbank.ca/system/downloads/current/$@

%_links.gz: %_links.csv.zip
	unzip $<
	csv2tab < $*_links.csv | unhead | gzip > $@

.META: target_links.gz
	1	ID
	2	Name
	3	GeneName
	4	GenBankProteinID
	5	GenBankGeneID
	6	UniProtID
	7	UniprotTitle
	8	PDB_ID
	9	GeneCardID
	10	GenAtlasID
	11	HGNC_ID
	12	HPRD_ID

ensembl_uniprot.map.gz: $(ENS_DIR)/translation-xref.map.gz $(ENS_DIR)/translation-gene.map.gz $(ENS_DIR)/gene-xref.map.gz
	bawk '$$2="Uniprot/SWISSPROT"' $< | translate -a -d -j <(zcat $^2) 1 | translate -a -d -j <(bawk '$$2=="EntrezGene"' $^3) 2 \
	| cut -f -2,4-6,8- \
	| gzip > $@

target_links.xref_map.gz: target_links.gz ensembl_uniprot.map
	zcat $< | translate -a -d -j -e NA -f 6 <(zcat ensembl_uniprot.map) 6



drugbank.gz: drugbank.txt.zip
	unzip $<
	cat drugbank.txt | gzip > $@
	rm drugbank.txt

drugbank.xml.gz: drugbank.xml.zip
	unzip drugbank.xml.zip
	gzip drugbank.xml
	
target_db.gz: drugbank.gz
	zcat $< \
	| perl -ne 'chomp; next if m/^\s*$$/;\\
		print ">$$1\t" if m/#BEGIN_DRUGCARD\s+(.*)$$/;\\
		if(m/#\s+Brand_Names:/){$$_=<>; chomp; print "$$_\t"};\\
		if(m/#\s+Description:/){$$_=<>; s/\t/ /g; print };\\
		if(m/#\sDrug_Target_(\d+)_(.*):$$/){\\
			$$2=~s/\s/_/g;\\
			while(<>){\\
				chomp;\\ 
				last if m/^\s*$$/;\\
				print "$$1\t$$2\t$$_\n"\\
			}\\
		}' \
	| sed 's/Not Available/NA/g' \
	| gzip >$@

id2generic_name.gz: drugbank.gz
	zcat $< \
	| perl -ne 'chomp; print "$$1\t" if m/#BEGIN_DRUGCARD\s+(.*)$$/; if(m/#\s*Generic_Name:\s*$$/){$$_=<>; print uc}'\
	| gzip >$@

id2InChIKey.gz: drugbank.gz
	zcat $< \
	| perl -ne 'chomp; print "$$1\t" if m/#BEGIN_DRUGCARD\s+(.*)$$/; if(m/#\s*InChI_Key:\s*$$/){$$_=<>; s/^InChIKey=//; s/Not\sAvailable/NA/; print}'\
	| gzip >$@

id2chembl.gz: id2InChIKey.gz
	zcat $< | chembl_compound_by_InChiKeys $@.partial;            * questa regola puo` essere richiamata piu` volte se fallisce, i dati gia` scaricati restano in $@.partial e non piu` scaricati al secondo tentativo*
	gzip < $@.partial > $@

.META: id2chembl.gz
	1	DrugbankId
	2	InChI_Key
	3	chemblId
	4	numRo5Violations
	5	molecularWeight
	6	preferredCompoundName
	7	alogp
	8	knownDrug
	9	medChemFriendly
	10	rotatableBonds
	11	passesRuleOfThree
	12	molecularFormula
	13	smiles
	14	species
	15	acdAcidicPka
	16	acdBasicPka
	17	acdLogp
	18	acdLogd

disease.targhet_HPRDandHGNC.map.gz: target_db.gz
	zcat $< | fasta2tab \
	| bawk '{print $$1,$$2,$$4,$$5,$$6,$$3}' \
	| bawk '\
		$$5!="NA" && $$4=="HPRD_ID" {print $$1~3, "HPRD:" $$5,$$6 }\
		$$5!="NA" && $$4=="HGNC_ID" {print $$1~3,$$5,$$6}'\
	| gzip > $@

target2SwissProt_ID.map.gz target2GenBank_ID_Protein.map.gz target2HPRD_ID.map.gz target2HGNC_ID.map.gz: target2%.map.gz: target_db.gz
	zcat $<\
	| fasta2tab | bawk '$$5=="$*" && $$6!="NA" {print $$1 ";" $$4,$$6}' \
	| perl -lnpe 's/\t[^:]+:/\t/' \
	| gzip > $@

target2entrez.map.gz: target2GenBank_ID_Protein.map.gz target2HPRD_ID.map.gz target2HGNC_ID.map.gz $(NCBI_ANNOT_DIR)/gene2accession.gz $(NCBI_ANNOT_DIR)/entrez-HPRD.map.gz $(NCBI_ANNOT_DIR)/entrez-HGNC.map.gz
	( zcat $<  | translate -d -a -j  -k    <(bawk '{print $$protein_gi, $$gene_id}' $^4) 2 | append_each_row $< ;\
  	  zcat $^2 | translate -d -a -j  -k -n <(zcat $^5) 2 | append_each_row $^2;\
	  zcat $^3 | translate -d -a -j  -k -n <(zcat $^6) 2 | append_each_row $^3;\
	) \ 
	| cut -f 1,3 \
	| sort | uniq \
	| gzip > $@

.META: target2entrez.map.gz
	1	targetid	DR00001;1  drug_id;targhet_ordinal
	2	entrez_gene_id

drug2status.gz: drugbank.xml.gz
	zcat $< | perl -lne 'print ">$$1" if m/<drugbank-id>([^<]+)/; print $$1 if m/<group>([^<]+)/' | fasta2tab | gzip >$@
	
target2entrez.approved.map.gz: target2entrez.map.gz drug2status.gz
	zcat $< | tr ";" "\t" \
	| filter_1col 1 <(bawk '$$2=="approved" {print $$1}' $^2) \
	| bawk '{print $$1 ";" $$2,$$3}' \
	| gzip > $@

actions.gz: drugbank.xml.gz
	zcat drugbank.xml.gz \
	| perl -ne 'chomp;\\
			print ">$$1\n" if m|<drugbank-id>(.*)</drugbank-id>|;\\
			if(m@\s+<(target|enzyme|transporter) [^>]*partner="(\d+)"[^>]*>@){$$t=$$1; $$p=$$2 }\\
			print "$$p\t$$t\t$$1\n" if m|\s+<action>([^<]+)</action>|;\\
			print "$$p\t$$t\tNA\n" if m|<actions/>|;\\
		' \
	| fasta2tab | gzip > $@

.META: actions.gz
	1	DrugBankID	DB00001
	2	MoleculeID	54	as in http://www.drugbank.ca/molecules/54
	3	PathnerType	target
	4	InteractionType	inhibitor

target_actions.gz: target_db.gz actions.gz target2entrez.map.gz
	zcat $< | perl -pe 's/^>([^\s]+).*/>$$1/' | fasta2tab \
	| bawk '$$3=="ID" {print $$1";"$$2,$$1";"$$4}'\
	| translate -a -d -j <(bawk '{print $$1 ";" $$2,$$4}' $^2) 2 \
	| translate -a -d -j -v -e NULL <(zcat $^3) 1\
	| tr ";" "\t" \
	| cut -f -3,5- \
	| gzip > $@

.META: target_actions.gz
	1	drug_id
	2	target_ordinal
	3	entrez_gene_id
	4	target_internal_id
	5	action

