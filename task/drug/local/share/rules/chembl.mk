target2compounds.gz:
	chembl_download_target2compounds | gzip > $@

.META: target2compounds.gz
	1	reference
	2	bioactivity_type
	3	ingredient_cmpd_chemblid
	4	value
	5	units
	6	assay_chemblid
	7	parent_cmpd_chemblid
	8	operator
	9	activity_comment
	10	name_in_reference
	11	assay_description
	12	organism
	13	assay_type
	14	target_confidence


.DOC: target2compounds.gz
	is a fasta file, the header contains:
	1	chemblId
	2	description
	3	geneNames
	4	organism
	5	preferredName
	6	proteinAccession
	7	synonyms
	8	targetType

.PHONY: target2compounds.statistics
target2compounds.statistics: target2compounds.gz
	zgrep -v '^>' target2compounds.gz | grep -v '^ERRO' | grep 'Homo sapiens' | tee >(cut -f 9 | symbol_count > target2compounds.gz.activity_comment) >(cut -f 2,4,5,8 | symbol_count > target2compounds.gz.bioactivity_quantification) >(cut -f 12 | symbol_count > target2compounds.gz.organism) >(cut -f 14 | symbol_count > target2compounds.gz.target_confidence) | cut -f 13 | symbol_count > target2compounds.gz.assay_type

card/%.gz:
	wget -O >(gzip > cards/@.gz) https://www.ebi.ac.uk/chembldb/index.php/compound/inspect/@

.PHONY: cards_all

cards_all: target2compounds.gz
	zgrep -v '^>' $<\
	| cut -f 3 | sort | uniq\
	| xargs -I @ bmake cards/@.gz
