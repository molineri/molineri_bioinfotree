#!/bin/bash
wget -q -O - www.drugbank.ca/drugs/$1 | grep -A3 '/molecules/.*as=target' | perl -lne 'next if m/^--/; next if m/^\s*$/; next if m/Pharmacological action:/; if(s/\s+<p><strong//){s/\..*//}; s|</?strong>||g; s|<br>||; s/^\s+//; print'
