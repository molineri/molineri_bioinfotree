#! /usr/bin/env python

import urllib2
import json
import re
from sys import stdin, stderr, argv

######

def looks_like_number(x):
    try:
        float(x)
        return True
    except ValueError:
        return False

def GET(url):
	try:
		return json.loads(urllib2.urlopen(url).read())
	except urllib2.HTTPError:
		print >>stderr, "GET FAILED at url %s" % url
		raise

########################################################################



if len(argv) != 2:
	exit('Unexpected argument number.')


keys=["chemblId","numRo5Violations","molecularWeight","preferredCompoundName","alogp","knownDrug","medChemFriendly","rotatableBonds","passesRuleOfThree","molecularFormula","smiles","species","acdAcidicPka","acdBasicPka","acdLogp","acdLogd"]

prev_file={}
with file(argv[1], 'r') as fd:
	for line in fd:
		tokens = line.rstrip().split('\t')
		#tokens = line.rstrip().split('\t')
		assert len(tokens) == len(keys)+2
		prev_file[tokens.pop(0)]=tokens



with file(argv[1], 'a') as fd:
	for line in stdin:
		(DrugbankId, InChiKey) = line.rstrip().split('\t')
		to_print = [DrugbankId,InChiKey]

		if InChiKey == "NA":
			for i in xrange(0,len(keys)):
				to_print.append("NA")
			print >>fd,"\t".join(to_print)
			continue
		
		if prev_file.get(DrugbankId, None):
			continue
			
		
		try:
			data = GET("http://www.ebi.ac.uk/chemblws/compounds/stdinchikey/%s.json" % InChiKey)
			data= data['compounds']
		except urllib2.HTTPError:
			data = {}
			
		for k in keys:
			try:
				v = data[k]
				if looks_like_number(data[k]):
					v = str(v)
				else:
					v = data[k].encode('ascii', 'xmlcharrefreplace')
					if v.find("\t") > 0:
						raise ValueError("there is a TAB in a field.")
				to_print.append(v)
			except KeyError:
				to_print.append("NA")

		print >>fd,"\t".join(to_print)
