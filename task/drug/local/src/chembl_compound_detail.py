#! /usr/bin/env python

from sys import stdin, stderr
import urllib2
import json

for line in stdin:
	c = line.rstrip()
	url = "http://www.ebi.ac.uk/chemblws/compounds/%s.json" % c
	try:
		c = json.loads(urllib2.urlopen(url).read())
		c=c['compounds']

		to_print = []
		for k in sorted(c.keys()):
			v = str(c[k]).encode('ascii', 'xmlcharrefreplace').replace("\t","#@#@")
			to_print.append("%s: %s" % (k, v))
		print "\t".join(to_print)
	except urllib2.HTTPError:
		print "ERROR opening %s try without the .json extension" % url
	except urllib2.URLError:
		print "ERROR opening %s try without the .json extension" % url
		

