#! /usr/bin/env python

import urllib2
import json
import re

######

def looks_like_number(x):
    try:
        float(x)
        return True
    except ValueError:
        return False

def GET(url):
	try:
		return json.loads(urllib2.urlopen(url).read())
	except urllib2.HTTPError:
		print "GET FAILED at url %s" % url
		raise urllib2.HTTPError

########################################################################

# 1. get all targets

ts = GET("http://www.ebi.ac.uk/chemblws/targets.json")
ts=ts["target"]

target_keys=('chemblId','proteinAccession','organism','targetType','preferredName','geneNames','synonyms','description')
bioactivties_keys=('reference','bioactivity_type','ingredient_cmpd_chemblid','value','units','assay_chemblid','parent_cmpd_chemblid','operator','activity_comment','name_in_reference','assay_description','organism','assay_type','target_confidence')

for t in ts:
	to_print = []
	for k in target_keys:
		to_print.append(str(t[k]).replace("\t"," "))
	print ">" + "\t".join(to_print)

	# 2. Get all bioactivties for target CHEMBL_ID
	try:
		bs = json.loads(urllib2.urlopen("https://www.ebi.ac.uk/chemblws/targets/%s/bioactivities.json" % t['chemblId']).read())
		bs = bs['bioactivities']
	except urllib2.HTTPError:
		print "ERROR opening https://www.ebi.ac.uk/chemblws/targets/%s/bioactivities.json try without the .json extension" % t['chemblId']
		
	
	for b in bs:
		to_print = []
		for k in bioactivties_keys:
			v = b[k].encode('ascii', 'xmlcharrefreplace')
			if v.find("\t") > 0:
				raise ValueError("there is a TAB in a field.")
			to_print.append(v)
		print "\t".join(to_print)
