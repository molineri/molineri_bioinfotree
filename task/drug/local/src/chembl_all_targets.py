#! /usr/bin/env python

import urllib2
import json

def GET(url):
	try:
		return json.loads(urllib2.urlopen(url).read())
	except urllib2.HTTPError:
		print "GET FAILED at url %s" % url
		raise urllib2.HTTPError

ts = GET("http://www.ebi.ac.uk/chemblws/targets.json")
ts=ts["target"]


for t in ts:
	to_print = []
	for k in sorted(t.keys()):
		to_print.append("%s: %s" % (k, str(t[k]).replace("\t"," ")))
	print "\t".join(to_print)
