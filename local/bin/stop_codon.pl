#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

my $usage="$0 [-p] < seq
con -p stampa come prima riga dell'output 
# (probabilita` di NON osservare nussun stop codon in nessun frame di lettura) (numero atteso di stop codon in qualsiasi frame)

OUTPUT: ogni riga riporta la posizione di uno stop codon,
strand, frame, nucleotide di start
";
my $p=0;
GetOptions (
	'probability|p' => \$p,
) or die($usage);

my @stop_codons=("TAG",'TAA',"TGA");

my $seq="";
while(<>){
	chomp;
	$seq.=$_;
}

$seq =~ s/U/T/g;

print "#", ( ( (64 - 3) / 64 ) ** (length($seq)/3) ) * 6, ( ( 3/64 ) * (length($seq)/3) ) * 6 if $p;

for(my $i=0; $i <= length($seq) - 3; $i++){
	my $codon=substr($seq,$i,3);
	for(@stop_codons){
		if($codon eq $_){
			print '+', $i%3 , $i;
		}
	}
}

$seq = reverse $seq;
$seq =~ tr/ACGT/TGCA/;

for(my $i=0; $i <= length($seq) - 3; $i++){
	my $codon=substr($seq,$i,3);
	for(@stop_codons){
		if($codon eq $_){
			print '-', $i%3 , $i;
		}
	}
}
