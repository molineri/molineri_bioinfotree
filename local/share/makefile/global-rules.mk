ifdef BIOINFO_PARALLEL
	SHELL:=$(BIOINFO_ROOT)/binaries/$(BIOINFO_HOST)/local/bin/qbash
else
	SHELL:=$(BIOINFO_ROOT)/local/bin/bash-make
endif

.DELETE_ON_ERROR:
.PHONY: all clean clean.full clean.mkpaths layout

ifdef PHASES
all clean clean.full clean.mkpaths: layout
all clean clean.full clean.mkpaths:
	+wd=$$PWD; \
	for phase in $(PHASES); do \
		if ! cd $$wd/$$phase; then \
			echo "*** Error changing working directory to phase $$phase" >&2; \
			exit 1; \
		fi; \
		if ! $(MAKE:make=bmake) -w  $@; then \
			echo "*** Error running phase $$phase" >&2; \
			exit 1; \
		fi; \
	done

layout: $(PHASES) $(addsuffix /rules.mk,$(PHASES)) $(addsuffix /makefile,$(PHASES))
	set -e; \
	for phase in $(PHASES); do \
		( \
			cd $$phase && \
			$(MAKE:make=bmake) -w -f <(sed -e 's/^include.*//' [Mm]akefile; sed -e '/global-rules\.mk$$/b;s/^include.*//' rules.mk) $@ MAKE_LAYOUT=1 \
		); \
	done

$(PHASES):
	mkdir $@

else
all:
layout:
endif

clean.full: clean

ifndef MAKE_LAYOUT
$(BIOINFO_HOST).mkpaths:
	$(BIOINFO_ROOT)/local/bin/dynpaths_old >$@
clean.mkpaths:
	rm -f *.mkpaths
clean: clean.mkpaths

-include $(BIOINFO_HOST).mkpaths
endif
