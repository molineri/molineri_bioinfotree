#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader



def main():
	usage = format_usage('''
		%prog PARAM1 PARAM2 < STDIN
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-c', '--cutoff', type=float, dest='cutoff', default=0.05, help='some help CUTOFF [default: %default]', metavar='CUTOFF')
	parser.add_option('-p', '--dump-params', dest='params_file', help='some help FILE [default: %default]', metavar='FILE')
	parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='some help [default: %default]')

	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	#for id, sample, raw, norm in Reader(stdin, '0u,1s,2i,3f', False):
	with file(FILENAME, 'r') as fd:
		for line in fd:
			tokens = safe_rstrip(line).split('\t')
			#tokens = line.rstrip().split('\t')
			assert len(tokens) == NUM

if __name__ == '__main__':
	main()

