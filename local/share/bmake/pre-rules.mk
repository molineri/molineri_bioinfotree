# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

LOCAL_SHELL    := $(BIOINFO_ROOT)/local/bin/bash-make
SERIAL_SHELL   := $(BIOINFO_ROOT)/local/bin/bmake-serial
PARALLEL_SHELL := $(BIOINFO_ROOT)/binary/$(BIOINFO_HOST)/local/bin/qbash

define __bmake_mark_serial
$1: SHELL=$(SERIAL_SHELL) $(LOCAL_SHELL) $2
endef

# The shell used for bootstrap
SHELL          := $(LOCAL_SHELL)

# Moved here to avoid unexpected pattern capturing
# See commit 5c8a16c6227ee2032f6598fca8a068941d6f1a38
%.__bmake_clean:
	rm -f '$*'

# Additional functions
reverse = $(if $(1),$(call reverse,$(wordlist 2,$(words $(1)),$(1)))) $(firstword $(1)) # reverse the order of words in a string http://stackoverflow.com/a/786530/849877 #
# Usage:
# foo= please reverse me 
# all:
# 	@echo $(call reverse,$(foo))
