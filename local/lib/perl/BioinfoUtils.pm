#package BioinfoUtils;
#our @EXPORT = qw(cmd_exec cmd_exec_nofail cmd_exec_system);
use warnings;
use strict;

sub cmd_exec
{
	my $cmd = shift;
	`set -o pipefail; $cmd`;
	die "FAILED: $cmd\n$!" if $?;
	return $?;
}

sub cmd_exec_nofail
{
	my $cmd = shift;
	`set -o pipefail; $cmd`;
	return $?;
}

sub cmd_exec_system
{
	my $cmd = shift;
	system("set -o pipefail; $cmd");
	return $?;
}


1
