import AbstractMeasure

class Measure(AbstractMeasure.Measure):
    """One vs. all multiclass accuracy performance measure"""
    
    
    def multiOutputPerformance(self, Y, Y_predicted, verbose=False):
        """Calculates performance for multiple outputs, returns the result as one number.
        Useful for model selection.
        
        @param Y: matrix of correct labels, each row corresponds to one example, each column
        to one possible task
        @type Y: numpy matrix
        @param Y_predicted: matrix of predicted labels, each row corresponds to one example,
        each column to one possible task
        @type Y_predicted: numpy matrix
        @return: multiclass performance
        @rtype: float
        """
        assert Y.shape == Y_predicted.shape
        correct = 0
        for i in range(Y.shape[0]):
            largest_pred = None
            predicted = None
            true = None
            for j in range(Y.shape[1]):
                if Y[i,j] == 1:
                    true = j
                if (not largest_pred) or  (Y_predicted[i,j]>largest_pred):
                    largest_pred = Y_predicted[i,j]
                    predicted = j
            if true == predicted:
                correct += 1
        perf = float(correct)/float(Y.shape[0])
        return perf

    def checkOutputs(self, Y):
        """Checks that the outputs consist of only values 1 (positive class) and -1 (negative class).
        and that each instance belongs to one and only one class
        
        @param Y: matrix correct labels, each column in a list corresponds to one possible class
        @type Y: numpy matrix
        """   
        for i in range(Y.shape[0]):
            classes = 0
            for j in range(Y.shape[1]):
                if Y[i, j] not in [1., -1.]:
                    print "Error: MultiAccuracy measure supports only two types of outputs, 1 (positive) and -1 (negative). Found %f" % Y[i, 0]
                    sys.exit(-1)
                elif Y[i,j] == 1:
                    classes += 1
            if classes == 0:
                raise Exception("Error in labels: instance number %d has no class" %(i+1))
            elif classes>1:
                raise Exception("Error in labels: instance number %d has more than one classes" %(i+1))


    def getName(self):
        return "Multiclass accuracy"
