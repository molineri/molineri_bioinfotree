
from numpy import multiply, mean

import AbstractSingleOutputMeasure

class Measure(AbstractSingleOutputMeasure.Measure):
    """Squared error"""

    def __init__(self):
        self.name = "squared error"

    def getPerformance(self, correct, predictions):
        diff = correct - predictions
        sqerror = (diff.T * diff)[0, 0]
        sqerror /= correct.shape[0]
#        assert len(correct) == len(predictions)
#        sqerror = 0.
#        for i in range(len(correct)):
#            sqerror += (correct[i] - predictions[i]) ** 2
#        sqerror /= len(correct)
        return sqerror


    def multiTaskPerformance(self, Y, Y_predicted, verbose=False):  
        """Estimates the performance for several tasks in parallel.
        
        @param Y: correct labels, one column per task
        @type Y: numpy.matrix
        @param Y_predicted: predicted labels, one column per task
        @type Y_predicted: numpy.matrix
        @param verbose: verbosity (default False)
        @type verbose: boolean
        @return: performances
        @rtype: list of floats
        """
        sqerror = Y - Y_predicted
        multiply(sqerror, sqerror, sqerror)
        performances = mean(sqerror, axis = 0)
        return performances
    
    
    def aggregate(self, performances):
        return mean(performances)
    
    
    def comparePerformances(self, perf1, perf2):
        """Given two performance values returns 1 if the first implies better performance,
        zero if the performances are tied, and -1 if the second one is better
        
        @param perf1: performance
        @type perf1: float
        @param perf2: performance
        @type perf2: float
        @return: 1, 0 or -1
        @rtype: integer"""
        if perf1 < perf2:
            return 1
        elif perf1 > perf2:
            return - 1
        else:
            return 0
    
    
    def isErrorMeasure(self):
        return True


    def getName(self):
        return "squared error"
