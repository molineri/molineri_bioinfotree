import AbstractPWMeasure
from AbstractMeasure import UndefinedPerformance
import operator

class Measure(AbstractPWMeasure.Measure):
    """Area Under ROC curve (AUC) performance measure, the outputs are expected to be 1 / -1"""


    def tieCorrection(self, pairs):
        #Fast AUC calculation formula must be corrected in case there are tied predictions
        #A bit cumbersome fix for an issue that practically never arises when using a
        #learner that produces real-valued predictions.
        correction = 0
        all_ties = []
        tie = []
        for i in range(0,len(pairs)):
            if  (i<len(pairs)-1) and (pairs[i][0]==pairs[i+1][0]):
                tie.append(pairs[i])
            elif (i>0) and (pairs[i][0] == pairs[i-1][0]):
                tie.append(pairs[i])
                all_ties.append(tie)
                tie = []
        for tie in all_ties:
            pos = 0
            neg = 0
            #How many incorrectly ranked pairs the uncorrected formula estimated
            calc_incorrect = 0
            count = 0
            for pair in tie:
                if pair[1] == 1:
                    count += 1
                    pos += 1
                else:
                    calc_incorrect += count
                    neg += 1
            #each tie should produce 0.5 errors
            true_incorrect = pos*neg*0.5
            #fix
            correction += true_incorrect-calc_incorrect
        return correction
                        
        
    

    def fastAUC(self, correct, predictions):
        #The n(log(n)) implementation
        assert len(correct) == len(predictions)
        pos = 0
        neg = 0
        pairs = []
        for i in range(len(correct)):
            pairs.append((predictions[i],correct[i]))
            assert correct[i] in [-1,1]
            if correct[i] == 1:
                pos += 1
            elif correct[i] == -1:
                neg += 1
        if pos == 0 or neg == 0:
            raise UndefinedPerformance("AUC Undefined: All examples belong to the same class")
        pairs.sort(key=operator.itemgetter(0))
        incorrect = 0
        p = 0
        for pair in pairs:
            if pair[1] == -1:
                incorrect += p
            else:
                p += 1
        incorrect += self.tieCorrection(pairs)
        AUC = 1.-(float(incorrect)/float(pos*neg))
        return AUC

    def getPerformance(self, correct, predictions):
        #the new implementation has n(log(n)) time complexity
        return self.fastAUC(correct, predictions)
#        #For reference, the old O(n^2) time implementation, based more directly
#        #on Wilcoxon-Mann-Whitney
#        assert len(correct) == len(predictions)
#        pos, neg = 0., 0.
#        posindices = []
#        negindices = []
#        for i in range(len(correct)):
#            if correct[i] > 0:
#                pos += 1.
#                posindices.append(i)
#            else:
#                neg += 1
#                negindices.append(i)
#        if pos == 0 or neg == 0:
#            raise UndefinedPerformance("AUC Undefined: All examples belong to the same class")
#        else:
#            auc = 0.
#            for i in posindices:
#                for j in negindices:
#                    if predictions[i] > predictions[j]:
#                        auc += 1.
#                    elif predictions[i] == predictions[j]:
#                        auc += 0.5
#            auc /= pos * neg
#        return auc
        

    def pairwisePerformance(self, pairs, Y, index, predicted):
        """Used for LPO-cross-validation. Supplied pairs should consist of all positive-negative
        pairs.
        
        @param pairs: a list of tuples of length two, containing the indices of the pairs in Y
        @type pairs: list of integer pairs
        @param Y: matrix of correct labels, each column corresponds to one task
        @type Y: numpy matrix
        @param index: the index of the task considered, this corresponding to a given column of Y
        @type index: integer
        @param predicted: a list of tuples of length two, containing the predictions for the pairs
        @type predicted: list of float pairs
        @return: performance
        @rtype: float"""
        assert len(pairs) == len(predicted)
        if len(pairs) == 0:
            return None
        auc = 0.
        for pair in predicted:
            if pair[0] > pair[1]:
                auc += 1.
            elif pair[0] == pair[1]:
                auc += 0.5
        auc /= len(predicted)
        return auc

    def getPairs(self, Y, index):
        """Returns all positive-negative pairs.
        
        @param Y: matrix of correct labels, each column corresponds to one task
        @type Y: numpy matrix
        @return: list of lists of index pairs
        @rtype list of lists of integer pairs"""
        pairs = []
        tsetsize = Y.shape[0]
        for i in range(tsetsize - 1):
            for j in range(i + 1, tsetsize):
                if Y[i, index] > Y[j, index]:
                    pairs.append((i, j))
                elif Y[i, index] < Y[j, index]:
                    pairs.append((j, i))
        return pairs

    def checkOutputs(self, Y):
        """Checks that the outputs consist of only values 1 (positive class) and -1 (negative class).
        Terminates the program if such is not the case
        
        @param Y: matrix correct labels, each column in a list corresponds to one task
        @type Y: numpy matrix
        """
        for i in range(Y.shape[0]):
            for j in range(Y.shape[1]):
                if Y[i, j] != 1. and Y[i, j] != -1.:
                    raise Exception("AUC measure supports only two types of labels, 1 (positive) and -1 (negative). Found %f" % Y[i, j])
                    
    
    def getName(self):
        return "AUC"
        

