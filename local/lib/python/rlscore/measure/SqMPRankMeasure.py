import AbstractPWMeasure
from AbstractMeasure import UndefinedPerformance

from numpy import *

class Measure(AbstractPWMeasure.Measure):
    """Squared magnitude preserving ranking error"""

    def __init__(self):
        self.pairs = None

    def getPerformance(self, correct, predictions):
        vlen = correct.shape[0]
        if vlen == 1:
            raise UndefinedPerformance("No pairs, only one label")
        diff = correct - predictions
        onevec = mat(ones((vlen, 1)))
        centereddiff = vlen * diff - onevec * (onevec.T * diff)
        sqerror = (centereddiff.T * diff)[0, 0] / ((len(correct) ** 2 - len(correct)) / 2)
#        assert len(correct) == len(predictions)
#        if len(correct) == 1:
#            raise UndefinedPerformance("No pairs, only one label")
#        else:
#            sqerror = 0.
#            for i in range(len(correct)):
#                for j in range(i + 1, len(correct)):
#                    sqerror += ((correct[i] - correct[j]) - (predictions[i] - predictions[j])) ** 2
#            sqerror /= (len(correct) ** 2 - len(correct)) / 2
        return sqerror
                               
    def pairwisePerformance(self, pairs, Y, index, predicted):
        """Used for LPO-cross-validation.
        
        @param pairs: a list of tuples of length two, containing the indices of the pairs in Y
        @type pairs: list of integer pairs
        @param Y: matrix of correct labels, each column corresponds to one task
        @type Y: numpy matrix
        @param index: the index of the task considered, this corresponding to a given column of Y
        @type index: integer
        @param predicted: a list of tuples of length two, containing the predictions for the pairs
        @type predicted: list of float pairs
        @return: performance
        @rtype: float"""
        assert len(pairs) == len(predicted)
        if len(pairs) == 0:
            return None
        sqerror = 0.
        for i_j, p1_p2 in zip(pairs, predicted):
            sqerror += ((Y[i_j[0], index] - Y[i_j[1], index]) - (p1_p2[0] - p1_p2[1])) ** 2
        sqerror /= len(pairs)
        return sqerror

    def comparePerformances(self, perf1, perf2):
        """Given two performance values returns 1 if the first implies better performance,
        zero if the performances are tied, and -1 if the second one is better
        Default behaviour assumes that the bigger the value, the better the performance.
        
        @param perf1: performance
        @type perf1: float
        @param perf2: performance
        @type perf2: float
        @return: 1, 0 or -1
        @rtype: integer"""
        if perf1 < perf2:
            return 1
        elif perf1 > perf2:
            return - 1
        else:
            return 0


    def getName(self):
        return "squared magnitude preserving ranking error"
