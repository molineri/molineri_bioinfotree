import AbstractSingleOutputMeasure

from numpy import *

class Measure(AbstractSingleOutputMeasure.Measure):
    """Accuracy performance measure, the outputs are expected to be 1 / -1"""


    def getPerformance(self, correct, predictions):
        """"Returns the performance given correct and predicted values.
        We assume two classes, 1 and -1. Accuracy is the fraction of correct decisions
        
        @param correct: correct output values
        @type correct: numpy matrix of floats
        @param predictions: predicted output values
        @type predictions: numpy matrix of floats"""
        assert correct.shape[0] == predictions.shape[0]
        vlen = float(correct.shape[0])
        accuracy = sum(sign(multiply(correct, predictions)) + 1.) / (2 * vlen)
#        assert len(correct) == len(predictions)
#        cor = 0
#        for c, p in zip(correct, predictions):
#            assert c in [-1., 1]
#            if c * p > 0.:
#                cor += 1.
#            elif c * p == 0.:
#                cor += 0.5
#        accuracy = float(cor) / float(len(correct))
        return accuracy


    def multiTaskPerformance(self, Y, Y_predicted, verbose=False):  
        """Estimates the performance for several tasks in parallel.
        
        @param Y: correct labels, one column per task
        @type Y: numpy.matrix
        @param Y_predicted: predicted labels, one column per task
        @type Y_predicted: numpy.matrix
        @param verbose: verbosity (default False)
        @type verbose: boolean
        @return: performances
        @rtype: list of floats
        """
        vlen = float(Y.shape[0])
        performances = sum(sign(multiply(Y, Y_predicted)) + 1., axis = 0) / (2 * vlen)
        return performances
    
    
    def aggregate(self, performances):
        return mean(performances)
    
    def checkOutputs(self, Y):
        """Checks that the outputs consist of only values 1 (positive class) and -1 (negative class).
        Terminates the program if such is not the case
        
        @param Y: matrix correct labels, each column in a list corresponds to one task
        @type Y: numpy matrix
        """
        for i in range(Y.shape[0]):
            for j in range(Y.shape[1]):
                if Y[i, j] not in [1., -1.]:
                    raise Exception("Error: Accuracy measure supports only two types of outputs, 1 (positive) and -1 (negative). Found %f" % Y[i, 0])


    def getName(self):
        return "accuracy"
