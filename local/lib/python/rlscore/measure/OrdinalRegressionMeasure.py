import AbstractPWMeasure
from AbstractMeasure import UndefinedPerformance
import operator
import DisagreementMeasure

class Measure(AbstractPWMeasure.Measure):
    """Generalization of AUC for ordinal regression, implementation not yet fully functional, the outputs are expected to be
    0 ... n-1 where n is the number of ordered classes"""

    def __init__(self):
        #number of distinct classes
        #the used labels should be 0 ... n-1, for n classes
        self.classes = None

    def setParameters(self, parameters):
        """Sets the parameter values of the performance measure.
        
        @param parameters: parameters for the kernel
        @type parameters: dictionary of key:value pairs"""
        if "classcount" in parameters:
            self.classes = int(parameters["classcount"])
        else:
            raise Exception("Did not supply the 'classcount' variable to the OrdinalRegressionMeasure")

    def getPerformance(self, correct, predictions):
        """"Returns the performance given correct and predicted values.
        
        @param correct: correct output values
        @type correct: list of floats
        @param predictions: predicted output values
        @type predictions: list of floats"""
        #this implementation has n(log(n)) time complexity
        #known issues: doesn't handle the case where predictions
        #are tied well
        assert len(correct) == len(predictions)
        pos = 0
        neg = 0
        #class_members = [0 for x in range(self.classes)]
        allowed = range(0, self.classes)
        pairs = []
        for i in range(len(correct)):
            pairs.append((predictions[i],correct[i]))
            assert correct[i] in allowed
            #class_members[correct[i]] += 1
        pairs.sort(key=operator.itemgetter(0))
        incorrect = 0
        encountered = [0 for x in range(self.classes)]
        for pair in pairs:
            if pair[1] < self.classes-1:
                incorrect += sum(encountered[int(pair[1])+1:])
            encountered[int(pair[1])] += 1
        pairs = 0
        for i in range(self.classes-1):
            for j in range(i+1, self.classes):
                pairs += encountered[i]*encountered[j]
        if pairs == 0:
            raise UndefinedPerformance("No pairs, all the instances have the same output")
        perf = 1.-(float(incorrect)/float(pairs))
        return perf
    
    def getName(self):
        return "Pairwise ranking accuracy"