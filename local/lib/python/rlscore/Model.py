from scipy import sparse
from kernel import LinearKernel
import DataSources

class ModelFactory(object):
    """Builds dual or primal models from the resource pool, based on available resources."""
    
    def __init__(self, rpool):
        """Initializes the model factory with the resource pool
        @param rpool: resource pool
        @type rpool: dict
        """
        self.rpool = rpool
        
    def buildModel(self, coeff, kernel):
        """Builds a dual or primal model from the resource pool. For the linear
        kernel a primal model is constructed, for all other kernels a dual one.
        @param coeff: the coefficients returned by the learner
        @type coeff: numpy matrix
        @param kernel: the kernel used for learning, initialized with the training data
        @type kernel: a subclass of AbstractKernel.Kernel"""
        #First transform reduced set solution if such available
        if self.rpool.has_key(DataSources.KMATRIX_VARIABLE):
            if isinstance(kernel, LinearKernel.Kernel):
                fs = self.rpool[DataSources.TRAIN_FEATURES_VARIABLE]
                X = kernel.getPrimalDataMatrix(fs)
                W = X*coeff
                model = PrimalModel(W, kernel)
            else:
                kernel.predictionCacheFromRpool(self.rpool)
                model = DualModel(coeff, kernel)
        elif self.rpool.has_key(DataSources.DECOMPOSITION_VARIABLE):
            dc = self.rpool[DataSources.DECOMPOSITION_VARIABLE]
            Z = dc.readZMatrix()
            if Z != None:
                #Maybe we could somehow guarantee that Z is always coupled with bvectors?
                if not self.rpool.has_key(DataSources.BASIS_VECTORS_VARIABLE):
                    raise Exception("Provided decomposition of the reduced set approximation of kernel matrix, but not the indices of the basis vectors")
                coeff = Z * coeff
            #Linear kernels receive special treatments, as using the linear model in the feature space
            #is always more efficient than using the dual model.
            #
            if isinstance(kernel, LinearKernel.Kernel):
                #If the solution is in the primal in the first case
                U = dc.readLeftSingularVectors()
                if Z==None and U != None:
                    model = PrimalModel(coeff, kernel)
                #Else we need to transform a dual model to primal
                else:
                    coeff = sparse.csr_matrix(coeff)
                    fs = self.rpool[DataSources.TRAIN_FEATURES_VARIABLE]
                    X = kernel.getPrimalDataMatrix(fs)
                    #If reduced set approximation was used
                    if Z != None:
                        bvectors = self.rpool[DataSources.BASIS_VECTORS_VARIABLE]
                        X = X[:,bvectors]
    
                    #The hyperplane is a linear combination of the feature vectors of the
                    #basis examples
                    W = X*coeff
                    model = PrimalModel(W, kernel)
            else:
                #The dual model consists of the sum of kernel evaluations between
                #training and test data
                kernel.predictionCacheFromRpool(self.rpool)
                model = DualModel(coeff, kernel)
        else:
            model = PrimalModel(coeff, kernel)
        #The model is put into the resource pool
        return model
        #self.rpool[DataSources.MODEL_VARIABLE] = model

class AbstractModel(object):
    
    def __init__(self, A, kernel):
        raise Exception('AbstractModel is not supposed to be initialized.')
    
    def predict(self, rpool):
        """Makes real-valued predictions for new examples
        @param rpool: resource pool from which test data is read from
        @type rpool: dict
        @return: predictions, one column per task
        @rtype: numpy matrix of floats"""
        pass

class DualModel(AbstractModel):
    """Represents a dual model for making predictions. New predictions are made
    by calculating the linear combination of kernel evaluations between basis
    training examples and test examples."""
    
    def __init__(self, A, kernel):
        """Initializes the dual model
        @param A: dual coefficients, one column per task
        @type A: numpy matrix
        @param kernel: kernel used for learning the model, initialized with training data
        @type kernel: subclass of AbstractKernel.Kernel"""
        #We assume that the kernel has already been initialized with training
        #data and all the relevant parameters
        self.A = A
        self.kernel = kernel
        
    def predict(self, rpool):
        """Makes real-valued predictions for new examples
        @param rpool: resource pool from which test data is read from
        @type rpool: dict
        @return: predictions, one column per task
        @rtype: numpy matrix of floats"""
        K = self.kernel.testKMFromPool(rpool)
        return K.T * self.A
    
class PrimalModel(AbstractModel):
    """Represents a primal (linear) model for making predictions. New predictions are made
    by multiplying the feature vectors of new examples with the learned hyperplane. Compatible
    only with the linear kernel."""
    
    def __init__(self, W, kernel):
        """Initializes a primal model
        @param W: coefficients of the linear model, one column per task
        @type W: numpy matrix
        @param kernel: kernel used for learning the model
        @type kernel: LinearKernel.Kernel"""
        #Assumption: the kernel is an instance of LinearKernel
        if not isinstance(kernel, LinearKernel.Kernel):
            raise Exception("Primal model supports only LinearKernel")
        self.kernel = kernel
        self.W = sparse.csr_matrix(W)
        self.dimensionality = self.W.shape[0]
        #One of the dimensions may correspond to a bias term
        if self.kernel.bias!=0:
            self.dimensionality -= 1
        
    def predict(self, rpool):
        """Makes real-valued predictions for new examples
        @param rpool: resource pool from which test data is read from
        @type rpool: dict
        @return: predictions, one column per task
        @rtype: numpy matrix of floats"""
        fs = rpool[DataSources.PREDICTION_FEATURES_VARIABLE]
        X = self.kernel.getPrimalDataMatrix(fs, dimensionality=self.dimensionality)
        return (X.T * self.W).todense()