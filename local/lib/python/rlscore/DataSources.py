#Sources for wrapping around data objects
from numpy import float64
from scipy.sparse import lil_matrix, vstack, csc_matrix

from measure import AbstractMeasure
import FileReader
import FileWriter

class DataSource(object):
    
    def __init__(self, data):
        self.data = data
    
    def getData(self):
        return self.data

class FeatureSource(DataSource):
    
    def __init__(self, X):
        self.data = csc_matrix(X)
    
    def readFeatures(self, subset=None, dimensionality=None):
        """Reads in features
        
        @param subset: restricts the read instances to consist only of those indexed by subset
        @type subset: list of integers
        @param dimensionality: dimensionality of feature space
        @type dimensionality: integer
        @return data matrix, rows correspond to features and columns to instances
        @rtype scipy sparse matrix"""
        X = self.data
        if subset !=None:
            X = X[:,subset]
        if dimensionality != None:
            if dimensionality < self.data.shape[0]:
                #Row slicing is efficient only for csr_matrix
                X = X.tocsr()[:dimensionality,:]
                X = X.tocsc()
            elif dimensionality > self.data.shape[0]:
                diff = dimensionality-self.data.shape[0]
                X = vstack([X,lil_matrix((diff,X.shape[1]), dtype=float64)])
                X = X.tocsc()
        return X


class FoldSource(DataSource):
    
    def __init__(self, folds):
        
        self.folds = folds
    
    def readFolds(self):
        """Reads in folds
        
        @return folds
        @rtype list of lists of integers"""
        return self.folds


class QidSource(FoldSource):
    
    def __init__(self, qids):
        
        self.qids = qids
        self.mapQids()
    
    def mapQids(self):
        """Maps qids to running numbering starting from zero, and partitions
        the training data indices so that each partition corresponds to one
        query"""
        #Used in FileReader, rls_predict
        qid_dict = {}
        folds = {}
        qid_list = []
        counter = 0
        for index, qid in enumerate(self.qids):
            if not qid in qid_dict:
                qid_dict[qid] = counter
                folds[qid] = []
                counter += 1
            qid_list.append(qid_dict[qid])
            folds[qid].append(index)
        final_folds = []
        for f in folds.values():
            final_folds.append(f)
        self.qids = qid_list
        self.folds = final_folds
    
    def readQids(self):
        """Returns query ids
        
        @return list of identifiers
        @rtype list of strings"""
        
        return self.qids


class DecompositionSource(DataSource):
    
    def __init__(self, svals, rsvecs, lsvecs = None, ZMatrix = None):
        
        self.svals = svals
        self.rsvecs = rsvecs
        self.lsvecs = lsvecs
        self.ZMatrix = ZMatrix
    
    def readSingularValues(self):
        """Reads in singular values
        
        @return: vector of singular values
        @rtype: numpy matrix"""
        
        return self.svals
    
    def readRightSingularVectors(self):
        """Reads in right singular vectors
        
        @return: matrix of singular vectors
        @rtype: numpy matrix"""
        
        return self.rsvecs
    
    def readLeftSingularVectors(self):
        """Reads in left singular vectors
        
        @return: matrix of singular vectors
        @rtype: numpy matrix"""
        
        return self.lsvecs
    
    def readZMatrix(self):
        """Reads in ZMatrix
        
        @return: ZMatrix
        @rtype: numpy matrix"""
        
        return self.ZMatrix


TRAIN_FEATURES_VARIABLE = 'train_features'
TRAIN_LABELS_VARIABLE = 'train_labels'
TRAIN_QIDS_VARIABLE = 'train_qids'
BASIS_VECTORS_VARIABLE = 'basis_vectors'
DECOMPOSITION_VARIABLE = 'train_decomposition'
CVFOLDS_VARIABLE = 'cross-validation_folds'
MODEL_VARIABLE = 'model'
PREDICTION_FEATURES_VARIABLE = 'prediction_features'
PREDICTION_QIDS_VARIABLE = 'test_qids'
PREDICTED_LABELS_VARIABLE = 'predicted_labels'
TEST_LABELS_VARIABLE = 'test_labels'
PREDICTED_CLUSTERS_FOR_TRAINING_DATA_VARIABLE = 'predicted_clusters_for_training_data'
SELECTED_FEATURES_VARIABLE = 'selected_features'
GREEDYRLS_LOO_PERFORMANCES_VARIABLE = 'GreedyRLS_LOO_performances'
PARAMETERS_VARIABLE = 'parameters'
PERFORMANCE_MEASURE_VARIABLE = 'measure'
KMATRIX_VARIABLE = 'kmatrix'
KFUNCTION_VARIABLE = 'kfunction'
REGGRID_RESULTS_VARIABLE = 'regrid_results'
TEST_PERFORMANCE_VARIABLE = 'test_performance'

LABELS_VARIABLE_TYPE = 'labels_variable_type'
BASIS_VECTORS_VARIABLE_TYPE = 'basis_vectors_variable_type'
INT_LIST_VARIABLE_TYPE = 'int_list_variable_type'
FLOAT_LIST_VARIABLE_TYPE = 'float_list_variable_type'


VARIABLE_TYPES = {
                  TRAIN_FEATURES_VARIABLE: FeatureSource,
                  PREDICTION_FEATURES_VARIABLE: FeatureSource,
                  TRAIN_LABELS_VARIABLE: LABELS_VARIABLE_TYPE,
                  TEST_LABELS_VARIABLE: LABELS_VARIABLE_TYPE,
                  PREDICTED_LABELS_VARIABLE: LABELS_VARIABLE_TYPE,
                  PREDICTED_CLUSTERS_FOR_TRAINING_DATA_VARIABLE: INT_LIST_VARIABLE_TYPE,
                  TRAIN_QIDS_VARIABLE: QidSource,
                  PREDICTION_QIDS_VARIABLE: QidSource,
                  CVFOLDS_VARIABLE: FoldSource,
                  MODEL_VARIABLE: 'model',
                  BASIS_VECTORS_VARIABLE: BASIS_VECTORS_VARIABLE_TYPE,
                  DECOMPOSITION_VARIABLE: DecompositionSource,
                  SELECTED_FEATURES_VARIABLE: INT_LIST_VARIABLE_TYPE,
                  GREEDYRLS_LOO_PERFORMANCES_VARIABLE: FLOAT_LIST_VARIABLE_TYPE,
                  PARAMETERS_VARIABLE: dict,
                  PERFORMANCE_MEASURE_VARIABLE: AbstractMeasure.Measure
                  }

DEFAULT_READERS = {
                   FeatureSource: FileReader.FeatureFile,
                   LABELS_VARIABLE_TYPE: FileReader.DenseTextFile,
                   QidSource: FileReader.QidFile,
                   FoldSource: FileReader.FoldFile,
                   'model': FileReader.PickleReader,
                   BASIS_VECTORS_VARIABLE_TYPE: FileReader.BvectorFile,
                   DecompositionSource: FileReader.DecompositionFile
                   }

DEFAULT_WRITERS = {
                   LABELS_VARIABLE_TYPE: FileWriter.DenseTextFile,
                   'model': FileWriter.PickleWriter,
                   DecompositionSource: FileWriter.DecompositionFile,
                   INT_LIST_VARIABLE_TYPE: FileWriter.IndexListFile,
                   FLOAT_LIST_VARIABLE_TYPE: FileWriter.FloatListFile
                   }

