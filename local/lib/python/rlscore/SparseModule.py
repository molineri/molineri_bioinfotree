from numpy.linalg import cholesky
from numpy.linalg import inv
import Decompositions
from numpy.linalg.linalg import LinAlgError
#Module for sparse learning with the empirical feature map

def decomposeSubsetKM(K_r, bvectors):
    """decomposes r*m kernel matrix, where r is the number of basis vectors and m the
    number of training examples
    
    @param K_r: r*m kernel matrix, where only the lines corresponding to basis vectors are present
    @type K_r: numpy matrix
    @param bvectors: the indices of the basis vectors
    @type bvectors: list of integers
    @return svals, evecs, U, C_T_inv
    @rtype tuple of numpy matrices"""
    K_rr = K_r[:, bvectors]
    try:
        C = cholesky(K_rr)
    except LinAlgError:
        #print "Warning: chosen basis vectors not linearly independent"
        #print "Shifting the diagonal of kernel matrix"
        __shiftKmatrix(K_r, bvectors)
        K_rr = K_r[:, bvectors]
        C = cholesky(K_rr)
    C_T_inv = inv(C.T)
    H = (K_r).T * C_T_inv
    svals, evecs, U = Decompositions.decomposeDataMatrix(H.T)
    return svals, evecs, U, C_T_inv

def __shiftKmatrix(K_r, bvectors, shift=0.000000001):
    """Diagonal shift for the basis vector kernel evaluations
    
    @param K_r: r*m kernel matrix, where only the lines corresponding to basis vectors are present
    @type K_r: numpy matrix
    @param bvectors: indices of the basis vectors
    @type bvectors: list of integers
    @param shift: magnitude of the shift (default 0.000000001)
    @type shift: float
    """
    #If the chosen subset is not linearly independent, we
    #enforce this with shifting the kernel matrix
    for i, j in enumerate(bvectors):
        K_r[i, j] += shift

