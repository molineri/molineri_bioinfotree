#Module for writing files
import numpy
import scipy.io
import DataSources
import cPickle



class AbstractWriterOld(object):
        
    def fromRpool(self, rpool, varname, vartype=None):
        """Takes data from the given resource pool into the writer object
        @param rpool: resource pool from which the data is taken
        @type rpool: dict
        @param varname: name of the variable to be saved
        @type varname: string
        @param vartype: class of the variable to be saved
        @type vartype: class
        """
        self.data = rpool[varname].getData()
    
    def write(self, filename):
        """Saves data into a file.
        @param filename: name of the file into which the data is saved
        @type filename: string
        """


class AbstractWriterSimplified(object):
        
    def fromRpool(self, rpool, varname, vartype=None):
        """Takes data from the given resource pool into the writer object
        @param rpool: resource pool from which the data is taken
        @type rpool: dict
        @param varname: name of the variable to be saved
        @type varname: string
        @param vartype: class of the variable to be saved
        @type vartype: class
        """
        self.data = rpool[varname]
    
    def write(self, filename):
        """Saves data into a file.
        @param filename: name of the file into which the data is saved
        @type filename: string
        """


class DecompositionFile(AbstractWriterOld):
    """Saves the singular value decomposition of the data into an npz file"""    
    
    def fromRpool(self, rpool, variable, vartype=None):
        """Saves the singular value decomposition of the data into an npz file
        @param rpool: resource pool from which the data is taken
        @type rpool: dict
        @param varname: name of the variable to be saved
        @type varname: string
        @param vartype: class of the variable to be saved
        @type vartype: class
        """
        dc = rpool[DataSources.DECOMPOSITION_VARIABLE]
        self.svals = dc.readSingularValues()
        self.rsvecs = dc.readRightSingularVectors()
        self.lsvecs = dc.readLeftSingularVectors()
        self.ZMatrix = dc.readZMatrix()
        
    def write(self, filename):
        if not self.lsvecs == None:
            if not self.ZMatrix == None:
                numpy.savez(filename, svals = self.svals, rsvecs = self.rsvecs, lsvecs = self.lsvecs, ZMatrix = self.ZMatrix)
            else:
                numpy.savez(filename, svals = self.svals, rsvecs = self.rsvecs, lsvecs = self.lsvecs)
        else:
            numpy.savez(filename, svals = self.svals, rsvecs = self.rsvecs)


class PickleWriter(AbstractWriterSimplified):
    
    def write(self, filename):
        f = open(filename,'wb')
        cPickle.dump(self.data, f, protocol=cPickle.HIGHEST_PROTOCOL)
        f.close()


class DenseTextFile(AbstractWriterSimplified):
    
    def write(self, filename):
        numpy.savetxt(filename, self.data)


class IndexListFile(AbstractWriterOld):
        
    def fromRpool(self, rpool, varname, vartype=None):
        self.data = rpool[varname]
    
    def write(self, filename):
        numpy.savetxt(filename, self.data, fmt='%i')


class FloatListFile(AbstractWriterOld):
        
    def fromRpool(self, rpool, varname, vartype=None):
        self.data = rpool[varname]
    
    def write(self, filename):
        numpy.savetxt(filename, self.data, fmt='%f')

