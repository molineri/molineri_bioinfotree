
import AbstractModelSelection
from random import sample
from .. import DataSources

class ModelSelection(AbstractModelSelection.ModelSelection):
    """N-fold cross-validation for model selection"""

    def __init__(self):
        AbstractModelSelection.ModelSelection.__init__(self)
        self.folds = None
        
    def loadResources(self):
        """Loads in the resources in resource pool. If folds are present
        in the resource pool, they will be used instead of randomly
        split tenfold.
        """
        AbstractModelSelection.ModelSelection.loadResources(self)
        if self.resource_pool.has_key(DataSources.CVFOLDS_VARIABLE):
            fs = self.resource_pool[DataSources.CVFOLDS_VARIABLE]
            self.folds = fs.readFolds()
        elif self.resource_pool.has_key(DataSources.TRAIN_QIDS_VARIABLE):
            qsource = self.resource_pool[DataSources.TRAIN_QIDS_VARIABLE]
            self.folds = qsource.readFolds()

    def setFolds(self, folds):
        """Sets user supplied folds
        
        @param folds: a list of lists, where each inner list contains the indices of examples belonging to one of the holdout sets
        @type folds: list of lists of integers"""
        self.folds = folds

    def setRandomFolds(self, foldcount):
        """Sets a randomized fold partition
        
        @param foldcount: the number of folds
        @type foldcount: integer"""
        self.folds = []
        indices = set(range(self.Y.shape[0]))
        foldsize = self.Y.shape[0] / foldcount
        leftover = self.Y.shape[0] % foldcount
        for i in range(foldcount):
            sample_size = foldsize
            if leftover > 0:
                sample_size += 1
                leftover -= 1
            fold = sample(indices, sample_size)
            indices = indices.difference(fold)
            self.folds.append(fold)

    def estimatePerformance(self, learner):
        """Estimates the expected N-fold cross-validation performance of a given model.
        By default 10-fold cross-validation with randomized fold partition is used.
        Another schemes can be used by setting the desired number of randomized or
        user defined folds with setFolds or setRandomFolds prior to calling this
        method.
 
        @param learner: trained learner object
        @type learner: RLS
        @return: estimated performance for the model
        @rtype: float"""
        #Default behaviour: random tenfold partition
        if not self.folds:
            self.setRandomFolds(10)
        self.Y_folds = []
        for fold in self.folds:
            self.Y_folds.append(self.Y[fold,:])
        performances = []
        for i in range(len(self.folds)):
            Y_pred = learner.computeHO(self.folds[i])
            performance = self.measure.multiOutputPerformance(self.Y_folds[i], Y_pred)
            performances.append(performance)
        performance = self.average(performances)
        return performance
    
    def average(self, performances):
        #None values are skipped when averaging, as this means undefined performance
        #for the given fold
        perf = 0.
        counter = 0
        for performance in performances:
            if performance != None:
                perf += performance
                counter += 1
        if counter == 0:
            return None
        perf /= counter
        return perf
        

