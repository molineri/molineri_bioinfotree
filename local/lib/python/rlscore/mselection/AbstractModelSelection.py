from .. import DataSources
from ..learner import AbstractSupervisedLearner

class ModelSelection(object):
    """Abstract base class for creating new model selection strategies, such as those based on
    cross-validation"""

    def __init__(self):
        self.models = None
        self.performances = None
        self.verbose = False

    def setLearner(self, learner):
        """Sets the learner which supplies the candidate models
        
        @param learner: learner object
        @type learner: RLS"""
        self.learner = learner
        if not self.learner.supportsModelSelection():
            raise Exception("Model selection is currently not available for the learner "+self.learner.__module__)
        
    def setResourcePool(self, rp):
        """
        Sets the ResourcePool object for the learner.
        
        @param rp: container of resources from which the learner infers the concept to be learned.
        @type rp: dict
        """
        self.resource_pool = rp
        
    def loadResources(self):
        """
        Loads the resources from the previously set resource pool.
        """
        if not self.resource_pool.has_key(DataSources.TRAIN_LABELS_VARIABLE):
            raise Exception("ModelSelection cannot be initialized without labels in resource pool")
        Y = self.resource_pool[DataSources.TRAIN_LABELS_VARIABLE]
        self.Y = Y

    def setMeasure(self, measure):
        """Sets the used performance measure
        
        @param measure: performance measure object
        @type measure: subclass of AbstractMeasure"""
        self.measure = measure

    def estimatePerformance(self, model):
        """Estimates the expected performance of a given model. This function should be overriden by the inheriting class
        
        @param model: trained learner object
        @type model: RLS
        @return: estimated performance for the model
        @rtype: float"""
        return 0.

    def setVerbosity(self, verbosity):
        """Controls whether the model selection procedure prints information to the standard output or not
        
        @param verbosity: truth value about whether verbosity mode is on or not
        @type verbosity: boolean"""
        self.verbose = verbosity

    def setParameters(self, parameters):
        if parameters.has_key("reggrid"):
            lower = int(parameters["reggrid"].split("_")[0])
            upper = int(parameters["reggrid"].split("_")[1])
            assert lower <= upper
            reggrid = range(lower, upper + 1)
            reggrid = [2 ** x for x in reggrid]
        else:
            reggrid = range(-5, 6)
            reggrid = [2. ** x for x in reggrid]
        self.reggrid = reggrid 

    def reggridSearch(self):
        """Searches the regularization parameter grid to choose the value which, according to
        the estimatePerformance function, seems to provide best performance"""
        #Current assumption is that all of the algorithms included in the package will be based on regularized
        #risk minimization
        self.models = []
        self.performances = []
        self.best_performance = None
        self.best_model = None
        self.best_regparam = None
        if self.verbose:
            print "Regularization parameter grid initialized to", self.reggrid
        for regparam in self.reggrid:
            if self.verbose:
                print "Solving %s for regularization parameter value %f" % ("learner", regparam)
            self.learner.solve(regparam)
            model = self.learner.getModel()
            self.models.append(model)
            performance = self.estimatePerformance(self.learner)
            self.performances.append(performance)
            if self.best_performance==None:
                self.best_performance = performance
                self.best_model = model
                self.best_regparam = regparam
            else:
                if self.measure.comparePerformances(performance, self.best_performance) > 0:
                    self.best_performance = performance
                    self.best_model = model
                    self.best_regparam = regparam
            if self.verbose:
                if performance != None:
                    print "%f %s (averaged), %f regularization parameter" % (performance, self.measure.getName(), regparam)
                else:
                    print "Performance undefined for %f regularization parameter" %regparam
        if self.verbose:
            if self.best_performance != None:
                print "Best cross-validation performance %f %s with regularization parameter %f" % (self.best_performance, self.measure.getName(), self.best_regparam)
            else:
                print "Performance undefined for all tried values"
        self.resource_pool[DataSources.REGGRID_RESULTS_VARIABLE] = RegGridResults(self.reggrid, self.performances, self.best_regparam, self.best_performance)

    def findBestModel(self):
        """Searches for the best model"""
        self.reggridSearch()

    def getBestModel(self):
        """Returns the best model
        @return: best model
        @rtype: RLS"""
        return self.best_model

class RegGridResults(object):
    
    def __init__(self, grid, perf, bestrp, bestperf):
        self.grid = grid
        self.perf = perf
        self.bestrp = bestrp
        self.bestperf = bestperf
