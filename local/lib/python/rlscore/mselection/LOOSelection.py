import AbstractModelSelection

class ModelSelection(AbstractModelSelection.ModelSelection):
    """Leave-one-out cross-validation for model selection"""

    def estimatePerformance(self, model):
        """Returns the leave-one-out estimate
        
        @param model: trained learner object
        @type model: RLS
        @return: estimated performance for the model
        @rtype: float"""
        Y_pred = model.computeLOO()
        performance = self.measure.multiOutputPerformance(self.Y, Y_pred)
        return performance
