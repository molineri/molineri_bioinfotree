import AbstractModelSelection
from ..measure import AbstractPWMeasure
from ..learner import AllPairsRankRLS

class ModelSelection(AbstractModelSelection.ModelSelection):
    """leave-pair-out cross-validation for model selection"""

    def setLearner(self, learner):
        """Sets the learner which supplies the candidate models
        
        @param learner: learner object
        @type learner: RLS"""
        self.learner = learner
        if not isinstance(self.learner, AllPairsRankRLS.RLS):
            raise Exception("LPO cross-validation implemented only for RankRLS")

    def setMeasure(self, measure):
        """Sets the used performance measure
        
        @param measure: performance measure object
        @type measure: subclass of AbstractPWMeasure"""
        if not isinstance(measure, AbstractPWMeasure.Measure):
            raise Exception("supplied performance measure %s does not support LPO cross-validation" % measure.name)
        self.measure = measure

    def estimatePerformance(self, model):
        """Leave-pair-out estimate of performance
        
        @param model: trained learner object
        @type model: RLS
        @return: estimated performance for the model
        @rtype: float"""
        predictions = []
        performances = []
        for i in range(self.Y.shape[1]):
            pairs = self.measure.getPairs(self.Y, i)
            pred = model.computePairwiseCV(pairs, i)
            perf = self.measure.pairwisePerformance(pairs, self.Y, i, pred)
            predictions.append(pred)
            performances.append(perf)
        performance = self.measure.aggregate(performances)
        return performance
