import sys
import AbstractKernel
from numpy import mat
from numpy import vstack
from numpy import array

class Kernel(AbstractKernel.Kernel):
    """Polynomial kernel."""
    
    def __init__(self):
        AbstractKernel.Kernel.__init__(self)
        #default parameters
        self.degree = 2
        self.coef0 = 0. 
        self.gamma = 1.
    
    def kernel(self, x, z):
        #Note, current implementation overrides the kernel matrix building methods
        #of AbstractKernel, so that this method is never called when building the
        #kernel matrix. This is left just to document what the kernel function
        #itself is like.
        """Kernel function is evaluated with the given arguments x and z.
        
        @param x: column vector containing the attribute values for the first instance
        @type x: scipy sparse matrix
        @param z: column vector containing the attribute values for the second instance
        @type z: scipy sparse matrix
        @return: value of the kernel evaluation
        @rtype: float"""
        return (self.gamma * (x.T * z)[0, 0] + self.coef0) ** self.degree
    
    
    def setParameters(self, parameters):
        """Sets the parameter values of the kernel function.
        
        @param parameters: 'degree_coef0_gamma' string. The kernel is defined
        as (gamma * <xi,xj> + coef0)**degree
        @type parameters: string"""
        if 'degree' in parameters:
            self.degree = int(parameters['degree'])
        else:
            if self.verbose:
                print "No degree parameter supplied to polynomial kernel, using default value 2"
                print "k(xi,xj) = (gamma * <xi,xj> + coef0)**degree"
            self.degree = 2
        if 'coef0' in parameters:
            self.coef0 = float(parameters['coef0'])
        else:
            if self.verbose:
                print "No coef0 parameter supplied to polynomial kernel, using default value 0"
                print "k(xi,xj) = (gamma * <xi,xj> + coef0)**degree"
            self.coef0 = 0.
        if 'gamma' in parameters:
            self.gamma = float(parameters['gamma'])
        else:
            if self.verbose:
                print "No gamma parameter supplied to polynomial kernel, using default value 1"
                print "k(xi,xj) = (gamma * <xi,xj> + coef0)**degree"
            self.gamma = 1.


    def getTrainingKernelMatrix(self, fs, bvectors=None):
        """Builds a kernel matrix from the training data"""
        #Builds first a linear kernel matrix, and from it constructs the polynomial
        #one
        degree, coef0, gamma = self.degree, self.coef0, self.gamma
        if bvectors == None:
            X = fs.readFeatures()
            X = X.tocsc()
            if self.verbose:
                print "Building polynomial kernel matrix for training data"
            #use array view instead of matrix to perform elementwise operations
            #instead of matrix operations
            K = array((X.T * X).todense())
            K *= gamma
            K += coef0
            K = K ** degree
            if self.bias != 0:
                K += self.bias
            K = mat(K)
            if self.verbose:
                print "Polynomial kernel matrix of shape %d*%d ready" % (K.shape[0], K.shape[1])
        else:
            K = self.getSparseTrainingKernelMatrix(fs, bvectors)
        return K
    
    def getSparseTrainingKernelMatrix(self, fs, bvectors):
        #Builds first a linear kernel matrix, and from it constructs the polynomial
        #one
        degree, coef0, gamma = self.degree, self.coef0, self.gamma
        X = fs.readFeatures()
        X = X.tocsc()
        if self.verbose:
            print "Building sparse polynomial kernel matrix for training data"
        K = array((X[:,bvectors].T*X).todense())
        K *= gamma
        K += coef0
        K = K ** degree
        if self.bias != 0:
            K += self.bias
        K = mat(K)
        if self.verbose:
             print "Polynomial kernel matrix of shape %d*%d ready" % (K.shape[0], K.shape[1])
        return K

    def getTestKernelMatrix(self, fs):
        """Builds a kernel matrix between training and test data"""
        #Builds first a linear kernel matrix, and from it constructs the polynomial
        #one
        degree, coef0, gamma = self.degree, self.coef0, self.gamma
        if self.train_X==None:
            raise Exception("buildTestKernelMatrix called before supplying features")
        test_X = fs.readFeatures(dimensionality=self.train_X.shape[0])
        test_X = test_X.tocsc()
        if self.verbose:
            print "Building polynomial kernel matrix between training and test data"
        K = array((self.train_X.T * test_X).todense())
        K *= gamma
        K += coef0
        K = K ** degree
        if self.bias != 0:
            K += self.bias
        K = mat(K)
        if self.verbose:
            print "Polynomial kernel matrix of shape %d*%d ready" % (K.shape[0], K.shape[1])
        return K

    def getName(self):
        """Return the name of the kernel
        
        @return: kernel name
        @rtype string  """
        return "polynomial kernel"




