import AbstractKernel
from .. import Decompositions
from .. import SparseModule
import numpy
from numpy import mat
from numpy import vstack
from numpy import ones
from scipy import sparse
from numpy import float64
from math import sqrt

class Kernel(AbstractKernel.Kernel):
    """Linear kernel"""

    def __init__(self):
        AbstractKernel.Kernel.__init__(self)
        self.parameters = ""

    def getDecomposition(self, fs, bvectors=None):
        """Returns the decomposition for the kernel matrix
        
        @return: svals, evecs, U, Z
        @rtype: tuple of numpy matrices"""
        X = fs.readFeatures()
        if bvectors != None or X.shape[0] > X.shape[1]:
            K = self.getTrainingKernelMatrix(fs, bvectors)
            #First possibility: subset of regressors has been invoked
            if bvectors:
                svals, evecs, U, Z = SparseModule.decomposeSubsetKM(K, bvectors)
            #Second possibility: dual mode if more attributes than examples
            else:
                svals, evecs = Decompositions.decomposeKernelMatrix(K)
                U, Z = None, None
        #Third possibility, primal decomposition
        else:
            #Invoking getPrimalDataMatrix adds the bias feature
            X = self.getPrimalDataMatrix(fs).todense()
            svals, evecs, U = Decompositions.decomposeDataMatrix(X)
            Z = None
        return svals, evecs, U, Z
    
    def getPrimalDataMatrix(self, fs, dimensionality=None):
        """
        Constructs the feature representation of the data.
        If bias is defined, a bias feature with value
        sqrt(bias) is added to each example. This function
        should be used when making predictions, or training
        the primal formulation of the learner.
        @param fs: feature source
        @type fs: FeatureSource
        @param dimensionality: dimensionality of the feature space
        (by default the number of rows in the data matrix in the feature source)
        @type dimensionality: integer
        @return: data matrix
        @rtype: scipy sparse matrix in csc format
        """
        X = fs.readFeatures(dimensionality=dimensionality)
        X = X.tocsc()
        if self.bias!=0:
            bias_slice = sqrt(self.bias)*mat(ones((1,X.shape[1]),dtype=float64))
            X = sparse.vstack([X,bias_slice])
            X = X.tocsc()
        return X

    def getTrainingKernelMatrix(self, fs, bvectors=None):
        if bvectors == None:
            X = fs.readFeatures()
            X = X.tocsc()
            """Builds a kernel matrix from the training data"""
            if self.verbose:
                print "Building linear kernel matrix for training data"
            K = mat((X.T * X).todense())
            if self.verbose:
                print "Linear kernel matrix of shape %d*%d ready" % (K.shape[0], K.shape[1])
            if self.bias != 0:
                K += self.bias
        else:
            K = self.getSparseTrainingKernelMatrix(fs, bvectors)
        return K

    def getTestKernelMatrix(self, fs):
        """Builds a kernel matrix between training and test data"""
        if self.train_X==None:
            raise Exception("buildTestKernelMatrix called before supplying features")
        test_X = fs.readFeatures(dimensionality=self.train_X.shape[0])
        test_X = test_X.tocsc()
        if self.verbose:
            print "Building linear kernel matrix between training and test data"
        K = mat((self.train_X.T * test_X).todense())
        if self.verbose:
            print "Linear kernel matrix of shape %d*%d ready" % (K.shape[0], K.shape[1])
        if self.bias != 0:
            K += self.bias
        return K

    def getSparseTrainingKernelMatrix(self, fs, bvectors):
        if self.verbose:
            print "Building sparse linear kernel matrix for training data"
        assert len(set(bvectors)) == len(bvectors)
        X = fs.readFeatures()
        X = X.tocsc()
        K = (X[:,bvectors].T*X).todense()
        if self.verbose:
             print "Linear kernel matrix of shape %d*%d ready" % (K.shape[0], K.shape[1])
        if self.bias != 0:
            K += self.bias
        return K

    def getName(self):
        """Return the name of the kernel
        
        @return: kernel name
        @rtype string  """
        return "linear kernel"
        
        
