from numpy import float64, mat, zeros
from .. import Decompositions
from .. import SparseModule
from .. import DataSources

class Kernel(object):
    """The abstract base class from which all kernel implementations
    should be derived. At minimum the deriving class must re-implement
    the kernel function, which calculates the dot product between two
    examples. For optimal performance (or for non-vectorial data), re-implement
    also getTrainingKernelMatrix and getTestKernelMatrix, since calling
    the kernel function for each pair of examples creates significant
    computational overhead on large data sets."""

    def __init__(self):
        """Default implementation uses the scipy sparse matrices for internal representation of the data."""
        self.verbose = False
        self.bias = 0.
            
    def predictionCacheFromRpool(self, rpool):
        """
        Prepares the kernel for prediction phase by storing the necessary information.
        @param rpool: resource pool
        @type rpool: dict
        """
        fs = rpool[DataSources.TRAIN_FEATURES_VARIABLE]
        if rpool.has_key(DataSources.BASIS_VECTORS_VARIABLE):
            bvectors = rpool[DataSources.BASIS_VECTORS_VARIABLE]
        else:
            bvectors = None
        self.buildPredictionCache(fs, bvectors)
            
    def buildPredictionCache(self, fs, bvectors = None):
        """
        Prepares the kernel for prediction phase by storing the necessary information
        @param fs: training set features
        @type fs: FeatureSource
        @param bvectors: basis vector indices, to be supplied is reduced set approximation is used
        @type bvectors: list of nonnegative integers
        """
        self.train_X = fs.readFeatures()
        self.train_X = self.train_X.tocsc()
        #for the reduced set approximation, only a subset of training examples are needed for
        #prediction
        if bvectors:
            self.train_X = self.train_X[:,bvectors]
    
    def setBias(self, bias):
        """
        Sets the value of the bias parameter
        @param bias: bias
        @type bias: float
        """
        #Note: This could be delegated to setParameters
        self.bias = bias
    
    def kernel(self, x, z):
        """Kernel function is evaluated with the given arguments x and z. This function should be overridden by the subclasses.
        
        @param x: column vector containing the attribute values for the first instance
        @type x: scipy sparse matrix
        @param z: column vector containing the attribute values for the second instance
        @type z: scipy sparse matrix
        @return: value of the kernel evaluation
        @rtype: float"""
        return 0

    def setParameters(self, parameters):
        """Sets the parameter values of the kernel function. Required parameters
        depend on the kernel.
        
        @param parameters: parameters for the kernel
        @type parameters: dictionary of parameters"""
        #Since it is not guaranteed that the caller will provide all necessary parameters,
        #implementations should have reasonable behavior in this case, such as using
        #default values, or throwing an exception with informative error message, if specifying
        #reasonable default values is not possible.
        pass
    
    def setVerbosity(self, verbosity):
        """Defines whether the kernel module should print information to the standard output or not
        
        @param verbosity: boolean value indicating whether the program is in the verbosity mode or not
        @type verbosity: bool"""
        self.verbose = verbosity

    def getDecomposition(self, fs, bvectors=None):
        """Returns a decomposition representation of the data. Default implementation
        builds and decomposes the kernel matrix itself (standard case), or the 
        empirical kernel map of the training data, if reduced set approximation is
        used. Inheriting classes may also re-implement this by decomposing the feature
        map of the data (e.g. linear kernel with low-dimensional data).
        @param fs: features of the training data
        @type fs: FeatureSource
        @param bvectors: indices of the basis vectors, to be supplied if reduced set
        approximation is used
        @type bvectors: list of integers
        @return: svals, evecs, U, Z
        @rtype: tuple of numpy matrices"""
        K = self.getTrainingKernelMatrix(fs, bvectors)
        if not bvectors == None:
            svals, evecs, U, Z = SparseModule.decomposeSubsetKM(K, bvectors)
        else:
            svals, evecs = Decompositions.decomposeKernelMatrix(K)
            U, Z = None, None
        return svals, evecs, U, Z
        
    def decompositionFromPool(self, rpool):
        """Builds decomposition representing the training data from resource pool.
        Default implementation
        builds and decomposes the kernel matrix itself (standard case), or the 
        empirical kernel map of the training data, if reduced set approximation is
        used. Inheriting classes may also re-implement this by decomposing the feature
        map of the data (e.g. linear kernel with low-dimensional data).
        @param rpool: resource pool
        @type rpool: dict
        @return: svals, evecs, U, Z
        @rtype: tuple of numpy matrices
        """
        fs = rpool[DataSources.TRAIN_FEATURES_VARIABLE]
        if rpool.has_key(DataSources.BASIS_VECTORS_VARIABLE):
            bvectors = rpool[DataSources.BASIS_VECTORS_VARIABLE]
        else:
            bvectors = None
        return self.getDecomposition(fs, bvectors)
        
    def trainKMFromPool(self, rpool):
        """Builds kernel matrix for the training data from resource pool. If resource
        pool contains basis vectors, a |bvectors|*|train_examples| kernel matrix is
        returned instead of the full kernel matrix.
        @param rpool: resource pool
        @type rpool: dict
        @return: kernel matrix
        @rtype: numpy matrix
        """
        fs = rpool[DataSources.TRAIN_FEATURES_VARIABLE]
        if rpool.has_key(DataSources.BASIS_VECTORS_VARIABLE):
            bvectors = rpool[DataSources.BASIS_VECTORS_VARIABLE]
        else:
            bvectors = None
        return self.getTrainingKernelMatrix(fs, bvectors)
    
    def testKMFromPool(self, rpool):
        """Builds kernel matrix between training and test examples. If reduced
        set was used, only the subset of the training examples which were the
        basis vectors is considered.
        buildPredictionCache() or predictionCacheFromRpool()
        method must have been invoked before calling this function.
        @param rpool: resource pool
        @type rpool: ResourcePool
        @return: kernel matrix
        @rtype: numpy matrix
        """
        fs = rpool[DataSources.PREDICTION_FEATURES_VARIABLE]
        return self.getTestKernelMatrix(fs)
        
    def getTrainingKernelMatrix(self, fs, bvectors=None):
        """
        Constructs and returns the kernel matrix for the training data
        @param fs: feature source containing the training data
        @type fs: FeatureSource
        @param bvectors: basis vectors, supply these if using the reduced set approximation
        @type bvectors: list of indices (0 <= i < number of training examples)
        @return: bvectors*training_examples kernel matrix
        @rtype: numpy matrix (dense)
        """
        if bvectors == None:
            X = fs.readFeatures()
            X = X.tocsc()
            if self.verbose:
                print "Building a kernel matrix for training data"
            m = X.shape[1]
            K = mat(zeros((m, m), dtype=float64))
            for i in range(m):
                for j in range(i, m):
                    val = self.kernel(X[:, i], X[:, j])
                    K[i, j] = val
                    K[j, i] = val
            if self.verbose:
                print "Kernel matrix of shape %d*%d ready" % (K.shape[0], K.shape[1])
            if self.bias != 0:
                K += self.bias
        else:
            K = self.getSparseTrainingKernelMatrix(fs, bvectors)
        return K        

    def getTestKernelMatrix(self, fs):
        """
        Constructs and returns the kernel matrix between the training data
        and the test data. buildPredictionCache() or predictionCacheFromRpool()
        method must have been invoked before calling this function.
        @param fs: feature source containing the test data
        @type fs: TestFeatureSource
        @return: training_examples*test_examples kernel matrix, where the set of
        training examples consists only of the basis vectors, if reduced set approximation
        is used
        @rtype: numpy matrix (dense)
        """
        if (self.train_X==None):
            raise Exception("buildTestKernelMatrix called without training data present")
        if self.verbose:
            print "Building kernel matrix between training and test data"
        test_X = fs.readFeatures(dimensionality=self.train_X.shape[0])
        test_X = test_X.tocsc()
        m = self.train_X.shape[1]
        n = test_X.shape[1]
        K = mat(zeros((m, n), dtype=float64))
        for i in range(m):
            if (i + 1) % 50 == 0 and self.verbose:
                print "Row %d of kernel matrix ready" % (i + 1)
            for j in range(n):
                val = self.kernel(self.train_X[:, i], test_X[:,j])
                K[i, j] = val
        if self.verbose:
            print "Kernel matrix of shape %d*%d ready" % (K.shape[0], K.shape[1])
        if self.bias != 0:
            K += self.bias
        return K

    def getSparseTrainingKernelMatrix(self,fs, bvectors):
        #A helper routine for getTrainingKernelMatrix
        #No duplicates allowed in the basis vectors
        assert len(set(bvectors)) == len(bvectors)
        X = fs.readFeatures()
        X = X.tocsc()
        if self.verbose:
            print "Building a sparse kernel matrix for training data"
        m = X.shape[1]
        n = len(bvectors)
        K = mat(zeros((n, m), dtype=float64))
        for i in range(n):
            if (i + 1) % 50 == 0 and self.verbose:
                print "Row %d of kernel matrix ready" % (i + 1)            
            bvind = bvectors[i]
            for j in range(m):
                val = self.kernel(X[:, bvind], X[:, j])
                K[i, j] = val
        if self.verbose:
            print "Kernel matrix of shape %d*%d ready" % (K.shape[0], K.shape[1])
        if self.bias != 0:
            K += self.bias
        return K

    def getName(self):
        """Return the name of the kernel
        
        @return: kernel name
        @rtype string  """
        return "abstract kernel"
