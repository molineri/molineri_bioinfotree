import AbstractKernel
import numpy
from numpy import mat
from numpy import float64
import sys
import math

class Kernel(AbstractKernel.Kernel):
    """Gaussian (RBF) kernel""" 

    def __init__(self):
        AbstractKernel.Kernel.__init__(self)
        self.gamma = 1.0
        
    def buildPredictionCache(self, fs, bvectors = None):
        self.train_X = fs.readFeatures()
        self.train_X = self.train_X.tocsc()
        if bvectors:
            self.train_X = self.train_X[:,bvectors]
        #norms of the training examples are cached to speed up prediction
        train_norms = []
        for i in range(self.train_X.shape[1]):
            train_norms.append((self.train_X.T[i]*self.train_X.T[i].T)[0,0])
        self.train_norms = mat(train_norms).T
        

    def setParameters(self, parameters):
        """Sets the parameter values of the kernel function.
        
        @param parameters: the gamma parameter controlling the kernel width. Gamma must be greater than zero
        @type parameters: string or float"""
        if not 'gamma' in parameters:
            if self.verbose:
                print "No gamma parameter supplied to gaussian kernel, using default value 1.0"
                print "k(xi,xj) = e^(-gamma*<xi-xj,xi-xj>)"
            gamma = 1.0
        else:
            gamma = parameters['gamma']
            try:
                gamma = float(gamma)
            except ValueError:
                raise Exception('ERROR: Malformed kernel parameter for Gaussian kernel. "%s" not a real valued number.\n' % parameters)
            if gamma <= 0.:
                raise Exception('ERROR: nonpositive kernel parameter for Gaussian kernel\n')
        self.gamma = gamma
    
    
    def getTrainingKernelMatrix(self, fs, bvectors=None):
        """Builds a kernel matrix from the training data""" 
        if bvectors == None:
            X = fs.readFeatures()
            X = X.tocsc()
            if self.verbose:
                print "Building Gaussian kernel matrix for training data"
            gamma = self.gamma
            m = X.shape[1]
            #The Gaussian kernel matrix is constructed from a linear kernel matrix
            linkm = mat((X.T * X).todense())
            trnorms = mat(numpy.diag(linkm)).T
            K = trnorms * mat(numpy.ones((1, m), dtype = float64))
            K = K + K.T
            K = K - 2 * linkm
            K = - gamma * K
            K = numpy.exp(K)
            if self.verbose:
                print "Gaussian kernel matrix of shape %d*%d ready" % (K.shape[0], K.shape[1])
            if self.bias != 0:
                K += self.bias
        else:
            K = self.getSparseTrainingKernelMatrix(fs, bvectors)
        return K

    def getTestKernelMatrix(self, fs):
        """Builds a kernel matrix between training and test data"""
        if self.train_X==None:
            raise Exception("buildTestKernelMatrix called before supplying features")
        test_X = fs.readFeatures(dimensionality=self.train_X.shape[0])
        test_X = test_X.tocsc()
        if self.verbose:
            print "Building Gaussian kernel matrix between training and test data"
        gamma = self.gamma
        m = self.train_X.shape[1]
        n = test_X.shape[1]
        #The Gaussian kernel matrix is constructed from a linear kernel matrix
        linkm = mat((self.train_X.T * test_X).todense())
        test_norms = []
        for i in range(n):
            test_norms.append((test_X.T[i] * test_X.T[i].T)[0,0])
        test_norms = mat(test_norms).T
        K = mat(numpy.ones((m, 1), dtype = float64)) * test_norms.T
        K = K + self.train_norms * mat(numpy.ones((1, n), dtype = float64))
        K = K - 2 * linkm
        K = - gamma * K
        K = numpy.exp(K)
        if self.verbose:
            print "Gaussian kernel matrix of shape %d*%d ready" % (K.shape[0], K.shape[1])
        if self.bias != 0:
            K += self.bias
        return K

    def getSparseTrainingKernelMatrix(self, fs, bvectors):
        """Builds a sparse kernel matrix, containing only the rows in the subset"""
        assert len(set(bvectors)) == len(bvectors)
        X = fs.readFeatures()
        X = X.tocsc()
        if self.verbose:
            print "Building sparse Gaussian kernel matrix for training data"
        gamma = self.gamma
        #The Gaussian kernel matrix is constructed from a linear kernel matrix
        linkm = (X[:,bvectors].T*X).todense()
        m = X.shape[1]
        n = len(bvectors)
        trnorms = []
        for i in range(m):
            trnorms.append((X.T[i] * X.T[i].T)[0,0])
        trnorms = mat(trnorms).T
        bvnorms = trnorms[bvectors]
        K = mat(numpy.ones((n, 1), dtype = float64)) * trnorms.T
        K = K + bvnorms * mat(numpy.ones((1, m), dtype = float64))
        K = K - 2 * linkm
        K = - gamma * K
        K = numpy.exp(K)
        if self.bias != 0:
            K += self.bias
        if self.verbose:
            print "Gaussian kernel matrix of shape %d*%d ready" % (K.shape[0], K.shape[1])
        return K
 
    def getName(self):
        """Return the name of the kernel
        
        @return: kernel name
        @rtype string  """
        return "Gaussian kernel"

                



        
        
