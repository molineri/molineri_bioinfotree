
from numpy import *
import numpy.linalg as la

SMALLEST_EVAL = 0.0000000001

def decomposeDataMatrix(X):
    """Returns the reduced singular value decomposition of the data matrix X so that only the singular vectors corresponding to the nonzero singular values are returned.
    
    @param X: data matrix whose rows and columns correspond to the features and datumns, respectively.
    @type X: numpy matrix of floats
    @return: the nonzero singular values and the corresponding left and right singular vectors of X. The singular vectors are contained in a r*1-matrix, where r is the number of nonzero singular values. 
    @rtype: a tuple of three numpy matrices"""
    svecs, svals, U = la.svd(X.T, full_matrices=0)
    svals, evecs = mat(svals), mat(svecs)
    evals = multiply(svals, svals)
    
    maxnz = min(X.shape[0], X.shape[1])
    nz = 0
    for l in range(maxnz):
        if evals[0, l] > SMALLEST_EVAL:
            nz += 1
    rang = range(0, nz)
    evecs = evecs[:, rang]
    svals = svals[:, rang]
    U = U[rang]
    return svals, evecs, U


def decomposeKernelMatrix(K):
    """"Returns the reduced eigen decomposition of the kernel matrix K so that only the eigenvectors corresponding to the nonzero eigenvalues are returned.
    
    @param K: a positive semi-definite kernel matrix whose rows and columns are indexed by the datumns.
    @type K: numpy matrix of floats
    @return: the square roots of the nonzero eigenvalues and the corresponding eigenvectors of K. The square roots of the eigenvectors are contained in a r*1-matrix, where r is the number of nonzero eigenvalues. 
    @rtype: a tuple of two numpy matrices"""
    evals, evecs = la.eigh(K)
    evals, evecs = mat(evals), mat(evecs)
    nz = 0
    maxnz = K.shape[0]
    for l in range(maxnz):
        if evals[0, l] > SMALLEST_EVAL:
            nz += 1
    rang = range(maxnz - nz, maxnz)
    evecs = evecs[:, rang]
    evals = evals[:, rang]
    svals = sqrt(evals)
    return svals, evecs
    

