#Utilities for reading in the files used by the
#RLS software package
import sys
from scipy import sparse
import numpy
from numpy import float64
from numpy import zeros
from numpy import mat
import DataSources
import cPickle

def readFeatureFile(f):
    """Reads in the feature values and checks that the input file is correctly formatted.
    Returns the attribute values, comments, dimensionality of the feature space and number of
    nonzero attributes
    
    param f: open file
    @type f: file object
    @param subset: if supplied only the lines whose indices are in subset are read in (default None)
    @type subset: list of integers
    @return features, comments, dimensionality of feature space, number of nonzero features
    @rtype: list of integer-float pairs, list of strings, integer, integer"""

    #some interesting statistics are calculated
    inst_size, fspace_size, nzero_size = getStatistics(f)
    f.seek(0)
    #Features and comments are returned to the caller 
    #The indexing, with respect to the instances, is the same in all the lists.
    #Each line in the source represents an instance
    #each row represents a feature, each column an instance
    rows = numpy.zeros(nzero_size, dtype=numpy.int32)
    columns = numpy.zeros(nzero_size, dtype=numpy.int32)
    values = numpy.zeros(nzero_size, dtype=numpy.float32)
    linecounter = 0
    feaspace_dim = 0
    nonzeros = 0
    for line in f:
        linecounter += 1
        #Empty lines and commented lines are passed over
        if len(line.strip()) == 0 or line[0] == '#':
            print "Warning: no inputs on line %d" % linecounter
            continue
        line = line.split("#",1)
        attributes = line[0].split()
        #if len(line)==2:
        #    comment = line[1]
        #else:
        #    comment = ""
        #We gather the comments, in case caller is interested in them
        #all_comments.append(comment)
        previous = -1
        #Attributes indices must be positive integers in an ascending order,
        #and the values must be real numbers.
        for att_val in attributes:
            if len(att_val.split(":")) != 2:
                raise Exception("Error when reading in feature file: feature:value pair %s on line %d is not well-formed\n" % (att_val, linecounter))
            index, value = att_val.split(":")
            try:
                index = int(index)
                value = float(value)
                if value != 0.:
                    rows[nonzeros] = index
                    columns[nonzeros] = linecounter-1
                    values[nonzeros] = value
                    nonzeros += 1
            except ValueError:
                raise Exception("Error when reading in feature file: feature:value pair %s on line %d is not well-formed\n" % (att_val, linecounter))
            if not index > previous:
                raise Exception("Error when reading in feature file: line %d features must be in ascending order\n" % (linecounter))
            previous = index
            if index+1 > feaspace_dim:
                feaspace_dim = index+1
    #That's all folks
    row_size = feaspace_dim
    col_size = linecounter
    return rows, columns, values, row_size, col_size, nonzeros    

def getStatistics(f):
    """Calculates number of examples, dimensionality of the feature space and number of
    nonzero attributes
    
    param f: lines of the feature file (possibly an open file)
    @type f: iterable object
    @param subset: if supplied only the lines whose indices are in subset are read in (default None)
    @type subset: list of integers
    @return dimensionality of feature space, number of nonzero features
    @rtype: integer integer, integer"""

    #some interesting statistics are calculated
    linecounter = 0
    feaspace_dim = 0
    nonzeros = 0
    #Features and comments are returned to the caller 
    #The indexing, with respect to the instances, is the same in all the lists.
    for line in f:
        linecounter += 1
        #Empty lines and commented lines are passed over
        if len(line.strip()) == 0 or line[0] == '#':
            continue
        line = line.split("#",1)
        attributes = line[0].split()
        #Attributes indices must be positive integers in an ascending order,
        #and the values must be real numbers.
        for att_val in attributes:
            if len(att_val.split(":")) != 2:
                raise Exception("Error when reading in feature file: feature:value pair %s on line %d is not well-formed\n" % (att_val, linecounter))
            index, value = att_val.split(":")
            try:
                index = int(index)
                value = float(value)
                if value != 0.:
                    nonzeros += 1
            except ValueError:
                raise Exception("Error when reading in feature file: feature:value pair %s on line %d is not well-formed\n" % (att_val, linecounter))
            if index+1 > feaspace_dim:
                feaspace_dim = index+1
    #That's all folks
    return feaspace_dim, linecounter, nonzeros    


def readSparseMatrix(f):
    """Reads a sparse matrix representation of the data from an
    open file-like object that is provided
    
    @param source: lines of the attribute file (possibly an open file)
    @type source: iterable object
    @param subset: if supplied only the lines whose indices are in subset are read in (default None)
    @type subset: list of integers
    @param dimensionality: dimensionality of the feature space
    @type dimensionality: integer
    @return X, comments
    @rtype scipy sparse matrix, list of strings"""
    rows, columns, values, row_size, col_size, nonzeros   = readFeatureFile(f)
    X = sparse.coo_matrix((values,(rows,columns)),(row_size, col_size), dtype=float64)
    X = X.tocsc()
    return X


class AbstractReaderOld(object):
        
    def toRpool(self, rpool, varname, vartype = None):
        """Puts data to the given resource pool
        @param rpool: resource pool
        @type rpool: ResourcePool
        @param varname: name of the loaded variable
        @type varname: string
        @param vartype: class of the loaded variable
        @type vartype: class
        """
        if vartype == None:
            if DataSources.VARIABLE_TYPES.has_key(varname):
                vartype = DataSources.VARIABLE_TYPES[varname]
            else:
                vartype = DataSources.DataSource
        ds = vartype(self.data)
        rpool[varname] = ds


class AbstractReaderSimplified(object):
        
    def toRpool(self, rpool, varname, vartype = None):
        """Puts data to the given resource pool
        @param rpool: resource pool
        @type rpool: ResourcePool
        @param varname: name of the loaded variable
        @type varname: string
        @param vartype: class of the loaded variable
        @type vartype: class
        """
        rpool[varname] = self.data


class BvectorFile(AbstractReaderSimplified):
    """File containing basis vectors"""
    
    def __init__(self, filename):
        """Reads in a basis-vectors file
        
        @param filename: path to basis vector file
        @type filename: string"""
        f = open(filename)
        bvectors = f.readline().strip().split()
        bvectors = [int(x) for x in bvectors]
        for x in bvectors:
            if x < 0:
                raise Exception("Error when reading in basis vector file: Indexing for basis vectors should start from 0, %d found" % (x))
        if len(set(bvectors)) != len(bvectors):
            raise Exception("Error when reading in basis vector file: The same basis vector index was supplied multiple times")
        self.data = bvectors
        f.close()

class FeatureFile(AbstractReaderOld):
    
    def __init__(self, filename):
        """Reads in a RLScore format feature file 
        
        @param filename: path tofeature file
        @type filename: string"""
        f = open(filename)
        self.data = readSparseMatrix(f)
        f.close()

class FoldFile(AbstractReaderOld):
    
    def __init__(self, filename):
        """Reads in a fold file for cross-validation 
        
        @param filename: path to fold file
        @type filename: string"""
        f = open(filename)
        folds = []
        for i, line in enumerate(f):
            #We allow comments starting with #
            cstart = line.find("#")
            if cstart != -1:
                comment = line[cstart:]
                line = line[:cstart]
            fold = []
            foldset = set([])
            line = line.strip().split()
            for x in line:
                try:
                    index = int(x)
                except ValueError:
                    raise Exception("Error when reading in fold file: malformed index on line %d in the fold file: %s" % (i + 1, x))
                if index < 0:
                    raise Exception("Error when reading in fold file: negative index on line %d in the fold file: %d" % (i + 1, index))
                if index in foldset:
                    raise Exception("Error when reading in fold file: duplicate index on line %d in the fold file: %d" % (i + 1, index + 1))
                fold.append(index)
                foldset.add(index)
            folds.append(fold)
        f.close()
        self.data = folds
        f.close()

class DenseTextFile(AbstractReaderSimplified):
    
    def __init__(self, filename):
        """Reads in a text file in dense matrix format. Uses Numpy loadtxt.
        
        @param filename: path to text file in dense matrix format
        @type filename: string"""
        f = open(filename)
        self.data = numpy.loadtxt(f)
        self.data = numpy.mat(self.data)
        if self.data.shape[0] == 1:
            self.data = self.data.T
        f.close()

class QidFile(AbstractReaderOld):
    
    def __init__(self, filename):
        """Reads in a Qid file 
        
        @param filename: path to qid file
        @type filename: string"""
        f = open(filename)
        self.readQids(f)
        self.mapQids()
        self.data = self.qids
        f.close()
    
    def readQids(self, f):
        """Reads the query id file, used typically with label ranking
        
        @param f: lines of the qid file (possibly an open file)
        @type f: iterable object"""
        qids = []
        for line in f:
            qid = line.strip()
            qids.append(qid)
        #Check that at least some queries contain more than one example
        if len(qids) == len(set(qids)):
            raise Exception("Error in the qid file: all the supplied queries consist only of a single example\n")
        self.qids = qids
        
    def mapQids(self):
        """Maps qids to running numbering starting from zero, and partitions
        the training data indices so that each partition corresponds to one
        query"""
        #Used in FileReader, rls_predict
        qid_dict = {}
        folds = {}
        qid_list = []
        counter = 0
        for index, qid in enumerate(self.qids):
            if not qid in qid_dict:
                qid_dict[qid] = counter
                folds[qid] = []
                counter += 1
            qid_list.append(qid_dict[qid])
            folds[qid].append(index)
        final_folds = []
        for f in folds.values():
            final_folds.append(f)
        self.qids = qid_list
        self.folds = final_folds


class DecompositionFile(AbstractReaderOld):
    
    def __init__(self, filename):
        """Loads the singular value decomposition of the data from an npz file
        @param filename: path todecompositions file
        @type filename: string
        """
        f = open(filename, 'rb')
        di = numpy.load(f)
        svals = mat(di['svals'])
        rsvecs = mat(di['rsvecs'])
        if 'lsvecs' in di.files:
            lsvecs = mat(di['lsvecs'])
        else:
            lsvecs = None
        if 'ZMatrix' in di.files:
            ZMatrix = mat(di['lsvecs'])
        else:
            ZMatrix = None
        self.dcompsource = DataSources.DecompositionSource(svals, rsvecs, lsvecs = lsvecs, ZMatrix = ZMatrix)
        f.close()
    
    def toRpool(self, rpool, varname, vartype):
        rpool[varname] = self.dcompsource


class PickleReader(AbstractReaderSimplified):
    
    def __init__(self, filename):
        """ Loads data from a pickled file.
        @param filename: path to file containing pickled data
        @type filename: string
        """
        f = open(filename, 'rb')
        self.data = cPickle.load(f)
        f.close()


class SVMlightFile(AbstractReaderOld):

    def toRpool(self, rpool, varname, vartype):
        if vartype == DataSources.FeatureSource:
            rpool[varname] = self.fs
        elif vartype == DataSources.LABELS_VARIABLE_TYPE:
            rpool[varname] = self.ls
        elif vartype == DataSources.QidSource:
            if not self.qs == None:
                rpool[varname] = self.qs
            else:
                raise Exception('No qids available')
        else:
            raise Exception('Unknown variable type '+vartype)
    
    def __init__(self, filename):
        """ Loads examples from an SVM-light format data file. The
        file contains attributes, one label per example and optionally
        qids.
        @param filename: path to SVM-light file
        @type filename: string
        """
        inst_size, fspace_size, nzero_size = self.getStatistics(filename)
        f = open(filename)
        #some interesting statistics are calculated
        labelcount = None
        linecounter = 0
        feaspace_dim = 0
        nonzeros = 0
        #Features, labels, comments and possibly qids are later returned to caller
        #The indexing, with respect to the instances, is the same in all the lists.
        qids = None
        instances = []
         
        rows = numpy.zeros(nzero_size, dtype=numpy.int32)
        columns = numpy.zeros(nzero_size, dtype=numpy.int32)
        values = numpy.zeros(nzero_size, dtype=numpy.float32)
        
        all_outputs = []
        
        #Each line in the source represents an instance
        for line in f:
            if line[0] == "#" or line.strip() == "":
                continue
            linecounter += 1
            #Empty lines and commented lines are passed over
            if len(line.strip()) == 0 or line[0] == '#':
                pass
            labels, attributes, comment, qid = self.__split_instance_line(line)
            #Either all instances must have a qid, or none of them
            if qid != None:
                if qids == None:
                    if linecounter > 1:
                        raise Exception("Error when reading in SVMLight file: Line %d has a qid, previous lines did not have qids defined" % (linecounter))   
                    else:
                        qids = [qid]
                else:
                    qids.append(qid)
            else:
                if qids != None:
                    raise Exception("Error when reading in SVMLight file: Line %d has no qid, previous lines had qids defined" % (linecounter))
            #We gather the comments
            #Multiple labels are allowed, but each instance must have the
            #same amount of them. Labels must be real numbers.
            labels = labels.split("|")
            if labelcount == None:
                labelcount = len(labels)
            #Check that the number of labels is the same for all instances
            #and that the labels are real valued numbers.
            else:
                if labelcount != len(labels):
                    raise Exception("Error when reading in SVMLight file: Number of labels assigned to instances differs.\n First instance had %d labels whereas instance on line %d has %d labels\n" % (labelcount, linecounter, len(labels)))
            label_list = []
            #We check that the labels are real numbers and gather them
            for label in labels:
                try:
                    label = float(label)
                    label_list.append(label)
                except ValueError:
                    raise Exception("Error when reading in SVMLight file: label %s on line %d not a real number\n" % (label, linecounter))
            all_outputs.append(label_list)
            previous = 0
            features = []
            #Attributes indices must be positive integers in an ascending order,
            #and the values must be real numbers.
            for att_val in attributes:
                if len(att_val.split(":")) != 2:
                    raise Exception("Error when reading in SVMLight file: feature:value pair %s on line %d is not well-formed\n" % (att_val, linecounter))
                index, value = att_val.split(":")
                try:
                    index = int(index)
                    value = float(value)
                    if value != 0.:
                        rows[nonzeros] = index-1
                        columns[nonzeros] = linecounter-1
                        values[nonzeros] = value
                        nonzeros += 1
                except ValueError:
                    raise Exception("Error when reading in SVMLight file: feature:value pair %s on line %d is not well-formed\n" % (att_val, linecounter))
                if not index > previous:
                    raise Exception("Error when reading in SVMLight file: line %d features must be in ascending order\n" % (linecounter))
                previous = index
                if index > feaspace_dim:
                    feaspace_dim = index
        X = sparse.coo_matrix((values,(rows,columns)),(feaspace_dim, linecounter), dtype=float64)
        X = X.tocsc()
        self.fs = DataSources.FeatureSource(X)
        Y = mat(all_outputs)
        self.ls = Y
        if not qids == None:
            self.qs = DataSources.QidSource(qids)
        else:
            self.qs = None
        f.close()
        
    def getStatistics(self, filename):
        """ Loads examples from an SVM-light format data file. The
        file contains attributes, one label per example and optionally
        qids.
        @param filename: path to SVM-light file
        @type filename: string
        """
        f = open(filename)
        #some interesting statistics are calculated
        linecounter = 0
        feaspace_dim = 0
        nonzeros = 0
        #Features, labels, comments and possibly qids are later returned to caller
        #The indexing, with respect to the instances, is the same in all the lists.
        #Each line in the source represents an instance
        #X = sparse.dok_matrix((700, 34815))
        for line in f:
            if line[0] == "#" or line.strip() == "":
                continue
            linecounter += 1
            #Empty lines and commented lines are passed over
            if len(line.strip()) == 0 or line[0] == '#':
                pass
            labels, attributes, comment, qid = self.__split_instance_line(line)
            #Attributes indices must be positive integers in an ascending order,
            #and the values must be real numbers.
            for att_val in attributes:
                if len(att_val.split(":")) != 2:
                    raise Exception("Error when reading in SVMLight file: feature:value pair %s on line %d is not well-formed\n" % (att_val, linecounter))
                index, value = att_val.split(":")
                try:
                    index = int(index)
                    value = float(value)
                    if value != 0.:
                        nonzeros += 1
                except ValueError:
                    raise Exception("Error when reading in SVMLight file: feature:value pair %s on line %d is not well-formed\n" % (att_val, linecounter))
                if index > feaspace_dim:
                    feaspace_dim = index
        f.close()
        return linecounter, feaspace_dim, nonzeros
    
    
    def __split_instance_line(self, line):
        """Splits an instance line, supplied in the similar format as
        that used by SVMLight package, to labels, qid, attributes and comment.
        Labels are the first sequence of characters at the start of line,
        then follows optionally a qid-attribute, then normal attributes,
        and finally # can be used to comment out rest of the line."""
        #Probably unnecessarily complicated piece of code, consider
        #simplifying at some point
        comment = ""
        qid = None
        #The #-character begins the optional comment section of a line
        line = line.strip()
        cstart = line.find("#")
        if cstart != -1:
            comment = line[cstart:]
            line = line[:cstart]
        line = line.split()
        if len(line) == 0:
            raise Exception("Error: empty line in the data file")
        #One or more labels, using separator |, are supposed to be at
        #the beginning of the line
        labels = line.pop(0)
        #Optionally, next may be the qid:value pair which can be used
        #to define comparable subsets when ranking.
        if len(line)>0 and line[0].startswith("qid"):
            qid = line.pop(0)
            qid = qid.split(":")
            if not len(qid) == 2:
                raise Exception("Error: qid-attribute badly formed")
            else:
                qid = qid[1]
        #Rest of the line contains attribute:value pairs
        return labels, line, comment, qid

