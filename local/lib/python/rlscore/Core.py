import DataSources
import FileWriter
from kernel import LinearKernel
from learner import RLS
from measure import SqerrorMeasure
import sys
import kernel
import measure
import learner
import mselection

DEFAULT_MODULES = {'learner':None, 'kernel':'LinearKernel', 'measure':'SqerrorMeasure'}
DEFAULT_PARAMS = {'regparam':'1', 'reggrid':'-5_5', 'bias':'0', 'verbose':'True'}

class Core(object):
    """The high level interface to the RLScore. Allows initializing the
    learning process, training a model according to a chosen model selection
    strategy and writing it to a file"""
    
    def __init__(self, rpool, learner = None, kernel = None, params = {}, mselection = None, measure = None):
        """
        Initializes the high level interface to the RLScore. The default values of the
        learner, kernel, performance measure, model selection strategy, regularization parameter, and verbosity
        are set to RLS, LinearKernel, SqerrorMeasure, None, 1.0, and False, respectively.
        """
        self.resource_pool = rpool
        self.learner = learner
        self.regparam = 1.0
        self.kernel = kernel
        self.verbose = False
        self.model = None
        self.mselection = mselection
        self.measure = measure
        self.resource_pool[DataSources.PERFORMANCE_MEASURE_VARIABLE] = self.measure
        self.resource_pool[DataSources.KFUNCTION_VARIABLE] = self.kernel
        self.setParameters(params)
        
    def run(self):
        #Train, if learner defined
        if self.learner != None:
            if self.verbose:
                print "Learning model"
            self.learnModel()
        #Make predictions, if model and test examples available
        if self.resource_pool.has_key(DataSources.MODEL_VARIABLE) and self.resource_pool.has_key(DataSources.PREDICTION_FEATURES_VARIABLE):
            if self.verbose:
                print "Making predictions on test data"
            self.predict()
        #Measure performance, if predictions, true labels and performance measure available
        if self.measure != None and self.resource_pool.has_key(DataSources.PREDICTED_LABELS_VARIABLE) and self.resource_pool.has_key(DataSources.TEST_LABELS_VARIABLE):
            self.evaluatePerformance()        
    
    def getKernel(self):
        """Returns the kernel function object.
        @return: kernel object
        @rtype: Kernel
        """
        return self.kernel
    
    def getResourcePool(self):
        """Returns the resource pool object.
        @return: resource_pool: resource pool containing all the data necessary for the learning process
        @rtype: dict
        """
        return self.resource_pool
        
    def setParameters(self, params):
        """Sets the parameters of the learner
        @param params: dictionary of parameter:value pairs
        @type dictionary
        """
        self.resource_pool[DataSources.PARAMETERS_VARIABLE] = params
        self.params = params
        if not self.kernel == None:
            self.kernel.setParameters(params)
            if params.has_key('bias'):
                self.kernel.setBias(float(params['bias']))
        if not self.learner == None:
            self.learner.setParameters(params)
        if params.has_key('regparam'):
            self.regparam = float(params["regparam"])
        if not (self.regparam>0):
            raise Exception("Supplied non-positive regularization parameter to Core")
        if params.has_key('verbose'):
            self.verbose = (params["verbose"]=="True")
        else:
            self.verbose = True
        if not self.mselection == None:
            self.mselection.setParameters(params)
        if not self.measure == None:
            self.measure.setParameters(params)
        
    def learnModel(self):
        """Learns the model using the chosen learner, and the chosen
        model selection strategy and performance measure if any.
        """
        #Construction of the decomposition
        if not self.mselection == None:
            self.mselection.setResourcePool(self.resource_pool)
        #A potential source of inefficiency here, if a learner required
        #both the kernel matrix and its decomposition, K would
        #be built twice. No learner currently does this however.
        if not self.resource_pool.has_key(DataSources.KMATRIX_VARIABLE) and DataSources.KMATRIX_VARIABLE in self.learner.requiredResources():
            K = self.kernel.trainKMFromPool(self.resource_pool)
            self.resource_pool[DataSources.KMATRIX_VARIABLE] = K
        if not self.resource_pool.has_key(DataSources.DECOMPOSITION_VARIABLE) and DataSources.DECOMPOSITION_VARIABLE in self.learner.requiredResources():
            svals, rsvecs, U, Z = self.kernel.decompositionFromPool(self.resource_pool)
            dc = DataSources.DecompositionSource(svals, rsvecs, lsvecs = U, ZMatrix = Z)
            self.resource_pool[DataSources.DECOMPOSITION_VARIABLE] = dc
        self.learner.setResourcePool(self.resource_pool)
        self.learner.loadResources()
        model = None
        #If model selection is to be done...
        if self.mselection:
            #Give model selection all that it desires
            self.mselection.loadResources()
            self.mselection.setLearner(self.learner)
            self.mselection.setMeasure(self.measure)
            self.mselection.setVerbosity(self.verbose)
            self.mselection.findBestModel()
            #and we have the most promising model
            model = self.mselection.getBestModel()
        #else we simply train the learner with the supplied parameter value
        else:
            if self.verbose:
                print "Training model with regularization parameter value %f" % self.regparam
            self.learner.solve(self.regparam)
            model = self.learner.getModel()
        self.resource_pool[DataSources.MODEL_VARIABLE] = model
        
    def predict(self):
        model = self.resource_pool[DataSources.MODEL_VARIABLE]
        predictions = model.predict(self.resource_pool)
        self.resource_pool[DataSources.PREDICTED_LABELS_VARIABLE] = predictions
        
    def evaluatePerformance(self):
        correct = self.resource_pool[DataSources.TEST_LABELS_VARIABLE]
        predicted = self.resource_pool[DataSources.PREDICTED_LABELS_VARIABLE]
        q_partition = None
        if self.resource_pool.has_key(DataSources.PREDICTION_QIDS_VARIABLE):
            print "calculating performance as averages over queries"
            qs = self.resource_pool[DataSources.PREDICTION_QIDS_VARIABLE]
            q_partition = qs.folds
        self.measure.checkOutputs(correct)
        performance = self.measure.wrapper(correct, predicted, q_partition)
        measure_name = self.measure.getName()
        for i in range(len(performance)):
            if not performance[i] == None:
                print "Performance for task %d: %f %s" % (i + 1, performance[i], measure_name)
            else:
                print "Performance for task %d: undefined %s" % (i + 1, measure_name)
        self.resource_pool[DataSources.TEST_PERFORMANCE_VARIABLE] = performance
        

def loadCore(modules, parameters, input_file, output_file, file_reader = {}, file_writer = {}):
    """Loads dynamically the modules necessary for learning
    @param modules: learner, kernel, mselection and measure
    @type modules: dictionary of type:module string pairs
    @param parameters: reggrid, regparam, kparam
    @type parameters: dictionary of parameter:value string pairs
    @param input_file: variable-file pairs for input data
    @type input_file: dictionary of variable:file string pairs
    @param output_file: variable-file pairs for output data
    @type output_file: dictionary of variable:file string pairs
    @param file_reader: file-reader pairs for input data
    @type file_reader: dictionary of file:reader string pairs
    @param file_writer: file-writer pairs for input data
    @type file_writer: dictionary of file:writer string pairs
    """
    for key in DEFAULT_MODULES.keys():
        if not key in modules:
            modules[key] = DEFAULT_MODULES[key]
    for key in DEFAULT_PARAMS.keys():
        if not key in parameters:
            parameters[key] = DEFAULT_PARAMS[key]
    file_readerobj = {}
    for filename in file_reader.keys():
        reader = file_reader[filename]
        module = reader.rsplit(".")[0]
        exec "import " + module
        reader = eval(reader+"(filename)")
        file_readerobj[filename] = reader
    variable_type_file = []
    for variable in input_file.keys():
        filename = input_file[variable]
        vartype = DataSources.VARIABLE_TYPES[variable]
        if not file_readerobj.has_key(filename):
            readerclass = DataSources.DEFAULT_READERS[vartype]
            readerobj = readerclass(filename)
            file_readerobj[filename] = readerobj
        variable_type_file.append((variable, vartype, filename))     
    if 'importpath' in parameters:
        paths = parameters['importpath'].split(";")
        for path in paths:
            sys.path.append(path)
    if not modules["learner"] == None:
        exec "import learner." + modules["learner"]
        learner = eval("learner."+modules["learner"]).RLS()
    else:
        learner = None
    exec "import kernel." + modules["kernel"]
    kernel = eval("kernel."+modules["kernel"]).Kernel()
    verbose = (parameters["verbose"]=="True")
    kernel.setVerbosity(verbose)
    #dynamic import of the model selection strategy
    if 'mselection' in modules:
        exec "import mselection." + modules["mselection"]
        mselection = eval("mselection."+modules["mselection"]).ModelSelection()
    else:
        mselection = None
    if 'measure' in modules:
        exec "import measure." + modules["measure"]
        measure = eval("measure."+modules["measure"]).Measure()
    else:
        measure = None
    rpool = {}
    if mselection:
        mselection.setResourcePool(rpool)
    if verbose == True:
        print "Reading input files"
    for variable, vartype, file in variable_type_file:
        reader = file_readerobj[file]
        reader.toRpool(rpool, variable, vartype)
    core = Core(rpool, learner = learner, kernel = kernel, params = parameters, mselection = mselection, measure = measure)
    core.run()
    
    writers = []
    for variable, filename in output_file.iteritems():
        vartype = DataSources.VARIABLE_TYPES[variable]
        if not file_writer.has_key(filename):
            writerclass = DataSources.DEFAULT_WRITERS[vartype]
            writerobj = writerclass()
        else:
            writerclass = file_writer[filename]
            module = writerclass.rsplit(".")[0]
            exec "import " + module
            writerobj = eval(writerclass+"()")
        writerobj.fromRpool(rpool, variable, vartype)
        writers.append((writerobj, filename))
    
    for writerobj, filename in writers:
        writerobj.write(filename)
    
    return core        
        
        
    
