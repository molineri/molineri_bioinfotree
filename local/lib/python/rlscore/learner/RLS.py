
import sys

from numpy import float64, identity, multiply, mat, zeros
import numpy.linalg as la

import AbstractSupervisedLearner

class RLS(AbstractSupervisedLearner.RLS):
    '''RLS learner module for performing regularized least-squares regression or classification.'''
    
   
    def solve(self, regparam):
        """Trains RLS with the given value of the regularization parameter
        
        @param regparam: value of the regularization parameter
        @type regparam: float
        """
        
        if not hasattr(self, "multiplyright"):
            self.multiplyright = self.svecs.T * self.Y
        
            #Eigenvalues of the kernel matrix
            self.evals = multiply(self.svals, self.svals)
        
        self.newevals = 1. / (self.evals + regparam)
        self.regparam = regparam
        if self.U == None:
            #Dual RLS
            self.A = self.svecs * multiply(self.newevals.T, self.multiplyright)
        else:
            #Primal RLS
            bevals = multiply(self.svals, self.newevals)
            self.A = self.U.T * multiply(bevals.T, self.multiplyright)
    
    
    def computeHO(self, indices):
        """Method for computing hold-out predictions for a trained RLS.
        
        @param indices: A list of indices of training examples belonging to the set for which the hold-out predictions are calculated. The list can not be empty.
        @type indices: a list of integers
        @return: Hold-out predictions as a matrix, whose rows are indexed by the training examples and columns by labels.
        @rtype: numpy.matrix
        @raise Exception: when the indices list is empty or if the same index is in the index set more than once.
        """
        
        self.checkHoldOutSet(indices)
        
        I = mat(identity(len(indices)))
        bevals = multiply(self.evals, self.newevals)
        RV = self.svecs[indices]
        right = self.multiplyright - RV.T * self.Y[indices]
        RQY = RV * multiply(bevals.T, right)
        RQRT = RV * multiply(bevals.T, RV.T)
        result = la.inv(I - RQRT) * RQY
        return result
    
    def checkHoldOutSet(self, indices):
        
        if len(indices) == 0:
            raise Exception('Hold-out predictions can not be computed for an empty hold-out set.')
        
        if len(indices) != len(set(indices)):
            raise Exception('Hold-out can have each index only once.')
    
    def computeLOO(self):
        """Compute predictions for leave-one-out cross-validation
        
        @return: LOOCV predictions as a matrix, whose rows are indexed by the training examples and columns by labels.
        @rtype: numpy.matrix
        """
        LOO = mat(zeros((self.size, self.ysize), dtype=float64))
        bevals = multiply(self.evals, self.newevals)
        rightall = multiply(bevals.T, self.multiplyright)
        for i in range(self.size):
            RV = self.svecs[i]
            RVm = multiply(bevals, RV)
            right = rightall - RVm.T * self.Y[i]
            RQY = RV * right
            RQRT = RV * RVm.T
            LOO[i] = (1. / (1. - RQRT)) * RQY
        return LOO
    


