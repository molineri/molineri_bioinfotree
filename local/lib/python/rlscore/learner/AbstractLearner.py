from .. import DataSources


class RLS(object):
    '''RLS learner module.'''
    
    
    def setResourcePool(self, rp):
        """
        Sets the resource pool for the learner.
        
        @param rp: container of resources from which the learner infers the concept to be learned.
        @type rp: dict
        """
        self.resource_pool = rp
    
    def loadResources(self):
        """
        Loads the resources from the previously set ResourcePool object.
        
        @raise Exception: when some of the resources required by the learner is not available in the ResourcePool object.
        """
        
        dc = self.resource_pool[DataSources.DECOMPOSITION_VARIABLE]
        svals = dc.readSingularValues()
        svecs = dc.readRightSingularVectors()
        U = dc.readLeftSingularVectors()
        Z = dc.readZMatrix()
        self.setDecomposition(svals, svecs, U, Z)
    
    
    def requiredResources(self):
        """
        Returns the names of the DataSources corresponding to the resources required by the learner.
        
        @return: a list of class names of the subclasses of the DataSource class.
        @rtype: list
        """
        
        names = []
        names.append(DataSources.DECOMPOSITION_VARIABLE)
        return names
    
    
    def getResourcePool(self):
        """
        Returns the ResourcePool object of the learner.
        
        @return: container of resources from which the learner infers the concept to be learned.
        @rtype: dict
        """
        return self.resource_pool
    
    
    def setDecomposition(self, svals, svecs, U = None, Z = None):
        """
        Sets the decomposition data of the empirical feature representation for RLS.
        
        @param svals: Singular (nonzero) values of the empirical feature representation
        @type svals: numpy.matrix
        @param svecs: Singular vectors of the empirical feature representation
        @type svecs: numpy.matrix
        @param U: Right singular vectors. These can be supplied if primal RLS is used.
        @type U: numpy.matrix
        """
        
        self.svals = svals
        self.svecs = svecs
        self.U = U
        self.Z = Z

    def setParameters(self, parameters):
        """Sets the parameter values of the learner.
        
        @param parameters: parameters for the learner
        @type parameters: string"""
        self.parameters = parameters
    
    
    def supportsModelSelection(self):
        """Returns whether the learner supports model selection.
        
        @return: whether the learner supports model selection
        @rtype: boolean"""
        return False
        
    
    def solve(self, regparam):
        """Trains RLS with the given value of the regularization parameter
        
        @param regparam: value of the regularization parameter
        @type regparam: float
        """
        pass
    
    
    def getA(self):
        """Returns the learned RLS solution. Can be either primal or dual.
        
        @return: primal or dual variables as a matrix, whose rows are indexed by the training examples or features and columns by labels.
        @rtype: numpy.matrix
        """
        return self.A
    
    def getModel(self):
        from .. import Model
        mf = Model.ModelFactory(self.resource_pool)
        return mf.buildModel(self.A, self.resource_pool[DataSources.KFUNCTION_VARIABLE])
    
    
    def predict(self, pdm):
        """Compute predictions for new data points.
        
        @param pdm: either a kernel matrix for the training examples (rows)
            and for the new data points (columns), or a data matrix for the new data points.
        @type pdm: numpy.matrix
        @return: predictions as a matrix, whose rows are indexed by the new data points and columns by labels.
        @rtype: numpy.matrix
        """
        return pdm.T * self.A

