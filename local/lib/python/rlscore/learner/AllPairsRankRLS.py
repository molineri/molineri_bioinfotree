
from numpy import array, eye, float64, identity, multiply, mat, ones, sqrt, sum, zeros
import numpy.linalg as la

import AbstractSupervisedLearner

class RLS(AbstractSupervisedLearner.RLS):
    '''RLS learner module for performing regularized least-squares object ranking.'''
    
    
    def solve(self, regparam):
        
        if not hasattr(self, "multiplyright"):
            
            #Eigenvalues of the kernel matrix
            self.evals = multiply(self.svals, self.svals)
            
            #Temporary variables
            ssvecs = multiply(self.svecs, self.svals)
            J = mat(ones((self.size, 1), dtype=float64))
            
            #These are cached for later use in solve function
            self.lowrankchange = ssvecs.T * J[range(ssvecs.shape[0])]
            self.multipleright = ssvecs.T * (self.size * self.Y - J * (J.T * self.Y))
        
        self.regparam = regparam
        
        #Compute the eigenvalues determined by the given regularization parameter
        neweigvals = 1. / (self.size * self.evals + regparam)
        
        P = self.lowrankchange
        nP = multiply(neweigvals.T, P)
        fastinv = 1. / (-1. + P.T * nP)
        if self.U == None:
            #Dual RLS
            self.A = self.svecs * multiply(1. / self.svals.T, \
                (multiply(neweigvals.T, self.multipleright) \
                - nP * (fastinv * (nP.T * self.multipleright))))
        else:
            #Primal RLS
            self.A = self.U.T * (multiply(neweigvals.T, self.multipleright) \
                - nP * (fastinv * (nP.T * self.multipleright)))
    
    
    def computePairwiseCV(self, pairs, oind=0):
        """Method for computing leave-pair-out predictions for a trained RLS.
        
        @param pairs: A list of index pairs for which the leave-pair-out predictions are calculated.
        @type pairs: A list of pairs of integers.
        @param oind: Index of the label for which the leave-pair-out predictions should be computed.
        @type oind: int.
        @return: Leave-pair-out predictions.
        @rtype: List of two dimensional tuples of floats.
        """
        
        evals, svecs = self.evals, self.svecs
        m = self.size
        
        Y = self.Y
        
        #This is, in the worst case, a cubic operation.
        #If there are multiple outputs,
        #this operation should be common for them all. THIS IS TO BE FIXED!
        def computeG():
            regparam = self.regparam
            G = svecs * multiply(multiply(evals, 1. / ((m - 2.) * evals + regparam)).T, svecs.T)
            return G
        G = computeG()
        
        GDY = (self.size - 2.) * G * Y
        GC = sum(G, axis=1)
        
        CTGC = sum(GC)
        
        allresults = []
        
        CTY = sum(Y, axis=0)[0, oind]
        CTGDY = sum(GDY, axis=0)[0, oind]
        
        sm2 = self.size - 2.
        sqrtsm2 = sqrt(sm2)
        
        #Array is faster to access than matrix
        G = array(G)
        
        #Lists are faster to access than matrices or arrays
        def hack():
            GDY_ = []
            sqrtsm2GDY_ = []
            GC_ = []
            Y_ = []
            BTY_ = []
            Gdiag_ = []
            sm2Gdiag_ = []
            BTGBBTY_ = []
            for i in range(m):
                GDYi = GDY[i, oind]
                GDY_.append(GDYi)
                sqrtsm2GDY_.append(sqrtsm2 * GDYi)
                GC_.append(GC[i, 0])
                Yi = Y[i, oind]
                Y_.append(Yi)
                BTY_.append(sqrtsm2 * Yi)
                Gii = G[i, i]
                Gdiag_.append(Gii)
                sm2Gdiag_.append(sm2 * Gii - 1.)
                BTGBBTY_.append(sm2 * Gii * sqrtsm2 * Yi)
            return GDY_, sqrtsm2GDY_, GC_, Y_, BTY_, Gdiag_, sm2Gdiag_, BTGBBTY_
        GDY_, sqrtsm2GDY_, GC_, Y_, BTY_, Gdiag_, sm2Gdiag_, BTGBBTY_ = hack()
        
        results = []
        
        #This loops through the list of hold-out pairs.
        #Each pair is handled in a constant time.
        def looppairs(results):
            for i, j in pairs:
                
                Gii = Gdiag_[i]
                Gij = G[i, j]
                Gjj = Gdiag_[j]
                
                GCi = GC_[i]
                GCj = GC_[j]
                
                Yi = Y_[i]
                Yj = Y_[j]
                
                GDYi = GDY_[i]
                GDYj = GDY_[j]
                
                BTY0 = CTY - Yi - Yj
                BTY1 = BTY_[i]
                BTY2 = BTY_[j]
                
                GiipGij = Gii + Gij
                GijpGjj = Gij + Gjj
                GCipGCj = GCi + GCj
                
                BTGB00 = GiipGij + GijpGjj + CTGC - GCipGCj - GCipGCj
                BTGB01 = sqrtsm2 * (GCi - GiipGij)
                BTGB02 = sqrtsm2 * (GCj - GijpGjj)
                BTGB12 = sm2 * Gij
                
                BTGLY0 = CTGDY - (GDYi + GDYj + BTGB00 * BTY0 + BTGB01 * BTY1 + BTGB02 * BTY2)
                BTGLY1 = sqrtsm2GDY_[i] - (BTGB01 * BTY0 + BTGBBTY_[i] + BTGB12 * BTY2)
                BTGLY2 = sqrtsm2GDY_[j] - (BTGB02 * BTY0 + BTGB12 * BTY1 + BTGBBTY_[j])
                
                BTGB00m1 = BTGB00 - 1.
                BTGB11m1 = sm2Gdiag_[i]
                BTGB22m1 = sm2Gdiag_[j]
                
                CF00 = BTGB11m1 * BTGB22m1 - BTGB12 * BTGB12
                CF01 = -BTGB01 * BTGB22m1 + BTGB12 * BTGB02
                CF02 = BTGB01 * BTGB12 - BTGB11m1 * BTGB02
                CF11 = BTGB00m1 * BTGB22m1 - BTGB02 * BTGB02
                CF12 = -BTGB00m1 * BTGB12 + BTGB01 * BTGB02
                CF22 = BTGB00m1 * BTGB11m1 - BTGB01 * BTGB01
                
                invdeter = 1. / (BTGB00m1 * CF00 + BTGB01 * CF01 + BTGB02 * CF02)
                
                b0 = invdeter * (CF00 * BTGLY0 + CF01 * BTGLY1 + CF02 * BTGLY2) + BTY0
                b1 = invdeter * (CF01 * BTGLY0 + CF11 * BTGLY1 + CF12 * BTGLY2) + BTY1
                b2 = invdeter * (CF02 * BTGLY0 + CF12 * BTGLY1 + CF22 * BTGLY2) + BTY2
                
                t1 = -b0 + sqrtsm2 * b1
                t2 = -b0 + sqrtsm2 * b2
                F0 = GDYi - (Gii * t1 + Gij * t2 + GCi * b0)
                F1 = GDYj - (Gij * t1 + Gjj * t2 + GCj * b0)
                
                results.append((F0, F1))
        looppairs(results)
        #allresults.append(results)
        # if Y.shape[1] > 1:
            # allresultsvec = []
            # for pairind in range(len(pairs)):
                # F0 = mat(zeros((1, Y.shape[1]), dtype = float64))
                # F1 = mat(zeros((1, Y.shape[1]), dtype = float64))
                # for oind in range(Y.shape[1]):
                    # F0[0, oind] = allresults[oind][pairind][0]
                    # F1[0, oind] = allresults[oind][pairind][1]
                # allresultsvec.append((F0,F1))
            # return allresultsvec
        # else:
            # return allresults[0]
        return results
    
    
    def computeHO(self, indices):
        
        self.checkHoldOutSet(indices)
        
        Y = self.Y
        m = self.size
        
        evals, V = self.evals, self.svecs
        
        #results = []
        
        C = mat(zeros((self.size, 3), dtype=float64))
        onevec = mat(ones((self.size, 1), dtype=float64))
        for i in range(self.size):
            C[i, 0] = 1.
        
        
        VTY = V.T * Y
        VTC = V.T * onevec
        CTY = onevec.T * Y
        
        holen = len(indices)
        
        newevals = multiply(evals, 1. / ((m - holen) * evals + self.regparam))
        
        R = mat(zeros((holen, holen + 1), dtype=float64))
        for i in range(len(indices)):
            R[i, 0] = -1.
            R[i, i + 1] = sqrt(self.size - float(holen))
        
        Vho = V[indices]
        Vhov = multiply(Vho, newevals)
        Ghoho = Vhov * Vho.T
        GCho = Vhov * VTC
        GBho = Ghoho * R
        for i in range(len(indices)):
            GBho[i, 0] += GCho[i, 0]
        
        CTGC = multiply(VTC.T, newevals) * VTC
        RTGCho = R.T * GCho
        
        BTGB = R.T * Ghoho * R
        for i in range(len(indices) + 1):
            BTGB[i, 0] += RTGCho[i, 0]
            BTGB[0, i] += RTGCho[i, 0]
        BTGB[0, 0] += CTGC[0, 0]
        
        BTY = R.T * Y[indices]
        #BTY[0, 0] += CTY[0, 0]
        BTY[0] = BTY[0] + CTY[0]
        
        GDYho = Vhov * (self.size - holen) * VTY
        GLYho = GDYho - GBho * BTY
        
        CTGDY = multiply(VTC.T, newevals) * (self.size - holen) * VTY
        BTGLY = R.T * GDYho - BTGB * BTY
        #BTGLY[0, 0] += CTGDY[0, 0]
        BTGLY[0] = BTGLY[0] + CTGDY[0]
        
        F = GLYho - GBho * la.inv(-mat(eye(holen + 1)) + BTGB) * BTGLY
        
        #results.append(F)
        #return results
        return F
        
    def computeLOO(self):
        
        LOO = mat(zeros((self.size, self.ysize), dtype=float64))
        for i in range(self.size):
            LOO[i,:] = self.computeHO([i])
        return LOO
    
    def reference(self, pairs):
        
        evals, evecs = self.evals, self.svecs
        m = self.size
        Y = self.Y
        m = self.size
        
        
        results = []
        
        D = mat(zeros((self.size, 1), dtype=float64))
        C = mat(zeros((self.size, 3), dtype=float64))
        for i in range(self.size):
            D[i, 0] = self.size - 2.
            C[i, 0] = 1.
        lamb = self.regparam
        
        G = evecs * multiply(multiply(evals, 1. / ((m - 2.) * evals + lamb)).T, evecs.T)
        
        
        DY = multiply(D, Y)
        GDY = G * DY
        GC = G * C
        CTG = GC.T
        CT = C.T
        CTGC = CT * GC
        CTY = CT * Y
        GCCTY = (G * C) * (C.T * Y)
        CTGDY = CT * GDY
        
        minusI3 = -mat(eye(3))
        for i, j in pairs:
            hoinds = [i, j]
            
            R = mat(zeros((2, 3), dtype=float64))
            R[0, 0] = -1.
            R[1, 0] = -1.
            R[0, 1] = sqrt(self.size - 2.)
            R[1, 2] = sqrt(self.size - 2.)
            RT = R.T
            
            GBho = GC[hoinds] + G[ix_(hoinds, hoinds)] * R
            
            BTGB = CTGC \
                + R.T * GC[hoinds] \
                + CTG[:, hoinds] * R \
                + R.T * G[ix_(hoinds, hoinds)] * R
            
            BTY = CTY + R.T * Y[hoinds]
            
            GLYho = GDY[hoinds] - GBho * BTY
            BTGLY = CTGDY + R.T * GDY[hoinds] - BTGB * BTY
            
            F = GLYho - GBho * la.inv(minusI3 + BTGB) * BTGLY
            
            results.append(F)
        return results


