
import sys

from numpy import *
import numpy.linalg as la
import scipy
import scipy.sparse as sp

import AbstractSupervisedLearner
from .. import DataSources

class RLS(AbstractSupervisedLearner.RLS):
    
    def loadResources(self):
        """
        Loads the resources from the previously set resource pool.
        
        @raise Exception: when some of the resources required by the learner is not available in the ResourcePool object.
        """
        Y = self.resource_pool[DataSources.TRAIN_LABELS_VARIABLE]
        self.setLabels(Y)
        fs = self.resource_pool[DataSources.TRAIN_FEATURES_VARIABLE]
        X = fs.readFeatures()
        self.setDataMatrix(X)
        if self.resource_pool[DataSources.PARAMETERS_VARIABLE].has_key('bias'):
            self.bias = float(self.resource_pool[DataSources.PARAMETERS_VARIABLE]['bias'])
        else:
            self.bias = 0.
        if self.resource_pool.has_key(DataSources.PERFORMANCE_MEASURE_VARIABLE):
            self.measure = self.resource_pool[DataSources.PERFORMANCE_MEASURE_VARIABLE]
        else:
            self.measure = None
    
    
    def setLabels(self, Y):
        """
        Sets the label data for RLS.
        
        @param Y: Labels of the training examples. Can be either a single-column matrix (single output) or a multi-column matrix (multiple output).
        @type Y: numpy.matrix
        """
        
        self.Y = Y
        
        #Number of training examples
        self.size = Y.shape[0]
        
        if not Y.shape[1] == 1:
            raise Exception('GreedyRLS currently supports only one output at a time. The output matrix is now of shape ' + str(Y.shape) + '.')
    
    
    def setDataMatrix(self, X):
        """
        Sets the label data for RLS.
        
        @param X: Features of the training examples.
        @type X: scipy sparse matrix
        """
        if isinstance(X, scipy.sparse.base.spmatrix):
            self.X = X.todense()
        else:
            self.X = X
    
    
    def requiredResources(self):
        """
        Returns the names of the DataSources corresponding to the resources required by the learner.
        
        @return: a list of class names of the subclasses of the DataSource class.
        @rtype: list
        """
        
        names = []
        names.append(DataSources.TRAIN_FEATURES_VARIABLE)
        names.append(DataSources.TRAIN_LABELS_VARIABLE)
        names.append(DataSources.PARAMETERS_VARIABLE)
        return names
    
    
    def supportsModelSelection(self):
        return False
    
    
    def solve(self, regparam):
        
        #The current version works only with the squared error measure
        self.measure = None
        
        self.solve_new(regparam)
        #self.solve_bu(regparam)
    
    
    def solve_new(self, regparam):
        """Trains RLS with the given value of the regularization parameter
        
        @param regparam: value of the regularization parameter
        @type regparam: float
        """
        
        self.regparam = regparam
        X = self.X
        Y = self.Y
        
        if not hasattr(self, "bias"):
            self.bias = 0.
        
        tsize = self.size
        fsize = X.shape[0]
        assert X.shape[1] == tsize
        
        rp = regparam
        rpinv = 1. / rp
        
        if not self.parameters.has_key('subsetsize'):
            raise Exception("Parameter 'subsetsize' must be given.")
        desiredfcount = int(self.parameters['subsetsize'])
        if not fsize >= desiredfcount:
            raise Exception('The overall number of features ' + str(fsize) + ' is smaller than the desired number ' + str(desiredfcount) + ' of features to be selected.')
        
        
        #Biaz
        cv = sqrt(self.bias)*mat(ones((1, tsize)))
        ca = rpinv * (1. / (1. + cv * rpinv * cv.T)) * (cv * rpinv)
        
        
        A = rpinv * Y - cv.T * rpinv * (1. / (1. + cv * rpinv * cv.T)) * (cv * rpinv * Y)
        
        GXT = cv.T * ((rpinv * (1. / (1. + cv * rpinv * cv.T)) * (cv * rpinv)) * X.T)
        tempmatrix = rpinv * X.T
        subtract(tempmatrix, GXT, GXT)
        
        diagG = []
        for i in range(tsize):
            diagGi = rpinv - cv.T[i, 0] * ca[0, i]
            diagG.append(diagGi)
        diagG = mat(diagG).T
        
        self.selected = []
        self.performances = []
        currentfcount = 0
        
        while currentfcount < desiredfcount:
            
            multiply(X.T, GXT, tempmatrix)
            XGXTdiag = sum(tempmatrix, axis = 0)
            
            XGXTdiag = 1. / (1. + XGXTdiag)
            multiply(GXT, XGXTdiag, tempmatrix)
            
            tempvec1 = multiply((X * A).T, XGXTdiag)
            temp2 = A - multiply(GXT, tempvec1)
            
            multiply(tempmatrix, GXT, tempmatrix)
            subtract(diagG, tempmatrix, tempmatrix)
            divide(1, tempmatrix, tempmatrix)
            multiply(tempmatrix, temp2, tempmatrix)
            
            
            if not self.measure == None:
                subtract(Y, tempmatrix, tempmatrix)
                multiply(temp2, 0, temp2)
                add(temp2, Y, temp2)
                looperf = self.measure.multiTaskPerformance(temp2, tempmatrix)
                looperf = mat(looperf)
                if self.measure.isErrorMeasure():
                    looperf[0, self.selected] = float('inf')
                    bestcind = argmin(looperf)
                    self.bestlooperf = amin(looperf)
                else:
                    looperf[0, self.selected] = - float('inf')
                    bestcind = argmax(looperf)
                    self.bestlooperf = amax(looperf)
            else:
                multiply(tempmatrix, tempmatrix, tempmatrix)
                looperf = sum(tempmatrix, axis = 0)
                looperf[0, self.selected] = float('inf')
                bestcind = argmin(looperf)
                self.bestlooperf = amin(looperf)
            self.performances.append(self.bestlooperf)
            cv = X[bestcind]
            GXT_bci = GXT[:, bestcind]
            ca = GXT_bci * (1. / (1. + cv * GXT_bci))
            A = A - ca * (cv * A)
            diagG = diagG - multiply(ca, GXT_bci)
            tempmatrix = ca * (cv * GXT)
            subtract(GXT, tempmatrix, GXT)
            self.selected.append(bestcind)
            currentfcount += 1
        bias_slice = sqrt(self.bias)*mat(ones((1,X.shape[1]),dtype=float64))
        X = vstack([X,bias_slice])
        selected_plus_bias = self.selected+[fsize]
        cutdiag = sp.lil_matrix((fsize+1, currentfcount + 1))
        for ci, col in zip(selected_plus_bias, range(currentfcount + 1)):
            cutdiag[ci, col] = 1.
        self.A = cutdiag * (X[selected_plus_bias]*A)
        self.resource_pool[DataSources.SELECTED_FEATURES_VARIABLE] = self.selected
        self.resource_pool[DataSources.GREEDYRLS_LOO_PERFORMANCES_VARIABLE] = self.performances
    
    
    def solve_bu(self, regparam):
        """Trains RLS with the given value of the regularization parameter
        
        @param regparam: value of the regularization parameter
        @type regparam: float
        """
        
        self.regparam = regparam
        X = self.X
        Y = self.Y
        
        if not hasattr(self, "bias"):
            self.bias = 0.
        
        tsize = self.size
        fsize = X.shape[0]
        assert X.shape[1] == tsize
        
        rp = regparam
        rpinv = 1. / rp
        
        desiredfcount = int(self.parameters)
        
        
        #Biaz
        cv = sqrt(self.bias)*mat(ones((1, tsize)))
        ca = rpinv * (1. / (1. + cv * rpinv * cv.T)) * (cv * rpinv)
        
        
        A = rpinv * Y - cv.T * rpinv * (1. / (1. + cv * rpinv * cv.T)) * (cv * rpinv * Y)
        
        XT = X.T
        GXT = rpinv * XT - cv.T * rpinv * (1. / (1. + cv * rpinv * cv.T)) * ((cv * rpinv) * XT)
        diagG = []
        for i in range(tsize):
            diagGi = rpinv - cv.T[i, 0] * ca[0, i]
            diagG.append(diagGi)
        diagG = mat(diagG).T
        
        listX = []
        for ci in range(fsize):
            listX.append(X[ci])
        
        self.selected = []
        
        currentfcount = 0
        self.performances = []
        while currentfcount < desiredfcount:
            
            if not self.measure == None:
                bestlooperf = None
            else:
                bestlooperf = 9999999999.
            
            for ci in range(fsize):
                if ci in self.selected: continue
                cv = listX[ci]
                GXT_ci = GXT[:, ci]
                ca = GXT_ci * (1. / (1. + cv * GXT_ci))
                updA = A - ca * (cv * A)
                invupddiagG = 1. / (diagG - multiply(ca, GXT_ci))
                
                if not self.measure == None:
                    loopred = Y - multiply(invupddiagG, updA)
                    looperf = self.measure.multiOutputPerformance(Y, loopred)
                    if bestlooperf == None:
                        bestlooperf = looperf
                        bestcind = ci
                    if self.measure.comparePerformances(looperf, bestlooperf) > 0:
                        bestcind = ci
                        bestlooperf = looperf
                else:
                    #This default squared performance is a bit faster to compute than the one loaded separately.
                    loodiff = multiply(invupddiagG, updA)
                    looperf = (loodiff.T * loodiff)[0, 0]
                    if looperf < bestlooperf:
                        bestcind = ci
                        bestlooperf = looperf
            self.bestlooperf = bestlooperf
            self.performances.append(bestlooperf)
            cv = listX[bestcind]
            GXT_bci = GXT[:, bestcind]
            ca = GXT_bci * (1. / (1. + cv * GXT_bci))
            A = A - ca * (cv * A)
            diagG = diagG - multiply(ca, GXT_bci)
            GXT = GXT - ca * (cv * GXT)
            self.selected.append(bestcind)
            print self.selected
            currentfcount += 1
        bias_slice = sqrt(self.bias)*mat(ones((1,X.shape[1]),dtype=float64))
        X_biased = vstack([X,bias_slice])
        selected_plus_bias = self.selected+[fsize]
        self.A = mat(eye(fsize+1))[:,selected_plus_bias]*(X_biased[selected_plus_bias]*A)
        self.resource_pool[DataSources.SELECTED_FEATURES_VARIABLE] = self.selected
