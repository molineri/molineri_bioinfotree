
from random import *

from numpy import *
import numpy.linalg as la
import numpy.random as mlab

import AbstractLearner
from .. import DataSources

class RLS(AbstractLearner.RLS):
    
    
    def loadResources(self):
        AbstractLearner.RLS.loadResources(self)
        if self.resource_pool.has_key(DataSources.TRAIN_LABELS_VARIABLE):
            Y_orig = self.resource_pool[DataSources.TRAIN_LABELS_VARIABLE]
        else:
            size = self.svecs.shape[0]
            ysize = self.labelcount
            if self.labelcount == None: self.labelcount = 2
            Y_orig = RandomLabelSource(size, ysize).readLabels()
        self.Y = Y_orig.copy()
        self.size = self.Y.shape[0]
        self.labelcount = self.Y.shape[1]
        self.classvec = - mat(ones((self.size, 1), dtype = int32))
        self.classcounts = mat(zeros((self.labelcount, 1), dtype = int32))
        for i in range(self.size):
            clazzind = 0
            largestlabel = self.Y[i, 0]
            for j in range(self.labelcount):
                if self.Y[i, j] > largestlabel:
                    largestlabel = self.Y[i, j]
                    clazzind = j
            self.classvec[i] = clazzind
            self.classcounts[clazzind] = self.classcounts[clazzind] + 1
        
        self.svecs_list = []
        for i in range(self.size):
            self.svecs_list.append(self.svecs[i].T)
        
    
    '''    
    def __init__(self, svals, svecs, U = None):
        """
        svals:    Singular (nonzero) values of the data matrix
        svecs:    Singular vectors of the data matrix
        U:    Right singular vectors. These can be supplied if primal RLS is used.
        """
        
        self.rand = Random()
        self.rand.seed(100)
        
        #Number of training examples
        self.size = svecs.shape[0]
        
        #Eigenvalues of the kernel matrix
        self.evals = multiply(svals, svals)
        
        self.svals = svals
        self.svecs = svecs
        self.U = U
        
        self.svecs_list = []
        for i in range(self.size):
            self.svecs_list.append(self.svecs[i].T)
    '''
    
    
    #def setParameters(self, regparam, numofclasses, constraint = 0):
    def setParameters(self, parameters):
        
        
        #self.constraint = constraint
        self.constraint = 0
        #self.labelcount = numofclasses
        if not parameters.has_key('number_of_clusters'):
            raise Exception("Parameter 'number_of_clusters' must be given.")
        self.labelcount = int(parameters['number_of_clusters'])
    
    
    def solve(self, regparam):
        self.regparam = regparam
        
        #Some counters for bookkeeping
        self.stepcounter = 0
        self.flipcounter = 0
        self.nochangecounter = 0
        
        '''
        """Initialize the label vector randomly."""
        self.Y = - mat(ones((self.size,self.labelcount), dtype = float64))
        self.classvec = - mat(ones((self.size,1), dtype = int32))
        allinds = set(range(self.size))
        self.classcounts = mat(zeros((self.labelcount,1), dtype = int32))
        for i in range(self.labelcount-1):
            inds = self.rand.sample(allinds, self.size / self.labelcount)
            allinds = allinds - set(inds)
            for ind in inds:
                self.Y[ind, i] = 1.
                self.classvec[ind, 0] = i
                self.classcounts[i,0] += 1
        for ind in allinds:
            self.Y[ind, self.labelcount - 1] = 1.
            self.classvec[ind, 0] = self.labelcount - 1
            self.classcounts[self.labelcount - 1,0] += 1
        '''
        
        #Cached results
        self.evals = multiply(self.svals, self.svals)
        self.newevals = 1. / (self.evals + regparam)
        newevalslamtilde = multiply(self.evals, self.newevals)
        A1 = multiply(newevalslamtilde, newevalslamtilde)
        A2 = - 2 * newevalslamtilde
        A3 = self.regparam * multiply(newevalslamtilde, self.newevals)
        self.D = A1 + A2 + A3
        VTY = self.svecs.T * self.Y
        
        #Using lists in order to avoid unnecessary matrix slicings
        self.VTY_list = []
        self.classFitnessList = []
        for i in range(self.labelcount):
            VTY_i = VTY[:,i]
            self.VTY_list.append(VTY_i)
            fitness_i = self.size + VTY_i.T * multiply(self.D.T, VTY_i)
            self.classFitnessList.append(fitness_i)
        
        self.updateA()
        
        converged = False
        print self.classcounts.T
        while True:
            
            converged = self.roundRobin()
            print self.classcounts.T
            if converged: break
        self.resource_pool[DataSources.PREDICTED_CLUSTERS_FOR_TRAINING_DATA_VARIABLE] = self.Y
    
    def computeGlobalFitness(self):
        fitness = 0.
        for classind in range(self.labelcount):
            fitness += self.classFitnessList[classind]
        return fitness
    
    
    def step(self, flipindex):
        """Perform one iteration step by checking whether flipping the label indexed by flipindex improves the current state. If it does, then the label is flipped. Otherwise, the the current label vector is left unchanged.
        flipindex:    the index of the label to be flipped.
        returns:      True, if the label is flipped and False otherwise."""
        y = self.Y[flipindex, 0]
        currentclassind = self.classvec[flipindex, 0]
        
        VTY_old_currentclass = self.VTY_list[currentclassind]
        fitness_old_currentclass = self.classFitnessList[currentclassind]
        VTY_new_currentclass = VTY_old_currentclass - 2 * self.svecs_list[flipindex]
        fitness_new_currentclass = self.size + VTY_new_currentclass.T * multiply(self.D.T, VTY_new_currentclass)
        fitnessdiff_currentclass = fitness_new_currentclass - fitness_old_currentclass
        
        changed = False
        
        bestclassind = None
        VTY_new_bestclass = None
        bestfitnessdiff = 10.
        fitness_new_bestclass = None
        
        for newclassind in range(self.labelcount):
            
            if newclassind == currentclassind: continue
            
            VTY_old_newclass = self.VTY_list[newclassind]
            VTY_new_newclass = VTY_old_newclass + 2 * self.svecs_list[flipindex]
            fitness_old_newclass = self.classFitnessList[newclassind]
            fitness_new_newclass = self.size + VTY_new_newclass.T * multiply(self.D.T, VTY_new_newclass)
            fitnessdiff_newclass = fitness_new_newclass - fitness_old_newclass
            
            fitnessdiff = fitnessdiff_currentclass + fitnessdiff_newclass
            
            if fitnessdiff < 0 and fitnessdiff < bestfitnessdiff:
                bestfitnessdiff = fitnessdiff
                bestclassind = newclassind
                VTY_new_bestclass = VTY_new_newclass
                fitness_new_bestclass = fitness_new_newclass
        
        if bestclassind != None:
            if self.classcounts[currentclassind,0] > self.constraint:
                
                self.Y[flipindex, currentclassind] = -1.
                self.Y[flipindex, bestclassind] = 1.
                self.classcounts[currentclassind,0] -= 1
                self.classcounts[bestclassind,0] += 1
                self.classvec[flipindex, 0] = bestclassind
                
                self.VTY_list[currentclassind] = VTY_new_currentclass
                self.VTY_list[bestclassind] = VTY_new_bestclass
                self.classFitnessList[currentclassind] = fitness_new_currentclass
                self.classFitnessList[bestclassind] = fitness_new_bestclass
                
                changed = True
        
        return changed
    
    
    def stepLOO(self, flipindex):
        """Perform one iteration step by checking whether flipping the label indexed by flipindex improves the current state. If it does, then the label is flipped. Otherwise, the the current label vector is left unchanged.
        flipindex:    the index of the label to be flipped.
        returns:      True, if the label is flipped and False otherwise."""
        y = self.Y[flipindex, 0]
        currentclassind = self.classvec[flipindex, 0]
        
        VTY_old_currentclass = self.VTY_list[currentclassind]
        #fitness_old_currentclass = self.classFitnessList[currentclassind]
        VTY_new_currentclass = VTY_old_currentclass - 2 * self.svecs_list[flipindex]
        #fitness_new_currentclass = self.size + VTY_new_currentclass.T * multiply(self.D.T, VTY_new_currentclass)
        #fitnessdiff_currentclass = fitness_new_currentclass - fitness_old_currentclass
            
        bevals = multiply(self.evals, self.newevals)
        RV = self.svecs_list[flipindex]
        right = VTY_old_currentclass - RV * (-1.)
        #print RV.shape, bevals.shape, right.shape
        RQY = RV.T * multiply(bevals.T, right)
        RQRT = RV.T * multiply(bevals.T, RV)
        result = 1. / (1. - RQRT) * RQY
        fitnessdiff_currentclass = (-1. - result) ** 2 - (1. - result) ** 2
        
        changed = False
        
        bestclassind = None
        VTY_new_bestclass = None
        bestfitnessdiff = 10.
        fitness_new_bestclass = None
        
        for newclassind in range(self.labelcount):
            
            if newclassind == currentclassind: continue
            
            VTY_old_newclass = self.VTY_list[newclassind]
            VTY_new_newclass = VTY_old_newclass + 2 * self.svecs_list[flipindex]
            
            right = VTY_old_newclass - RV * (-1.)
            RQY = RV.T * multiply(bevals.T, right)
            RQRT = RV.T * multiply(bevals.T, RV)
            result = 1. / (1. - RQRT) * RQY
            
            #fitness_old_newclass = self.classFitnessList[newclassind]
            #fitness_new_newclass = self.size + VTY_new_newclass.T * multiply(self.D.T, VTY_new_newclass)
            fitnessdiff_newclass = (1. - result) ** 2 - (-1. - result) ** 2
            
            fitnessdiff = fitnessdiff_currentclass + fitnessdiff_newclass
            
            if fitnessdiff < 0 and fitnessdiff < bestfitnessdiff:
                bestfitnessdiff = fitnessdiff
                bestclassind = newclassind
                VTY_new_bestclass = VTY_new_newclass
                #fitness_new_bestclass = fitness_new_newclass
        
        if bestclassind != None:
            if self.classcounts[currentclassind,0] > self.constraint:
                
                self.Y[flipindex, currentclassind] = -1.
                self.Y[flipindex, bestclassind] = 1.
                self.classcounts[currentclassind,0] -= 1
                self.classcounts[bestclassind,0] += 1
                self.classvec[flipindex, 0] = bestclassind
                
                self.VTY_list[currentclassind] = VTY_new_currentclass
                self.VTY_list[bestclassind] = VTY_new_bestclass
                #self.classFitnessList[currentclassind] = fitness_new_currentclass
                #self.classFitnessList[bestclassind] = fitness_new_bestclass
                
                changed = True
        
        return changed
    
    
    def updateA(self):
        VTY = mat(zeros((self.svecs.shape[1],self.labelcount), dtype = float64))
        for i in range(self.labelcount):
            VTY[:,i] = self.VTY_list[i]
        if self.U == None:
            self.A = self.svecs * multiply(self.newevals.T, VTY)
        else:
            bevals = multiply(self.svals, self.newevals)
            self.A = self.U.T * multiply(bevals.T, VTY)
    
    
    def roundRobin(self, LOO = False):
        
        localstepcounter = 0
        
        converged = False
        
        flipindex = self.size - 1
        for flipindex in range(self.size):
            if LOO:
                flipped = self.stepLOO(flipindex)
            else:
                flipped = self.step(flipindex)
            self.stepcounter += 1
            localstepcounter += 1
            if flipped:
                self.flipcounter += 1
                self.nochangecounter = 0
            else:
                self.nochangecounter += 1
            if self.nochangecounter >= self.size:
                converged = True
                break
        
        self.updateA()
        return converged
    
    
    def getA(self):
        """Return coefficients"""
        return self.A
    
    
    def predict(self, pdm):
        """Compute predictions for new data points.
        pdm:    either a kernel matrix for the training examples (rows)
            and for the new data points (columns),
            or a data matrix for the new data points.
        """
        return pdm.T * self.A
    



class RandomLabelSource(object):
    
    def __init__(self, size, labelcount):
        self.rand = Random()
        #self.rand.seed(100)
        self.Y = - mat(ones((size, labelcount), dtype = float64))
        self.classvec = - mat(ones((size, 1), dtype = int32))
        allinds = set(range(size))
        self.classcounts = mat(zeros((labelcount, 1), dtype = int32))
        for i in range(labelcount-1):
            inds = self.rand.sample(allinds, size / labelcount) #sampling without replacement
            allinds = allinds - set(inds)
            for ind in inds:
                self.Y[ind, i] = 1.
                self.classvec[ind, 0] = i
                self.classcounts[i, 0] += 1
        for ind in allinds:
            self.Y[ind, labelcount - 1] = 1.
            self.classvec[ind, 0] = labelcount - 1
            self.classcounts[labelcount - 1, 0] += 1
    
    def readLabels(self):
        return self.Y

