from numpy import arange, float64, identity, multiply, mat, ones, sum, zeros
import numpy.linalg as la
from scipy.sparse import lil_matrix
import scipy.sparse
from .. import DataSources
from .. import Decompositions
import AbstractSupervisedLearner

class RLS(AbstractSupervisedLearner.RLS):
    '''RLS learner module for performing regularized least-squares label ranking.'''
    
    
    def loadResources(self):
        AbstractSupervisedLearner.RLS.loadResources(self)
        qsource = self.getResourcePool()[DataSources.TRAIN_QIDS_VARIABLE]
        qids = qsource.readQids()
        if not self.getResourcePool().has_key(DataSources.CVFOLDS_VARIABLE):
            self.getResourcePool()[DataSources.CVFOLDS_VARIABLE] = qsource
        self.setQids(qids)
    
    
    def requiredResources(self):
        names = []
        names.append(DataSources.DECOMPOSITION_VARIABLE)
        names.append(DataSources.TRAIN_LABELS_VARIABLE)
        names.append(DataSources.TRAIN_QIDS_VARIABLE)
        return names
    
    
    def setQids(self, qids):
        """Sets the qid parameters of the training examples. The list must have as many qids as there are training examples.
        
        @param qids: A list of qid parameters.
        @type qids: List of integers."""
        
        if len(qids) != self.size:
            qc = str(len(qids))
            ss = str(self.size)
            raise Exception('The number ' + ss + ' of training feature vectors is different from the number ' + qc + " of training qids.")
        
        self.qidlist = qids
        
        self.qidmap = {}
        for i in range(len(qids)):
            qid = qids[i]
            if self.qidmap.has_key(qid):
                sameqids = self.qidmap[qid]
                sameqids.append(i)
            else:
                self.qidmap[qid] = [i]
    
    
    def solve(self, regparam):
        
        if not hasattr(self, "D"):
            qidlist = self.qidlist
            objcount = max(qidlist) + 1
            
            labelcounts = mat(zeros((1, objcount)))
            P = scipy.sparse.coo_matrix((ones(self.size), (arange(0, self.size), qidlist)), shape=(self.size,objcount))
            P_csc = P.tocsc()
            P_csr = P.tocsr()
            for i in range(self.size):
                qid = qidlist[i]
                labelcounts[0, qid] = labelcounts[0, qid] + 1
            D = mat(zeros((1, self.size), dtype=float64))
            for i in range(self.size):
                qid = qidlist[i]
                D[0, i] = labelcounts[0, qid]
            
            
            #Eigenvalues of the kernel matrix
            evals = multiply(self.svals, self.svals)
            
            #Temporary variables
            ssvecs = multiply(self.svecs, self.svals)
            
            #These are cached for later use in solve and computeHO functions
            ssvecsTLssvecs = (multiply(ssvecs.T, D) - (ssvecs.T * P_csc) * P_csr.T) * ssvecs
            LRsvals, LRevecs = Decompositions.decomposeKernelMatrix(ssvecsTLssvecs)
            LRevals = multiply(LRsvals, LRsvals)
            LY = multiply(D.T, self.Y) - P_csr * (P_csc.T * self.Y)
            self.multipleright = LRevecs.T * (ssvecs.T * LY)
            self.multipleleft = ssvecs * LRevecs
            self.LRevals = LRevals
            self.LRevecs = LRevecs
            self.D = D
        
        
        self.regparam = regparam
        
        #Compute the eigenvalues determined by the given regularization parameter
        self.neweigvals = 1. / (self.LRevals + regparam)
        
        if self.U == None:
            #Dual RLS
            self.A = self.svecs * multiply(1. / self.svals.T, (self.LRevecs * multiply(self.neweigvals.T, self.multipleright)))
        else:
            #Primal RLS
            self.A = self.U.T * (self.LRevecs * multiply(self.neweigvals.T, self.multipleright))
    
    
    def computeHO(self, indices):
        """Method for computing hold-out predictions for a trained RLS. All examples in the hold-out set must have the same qid and all examples in the whole training set having the qid must belong to the hold out set.
        
        @param indices: A list of indices of training examples belonging to the set for which the hold-out predictions are calculated. The list can not be empty.
        @type indices: a list of integers
        @return: Hold-out predictions as a matrix, whose rows are indexed by the training examples and columns by labels.
        @rtype: numpy.matrix
        @raise Exception: when the indices list is empty, or if the same index is in the index set more than once, or if the qid assumptions are violated.
        """
        
        self.checkHoldOutSet(indices)
        
        hoqid = self.qidlist[indices[0]]
        for ind in indices:
            if not hoqid == self.qidlist[ind]:
                raise Exception('All examples in the hold-out set must have the same qid.')
        
        if not len(self.qidmap[hoqid]) == len(indices):
            raise Exception('All examples in the whole training set having the same qid as the examples in the hold-out set must belong to the hold out set.')
        
        indlen = len(indices)
        Qleft = self.multipleleft[indices]
        Qho = Qleft * multiply(self.neweigvals.T, Qleft.T)
        Pho = mat(ones((len(indices),1)))
        Yho = self.Y[indices]
        Dho = self.D[:, indices]
        LhoYho = multiply(Dho.T, Yho) - Pho * (Pho.T * Yho)
        RQY = Qleft * multiply(self.neweigvals.T, self.multipleright) - Qho * LhoYho
        RQRTLho = multiply(Qho, Dho) - (Qho * Pho) * Pho.T
        I = mat(identity(indlen))
        return la.inv(I - RQRTLho) * RQY
        #return RQY - RQRTLho * la.inv(-I + RQRTLho) * RQY
    
    
    def checkHoldOutSet(self, indices):
        
        if len(indices) == 0:
            raise Exception('Hold-out predictions can not be computed for an empty hold-out set.')
        
        if len(indices) != len(set(indices)):
            raise Exception('Hold-out can have each index only once.')
    
