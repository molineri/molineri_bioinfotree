


import DataSources
from kernel import LinearKernel
from learner import RLS

def constructRLS(features, labels, kernel = LinearKernel.Kernel(), rls = RLS.RLS()):
    
    fs = DataSources.FeatureSource(features)
    rpool = {}
    rpool[DataSources.TRAIN_FEATURES_VARIABLE] = fs
    rpool[DataSources.TRAIN_LABELS_VARIABLE] = labels
    
    svals, rsvecs, U, Z = kernel.decompositionFromPool(rpool)
    dc = DataSources.DecompositionSource(svals, rsvecs, lsvecs = U, ZMatrix = Z)
    rpool[DataSources.DECOMPOSITION_VARIABLE] = dc
    rls.setResourcePool(rpool)
    rls.loadResources()
    return rls

def predict(features, trainedrls, kernel = LinearKernel.Kernel()):
    fs = DataSources.FeatureSource(features)
    rpool = trainedrls.getResourcePool()
    rpool[DataSources.PREDICTION_FEATURES_VARIABLE] = fs
    kernel.predictionCacheFromRpool(rpool)
    tkm = kernel.testKMFromPool(rpool)
    return trainedrls.predict(tkm)

