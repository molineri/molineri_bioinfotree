#ifndef COUNTED_PTR_H_
#define COUNTED_PTR_H_

template <typename T>
class delete_destructor
{
public:
	void operator()(T* ptr) { delete ptr; }
};

template <typename T>
class delete_array_destructor
{
public:
	void operator()(T* ptr) { delete[] ptr; }
};

template <typename T, typename destructor_trait=delete_destructor<T> >
class counted_ptr
{
public:
	counted_ptr<T, destructor_trait>(T* ptr = 0) : inner_(new inner_ptr(ptr)) {}
	explicit counted_ptr<T, destructor_trait>(const counted_ptr<T>& obj) : inner_(obj.inner_) { inner_->acquire(); }
	~counted_ptr<T, destructor_trait>() { inner_->release(); }
	
	counted_ptr<T, destructor_trait>& operator=(const counted_ptr<T>& rhs)
	{
		if (rhs.inner_ != inner_)
		{
			inner_->release();
			inner_ = rhs.inner_;
			inner_->acquire();
		}
		
		return *this;
	}
	
	counted_ptr<T, destructor_trait>& operator=(T* ptr)
	{
		inner_->release();
		inner_ = new inner_ptr(ptr);
	}
	
	T* operator->() { return inner_->ptr_; }
	operator const T*() { return inner_->ptr_; }
	const T* get() const { return inner_->ptr_; }
	
private:
	struct inner_ptr
	{
		inner_ptr(T* ptr) : ptr_(ptr), count_(1) {}
		~inner_ptr()
		{
			destructor_trait d;
			d(ptr_);
		}
		void acquire() { ++count_; }
		void release()
		{
			--count_;
			if (count_ == 0)
				delete this;
		}
		
		T* ptr_;
		int count_;
	};
	
	inner_ptr* inner_;
};

#endif /*COUNTED_PTR_H_*/
