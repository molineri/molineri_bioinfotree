#ifndef EXCEPTIONS_H_
#define EXCEPTIONS_H_

#include <exception>
#include <string>

class IOError : public std::exception
{
public:
	IOError(const std::string& reason) : reason_(reason) {}
	IOError(const IOError& obj) : std::exception(obj), reason_(obj.reason_) {}
	~IOError() throw() {}
	
	const char* what() const throw () { return reason_.c_str(); }
	
private:
	IOError& operator=(const IOError& rhs);

	const std::string reason_;
};

#endif /*EXCEPTIONS_H_*/
