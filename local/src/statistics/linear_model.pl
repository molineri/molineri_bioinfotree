#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;

my $usage = <<EOF;
usage linear_model -x independent_variables -y dependent_variables

  [-c coefficient_file (deafult do not write coefficients)]
  [-r residuals_file (deafult do not write residuals)]

EOF

our($opt_h, $opt_x, $opt_y, $opt_c, $opt_r);
getopts('hx:y:c:r:');
if ($opt_h || ! $opt_x || ! $opt_y) {
   die $usage;
}

if (! defined $opt_c) {
    $opt_c = "/dev/null"
}
else {
    system "rm -f $opt_c";
}

if (! defined $opt_r) {
    $opt_r = "/dev/null"
}
else {
    system "rm -f $opt_r";
}

#where is the R script?
#my $R_script = "./linear_model.R";
use Env '$BIOINFO_ROOT','@BIOINFO_ROOT','printenv';
my $R_script = "@BIOINFO_ROOT"."/local/bin/linear_model.R";

my %x;
open FX, $opt_x or die "Error opening $opt_x: $!\n";
my $head_x = <FX>;
chomp $head_x;
while (<FX>) {
    chomp;
    my @line = split("\t");
    my $id = shift @line;
    $x{$id} = join("\t", @line);
}
close FX;

my %y;
open FY, $opt_y or die "Error opening $opt_y: $!\n";
my $head_y = <FY>;
chomp $head_y;
while (<FY>) {
    chomp;
    my @line = split("\t");
    my $id = shift @line;
    next unless defined $x{$id};
    @{ $y{$id} } = @line;
}
close FY;

my @response = split("\t", $head_y);
shift @response;
for (my $i = 0; $i <= $#response; ++$i) {
    open OUT, ">linear_model.temp.in";
    print OUT $head_x, "\t", $response[$i], "\n";
    foreach my $id (keys %y) {
        print OUT $id, "\t", $x{$id}, "\t", ${ $y{$id} }[$i], "\n";
    }
    close OUT;
#    my $command = "Rscript $R_script linear_model.temp.in $response[$i] $opt_c; rm -f linear_model.temp.in";
    my $command = "Rscript $R_script linear_model.temp.in $response[$i] $opt_c $opt_r";
    system $command;
    system "rm -f linear_model.temp.in";
}




