#include "../math/KruskalWallisTest.h"

bool testKW(){
	bool res = true;
	const double PREC = 0.01;
	double KW(0.0);
	double P(0.0);	
	
	int ranked[]	= {6,3,12,7,8,5,2,1,9,4,10,11};
	int group[]	= {0,1,2,0,1,2,0,1,2,1,2,2};
	kruskalWallisTest(group, ranked, 12, P, KW);
	res = res && abs(KW - 5.68)	<PREC;
	res = res && abs(P - 0.0584)<PREC;
	
	double data[]	= {6,3,12,7,8,5,2,1,9,4,10,11};
	kruskalWallisTest(group, data, 12, P, KW);
	res = res && abs(KW - 5.68)	<PREC;
	res = res && abs(P - 0.0584)<PREC;
	return res;
}

