#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <sstream>
#include <queue>
#include <gsl/gsl_cdf.h>
#include <assert.h>

#define strong_assert(ASSERTION) ({\
        if ((ASSERTION)==0){\
                fprintf (stderr,\
                        "Assertion failed: " # ASSERTION\
                        ", function %s, file %s, line %u.\n",\
                        __func__, __FILE__, __LINE__);\
                        exit(1);\
        }\
})



using namespace std;

typedef std::queue<double> Values;
typedef std::queue<int> Groups;

int setup(int argc, char* argv[]);
void usage(char** argv);
template <class T> T *queue2array(std::queue<T> &q, T arr[]); //clear the queue and return a corresponding array
void kp_wrapper(Values &values, Values &values_ref, Groups &groups, int groups_count, int min_group);
void pearson(Values &values1, Values &values2, double &r, double &p);
bool options_do_not_test_ordering = false;

int main( int argc, char* argv[] ){
	cout.precision(12);

	//controllo le opzioni passate da riga di comando
	switch(setup(argc, argv)){
		case 0:
			break;
		case -1:
			return 0;
			break;
		case 1:
			cout << "main:: termined because setup error"<<endl;
			return 1;
	}

	cin.imbue(std::locale("C"));
	cout.imbue(std::locale("C"));
	 
	Values values;
	Values values_ref;
	Groups groups;
	int groups_count = 0;
	int pre_group = -1;
	int min_group = -1;
	bool first_line = true;
	string prev_test_data;
	// Load table from file
	string line;
	while (getline(cin,line)){
		istringstream is(line);
		string current_test_data;
		is >> current_test_data;
		if(!first_line and current_test_data != prev_test_data){
			if(!options_do_not_test_ordering and current_test_data < prev_test_data){
				cerr << "ERROR: lexicografic disorder found in column 1" << endl;
				exit(1);
			}
			cout << ">" << prev_test_data << endl;
			kp_wrapper(values, values_ref, groups, groups_count, min_group);
			strong_assert(values.empty());
			strong_assert(values_ref.empty());
			strong_assert(groups.empty());
			groups_count = 0;
			pre_group = -1;
			min_group = -1;
		}
		first_line = false;
		prev_test_data = current_test_data;

		double data;
		if(!(is >> data)){
			cerr << "Malformed input in the following line" << endl;
			cerr << line <<endl;
			exit(1);
		}
		values.push(data);

		if(!(is >> data)){
			cerr << "Malformed input in the following line" << endl;
			cerr << line <<endl;
			exit(1);
		}
		values_ref.push(data);

		int group;
		if(!(is >> group) || group < 0){
			cerr << "Malformed input in the following line" << endl;
			cerr << line <<endl;
			exit(1);
		}
		if(pre_group != group){
			groups_count++;
			if(min_group < 0 or group < min_group){
				min_group = group;
			}
		}
		pre_group = group;
		groups.push(group);
	}
	cout << ">" << prev_test_data << endl;
	kp_wrapper(values, values_ref, groups, groups_count, min_group);
}

void kp_wrapper(Values &values, Values &values_ref, Groups &groups, int groups_count, int min_group){
	Values * vs  = new Values[groups_count]; 
	Values * vsr = new Values[groups_count]; 

	strong_assert(values.size() == values_ref.size());
	strong_assert(values.size() == values_ref.size());
	strong_assert(values.size() == groups.size());

	while(!values.empty()){
		double	v  = values.front(); 
		values.pop();

		double	vr = values_ref.front(); 
		values_ref.pop();

		int g = groups.front();
		groups.pop();

		//cout << "min: " << min_group << "\tcount: " << groups_count << "\tactual: " << g << "\ttransformed: " << g - min_group << endl;
		strong_assert(g - min_group < groups_count && g - min_group >=0);

		 vs[g-min_group].push(v);
		vsr[g-min_group].push(vr);
	}

	for(int i=0; i < groups_count; i++){
		double r;
		double p;
		int n = vs[i].size();
		if(n>3){
			pearson(vs[i], vsr[i], r, p);
			cout << i+min_group << "\t" << n << "\t" << r  << "\t" << p  << endl;
		}else{
			cout << i+min_group << "\t" << n << "\t" << "NA\tNA" << endl;
		}
	}
	delete[] vs;
	delete[] vsr;
}

int setup(int argc, char* argv[]){
	
	////////////////////////////////////////////////////////////////
	// gestione delle opzioni
	//
	int i;
	while((i=getopt(argc, argv, "hs"))!=-1){
		switch(i){
			case 's':
				options_do_not_test_ordering = true;
				break;
			case 'h':
				usage(argv);
				return -1;
				break;
			default:
				cerr<<"wrong options given"<<endl;
				usage(argv);
				return 1;
		}
	}
	return(0);
}


void usage(char** argv){
	cout << "usage: " << argv[0] << "" << endl
		<<"\t-h print this help and exit"<<endl
		<<"\t-s do not test if the stdin is lexicografically sorted"<<endl
		<<endl
		<<".META: stdin"<< endl
		<<"	1	test		str"<< endl
		<<"	2	value		double"<< endl
		<<"	3	value_ref	double"<< endl
		<<"	4	group		int"<< endl
		<<endl
		<<".META: stdout"<< endl
		<<"	1	test	the same as the stdin"<< endl
		<<"	2	group"<< endl
		<<"	3	pearson"<< endl
		<<"	4	pvalue"<< endl
		<<endl
		<<"for each test, for each group, compute the correlation between" <<endl
		<<"the values in col 2 and the reference values in col 3"<<endl;
}

void pearson(Values &values1, Values &values2, double &r, double &p){
	int n=0;
	double s_x=0.;
	double s_y=0.;
	double s_xx=0.;
	double s_yy=0.;
	double s_xy=0.;

	assert(values1.size=values2.size);
	while(!values1.empty()){
		double	v1 = values1.front(); values1.pop();
		double	v2 = values2.front(); values2.pop();

		n++;

		s_x  += v1;
		s_xx += v1 * v1;
		
		s_y  += v2;
		s_yy += v2 * v2;
		
		s_xy += v1 * v2;
	
	}
	r = s_xy - s_x*s_y/n;
	r = r/sqrt( (s_xx-s_x*s_x/n) * (s_yy-s_y*s_y/n) );
	p = 2 * gsl_cdf_tdist_Q( fabs(r)/sqrt((1-r*r)/(n-2)) , n-2 );
}
