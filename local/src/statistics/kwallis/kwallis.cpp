#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
//#include <gsl/gsl_cdf.h>
#include <sstream>
#include <queue>
#include "KruskalWallisTest.h"



using namespace std;

typedef std::queue<double> Values;
typedef std::queue<int> Groups;

int setup(int argc, char* argv[]);
void usage(char** argv);
template <class T> T *queue2array(std::queue<T> &q, T arr[]); //clear the queue and retur a corresponding array
void kw_wrapper(Values &values, Groups &groups, int groups_count);
bool options_do_not_test_ordering = false;

int main( int argc, char* argv[] ){

	//controllo le opzioni passate da riga di comando
	switch(setup(argc, argv)){
		case 0:
			break;
		case -1:
			return 0;
			break;
		case 1:
			cout << "main:: termined because setup error"<<endl;
			return 1;
	}

	cin.imbue(std::locale("C"));
	cout.imbue(std::locale("C"));
	 
	Values values;
	Groups groups;
	int groups_count = 0;
	int pre_group = -1;
	bool first_line = true;
	string prev_test_data;
	// Load table from file
	string line;
	while (getline(cin,line)){
		istringstream is(line);
		string current_test_data;
		is >> current_test_data;
		if(!first_line and current_test_data != prev_test_data){
			if(!options_do_not_test_ordering and current_test_data < prev_test_data){
				cerr << "ERROR: lexicografic disorder found in column 1" << endl;
				exit(1);
			}
			cout << prev_test_data << "\t";
			kw_wrapper(values, groups, groups_count);
			groups_count = 0;
			pre_group = -1;
			//assert(values.size()==0)
			//assert(group.size()==0)
		}
		first_line = false;
		prev_test_data = current_test_data;

		double data;
		if(!(is >> data)){
			cerr << "Malformed input in the following line" << endl;
			cerr << line <<endl;
			exit(1);
		}
		values.push(data);

		int group;
		if(!(is >> group) || group < 0){
			cerr << "Malformed input in the following line" << endl;
			cerr << line <<endl;
			exit(1);
		}
		if(pre_group != group){
			groups_count++;
		}
		pre_group = group;
		groups.push(group);
	}
	cout << prev_test_data << "\t";
	kw_wrapper(values, groups, groups_count);
}

void kw_wrapper(Values &values, Groups &groups, int groups_count){
	if(groups_count < 2){
		cout << "NA\tNA\tonly one group" << endl;
		
	}else{
		double KW = 0.;
		double P  = 0.;
		int test_data_size = values.size();

		double *value_arr = new double[test_data_size];
		int    *group_arr = new int[test_data_size];
		queue2array(values, value_arr);
		queue2array(groups, group_arr);

		//for( int i=0; i<test_data_size; i++){
		//	cout << value_arr[i] << "\t" << group_arr[i] << endl;
		//}

		kruskalWallisTest(group_arr, value_arr, test_data_size, P, KW);

		delete [] group_arr;
		group_arr = NULL;
		delete [] value_arr;
		value_arr = NULL;

		cout << KW << "\t" << P << endl;
	}
}

int setup(int argc, char* argv[]){
	
	////////////////////////////////////////////////////////////////
	// gestione delle opzioni
	//
	int i;
	while((i=getopt(argc, argv, "hs"))!=-1){
		switch(i){
			case 's':
				options_do_not_test_ordering = true;
				break;
			case 'h':
				usage(argv);
				return -1;
				break;
			default:
				cerr<<"wrong options given"<<endl;
				usage(argv);
				return 1;
		}
	}
	return(0);
}


void usage(char** argv){
	cout << "usage: " << argv[0] << "" << endl
		<<"\t-h print this help and exit"<<endl
		<<"\t-s do not test if the stdin is lexicografically sorted"<<endl
		<<endl
		<<".META: stdin"<< endl
		<<"	1	test	str"<< endl
		<<"	1	value	double"<< endl
		<<"	1	group	int"<< endl
		<<endl
		<<".META: stdout"<< endl
		<<"	1	test	the same as the stdin"<< endl
		<<"	1	KW	the vale of the Kruskal Wallis statistic"<< endl
		<<"	1	Pvalue	pvalue of the performed test"<< endl
		<<endl
		<<"taken indipendently each group of row in the stdin aving the same value in the test comum (1)"
		<<"the program compute the Kruskal Wallis test of the values in the value column (2) separed in"
		<<"groups as in the group column (3)"
		<<endl;
}

template <class T>
T *queue2array (std::queue<T> &q, T arr[]) {
	int s = q.size();
	for(int i=0; i<s; i++){
		arr[i]=q.front();
		q.pop();
	}
	return (arr);
}
