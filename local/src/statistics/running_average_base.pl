#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 -l|bin_len N\n";

my $help=0;
my $len=undef;
GetOptions (
	'h|help' => \$help,
	'l|bin_len=i' => \$len,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my @buffer;
while(scalar(@buffer)<$len){
	$_=<>;
	chomp;
	push @buffer,$_;
}
&print_buffer();


while(<>){
	chomp;
	my $garbage = shift @buffer;
	push @buffer, $_;
	&print_buffer();
}


sub print_buffer{
	$buffer[0] =~ /^([^\t]+)/;
	my $label = $1;
	for(@buffer){
		print $label,$_;
	}
}
