#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <vector>
#include <tnt/tnt.h>
#include <sstream>
#include <gsl/gsl_cdf.h>

using namespace std;
using namespace TNT;

#define PEARSON 1
#define WEIGHTED_MEAN_EXP 2
#define EUCLIDEAN 3

int setup(int argc, char* argv[]);
bool test_file(char * filename);
bool parse_next_row(double value[], bool present[], unsigned int array_size, unsigned int header_cols_num, ifstream* infile);
void usage(char** argv);

void dump(	Array2D< double > &mtx,
		Array2D< bool > &prensent
	 );
void read_index(vector<string> &index, char* filename, int file_head_rows );
void fill_matrix(	
		Array2D< double > &mtx,
		Array2D< bool> &present_mtx,
		int file_cols,
		int file_head_rows,
		int file_head_cols,
		char* filename,
		int data_cols
	);

int options_header_rows=-1;
int options_header_cols=-1;
int options_index_col=-1;
int options_file_num_cols=-1;
int options_file1_num_rows=-1;
int options_file2_num_rows=-1;
int options_n_min=-1;
int options_only_same_indexes = false;
int options_all_couples = false;
int options_verbose = false;
int options_compute_pvalues = false;
char * options_input_filename1;
char * options_input_filename2;
int options_distance = PEARSON;


int main( int argc, char* argv[] ){
	cout.precision(12);

	//controllo le opzioni passate da riga di comando
	if(setup(argc, argv)){
		cout << "main:: termined because setup error"<<endl;
		return(-1);
	}else{
		//atexit(stop);
	}

	vector <string> index1;
	vector <string> index2;

	read_index(index1, options_input_filename1, options_header_rows);
	read_index(index2, options_input_filename2, options_header_rows);


	//cerr << index1.size() << endl;
	//cerr << index2.size() << endl;
	//return 1;
	
	Array2D< double > mtx1(options_file1_num_rows-options_header_rows,options_file_num_cols-options_header_cols);
	Array2D< bool > present1(options_file1_num_rows-options_header_rows,options_file_num_cols-options_header_cols);
	Array2D< double > mtx2(options_file2_num_rows-options_header_rows,options_file_num_cols-options_header_cols);
	Array2D< bool > present2(options_file2_num_rows-options_header_rows,options_file_num_cols-options_header_cols);
	fill_matrix(	mtx1,
			present1, 
			options_file_num_cols, 
			options_header_rows, 
			options_header_cols, 
			options_input_filename1,
			options_file_num_cols-options_header_cols
		);
	if(options_verbose == 1){
		cerr << "MSG (pearson2) file " << options_input_filename1 << " readed.\n" << endl;
	}

	fill_matrix(	mtx2,
			present2, 
			options_file_num_cols, 
			options_header_rows, 
			options_header_cols, 
			options_input_filename2,
			options_file_num_cols-options_header_cols
		);
	if(options_verbose == true){
		cerr << "MSG (pearson2) file " << options_input_filename2 << " readed." << endl;
	}

	bool samefile = strcmp(options_input_filename1,options_input_filename2)==0;

	int i_max=mtx1.dim1();
	int j_max=mtx2.dim1();
	int k_max=mtx2.dim2();
	for(int i=0; i<i_max; i++){
		
		int j_min=0;
		if(not options_only_same_indexes and not options_all_couples){
			if(samefile){
				j_min=i+1;
			}
		}

		
		for(int j=j_min; j<j_max; j++){
			if(options_only_same_indexes){
				if(index1[i]!=index2[j])
					continue;
			}else if(samefile and index1[i]==index2[j]){
				continue;
			}

			int n=0;
			double r=0.;
			double s_x=0.;
			double s_y=0.;
			double s_xx=0.;
			double s_yy=0.;
			double s_xy=0.;

			switch(options_distance){
				case PEARSON:
					for(int k=0; k<k_max; k++){
						if(present1[i][k] and present2[j][k]){
							
							n++;

							s_x  += mtx1[i][k];
							s_xx += mtx1[i][k] * mtx1[i][k];
							
							s_y  += mtx2[j][k];
							s_yy += mtx2[j][k] * mtx2[j][k];
							
							s_xy += mtx1[i][k] * mtx2[j][k];
						
						}
					}
					if(n>=options_n_min){
						r = s_xy - s_x*s_y/n;
						r = r/sqrt( (s_xx-s_x*s_x/n) * (s_yy-s_y*s_y/n) );
					}
					break;

				case WEIGHTED_MEAN_EXP:
					for(int k=0; k<k_max; k++){
						if(present1[i][k] and present2[j][k]){
							if( mtx2[j][k] != 0 ){
								n++;
								r += mtx1[i][k] / mtx2[j][k];
							}
						}
					}
					if(n>=options_n_min){
						r /= n;
					}
					break;
				case EUCLIDEAN:
					for(int k=0; k<k_max; k++){
						if(present1[i][k] and present2[j][k]){
							n++;
							r += pow( mtx1[i][k]-mtx2[j][k], 2);
						}
					}
					r = sqrt(r);
					break;
				default:
					cerr << "invalid distance" << endl;
					exit(2);
			}
			if(n>=options_n_min){
				cout << index1[i] << "\t" << index2[j] << "\t";
				cout <<  r << "\t" << n; 
				if(options_compute_pvalues){
					cout << "\t" << gsl_cdf_tdist_Q( fabs(r)/sqrt((1-r*r)/(n-2)) , n-2 );
				}
				cout << endl;
			}
		}
	}
}


void read_index(vector<string> &index, char* filename, int file_head_rows){
	ifstream infile (filename);
	for(int i=0;i<file_head_rows;i++){//perdo le righe di intestazione
		string line;
		getline(infile,line,'\n');
	}
	string line;
	while(getline(infile,line,'\n')){
		string tok;
		stringstream  ss (stringstream::in | stringstream::out);
		ss << line;
		ss >> tok;
		if(ss.fail() and ! ss.eof()){
			cerr << "Problem parsing string, error n. " << ss.rdstate() << endl;
			exit(-1);
		}
		index.push_back(tok);
	}
	infile.close();
}

void fill_matrix(	
		Array2D< double > &mtx,
		Array2D< bool> &present_mtx,
		int file_cols,
		int file_head_rows,
		int file_head_cols,
		char* filename,
		int data_cols
	){

	ifstream myfile (filename);
	for(int i=0;i<file_head_rows;i++){//perdo le righe di intestazione
		string line;
		getline(myfile,line,'\n');
	}

	
	double * value   = new double[data_cols]; //i dati di una singola riga
	bool * present  = new bool[data_cols];	 //il dato di una colonna puo` non essere presente per una data riga
	
	int i=0;
	while(	parse_next_row(	value, 
				present, 
				data_cols, 
				options_header_cols, 
				&myfile
			)
		){ 
		for(int j=0; j<data_cols; j++){
			mtx[i][j]=value[j];
			present_mtx[i][j]=present[j];
		}
		i++;
	}
	
	myfile.close();
	delete [] value;
	delete [] present;
}



int setup(int argc, char* argv[]){
	
	////////////////////////////////////////////////////////////////
	// gestione delle opzioni
	//
	int i;
	while((i=getopt(argc, argv, "vr:s:c:h:p:f:g:n:d:oaP"))!=-1){
		switch(i){
			case 'P':
				options_compute_pvalues=true;
				break;
			case 'v':
				options_verbose=true;
				break;
			case 'r': 
				options_file1_num_rows=atoi(optarg);
				break;
			case 's': 
				options_file2_num_rows=atoi(optarg);
				break;
			case 'h': 
				options_header_rows=atoi(optarg);
				break;
			case 'p':
				options_header_cols=atoi(optarg);
				break;
			case 'c':
				options_file_num_cols=atoi(optarg);
				break;
			case 'f':
				options_input_filename1=optarg;
				break;
			case 'g':
				options_input_filename2=optarg;
				break;
			case 'n':
				options_n_min=atoi(optarg);
				break;
			case 'o':
				options_only_same_indexes=true;
				break;
			case 'a':
				options_all_couples=true;
				break;
			case 'd':
				if(strcmp(optarg,"p")==0){
					options_distance = PEARSON;
				}else if(strcmp(optarg,"w")==0){
					options_distance = WEIGHTED_MEAN_EXP;
				}else if(strcmp(optarg,"e")==0){
					options_distance = EUCLIDEAN;
				}else{
					cerr <<"invalid distance"<<endl;
					usage(argv);
				}
				break;
			default:
				cerr<<"wrong options given"<<endl;
				usage(argv);
				return -1;
		}	
	}
	if(
		options_file1_num_rows<0
		or
		options_file2_num_rows<0
		or
		options_header_rows<0 
		or 
		options_header_cols!=1
		or 
		options_file_num_cols<0
		or
		options_distance == WEIGHTED_MEAN_EXP and not options_all_couples
		or 
		options_compute_pvalues and options_distance != PEARSON
	){
		cerr<<"wrong options value. N.B. -p option must be == 1"<<endl;
		usage(argv);
		return -1;
	}
	if(optind<argc){
		//vi sono parametri dopo l'elenco delle opzioni
		usage(argv);
		return -1;
	}
	if(!test_file(options_input_filename1) or !test_file(options_input_filename2)){
		return -1;
	}
	return(0);
}

bool test_file(char * filename){
	ifstream test (filename,ios::in);
	if (!test.is_open()){
		cerr<<"ERROR: cloe_parallel::setup: cant open file '" << filename <<"'" << endl;
		return false;
	}
	test.close();
	return true;
}

void usage(char** argv){
	cout << "usage: " << argv[0] <<  " -r 1430 -h 1 -p 1 -c 100 -f filename1 -g filename2 -n 3"<<endl
		<<"\t-v verbose"<<endl
		<<"\t-r number of rows in filename1"<<endl
		<<"\t\tes: -r `cat filename2 | wc -l`"<<endl
		<<"\t-s number of rows in filename2"<<endl
		<<"\t\tes: -s `cat filename2 | wc -l`"<<endl
		<<"\t-h number of header rows taht will be ignored (in both files)"<<endl
		<<"\t-p number of header columns (ignored calculating pearsons)"<<endl
		<<"\t\tthe first column is always used to label te rows, then the value" << endl
		<<"\t\tof -p mus be at least 1" << endl
		<<"\t-c number of columns in files (must be te same for both files)"<<endl
		<<"\t\t(head -1 filename1 | tr \"\\t\" \"\\n\" | wc -l) "<<endl
		<<"\t-f first file path"<<endl
		<<"\t-g second file path"<<endl
		<<"\t-n minimum common timepoit for calculate pearson"<<endl
		<<"\t-o compute only the pearson of the rows that have the same index"<<endl
		<<"\t-d DIST	select the measure to compute:"<<endl
		<<"\t\t DIST=p	pearson"<<endl
		<<"\t\t DIST=w	mean of the values of the first id wheighted on values of the second id"<<endl
		<<"\t\t\t	this measure is not simmetrical if there are 0 in the values"<<endl
		<<"\t\t\t	since the division by 0 is avoided when a 0 is encountered as denominator it is treated as NA"<<endl
		<<"\t-P compunte p-value (only for DIST=p)"<<endl
		<<"data in filename1 and filename2 must be tab delimited"<<endl
		<<"each field not in header rows or colls must be formatted like"<<endl
		<<"[+|-][nnnnn][.nnnnn][e|E[+|-]nnnn]"<<endl
		<<"'NA' are accepted and considered missing values"<<endl;
	exit(-1);
}


bool parse_next_row(
		double value[],
		bool present[],
		unsigned int array_size,
		unsigned int header_cols_num,
		ifstream* infile
	){
	
	unsigned int row_index=0;

	string line;
	if(getline(*infile,line,'\n')){
		stringstream  ss (stringstream::in | stringstream::out);
		ss << line;
		//noskipws(ss);
		//ss >> noskipws;
		
		for(int i=0; i<header_cols_num; i++){
			string garbage;
			ss >> garbage;
		}

		int i = 0;
		string tok;
		while(i<array_size and ss >> tok){
			//cerr << tok << endl;
			//cerr << "j: " << j << "\tj_min: " << j_min << endl; 
			if(
//				(tok=="NaN")
//				or
//				(tok=="nan")
//				or
//				(tok=="X")
//				or
//				(tok=="x")
//				or
				(tok=="NA")
			){
				//cout<<"token absen: "<<token<<endl;
				value[i]=0.;
				present[i]=false;
			}else{
				//cout<<"token present: "<<token<<endl;
				//value[i]=strtod(tok.c_str(),NULL);
				value[i]=atof(tok.c_str());
				present[i]=true;
			}
			i++;
		}
		if(ss.fail() and ! ss.eof()){
			cerr << "Problem parsing string, error n. " << ss.rdstate() << " in token at position "<< i+1 << " in line "<<endl<<line<<endl;
			cerr << "Seeen " << i << " columns, expected " << array_size << endl;
			exit(-1);
		}
		if(i != array_size){
			cerr << "Wrong number of columns: seen " << i << " expected " << array_size << endl;
			exit(-1);
		}
		return true;
	}else{
		return false;
	}
}
