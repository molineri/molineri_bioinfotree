#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.util import exit, format_usage
from mdp import pca
from numpy import array
#from subprocess import Popen, PIPE
#from collections import defaultdict

def main():
	usage = format_usage('''
		%prog < STDIN
		project the data (each vector is one row of input) on the principal components (most important first)
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-n', '--out_dim', type=int, dest='out_dim', default=None, help='set to N (usually 2) the number of dimension to print', metavar='N')
	#parser.add_option('-p', '--dump-params', dest='params_file', help='some help FILE', metavar='FILE')
	#parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='some help')

	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	cols = None;
	data = []
	for line in stdin:
		tokens = line.rstrip().split('\t')
		if cols is not None:
			assert len(tokens) == cols
		cols = len(tokens)
		data += [ float(i) for i in tokens]

	x=array(data)
	x.shape = len(data)/cols, cols

	out_dim = cols
	if options.out_dim is not None:
		out_dim = options.out_dim

	for row in pca(x, output_dim=out_dim):
		print "\t".join([ str(i) for i in row])



if __name__ == '__main__':
	main()

