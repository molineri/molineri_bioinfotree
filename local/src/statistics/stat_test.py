#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from warnings import warn
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from random import shuffle
from collections import defaultdict
from vfork.fasta.reader import MultipleBlockStreamingReader
from vfork.stat import PvalueHisto
from vfork.util import safe_import
from math import log, floor, ceil, pow
from scipy.stats import mannwhitneyu

with safe_import('RPy'):
	from rpy import r as R

def wilcox_py(left, right, alternative=None, paired=None, method=None):#paired e alternative sono gestiti nel controllo delle opzioni, method e` sempre wpy in questo caso
	retval=mannwhitneyu(left,right)
	return (retval[0],2*retval[1])

def test_print(id, options, res, l, r):
	statistic, p_value, ml, mr = parse_res(res, options)
	if options.test not in ('pearson','kendall','spearman'):
		if ml is None: ml = sum(l)/len(l)
		if not options.mu:
			if mr is None and options.mu is None: mr = sum(r)/len(r)
			if options.mu is not None:
				mr = options.mu
			print "%s\t%g\t%g\t%g\t%g" % (id,statistic,ml,mr,p_value)
		else:
			print "%s\t%g\t%g\t%g\t%g" % (id,statistic,ml,options.mu,p_value)
	else:
		print "%s\t%g\t%g" % (id, statistic, p_value)
		
	
		
def parse_res(res, options):
	ml = mr = None
	if options.test == 't':
		statistic = res['statistic']['t']
		ml        = res['estimate']['mean of x']
		if options.mu is None:
			mr= res['estimate']['mean of y']
		pvalue = res['p.value']
	elif options.test == 'w':
		if options.paired or options.mu:
			statistic=res['statistic']['V']
		else:
			statistic=res['statistic']['W']
		pvalue = res['p.value']
	elif options.test == 'pearson':
		statistic = res['estimate']['cor']
		pvalue = res['p.value']
	elif options.test == "ks":
		statistic = res['statistic']['D']
		pvalue = res['p.value']
	elif options.test == "wpy":
		statistic = res[0]
		pvalue = res[1]
	else:
		statistic = res['estimate']['rho']
		pvalue = res['p.value']
	return (statistic, pvalue, ml, mr)
	

def single(options, test_func, background):
	fasta = MultipleBlockStreamingReader(stdin, join_lines=False)
	for header,content in fasta:
		left=[]
		right=[]
		for line in content:
			if options.mu is not None or options.background_file is not None:
				tokens = safe_rstrip(line).split('\t')
				if len(tokens) !=1:
					exit("in -m or -b mode only one column of data allowed")
				if tokens[0] != '':
					left.append(float(tokens[0]))
			else:
				tokens = safe_rstrip(line).split('\t')
				if options.paired:
					assert len(tokens) == 2
				if(options.labels):
					if tokens[1] == "L" or tokens[1]=="l":
						left.append(float(tokens[0]))
					elif tokens[1] == "R" or tokens[1]=="r":
						right.append(float(tokens[0]))
					else:
						exit("label not known (%s)" % tokens[1])
				else:
					if tokens[0] != '':
						left.append(float(tokens[0]))
					if len(tokens)==2 and tokens[1] != '':
						right.append(float(tokens[1]))

		if options.background_file is not None:
			right = background

		if options.mu is not None:
			res=test_func(left, alternative=options.side, mu=options.mu)
		else:
			if options.test not in ('pearson','kendall','spearman'):
				res=test_func(left, right, paired=options.paired, alternative=options.side, var_equal=options.var_equal)
			else:
				res=test_func(left, right, alternative=options.side, method=options.test)

		if len(left) >= options.min_set_size and len(right) >= options.min_set_size:
			test_print(header, options, res, left, right)

def multi(options, test_func, values, sets, random=False):
	for set_id, items in sets.iteritems():
		items_len = len(items)
		if items_len >= options.min_set_size and \
			(options.max_set_size is None or items_len <= options.max_set_size):  # sets have only items also present in values
			left = []
			right = []
			for id,v in values.iteritems():
				if id in items:
					left+=v
				else:
					right+=v

			if len(right) < options.min_set_size:
				warn('the set "%s" include (almost %d vs %d) each items , no pvale reported' % (set_id, items_len, items_len+len(right)))
				continue

			res = test_func(left, right, paired=options.paired, alternative=options.side)
			if not random:
				statistic, p_value, ml, mr  = parse_res(res, options)
				if ml is None:
					ml = sum(left)/len(left)
					mr = sum(right)/len(right)
				yield (set_id, statistic, ml, mr, p_value)
			else:
				if res['p.value']<0:
					raise(ValueError("Some problem running test_func.\n%s" % str(res)));
				yield res['p.value']
				
				
		
def read_sets(filename, values):
	sets=defaultdict(set)
	with file(filename, 'r') as fd:
		for line in fd:
			tokens = line.rstrip().split('\t')
			assert len(tokens) == 2
			(set_id, item_id) = tokens
			if item_id in values:
				sets[set_id].add(item_id)
	return sets

def read_values(duplicated_items):
	values={} # cannot use defaultdict
	for line in stdin:
		item_id, item_val = line.rstrip().split('\t')
		if not duplicated_items and item_id in values:
			raise(ValueError("duplicated items (%s), use -d to allow duplicated items" % item_id))
		v = values.get(item_id,[])
		v.append(float(item_val))
		values[item_id] = v
			
	return values

def randomizze(values):
	ks = values.keys()
	vs = values.values()
	shuffle(vs)
	retval={}
	for k,v in zip(ks, vs):
		retval[k]=v

	#for k,v in retval.iteritems():
	#	print "random\t%i\t%g" % (k,v[0])

	return retval

def main():
	usage = format_usage('''
		%prog < STDIN

		there are two usage mode, the standar and the multi mode (-M)

		= default mode = 
			the standard input must be a fasta file, each block containing 2 columns of numbers
			for each fasta the 2 columns of numbers will be compared using the required statistical test

			if the -l (labels) option is given then each fasta block must have teh following form:

			.META: stdin
				1	number
				2	labels	[L|R]

			the numbers associated with a R label will be compared with those having a L label.

		= multi mode (-M) = 
			the SET_FILE (see -M option) must be a set file
			.META:
				1. set_id
				2. item_id

			the stdin file must be
			.META:
				1. item_id
				2. value
			
			for eache set A, the values associated to items \in A are compared versus the values of each other items
			the items in the set A also present in the standard input are the left (L) list, the other item in the 
			STDIN are the right (R) list

			So if you wont to test if the items of the set are greater you may use the option -s greater

		the standard output is in the form:
		.META:
			1	id
			2	statistics (t, w)
			3	mean of left values
			4	mean of right values
			5	p-value

		if the -r option is given then randomization are performed and the output is:
		.META:
			1	id
			2	statistics (t, w)
			3	mean of left values
			4	mean of right values
			5	p-value
			6	false_positives
			7	fdr if the cutoff was set at this row
			8	best fdr
			9	p-value cutof for the best fdr

	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='verbose [default: %default]')
	parser.add_option('-t', '--test', type=str, dest='test', default='t', help="perform a t test [T=t] (dafault), Wilcoxon (Mann Whitney) test [T=w], Wilcoxon (Mann Whitney, scipy implementation) test [T=wpy], Pearson test [T=pearson], Kendall test [T=kendall], Spearman test [T=spearman], Kolmogorov-Smirnov test [T=ks]\n[default: %default]", metavar="T")
	parser.add_option('-l', '--labels', dest='labels', action='store_true', default=False, help='use a labelled input file format (see abowe) [default: %default]')
	parser.add_option('-s', '--side', type=str, dest='side', default='two.sided', help='one tile or two tile test. Available values are: two.sided [test if the first list differ form the second, or if the correlation is not null], greater [test if the first list [L] is greater or if the correlation is positive], less [test if the second list [R] is greater or if the correlation is negative] [default: %default].  ', metavar='SIDE')
	parser.add_option('-p', '--paired', dest='paired', action='store_true', default=False, help='perform a paired test (Wilcoxon signed rank test or paired t test) [default: %default]')
	parser.add_option('-e', '--var_equal', dest='var_equal', action='store_true', default=False, help='performa a two sample t test assumng the variance of two sample equal[default: %default]')
	parser.add_option('-m', '--mu', type=float, dest='mu', default=None, help='a number indicating the true value of the mean (or difference in means if you are performing a two sample test. In Wilcoxon mode perform a Wilcoxon signed rank test of the null hp that the distribution of one sample values is symmetric about MU [default: %default]', metavar='MU')
	parser.add_option('-b', '--background-file', type=str, dest='background_file', default=None, help='use a separate file for the background list [default: %default]', metavar='FILE')
	parser.add_option('-M', '--multi', type=str, dest='multi', default=None, help='activate the multi mode and give te SET_FILE  [default: %default]', metavar='SET_FILE')
	parser.add_option('-d', '--duplicated_items', dest='duplicated_items', action='store_true', default=False, help='allow duplicated items in input (meaningfull only with -M) [default: %default]')
	parser.add_option('-n', '--min_set_size', type=int, dest='min_set_size', default=0, help='do not compute statistics and do not report anityng if a set has less than N item with values [default: %default]', metavar='N')
	parser.add_option('-q', '--max_set_size', type=int, dest='max_set_size', default=None, help='do not compute statistics and do not report anityng if a set has greater than N item with values [default: %default]', metavar='N')
	parser.add_option('-r', '--randomizations_rounds', type=int, dest='randomizations_rounds', default=0, help='number of randomization to perform (the output has more column if N>0)  [default: %default]', metavar='N')
	parser.add_option('-a', '--histo_min', type=float, dest='histo_min', default=1e-100, help='the min value of the random pvalue histogram range  [default: %default]', metavar='N')
	parser.add_option('-A', '--histo_max', type=int, dest='histo_max', default=1, help='the max value of the random pvalue histogram range  [default: %default]', metavar='N')
	parser.add_option('-N', '--histo_bin_num', type=int, dest='histo_bin_num', default=10000, help='the number of bin of the random pvalue histogram (the binning is a log binning)  [default: %default]', metavar='N')
	parser.add_option('-w', '--histo_write_in_file', type=str, dest='histo_write_in_file', default=None, help='write the hisogram in FILE, see the options -f and -R  [default: %default]', metavar='FILE')
	parser.add_option('-R', '--histo_write_in_file_every', type=int, dest='histo_write_in_file_every', default=10, help='write the hisogram in FILE every R randomizations  [default: %default]', metavar='R')
	parser.add_option('-f', '--histo_load_from_file', type=str, dest='histo_load_from_file', default=None, help='load a hisogram FILE [default: %default]', metavar='FILE')

	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	test_func = None
	if options.test == 't':
		test_func = R.t_test
	elif options.test == 'w':
		test_func = R.wilcox_test
	elif options.test == 'ks':
		test_func = R.ks_test
	elif options.test in ('pearson','kendall','spearman'):
		if options.mu is not None:
			exit("options -m|mu and -t|test_func p or k or s are incompatible")
		if options.multi:
			exit("options -M|multi and -t|test_func p or k or s are incompatible")
		test_func = R.cor_test
	elif options.test == "wpy":
		test_func = wilcox_py
		if options.mu is not None:
			exit("options -m|mu and -t|test_func wpy are incompatible")
		if options.var_equal is not False:
			exit("options -var_equal and -t|test_func wpy are incompatible")
		if options.paired is not False:
			exit("options -paired and -t|test_func wpy are incompatible")
		if options.side != "two.sided":
			exit("options -side and -t|test_func wpy are incompatible")
		

	else:
		exit("invalid value for -t option")
	
	if options.mu is not None and options.background_file is not None:
		exit("-m mode incompatible with -b mode.")
	
	if options.multi:
		if options.background_file:
			exit("--multi mode incompatible with --background_file")
		if options.paired:
			exit("--multi mode incompatible with --paired")
		if options.mu:
			exit("--multi mode incompatible with --mu")
		if options.randomizations_rounds == 0:
			if options.histo_write_in_file or options.histo_load_from_file:
				exit('no randomizations but histogram file handling required')
			
	else:
		if options.histo_write_in_file or options.histo_load_from_file:
			exit('single mode but histogram file handling required')




	#r.library("exactRankTests")
	background = None
	if options.background_file is not None:
		background = []
		with file(options.background_file, 'r') as fd:
			for line in fd:
				tokens = safe_rstrip(line).split('\t')
				if len(tokens) !=1:
					exit("The background file must be a single column of numbers.")
				try:
					v = int(tokens[0])
				except ValueError:
					try:
						v = float(tokens[0])
					except ValueError:
						exit("The background file must be a single column of numbers.")
				background.append(v)
						
	if options.multi is None:
		single(options, test_func, background)
	else:
		values=read_values(options.duplicated_items)
		sets=read_sets(options.multi, values)
		if options.randomizations_rounds == 0:
			for v in multi(options, test_func, values, sets):
				print "%s\t%g\t%g\t%g\t%g" % v
		else:
			if options.verbose: stderr.write("\n")
			histo = PvalueHisto(options.histo_min,options.histo_max,options.histo_bin_num)

			if options.histo_write_in_file is not None:
				histo_write_fd = file(options.histo_write_in_file,'w')

			for i in xrange(0, options.randomizations_rounds):
				random_values = randomizze(values)
				for p_value in multi(options, test_func, random_values, sets, True):
					assert p_value > 0
					histo.put(p_value)
				if options.histo_write_in_file is not None and i % options.histo_write_in_file_every == 0:
					histo.write_in_file(histo_write_fd,i)

				if options.verbose: stderr.write("\r                         \r")
				if options.verbose: stderr.write("\rrandomization run: %i\r" % (i+1) )
			if options.histo_write_in_file is not None: 
				histo.write_in_file(histo_write_fd, options.randomizations_rounds)

			res=[]
			for set_id, statistic, ml, mr, pvalue in multi(options, test_func, values, sets):
				res.append([set_id, statistic, ml, mr, pvalue])

			res.sort(lambda a,b: cmp(a[4],b[4]))

			histo.cumulate()
			#histo.print_cumulative()

			for i,r in enumerate(res):
				false_positives = float(histo.get_smaller(r[4])) / options.randomizations_rounds
				res[i].append( false_positives )
				res[i].append( false_positives / (i+1) ) #fdr for that p-value (strictly speaking for that row)

			fdr = None
			cutoff = None
			res.reverse()
			for i,r in enumerate(res):
				if fdr is None or fdr > r[6]:
					fdr = r[6]
				res[i][6] = fdr
			res.reverse()

			for r in res:
				r = [  "%g" % v if type(v) == float else v for v in r ]
				print "\t".join(r)
				



		


if __name__ == '__main__':
	main()
