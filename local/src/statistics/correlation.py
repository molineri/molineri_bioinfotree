#!/usr/bin/env python
#
# Copyright 2008-2009 Gabriele Sales <gbrsales@gmail.com>

from __future__ import with_statement
from cmath import isnan
from itertools import groupby, izip
from multiprocessing import Pool
from optparse import OptionParser
from os.path import basename
from sys import argv, stdin, stderr
from vfork.io.colreader import Reader
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage, safe_import

with safe_import('numpy (http://numpy.scipy.org)'):
	from numpy import asarray
	from numpy import random

with safe_import('scipy (http://scipy.org)'):
	from scipy.stats.stats import pearsonr, spearmanr


class NumerosityError(Exception):
	''' An exception to signal non fatal errors. '''


def correlation_type(script_name, options):
	bn = basename(script_name)
	known_types = ('pearson', 'spearman')
	if bn in known_types:
		if options.type is not None and options.type != bn:
			exit('You invoked the %s correlator, but then asked for the %s correlator.' % (bn, options.type))
		else:
			return bn
	elif options.type is None:
		return 'pearson'
	elif options.type in known_types:
		return options.type
	else:
		exit('Unknown correlator type: %s' % options.type)

def iter_rows(fd, matrix_name, skip_first):
	if skip_first:
		fd.readline()
	
	for line_idx, line in enumerate(fd):
		tokens = safe_rstrip(line).split('\t')
		label = tokens[0]

		values = []
		for t in tokens[1:]:
			try:
				values.append(float(t))
			except ValueError:
				exit('Invalid float value at line %d of matrix %s: %s' % (line_idx+1, matrix_name, t))

		yield label, asarray(values)


def filter_nan(computation):
	def helper(x_name, xs, y_name, ys):
		fxs = []
		fys = []
		for x, y in izip(xs, ys):
			if not isnan(x) and not isnan(y):
				fxs.append(x)
				fys.append(y)
		
		return computation(x_name, fxs, y_name, fys)
	
	return helper

def load_seeds(filename):
	seeds = {}
	with file(filename) as fd:
		reader = Reader(fd, '0s,1u', False)
		for label, seed in reader:
			seeds[label] = seed
	return seeds

def shuffle_rows(computation, num, seed_map):
	def helper(x_name, xs, y_name, ys):
		if seed_map is not None:
			try:
				seed = seed_map[x_name]
				
				# fix problem with NumPy
				assert seed == int(seed)
				seed = int(seed)
				
				random.seed(seed)

			except KeyError:
				exit('Unknown label: %s' % x_name)

		accum = 0.0
		for i in xrange(num):
			random.shuffle(xs)
			accum += computation(x_name, xs, y_name, ys)[0]

		return accum/num, None
	
	return helper

def compute_correlation(correlator):
	def helper(x_name, xs, y_name, ys):
		if len(xs) != len(ys):
			exit('Different number of values between %s and %s.' % (x_name, y_name))
		elif len(xs) < 3:
			raise NumerosityError, 'Too few values to compute correlation between %s and %s.' % (x_name, y_name)

		return correlator(xs, ys)

	return helper

computation = None

def init_computation(correlator_type, options):
	global computation

	if correlator_type == 'pearson':
		correlator = pearsonr
	elif correlator_type == 'spearman':
		correlator = spearmanr

	computation = compute_correlation(correlator)
	if options.shuffle_num is not None:
		seed_map = load_seeds(options.seed_map) if options.seed_map is not None else None
		computation = shuffle_rows(computation, options.shuffle_num, seed_map)
	if options.filter_nan:
		computation = filter_nan(computation)


def corresponding_rows(xss, yss):
	for (x_name, xs), (y_name, ys) in izip(xss, yss):
		yield (x_name, xs, y_name, ys)

def list_pairs(filename):
	with file(filename, 'r') as fd:
		return set(ns for ns in Reader(fd, '0s,1s', False))

def listed_pairs(pairs):
	def filter(xss, yss):
		for x_name, xs in xss:
			for y_name, ys in yss:
				if (x_name, y_name) in pairs:
					yield (x_name, xs, y_name, ys)
	return filter

def all_pairs(xss, yss):
	for x_name, xs in xss:
		for y_name, ys in yss:
			yield (x_name, xs, y_name, ys)

def all_pairs_simmetric(xss, yss):
	for x_name, xs in xss:
		for y_name, ys in yss:
			if x_name < y_name:
				yield (x_name, xs, y_name, ys)



def print_correlation(x_name, y_name, corr, pvalue):
	if pvalue is None:
		print '%s\t%s\t%g\t' % (x_name, y_name, corr)
	else:
		print '%s\t%s\t%g\t%g' % (x_name, y_name, corr, pvalue)

def serial_evaluate(pairs, comp_info):
	init_computation(*comp_info)
	for x_name, xs, y_name, ys in pairs:
		try:
			corr, pvalue = computation(x_name, xs, y_name, ys)
			print_correlation(x_name, y_name, corr, pvalue)
		except NumerosityError, e:
			print >>stderr, e.args[0]

def parallel_comp((x_name, xs, y_name, ys)):
	try:
		corr, pvalue = computation(x_name, xs, y_name, ys)
		return (x_name, y_name, corr, pvalue)
	except NumerosityError, e:
		return e

def parallel_evaluate(jobnum):
	def helper(pairs, comp_info):
		pool = Pool(jobnum, init_computation)
		for res in pool.imap(parallel_comp, pairs):
			if isinstance(res, CorrelationWarning):
				print >>stderr, e.args[0]
			else:
				print_correlation(*res)
	return helper


def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] YS <XS >CORRELATIONS
		
		Loads two matrices of values (XS an YS) and computes the correlation
		between corresponding rows.
		
		The first column of both matrices contains the row label. All other
		entries are treated as floating point numbers.
		
		You can use the --cross option to compute the correlation between all
		row pairs or --pairs to explicitly list those you are interested in.

		The correlation type is set by using the --type switch followed by
		one of:
		
		- pearson
		- spearman
		
		The output has the following format:
		
		1. row label from XS
		2. row label from YS
		3. correlation
		4. p-value (missing if --shuffle is used)
		
		P-values are not corrected for multiple testing.
	'''))
	parser.add_option('-m', '--seed-map', dest='seed_map', help='map linking row labels to seeds used for the shuffling (requires --shuffle)', metavar='SEED_MAP')
	parser.add_option('-n', '--filter-nan', dest='filter_nan', action='store_true', default=False, help='discard NaN entries')
	parser.add_option('-1', '--skip-first-row', dest='skip_first', action='store_true', default=False, help='skip the first row of both matrices')
	parser.add_option('-c', '--cross', dest='cross', action='store_true', default=False, help='compute correlation between all row pairs')
	parser.add_option('-i', '--simmetric', dest='simmetric', action='store_true', default=False, help='in --cross mode do not compute simmetic pairs')
	parser.add_option('-p', '--pairs', dest='pairs', default=None, help='compute correlation between row pairs listed in PAIR_FILE', metavar='PAIR_FILE')
	parser.add_option('-s', '--shuffle', dest='shuffle_num', type='int', help='shuffle XS values SHUFFLE_NUM times and report the average correlation', metavar='SHUFFLE_NUM')
	parser.add_option('-t', '--type', dest='type', help='set the correlation type (default: pearson)')
	parser.add_option('-j', '--jobs', dest='jobs', type='int', default=1, help='split the computation into N parallel jobs', metavar='N')
	options, args = parser.parse_args()

	if len(args) != 1:
		exit('Unexpected argument number.')
	elif options.cross and options.pairs:
		exit('Cannot use --cross and --pairs together.')
	elif options.shuffle_num is not None and options.shuffle_num <= 0:
		exit('Invalid shuffle number: %d' % options.shuffle_num)
	elif options.shuffle_num is None and options.seed_map is not None:
		exit('--seed-map requires --shuffle.')
	elif options.jobs < 0:
		exit('Invalid number of jobs: %d' % options.jobs)
	if options.simmetric and not options.cross:
		exit('Cannot use --simmetric witout --cross.')
	
	tp = correlation_type(argv[0], options)

	if options.cross:
		strategy = all_pairs
		if options.simmetric:
			strategy = all_pairs_simmetric
	elif options.pairs:
		strategy = listed_pairs(list_pairs(options.pairs))
	else:
		strategy = corresponding_rows

	if options.jobs == 1:
		evaluate = serial_evaluate
	else:
		evaluate = parallel_evaluate(options.jobs)

	with file(args[0], 'r') as fd:
		yss = list(iter_rows(fd, args[0], options.skip_first))
	
	xss = iter_rows(stdin, '<stdin>', options.skip_first)
	evaluate(strategy(xss, yss), (tp, options))

if __name__ == '__main__':
	main()
