#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;

my $mult_test_R = $ENV{'BIOINFO_ROOT'}."/local/src/statistics/multiple_test_correction.R";

my $usage = <<EOF;
usage $0 < input_file > output_file

  [-B bonferroni correction]
  [-H Benjamini-Hochberg FDR]
  [-Y Benjamini-Yekutieli FDR]

  assume the p-value in the last column and add a column for each correction required (in the reqired order)
EOF

our($opt_h, $opt_B, $opt_H, $opt_Y);
getopts('hBHY');
if ($opt_h) {
   die $usage;
}

my @corr = ();
if ($opt_B) {
    push @corr, "Bonferroni";
}
if ($opt_H) {
    push @corr, "BH";
}
if ($opt_Y) {
    push @corr, "BY";
}
my $corr = join(" ", @corr);

if (@ARGV) {
    system "Rscript $mult_test_R $corr < $ARGV[0]";
}
else {
    system "Rscript $mult_test_R $corr < /dev/stdin";
}

