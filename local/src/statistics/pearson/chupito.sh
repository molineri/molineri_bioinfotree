#!/bin/bash

#simple shell script which calls peer without asking n. or rows or other info from the user
#WARNING the header row should NOT be commented!!!!!
#better to remove comments here and simplify the C? XXX

if [ $# != 2 ]; then
	echo "usage: chupito.sh input_file output_file\n"
	exit
fi

COL=`tail -n 1 $1 | tr "\t" "\n" | wc -l`
ROW=`grep -vc "#" $1`

./peer $ROW $COL < $1 > $2
