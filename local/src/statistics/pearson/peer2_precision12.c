#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#define MAX_ID_LENGTH 100
#define INPUT_ERROR 1
#define ID_ERROR 2
#define DATA_ERROR 3
/* simple c that computes pearson correlation coefficient from a _complete_ matrix of experiment/value data.
 * all vs all: we compute pearson coeff between all possible couples of rows of the file
 * first row and comment line (starting with a #) are skipped
 * output: id1 \t id2 \t pearson
 * argv expected: a[1] = n. of rows (excluding comments!), a[2] = n. of columns (head -n file | tr "\t" "\n" | wc -c)
 * the input file is given by <
 * */
double *get_next_line(double *line, int col);
char *get_id(char *id);
void skip_line(void);
void compute_print_pearson(double *exp_data, char *id, int col, int row, int option_redundant_couples);


int main(int argc, char* argv[]) 
{
	int option_redundant_couples = 0;
	char *id;
	int col, row;
	int j;
	double *exp_data;
	
	int opt;
	while((opt=getopt(argc, argv, "a"))!=-1){
		switch(opt){
			case 'a':
				option_redundant_couples=1;
				break;
			default:
				printf("wrong options given");
				return -1;
		}	
		
	}
   if (argc - optind != 2) {
		printf("Usage: %s [-a] n.rows (excluding comments) n.column.\nCompute the Pearson coefficent for each couple of rows in a matrix.\n  -a\tprint all couple in both orders (a,b) (b,a) even if the pearson is the obviously the same.\n\n See $(BIOINFO_ROOT)/local/src/statistics/pearson/test/makefile for usage examples.\n", argv[0]);
		return INPUT_ERROR;
	}

	col = atoi(argv[optind + 1]);
	row = atoi(argv[optind]);
	printf("#%s %d %d\n", *(argv), row--, col--); //-- because we use col and row as number of exp and genes...
	
   j = 0;
   exp_data = (double *) malloc ((row*col) * sizeof(double)); //we use a simple array instead of a 2d matrix to malloc only here
	id = (char *) malloc ((row*MAX_ID_LENGTH) * sizeof(char));	//array to keep genes' ids (first column)
   
	skip_line(); //eat header line
	while (j < row) {
      if ( get_id(&id[j*MAX_ID_LENGTH]) == NULL) 
           return ID_ERROR;
      if ( get_next_line(&exp_data[j*col], col) == NULL)
           return DATA_ERROR;
		j++;
	}
	
	compute_print_pearson(exp_data, id, col, row, option_redundant_couples);
	free(exp_data);
	free(id);
	return 0;
}

void compute_print_pearson(double *exp_data, char *id, int col, int row, int option_redundant_couples)
{
	int i, j, k;
	double s_x, s_y, s_xx, s_yy, s_xy = 0;
	double r;

	for (j = 0; j < row; j++) { 
		int i_min = 0;
		if(option_redundant_couples != 1){
			i_min = j+1;
		}
		for (i = i_min; i < row; i++) { //two loops to select rows to correlate
			if(i==j){
				continue;
			}
			s_x = 0;
			s_y = 0;
			s_xx = 0;
			s_yy = 0;
			s_xy = 0;
			for (k = 0; k < col; k++) {
				s_x += exp_data[j*col+k];	
				s_y += exp_data[i*col+k];	
				s_xx += exp_data[j*col+k] * exp_data[j*col+k];
				s_yy += exp_data[i*col+k] * exp_data[i*col+k];
				s_xy += exp_data[j*col+k] * exp_data[i*col+k];
			}
			r = s_xy - s_x*s_y/col;
			r = r/sqrt( (s_xx-s_x*s_x/col) * (s_yy-s_y*s_y/col) );
			printf("%s\t%s\t%.12g\n", &id[j*MAX_ID_LENGTH], &id[i*MAX_ID_LENGTH], r);
			fflush(stdout);
		}
	}
}

void skip_line()
{
	char c;
	while ((c = getchar()) != '\n') {}
}

double *get_next_line(double *line, int col) 
{	
	double *begin;
	double next;

	begin = line;
	
	while (scanf("%lg", &next)) {
		*line = next;
		line++;
		if (line == begin+col) //we never want to read too many columns!
			return begin;
	}
   if (line == begin+col) //ma succede che ne legga di meno? o salta gli acapi?
	   return begin; 
   else
      return NULL;
}

char *get_id(char *id) 
{
	if (scanf("%s", id)) {
		if (id[0] == '#') {
			skip_line();
			return get_id(id); //we skip a line! expensive but...
		}
		return id;
	}
	else
		return NULL; //it should never happen...
}
