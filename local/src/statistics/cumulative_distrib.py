#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from scipy.stats import norm, t, hypergeom, chi2, binom 
from math import sqrt

#from subprocess import Popen, PIPE
#from collections import defaultdict

def main():
	usage = format_usage('''
		%prog COL < STDIN
			add a column in the COL+1 position with the value of the cumulative distrubution at the x value present in the COL.
	''')
	parser = OptionParser(usage=usage)

	distrib={}
	distrib['norm']=norm
	distrib['student']=t
	distrib['pearson']=t #with correction
	distrib['hypergeo']=hypergeom #with correction
	distrib['chi2']=chi2
	distrib['binom']=binom
	
	parser.add_option('-d', '--distribution', type=str, dest='distribution', default="norm", help='use the DIST distribution, example ov valid DIST are %s default is norm' % str(distrib.keys()), metavar='DIST')
	parser.add_option('-n', '--df', type=int, dest='df', default=None, help='the degree of freedom to use', metavar='DF')
	parser.add_option('-i', '--invert', action="store_true", dest='invert', default=False, help='report the size of the tail of the distribtion (1 - the cdf value)')

	options, args = parser.parse_args()

	if (options.distribution == "student" or options.distribution == "pearson" or options.distribution == "binom") and options.df is None:
		exit("The required distribution need the indication of degree of fredom to use.")
	
	if len(args) != 1:
		exit('Unexpected argument number.')

	c = int(args[0])
	c-=1

	if options.distribution=="binom":
		distrib["binom"]=binom(options.df, 0.5)

		
	#Phyper = 1. - hypergeom.cdf(n, n_tissue, n1, n2)
	
	for line in stdin:
		tokens = safe_rstrip(line).split('\t')
		x = float(tokens[c])
		
		if options.distribution != 'hypergeo':
			if options.distribution == "pearson":
				x = sqrt(options.df - 2) * x / sqrt(1 - x*x)

			if options.df is not None and options.distribution != "binom":
				v = distrib[options.distribution].cdf(x,options.df)
			else:
				v = distrib[options.distribution].cdf(x)
		else:
			assert c>=3, "-d hypergeo requre 4 parameters"
			x = int(tokens[c])
			n = int(tokens[c-1])
			M = int(tokens[c-2])
			N = int(tokens[c-3])
			v = hypergeom.cdf(x, N, M, n)


		if options.invert:
			v = 1-v

		tokens[c] += "\t"+str(v)
		print "\t".join(tokens)
	
			
		

if __name__ == '__main__':
	main()

