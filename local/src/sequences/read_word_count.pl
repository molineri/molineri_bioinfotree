#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] [-g glue]]\n";

my $help=0;
my $glue=",";
GetOptions (
	'h|help' => \$help,
	'g|glue=s' => \$glue,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my %a3=();
my %a4=();
my %a5=();
while(<>){
	next if m/^@/;
	if(m/^\+/){
		$_=<>;
		next;
	}
	chomp;
	while(length>=3){
		if(length>=3){
			$a3{substr($_,0,3)}++
		}
		if(length>=4){
			$a4{substr($_,0,4)}++
		}
		if(length>=5){
			$a5{substr($_,0,5)}++
		}
		substr($_,0,1,"");
	}
}

my $key;
my $value;

while ( ($key, $value) = each %a3 )
{
  print $key, $a3{$key}
}
while ( ($key, $value) = each %a4 )
{
  print $key, $a4{$key}
}
while ( ($key, $value) = each %a5 )
{
  print $key, $a5{$key}
}
