#!/usr/bin/perl
use warnings;
use strict;
$\="\n";

my $usage = "$0 coords < chr.fa
coords:
b	e
b	e

mask wiht E the indicated coords
";

my $coords_file = shift @ARGV;
die $usage if !defined $coords_file;

open COORDS,$coords_file or die "Can't open file ($coords_file)\n";


my $old_e = undef;
my @start = ();
my @stop = ();

while (<COORDS>) {
	chomp;
	my ($b,$e)=split /\t/;

	die "b >= e in $coords_file\n" if ($b >= $e);
	die "$coords_file not sorted on b_e or not disjoined (union)\n" if ( defined $old_e and ($b < $old_e) );

	push @start,$b;
	push @stop,$e;
	$old_e = $e;
}


my $read_len = 0;
my $i = 0;
while (my $seq = <>) {
	chomp $seq;

	if ($seq =~ /^>/){
	 	print $seq;
		next;
	}

	my $line_len = length $seq;


	if($i >= scalar(@start) or $start[$i] >= $read_len + $line_len){
		print $seq;
		$read_len += $line_len;
		next;
	}

	die("Algorithm error") if($stop[$i] < $read_len);

	while($i < scalar(@start) and $start[$i] < $read_len + $line_len){
		if ($stop[$i] <= $read_len + $line_len) {
			substr($seq, $start[$i] - $read_len, $stop[$i]-$start[$i], 
				'E' x ($stop[$i]-$start[$i]) );
			$i++;
		} else {
			substr($seq, $start[$i] - $read_len, $read_len + $line_len - $start[$i], 
				'E' x ($read_len + $line_len - $start[$i]) );
			$start[$i] = $read_len + $line_len;
		}
	}
	print $seq;

	$read_len += $line_len;
}
