#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $help=0;
my %words=();
my $probe_file;
my $verbose = 0;

my $usage="$0 [-v] -p probe_file < nucloetides
	the prob_file is in the form
ID1	sequence1
ID2	sequence2
...

";

GetOptions (
	'h|help' => \$help,
	'p|probe=s'  => \$probe_file,
	'v|verbose'  => \$verbose
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

die($usage) if !defined($probe_file);

open(PROB,$probe_file) or die("Can't open file ($probe_file)");

my %probe=();
my $k=undef;#prob length
while(<PROB>){
	chomp;
	my ($id,$seq) = split /\t/;
	if(defined $probe{$seq}){ 
		$probe{$seq}.=";".$id;
	}else{
		$probe{$seq}=$id;
	}
	if(!defined($k)){
		$k=length($seq);
	}elsif(length($seq)!=$k){
		die("Pobes of different size found.");
	}
}

print STDERR "probes letti - ". `date`   if $verbose;
 
my $pos=0;
$_=<>;
chomp;
die("Header fasta not found") if $_!~/^>/;

my $seqid = $_;
$seqid =~ s/^>//;
$_="";

while( 1 ){
	
	if(length($_) < $k){
		my $new_row=<>;
		#print STDERR length($_),$new_row;
		if(defined($new_row)){
			chomp $new_row;
			if($new_row =~ /^>/){
				#print STDERR "hash costruito - ". `date`  if $verbose;
				#&print_coords();
				%words=();
				$seqid = $new_row;
				$seqid =~ s/^>//;
				#my @data = split /\t/, $seqid;
				$pos=0; #alternative = $pos=$data[1];
				$_="";
				next;
			}
			$_.=$new_row;
		}else{
			last;
		}
	}


$_ =~ tr/atcg/ATCG/;

	my $w=substr($_,0,$k);
	substr($_,0,1,"");

	if ($w!~/^[ACGT]+$/) {
	$pos+=1;
	next;
	}

	my $ids = $probe{$w};
	print $seqid, $ids, $w, '+', $pos if defined($ids);

#modificato rw prima w
	my $rw = reverse $w;
	$rw =~ tr/ACGT/TGCA/; #la parola trovata viene rev-com; siamo virtualmente sullo strand - e cerchiamo lo stesso seed!

	$ids = $probe{$rw};
	print $seqid, $ids, "s:".$w."_m:".$rw, '-', $pos if defined($ids);

	$pos+=1;
}
