#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my %words=();
my $probe_file;
my $verbose = 0;

my $usage="$0 [-v] -p probe_file < nucloetides";

GetOptions (
	'p|probe=s'  => \$probe_file,
	'v|verbose'  => \$verbose
) or die($usage);

die($usage) if !defined($probe_file);

open(PROB,$probe_file) or die("Can't open file ($probe_file)");

my %probe=();
my $k=undef;#prob length
while(<PROB>){
	chomp;
	my ($id,$seq) = split /\t/;
	$probe{$id}=$seq;
	if(!defined($k)){
		$k=length($seq);
	}elsif(length($seq)!=$k){
		die("Pobes of different size found.");
	}
}

print STDERR "probes letti - ". `date`   if $verbose;
 
my $pos=-1;
$_=<>;
chomp;
die("Header fasta not found") if $_!~/^>/;

my $seqid = $_;
$seqid =~ s/^>//;
$_="";

while( 1 ){
	
	if(length($_) < $k){
		my $new_row=<>;
		#print STDERR length($_),$new_row;
		if(defined($new_row)){
			chomp $new_row;
			if($new_row =~ /^>/){
				#print STDERR "hash costruito - ". `date`  if $verbose;
				&print_coords();
				%words=();
				$seqid = $new_row;
				$seqid =~ s/^>//;
				$pos=-1;
				$_="";
				next;
			}
			$_.=$new_row;
		}else{
			last;
		}
	}

	my $w=substr($_,0,$k);
	substr($_,0,1,"");
	$pos+=1;
	next if $w!~/^[ACGT]+$/;
	$words{$w}.=$pos.",";
}
&print_coords();


sub print_coords
{
	while ( my ($probid, $seq) = each(%probe) ) {
		my $p = $words{$seq};
		if(defined($p)){
			$p=~s/,$//;
			print $seqid, $probid, $seq, '+', $p;
		}
		
		$seq = reverse $seq;
		$seq =~ tr/ACGT/TGCA/;
		$p = $words{$seq};
		if(defined($p)){
			$p=~s/,$//;
			print $seqid, $probid, $seq, '-', $p;
		}
	}

	print STDERR $seqid if $verbose;
}
