#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] [-c|chars] LEN
count the frequency of each word of LEN length, indipendently for each fasta block,
if -c is not given count only exixting words,
if -c has a value like \"ACGT\" then count all possible words of len LEN composed of such characters,
	even if not present in the fasta block (0 frequency)
\n";

my $help=0;
my $chars_opt=undef;
GetOptions (
	'h|help' => \$help,
	'c|chars=s' => \$chars_opt,
) or die($usage);

my $len = shift(@ARGV);
die $usage if not $len;

my @all_words = ();
if(defined($chars_opt)){
	my @chars = split("",$chars_opt);
	@all_words=@chars;
	for(my $i=0;$i<$len-1;$i++){
		@all_words = cartesian(\@all_words,\@chars)
	}
}

if($help){
	print $usage;
	exit(0);
}


my $header=undef;
my $block="";
while(<>){
	chomp;
	next if length==0;
	if(/^>/){
		my $post_header=$_;
		if(defined($header)){
			print $header;
			word_count($block);
			$block = "";
		}
		$header=$post_header;
		next;
	}
	$block.=$_;
}
if(length($block)>0){
	print $header;
	word_count($block);
}

sub word_count{
	my $s=shift;
	my $L = length($s);
	my %a=();
	my $word = substr($s,0,$len);
	while(length($word)==$len){
		$a{$word}++;
		substr($s,0,1,"");
		$word = substr($s,0,$len);
	}
	
	if(scalar(@all_words)!=0){
		for(@all_words){
			my $v=$a{$_};
			$v = 0 if not defined($v);
			print $_,$v / ($L- $len + 1)
		}
	}else{
		while ( my ($key, $value) = each %a )
		{
		  print $key, $a{$key}
		}
	}
}

sub cartesian{
	my $a_ref = shift;
	my @a=@{$a_ref};
	my $b_ref = shift;
	my @b=@{$b_ref};
	my @retval=();
	foreach my $i (@a){
		foreach my $j (@b){
			push(@retval,$i.$j)
		}
	}
	return @retval;
}
