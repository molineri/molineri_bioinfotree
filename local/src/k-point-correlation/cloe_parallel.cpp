#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <unistd.h>
#include <string>
#include <vector>
#include "tnt.h"
#include "StringTokenizer.h"

using namespace std;
using namespace TNT;

int setup(int argc, char* argv[]);
bool test_file(char * filename);
void usage();

void dump(	Array2D< float > &mtx,
		Array2D< bool > &prensent
	 );
bool parse_next_row(
		float value[],
		bool present[],
		unsigned int array_size,
		unsigned int header_cols_num,
		ifstream* myfile
	);
void read_index(vector<string> &index, char* filename, int file_head_rows );
void fill_matrix(	
		Array2D< float > &mtx,
		Array2D< bool> &present_mtx,
		int file_cols,
		int file_head_rows,
		int file_head_cols,
		char* filename,
		int data_cols
	);

int options_header_rows=-1;
int options_header_cols=-1;
int options_index_col=-1;
int options_file_num_cols=-1;
int options_file1_num_rows=-1;
int options_file2_num_rows=-1;
int options_n_min=-1;
char * options_input_filename1;
char * options_input_filename2;


int main( int argc, char* argv[] ){

	//controllo le opzioni passate da riga di comando
	if(setup(argc, argv)){
		cout << "main:: termined because setup error"<<endl;
		return(-1);
	}else{
		//atexit(stop);
	}

	vector <string> index1;
	vector <string> index2;

	read_index(index1, options_input_filename1, options_header_rows);
	read_index(index2, options_input_filename2, options_header_rows);

	//cerr << index1.size() << endl;
	//cerr << index2.size() << endl;
	//return 1;
	
	Array2D< float > mtx1(options_file1_num_rows-options_header_rows,options_file_num_cols-options_header_cols);
	Array2D< bool > present1(options_file1_num_rows-options_header_rows,options_file_num_cols-options_header_cols);
	Array2D< float > mtx2(options_file2_num_rows-options_header_rows,options_file_num_cols-options_header_cols);
	Array2D< bool > present2(options_file2_num_rows-options_header_rows,options_file_num_cols-options_header_cols);
	fill_matrix(	mtx1,
			present1, 
			options_file_num_cols, 
			options_header_rows, 
			options_header_cols, 
			options_input_filename1,
			options_file_num_cols-options_header_cols
		);
	fill_matrix(	mtx2,
			present2, 
			options_file_num_cols, 
			options_header_rows, 
			options_header_cols, 
			options_input_filename2,
			options_file_num_cols-options_header_cols
		);

	int i_max=mtx1.dim1();
	int j_max=mtx2.dim1();
	int k_max=mtx2.dim2();
	for(int i=0; i<i_max; i++){
		
		int j_min=0;
		if(strcmp(options_input_filename1,options_input_filename2)==0){
			j_min=i+1;
		}
		
		for(int j=j_min; j<j_max; j++){
			float s_x=0.;
			float s_y=0.;
			float s_xx=0.;
			float s_yy=0.;
			float s_xy=0.;
			int n=0;
				
			for(int k=0; k<k_max; k++){
				if(present1[i][k] and present2[j][k]){
					
					n++;

					s_x  += mtx1[i][k];
					s_xx += mtx1[i][k] * mtx1[i][k];
					
					s_y  += mtx2[j][k];
					s_yy += mtx2[j][k] * mtx2[j][k];
					
					s_xy += mtx1[i][k] * mtx2[j][k];
				
				}
			}
			cout << index1[i] << "\t" << index2[j] << "\t";
			float r=0.;
			if(n>=options_n_min){
				//float m_x=s_x/n;
				//float m_y=s_y/n;
				r = s_xy - s_x*s_y/n;
				r = r/sqrt( (s_xx-s_x*s_x/n) * (s_yy-s_y*s_y/n) );
				cout <<  r << "\t" << n << endl;
			}else{
				cout << "missing" << "\t" << n << endl;
			}
		}
	}
}

void read_index(vector<string> &index, char* filename, int file_head_rows){
	ifstream infile (filename);
	string line;
	for(int i=0;i<file_head_rows;i++){//perdo le righe di intestazione
		getline(infile,line,'\n');
	}
	while(getline(infile,line,'\n')){
	   	StringTokenizer strtok = StringTokenizer(line,"\t",false);
		index.push_back(strtok.nextToken());
	}
	infile.close();
}

void fill_matrix(	
		Array2D< float > &mtx,
		Array2D< bool> &present_mtx,
		int file_cols,
		int file_head_rows,
		int file_head_cols,
		char* filename,
		int data_cols
	){

	ifstream myfile (filename);
	for(int i=0;i<file_head_rows;i++){//perdo le righe di intestazione
		string line;
		getline(myfile,line,'\n');
	}

	
	float * value   = new float[data_cols];	//i dati di una singola riga
	bool * present  = new bool[data_cols];	//il dato di una colonna puo` non essere presente per una data riga
	
	int i=0;
	while(	parse_next_row(
				value, 
				present, 
				data_cols, 
				options_header_cols, 
				&myfile
			)
		){ 
		for(int j=0; j<data_cols; j++){
			mtx[i][j]=value[j+file_head_cols];
			present_mtx[i][j]=present[j+file_head_cols];
		}
		i++;
	}
	
	myfile.close();
	delete [] value;
	delete [] present;
}



int setup(int argc, char* argv[]){
	
	////////////////////////////////////////////////////////////////
	// gestione delle opzioni
	//
	int i;
	while((i=getopt(argc, argv, "r:s:c:h:p:f:g:n:"))!=-1){
		switch(i){
			case 'r': 
				options_file1_num_rows=atoi(optarg);
				break;
			case 's': 
				options_file2_num_rows=atoi(optarg);
				break;
			case 'h': 
				options_header_rows=atoi(optarg);
				break;
			case 'p':
				options_header_cols=atoi(optarg);
				break;
			case 'c':
				options_file_num_cols=atoi(optarg);
				break;
			case 'f':
				options_input_filename1=optarg;
				break;
			case 'g':
				options_input_filename2=optarg;
				break;
			case 'n':
				options_n_min=atoi(optarg);
				break;
			default:
				cerr<<"wrong options given"<<endl;
				usage();
				return -1;
		}	
	}
	if(
		options_file1_num_rows<0
		or
		options_file2_num_rows<0
		or
		options_header_rows<0 
		or 
		options_header_cols!=1
		or 
		options_file_num_cols<0
	){
		cerr<<"wrong options value. N.B. -p option must be == 1"<<endl;
		usage();
		return -1;
	}
	if(optind<argc){
		//vi sono parametri dopo l'elenco delle opzioni
		usage();
		return -1;
	}
	if(!test_file(options_input_filename1) or !test_file(options_input_filename2)){
		return -1;
	}
	return(0);
}

bool test_file(char * filename){
	ifstream test (filename,ios::in);
	if (!test.is_open()){
		cerr<<"ERROR: cloe_parallel::setup: cant open file '" << filename <<"'" << endl;
		return false;
	}
	test.close();
	return true;
}

void usage(){
	cout << "usage: cloe -r 1430 -h 1 -p 1 -c 100 -f filename1 -g filename2 -n 3"<<endl
		<<"\t-r number of rows in file1"<<endl
		<<"\t\tes: -r `cat file1 | wc -l`"<<endl
		<<"\t-s number of rows in file2"<<endl
		<<"\t\tes: -s `cat file2 | wc -l`"<<endl
		<<"\t-h number of header rows taht will be ignored"<<endl
		<<"\t-p number of header columns (used for indexing)"<<endl
		<<"\t-c number of columns in filename"<<endl
		<<"\t\t(head -1 tab_collapsed_homo | tr \"\\t\" \"\\n\" | wc -l) "<<endl
		<<"\t-f first file path"<<endl
		<<"\t-g second file path"<<endl
		<<"\t\tdata in filename1 and filename2 must be tab delimited"<<endl
		<<"\t\teach field not in header rows ol colls must be formatted like"<<endl
		<<"\t\t[+|-][nnnnn][.nnnnn][e|E[+|-]nnnn]"<<endl
		<<"\t\t'NaN' 'X' or empty string are accepted"<<endl
		<<"\t\tand considered missing values"<<endl
		<<"\t\t-n minimum common timepoit for calculate pearson"<<endl;
	exit(-1);
}


bool parse_next_row(
		float value[],
		bool present[],
		unsigned int array_size,
		unsigned int header_cols_num,
		ifstream* infile
	){
	
	unsigned int row_index=0;

	string line;
	if(getline(*infile,line,'\n')){
	   	StringTokenizer strtok = StringTokenizer(line,"\t",false);
	
		unsigned int ct=strtok.countTokens();
		if(ct - header_cols_num>array_size){
			cerr<<"WARNING: cloe_parallel::parse_next_row: array size < file column number"<<endl;
		};
		
		for(unsigned int j=0; j<header_cols_num; j++){//perdo le colonne di intestazione
			string tok=strtok.nextToken();
		}
		
		for(unsigned int j=0; j<array_size; j++){
			//cerr << "j: " << j << "\tj_min: " << j_min << endl; 
			string tok=strtok.nextToken();
			if(
//				(tok=="NaN")
//				or
//				(tok=="nan")
//				or
//				(tok=="X")
//				or
//				(tok=="x")
//				or
				(tok=="")
			){
				//cout<<"token absen: "<<token<<endl;
				value[j]=0.;
				present[j]=false;
			}else{
				//cout<<"token present: "<<token<<endl;
				value[j]=atof(tok.c_str());
				present[j]=true;
			}
		}
		return true;
	}else{
		return false;
	}
}

