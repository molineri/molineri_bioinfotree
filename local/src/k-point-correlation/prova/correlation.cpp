#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <unistd.h>
#include <string>
#include <vector>
#include "tnt.h"
#include "StringTokenizer.h"
#include "Histogram.h"

using namespace std;
using namespace TNT;

int setup(int argc, char* argv[]);
bool test_file(char * filename);
void usage(char** argv);

void dump(	Array2D< float > &mtx,
		Array2D< bool > &prensent
	 );
bool parse_next_row(
		float value[],
		bool present[],
		unsigned int array_size,
		unsigned int header_cols_num,
		ifstream* myfile
	);
void read_index(vector<string> &index, char* filename, int file_head_rows );
void fill_matrix(	
		Array2D< float > &mtx,
		Array2D< bool> &present_mtx,
		int file_cols,
		int file_head_rows,
		int file_head_cols,
		char* filename,
		int data_cols
	);
float correlate(
		float a[],
		float b[],
		float c[],
		bool present[],
		int k_max 
	);
int ssign(
		float x,
		float y,
		float z
	);
void center_normalize(
		float a[],
		bool present[],
		int data_cols,
		bool center
	);
void norm_euclide(
		float a[],
		bool present[],
		int data_cols,
		bool center
	);
double volume(
		float a[],
		float b[],
		float c[],
		bool present[],
		int k_max 
	);
		
double determinant(
		Array2D< double > &p
	);
double bisettr_dist(
		float a[],
		float b[],
		float c[],
		bool present[],
		int k_max
	);
float area_triangolo(
		float a[],
		float b[],
		float c[],
		bool present[],
		int k_max
	);


float options_cutoff=-2.;
bool options_histogram=false;
int options_header_rows=-1;
int options_header_cols=-1;
int options_index_col=-1;
int options_file_num_cols=-1;
int options_file1_num_rows=-1;
int options_file2_num_rows=-1;
int options_file3_num_rows=-1;
int options_n_min=-1;
char * options_input_filename1;
char * options_input_filename2;
char * options_input_filename3;


int main( int argc, char* argv[] ){

	//controllo le opzioni passate da riga di comando
	if(setup(argc, argv)){
		cout << "main:: termined because setup error"<<endl;
		return(-1);
	}

	vector <string> index1;
	vector <string> index2;
	vector <string> index3;

	read_index(index1, options_input_filename1, options_header_rows);
	read_index(index2, options_input_filename2, options_header_rows);
	read_index(index3, options_input_filename3, options_header_rows);
	
	Array2D< float > mtx1(options_file1_num_rows-options_header_rows,options_file_num_cols-options_header_cols);
	Array2D< bool >  present1(options_file1_num_rows-options_header_rows,options_file_num_cols-options_header_cols);
	Array2D< float > mtx2(options_file2_num_rows-options_header_rows,options_file_num_cols-options_header_cols);
	Array2D< bool >  present2(options_file2_num_rows-options_header_rows,options_file_num_cols-options_header_cols);
	Array2D< float > mtx3(options_file3_num_rows-options_header_rows,options_file_num_cols-options_header_cols);
	Array2D< bool >  present3(options_file3_num_rows-options_header_rows,options_file_num_cols-options_header_cols);

	Histogram *Hist = NULL;
	if(options_histogram){
		Hist = new Histogram(-1., 1., 256*256, options_file1_num_rows * options_file2_num_rows * options_file3_num_rows);
	}
	
	fill_matrix(	mtx1,
			present1, 
			options_file_num_cols, 
			options_header_rows, 
			options_header_cols, 
			options_input_filename1,
			options_file_num_cols-options_header_cols
		);
	fill_matrix(	mtx2,
			present2, 
			options_file_num_cols, 
			options_header_rows, 
			options_header_cols, 
			options_input_filename2,
			options_file_num_cols-options_header_cols
		);
	fill_matrix(	mtx3,
			present3, 
			options_file_num_cols, 
			options_header_rows, 
			options_header_cols, 
			options_input_filename3,
			options_file_num_cols-options_header_cols
		);

	int i_max=mtx1.dim1();
	int j_max=mtx2.dim1();
	int t_max=mtx3.dim1();
	int k_max=mtx2.dim2();
	for(int i=0; i<i_max; i++){
		
		int j_min=0;
		if(strcmp(options_input_filename1,options_input_filename2)==0){
			j_min = i+1;
		}
		
		for(int j=j_min; j<j_max; j++){
			
			int t_min=0;
			if(strcmp(options_input_filename2,options_input_filename3)==0){
				t_min = j+1;
			}
			if(strcmp(options_input_filename1,options_input_filename3)==0){
				if(i >= t_min){
					t_min = i+1;
				}
			}

			for(int t=t_min; t<t_max; t++){
				int n=0;
					
				bool * present  = new bool[k_max];
				float * a  = new float[k_max];
				float * b  = new float[k_max];
				float * c  = new float[k_max];
				for(int k=0; k<k_max; k++){
					if(present1[i][k] and present2[j][k] and present3[t][k]){
						present[k]=true;
						n++;
					}else{
						present[k]=false;
					}
					a[k]=mtx1[i][k];
					b[k]=mtx2[j][k];
					c[k]=mtx3[t][k];
				}
				
				if(!options_histogram){
					cout << index1[i] << "\t" << index2[j] << "\t" << index3[t] << "\t";
					if(options_cutoff < -1.){
						if(n>=options_n_min){
							cout << correlate(a,b,c,present,k_max) << "\t"
								<< volume(a,b,c,present,k_max) << "\t"
								<< bisettr_dist(a,b,c,present,k_max) << "\t"
								<< area_triangolo(a,b,c,present,k_max) << "\t"
								<< n << endl;
						}else{
							cout << "missing" << "\t" << n << endl;
						}
					}else{
						float r = correlate(a,b,c,present,k_max);
						if(r>options_cutoff){
							if(n>=options_n_min){
								cout << r << "\t"
									<< volume(a,b,c,present,k_max) << "\t"
									<< bisettr_dist(a,b,c,present,k_max) << "\t"
									<< area_triangolo(a,b,c,present,k_max) << "\t"
									<< n << endl;
							}else{
								cout << "missing" << "\t" << n << endl;
							}
						}
					}
				}else{
					if(n>=options_n_min){
						float r = correlate(a,b,c,present,k_max);
						Hist->add(r);
					}
				}
				delete a;
				delete b;
				delete c;
			}
		}
	}
	if(options_histogram){
		Hist->print();
	}
}

float correlate(float a[], float b[], float c[], bool present[], int k_max ){
	float * a_cent_norm  = new float[k_max];
	float * b_cent_norm  = new float[k_max];
	float * c_cent_norm  = new float[k_max];
	int i;
	for (i=0; i<k_max; i++){
		a_cent_norm[i]= a[i];
		b_cent_norm[i]= b[i];
		c_cent_norm[i]= c[i];
	}
	center_normalize(a_cent_norm, present, k_max, true);
	center_normalize(b_cent_norm, present, k_max, true);
	center_normalize(c_cent_norm, present, k_max, true);
	
	float s_xyz=0.;
	int n=0;
	for(int k=0; k<k_max; k++){
		if(present[k]){
			s_xyz += ssign(a_cent_norm[k],b_cent_norm[k],c_cent_norm[k]) * a_cent_norm[k] * b_cent_norm[k] * c_cent_norm[k];
			n++;
		}
	}
	
	//return s_xyz/(n - 1); 
	return s_xyz; 
}

int ssign (float x, float y, float z){
	int sign = 0; // count of positive elements
	sign += (x>=0);
	sign += (y>=0);
	sign += (z>=0);
	if ( (sign == 3) or (ssign == 0) ){
		return 1;
	} else {
		return -1;
	}
}

void center_normalize(float a[], bool present[], int data_cols, bool center){
	float s=0;
	int n=0;
	for(int j=0; j<data_cols; j++){
		if(present[j]){
			s += a[j];
			n++;
		}
	}
	float m=s/n;
	
	double stdev = 0;
	for(int j=0; j<data_cols; j++){
		if(present[j]){
			stdev += abs((a[j] - m)*(a[j] - m)*(a[j] - m));
		}
	}
	if (stdev == 0){
	// tutte le componenti sono uguali alla media
		if (center){
			for(int j=0; j<data_cols; j++){
				if(present[j]){
					a[j] = 0;
				}
			}
		}
		return;
	}
	//stdev /= (n-1);
	stdev = pow(stdev,1.0/3.0);

	if (!center){
		m = 0;
	}
	for(int j=0; j<data_cols; j++){
		if(present[j]){
			a[j] = (a[j] - m)/stdev;
		}
	}
}

void norm_euclide(double a[], bool present[], int data_cols, bool center){
	double s=0;
	int n=0;
	for(int j=0; j<data_cols; j++){
		if(present[j]){
			s += a[j];
			n++;
		}
	}
	double m=s/n;
	
	if (!center){
		m = 0;
	}
	double norma = 0;
	for(int j=0; j<data_cols; j++){
		if(present[j]){
			norma += abs((a[j] - m)*(a[j] - m));
		}
	}
	norma = sqrt(norma);
	if (norma == 0){
		return;
	}

	for(int j=0; j<data_cols; j++){
		if(present[j]){
			a[j] = (a[j] - m)/norma;
		}
	}
}

double volume(float a[], float b[], float c[], bool present[], int k_max ){
	Array2D< double > vett_matr(3,k_max);
	Array2D< double > matr_prod(3,3);
	int i,j;
	double * a_norm_eucl  = new double[k_max];
	double * b_norm_eucl  = new double[k_max];
	double * c_norm_eucl  = new double[k_max];
	for (i=0; i<k_max; i++){
		a_norm_eucl[i]= a[i];
		b_norm_eucl[i]= b[i];
		c_norm_eucl[i]= c[i];
	}

	norm_euclide(a_norm_eucl,present, k_max, false);
	norm_euclide(b_norm_eucl,present, k_max, false);
	norm_euclide(c_norm_eucl,present, k_max, false);

	bool ab_equal = true;
	bool ac_equal = true;
	bool bc_equal = true;
	for (i=0; i<k_max; i++){
		if (!present[i]){
			continue;
		}
		ab_equal = (ab_equal and (a_norm_eucl[i] == b_norm_eucl[i]));
		ac_equal = (ac_equal and (a_norm_eucl[i] == c_norm_eucl[i]));
		bc_equal = (bc_equal and (b_norm_eucl[i] == c_norm_eucl[i]));
	}
	if (ab_equal or ac_equal or bc_equal) {
		return 0;
	}

	int absent_i=0;
	for (i=0; i<k_max; i++){
		if (!present[i]){
			absent_i++;
			continue;
		}
		vett_matr[0][i-absent_i] = a_norm_eucl[i];
		vett_matr[1][i-absent_i] = b_norm_eucl[i];
		vett_matr[2][i-absent_i] = c_norm_eucl[i];
	}
	for (i=0; i<3; i++){
		for (j=0; j<3; j++){
			matr_prod[i][j] = 0;
			for (int k=0; k<k_max-absent_i; k++) {
				matr_prod[i][j] += vett_matr[i][k] * vett_matr[j][k];
			}
		}
	}

	return sqrt(abs(determinant(matr_prod)));
}

double determinant(Array2D< double > &p){
	if (p.dim1() != p.dim2()){
		cerr<<"ERROR: correl::determinant: non square matrix" <<endl;
		exit(1);
	}
	int n=p.dim1();
	double det;
	if (n>3){
		cerr<<"ERRORE: n>3; n="<<n<<endl;
		exit(1);
		//matr_prod e' 3x3!!
		det = 0.;
		Array2D< double > minore_compl(n-1,n-1);
		int i,j,k;
		for (k=0; k<n; k++){
			for (i=0; i<n-1; i++) {
				for (j=0; j<n-1; j++) {
					if (j<k) {
						minore_compl[i][j] = p[i+1][j];
					} else {
						minore_compl[i][j] = p[i+1][j+1];
					}
				}
			}
			int sgn = 1;
			if (k % 2 == 1) {
				sgn = -1;
			}
			det += p[0][k] * sgn * determinant(minore_compl);
		}
	} else {
		if (n==3){
			det = p[0][0] * p[1][1] * p[2][2]
				+ p[0][1] * p[1][2] * p[2][0]
				+ p[0][2] * p[1][0] * p[2][1]
				- p[2][0] * p[1][1] * p[0][2]
				- p[2][1] * p[1][2] * p[0][0]
				- p[2][2] * p[1][0] * p[0][1];
		} else {
			if (n==2){
				det = p[0][0] * p[1][1] - p[1][0] * p[0][1];
			} else {
				cerr << "ERROR: correl::determinant: wrong matrix size" << endl;
				exit(1);
			}
		}
	}
	return det;
}

double bisettr_dist(float a[], float b[], float c[], bool present[], int k_max ){
	double * a_cent_norm  = new double[k_max];
	double * b_cent_norm  = new double[k_max];
	double * c_cent_norm  = new double[k_max];
	for (int i=0; i<k_max; i++){
		a_cent_norm[i] = a[i];
		b_cent_norm[i] = b[i];
		c_cent_norm[i] = c[i];
	}
	norm_euclide(a_cent_norm, present, k_max, true);
	norm_euclide(b_cent_norm, present, k_max, true);
	norm_euclide(c_cent_norm, present, k_max, true);
	
	bool equal = true;
	for (int i=0; i<k_max; i++){
		equal = (equal and (a_cent_norm[i] == b_cent_norm[i] and a_cent_norm[i] == c_cent_norm[i]));
	}
	if (equal) {
		return 0;
	}

	double s_xyz=0.;
	int n=0;
	for(int k=0; k<k_max; k++){
		if(present[k]){
			double prod_scal = abs(a_cent_norm[k] + b_cent_norm[k] + c_cent_norm[k]);
			if (prod_scal > 0){
				prod_scal /= sqrt( 3 * ( (a_cent_norm[k] * a_cent_norm[k]) + 
					(b_cent_norm[k] * b_cent_norm[k]) + (c_cent_norm[k] * c_cent_norm[k]) ) );
			} else {
				prod_scal = 1;
			}
			s_xyz += (1 - prod_scal) * (1 - prod_scal);
			n++;
		}
	}
	s_xyz = sqrt(s_xyz);
	s_xyz /= n;
	
	return s_xyz; 
}

float area_triangolo(float a[], float b[], float c[], bool present[], int k_max ){
	float * l = new float[k_max];
	l[0] = 0;
	l[1] = 0;
	l[2] = 0;
	double * a_norm  = new double[k_max];
	double * b_norm  = new double[k_max];
	double * c_norm  = new double[k_max];
	for (int i=0; i<k_max; i++){
		a_norm[i] = a[i];
		b_norm[i] = b[i];
		c_norm[i] = c[i];
	}
	norm_euclide(a_norm, present, k_max, false);
	norm_euclide(b_norm, present, k_max, false);
	norm_euclide(c_norm, present, k_max, false);
	for(int k=0; k<k_max; k++){
		if(present[k]){
			l[0] += (a_norm[k] - b_norm[k]) * (a_norm[k] - b_norm[k]);
			l[1] += (a_norm[k] - c_norm[k]) * (a_norm[k] - c_norm[k]);
			l[2] += (b_norm[k] - c_norm[k]) * (b_norm[k] - c_norm[k]);
		}
	}
	l[0] = sqrt(l[0]); 
	l[1] = sqrt(l[1]); 
	l[2] = sqrt(l[2]); 
	float area = sqrt(pow( (l[0] * l[0] + l[1] * l[1] + l[2] * l[2]),2 ) 
			- 2 * ( pow(l[0],4) + pow(l[1],4) + pow(l[2],4) ) ) / 4;
	area /= 3.0/4*sqrt(3);
	return area;
}

void read_index(vector<string> &index, char* filename, int file_head_rows){
	ifstream infile (filename);
	string line;
	for(int i=0;i<file_head_rows;i++){//perdo le righe di intestazione
		getline(infile,line,'\n');
	}
	while(getline(infile,line,'\n')){
	   	StringTokenizer strtok = StringTokenizer(line,"\t",false);
		index.push_back(strtok.nextToken());
	}
	infile.close();
}

void fill_matrix(	
		Array2D< float > &mtx,
		Array2D< bool> &present_mtx,
		int file_cols,
		int file_head_rows,
		int file_head_cols,
		char* filename,
		int data_cols
	){

	ifstream myfile (filename);
	for(int i=0;i<file_head_rows;i++){//perdo le righe di intestazione
		string line;
		getline(myfile,line,'\n');
	}

	
	float * value   = new float[data_cols];	//i dati di una singola riga
	bool * present  = new bool[data_cols];	//il dato di una colonna puo` non essere presente per una data riga
	
	int i=0;
	while(	parse_next_row(
				value, 
				present, 
				data_cols, 
				options_header_cols, 
				&myfile
			)
		){

		int n=0;
		float s=0;
		for(int j=0; j<data_cols; j++){
			mtx[i][j]=value[j];
			present_mtx[i][j]=present[j];
			if(present[j]){
				n++;
				s += value[j];
			}

		}
		
		i++;
	}
	
	myfile.close();
	delete [] value;
	delete [] present;
}



int setup(int argc, char* argv[]){
	
	////////////////////////////////////////////////////////////////
	// gestione delle opzioni
	//
	int i;
	while((i=getopt(argc, argv, "C:Hi:j:k:c:h:p:n:"))!=-1){
		switch(i){
			case 'C':
				options_cutoff=atof(optarg);
				if(options_cutoff > 1. or options_cutoff < -1.){
					cerr << "correl::setup: options_cutoff > 1. or options_cutoff < -1." << endl;
					return -1;
				}
				break;
			case 'H':
				options_histogram=true;
				break;
			case 'i': 
				options_file1_num_rows=atoi(optarg);
				break;
			case 'j': 
				options_file2_num_rows=atoi(optarg);
				break;
			case 'k': 
				options_file3_num_rows=atoi(optarg);
				break;
			case 'h': 
				options_header_rows=atoi(optarg);
				break;
			case 'p':
				options_header_cols=atoi(optarg);
				break;
			case 'c':
				options_file_num_cols=atoi(optarg);
				break;
			case 'n':
				options_n_min=atoi(optarg);
				break;
			default:
				cerr<<"wrong options given"<<endl;
				usage(argv);
				return -1;
		}	
	}
	if(
		options_file1_num_rows<0
		or
		options_file2_num_rows<0
		or
		options_header_rows<0 
		or 
		options_header_cols!=1
		or 
		options_file_num_cols<0
	){
		cerr<<"wrong options value. N.B. -p option must be == 1"<<endl;
		usage(argv);
		return -1;
	}
	if(argc == optind + 3){
		//vi sono parametri dopo l'elenco delle opzioni
		options_input_filename1=argv[optind];
		optind++;
		options_input_filename2=argv[optind];
		optind++;
		options_input_filename3=argv[optind];
		optind++;
	}else{
		usage(argv);
		return -1;
	}
	if(!test_file(options_input_filename1) or !test_file(options_input_filename2) or !test_file(options_input_filename3)){
		return -1;
	}
	return(0);
}

bool test_file(char * filename){
	ifstream test (filename,ios::in);
	if (!test.is_open()){
		cerr<<"ERROR: correl::setup: cant open file '" << filename <<"'" << endl;
		return false;
	}
	test.close();
	return true;
}

void usage(char** argv){
	cout << "usage: " << argv[0] <<  " -i 203 -j 250 -k 180 -c 10 -n 8 -h 0 -p 1 filename1 filename2 filename3"<<endl
		<<"\t-i number of rows in filename1"<<endl
		<<"\t\tes: -i `cat filename1 | wc -l`"<<endl
		<<"\t-j number of rows in filename2"<<endl
		<<"\t\tes: -j `cat filename2 | wc -l`"<<endl
		<<"\t-k number of rows in filename3"<<endl
		<<"\t\tes: -k `cat filename3 | wc -l`"<<endl
		<<"\t[-C] Pearson cutoff"<<endl
		<<"\t[-H] prints out Pearson histogram"<<endl
		<<"\t-h number of header rows that will be ignored"<<endl
		<<"\t-p number of header columns (used for indexing)"<<endl
		<<"\t-c total number of columns in filename"<<endl
		<<"\t[-n] minimum common timepoints number to calculate correlation"<<endl
		<<"\t\t(head -1 tab_collapsed_homo | tr \"\\t\" \"\\n\" | wc -l) "<<endl
		<<"\tdata in filename* must be tab delimited"<<endl
		<<"\teach field not in header rows or cols must be formatted like"<<endl
		<<"\t[+|-][nnnnn][.nnnnn][e|E[+|-]nnnn]"<<endl
		<<"\tempty strings are accepted"<<endl
		<<"\tand considered missing values"<<endl
		<<"OUTPUT:"<<endl
		<<"\t1. id_1"<<endl
		<<"\t2. id_2"<<endl
		<<"\t3. id_3"<<endl
		<<"\t4. Pearson -1 <= r <= 1"<<endl
		<<"\t5. volume del solido multidimensionale definito dai 3 vettori normalizzati (norma euclidea 1), 0 <= vol <= 1"<<endl
		<<"\t6. distanza quadratica media delle terne a_i, b_i, c_i normalizzate dalla bisettrice 3D, 0 <= d <= 1)"<<endl
		<<"\t7. area del triangolo definito dai 3 vettori"<<endl
		<<"\t8. numero di punti su cui sono state calcolate le correlazioni"<<endl;
	exit(-1);
}


bool parse_next_row(
		float value[],
		bool present[],
		unsigned int array_size,
		unsigned int header_cols_num,
		ifstream* infile
	){
	
	string line;
	if(getline(*infile,line,'\n')){
	   	StringTokenizer strtok = StringTokenizer(line,"\t",false);
	
		unsigned int ct=strtok.countTokens();
		if(ct - header_cols_num>array_size){
			cerr<<"WARNING: correl::parse_next_row: array size < file column number"<<endl;
		};
		
		for(unsigned int j=0; j<header_cols_num; j++){//perdo le colonne di intestazione
			string tok=strtok.nextToken();
		}
		
		for(unsigned int j=0; j<array_size; j++){
			//cerr << "j: " << j << "\tj_min: " << j_min << endl; 
			string tok=strtok.nextToken();
			if(
//				(tok=="NaN")
//				or
//				(tok=="nan")
//				or
//				(tok=="X")
//				or
//				(tok=="x")
//				or
				(tok=="")
			){
//				cout<<"token absen: "<<tok<<endl;
				value[j]=0.;
				present[j]=false;
			}else{
				//cout<<"token present: "<<token<<endl;
				value[j]=atof(tok.c_str());
				present[j]=true;
			}
		}
		return true;
	}else{
		return false;
	}
}

