#include "Histogram.h"

Histogram::Histogram(float min,float max, int bin_n, int expected_number){
	if(expected_number >= (std::numeric_limits<int>::max)()){
		cerr << "Histogram::Histogram: possible int overflow" << endl;
		exit(-1);
	}
	count = new int[bin_n];
	this->min = min;
	this->max = max;
	this->tot_span = max-min;
	this->span = this->tot_span/bin_n;
	this->bin_n = bin_n;
}

void Histogram::add(float f){
	int bin;
	if(f!=this->max){
		bin = (int)((f - this->min) / this->span);
	}else{
		bin = bin_n - 1;
	}
	this->count[bin]++;
}

void Histogram::print(){
	for(int i=0; i<this->bin_n; i++){
		cout << i*this->span + this->min << "\t" << this->count[i] << endl; 
	}
}

