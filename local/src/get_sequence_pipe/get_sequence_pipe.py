#!/usr/bin/env python

from collections import deque
from optparse import OptionParser
from os.path import isdir, join
from sys import exit, stdin, stdout
from vfork.fasta.reader import SingleBlockReader

class ReaderCache(object):
	def __init__(self, base_dir, cache_size):
		assert cache_size > 0
		
		self.base_dir = base_dir
		self.cache_size = cache_size
		self.readers = {}
		self.by_age = deque()
	
	def get(self, sequence_name):
		try:
			return self.readers[sequence_name]
		except KeyError:
			try:
				filename = join(self.base_dir, '%s.fa' % sequence_name)
				reader = SingleBlockReader(filename)
			except IOError:
				try:
					filename = join(self.base_dir, 'chr%s.fa' % sequence_name)
					reader = SingleBlockReader(filename)
				except IOError:
					raise ValueError, 'no such sequence'
			
			if len(self.by_age) > self.cache_size:
				old_reader = self.readers.pop(self.by_age.popleft())
				old_reader.close()
				
			self.readers[sequence_name] = reader
			self.by_age.append(sequence_name)
			return reader

def main():
	parser = OptionParser(usage='%prog SEQUENCE_DIR')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	elif not isdir(args[0]):
		exit('Invalid sequence directory: %s' % args[0])
	
	reader_cache = ReaderCache(args[0], 2)
	while True:
		line = stdin.readline()
		if len(line) == 0:
			break
		line = line.rstrip()
		
		try:
			tokens = line.split('\t')
			if len(tokens) != 3:
				raise ValueError
				
			start = int(tokens[1])
			stop = int(tokens[2])
			if start < 0 or stop <= start:
				raise ValueError
			
			reader = reader_cache.get(tokens[0])
		
		except ValueError:
			exit('Invalid sequence coordinates: %s' % line)
		
		stdout.write(reader.get(start, stop-start))
		stdout.write('\n')
		stdout.flush()

if __name__ == '__main__':
	main()
