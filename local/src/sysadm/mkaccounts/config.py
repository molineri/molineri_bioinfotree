# Copyright (C) 2008 by Gabriele Sales (gbrsales@gmail.com)

try:
	from pyparsing import *
except ImportError:
	from sys import exit
	exit('Cannot import module pyparsing.\nThis most likely means that library pyparsing is missing; please install it.')

class Configuration(object):
	def __init__(self):
		self.root = None
		self.base_group = None
		self.user_group = None
		self.groups = []
		self.users = []
		self.policies = []
	
	def __repr__(self):
		res = [
			'<Configuration instance:',
			'root: %s' % self.root,
			'base_group: %r' % self.base_group,
			'user_group: %r' % self.user_group,
			'groups: %r' % self.groups,
			'users: %r' % self.users,
			'policies: %r' % self.policies,
			'>'
		]
		return '\n'.join(res)
	
	@classmethod
	def from_file(klass, filename):
		return ConfigReader(klass).parseFile(filename)

class ConfigReader(object):
	equal = Literal('=').suppress()
	
	def __init__(self, config_class):
		self.config_class = config_class
		
		comment = Literal('#') + SkipTo(LineEnd())
		name = Word(alphas, alphanums + '_')
		number = Word(nums)
		path = Word('/', alphanums + '_-/')
		mode_sep = Optional('-').suppress()
		mode = Combine(('r' | mode_sep) + ('w' | mode_sep) + ('x' | mode_sep))
		mode_ext_block = Group((Literal('u') | Literal('g') | Literal('o')) + (Literal('+') | Literal('-') | Literal(':')) + mode)
		mode_ext = mode_ext_block + Optional(Literal(',').suppress() + mode_ext_block)
		boolean = CaselessLiteral('true') | CaselessLiteral('false') | CaselessLiteral('yes') | CaselessLiteral('no')
		acl = Group((Literal('u') | Literal('g')) + Literal(':').suppress() + name + Literal(':').suppress() + mode)
		
		root = self._assignment('root', path, self._set_root)
		
		gid = self._assignment('gid', number, self._set_gid)
		base_group_block = self._block('base_group', name, [gid], self._reset_group, self._set_base_group)
		
		user_group_block = self._block('user_group', name, [gid], self._reset_group, self._set_user_group)
		
		group_path = self._assignment('path', path, self._set_group_path)
		mode = self._assignment('mode', mode, self._set_mode)
		group_block = self._block('group', name, [gid, group_path, mode], self._reset_group, self._add_group)
		
		uid = self._assignment('uid', number, self._set_uid)
		admin = self._assignment('admin', boolean, self._set_admin)
		group_membership = self._assignment('group', name, self._add_group_membership)
		user_block = self._block('user', name, [uid, admin, group_membership], self._reset_user, self._add_user)
		
		user = self._assignment('user', name, self._set_user)
		policy_mode = self._assignment('mode', mode_ext, self._set_policy_mode)
		policy_acl = self._assignment('acl', acl, self._add_acl)
		reset_acl = self._entry(Literal('reset_acl')).setParseAction(self._set_reset_acl)
		dir_block = self._block('dir', path, [user, policy_mode, policy_acl, reset_acl], self._reset_dir, self._add_dir)
		subtree_block = self._block('subtree', path, [user, policy_mode, policy_acl, reset_acl], self._reset_dir, self._add_subtree)
		policy_block = self._block('policy', name, [dir_block, subtree_block], self._reset_policy, self._add_policy)
		
		self.parser = StringStart() + ZeroOrMore(root | base_group_block | user_group_block | group_block | user_block | policy_block) + StringEnd()
		self.parser.ignore(comment)
	
	def parseFile(self, filename):
		self.cfg = self.config_class()
		
		try:
			self.parser.parseFile(filename)
		except ParseException, e:
			raise ConfigError, '%s in file %s' % (str(e), filename)
		
		if self.cfg.root is None:
			raise ConfigError, 'missing root definition'
		elif self.cfg.base_group is None:
			raise ConfigError, 'missing base group definition'
		elif self.cfg.user_group is None:
			raise ConfigError, 'missing user group definition'
		elif self.cfg.base_group == self.cfg.user_group:
			raise ConfigError, 'base and user group must be different'
		
		return self.cfg
	
	def _entry(self, expr):
		return Or([expr + LineEnd().suppress(),
		           expr + Or([Literal(';'), FollowedBy('}')]).suppress()])
	
	def _assignment(self, head, value, action):
		expr = Literal(head) + self.equal + value
		expr.setParseAction(action)
		return self._entry(expr)
	
	def _block(self, label, name, attributes, entry_action, exit_action):
		name = name.copy().setParseAction(entry_action)
		attributes = ZeroOrMore(Or(attributes))
		block = Literal(label) + name + Literal('{').suppress() + attributes + Literal('}').suppress()
		block.setParseAction(exit_action)
		return block
	
	def _set_root(self, src, loc, res):
		if self.cfg.root is None:
			self.cfg.root = res[1]
		else:
			self._make_error(src, loc, 'duplicated definition of root')
	
	def _reset_group(self, src, loc, res):
		name = res[0]
		if name in [ g[0] for g in self.cfg.groups ] or \
		   (self.cfg.base_group and name == self.cfg.base_group['name']) or \
		   (self.cfg.user_group and name == self.cfg.user_group['name']):
			self._make_error(src, loc, 'duplicated group name')
		
		self.gid = None
		self.paths = []
		self.mode = None
	
	def _set_base_group(self, src, loc, res):
		if self.gid is None:
			raise ConfigError, 'missing gid attribute in the definition of group %s' % res[1]
		
		attributes = {
			'name' : res[1],
			'gid' : self.gid
		}
		self.cfg.base_group = attributes

	def _set_user_group(self, src, loc, res):
		if self.gid is None:
			raise ConfigError, 'missing gid attribute in the definition of group %s' % res[1]
		
		attributes = {
			'name' : res[1],
			'gid' : self.gid
		}
		self.cfg.user_group = attributes
	
	def _set_gid(self, src, loc, res):
		if self.gid is not None:
			self._make_error(src, loc, 'duplicated gid definition')
		
		gid = int(res[1])
		if gid in [ i['gid'] for n,i in self.cfg.groups ] or \
		   (self.cfg.base_group and gid == self.cfg.base_group['gid']) or \
		   (self.cfg.user_group and gid == self.cfg.user_group['gid']):
			self._make_error(src, loc, 'found gid already used by another group')
		
		self.gid = gid
	
	def _set_group_path(self, src, loc, res):
		path = res[1]
		if path in self.paths:
			self._make_error(src, loc, 'duplicated path definition')
		else:
			self.paths.append(res[1])
	
	def _set_mode(self, src, loc, res):
		if self.mode is not None:
			self._make_error(src, loc, 'duplicated mode definition')
		else:
			self.mode = res[1]
	
	def _add_group(self, src, loc, res):
		if self.gid is None:
			raise ConfigError, 'missing gid attribute in the definition of group %s' % res[1]
		elif len(self.paths) == 0:
			raise ConfigError, 'missing path attribute in the definition of group %s' % res[1]
		elif self.mode is None:
			raise ConfigError, 'missing mode attribute in the definition of group %s' % res[1]
		
		attributes = {
			'gid' : self.gid,
			'paths' : self.paths,
			'mode' : self.mode
		}
		self.cfg.groups.append((res[1], attributes))
	
	def _reset_user(self, src, loc, res):
		name = res[0]
		if name in [ u[0] for u in self.cfg.users ]:
			self._make_error(src, loc, 'duplicated user name')
		
		self.uid = None
		self.admin = None
		self.groups = []
	
	def _set_uid(self, src, loc, res):
		if self.uid is not None:
			self._make_error(src, loc, 'duplicated uid definition')
		
		uid = int(res[1])
		if uid in [ i['uid'] for n,i in self.cfg.users ]:
			self._make_error(src, loc, 'found uid already used by another user')
		
		self.uid = uid
	
	def _set_admin(self, src, loc, res):
		if self.admin is not None:
			self._make_error(src, loc, 'duplicated admin definition')
		
		if res[1].lower() in ('true', 'yes'):
			self.admin = True
		else:
			self.admin = False
	
	def _add_group_membership(self, src, loc, res):
		group = res[1]
		if group in self.groups:
			self._make_error(src, loc, 'duplicated group membership')
		elif group not in [ g[0] for g in self.cfg.groups ]:
			self._make_error(src, loc, 'reference to an undefined group')
		
		self.groups.append(group)
	
	def _add_user(self, src, loc, res):
		if self.uid is None:
			raise ConfigError, 'missing uid attribute in the definition of user %s' % res[1]
		
		if self.admin is None:
			self.admin = False
		
		attributes = {
			'uid' : self.uid,
			'admin' : self.admin,
			'groups' : self.groups
		}
		self.cfg.users.append((res[1], attributes))
	
	def _reset_policy(self, src, loc, res):
		name = res[0]
		if name in [ p[0] for p in self.cfg.policies ]:
			self._make_error(src, loc, 'duplicated policy name')
		
		self.policy = []
		
	def _reset_block(self, src, loc, res):
		path = res[0]
		if path in [ b[0] for b in self.policy ]:
			self._make_error(src, loc, 'duplicated path definition')
		
		self.user = None
		self.mode = None
		self.reset_acl = False
		self.acls = []
	
	def _reset_dir(self, src, loc, res):
		self._reset_block(src, loc, res)
		self.block_type = 'dir'
	
	def _set_user(self, src, loc, res):
		if self.user is not None:
			self._make_error(src, loc, 'duplicated user definition')
		
		user = res[1]
		if user not in [ u[0] for u in self.cfg.users ]:
			self._make_error(src, loc, 'reference to an undefined user')
		else:
			self.user = user
	
	def _set_policy_mode(self, src, loc, res):
		if self.mode is not None:
			self._make_error('duplicated mode definition')
		
		mode = []
		for role, action, flags in res[1:]:
			role = self._translate_role(role)
			if role in [ m[0] for m in mode ]:
				self._make_error(src, loc, 'duplicate role in mode definition')
			
			action = {
				'+' : 'add',
				'-' : 'remove',
				':' : 'set'
			}[action]
			
			mode.append((role, action, flags))
		
		self.mode = mode
	
	def _set_reset_acl(self, src, loc, res):
		if self.reset_acl:
			self._make_error(src, loc, 'duplicate reset_acl entry')
		else:
			self.reset_acl = True
	
	def _add_acl(self, src, loc, res):
		role, qualifier, mode = res[1]
		role = self._translate_role(role)
		
		if role == 'user':
			if qualifier not in [ u[0] for u in self.cfg.users ]:
				self._make_error(src, loc, 'ACL references an undefined user')
		elif role == 'group':
			if qualifier not in [ g[0] for g in self.cfg.groups ] and \
			   (self.cfg.base_group and qualifier != self.cfg.base_group['name']) and \
			   (self.cfg.user_group and qualifier != self.cfg.user_group['name']):
				self._make_error(src, loc, 'ACL references an undefined group')
		
		if (role, qualifier) in [ a[:2] for a in self.acls ]:
			self._make_error(src, loc, 'duplicated ACL')
		
		self.acls.append((role, qualifier, mode))
	
	def _add_dir(self, src, loc, res):
		self._add_policy_entry(src, loc, res, 'dir')
	
	def _add_subtree(self, src, loc, res):
		self._add_policy_entry(src, loc, res, 'subtree')
	
	def _add_policy_entry(self, src, loc, res, entry_type):
		if self.user is None and self.mode is None and len(self.acls) == 0:
			self._make_error(src, loc, 'empty definition')
		
		attributes = { 'type' : entry_type, 'reset_acl' : self.reset_acl }
		if self.user:
			attributes['user'] = self.user
		if self.mode:
			attributes['mode'] = self.mode
		if len(self.acls):
			attributes['acl'] = self.acls
		
		self.policy.append((res[1], attributes))
	
	def _add_policy(self, src, loc, res):
		if len(self.policy) == 0:
			self._make_error(src, loc, 'empty policy')
		
		self.cfg.policies.append((res[1], self.policy))
	
	def _translate_role(self, role):
		return {
			'u' : 'user',
			'g' : 'group',
			'o' : 'others'
		}[role]
	
	def _make_error(self, src, loc, msg):
		raise ConfigError, '%s at line %d' % (msg, lineno(loc+1, src))

class ConfigError(Exception):
	''' An error in the configuration. '''
