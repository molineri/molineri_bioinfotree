#!/usr/bin/env python
#
# Copyright (C) 2008 by Gabriele Sales (gbrsales@gmail.com)
#

from account import User, Group, GroupError, UserError
from config import Configuration, ConfigError
from optparse import OptionParser
from os import getuid
from os.path import join
from perm import make_base_acl, update_perms
from policy import Policy
from sys import exit
from walker import Walker, PathPicker
import stat as st

def get_base_groups(cfg):
	base = Group(cfg.base_group['name'], cfg.base_group['gid'])
	if not base.exists():
		base.create()
	
	users = Group(cfg.user_group['name'], cfg.user_group['gid'])
	if not users.exists():
		users.create()
	
	return {
		'__base__'  : base,
		base.name   : base,
		'__users__' : users,
		users.name  : users
	}

def get_ext_groups(cfg, groups):
	for name, info in cfg.groups:
		if name in groups:
			raise GroupError, 'group %s is defined twice' % name
		
		group = Group(name, info['gid'])
		if not group.exists():
			group.create()
		groups[group.name] = group

def get_user_map(cfg, groups):
	uids = set()
	user_map = {}
	base_group = groups['__base__']
	user_group = groups['__users__']
	
	for name, info in cfg.users:
		user = User(name, info['uid'])
		if not user.exists():
			user.create(user_group.name)
		elif not user_group.is_member(user):
			user_group.add_member(user)
		
		if info.get('admin', False):
			if not base_group.is_member(user):
				base_group.add_member(user)
		else:
			if base_group.is_member(user):
				base_group.remove_member(user)
		
		for group in groups.itervalues():
			if group is base_group or group is user_group:
				continue
			
			should_be_member = group.name in info['groups']
			if should_be_member and not group.is_member(user):
				group.add_member(user)
			elif not should_be_member and group.is_member(user):
				group.remove_member(user)
		
		uids.add(user.uid)
		user_map[user.name] = user.uid
	
	return user_map

def iter_group_infos(cfg):
	for name, info in cfg.groups:
		for path in info['paths']:
			yield path, (info['gid'], info['mode'])

def iter_policy_infos(cfg, user_map, groups):
	for name, policy in cfg.policies:
		for path, info in policy:
			yield path, Policy(info, user_map, groups)

def fix_mode(info):
	mode = st.S_IMODE(info.st_mode)
	mode |= (st.S_IRGRP | st.S_IWGRP)
	mask = ~(st.S_IROTH | st.S_IWOTH | st.S_IXOTH | st.S_ISUID)
	mode &= mask
	
	mask = st.S_IXUSR | st.S_IXGRP
	if st.S_ISDIR(info.st_mode):
		return mode | mask | st.S_ISGID
	elif mode & mask:
		return mode | mask
	else:
		return mode & (~mask)

def fix_perms(cfg, user_map, groups):
	base_gid = groups['__base__'].gid
	walker = Walker(cfg.root)
	walker.attach_mark(make_base_acl(groups['__users__'].gid))
	groups = PathPicker(cfg.root, iter_group_infos(cfg))
	policies = PathPicker(cfg.root, iter_policy_infos(cfg, user_map, groups))
	
	for path, info, files in walker:
		uid = info.st_uid
		gid = base_gid
		mode = fix_mode(info)
		acl = walker.get_mark()
		
		gs = groups.by_path(path)
		ps = policies.by_path(path)
		if len(gs) or len(ps):
			acl = acl.clone()
		
		if len(gs):
			for acl_gid, acl_mode in gs:
				acl.extend('group', acl_gid, acl_mode)
			walker.attach_mark(acl)
		
		local_acl = acl
		if len(ps):
			has_local_ps = False
			for p in ps:
				if p.recursive:
					uid, gid, mode = p.apply(uid, gid, mode, acl)
				else:
					has_local_ps = True
					
			walker.attach_mark(acl)
			
			if has_local_ps:
				local_acl = acl.clone()
				for p in ps:
					if not p.recursive:
						uid, gid, mode = p.apply(uid, gid, mode, local_acl)
		
		update_perms(path, info, uid, gid, mode, local_acl, acl)
		for f, info in files:
			update_perms(f, info, uid, gid, mode, acl)

def main():
	parser = OptionParser(usage='%prog CONFIG')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	elif getuid() != 0:
		exit('This program requires super-user privileges.')
	
	try:
		cfg = Configuration.from_file(args[0])
	except IOError, e:
		exit('Error opening configuration file: %s' % e.args[1])
	except ConfigError, e:
		exit('Error processing configuration: %s' % e.args[0])
		
	groups = get_base_groups(cfg)
	get_ext_groups(cfg, groups)
	user_map = get_user_map(cfg, groups)
	fix_perms(cfg, user_map, groups)

if __name__ == '__main__':
	main()
