# Copyright (C) 2008 by Gabriele Sales (gbrsales@gmail.com)

import stat as st

class Policy(object):
	def __init__(self, spec, user_map, groups):
		self.recursive = spec.get('type', 'subtree') == 'subtree'
		self.uid = self._load_user(spec.get('user', None), user_map)
		self.mode = spec.get('mode', [])
		self.reset_acl = spec['reset_acl']
		self.acls = self._load_acls(spec.get('acls', []), user_map, groups)
	
	def apply(self, uid, gid, mode, acl):
		if self.uid is not None:
			uid = self.uid
		
		for role, action, flags in self.mode:
			if role == 'user':
				flags = self._user_mode(flags)
			elif role == 'group':
				flags = self._group_mode(flags)
			elif role == 'others':
				flags = self._other_mode(flags)
			else:
				raise ValueError, 'invalid mode role'
			
			if action == 'set':
				mode = flags
			elif action == 'add':
				mode |= flags
			elif action == 'remove':
				mode &= ~flags
			else:
				raise ValueError, 'invalid mode action'
		
		if self.reset_acl:
			acl.reset()
		for role, qualifier, acl_mode in self.acls:
			acl.extend(role, qualifier, acl_mode)
		
		return uid, gid, mode
	
	def _load_user(self, name, user_map):
		if name is None:
			return name
		else:
			return user_map[name]
	
	def _load_acls(self, acls, user_map, groups):
		def convert_id(acl):
			role, qualifier, mode = acl
			if role == 'user':
				return (role, user_map[qualifier], mode)
			elif role == 'group':
				return (role, groups[qualifier].gid, mode)
			else:
				raise ValueError, 'invalid acl role'
		
		return [ convert_id(a) for a in acls ]
	
	def _user_mode(self, mode):
		flags = 0
		if 'r' in mode:
			flags |= st.S_IRUSR
		if 'w' in mode:
			flags |= st.S_IWUSR
		if 'x' in mode:
			flags |= st.S_IXUSR
		return flags
	
	def _group_mode(self, mode):
		flags = 0
		if 'r' in mode:
			flags |= st.S_IRGRP
		if 'w' in mode:
			flags |= st.S_IWGRP
		if 'x' in mode:
			flags |= st.S_IXGRP
		return flags
	
	def _other_mode(self, mode):
		flags = 0
		if 'r' in mode:
			flags |= st.S_IROTH
		if 'w' in mode:
			flags |= st.S_IWOTH
		if 'x' in mode:
			flags |= st.S_IXOTH
		return flags
