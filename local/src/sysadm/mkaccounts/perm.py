# Copyright (C) 2008 by Gabriele Sales (gbrsales@gmail.com)

from os import chmod, chown
import stat as st

try:
	from posix1e import ACL as PosixACL
	from posix1e import Entry, Permset, ACL_USER, ACL_GROUP, ACL_WRITE, ACL_READ, ACL_EXECUTE, ACL_TYPE_DEFAULT
except ImportError:
	from sys import exit
	exit('Cannot import module posix1e.\nThis most likely means that library pylibacl is missing; please install it.')

def make_base_acl(user_gid):
	acl = ACL()
	acl.extend('group', user_gid, 'rx')
	return acl

class ACL(object):
	pacl = PosixACL()
	
	def __init__(self):
		self.entries = []
	
	def __repr__(self):
		res = [ '<ACL object:' ]
		for entry in self.entries:
			if entry.tag_type == ACL_USER:
				role = 'user'
			elif entry.tag_type == ACL_GROUP:
				role = 'group'
			else:
				res.append('\tunknown')
				continue
			
			res.append('\t%s %d: %s' % (role, entry.qualifier, str(entry.permset)))
		
		res.append('>')
		return '\n'.join(res)
	
	def clone(self):
		acl = ACL()
		acl.entries = self.entries[:]
		return acl
	
	def reset(self):
		self.entries = []
	
	def extend(self, role, qualifier, perms):
		if role == 'user':
			tag_type = ACL_USER
		elif role == 'group':
			tag_type = ACL_GROUP
		else:
			raise ValueError, 'invalid role'
		
		self.entries = [ e for e in self.entries if (e.tag_type != tag_type or e.qualifier != qualifier) ]
		if len(perms.replace('-', '')) == 0:
			return
		
		e = Entry(self.pacl)
		e.tag_type = tag_type
		e.qualifier = qualifier
		
		if 'r' in perms:
			e.permset.read = True
		if 'w' in perms:
			e.permset.write = True
		if 'x' in perms:
			e.permset.execute = True
		
		self.entries.append(e)

def update_perms(path, info, uid, gid, mode, acl, default_acl=None):
	blocked = False
	if info.st_uid != uid or info.st_gid != gid:
		chmod(path, 0)
		chown(path, uid, gid)
		blocked = True
	
	if st.S_IMODE(info.st_mode) != mode:
		chmod(path, mode)
	
	pacl = PosixACL(mode=mode)
	for entry in acl.entries:
		pacl.append(entry)
	pacl.calc_mask()
	
	if not pacl == PosixACL(file=path):
		pacl.applyto(path)
	
	if default_acl and st.S_ISDIR(info.st_mode):
		pdacl = PosixACL(mode=mode)
		for entry in default_acl.entries:
			pdacl.append(entry)
		pdacl.calc_mask()
		
		if pdacl != PosixACL(filedef=path):
			pdacl.applyto(path, ACL_TYPE_DEFAULT)
	
	if blocked:
		chown(path, uid, gid)
