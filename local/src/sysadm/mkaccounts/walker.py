# Copyright (C) 2008 by Gabriele Sales (gbrsales@gmail.com)

from bisect import bisect_left
from os import listdir, lstat
from os.path import join
from stat import S_ISDIR, S_ISREG

class Walker(object):
	def __init__(self, root):
		self.walk_stack = [ [(root, lstat(root))] ]
		self.client_stack = [ None ]
	
	def __iter__(self):
		return self
	
	def next(self):
		path, dir_info = self._pop_dir()

		files = []
		dirs = []
		for item in listdir(path):
			item_path = join(path, item)
			info = lstat(item_path)

			if S_ISREG(info.st_mode):
				files.append((item_path, info))
			elif S_ISDIR(info.st_mode):
				dirs.append((item_path + '/', info))
		
		self._push_dirs(dirs)
		return path, dir_info, files

	def attach_mark(self, obj):
		self.client_stack[-1] = obj
	
	def get_mark(self):
		return self.client_stack[-1]

	def _pop_dir(self):
		while True:
			if len(self.walk_stack) == 0:
				raise StopIteration
			
			last_level = self.walk_stack[-1]
			if len(last_level) == 0:
				del self.walk_stack[-1]
				del self.client_stack[-1]
			else:
				return last_level.pop()
	
	def _push_dirs(self, dirs):
		self.walk_stack.append(dirs)
		self.client_stack.append(self.client_stack[-1])

class PathPicker(object):
	def __init__(self, root, item_iterator):
		self.root_len = len(root)
		if root[-1] == '/':
			self.root_len -= 1
		
		aux = []
		for path, item in item_iterator:
			if path != '/':
				path += '/'
			aux.append((path, item))
		aux.sort()
		
		self.paths = [ i[0] for i in aux ]
		self.items = [ i[1] for i in aux ]

	def by_path(self, path):
		suffix = path[self.root_len:]
		start_pos = bisect_left(self.paths, suffix)
		pos = start_pos
		while pos < len(self.paths) and self.paths[pos] == suffix:
			pos += 1
		return self.items[start_pos:pos]
