#
# Copyright (C) 2008 by Gabriele Sales (gbrsales@gmail.com)
#

from grp import getgrnam
from os import system
from pwd import getpwnam
import re

_name_rx = re.compile(r'^[a-zA-Z][a-zA-Z0-9_]*$')

class User(object):
	def __init__(self, name, uid):
		self._check_name(name)
		self.name = name
		self.uid = uid
	
	def exists(self):
		try:
			info = getpwnam(self.name)
			if info.pw_uid != self.uid:
				raise UserError, 'invalid uid for user ' + self.name
			return True
		except KeyError:
			return False
	
	def create(self, group):
		if system('useradd -m -u %d -g users -G users,%s %s >/dev/null' % (self.uid, group, self.name)) != 0:
			raise UserError, 'error creating user ' + self.name
	
	def _check_name(self, name):
		if _name_rx.match(name) is None:
			raise UserError, 'invalid user name ' + self.name

class UserError(Exception):
	pass

class Group(object):
	def __init__(self, name, gid):
		self._check_name(name)
		self.name = name
		self.gid = gid
		self.members = None
	
	def exists(self):
		try:
			info = getgrnam(self.name)
			if info.gr_gid != self.gid:
				raise GroupError, 'invalid gid for group ' + self.name
			self.members = set(info.gr_mem)
			return True
		except KeyError:
			return False
	
	def create(self):
		if system('groupadd -g %d %s >/dev/null' % (self.gid, self.name)) != 0:
			raise GroupError, 'error creating group ' + self.name
		self.members = set()
	
	def is_member(self, user):
		return user.name in self.members
	
	def add_member(self, user):
		if system('gpasswd -a %s %s >/dev/null' % (user.name, self.name)) != 0:
			raise GroupError, 'error adding user %s to group %s' % (user.name, self.name)
		self.members.add(user.name)
	
	def remove_member(self, user):
		if system('gpasswd -d %s %s >/dev/null' % (user.name, self.name)) != 0:
			raise GroupError, 'error removing user %s from group %s' % (user.name, self.name)
		self.members.remove(user.name)
	
	def _check_name(self, name):
		if _name_rx.match(name) is None:
			raise UserError, 'invalid group name ' + self.name

class GroupError(Exception):
	pass
