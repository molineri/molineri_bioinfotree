#!/bin/bash
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

function error {
	echo "[install_ensembl_api] $1" >&2
	exit 1
}

function print_help {
	echo "Usage: $1 API_VERSION [CMD...]"
	echo
	echo "Setup the environment for a specific version of the Ensembl APIs."
	echo "If a command is specified, it is run in the new environment."
	echo "Otherwise a new shell is started."
	exit 1
}

if [ $# -lt 1 ]; then
	error "Unexpected argument number."
elif [ $1 == "-h" -o $1 == "--help" ]; then
	print_help "$0"
elif [ -z $BIOINFO_ROOT ]; then
	error "BIOINFO_ROOT is not defined."
elif [ -z $BIOINFO_HOST ]; then
	error "BIOINFO_HOST is not defined."
fi

install_dir="$BIOINFO_ROOT/binary/$BIOINFO_HOST/local/lib/ensembl/$1"
if [ ! -d "$install_dir" ]; then
	error "Ensembl APIs version $1 are not installed."
fi

PERL5LIB="$PERL5LIB:$install_dir/bioperl"
PERL5LIB="$PERL5LIB:$install_dir/ensembl/modules"
PERL5LIB="$PERL5LIB:$install_dir/ensembl-compara/modules"
PERL5LIB="$PERL5LIB:$install_dir/ensembl-functgenomics/modules"
PERL5LIB="$PERL5LIB:$install_dir/ensembl-variation/modules"
export PERL5LIB

shift
if [ $# -eq 0 ]; then
	exec bash -l
else
	exec "$@"
fi
