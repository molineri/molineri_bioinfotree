#!/bin/bash
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

function error {
	echo "[install_ensembl_api] $1" >&2
	exit 1
}

function print_help {
	echo "Usage: $1 API_VERSION"
	echo
	echo "Fetches and installs the required version of Ensembl APIs."
	exit 1
}

function fetch_ensembl_pkg {
	# $1: package
	# $2: version
	wget -q -O- 'http://cvs.sanger.ac.uk/cgi-bin/viewvc.cgi/'$1'.tar.gz?root=ensembl&only_with_tag=branch-ensembl-'$2'&view=tar' | tar xz
}

if [ $# -ne 1 ]; then
	error "Unexpected argument number."
elif [ $1 == "-h" -o $1 == "--help" ]; then
	print_help "$0"
elif [ -z $BIOINFO_ROOT ]; then
	error "BIOINFO_ROOT is not defined."
elif [ -z $BIOINFO_HOST ]; then
	error "BIOINFO_HOST is not defined."
fi

install_dir_base="$BIOINFO_ROOT/binary/$BIOINFO_HOST/"
if [ ! -d "$install_dir_base" ]; then
	error "Missing directory: $install_dir_base"
fi

install_dir="$install_dir_base/local/lib/ensembl/$1"
[[ -e "$install_dir" ]] && error "Installation directory ($install_dir) already exists; please remove it and rerun this script."
mkdir -p "$install_dir" || error "Cannot create directory: $install_dir"
touch "$(dirname "$install_dir")/.do_not_clean"
cd "$install_dir" || error "Cannot change directory to: $install_dir"

set -o pipefail
if ! wget -q -O- 'http://bioperl.org/DIST/old_releases/bioperl-1.2.3.tar.gz' | tar xz; then
	error "Cannot retrieve BioPerl."
elif ! mv bioperl-1.2.3 bioperl; then
	error "Cannot setup BioPerl."
elif ! fetch_ensembl_pkg ensembl $1; then
	error "Cannot retrieve Ensembl Core package."
elif ! fetch_ensembl_pkg ensembl-variation $1; then
	error "Cannot retrieve Ensembl Variation package."
elif ! fetch_ensembl_pkg ensembl-compara $1; then
	error "Cannot retrieve Ensembl Compara package."
elif ! fetch_ensembl_pkg ensembl-functgenomics $1; then
	error "Cannot retrieve Ensembl Functional Genomics package."
fi
