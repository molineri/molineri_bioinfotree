#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] from to
	produce a bash script to clone recursively a project with all the phases ad versions
\n";

my $help=0;
my $glue=",";
GetOptions (
	'h|help' => \$help,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my $from = $ARGV[0];
my $to = $ARGV[1];
my $project_root = `pwd`;
chomp $project_root;
die("The directory $project_root is not a project root dir.") if not $project_root =~ s/dataset$//;

die("There is not a valid dataset in \"$project_root/dataset/$from\".") if not -e "$project_root/dataset/$from";
die("Target dataset \"$to\" already exists") if -e $to;

my $find_cmd = "find $from -name '*.mk' -o -name 'makefile' | grep -v '/\\..*'";
my @files = split /\n/,`$find_cmd`;

for(@files){

	next if not -l $_;

	print "##########################";
	print "#    processing file: $_";
	print "#";

	my @F = split /\s+/, `ls -l $_`;
	
	my $orig = $F[-1]; 
	my $dest = $F[-3]; 

	$dest =~ s/$from/$to/;
	$orig =~ s|(\.\./)+|$project_root| if $orig =~ m|/local/share/|;
	
	my $dest_dir = $dest; 
	$dest_dir =~ s/[^\/]+.mk$//;
	$dest_dir =~ s/makefile$//; 
	print "mkdir -p $dest_dir";
	
	if($orig=~/$from/){
		my $orig_cp = $orig; 
		$orig_cp=~s/$from/$to/;
		print "cp $orig $orig_cp"; 
		$orig=$orig_cp;
	}
	
	if($orig =~ /\/local\/share\/.*makefile$/){
		print "link_install $orig $dest";
	}elsif($orig=~/makefile$/){#relative link
		print "cd $dest_dir";
		print "link_install $orig makefile";
		print 'cd $OLDPWD';
	}else{
		print "link_install $orig $dest";
	}
}
