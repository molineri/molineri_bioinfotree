#!/usr/bin/env python
#
# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

from optparse import OptionParser
from os import chdir, environ, mkdir, readlink, symlink
from os.path import abspath, exists, isdir, islink, join, normpath, split
from subprocess import Popen, PIPE
from tempfile import NamedTemporaryFile
from vfork.util import exit, format_usage
import re

def load_env(varname):
    try:
        v = environ[varname]
        if len(v) == 0: raise ValueError
        return v
    except (KeyError, ValueError):
        exit('Missing or empty environment variable: %s' % varname)

def outside_root(path, root):
    return not normpath(abspath(path)).startswith(root + '/')

def transform(value, exprs):
    cmd = ['sed', '-r']
    for expr in exprs:
        cmd += ['-e', expr]
    
    proc = Popen(cmd, shell=False, close_fds=True, stdin=PIPE, stdout=PIPE)
    pout, _ = proc.communicate(value)
    if proc.wait() != 0:
        exit('Error in sed expressions.')
        
    return pout.rstrip('\n')

def transform_name(name, exprs):
    return transform(name + '\n', exprs).rstrip('\n')

def run_editor(path, editor):
    proc = Popen([editor, path], shell=False, close_fds=True)
    if proc.wait() != 0:
        exit('Error running EDITOR.')

def split_manifest(content):
    header = []
    body = []
    inside_header = False

    for line in content.split('\n'):
        if '-*- dataset_clone -*-' in line:
            inside_header = not inside_header
        elif inside_header:
            header.append(line)
        else:
            body.append(line)

    return parse_header(header), '\n'.join(body)

def parse_header(lines):
    name = None
    clone = False
    
    name_rx = re.compile('DATASET_NAME\s+(.*)')
    makefile_rx = re.compile('MAKEFILE_NAME\s+(.*)')
    clone_rx = re.compile('CLONE\s+OK')

    for line in lines:
        m = name_rx.search(line)
        if m is not None:
            name = m.group(1)
            continue

        m = makefile_rx.search(line)
        if m is not None:
            mkname = m.group(1)
            continue
        
        m = clone_rx.search(line)
        if m is not None:
            clone = True

    if name is None:
        exit('Dataset name is missing from header; did you cancel it?')
    elif not clone:
        exit('No "CLONE OK" statement. Aborting.')
    else:
        return name, mkname

def relative_link(src, dest):
    proc = Popen(['link_install', src, dest], shell=False, close_fds=True)
    if proc.wait() != 0:
        exit('Error creating symlink.')


def main():
    parser = OptionParser(usage=format_usage('''
        %prog [OPTIONS] TEMPLATE SED_EXPR...

        Clones a dataset transforming its name and
        makefile according to the provided list of
        sed-like replacement expressions.

        Such expressions are evaluated as extended
        regular expressions.
    '''))

    parser.add_option('-v', '--new_dataset_name', type=str, dest='dname_new', default=None, help='give the name of the new directory to be create and do not try to guess it', metavar='NAME')

    options, args = parser.parse_args()
    if len(args) < 2:
        exit('Unexpected argument number.')
    
    root = normpath(load_env('BIOINFO_ROOT'))
    editor = load_env('EDITOR')

    dataset = normpath(args[0])
    parent, dname = split(dataset) 
    if not isdir(dataset) or dname == '':
        exit('Not a directory: %s' % dataset)
    elif outside_root(parent, root):
        exit('The dataset must be inside BIOINFO_ROOT.')

    links = {}
    for l in ('makefile', 'rules.mk'):
        path = join(dataset, l)
        if not islink(path):
            exit('Missing "%s" link in: %s' % (l, dataset))
        
        lpath = readlink(path)
        if outside_root(lpath, root):
            exit('Link "%s" points outside of BIOINFO_ROOT: %s' % (l, abspath(lpath)))
        else:
            links[l] = abspath(normpath(join(dataset, lpath)))

    mkpath, mkname = split(links['makefile'])
    dname_new = options.dname_new
    if dname_new is None:
	    dname_new = transform_name(dname, args[1:])
	    mkname_new = transform_name(mkname, args[1:])
    else:
	mkname_new = mkname.replace(dname, dname_new)

    with file(links['makefile']) as fd:
        mknew = transform(fd.read(), args[1:])
    
    with NamedTemporaryFile(mode='w+') as fd:
        print >>fd, '# -*- dataset_clone -*-'
        print >>fd, '# DATASET_NAME  %s' % dname_new
        print >>fd, '# MAKEFILE_NAME %s' % mkname_new
        print >>fd, '#'
        print >>fd, '# Delete the following line to abort cloning.'
        print >>fd, '# CLONE OK'
        print >>fd, '# -*- dataset_clone -*-'
        fd.write(mknew)
        fd.flush()

        run_editor(fd.name, editor)

        fd.seek(0)
        (dname_new, mkname_new), body = split_manifest(fd.read())

    if exists(dname_new):
        exit('Dataset already exists: %s' % dname_new)

    mkpath_new = join(mkpath, mkname_new)
    if exists(mkpath_new):
        exit('Makefile already exists: %s' % mkpath_new)
    
    mkdir(dname_new)
    chdir(dname_new)

    with file(mkpath_new, 'w') as fd:
        fd.write(body)
    relative_link(mkpath_new, 'makefile')

    relative_link(links['rules.mk'], 'rules.mk')


if __name__ == '__main__':
    main()
