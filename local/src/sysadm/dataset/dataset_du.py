#!/usr/bin/env python

from commands import getstatusoutput
from optparse import OptionParser
from os import environ, walk
from os.path import abspath, normpath
from sys import exit

def get_bioinfo_root():
	try:
		root = abspath(normpath(environ['BIOINFO_ROOT']))
		if root[-1] != '/': root += '/'
		return root
	except KeyError:
		exit('BIOINFO_ROOT is not defined.')

def is_subdir(path, root):
	path = abspath(normpath(path))
	return path == root[:-1] or path.startswith(root)

def call_du(path, root):
	status, output = getstatusoutput("du -hs '%s'" % path)
	if status != 0:
		exit('Error running du on %s' % path)
	
	return output.replace(root, '')

def parse_size(entry):
	size = entry.split(None, 2)[0].lower()
	
	scale = 1
	if size[-1] in 'kmg':
		scale = size[-1]
		size = size[:-1]
		
		if scale == 'k':
			scale = 1024
		elif scale == 'm':
			scale = 1024*1024
		else:
			scale = 1024*1024*1024
	
	try:
		size = float(size.replace(',', '.'))
	except ValueError:
		exit('Unexpected size format in du output: %s' % size)
	
	return size * scale

def main():
	parser = OptionParser(usage='''%prog [OPTIONS] [ROOT]

Shows the disk usage of each dataset under ROOT (by default BIOINFO_ROOT).''')
	parser.add_option('-s', '--sort', dest='sort', action='store_true', default=False, help='sort entries so that bigger ones appear first')
	options, args = parser.parse_args()
	
	if len(args) > 1:
		exit('Unexpected argument number.')
	
	bioinfo_root = get_bioinfo_root()
	if len(args):
		root = args[0]
		if not is_subdir(root, bioinfo_root):
			exit('The supplied path is outside of BIOINFO_ROOT.')
	else:
		root = bioinfo_root
	
	log = []
	for dirpath, dirnames, filenames in walk(root):
		if '/dataset/' in dirpath and 'makefile' in [ f.lower() for f in filenames ]:
			dirnames[:] = []
			
			entry = call_du(dirpath, root)
			if options.sort:
				log.append((parse_size(entry), entry))
			else:
				print entry
	
	if options.sort:
		log.sort(reverse=True)
		for _, entry in log:
			print entry

if __name__ == '__main__':
	main()
