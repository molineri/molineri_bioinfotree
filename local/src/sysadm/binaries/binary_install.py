#!/usr/bin/env python
#
# Copyright 2008-2009 Gabriele Sales <gbrsales@gmail.com>

from __future__ import with_statement
from optparse import OptionParser
from os import chdir, environ, getcwd, readlink, rmdir, system, unlink, walk
from os.path import abspath, dirname, islink, join
from sys import exit
import re

def get_bioinfo_env():
	try:
		root = abspath(environ['BIOINFO_ROOT'])
	except KeyError:
		exit('BIOINFO_ROOT is not defined.')
	
	try:
		host = environ['BIOINFO_HOST']
	except KeyError:
		exit('BIOINFO_HOST is not defined.')
	
	return root, host

def clean_files(root):
	empty_dirs = []
	for dirpath, dirnames, filenames in walk(root):
		if '.do_not_clean' in filenames:
			dirnames[:] = []
			continue
		
		try:
			dirnames.remove('stow')
		except ValueError:
			pass
		
		n = len(filenames)
		for filename in filenames:
			clean = True
			path = join(dirpath, filename)
			
			if islink(path):
				link_path = join(dirname(path), readlink(path))
				if '/stow/' in link_path:
					clean = False
			
			if clean:
				unlink(path)
				n -= 1
		
		if n == 0 and len(dirnames) == 0:
			empty_dirs.append(dirpath)
	
	return empty_dirs

def clean_binaries(root):
	for path in clean_files(root):
		while len(path) >= len(root):
			try:
				rmdir(path)
			except OSError:
				break
			else:
				path = dirname(path)

def iter_srcs(root):
	for dirpath, dirnames, filenames in walk(root):
		if '/local/src/' in (dirpath+'/'):
			lnames = [ f.lower() for f in filenames ]
			try:
				idx = lnames.index('makefile')
				yield dirpath, filenames[idx]
			except ValueError:
				continue

def has_install_target(filepath):
	target_rx = re.compile('^install\s*:')
	with file(filepath, 'r') as fd:
		for line in fd:
			if target_rx.match(line):
				return True
	return False

def install_path(root, host, path):
	low = len(root) + 1
	high = path.index('/local/src/')
	return join(root, 'binary', host, path[low:high], 'local')

def main():
	parser = OptionParser(usage='''%prog [OPTIONS]

Install binaries.

The software scans the subtree rooted in the working directory looking for
software sources; it compiles and installs them in the /binary tree.''')
	parser.add_option('-a', '--all', dest='all', action='store_true', default=False, help='install all binaries in the tree, even those outside of the working directory')
	parser.add_option('-c', '--clean', dest='clean', action='store_true', default=False, help='remove old binaries before installing new ones')
	parser.add_option('-f', '--force', dest='force', action='store_true', default=False, help='force a clean build')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	elif options.clean and not options.all:
		exit('Option error: --clean requires --all.')
	
	root, host = get_bioinfo_env()
	
	if options.clean:
		clean_binaries(join(root, 'binary', host))
	
	if options.all:
		cwd = root
	else:
		cwd = getcwd()
		if not cwd == root and not cwd.startswith(root + '/'):
			exit('Cannot run outside of BIOINFO_ROOT.')
	
	with_errors = False
	for path, makefile in iter_srcs(cwd):
		if has_install_target(join(path, makefile)):
			print '>', path
			chdir(path)
			
			if options.force:
				if system("make clean") != 0:
					with_errors = True
					print 'EEE', path
					continue
			
			if system("make install DEST='%s'" % install_path(root, host, path)) != 0:
				with_errors = True
				print 'EEE', path
	
	if with_errors:
		exit('Some installs have errors.')

if __name__ == '__main__':
	main()
