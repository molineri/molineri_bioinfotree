#!/usr/bin/env python
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from optparse import OptionParser
from os import symlink, sep, unlink
from os.path import abspath, dirname, join, normpath, split
from vfork.util import exit, format_usage
import errno

def main():
	usage = format_usage('''
		%prog SRC DEST
		
		Create a relative symbolic link DEST pointing to SRC.
	''') 
	parser = OptionParser(usage=usage)
	parser.add_option('-f', '--force', dest='force', action='store_true', default=False, help='(re)create the link even when it already exists')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	src_dir, src_filename = split(normpath(abspath(args[0])))
	src_dir = src_dir.split(sep)[1:]
	dest_dir = dirname(normpath(abspath(args[1]))).split(sep)[1:]
	
	level = 0
	while True:
		if len(src_dir) == level or len(dest_dir) == level:
			break
		elif src_dir[level] == dest_dir[level]:
			level += 1
		else:
			break
	
	rel = [ '..' for i in xrange(len(dest_dir)-level) ]
	rel += src_dir[level:]
	rel.append(src_filename)
	
	if options.force:
		try:
			unlink(args[1])
		except OSError, e:
			if e.errno != errno.ENOENT:
				raise

	try:
		symlink(sep.join(rel), args[1])
	except OSError, e:
		if e.errno == errno.EEXIST:
			exit('Link already exists.')
		else:
			raise

if __name__ == '__main__':
	main()
