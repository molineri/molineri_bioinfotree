#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\n";
$\="\n";


my $usage = "$0 [-r|reverse] -n N   ID_COL VAL_COL\n";

my $n=0;
my $reverse =0;
GetOptions (
	'n=i' => \$n,
	'reverse|r' => \$reverse
) or die($usage);

$SIG{__WARN__} = sub {my @loc = caller(1); CORE::die "WARNING generated at line $loc[2] in $loc[1]: ", @_};

die($usage) if $n <= 0;

my $id_col = shift;
my $val_col = shift;

my $pre_id = undef;
my $pre_val = undef;
my $group_n = 0;
my @rows = ();

while(<>){
	chomp;
	my @F = split /\t/;
	my $id = $F[$id_col];
	my $val = $F[$val_col];
	die("input not sorted on col ID_COL ($id_col)")  if defined $pre_id and ($pre_id lt $id);
	die("input not sorted on col VAL_COL ($val_col)") if defined $pre_val and ((not $reverse and $pre_val lt $val) or ($reverse and $pre_val > $val));
	if($group_n < $n){
		push @rows, $_;
		$group_n++;
		$pre_val = $val;

	}else{
		print @rows;
		while(<>){
			chomp;
			my @F = split /\t/;
			my $id = $F[$id_col];
			my $val = $F[$val_col];
			next if $pre_id eq $id;
			$pre_id = $id;
			$pre_val = $val;
			@rows = ($_);
			$group_n = 1;
		}
	}
}

print @rows if @rows;
