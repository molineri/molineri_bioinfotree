#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <vector>
#include <tnt/tnt.h>
#include <strstream>
#include <gsl/gsl_cdf.h>
#include <queue>
#include <unordered_map>
#include <stdexcept> // out_of_range exception
#include "find_best.h"

using namespace std;
using namespace TNT;

typedef BestN<PrioritizedIndex> Best;
typedef Best *BestPointer;
typedef	std::unordered_map<int, BestPointer> BestSet;
typedef	std::unordered_map<int, int> map;
typedef	std::unordered_map<int, map> double_map;


int setup(int argc, char* argv[]);
void usage(char** argv);

int option_size = 1;
bool option_reverse = false;
bool option_verbose = false;
bool option_reverse_check = false;

typedef struct {
	int key;
	int gen;
	double val;
	int rank;
	int reciprocal_rank;
} out_line;

int main( int argc, char* argv[] ){
	cin.imbue(std::locale("C"));
	cout.imbue(std::locale("C"));
	cout.precision(12);

	//controllo le opzioni passate da riga di comando
	if(setup(argc, argv)){
		cerr << "ERROR: setup error"<<endl;
		exit(1);
	}else{
		//atexit(stop);
	}

	BestSet fen;
	double_map reverse_check;

	int key,gen=0;
	double val = 0.;
 
 	int colnum=3;

	while(colnum = scanf("%d\t%d\t%lf\n", &key, &gen, &val)==3){
		Best * best_istance_key;
		Best * best_istance_gen;
	
		try{
			best_istance_key = fen.at(key);
		}
		catch (std::out_of_range& e)
		{
			Best * best_istance = new Best(option_size, option_reverse);
			fen[key] = best_istance;
			best_istance_key = best_istance;
		}

		try{
			best_istance_gen = fen.at(gen);
		}
		catch (std::out_of_range& e)
		{
			Best * best_istance = new Best(option_size, option_reverse);
			fen[gen] = best_istance;
			best_istance_gen = best_istance;
		}

		PrioritizedIndex i = PrioritizedIndex(key,val);
		best_istance_gen->check(i);
		PrioritizedIndex j = PrioritizedIndex(gen,val);
		best_istance_key->check(j);
	}
	if(option_verbose)
		cerr << endl;
	
	for (BestSet::const_iterator it = fen.begin(); it != fen.end(); ++it){ 
		//cout << " [" << it->first << ", " << it->second << "]";
		//
		int key = it->first;
		PrioritizedIndex l;
		Best B = * it->second;
		int size = B.actual_size();
		out_line * out = new out_line[size];
		int i=0;
		int r=size;
		while(B.next(l)){
			int a = it->first;
			int b = l.idx;
			if(not option_reverse_check){
				out[i].key = a;
				out[i].gen = b;
				out[i].val = l.priority;
				out[i].rank = i;
				i++;
			}else{
				try{
					out[i].reciprocal_rank = reverse_check.at(b).at(a);// throws an out_of_range exception if the couple b,a is not in the double map
					out[i].key = a;
					out[i].gen = b;
					out[i].val = l.priority;
					out[i].rank = r;
					i++;
				}catch (std::out_of_range& e){
					reverse_check[a][b]=r;
				}
			}
			r--;
		}
		for(int j = i-1; j >=0; j--){//print the output in the expected order
			cout << out[j].key << "\t" << out[j].gen << "\t" << out[j].val << "\t" << out[j].rank;
			if(option_reverse_check){
				cout << "\t" << out[j].reciprocal_rank;
			}
			cout << endl;
		}
		delete [] out;
	}
}

int setup(int argc, char* argv[]){
	
	////////////////////////////////////////////////////////////////
	// gestione delle opzioni
	//
	int i;
	while((i=getopt(argc, argv, "n:rve"))!=-1){
		switch(i){
			case 'n':
				option_size = atoi(optarg);
				break;
			case 'r':
				option_reverse = true;
				break;
			case 'v':
				option_verbose = true;
				break;
			case 'e':
				option_reverse_check = true;
				break;
			default:
				cerr<<"wrong options given (" << i << ")" <<endl;
				usage(argv);
				return -1;
		}	
	}
	if(optind<argc){
		//vi sono parametri dopo l'elenco delle opzioni
		usage(argv);
		return -1;
	}
	return(0);
}

void usage(char** argv){
	cout << "usage: " << argv[0] <<  "[-r] [-n N] [-e]"<<endl
		<<"\t-r reverse the ordering"<<endl
		<<"\t-n the max number of rors to keep for each key, defalut 1"<<endl
		<<"\t-e reciprocal"<<endl
		<<"\t-v verbose"<<endl
		<<endl
		<<".META: stdin"<<endl
			<<"	   1	id1	3863743               "<<endl
			<<"	   2    id2	6979279"<<endl
			<<"	   3	pval	8.48705532084e-08	" << endl
			<<endl
			<<"in normal mode"<<endl
			<<endl
			<<".META: stdout"<<endl
			<<"	   1	id1	3863743            "<<endl
			<<"	   2    in2	6979279"<<endl
			<<"	   3    p_value_prod	8.48705532084e-08	"<<endl
			<<"	   4    rand_of_id2_in_id1_list	5	"<<endl
			<<endl
			<<"for each vertex in the 1 col, the best N edges are reported, with the corresponding vertex in the col 2 and the edge weight in the col 3"<<endl
			<<"given each vertex in the col 1, the edges are sorted on edge weight"<<endl
			<<endl
			<<"in -e (reciprocal) mode"<<endl
			<<"\tonly edges between ids that are both in the top N for eachother are reported "<<endl
			<<"\tfor each edge only one row is reported: (A,B) and not (B,A)"<<endl
			<<endl
			<<"\t.META: stdout"<<endl
			<<"\t	   1	id1	3863743            "<<endl
			<<"\t	   2    in2	6979279"<<endl
			<<"\t	   3    p_value_prod	8.48705532084e-08	"<<endl
			<<"\t	   4    rand_of_id2_in_id1_list	5	"<<endl
			<<"\t	   5    rand_of_id1_in_id2_list	7	"<<endl
			<<endl
			<<endl
		<<endl;
	exit(-1);
}
