#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr, stdout
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from subprocess import Popen, PIPE
from collections import defaultdict

class best_n:
	def __init__(self, id, element_n, reverse_order):
		self.filled = False
		self.id = id
		self.element_n = element_n
		self.elements = []
		self.reverse_order = reverse_order

	def worst(self):
		if not self.reverse_order:
			min = None
			min_index = None
			i = 0
			for line,val in self.elements:
				if min is None or min > val:
					min = val
					min_index = i
				i+=-1
			self.worst_val = min
			self.worst_index = min_index
		else:
			max = None
			max_index = None
			i = 0
			for line,val in self.elements:
				if max is None or max < val:
					max = val
					max_index = i
				i+=-1
			self.worst_val = max
			self.worst_index = max_index

	def better_than_worst(self, v):
		if not self.reverse_order:
			return v > self.worst_val
		else:
			return v < self.worst_val
	
	def check(self, val, line):
		if not self.filled:
			self.elements.append( (line, val) )
			if len(self.elements) >= self.element_n:
				self.filled = True
				self.worst()
		elif self.better_than_worst(val):
			self.elements[self.worst_index]=(line,val) #overwrite of worst vale
			self.worst()
	
	def __str__(self):
		retval = "" 
		for line,val in self.elements:
			retval += line
		return retval
				
				
def main():
	exit("Bogous version, use the version linked in $BIOINFO_ROOT/local/bin.")
	usage = format_usage('''
		%prog ID_COL VAL_COL < STDIN
		for all rows with the same value in ID_COL report only the row with the best value in VAL_COL
the worst value to keep for the id, then only M are keped, the others discarded.
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-n', '--best_n', type=int, dest='best_n', default=1, help='''report the best N rows for each ID
	N.B. if for te given id there are more than N duplicated values of a value V equal to
	the worst value to keep for the id, then only N are keped, the others
	discarded.''', metavar='N')
	parser.add_option('-r', '--reverse', dest='reverse', action='store_true', default=False, help='find the lowest value')
	parser.add_option('-s', '--already_sorted', dest='already_sorted', action='store_true', default=False, help='assume the input already sorted on ID_COL')

	options, args = parser.parse_args()
	
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	id_col = int(args[0]) - 1
	val_col = int(args[1]) - 1
	
	retval = {}

	pre_id = None;
	block = None;
	for line in stdin:
		tokens = safe_rstrip(line).split('\t')
		id = tokens[id_col]
		val = float(tokens[val_col])

		already_wanr = False
		if not options.already_sorted:
			try:
				retval[id].check(val,line)
			except KeyError:
				retval[id] = best_n(id, options.best_n, options.reverse)
				retval[id].check(val,line)
		else:
			if pre_id != id:
				if not already_wanr and pre_id > id:
					stderr.write("WARN: input non lexicografically sorted on ID_COL, continue aniway.")
					already_wanr = True;
				if block is not None:
					print "print"
					stdout.write(str(block))
				block = best_n(id, options.best_n, options.reverse)
			block.check(val,line)
			pre_id = id

	if options.already_sorted and block is not None:
		stdout.write(str(block))
	else:
		for id, block in retval.iteritems():
			stdout.write(str(block))
		#	print "\t".join(v)

		

if __name__ == '__main__':
	main()

