#include <vector>

using namespace std;

class PrioritizedIndex {
	public:
		double priority;
		int idx;
		PrioritizedIndex() { 
			idx = NULL;
			priority = 0; 
		}
		PrioritizedIndex(int i, double p) { 
			idx = i;
			priority = p; 
		}
};

class Line {
	public:
		double priority;
		int idx;
		string content;
		Line() { 
			content = ""; 
			idx = NULL;
			priority = 0; 
		}
		Line(int i, double p, string l) { 
			idx = i;
			content = l; 
			priority = p; 
		}
};

bool operator<(const Line &a, const Line &b){
	return a.priority < b.priority;
}
//in order to use BestN with to store object != Line those object mus have a public "priority" and a similar operator must exist

bool operator<(const PrioritizedIndex &a, const PrioritizedIndex &b){
	return a.priority < b.priority;
}
//in order to use BestN with to store object != Line those object mus have a public "priority" and a similar operator must exist

template <class T> class BestN{
	priority_queue<T> q;
	public:
		int size;
		bool reverse;
		BestN(){
			size = 0;
			reverse = false;
		}
		BestN(int size_param, bool reverse_param=false){
			size = size_param;
			reverse = reverse_param;
		}
		//~BestN();
		void check(T t){
			if(!reverse){ 	// !reverse instead of simply reverse because 
					// q.pop() will remove the best value => the 
					// best case for the priority_queue must be 
					// the worst case for us
				t.priority=-t.priority;
			}
			if(q.size() < size){
				q.push(t);
			}else{
				q.push(t);
				q.pop(); //remove the best value
			}
		}
		bool next(T &t){
			if(q.empty()){
				return false;
			}
			t = q.top();
			q.pop();
			if(!reverse){ 	// !reverse instead of simply reverse, see the BestN::check 
				t.priority=-t.priority;
			}
			return true;
		}
		int actual_size(){
			return q.size();
		}
};

//BestN::~BestN(){
//	delete q;
//}
