#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from itertools import groupby
from operator import itemgetter
from vfork.io.colreader import Reader
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader


def line_parser(lines):
	for l in lines:
		i,j,v = safe_rstrip(l).split('\t')
		yield int(i), int(j), float(v)
	

class SortableReader:
	def __init__(self, filename):
		self.filename = filename
		self.group_reader = 	groupby(\
						line_parser(file(filename, 'r')),\
						itemgetter(0)\
					)
		self.key = None
		self.next()
	
	def next(self):
		try:
			k, lines = self.group_reader.next()
			k = int(k)
			if self.key is not None and self.key > k:
				raise ValueError("Input file %s not sorted on first column (keys: %s - %s)" % (self.filename, self.key, k))
			self.key   = k
			self.group = lines
		except StopIteration:
			self.key = None
			self.group = None

def main():
	usage = format_usage('''
		%prog PARAM1 PARAM2 < STDIN
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-n', '--size', type=int, dest='size', default=1, help='the max size of each group [default: %default]', metavar='N')
	parser.add_option('-r', '--reverse', dest='reverse', action='store_true', default=False, help='reverse the sorting criterion [default: %default]')

	options, args = parser.parse_args()
	
	if len(args) < 2:
		exit('Unexpected argument number.')
	
	groups=[]
	for a in args:
		groups.append(SortableReader(a))

	while(len([g for g in groups if g.key is None]) < len(groups)):
		groups.sort(key=lambda g: g.key)
		running_group=[None,[]]
		for g in groups:
			if g.key is None:
				continue
			if running_group[0] is None or running_group[0] == g.key:
				running_group[0]  = g.key
				running_group[1] += g.group
				g.next()
		lines = running_group[1]
		lines.sort(key=itemgetter(2), reverse= not options.reverse) # not perche` vogliamo i piu` piccoli se reverse e` true
		for l in lines[0:options.size]:
			print "%d\t%d\t%.12g" % l

if __name__ == '__main__':
	main()

