#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage="$0 [-r|reverse] [-s|sorted [-n|numeric]] [-m|multi] [-x|xmulti N]  ID_COL VAL_COL < IN_FILE
	-r|reverse	get the row with the lowest VAL_COL
	-m|multi	get all rows with the best VAL_COL (instead of only one at random)
	-s|sorted	assume the input file sorted on ID_COL
	-n|numeric	che file is numerically sorte on ID_COL
	-o|no_chek_sorted	do not chek the sorting
";

my $help=0;
my $reverse=0;
my $sorted=0;
my $numeric_sort=0;
my $multi=0;
my $no_chek_sorted=0;
GetOptions (
	'h|help' => \$help,
	'reverse|r' => \$reverse,
	'sorted|s'  => \$sorted,
	'numeric|n' => \$numeric_sort,
	'multi|m'   => \$multi,
	'no_chek_sorted|o'   => \$no_chek_sorted,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

die($usage) if $numeric_sort and !$sorted;
die($usage) if $no_chek_sorted and !$sorted;

my $id_col=shift @ARGV;
my $val_col=shift @ARGV;

$id_col=~/\d+/ or die($usage);
$val_col=~/\d+/ or die($usage);
$id_col--;
$val_col--;


if (!$sorted) {
	&not_sorted;
} else {
	&if_sorted;
}


sub if_sorted
{
	my $pre_id = undef;
	my $best_value = undef;
	my @best_line = ();

	while (<>) {
		chomp;
		my @F=split /\t/;
		my $id=$F[$id_col];

		if(defined $pre_id){
			unless($no_chek_sorted){
				if($numeric_sort){
					die("ERROR: input not sorted") if $id < $pre_id;
				}else{
					die("ERROR: input not sorted") if $id lt $pre_id;
				}
			}

			if ($id eq $pre_id) {
				if ($F[$val_col] == $best_value) {
					push @best_line,\@F;
				} elsif ( !$reverse and ($F[$val_col] > $best_value) ) {
					$best_value = $F[$val_col];
					@best_line = (\@F);
				} elsif ( $reverse and ($F[$val_col] < $best_value) ) {
					$best_value = $F[$val_col];
					@best_line = (\@F);
				}
			} else {
				foreach my $line_ref (@best_line) {
					print @{$line_ref};
					last if !$multi;
				}
				$best_value = $F[$val_col];
				@best_line = (\@F);
			}

		} else {
			$best_value = $F[$val_col];
			@best_line = (\@F);
		}

		$pre_id = $id;
	}

	if (scalar @best_line) {
		foreach my $line_ref (@best_line) {
			print @{$line_ref};
			last if !$multi;
		}
	}
}


sub not_sorted
{
	my %best=();
	while(<>){
		chomp;
		my @F=split /\t/;
		my $id=$F[$id_col];

		if($multi){
			if(!defined($best{$id})){
				my @tmp=(\@F);
				$best{$id}=\@tmp;
			}else{
				if($best{$id}[0][$val_col] == $F[$val_col]){
					push @{$best{$id}},\@F;
				}else{
					if(!$reverse){
						if( $best{$id}[0][$val_col] < $F[$val_col]){
							my @tmp=(\@F);
							$best{$id}=\@tmp;
						}
					}else{
						if( $best{$id}[0][$val_col] > $F[$val_col]){
							my @tmp=(\@F);
							$best{$id}=\@tmp;
						}
						
					}
				}
			}
		}else{
			if(!defined($best{$id})){
				$best{$id}=\@F;
			}else{
				if(!$reverse){
					if( $best{$id}[$val_col] < $F[$val_col]){
						$best{$id}=\@F;
					}
				}else{
					if( $best{$id}[$val_col] > $F[$val_col]){
						$best{$id}=\@F;
					}
					
				}
			}
		}
	}

	for(keys(%best)){
		if($multi){
			foreach my $ref (@{$best{$_}}){
				print @{$ref};
			}
		}else{
			print @{$best{$_}};
		}
	}
}
