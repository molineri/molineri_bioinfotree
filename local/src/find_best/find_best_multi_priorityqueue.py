#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr, stdout
from optparse import OptionParser
#from subprocess import Popen, PIPE
from collections import defaultdict

class best_n:
	def __init__(self, id, element_n, reverse_order):
		self.id = id
		self.element_n = element_n
                self.length = 0
		self.elements = [None]*element_n
		self.reverse_order = reverse_order
        
        def insert(self, v):    
                """Adds a new element in the priority queue,
                   assuming it's still not full."""
                self.elements[self.length] = v
                self.move_up(self.length)
                self.length = self.length+1
         
        def get_father(self, i):
                """Returns the index of an element's father (having its index)
                   [2-heap with fathers array representation]."""
                return (i-1)/2

        def move_up(self, i):   
                """Restores the heap invariant moving a new element up."""
                f = self.get_father(i)
                if not self.reverse_order:
                    while i > 0 and self.elements[i][0] < self.elements[f][0]:
                        swap = self.elements[i]
                        self.elements[i] = self.elements[f]
                        self.elements[f] = swap
                        i = f
                        f = self.get_father(f)                 
                else:
                    while i > 0 and self.elements[i][0] > self.elements[f][0]:
                        swap = self.elements[i]
                        self.elements[i] = self.elements[f]
                        self.elements[f] = swap
                        i = f
                        f = self.get_father(f)                 

        def get_worst_son(self, i):
                """Returns the index of the worst son of a given element 
                   (having its index)."""
                first = i * 2 + 1
                second = i * 2 + 2
                if second >= self.length:
                    if first >= self.length:
                        return -1
                    else:
                        return first
                #if we seek maximum elements the radix is the smallest, we move down something when it's bigger than its smallest son
                #if we seek minimum elements the radix is the biggest, we move down something when it's smaller than its bigger son
                if not self.reverse_order: 
                    if self.elements[first][0] < self.elements[second][0]:
                        return first
                    else:
                        return second
                else:
                    if self.elements[first][0] > self.elements[second][0]:
                        return first
                    else:
                        return second

        def move_down(self, i):
                """Restores the heap invariant moving an element down
                   (used when the radix is replaced with a better element)."""
                worst_son = self.get_worst_son(i)
                if not self.reverse_order:
                    while worst_son != -1 and self.elements[i][0] > self.elements[worst_son][0]:
                        swap = self.elements[worst_son]
                        self.elements[worst_son] = self.elements[i]
                        self.elements[i] = swap
                        i = worst_son
                        worst_son = self.get_worst_son(i)
                else:
                    while worst_son != -1 and self.elements[i][0] < self.elements[worst_son][0]:
                        swap = self.elements[worst_son]
                        self.elements[worst_son] = self.elements[i]
                        self.elements[i] = swap
                        i = worst_son
                        worst_son = self.get_worst_son(i)
        
        def better_than_worst(self, v):
                """Returns true if the given value is better than the 
                   actual best stored element."""
                if not self.reverse_order:
                        return v > self.elements[0][0]
                else:
                        return v < self.elements[0][0]
         

	def check(self, val, line):
                """Main method used to add elements."""
		if self.length < self.element_n:
                        self.insert((val, line))
		elif self.better_than_worst(val):
                        #del(self.elements[0])
                        self.elements[0] = (val, line)
                        self.move_down(0)
	
	def __str__(self):
		retval = "" 
                for i in range(0, self.length):
			retval += self.elements[i][1]
		return retval
				
				
def main():
	usage = '''
		%prog ID_COL VAL_COL < STDIN
		for all rows with the same value in ID_COL report only the row with the best value in VAL_COL
	'''
	parser = OptionParser(usage=usage)
	
	parser.add_option('-n', '--best_n', type=int, dest='best_n', default=1, help='report the best N rows for each ID', metavar='N')
	parser.add_option('-r', '--reverse', dest='reverse', action='store_true', default=False, help='find the lowest value')
	parser.add_option('-s', '--already_sorted', dest='already_sorted', action='store_true', default=False, help='assume the input already sorted on ID_COL')


	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	id_col = int(args[0]) - 1
	val_col = int(args[1]) - 1
	
	retval = {}
	pre_id = None
	block = None
	already_wanr = False
	for line in stdin:
		tokens = line.rstrip().split('\t')
		id = tokens[id_col]
		try:
			val = float(tokens[val_col])
		except Exception, e:
			exit("Malformed input row (%s)\n(%s)" % (line,e))

		if not options.already_sorted:
			try:
				retval[id].check(val,line)
			except KeyError:
				retval[id] = best_n(id, options.best_n, options.reverse)
				retval[id].check(val,line)
		else:
			if pre_id != id:
				if pre_id > id:
					if not already_wanr and pre_id > id:
						stderr.write("WARN: input non lexicografically sorted on ID_COL, continue aniway.")
						already_wanr = True;
					
				if block is not None:
					stdout.write(str(block))
				block = best_n(id, options.best_n, options.reverse)
			block.check(val,line)
			pre_id = id
				
	if not options.already_sorted:
		for id, block in retval.iteritems():
			stdout.write(str(block))
		#	print "\t".join(v)
	else:
		if block is not None:
			stdout.write(str(block))
			
	

		

if __name__ == '__main__':
	main()

