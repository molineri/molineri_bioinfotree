#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

my $usage="$0 [-r] [-s [-n]] [-m]  ID_COL VAL_COL < IN_FILE
	-r reverse (searches for the minimum)
	-s if IN_FILE is already sorted
	-n if IN_FILE is numerically sorted
	-m to print out all best rows (and not only the first)
";

my $reverse=0;
my $sorted=0;
my $numeric_sort=0;
my $multi=1;
GetOptions (
	'reverse|r' => \$reverse,
	'sorted|s'  => \$sorted,
	'numeric_sort|n' => \$numeric_sort,
	'multi|m'   => \$multi
) or die($usage);

die($usage) if $numeric_sort and !$sorted;

my $id_col=shift @ARGV;
my $val_col=shift @ARGV;

$id_col=~/\d+/ or die($usage);
$val_col=~/\d+/ or die($usage);
$id_col--;
$val_col--;

my %best=();
my $pre_id = undef;
while(<>){
	chomp;
	my @F=split /\t/;
	my $id=$F[$id_col];

	if($sorted and defined $pre_id){
		if($numeric_sort){
			die("ERROR: input not sorted") if $id < $pre_id;
		}else{
			die("ERROR: input not sorted") if $id lt $pre_id;
		}
	}
		

	if(!defined($best{$id})){
		if($sorted and defined $pre_id){
			foreach my $ref (@{$best{$pre_id}}){
				print @{$ref};
				last if !$multi;
			}
			%best=();
		}

		my @tmp=(\@F);
		$best{$id}=\@tmp;

	}else{
		if($best{$id}[0][$val_col] == $F[$val_col]){
			push @{$best{$id}},\@F;
		}else{
			if(!$reverse){
				if( $best{$id}[0][$val_col] < $F[$val_col]){
					my @tmp=(\@F);
					$best{$id}=\@tmp;
				}
			}else{
				if( $best{$id}[0][$val_col] > $F[$val_col]){
					my @tmp=(\@F);
					$best{$id}=\@tmp;
				}
				
			}
		}
	}
}

for(keys(%best)){
	foreach my $ref (@{$best{$_}}){
		print @{$ref};
		last if !$multi;
	}
}
