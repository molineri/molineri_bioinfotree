#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-d|downstream 500] [-i] [-A|from_poliA] chr_lengths_file UPSTREAM_LENGTH < gene_coords

-i	ignore chromosomes in stdin that are not present in chr_lengths_file
-A	produce upstream refered to the end of the transcript (the same as without -A but with the strand reversed)

chr_lengths_file:
	1 chr
	2 length

gene_coords
	1 gene_ID
	2 chr
	3 b
	4 e
	5 strand

\n";

my $help=0;
my $downstream_len=0;
my $ignore_strange_chromosomes=0;
my $from_poliA=0;
GetOptions (
	'h|help' => \$help,
	'd|downstream=i' => \$downstream_len,
	'i|ignore' => \$ignore_strange_chromosomes,
	'A|from_poliA' => \$from_poliA
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

$ARGV[0] or die("Wrong argument number");
$ARGV[1] or die("Wrong argument number");
my $chr_len_filename = $ARGV[0];
open FH, $chr_len_filename or die("Can't open file ($chr_len_filename)");

my $upstream_len = $ARGV[1];
die($usage) if $upstream_len !~ /^\d+$/;

my %chr_len=();
while(<FH>){
	chomp;
	my ($chr,$len) = split /\t/;
	$chr_len{$chr}=$len;

}

while(<STDIN>){
	chomp;
	my ($id,$chr,$b,$e,$strand) = split /\t/;
	if(not defined($chr_len{$chr})){
		if(not $ignore_strange_chromosomes){
			die("lengh of the chr ($chr) not defined");
		}else{
			next;
		}
	}
	die("out of chromosome ($id)") if $e > $chr_len{$chr};
	my $up_b=undef;
	my $up_e=undef;
	$strand = 1 if $strand eq '+';
	$strand = -1 if $strand eq '-';
	if($from_poliA){
		my $new_strand = 0;
		$new_strand = -1 if $strand == +1;
		$new_strand = +1 if $strand == -1;
		$strand = $new_strand;
	}
	if($strand == 1){
			$up_b = $b - $upstream_len;
			$up_e = $b + $downstream_len;
	}elsif($strand == -1){
			$up_b = $e - $downstream_len;
			$up_e = $e + $upstream_len;
	}else{
		die("Malforemd input.")
	}
	
	$up_b = 0 if $up_b < 0;
	$up_e =  $chr_len{$chr}  if $up_e > $chr_len{$chr};
	
	print $id,$chr,$up_b,$up_e,$strand,$upstream_len,$downstream_len,$b,$e;
}
