use warnings;
use strict;

sub merge{
	my @segs=@_;
	my @out;
	
	@segs = sort{ ${$a}[0] <=> ${$b}[0] } @segs;
	
	for(@segs){
		(my $s, my $e)=@{$_};
		my $merged=0;
		for(my $i=0; $i<scalar(@out); $i++){
			my $segment=$out[$i];
			(my $pre_s, my $pre_e)=@{$segment};
			
			my $max_s = $s < $pre_s ? $pre_s : $s;
			my $min_e = $e > $pre_e ? $pre_e : $e;
			#print $max_s,$min_e, $max_s <= $min_e;
			if ($max_s <= $min_e){
				my $min_s = $s > $pre_s ? $pre_s : $s;
				my $max_e = $e < $pre_e ? $pre_e : $e;
			
				$out[$i][0]=$min_s;
				$out[$i][1]=$max_e;
				$merged=1;
			}
		}
		if(!$merged){
			my @tmp=@{$_};
			push @out,\@tmp;
		}
	}
	return @out;
}

1
