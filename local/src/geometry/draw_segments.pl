#!/usr/bin/perl
use warnings;
use strict;
$,="\t";
$\="\n";

my @data=();

open PIPE,"|cgvs -k";
my $c=2;
while(<>){
	chomp;
	my @F=split;
	push @data,@F;
	print PIPE @F,$c,$c,0,0,0;
	print STDERR @F,$c,$c,0,0,0;
	$c++;
}

my $min=&min(@data);
my $max=&max(@data);

print PIPE $min    , $min + 1, 0 , 0 , 1, 1, 1;
print PIPE $max + 1, $max + 2, $c, $c, 1, 1, 1;

sub min{
	my $retval=undef;
	for(@_){
		$retval = $_ if not defined $retval or $_ < $retval;
	}
	return $retval;
}

sub max{
	my $retval=undef;
	for(@_){
		$retval = $_ if not defined $retval or $_ > $retval;
	}
	return $retval;
}
