#!/usr/bin/perl

use warnings;
use strict;
$,="\t";
$\="\n";

my $usage = "$0 INPUT_FILE
column expected in input
b	e	orig_b	orig_e	id
^       ^
|       |
of the intersection

OUTPUT:
id	abs_value_of_covered	fraction_covered
";

die($usage) if defined $ARGV[0] and $ARGV[0] =~ /^-h/;

my @unions=();
my @u=();
my $last_b   = undef;
my $last_len = undef;
my $last_id  = undef;
while(<>){
	chomp;
	my ($b, $e, $orig_b, $orig_e, $id) = split;
	
	if(defined $last_id and $last_id lt $id){
		my @tmp = @u;
		push @unions, \@tmp;
		my_print($last_id, $last_len, @unions);
		@unions = ();
		@u = ();
		$last_b = undef;
	}
	
	die("Disorder found in id column") if defined $last_id and $last_id gt $id;
	die("Error: undefined b")   if !defined($b)   or !length($b);
	die("Error: undefined e")   if !defined($e)   or !length($e);
	die("Error: undefined id")  if !defined($id)   or !length($id);
	die("Error: b>e") if $b>$e;
	die("Error: b>e") if $orig_b>$orig_e;
	die("Error: disorder found" ) if defined($last_b) and $b < $last_b;

	if(!@u){
		@u=($b, $e);
	}elsif($b > $u[1]){
		my @tmp = @u;
		push @unions, \@tmp;
		@u=($b, $e);
	}else{
		$u[1] = $e > $u[1] ? $e : $u[1];
	}

	my $len = $orig_e - $orig_b;
	die("Different original length for the same id ($last_id)") if defined $last_len and $id eq $last_id and $len != $last_len;
	$last_b   = $b;
	$last_id  = $id;
	$last_len = $orig_e - $orig_b;
}

push @unions, \@u;
my_print($last_id, $last_len, @unions) if defined(${$unions[0]}[0]);



sub my_print
{
	my $id = shift;
	my $len = shift;
	my $sum=0;
	for(@unions){
		$sum += ${$_}[1] - ${$_}[0];
	}
	die ("Coverage > 1" ) if $sum > $len;
	print $id , $sum, $sum/$len;
}
