#!/usr/bin/perl
use strict;
use warnings;

$,="\t";
$\="\n";

my %first=();
my %offset=();

open FIRST,shift(@ARGV) or die("Can't open file");

my $pre_first=-1;
while(<FIRST>){
	chomp;
	my @F = split;
	$F[0]=~s/chr//;
	
	die("ERROR: the file must be sorted") if $pre_first > $F[1];
	my @tmp=($F[1],$F[2]);
	push @{$first{$F[0]}}, \@tmp;
}

while(<>){
	chomp;
	my @F=split;
	$F[0]=~s/chr//;

	my $i=0;
	if(defined($offset{$F[0]})){
		my $i=$offset{$F[0]};
	}

	while( $i < scalar( @{$first{$F[0]}}  ) ){
		my $b = ${${$first{$F[0]}}[$i]}[0];
		my $e = ${${$first{$F[0]}}[$i]}[1];

		if($e <= $F[1]){
			$i++;
			next;
		}elsif( $b >= $F[2]){
			$offset{$F[0]}=$i;#il prossimo b che prendo e` sicuramente piu` grande di quello attuale
			last;
		}else{
			print $_, @{${$first{$F[0]}}[$i]}, @{&distance($F[1],$F[2],$b,$e)};
			$offset{$F[0]}=$i;
		}
	}
}

sub distance{
	my $b1 = shift;
	my $e1 = shift;
	my $b2 = shift;
	my $e2 = shift;
	
	my $max_b = $b1 > $b2 ? $b1 : $b2;
	my $min_b = $b1 > $b2 ? $b2 : $b1;
	my $min_e = $e1 < $e2 ? $e1 : $e2;
	my $max_e = $e1 < $e2 ? $e2 : $e1;

	my $int = $min_e - $max_b;
	my $sum = ($e1-$b1) + ($e2-$b2);
	my $mrg = $max_e - $min_b;
	 
	$int > 0 or warn("ERROR: unaxpected empty intersection ($int)");
	
	my @tmp = ($int, $mrg, ($sum - 2*$int));
	return \@tmp;
}
