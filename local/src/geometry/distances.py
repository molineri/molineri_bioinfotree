#!/usr/bin/env python

from sys import stdin, stderr
from itertools import groupby, izip
from operator import itemgetter
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from collections import defaultdict


col_id  = 0
col_chr = 1
col_b   = 2
col_e   = 3

def read_file(filename):
	out = []
	with file(filename, 'r') as fd:
		prev_line = None
		for lineno, line in enumerate(fd):
			tokens = safe_rstrip(line).split('\t')
			assert len(tokens) == 4, "not enought column in file (%s)" % filename

			tokens[col_b] = int(tokens[col_b])
			tokens[col_e] = int(tokens[col_e])
			
			cur_line = (tokens[col_chr], tokens[col_b])
			if prev_line is not None:
				if prev_line > cur_line:
					exit('Lexicografic disorder found at line %d of file %s.\n(%s, %d <-> %s, %d)' % (lineno+1, filename, cur_line[0], cur_line[1], prev_line[0], prev_line[1]))
			prev_line = cur_line
			
			out.append((tokens[0],tokens[1],int(tokens[2]),int(tokens[3])))
	return out

def main():
	usage = format_usage('''
		%prog SEGMENT_FILE1 SEGMENT_FILE2

		compute the distances between each couple of segments
		the first element of the couple taken from SEGMENT_FILE1 the second from the SEGMENT_FILE2

		.META: SEGMENT_FILE*
			1 id
			2 chr
			3 b
			4 e

		.DOC: SEGMENT_FILE1*
			must be sorted like this:
			bsort -k2,2 -k3,3n

		.META: stdout
			1 id1		id of a segment taken form SEGMENT_FILE1
			2 id2           id of a segment taken form SEGMENT_FILE2
			3 distance 	0 if id1 and id2 overlap
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-m', '--max_dist', type=int, dest='max_dist', default=None, help='do not show (and calculate) distances bigger than M [default: show all distances]', metavar='M')
	parser.add_option('-n', '--nearest', action='store_true', dest='nearest', default=None, help='show only the nearest segment in SEGMENT_FILE2 for each SEGMENT_FILE1 [default: show all distances]')
	options, args = parser.parse_args()
	
	if len(args) != 2:
		exit('Unexpected argument number.')
	
	if options.max_dist is not None:
		exit("Option -m is bugged, use insthead windowBed insthead")

	seg1 = read_file(args[0])
	seg2 = read_file(args[1])
	
	offset = 0
	seg2_len = len(seg2)
	for v1 in seg1:
		dist=None
		for i in xrange(offset, seg2_len):
			v2 = seg2[i]
			if v1[col_chr] != v2[col_chr]:
				continue
			
			if options.max_dist is not None:
				if v2[col_b] > v1[col_e] + options.max_dist:
					break		# since v2 are sorted
				if v1[col_b] > v2[col_e] + options.max_dist:
					offset = i	# since v1 are sorted
					continue	


			if v2[col_b] <= v1[col_e] and v2[col_b] >= v1[col_b]: #overlap
				dist=(v2[col_id], 0)
				print "%s\t%s\t%d" % (v1[col_id], v2[col_id], 0)
				continue

			if v2[col_e] <= v1[col_e] and v2[col_e] >= v1[col_b]: #overlap
				dist=(v2[col_id], 0)
				print "%s\t%s\t%d" % (v1[col_id], v2[col_id], 0)
				continue

			if v1[col_b] <= v2[col_e] and v1[col_b] >= v2[col_b]: #overlap
				dist=(v2[col_id], 0)
				print "%s\t%s\t%d" % (v1[col_id], v2[col_id], 0)
				continue

			d1 = v2[col_b] - v1[col_e]
			d2 = v1[col_b] - v2[col_e]

			# there are no more overlap then there are only 2 cases:
			#
			# v2    |-------------| 
			# v1   				|-----------|
			#
			# in this case d1 < 0 and d2 > 0
			#
			# v2                            |-----------| 
			# v1   	|-------------|
			#
			# in this case d1 > 0 and d2 < 0
			#
			# then che correct d is the positive (max) one

			d = max(d1,d2)

			if options.nearest:
				if dist is None or d < dist[1]:
					dist=(v2[col_id], d)
					offset = i   # since v1 are sorted, the next v1 can't be closer to any v2 preceding this
					continue

				if dist is not None and d > dist[1]:
					if d2 < 0:# the v2 trespassing v1
						break #all the other v2 can only be more distant
			else:
				print "%s\t%s\t%d" % (v1[col_id], v2[col_id], max(d1,d2))

		if options.nearest and dist is not None: # dist can be none for example becouse there are no v2 on the same chromosome
			if dist[1]!=0: # overlap arleady printed
				print "%s\t%s\t%d" % (v1[col_id], dist[0], dist[1])


if __name__ == '__main__':
	main()
