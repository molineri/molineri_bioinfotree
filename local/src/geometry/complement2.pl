#!/usr/bin/perl
use warnings;
use strict;
use Bit::Vector;
$,="\t";
$\="\n";

my $last_b = undef;
my $last_e = undef;
my $last_int_b = undef;
my $last_chr = undef;
my $vect=undef;
while(<>){
	chomp;
	my ($chr,$int_b,$int_e,$b,$e)=split;
	
	die("ERROR: data not grouped") if defined($last_b) and $last_b != $b;
	die("ERROR: data not grouped") if defined($last_e) and $last_e != $e;
	die("ERROR: data not grouped") if defined($last_chr) and $last_chr ne $chr;

	$vect = Bit::Vector->new($e - $b) if !defined($vect);

	$vect->Interval_Fill($int_b - $b, $int_e - $b - 1);

	$last_b=$b;
	$last_e=$e;
	$last_chr=$chr;
	$last_int_b = $int_b;
}

$vect->Flip();

my $begin=0;
while (($begin < $vect->Size()) && ((my $min, my $max) = $vect->Interval_Scan_inc($begin))){
	print $last_chr, $last_b + $min , $last_b + $max + 1;
	$begin=$max + 1;
}

