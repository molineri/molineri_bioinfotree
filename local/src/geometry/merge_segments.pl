#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;

my $usage="usage: $0 [-r] file
file: id start stop
";

my $reverse=undef;

GetOptions (
	'reverse|r' => \$reverse,
) or die($usage);

my $segments=undef;

my $pre_id=undef;
my $i=-1;#non conta le righe ma gli elementi di ${$segments}{$id} che sono <= alle righe
my $prestart=undef;
while(<>){#mi aspetto dati ordinati per id (eventualewmten id=cromosoma) e per start nel file di input
	chomp;
	my ($id,$start,$stop)=split /\t/;

	if($start > $stop){
		if($reverse){
			my $tmp=$start;
			$start=$stop;
			$stop=$tmp;
		}else{
			die("ERROR: reversed without -r flag\n$usage");
		}
	}

	
	if(!defined(${$segments}{$id})){
		my @tmp=();
		${$segments}{$id}=\@tmp;
	}
	
	my $pre = undef;
	if(defined($pre_id) and $id eq $pre_id){
		if(defined($prestart) and $prestart > $start){
			die("ERROR: require sorted input\n$_");
		}
		$prestart=$start;
		$pre=@{${$segments}{$id}}[$i];
	}else{
		$pre_id=undef;
		$prestart=undef;
		$i=-1;
	}

	if(defined($pre_id) and $id eq $pre_id and $start < ${$pre}[1]){#sovrapposizione
		$stop>${$pre}[1] and ${$pre}[1]=$stop;#aggiorno lo stop solo se il novo stop e` piu` grande`
	}else{
		my @tmp=($start,$stop);
		push @{${$segments}{$id}}, \@tmp;
		$i++
	}

	$pre_id=$id;
}


$,="\t";
$\="\n";

my @ids = sort keys %{$segments};
foreach my $id (@ids){
	for(@{${$segments}{$id}}){
		print $id,@{$_};
	}
}
