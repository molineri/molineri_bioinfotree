#!/usr/bin/env python

from itertools import groupby, izip
from operator import itemgetter
from optparse import OptionParser
from sys import exit, stdin
from vfork.io.util import safe_rstrip
from vfork.util import exit

def parse_file_spec(spec):
	tokens = spec.split(':')
	if len(tokens) > 4:
		exit('Malformed file spec: %s' % spec)
	
	res = [ tokens[0], 0, 1, 2 ]
	
	if len(tokens) > 1:
		try:
			v = int(tokens[1])
			if v < 0:
				raise ValueError
			res[1] = v - 1
		except ValueError:
			exit('Invalid id column in spec: %s' % spec)
	
	if len(tokens) > 2:
		try:
			v = int(tokens[2])
			if v < 0 or v == res[1]:
				raise ValueError
			res[2] = v - 1
		except ValueError:
			exit('Invalid start column in spec: %s' % spec)
	
	if len(tokens) > 3:
		try:
			v = int(tokens[3])
			if v < 0 or v == res[1] or v == res[2]:
				raise ValueError
			res[3] = v - 1
		except ValueError:
			exit('Invalid stop column in spec: %s' % spec)
	
	return res

def make_disjoint_printer(fd):
	def disjoint_printer(item):
		print >>fd, '\t'.join(str(i) for i in item if i is not None)
	return disjoint_printer

def iter_lines(filename, id_col, start_col, stop_col, fasta_printer=None):
	if filename == '-':
		fd = stdin
	else:
		fd = file(filename, 'r')
	
	min_col_num = max(id_col, start_col, stop_col) + 1
	prev_line = None
	
	for lineno, line in enumerate(fd):
		line = safe_rstrip(line)
		if len(line) and line[0] == '>':
			if fasta_printer:
				fasta_printer(line)
				continue
			else:
				exit('Unexpected FASTA header at line %d of file %s.\n(%s)' % (lineno+1, filename, line))
		
		tokens = line.split('\t')
		if len(tokens) < min_col_num:
			exit('Insufficient token number at line %d of file %s.\n(%s)' % (lineno+1, filename, line))

		try:
			start = int(tokens[start_col])
			if start < 0:
				raise ValueError
		except ValueError:
			exit('Malformed start coordinate at line %d of file %s.\n(%s)' % (lineno+1, filename, line))

		try:
			stop = int(tokens[stop_col])
			if stop < 0:
				raise ValueError
		except ValueError:
			exit('Malformed stop coordinate at line %d of file %s.\n(%s)' % (lineno+1, filename, line))
		
		if stop <= start:
			exit('Invalid stop coordinate at line %d of file %s.\n(%s)' % (lineno+1, filename, line))

		cur_line = (tokens[id_col], start)
		if prev_line is not None:
			if prev_line > cur_line:
				exit('Lexicografic disorder found at line %d of file %s.\n(%s)' % (lineno+1, filename, line))
		prev_line = cur_line
		
		if len(tokens) != 3:
			other_tokens = '\t'.join(tokens[i] for i in xrange(len(tokens)) if i not in (id_col, start_col, stop_col))
			yield tokens[id_col], start, stop, other_tokens
		else:
			yield tokens[id_col], start, stop, None

def consume_iterator(it, dp):
	for v in it:
		if dp:
			dp(v)

class ValueWindow(object):
	def __init__(self, it, disjoint_cbk):
		self.it = it
		self.window = []
		self.marks = []
		self.empty = []
		self.eof = False
		self.disjoint_cbk = disjoint_cbk
	
	def drop_until_id(self, id):
		drop_idx = 0
		while drop_idx < len(self.window) and self.window[drop_idx][0] < id:
			drop_idx += 1
		
		if drop_idx > 0:
			if self.disjoint_cbk:
				for item in (self.window[i] for i in xrange(drop_idx) if not self.marks[i]):
					self.disjoint_cbk(item)
				self.marks = self.marks[drop_idx:]
			self.window = self.window[drop_idx:]
		
		while len(self.window) == 0:
			try:
				v = self.it.next()
				if v[0] >= id:
					self.window.append(v)
					if self.disjoint_cbk:
						self.marks.append(False)
					return v[0] == id
			
			except StopIteration:
				self.eof = True
				raise EOFError
		
		return True
	
	def iter_range(self, id, start, stop):
		if len(self.window) > 0:
			first_idx = 0
			while first_idx < len(self.window) and self.window[first_idx][0] < id:
				first_idx += 1
			
			if first_idx > 0:
				if self.disjoint_fd:
					for item in (self.window[i] for i in xrange(first_idx) if not self.marks[i]):
						self.disjoint_cbk(item)
					self.marks = self.marks[first_idx:]
				self.window = self.window[first_idx:]
			
			if self.disjoint_cbk:
				aux = []
				for idx, item in enumerate(self.window):
					if item[0] != id:
						break
					elif item[2] <= start:
						aux.append(idx)
						if not self.marks[idx]:
							self.disjoint_cbk(item)
				
				aux.reverse()
				for idx in aux:
					del self.window[idx]
					del self.marks[idx]
				
			else:
				self.window = [ v for v in self.window if not (v[0] == id and v[2] <= start) ]
		
		last_idx = 0
		while last_idx < len(self.window):
			last = self.window[last_idx]
			if last[0] != id or last[1] >= stop:
				return self.window[:last_idx].__iter__()
			else:
				if self.disjoint_cbk:
					self.marks[last_idx] = True
				last_idx += 1

		if not self.eof:
			while True:
				try:
					v = self.it.next()
					if v[0] != id or v[2] > start:
						self.window.append(v)
					elif self.disjoint_cbk:
						self.disjoint_cbk(v)
					
					if v[0] != id or v[1] >= stop:
						if self.disjoint_cbk:
							self.marks.append(False)
						return self.window[:-1].__iter__()
					elif self.disjoint_cbk:
						self.marks.append(True)

				except StopIteration:
					self.eof = True
					break
		
		return self.window.__iter__()
	
	def flush(self):
		if self.disjoint_cbk:
			for item in (self.window[i] for i in xrange(len(self.window)) if not self.marks[i]):
				self.disjoint_cbk(item)
		
		consume_iterator(self.it, self.disjoint_cbk)

def main():
	parser = OptionParser(usage='%prog COORDS1:ID_COL:START_COL:STOP_COL COORDS2:ID_COL:START_COL:STOP_COL')
	parser.add_option('-f', '--print-fasta', dest='print_fasta', action='store_true', default=False, help='print FASTA header on the left side as soon as they\'re found')
	parser.add_option('-l', '--disjoint-left', dest='disjoint_left', help='write to FILE the list of disjoint regions on the left side', metavar='FILE')
	parser.add_option('-r', '--disjoint-right', dest='disjoint_right', help='write to FILE the list of disjoint regions on the right side', metavar='FILE')
	options, args = parser.parse_args()

	if options.disjoint_right :
	 	exit('ERROR: -r flag not implemented')

	if len(args) != 2:
		exit('Unexpected argument number.')
	
	file_spec1 = parse_file_spec(args[0])
	file_spec2 = parse_file_spec(args[1])
	
	dpl = make_disjoint_printer(file(options.disjoint_left, 'w')) if options.disjoint_left else None
	dpr = make_disjoint_printer(file(options.disjoint_right, 'w')) if options.disjoint_right else None
	
	if options.print_fasta:
		def print_fasta(header):
			print header
		file_spec1.append(print_fasta)
	it1 = iter_lines(*file_spec1)
	it2 = ValueWindow(iter_lines(*file_spec2), dpr)
	
	for id1, values1 in groupby(it1, itemgetter(0)):
		try:
			if not it2.drop_until_id(id1):
				consume_iterator(values1, dpl)
				continue
		except EOFError:
			# consume input to check for order and to output disjoint regions
			consume_iterator(it1, dpl)
			return
		
		for v1 in values1:
			disjoint = True
			
			for v2 in it2.iter_range(v1[0], v1[1], v1[2]):
				disjoint = False
				b = max(v1[1], v2[1])
				e = min(v1[2], v2[2])
				
				res = [ v1[0], b, e, v1[1], v1[2], v2[1], v2[2] ]
				if v1[3] is not None:
					res.append(v1[3])
				if v2[3] is not None:
					res.append(v2[3])
				print '\t'.join(str(i) for i in res)
			
			if disjoint and dpl:
				dpl(v1)
	
	it2.flush()

if __name__=='__main__':
	main()
