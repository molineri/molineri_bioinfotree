#!/usr/bin/env python
from itertools import groupby
from operator import itemgetter
from optparse import OptionParser
from sys import exit, stdin, stderr, argv
from vfork.io.colreader import Reader
from vfork.util import exit

def iter_intersection_rows(fd):
	reader= Reader(fd,"0s,1u,2u,3u,4u,7s", False)
	last_region_id = None
	last_chr = None
	last_b = None
	last_e = None
	for chr, int_b, int_e, b, e, region_id in reader:
		if int_b >= int_e: exit("Stop <= Start in col 2,3")
		if b >= e: exit("Stop <= Start in col 4,5")
		if b > int_b or e < int_e: exit("Invalid intersection")
		if last_region_id == region_id:
			if last_chr != chr or last_b != b or last_e != e:
				exit("Region with different b and e in col 4,5. Do you inverted the order of files in intersection?")
		elif last_region_id is not None and last_region_id > region_id:
			exit("Disorder found in column 8")
		else:
			last_region_id = region_id
			last_chr = chr
			last_b = b
			last_e = e
		
		yield chr, int_b, int_e, b, e, region_id

def iter_intersection_blocks(fd):
	for region_id, rows in groupby(iter_intersection_rows(fd), itemgetter(5)):
		rows = sorted(rows, key=itemgetter(1))
		
		chr = rows[0][0]
		b = rows[0][3]
		e = rows[0][4]
		
		rows = [ (r[1], r[2]) for r in rows ]
		for i in xrange(1, len(rows)):
			if rows[i][0] < rows[i-1][1]:
				exit('Overlapping regions in %s' % region_id)
		
		rows.insert(0, (None, b))
		rows.append((e, None))
		
		yield region_id, chr, rows

def iter_rows(fd):
	reader= Reader(fd,"0s,1u,2u,3s", False)
	last_chr = None
	last_e = None
	for chr, b, e, region_id in reader:
		if b >= e: exit("Stop <= Start in col 2,3")
		if last_chr == chr:
			if last_e > b:
				exit("Disorder or overlapping regions found")
		elif last_chr > chr:
			exit("Disorder found")
		last_e = e
		
		yield chr, b, e, region_id

def main():
	parser = OptionParser(usage='''%prog < REGIONS
		.META: REGIONS
			1 chr
			2 b
			3 e
			4 id

		.DOC: REGIONS
			regions must be sorted and not overlapping
	''')

	parser.add_option("-i", "--intersection", dest='intersection', action='store_true', default=False, help="use intersection output as input")
	parser.add_option("-p", "--point", dest='point', action='store_true', default=False, help="allow 0 length segments in output")
	
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	if options.intersection:
		for region_id, chr, coords in iter_intersection_blocks(stdin):
			for i in xrange(1, len(coords)):
				b = coords[i-1][1]
				e = coords[i][0]
				if b != e:
					print '%s\t%d\t%d\t%s' % (chr, b, e, region_id)
	else:
		for chr, rows in groupby(iter_rows(stdin), itemgetter(0)):
			last_row = rows.next()
			for row in rows:
				if last_row[2] != row[1] or options.point:
					print "%s\t%d\t%d\t%s\t%s" % (chr, last_row[2], row[1], last_row[3], row[3])
				last_row = row

if __name__ == '__main__':
	main()
