// Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

#include "fasta.h"
#include <iostream>
#include <limits>
#include "seedhash.h"
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <vector>

using namespace std;

static const int seed_size = 10;
static int min_query_size = numeric_limits<int>::max();

struct SeedInstance
{
	int seq_id;
	int pos;
};

typedef vector<SeedInstance> SeedInstances;
typedef vector<const FastaBlock*> QueryBlocks;


bool need_help(int argc, char **argv)
{
	for (int i = 1; i < argc; i++)
	{
		if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0)
			return true;
	}
	return false;
}

void store_hash(const FastaBlock* block, const int seq_id, const unsigned int pos, SeedHash& hash, SeedInstances** seeds)
{
	if (!hash.compute(block->sequence.data(), pos))
	{
		cerr << "Invalid character in query: " << block->label << endl;
		exit(1);
	}
	
	if (!seeds[hash])
		seeds[hash] = new SeedInstances;
	
	const SeedInstance instance = { seq_id, pos };
	seeds[hash]->push_back(instance);
}

void load_queries(const char* filename, QueryBlocks& queries, SeedInstances** seeds)
{
	FastaReader query_reader(filename);
	while (true)
	{
		const FastaBlock* block = query_reader.get_block();
		if (!block) break;
	
		if (block->sequence.length() < 2*seed_size)
		{
			cerr << "Query sequence too short: " << block->label << endl;
			exit(1);
		}
		
		const int query_size = block->sequence.length();
		if (min_query_size > query_size)
			min_query_size = query_size;
		
		queries.push_back(block);
	}
	
	SeedHash hash(seed_size);
	int seq_id;
	QueryBlocks::const_iterator it, end;
	for (it = queries.begin(), end = queries.end(), seq_id = 0; it != end; it++, seq_id++)
	{
		store_hash(*it, seq_id, 0, hash, seeds);
		store_hash(*it, seq_id, min_query_size - seed_size, hash, seeds);
	}
}

bool compare_sequences(const FastaBlock& query, const char* database, const int database_length, const int offset, const int seed2_offset)
{
	const int query_len = query.sequence.length();
	if (offset + query_len > database_length)
		return false;
	
	const int gap = seed2_offset - seed_size;
	if (gap > 0 && strncmp(query.sequence.data()+seed_size, database+offset+seed_size, gap) != 0)
		return false;
	
	const int trail = query_len - seed2_offset - seed_size;
	if (trail > 0 && strncmp(query.sequence.data()+seed2_offset+seed_size, database+offset+seed2_offset+seed_size, trail) != 0)
		return false;
	
	return true;
}

int main(int argc, char** argv)
{
	if (need_help(argc, argv))
	{
		cout << "Usage: " << argv[0] << " DATABASE_FASTA QUERY_FASTA" << endl;
		return 1;
	}
	else if (argc != 3)
	{
		cerr << "Unexpected argument number." << endl;
		return 1;
	}
	
	QueryBlocks queries;
	SeedInstances** seeds = new SeedInstances*[1024*1024];
	bzero(seeds, sizeof(SeedInstances*)*1024*1024);
	load_queries(argv[1], queries, seeds);
	
	FastaReader database_reader(argv[2]);
	while (true)
	{
		const FastaBlock* block = database_reader.get_block();
		if (!block) break;
		
		const char* data = block->sequence.data();
		const int data_len = block->sequence.length();
		SeedHash head_hash(seed_size), tail_hash(seed_size);
		for (int i = 0; i <= data_len - min_query_size; i++)
		{
			if (i == 0)
			{
				head_hash.compute(data, i);
				tail_hash.compute(data, i + min_query_size - seed_size);
			}
			else
			{
				head_hash.update(data, i);
				tail_hash.update(data, i + min_query_size - seed_size);
			}
			
			if (head_hash == -1 || tail_hash == -1 || !seeds[head_hash] || !seeds[tail_hash])
				continue;
			
			SeedInstances::const_iterator hit = seeds[head_hash]->begin(), hend = seeds[head_hash]->end();
			SeedInstances::const_iterator tit = seeds[tail_hash]->begin(), tend = seeds[tail_hash]->end();
			while (hit != hend && tit != tend)
			{
				if (hit->seq_id == tit->seq_id)
				{
					if (hit->pos == 0 && tit->pos > 0)
					{
						const FastaBlock* query = queries[hit->seq_id];
						if (compare_sequences(*query, data, data_len, i, tit->pos))
							cout << query->label << "\t" << block->label << "\t" << i << endl;
						
						hit++;
						tit++;
					}
					else if (hit->pos > 0)
					{
						hit++;
						tit++;
					}
					else
						tit++;
				}
				else if (hit->seq_id < tit->seq_id)
					hit++;
				else
					tit++;
			}
		}
		
		delete block;
	}
	
	return 0;
}
