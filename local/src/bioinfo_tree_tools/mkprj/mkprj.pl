#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

my $phase_rules = <<END;
include \$(BIOINFO_ROOT)/local/share/makefile/global-rules.mk

layout: \$(PHASES) \$(addsuffix /rules.mk,\$(PHASES)) \$(addsuffix /makefile,\$(PHASES))

\$(PHASES):
	mkdir \$@

%/rules.mk: ../../local/share/rules-%.mk %
	cd \$(word 2,\$^) && ln -sf ../\$< rules.mk

%/makefile: makefile %
	cd \$* && ln -sf ../\$\$(basename \$@)
END

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] [-n|dryrun] [-p|phases phase1 -p|phases phase2 ...] [-f|force] [-s|skip] prjname version\n
create the local/ directory structure for a prj and a skeleton based on prjname and version
-f 	force to overwrite the roules.mk if exixting (if at least one pahase is sindicated)
-s 	skip to overwrite the roules.mk if exixting (if at least one pahase is sindicated)";

my $help=0;
my $force=0;
my $skip=0;
my $dryrun=0;
my @phases=();
GetOptions (
	'h|help' => \$help,
	'n|dryrun' => \$dryrun,
	'f|force' => \$force,
	's|skip' => \$skip,
	'p|phases=s@' => \@phases
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

die("Wrong argument number") if scalar(@ARGV) != 2;

my $prjname = shift;
my $version = shift;
die("Do not include dataset/ in the version name") if $version =~ /^dataset/;
my $version_underscores = $version;
$version_underscores =~ s/\//_/g;
my $prj_root = $ENV{'BIOINFO_ROOT'}."/prj/$prjname";

if( (!$dryrun and !$force) and !$skip and (scalar(@phases) and -e "$prj_root/local/share/rules/rules.mk") ){
	warn("The file $prj_root/local/share/rules/rules.mk exist, it would be overwritten if you do mkprj with phases. Aborted.\n Use -f to force / -s to skip");
}

my $sh = "mkdir -p $prj_root;\n";
$sh .= "mkdir -p $prj_root/local;\n";
$sh .= "mkdir -p $prj_root/local/bin;\n";
$sh .= "mkdir -p $prj_root/local/src;\n";
$sh .= "mkdir -p $prj_root/local/share;\n";
$sh .= "mkdir -p $prj_root/local/share/rules;\n";
$sh .= "mkdir -p $prj_root/local/share/makefiles;\n";
$sh .= "mkdir -p $prj_root/local/share/data;\n";
$sh .= "mkdir -p $prj_root/local/share/doc;\n";
$sh .= "mkdir -p $prj_root/dataset/;\n";
$sh .= "mkdir -p $prj_root/dataset/$version;\n";
$sh .= "link_install -f $prj_root/local/share/makefiles/makefile_$version_underscores $prj_root/dataset/$version/makefile;\n";

if(!scalar(@phases)){
	$sh .= "link_install -f $prj_root/local/share/rules/rules.mk $prj_root/dataset/$version/rules.mk;\n";
}else{
	for(@phases){
		$sh .= "mkdir -p $prj_root/dataset/$version/$_;\n";
		$sh .= "link_install -f $prj_root/local/share/makefiles/makefile_$version_underscores $prj_root/dataset/$version/$_/makefile;\n";
		$sh .= "link_install -f $prj_root/local/share/rules/rules-$_.mk $prj_root/dataset/$version/$_/rules.mk;\n";
	}
}

my $rules_content= "PHASES := " . join(" ",@phases) . "\n";
$rules_content .= $phase_rules;

if($dryrun){
	print $sh;
	if(scalar(@phases)){
		print "cat $prj_root/local/share/rules/rules.mk << EOF";
		print $rules_content;
		print "EOF";
	}
}else{
	`$sh`;
	exit $? if $?;
	if(scalar(@phases) and not $skip){
		open FH,">$prj_root/local/share/rules/rules.mk" or die("Can't open file ($prj_root/local/share/rules/rules.mk)");
		print FH $rules_content;
	}
}
