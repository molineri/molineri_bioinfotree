#!/bin/bash

if [ "$1" = "--help" ]; then
	echo "redate all files to a month before the actual month (bug on Jan)";
	exit;
fi;

ls -l --full-time | unhead \
| grep -v '.mk$' \
| grep -v 'makefile' \
| awk -F " " '{print "touch -d '\''"$6 " " $7 "'\'' " $9}' | awk -F'-' 'BEGIN{OFS="-"}{$3=$3-1; print $0}'
