#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] [-n|dryrun] task_name template_makefile rules instance_dir < substitution\n
build a task instance
the substitution are applied to template_makefile before link in the instance_dir

Substituition are in the form
s/__SPECIES__/hsapiens/
s/__VERSION__/18/

-n 	dry run
";

my $help=0;
my $dryrun=0;
GetOptions (
	'h|help' => \$help,
	'n|dryrun' => \$dryrun,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

die("Wrong argument number") if scalar(@ARGV) != 4;

my $taskname = shift;
my $template_makefile = shift;
my $rules_path = shift;
my $instance_dir = shift;
my $instance_dir_underscores = $instance_dir;
$instance_dir_underscores =~ s/\//_/g;
my $task_root = $ENV{'BIOINFO_ROOT'}."/task/$taskname";

my @subst = <>;

my $sh = "mkdir -p $task_root/local/share/makefiles;\n";
$sh .= "mkdir -p $task_root/dataset/$instance_dir;\n";
$sh .= "link_install -f $rules_path $task_root/dataset/$instance_dir/rules.mk;\n";

my $dest_makefile =  "$task_root/local/share/makefiles/makefile_$instance_dir_underscores";

open FHR,$template_makefile or die("Can't open file ($template_makefile)");
open FHW,">$dest_makefile" or die("Can't open file ($dest_makefile)") if not $dryrun;
my $makeifle_content = "";
while(<FHR>){
	chomp;
	foreach my $s (@subst){
		eval($s);
	}
	print FHW if not $dryrun;
	$makeifle_content .= "$_\n";
}

$sh .= "link_install -f $dest_makefile $task_root/dataset/$instance_dir/makefile;\n";

if($dryrun){
	print $sh;
	print "----------------------\ncontent of $dest_makefile will be:\n\n";
	print $makeifle_content 

}else{
	`$sh`;
	exit $? if $?;
}
