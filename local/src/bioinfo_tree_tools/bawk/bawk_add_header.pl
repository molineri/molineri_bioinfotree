#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] FILENAME\n
	print the header taken form bawk -M of FILENAME; then append the content of the file
";

my $help=0;
my $glue=",";
GetOptions (
	'h|help' => \$help,
	'g|glue=s' => \$glue,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my $filename = $ARGV[0];
system("bawk -M $filename | cut -f 2 | transpose");

if($filename=~/.gz$/){
	system("zcat $filename")
}else{
	system("cat $filename")
}
