#!/usr/bin/perl
#
# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2010 Ivan Molineris <ivan.molineris@gmail.com>

use warnings;
use strict;
use lib $ENV{'BIOINFO_ROOT'}.'/local/lib/perl';
use MakefileParser;
use Getopt::Long;
#$,="\t";
#$\="\n";


$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [--debug] [-f makefile] [-e|extended] [-v|assing var=val] [-M|meta] '{awk program}' data_filename\n

-e	change the regexp used to identify named fileds: a named field like \$\$NF (with a double \$)
	is passed to awk as \$NF ad do not trigger the logic of named fields.
	Be carrefoul with -e option: do not confuse \$NF with NF. It is usefull only if you want 
	to use a variabile to indicate a field number or if you wont to pass shell variables to awk.

-M	print the .MTEA for the given file and exit

-v|assign var=val
	Assign the val to the variable var inside the bawk program
";

my $debug=0;
my $makefile_name = undef;
my $extended =  0;
my $help=0;
my $print_META_only = 0;
my @assign = ();
GetOptions (
	'debug' => \$debug,
	'f=s' => \$makefile_name,
	'e' => \$extended,
	'v|assign=s' => \@assign,
	'h|help' => \$help,
	'M|meta' => \$print_META_only,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}


my $meta_re = '\$(\w*(?:[a-z]|[A-Z])\w*)\W'; 	#the regexp match a $ followed by a word with at least 1 non \d char

if($extended){
	$meta_re = "[^\$]$meta_re"; 	#the regexp match a single $
	#a double $$ can be used to skip the parsing of the variable, as for $NF to print the last field of a row.
}

my $awk_prog = undef;

if(!$print_META_only){
	$awk_prog = shift;
	die($usage) if !$awk_prog;
	$awk_prog = "$awk_prog ";
}

my $data_filename = shift;

die("Wrong argument number (".join("\t",@ARGV).")") if scalar(@ARGV) != 0;

my $needs_meta = 0;

$needs_meta = 1 if $print_META_only or $awk_prog =~ /$meta_re/; #the regexp match a $ followed by a word with at least 1 non \d char

die("meta data required but there is no filename as second argument") if $needs_meta and !$data_filename;

if($needs_meta){
	die("File ($data_filename) do not exists") if !-e $data_filename;
	
	my $data_path = $ENV{'PWD'};
	my $data_filename_only = undef;
	$data_filename = './'.$data_filename if $data_filename !~ /\//;
	$data_filename =~ /^(.*)\/([^\/]+)$/;
	if(defined($1) and $1 ne "" and $1 ne "." ){
		$data_path = $1;
	}
	$data_filename_only = $2;
	if(defined($makefile_name)){
		die("Opion -f bugous and suspended, try without -f")
	}
	$makefile_name = "$data_path/makefile" if not defined $makefile_name;
	my $makefile_rules_name = "$data_path/rules.mk";

	my $makefile_content = get_makefile_content($makefile_name);
	$makefile_content .= get_makefile_content($makefile_rules_name) if -e $makefile_rules_name;

	my $meta = find_metadata($makefile_content,$data_filename_only);
	if($print_META_only){
		my @rows = split(/\n/,$meta);
		for(@rows){
			s/^\t//;
			print "$_\n";
		}
		exit(0);
	}
	
	my %fields = ();
	while($awk_prog =~ /$meta_re/g){
		$fields{$1}=0;
	}
	
	for(keys %fields){
		$meta =~ /^\t(\d+)\s+$_(?:\s|$)/m or die("metadata not found for field ($_) in file ($data_filename).");
		$fields{$_}=$1;
	}
	
	for(keys %fields){
		my $col = $fields{$_};
		$awk_prog =~ s/\$$_(\W)/\$$col$1/g;
	}
}

###############################
#
#	ranges
#

while($awk_prog =~ /\$(\d+~\d+)[^\w]/){
	my $from = $1;
	my @to = ();
	$from =~/(\d+)~(\d+)/;
	my $range_b = $1;
	my $range_e = $2;
	for(my $i=$range_b; $i<=$range_e; $i++){
		push(@to,$i);
	}
	my $to_string = join(', $',@to);
	$awk_prog =~ s/$from/$to_string/;
}

###############################

if($extended){
	# convert al $$ to single $, $$ are used to skip te standard bawk parsing, as in $$NF
	$awk_prog =~ s/\$\$/\$/g;
}

$data_filename = "" if not defined $data_filename;
$data_filename = "<(zcat $data_filename)" if $data_filename =~ /.gz$/;
my $assigend_vars = '';
for(@assign){
	$assigend_vars .= " -v $_"
}
my $cmd ="export LC_ALL=POSIX; gawk $assigend_vars -F'\\t' -v OFS='\\t' --re-interval '$awk_prog' $data_filename";
if($debug){
	print "$cmd\n";
	exit(0);
}else{
	exec '/bin/bash', '-c',$cmd;
}

sub find_metadata{
	my $makefile=shift;
	my $data_filename_only=shift;
	###############################
	#   search for metadata
	###############################
	
	#print "found" if $makefile =~ /^.META:\s+$data_filename_only(.*?)\n^$/sm;
	#print $1;
	#whit the following multiline regex we wont to keep all the META block
	my $meta = undef;
	if($makefile =~ /^.META:[^\n]*\s$data_filename_only(?:[ \t][^\n]*)?\n(.*?)\n^$/sm){ #? is for lazy match
		$meta = $1;
	}else{# if a .META for te $data_filename_only is not found search for .META with * glob
		my @all_META = ($makefile =~ /^.META:\s+([^\n]+)\n(.*?)\n^$/gsm); # get all .META, header and body in subcessive elements
		OUTER: for(my $i=0; $i<scalar(@all_META); $i++){
			my $patterns = $all_META[$i];
			if($patterns !~ m/^\s/){ # if the pattern start whit a space is the body of a .META, whe search for header
				my @pattern = split(/\s+/,$patterns);# for rows like .META *file1 *file2
				for(@pattern){
					$_ =~ s/\./\\./g; # the pattern will be interpreted as a regexp, so the "." must be slashed
					$_ =~ s/\*/.*/g; # * -> .*
					if($data_filename_only =~ /^$_$/){
						$meta = $all_META[$i + 1]; # the element subsequent the matched header is the required body
						last OUTER;
					}
				}
			}
		}
		if(not defined($meta)){
			if($debug){
				print $makefile;
			}
			die("metadata not found for file ($data_filename_only) in makefile ($makefile_name)");
		}
	}
	return $meta;
}

sub get_makefile_content{
	my $makefile_name = shift;
	#configure and use the MakefileParser module to read the makefile
	my $fh;
	open $fh, $makefile_name or die("Can't open $makefile_name");
	set_makefile_name($makefile_name);
	my $makefile = readmakefile($fh);
	close $fh;
	return $makefile;
}
