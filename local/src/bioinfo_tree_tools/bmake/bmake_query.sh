#!/bin/bash

if [[ $# -ne 1 ]]; then
	echo "Unexpected argument number." >&2
	exit 1
fi

export BIOINFO_PARALLEL=no
exec bmake --no-print-directory --silent echo.$1
