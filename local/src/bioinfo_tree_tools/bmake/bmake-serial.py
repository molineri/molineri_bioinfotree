#!/usr/bin/env python
#
# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

from __future__ import with_statement
from errno import ENOENT
from fcntl import lockf, LOCK_EX, LOCK_UN
from os import environ, stat, fstat, unlink
from os.path import join
from signal import signal, SIGTERM, SIGQUIT, SIGINT
from subprocess import Popen
from sys import argv
from sys import exit as system_exit
from vfork.util import exit

class SerialLock(object):
	def __init__(self, host, idx):
		self.filename = join('.bmake', host, 'serial-%s.lock' % idx)
		self.fd = None
	
	def __enter__(self):
		while True:
			self.fd = file(self.filename, 'w')
			lockf(self.fd, LOCK_EX)
			
			try:
				fs_ino = stat(self.filename).st_ino
			except OSError, e:
				if e.errno == ENOENT:
					fs_ino = -1
				else:
					raise
			
			if fs_ino != fstat(self.fd.fileno()).st_ino:
				lockf(self.fd, LOCK_UN)
				self.fd.close()
			else:
				break
	
	def __exit__(self, type, value, traceback):
		unlink(self.filename)
		lockf(self.fd, LOCK_UN)
		self.fd.close()

def main():
	global host
	
	if len(argv) != 5 or argv[3] != '-c':
		exit('Unexpected arguments.')
	
	try:
		host = environ['BIOINFO_HOST']
	except KeyError:
		exit('BIOINFO_HOST is not defined.')
	
	def abort_lock(signum, _):
		system_exit(-signum)
	signal(SIGINT, abort_lock)
	
	with SerialLock(host, argv[2]):
		proc = Popen([ argv[1], '-c', argv[4] ], shell=False)
		
		def killer(signum, _):
			proc.terminate()
			system_exit(proc.wait())
			system_exit(-signum)
		
		signal(SIGTERM, killer)
		signal(SIGQUIT, killer)
		signal(SIGINT,  killer)
		system_exit(proc.wait())

if __name__ == '__main__':
	main()
