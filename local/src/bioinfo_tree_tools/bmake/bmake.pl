#!/usr/bin/perl
use warnings;
use strict;
use lib $ENV{'BIOINFO_ROOT'}.'/local/lib/perl';
use MakefileParser;
use Getopt::Long;

Getopt::Long::Configure('pass_through');

$SIG{__WARN__} = sub {my @loc = caller(1); CORE::die "WARNING generated at line $loc[2] in $loc[1]: ", @_};

my $usage = "$0 [--debug] [-f makefile] make_options_and_targhets\n";

my $debug = 0;
my $print_directory = 0;
my $makefile_name = "makefile";
my $help = 0;
GetOptions (
	'debug' => \$debug,
	'w' => \$print_directory,
	'f=s' => \$makefile_name,
	'h|help' => \$help
);

if($help){
	print $usage;
	exit;
}

#die("BIOINFO_ROOT enviromental variable not defined") if !defined($ENV{'BIOINFO_ROOT'});

#my $filename = $ENV{'BIOINFO_ROOT'}.'/local/src/bioinfo_tree_tools/bmake/bmake.patterns';
#open PATTERNS, $filename or die("Can't open file $filename");


my %patterns=();
#while(<PATTERNS>){
#	chomp;
#	my ($pattern,$subst) = split(/\t/);
#	$subst = "" if $subst eq "NULL";
#	$patterns{$pattern} = $subst;
#	print "$pattern\n";
#}

#$patterns{'\$\^(\d+)'}='\$(word $1,\$^)';


my $fh;
open $fh, $makefile_name or die("Can't open $makefile_name");
set_makefile_name($makefile_name);
my $makefile = readmakefile($fh);
close $fh;

$makefile =~ s/^require:\s+bmake$//mg;
$makefile =~ s/\$\^(\d+)/\$(word $1,\$^)/gx;
$makefile =~ s/\s\s\s+\*.*\*$//mg;
$makefile =~ s/^.META:.*?\n^$//msg;#? is for lazy match
$makefile =~ s/^.DOC:.*?\n^$//msg;#? is for lazy match

if($debug){
	print $makefile;
	exit 0;
}else{
	if ($print_directory ne 0) {
		$print_directory = "-w";
	} else {
		$print_directory = "";
	}
	my $cmd = "make $print_directory -f - " . join(' ',@ARGV);
	#print "$cmd\n"; 
	open MK,"|$cmd" or die("Error running command ($cmd)");
	$| = 1;
	print MK $makefile;
	close(MK);
	exit $? >> 8;
}


#sub readmakefile
#{
#	my $out='';
#	my $fh=shift or die("readmakefile require a filename param");
#	while(<$fh>){
#		if( m/^include\s+([^\s]+)/){
#			my $path = $1;
#			$path =~ s/\$\(BIOINFO_ROOT\)/$ENV{'BIOINFO_ROOT'}/;
#			$path =~ s/\$\(BIOINFO_HOST\)/$ENV{'BIOINFO_HOST'}/;
#			my $fh2;
#			if(!-e $path){
#				my $dir = '.';
#				my $targhet = $path;
#				if($path =~ m|^(.*)/([^/]+)$|){
#					$dir = $1;
#					$targhet = $2;
#				}
#				my $cmd = "cd $dir; make -f $makefile_name $targhet";
#				print "$cmd\n";
#				print `$cmd`;
#			}
#			open $fh2,$path or die("Can't open $path");
#			$out.=readmakefile($fh2);
#			close $fh2;
#		}else{
#			$out.=$_;
#		}
#	}
#	return $out;
#}
