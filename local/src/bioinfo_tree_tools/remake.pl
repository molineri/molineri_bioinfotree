#!/usr/bin/perl
use warnings;
use strict;
use POSIX;
use Getopt::Long;
use File::Temp qw/ tempfile/;
use lib $ENV{'BIOINFO_ROOT'}.'/local/lib/perl';
use BioinfoUtils;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] [-n|dry_run] [-f|flags 'FLAGS'] file1 file2 ...\n
remake the files file1 file2 and other file passed as argument.
If a newly produced file is identical to previous file remake mantain the date 
of the previous file in order to not let make tink that the downstream pipeline 
is to be updated.

With the -n flag shows the command to produce the files indicated as arguments 
in a dry run fashion even if the files are already present.

The FLAGS are directly passed to (b)make called internally.
";

my $help=0;
my $dry_run=0;
my $required_flags="";
GetOptions (
	'h|help' => \$help,
	'n|dry_run' => \$dry_run,
	'f|flags=s' => \$required_flags,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

for(@ARGV){
	my $tmp_file = undef;
	if(-e $_){
		(my $fh, $tmp_file) = tempfile(DIR=>'.');
		if( m/\.gz$/){
			unlink($tmp_file);
			$tmp_file.=".gz" if m/\.gz$/;
		}
		rename($_, $tmp_file) or warn "Couldn't rename $_ to $tmp_file: $!\n";
	}

	my $flags=$required_flags;
	$flags .= " -n" if $dry_run;
	my $bmake_errors = &cmd_exec_system("bmake $flags '$_'");

	if($dry_run){
		rename($tmp_file,$_) or  warn "Couldn't rename $tmp_file to $_: $!\n";
	}
	
	if(defined($tmp_file)){ # i.e. the file $_ exists
		if( not $bmake_errors){
			my $differ = `zcmp $_ $tmp_file`;
			if(!$? and $differ eq ""){
				rename($tmp_file,$_) or  warn "Couldn't rename $tmp_file to $_: $!\n";
				print STDERR "[remake] remaked file identical to previous file";
			}else{
				#print STDERR "differ: $differ\n";
				unlink($tmp_file);
			}
		}else{
			rename($tmp_file,$_) or  warn "Couldn't rename $tmp_file to $_: $!\n";
			print STDERR "[remake] remake failed, revert to previous file";
		}
	}
}
