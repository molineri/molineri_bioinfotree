#!/usr/bin/env python
#
# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>

from optparse import OptionParser
from os import _exit, execvp, fork, waitpid
from vfork.util import exit, format_usage
import sys


def main():
    parser = OptionParser(usage=format_usage('''
	%prog [OPTIONS] -- CMD ARGS

	Runs CMD and ignore any error code it returns.

	This program is used to hide to the shell the
	actual exit code of CMD.
    '''))
    options, args = parser.parse_args()
    if len(args) == 0:
        exit('Nothing to do.')

    pid = fork()
    if pid == 0:
        execvp(args[0], args)
        _exit(1)
    else:
        waitpid(pid, 0)
        sys.exit(0)


if __name__ == '__main__':
    main()
