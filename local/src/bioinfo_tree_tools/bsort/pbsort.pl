#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
use POSIX qw/ mkfifo /;
use File::Temp qw/ tempfile /;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 -j N -S x% [-o \"sort options\"] IN_FILE\n\tthe -j option is required; the -S option is required and is mandatory to express them in %";

my $help=0;
my $pprocess_n=undef;
my $mem_perc=undef;
my $sort_options="";
GetOptions (
	'h|help' => \$help,
	'j|pprocess_n=i' => \$pprocess_n,
	'S=s' => \$mem_perc,
	'o|sort_options=s'=>\$sort_options,
) or die($usage);

if($help or not defined($pprocess_n) or not $mem_perc=~/(\d+)%/){
	print $usage;
	exit(0);
}

die("-o options must not have -S") if length($sort_options) and $sort_options=~/-S/;

$mem_perc = int($1 / ($pprocess_n + 1) );
if($mem_perc < 1){
	$mem_perc = "";
}else{
	$mem_perc = "-S$mem_perc%";
}

my $in_file = pop(@ARGV);
$in_file = "/dev/stdin" unless $in_file;

my @fifo;

for(my $i=0; $i<$pprocess_n; $i++){
	my (undef, $filename) = tempfile(OPEN => 0);
	my $cmd="mkfifo $filename";
	#print STDERR $cmd;
	system_bash($cmd);
	push @fifo,$filename;
}

my $cmd = "interleaver ". join(" ",@fifo)." < $in_file &";
#print STDERR $cmd;
system_bash($cmd);


$cmd = "bsort $sort_options $mem_perc -m ";
for(@fifo){
	$cmd.="<(bsort $sort_options $mem_perc $_) ";
}

#print STDERR $cmd;
system_bash($cmd);

for(@fifo){
	unlink($_);
}


sub system_bash {
	my @args = ( "bash", "-c", shift );
	system(@args) == 0 or die "system @args failed: $?";

}
