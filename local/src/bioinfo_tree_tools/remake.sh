#!/bin/bash

#if [[ $# -ne 1 ]]; then
#	echo "Unexpected argument number." >&2
#	exit 1
#fi

for i in $@; do
	if [ -e $i ]; then 
		tmp_file=`mktemp -p .`; 
		mv -f "$i" "$tmp_file"
	fi;
	bmake "$i";
	if [ "$?" != "0" ]; then
		#echo "[ERROR] make returned error at file $i";
		exit $?;
	fi;
	if [ $tmp_file ]; then
		if cmp -s "$i" $tmp_file; then
			mv "$tmp_file" "$i";
			echo "[remake] remaked file identical to previous file" >&2;
		else
			rm -f "$tmp_file";
		fi;
	fi;
done
