#!/bin/bash

if [[ $# -ne 1 ]]; then
	echo "Unexpected argument number." >&2
	exit 1
elif [[ $1 == -h ]] || [[ $1 == --help ]]; then
	echo "Usage: bmatlab MATLAB_SCRIPT" >&2
	exit 1
fi

unhead $1 \
| matlab -nodisplay -nojvm -nosplash
