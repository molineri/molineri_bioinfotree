#!/usr/bin/env python
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from glob import glob
from optparse import OptionParser
from os import environ, getcwd, readlink, sep
from os.path import abspath, dirname, isdir, islink, join, normpath
from sys import exit

from sys import stderr

def load_env():
	try:
		root = environ['BIOINFO_ROOT']
	except KeyError:
		exit('BIOINFO_ROOT is not defined.')
	else:
		root = abspath(normpath(root))
	
	try:
		host = environ['BIOINFO_HOST']
	except KeyError:
		exit('BIOINFO_HOST is not defined.')
	
	return root, host

class PathScanner(object):
	def __init__(self, base, root):
		self.start_points = [ base ]
		self.pos = base
		self.root = root

def is_subdir(path, root):
	root += '/'
	return path[:len(root)] == root

def select_children(root, paths):
	c = []
	o = []
	for p in paths:
		if is_subdir(p, root):
			c.append(p)
		else:
			o.append(p)
	return c, o

def list_paths(base, other_paths, root):
	def helper(base, other_paths, root, res):
		pos = base
		while pos == root or is_subdir(pos, root):
			children, other_paths = select_children(pos, other_paths)
			if len(children):
				children.sort(key=lambda x: len(x), reverse=True)
				helper(children[0], children[1:], pos, res)
			
			if pos != root:
				res.append(pos)
			pos = dirname(pos)
	
	res = []
	helper(base, other_paths, root, res)
	res.append(root)
	return res

def list_rule_paths(path, root):
	paths = set()
	for rule_file in glob(join(path, '*.mk')):
		while islink(rule_file):
			rule_file = normpath(join(dirname(rule_file), readlink(rule_file)))
		
		d = dirname(rule_file)
		if is_subdir(d, root):
			paths.add(d)
	
	return list(paths)

def collect_paths(base, host, root):
	dirnames = ('bin', 'lib', 'lib/python', 'man')
	varnames = ('path', 'library_path', 'python_path', 'man_path')
	paths = [ list() for i in dirnames ]
	
	for path in list_paths(base, list_rule_paths(base, root), root):
		bin_base = join(root, 'binary', host, path[len(root)+1:], 'local')
		for i, d in enumerate(dirnames):
			p = join(bin_base, d)
			if isdir(p):
				paths[i].append(p)
		
		base = join(path, 'local')
		for i, d in enumerate(dirnames):
			p = join(base, d)
			if isdir(p):
				paths[i].append(p)
	
	return dict(zip(varnames, paths))

def main():
	parser = OptionParser(usage='%prog >PATH_DEFS')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	root, host = load_env()
	cwd = getcwd()
	
	if cwd != root and not is_subdir(cwd, root):
		exit('Can\'t work outside BIOINFO_ROOT.')
	
	for n, ps in collect_paths(cwd, host, root).iteritems():
		print 'export MAKE_%s:=%s' % (n.upper(), ':'.join(ps))

if __name__ == '__main__':
	main()
