#!/usr/bin/env python
#
# Copyright 2008-2009 Gabriele Sales <gbrsales@gmail.com>

from glob import glob
from optparse import OptionParser
from os import environ, getcwd, readlink, sep, stat
from os.path import abspath, dirname, exists, isdir, islink, join, normpath
from sys import exit, stdin
from time import time
import errno

def load_env():
	try:
		root = environ['BIOINFO_ROOT']
	except KeyError:
		exit('BIOINFO_ROOT is not defined.')
	else:
		root = abspath(normpath(root))
	
	try:
		host = environ['BIOINFO_HOST']
	except KeyError:
		exit('BIOINFO_HOST is not defined.')
	
	start_time = environ.get('BMAKE_START_TIME', time())
	
	return root, host, start_time

class MalformedInfo(Exception): pass

def read_info(filename):
	res = dict((p,[]) for p in 'MIC')

	try:
		fd = file(filename, 'r')
	except IOError, e:
		if e.errno == errno.ENOENT:
			return res
		else:
			raise
	
	try:
		for line in fd:
			line = line.rstrip('\n')
			if len(line) < 3 or line[1] != '|': raise MalformedInfo
			try:
				res[line[0]].append(line[2:])
			except KeyError:
				raise MalformedInfo
	finally:
		fd.close()

	return res

def qualify_module(module, root):
	parts = module.split('/')
	varname = '__BMAKE_IMPORT_%s' % '_'.join(parts)
	path = join('/'.join(parts[:-1]), 'local', 'share', 'rules', '%s.mk' % parts[-1])
	return varname, path

class PathScanner(object):
	def __init__(self, base, root):
		self.start_points = [ base ]
		self.pos = base
		self.root = root

def is_subdir(path, root):
	root += '/'
	return path[:len(root)] == root

def select_children(root, paths):
	c = []
	o = []
	for p in paths:
		if is_subdir(p, root):
			c.append(p)
		else:
			o.append(p)
	return c, o

def list_paths(base, other_paths, root):
	def helper(base, other_paths, root, res):
		pos = base
		while pos == root or is_subdir(pos, root):
			children, other_paths = select_children(pos, other_paths)
			if len(children):
				children.sort(key=lambda x: len(x), reverse=True)
				helper(children[0], children[1:], pos, res)
			
			if pos != root:
				res.append(pos)
			pos = dirname(pos)
	
	res = []
	helper(base, other_paths, root, res)
	res.append(root)
	return res

def collect_paths(base, module_dirs, root, host):
	dirnames = ('bin', 'lib', 'lib/python', 'man')
	varnames = ('path', 'library_path', 'python_path', 'man_path')
	prefix_len = len(root) + 1
	paths = [ list() for i in dirnames ]
	task_root = None
	instance_root = None
	
	for path in list_paths(base, module_dirs, root):
		bin_base = join(root, 'binary', host, path[prefix_len:], 'local')
		for i, d in enumerate(dirnames):
			if isdir(join(bin_base, d)):
				paths[i].append(join('$(BIOINFO_ROOT)', 'binary', '$(BIOINFO_HOST)', path[prefix_len:], 'local', d))

		if instance_root is None and exists(join(path, 'makefile')) and not exists(join(path, '..', 'makefile')):
			instance_root = join('$(BIOINFO_ROOT)', path[prefix_len:])
		
		base = join(path, 'local')
		if task_root is None and isdir(base):
			task_root = join('$(BIOINFO_ROOT)', path[prefix_len:])

		for i, d in enumerate(dirnames):
			if isdir(join(base, d)):
				paths[i].append(join('$(BIOINFO_ROOT)', base[prefix_len:], d))
	
	if task_root is None:
		exit('Cannot find task root.')

	return task_root, instance_root, dict(zip(varnames, paths))

def main():
	parser = OptionParser(usage='%prog [OPTIONS] INFO >PATH_DEFS')
	parser.add_option('-f', '--fingerprints', dest='fprints', help='store module and include fingerprints into FILE', metavar='FILE')
	options, args = parser.parse_args()
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	root, host, start_time = load_env()
	cwd = getcwd()
	if cwd != root and not is_subdir(cwd, root):
		exit("Can't work outside BIOINFO_ROOT.")

	try:
		info = read_info(args[0])
	except MalformedInfo:
		exit('malformed dynpaths information file.\nRemove the .bmake directory and rerun bmake.')
	
	fprints_fd = file(options.fprints, 'w') if options.fprints is not None else None
	
	print '__BMAKE_DYNPATHS:=1'

	module_dirs = set()
	for module in info['M']:
		varname, path = qualify_module(module, root)
		module_dirs.add(dirname(join(root, path)))

		print '%s:=$(BIOINFO_ROOT)/%s' % (varname, path)
		if fprints_fd:
			finfo = stat(join(root, path))
			print >>fprints_fd, 'M|%s|%s|%d|%d' % (module.replace('/', '.'), path, finfo.st_ino, min(finfo.st_mtime, start_time))

	if fprints_fd:
		finfo = stat('rules.mk')
		print >>fprints_fd, 'M|%s|%s|%d|%d' % ('rules', join(cwd[len(root)+1:], 'rules.mk'), finfo.st_ino, min(finfo.st_mtime, start_time))

	for include in info['I']:
		print '__BMAKE_INCLUDE_%s:=%s' % (include.replace('.', '_'), include)
		if fprints_fd:
			finfo = stat(include)
			print >>fprints_fd, 'I|%s|%d|%d' % (include, finfo.st_ino, min(finfo.st_mtime, start_time))
	
	if fprints_fd:
		fprints_fd.close()

	for context in info['C']:
		module_dirs.add(join(root, context))
		print '__BMAKE_CONTEXT_%s:=yes' % context.replace('/', '_')

	task_root, instance_root, make_paths = collect_paths(cwd, module_dirs, root, host)
	print 'export TASK_ROOT:=%s' % task_root
	print 'export PRJ_ROOT:=%s' % task_root
	if instance_root:
		print 'export INSTANCE_ROOT:=%s' % instance_root
	for n, ps in make_paths.iteritems():
		print 'export MAKE_%s:=%s' % (n.upper(), ':'.join(ps))

if __name__ == '__main__':
	main()
