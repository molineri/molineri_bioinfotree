#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
use Scalar::Util 'looks_like_number'; 
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] [-p PRECISION] [-e|exclude_cols 1 -e|exclude_cols 2 ]\n";

my $help=0;
my $precision=2;
my @exclude_cols=();
GetOptions (
	'h|help' => \$help,
	'p|precision=i' => \$precision,
	'e|exclude_cols=i' =>\@exclude_cols,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

@exclude_cols = map{$_--} @exclude_cols;
my %e=();
for(@exclude_cols){
	$e{$_-1}=1
}

while(<>){
	chomp;
	my @F = split /\t/,$_,-1;
	my $i=-1;
	@F=map{$i++; (!defined $e{$i} and looks_like_number($_)) ? sprintf("%.$precision".'g',$_) : $_ } @F;
	print @F
}

