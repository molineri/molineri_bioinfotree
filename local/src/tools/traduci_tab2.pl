#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;


my $usage = "$0 [-r] vocabolario file
		-r se il vocabolario ha le colonne 1 e 2 invertite";

my $reverse_col = undef;
GetOptions (
	'reverse_col|r' => \$reverse_col,
);

my $filename = shift @ARGV;
die $usage if !$filename;


open FH,$filename or die("Can't open file ($filename)");

my %hash=();
while(<FH>){
	chomp;
	my ($a,$b)=split /\t/;
	if (defined $reverse_col) {
		push @{$hash{$b}},$a;
	} else {
		push @{$hash{$a}},$b;
	}
}

my @k=keys(%hash);

while(<>){
	while(s/([^\t]+)([\t\n]+)//){
		print $1;
		if (defined $hash{$1}) {
			my @a = @{$hash{$1}};
			print "\t",join (",",@a);
		} 
		print $2;
	}
}
