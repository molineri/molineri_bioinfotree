#!/bin/bash

echo "WARING: the conversion is very rough!">&2

sed 's/),(/\n/g; s/VALUES (/\n/g; s/INSERT INTO />/' \
| tr "," "\t"\
| perl -lne '$p=1 if m/^>/; print if $p' \
| fasta2tab
