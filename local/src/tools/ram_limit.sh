#!/bin/bash
# Argument = PERCENT

usage()
{
cat << EOF
usage: $0 options

This script limit the RAM available to the processes runned by the current shell to the PERCENT% of the total RAM size.

EOF
}

while getopts "h" OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         ?)
             usage
             exit
             ;;
     esac
done

shift $(($OPTIND - 1))
if [ -z $1 ]
then
	echo "ERROR $0: Wrong argument number"
	usage
	exit 1
fi

ulimit -v `free -k | unhead | awk -v P=$1 '{printf("%i\n",$2*P)}' | head -n1`
