#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;
use IO::Handle;

$,="\t";
$\="\n";

# JOINS TWO MORE FILES ON A COLUMN_ID

my @id_col = (1,1);
my $intersection = 0;
my $id_uniq = 0;
my $only_in_1 = undef;
my $only_in_2 = undef;
my $numeric   = undef;
my $empty     = 0;
my $assume_sorted = 0;
my $s1 = undef;
my $s2= undef;

my $usage = "cat file_1 | $0 - [-1 column_id_file_1] [-2 column_id_file_2] [-i] [-u] [-n] [-e] [-s] [-s1] [-s2] -- file_2
	-i displays only common lines
	-u to not repeat id colum
	-n if column_ids are numerically sorted
	-e doesn't die if one file is empty
	-s to assume that files are ordered (doesn't check if they are sorted)
	-s1 if file_1 isn't sorted
	-s2 if file_2 isn't sorted\n";

die ($usage) if (scalar @ARGV == 0);

GetOptions (
	'id_col_1|id_c_1|1=i'	=> \$id_col[0],
	'id_col_2|id_c_2|2=i'	=> \$id_col[1],
	'id_uniq|u'		=> \$id_uniq,
	'intersect|i'		=> \$intersection,
	'numeric_sort|n'	=> \$numeric,
	'empty|e'		=> \$empty,
	'assume_sorted|s'	=> \$assume_sorted,
	'sort1|s1'              => \$s1,
	'sort2|s2'              => \$s2,
) or die($usage);

#die ('ERROR: id_uniq|u available only with intersection|i') if $id_uniq and !$intersection;
die ("ERROR: assume_sorted|s available only if the 2 files have already been sorted (no options -s1 -s2)") 
	if $assume_sorted and ($s1 or $s2);


#
# 1. inizializzazione
#
#	a. apertura e sorting dei files
#
my $n=0;
my $num_files=2;
my @num_col=();
my @fh = ();
my @last_rows = ();

my $sort1 = "";
my $sort2 = "";
if ($s1) {
	$sort1 = "sort -k$id_col[0]";
	$sort1 .= "n" if $numeric;
}
if ($s2) {
	$sort2 = "sort -k$id_col[1]";
	$sort2 .= "n" if $numeric;
}


my $filename_1 = shift @ARGV;
my $filename_2 = shift @ARGV;
my $fh1;
my $fh2;
&open_files();


#
#	b. controllo struttura files
#
$id_col[0]--;
$id_col[1]--;

for(0,1){
	my $id_col=$id_col[$_];
	my $line = $_ == 0 ? <$fh1> : <$fh2>;
	if(!defined($line)){
		if($empty){
			exit 0;
		}else{
			die("ERROR: empty file in input");
		}
	}
	chomp $line;
	next if !length($line);
	
	my @F=split /\t/,$line;
	die("Error: undefined id") if !defined($F[$id_col[$_]]) or !length($F[$id_col[$_]]);
	$last_rows[$n]=\@F;
	$num_col[$n] = scalar(@F) if !defined $num_col[$n];
	$n++;
}

my @null_1=();
for(1..$num_col[0]){
	push @null_1,'';
}
my @null_2=();
for(1..$num_col[1]){
	push @null_2,'';
}

die ("ERROR: $0: ci devono essere $num_files in input, trovati $n!!\n") if ($n != $num_files);


#
# 2. join
#
my $blok_1_ref=&get_block(0);
my $blok_2_ref=&get_block(1);
my $id1 = $blok_1_ref->[0][$id_col[0]];
my $id2 = $blok_2_ref->[0][$id_col[1]];

while (1) {
	if(
		(defined($id1) and defined($id2))
			and
		($id1 eq $id2)
	){
		foreach my $a (@{$blok_1_ref}){
			foreach my $b (@{$blok_2_ref}){
				my @b_to_print=@{$b};
				if($id_uniq){
					splice @b_to_print, $id_col[1], 1;
				}
				print @{$a}, @b_to_print;
			}
		}
		
		$blok_1_ref=&get_block(0);
		$blok_2_ref=&get_block(1);
		$id1 = defined($blok_1_ref) ? $blok_1_ref->[0][$id_col[0]] : undef;
		$id2 = defined($blok_2_ref) ? $blok_2_ref->[0][$id_col[1]] : undef;

	}elsif(
		defined($id1)
			and
		( 
			!defined($id2)
				or 
			(cfr_id($id2,$id1,$numeric))
		)
	){#avanzo sull'1
		if (!$intersection) {
			foreach my $a (@{$blok_1_ref}){
				print @{$a}, @null_2;
			}
		}
		
		$blok_1_ref=&get_block(0);
		$id1 = defined($blok_1_ref) ? $blok_1_ref->[0][$id_col[0]] : undef;
	}else{
		last if !defined($id1) and !defined($id2);

		if (!$intersection) {
			foreach my $b (@{$blok_2_ref}){
				print @null_1, @{$b};
			}
		}
		$blok_2_ref=&get_block(1);
		$id2 = defined($blok_2_ref) ? $blok_2_ref->[0][$id_col[1]] : undef;
	}
}


###################################################################################################################

sub get_block
{
	my $file_idx = shift;
	
	my $file_handle=$fh[$file_idx];
	my $id_column=$id_col[$file_idx];
	my $id = $last_rows[$file_idx]->[$id_column];

	my @last_row=();
	if(defined $last_rows[$file_idx]){
		@last_row=@{$last_rows[$file_idx]};
	}else{
		return undef;
	}
	
	my @ret_val=(\@last_row);

	while(1){
		my $line = $file_idx == 0 ? <$fh1> : <$fh2>;

		if (!defined $line) {
			$last_rows[$file_idx]=undef;
			return \@ret_val;
		}
		chomp $line;
		my @F=split /\t/,$line;
		my $curr_id = $F[$id_column];
		die("ERROR: id not defined") if !defined($curr_id) or !length($curr_id);
		die ("ERROR: file ",$file_idx + 1 ," not sorted on column_id ",$id_col[$file_idx] + 1) 
			if (cfr_id($id,$curr_id,$numeric) and !$assume_sorted);

		if ($curr_id eq $id) {
			push @ret_val,\@F;
		}else{
			$last_rows[$file_idx]=\@F;
			return \@ret_val;
		}
	}
}



sub cfr_id
# returns 1 if the first id is greater than the second one; returns 0 otherwise
{
	my $a = shift;
	my $b = shift;
	my $flag = shift;

	if ( defined $flag ) {
		if ($a !~ /^\d+$/ or $b !~ /^\d+$/) {
			die ("ERROR: column_ids must be numeric!\n");
		}
		if ($a > $b) {
			return 1;
		} else {
			return 0;
		}
	} else {
		if ($a gt $b) {
			return 1;
		} else {
			return 0;
		}
	}
}



sub open_files
{
	if($filename_1 eq '-'){
		if ($sort1) {
			my $tmp_file = "/tmp/join4_pl.tmp";
			die ("File $tmp_file already exists!") if -e $tmp_file;
			open $fh1, ">$tmp_file" or die ("Can't write on temporary file $tmp_file\n");

			while (<>) {
				chomp;
				print $fh1 $_;
			}
			close $fh1;

			open $fh1,"$sort1 $tmp_file |" or die ("Can't open pipe to $sort1 $tmp_file\n");
			`rm -f $tmp_file`;
		} else {
			$fh1=*STDIN;
		}
	}else{
		if($filename_1 =~/\.gz$/){
			if ($sort1) {
				open $fh1, "zcat $filename_1 | $sort1 |" or die("Can't open pipe to zcat ($filename_1) | $sort1\n");
			} else {
				open $fh1, "zcat $filename_1 |" or die("Can't open pipe to zcat ($filename_1)\n");
			}
		}else{
			if ($sort1) {
				open $fh1,"$sort1 $filename_1 |" or die ("Can't pipe to $sort1 ($filename_1)\n");
			} else {
				open $fh1,$filename_1 or die ("Can't open file ($filename_1)\n");
			}
		}
	}
	if($filename_2 eq '-'){
		if ($sort2) {
			my $tmp_file = "/tmp/join4_pl.tmp";
			die ("File $tmp_file already exists!") if -e $tmp_file;
			open $fh2, ">$tmp_file" or die ("Can't write on temporary file $tmp_file");

			while (<>) {
				chomp;
				print $fh2 $_;
			}
			close $fh2;

			open $fh2,"$sort2 $tmp_file |" or die ("Can't open pipe to $sort2 $tmp_file\n");
			`rm -f $tmp_file`;
		} else {
			$fh2=*STDIN;
		}
	}else{
		if($filename_2 =~/\.gz$/){
			if ($sort2) {
				open $fh2, "zcat $filename_2 | $sort2 |" or die("Can't open pipe to zcat ($filename_2) | $sort2\n");
			} else {
				open $fh2, "zcat $filename_2 |" or die("Can't open pipe to zcat ($filename_2)\n");
			}
		}else{
			if ($sort2) {
				open $fh2,"$sort2 $filename_2 |" or die ("Can't pipe to $sort2 ($filename_2)\n");
			} else {
				open $fh2,$filename_2 or die ("Can't open file ($filename_2)\n");
			}
		}
	}
}
