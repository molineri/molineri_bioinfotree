#!/usr/bin/env python

from sys import stdin, stderr, argv
from optparse import OptionParser
from vfork.util import exit, format_usage


def main():
	usage = format_usage('''
		%prog file1 file2 file3 ... < STDIN
			split the input file in te files given as param, 
			does this interleaving block of lines, to improowe performance in pipes
	''')
	parser = OptionParser(usage=usage)
	options, args = parser.parse_args()
	
	if len(args) < 1 :
		exit('Unexpected argument number.')
	
	outputs = []
	numargs = 0
	for arg in argv[1:]:
	  outputs.append(open(arg, 'a'))
	  numargs += 1

	s = "str"
	counter = 0
	p = 0
	while len(s) > 0:
	  s = stdin.read(32 * 1024)
	  p = s.rfind('\n') + 1
	  if p > 0:
	    outputs[counter % numargs].write(s[:p])
	    counter += 1
	    outputs[counter % numargs].write(s[p:])
	  else:
	    outputs[counter % numargs].write(s)
	    counter += 1

if __name__ == '__main__':
	main()

