#!/usr/bin/perl

use warnings;
use strict;
$,="\t";
$\="\n";
use Getopt::Long;

$SIG{__WARN__} = sub {CORE::die @_};

my $usage="$0 [-a] [-g GLUE] [-m MAP_FILE] COL1 COL2 ... COLn < file
	convert the values in the COLs in integers,
	the 1 to 1 map is saved in file MAP_FILE

	in -a mode the program as a different beaviour:
	disambiguate repeated values in the cols adding a count

	-b mode is similar to -a but do not add anitying to the first id encountered
";


my $help=0;
my $append=0;
my $append_but_one=0;
my $glue = "_";
my $map_file = undef;

&GetOptions (
	'h|help' => \$help,
	"append|a" => \$append,
	"append_but_one|b" => \$append_but_one,
	"glue|g=s" => \$glue,
	"map_file|m=s" => \$map_file
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my @cols = map{
	die("column number not valid ($_)") if !m/^\d+$/;
	$_ - 1;
} @ARGV;

@ARGV=();

my %a=();
while(<>){
	chomp;
	my @F=split /\t/;
	foreach my $col (@cols){
		if(!$append and !$append_but_one){
			$a{$F[$col]} = scalar(keys(%a)) if( ! defined($a{$F[$col]}));
			$F[$col] = $a{$F[$col]};
		}else{
			if($append){
				$a{$F[$col]}++;
				$F[$col] .= $glue . $a{$F[$col]};
			}else{#$append_but_one
				$a{$F[$col]}++;
				$F[$col] .= $glue . $a{$F[$col]} if $a{$F[$col]} > 1;
			}
		}
	}
	print @F;
}

if(defined $map_file){
	open FH, ">$map_file" or die("Can't write on file ($map_file)");
	for(keys %a){
		print FH $_,$a{$_};
	}
}
