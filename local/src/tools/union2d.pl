#!/usr/bin/perl -d

use warnings;
use strict;
use constant CHR_COL   => 0;
use constant B_COL     => 1;
use constant E_COL     => 2;
use constant ID_COL    => 3;
#use Getopt::Long;

$,="\t";
$\="\n";


my $usage = "cat chr_b_e_id | $0";



#
# 1. inizializzazione
#
my @union = ();

my $line = <>;
die ("ERROR: empty first line!") if !length($line);
chomp $line;
my @F=split /\t/,$line;
die("Error: undefined chr") if !defined($F[CHR_COL]) or !length($F[CHR_COL]);
die("Error: undefined b")   if !defined($F[B_COL])   or !length($F[B_COL]);
my @last_row = @F;

my $block_ref=&get_block();

while (1) {

	last if (!defined $block_ref);

	my $aux_ref = &initialize_union($block_ref);
	push @union,$aux_ref;

	$block_ref = &get_block();
	
}

my $n = scalar @union;



#
#
#
while (1) {
	my @old_union = @union;
	my $old_n = $n;

	&union();

	$n = scalar @union;

	last if ($old_n == $n);
}


#
#
#
foreach my $el (@union) {
	my @reg = @{$el};
	print @reg;
}






sub get_block
{
	my @last_line=();
	if (scalar @last_row) {
		@last_line=@last_row;
	} else {
		return undef;
	}
	
	my $chr = $last_row[CHR_COL];
	my $b   = $last_row[B_COL];

	my @ret_val=(\@last_line);

	while(1){
		my $line = <>;
		if (!defined $line) {
			undef @last_row;
			return \@ret_val;
		}
		chomp $line;
		my @F=split /\t/,$line;
		my $curr_chr = $F[CHR_COL];
		my $curr_b   = $F[B_COL];

		die ("ERROR: chr not defined") if !defined($curr_chr) or !length($curr_chr);
		die ("ERROR: b not defined") if !defined($curr_b) or !length($curr_b);
#		die ("ERROR: file not sorted on chr_column") if ($chr gt $curr_chr);
		die ("ERROR: file not sorted on b_column") if ($b > $curr_b);

		if ( ($curr_chr eq $chr) and ($curr_b == $b) ) {
			push @ret_val,\@F;
		}else{
			@last_row=@F;
			return \@ret_val;
		}
	}
}


sub union
{
	my @new_union = ();

	for (my $i=0; $i<$n - 1; $i++) {

#		my $b1 = ${$union[$i]}[B_COL];
#		my $e1 = ${$union[$i]}[E_COL];
#		my $ids1 = ${$union[$i]}[ID_COL];
		my $chr = ${$union[$i]}[CHR_COL];
		my $union_b = ${$union[$i]}[B_COL];
		my $union_e = ${$union[$i]}[E_COL];
		my $union_ids = ${$union[$i]}[ID_COL];

		for (my $j=$i + 1; $j<$n; $j++) {
			my $b2 = ${$union[$j]}[B_COL];
			my $e2 = ${$union[$j]}[E_COL];
			my $ids2 = ${$union[$j]}[ID_COL];

			next if ($union_b > $e2);
			next if ($b2 > $union_e);

			$union_b = ($union_b < $b2) ? $union_b : $b2;
			$union_e = ($union_e < $e2) ? $e2 : $union_e;
			$union_ids = $union_ids.','.$ids2;

		}
		my @tmp = ($chr,$union_b,$union_e,$union_ids);

		push @new_union,\@tmp;
	}

	@union = @new_union;
}



sub initialize_union
{
	my $lines_ref = shift;

	my @lines = @{$lines_ref};
	my $chr = ${$lines[0]}[CHR_COL];
	my $union_b = ${$lines[0]}[B_COL];
	my $union_e = undef;
	my @line_ids = ();
	foreach my $row (@lines) {
		my @tmp = @{$row};
		$union_e = $tmp[E_COL] if ( (!defined $union_e) or ($tmp[E_COL] > $union_e) );
		push @line_ids, $tmp[ID_COL];
#		print $chr,$union_b,$union_e,@line_ids;
	}
	my $ids = join ',',@line_ids;

	my @out = ($chr,$union_b,$union_e,$ids);

	return \@out;

}
