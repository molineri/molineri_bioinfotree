#!/usr/bin/env python
#
# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

from collections import defaultdict
from itertools import groupby
from operator import itemgetter
from optparse import OptionParser
from sys import stdin, stderr
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage


def iter_records(fd, col, is_sorted, assume_sorted):
	prev_key = None
	for line_idx, line in enumerate(fd):
		tokens = safe_rstrip(line).split('\t')
		if len(tokens) <= col:
			exit('Insufficient token number at line %d: %s' % (line_idx+1, line))
		
		value = tokens[col]
		del tokens[col]
		key = tuple(tokens)
		
		if is_sorted:
			if not assume_sorted and (prev_key is not None and key < prev_key):
				exit('Disorder found at line %d: %s' % (line_idx+1, line))
			prev_key = key
		
		yield key, value

def group_sorted(records, preserve_order):
	for k, rs in groupby(records, itemgetter(0)):
		vs = [r[1] for r in rs]
		if preserve_order:
			vs = set(vs)
		yield k, vs

def group_unsorted(records, preserve_order):
	if preserve_order:
		rs = defaultdict(list)
		add_record = lambda k,v: rs[k].append(v)
	else:
		rs = defaultdict(set)
		add_record = lambda k,v: rs[k].add(v)
	
	for k, v in records:
		add_record(k,v)
	
	return rs.iteritems()

def main():
	parser = OptionParser(usage=format_usage('''
		%prog COL <STDIN

		Perform the opposite operation of expandsets.
	'''))
	parser.add_option('-c', '--cardinality', dest='cardinality', action='store_true', default=False, help='output set cardinality instead of its elements')
	parser.add_option('-a', '--add', dest='add_cardinality', action='store_true', default=False, help='print both the collapsed values and the cardinality')
	parser.add_option('-s', '--sorted-input', dest='sorted_input', action='store_true', default=False, help='assume the input sorted on all columns except COL')
	parser.add_option('-f', '--assume-sorted', dest='assume_sorted', action='store_true', default=False, help='assume the input sorted on all columns except COL and do not chek for disorder')
	parser.add_option('-o', '--preserve-order', dest='preserve_order', action='store_true', default=False, help='preserve the order (and dupplications) of collapsed element with respect to the input file')
	parser.add_option('-g', '--glue', dest='glue', type=str, default=';', help="glue string (default ';')")

	options, args = parser.parse_args()
	if len(args) != 1:
		exit('Unexpected argument number.')

	try:
		col = int(args[0]) - 1
	except ValueError:
		exit('Invalid column index: %s' % args[0])

	if(options.assume_sorted):
		options.sorted_input = True

	records = iter_records(stdin, col, options.sorted_input, options.assume_sorted)

	if options.sorted_input:
		group_strategy = group_sorted
	else:
		group_strategy = group_unsorted
	groups = group_strategy(records, options.preserve_order)

	for k, vs in groups:
		o = list(k[:col])
		o.append(options.glue.join(vs))
		o += k[col:]
		print '\t'.join(o)


if __name__ == '__main__':
	main()

