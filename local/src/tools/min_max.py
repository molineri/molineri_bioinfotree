#!/usr/bin/env python

from optparse import OptionParser
from sys import exit, stdin
from vfork.io.colreader import Reader
from vfork.io.util import safe_rstrip
import re

integer_rx = re.compile(r'^(\+|-)?\d+$')
float_rx = re.compile(r'^(\+|-)?(\d+\.?\d*|\.\d+)([eE](\+|-)?\d+)?$')

def deduce_format(tokens):
	def analyze(token, column):
		m = integer_rx.match(token)
		if m is not None:
			return 'i'
		
		m = float_rx.match(token)
		if m is not None:
			return 'f'
		
		exit('Column %d has an unknown format: %s' % (column, token))
	
	return tuple(analyze(t, i+1) for i, t in enumerate(tokens))

def parse_first_line(tokens, format):
	res = []
	for idx, (token, format) in enumerate(zip(tokens, format)):
		if format == 'i':
			try:
				res.append(int(token))
			except ValueError:
				exit('Invalid integer value at line 1, column %d.' % (idx+1,))
		elif format == 'f':
			try:
				res.append(float(token))
			except ValueError:
				exit('Invalid float value at line 1, column %d.' % (idx+1,))
		else:
			raise ValueError, 'internal error'
	
	return res

def format2string(format):
	return ','.join('%d%s' % (idx, format) for idx, format in zip(xrange(len(format)), format))

def flat_zip(l1, l2):
	res = []
	for a, b in zip(l1, l2):
		res.append(a)
		res.append(b)
	
	return res

def main():
	parser = OptionParser(usage='%prog <NUMBERS')
	options, args = parser.parse_args()

	if len(args) != 0:
		exit('Unexpected argument number.')
	
	tokens = safe_rstrip(stdin.readline()).split('\t')
	column_number = len(tokens)
	format = deduce_format(tokens)
	
	min_list = parse_first_line(tokens, format)
	max_list = list(min_list)

	reader = Reader(stdin, format2string(format), False)
	while True:
		tokens = reader.readline()
		if tokens is None:
			break

		for idx in xrange(column_number):
			if tokens[idx] < min_list[idx]:
				min_list[idx] = tokens[idx]
			if tokens[idx] > max_list[idx]:
				max_list[idx] = tokens[idx]
	
	print '\t'.join(str(v) for v in flat_zip(min_list, max_list))

if __name__ == '__main__':
	main()
