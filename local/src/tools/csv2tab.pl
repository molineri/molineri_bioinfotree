#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;
use Text::CSV;

$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help]\n
	require # sudo apt-get install libtext-csv-perl
";

my $help=0;
GetOptions (
	'h|help' => \$help,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my $csv = Text::CSV->new();

while (<>) {
	chomp;
	if ($csv->parse($_)) {
	    my @F = $csv->fields();
	    print @F;
	} else {
	    my $err = $csv->error_input;
	    print "Failed to parse line: $err";
	}
}
