#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";


my $usage = "$0 [-i|ignore-missing-columns] [-s|split_separator][-a|append] [-d [-g glue] [-j]] [-m glue] [-v|no_verbose [-e VALUE ]] [-k|kill] [-n|invert-dictionary] [-f|key_field N]DICTIONARY 1 2 3 < tab_file\
	-d	allow duplicated keys in DICTIONARY
	-g	indicate the separator for multiple values of the same key in output
	-m	if the dictionary file has more than 2 columns, the transaltion is multi column and the separator is glue
	-k	kill untranslated rows
	-e	use VALUE as translation when a key si not present in dictionary
	-j	join lyke out (when there duplicated translation for the same key generate multiple rows)
";

my $key_field = 1;
my $ignore_missing_columns=0;
my $append=0;
my $duplicated_keys=0;
my $glue=undef;
my $split_separator="\t";
my $split_each_word=0;
my $kill=0;
my $invert_dictionary=0;
my $no_verbose=0;
my $join_lyke_out=0;
my $empty_key = undef;
my $multi_column_separator = undef;
GetOptions (
	'f|key_field=i' => \$key_field,
	'i|ignore-missing-columns' => \$ignore_missing_columns,
	'n|invert-dictionary' => \$invert_dictionary,
	'a|append' => \$append,
	'd|duplicated_keys' => \$duplicated_keys,
	's|split_separator=s' => \$split_separator,
	'e|empty=s' => \$empty_key,
	'w|split_each_word' => \$split_each_word,
	'g|glue=s' => \$glue,
	'j|join' => \$join_lyke_out,
	'k|kill' => \$kill,
	'v|no_verbose' => \$no_verbose,
	'm|multi_column_separator=s' => \$multi_column_separator
) or die($usage);

$SIG{__WARN__} = sub {die @_};

die("-g option meaningless without -d option") if defined $glue  and !$duplicated_keys;

$glue = ',' if not defined $glue;

$no_verbose = 1 if $kill;

my $filename = shift @ARGV;

open FH,$filename or die("Can't open file ($filename)");

my @columns=@ARGV;

die("no column indication\n$usage") if scalar(@columns) == 0 and !$split_each_word; 

for(@columns){
	die("invalid columns ($_)") if !m/^\d+$/;
	$_--;
}

die("-w option confict with column indication") if($split_each_word and scalar(@columns) > 0);
die("-j option require -d and -a options and conflicts with -w") if $join_lyke_out and ($split_each_word or (!$duplicated_keys or !$append));
die("-w option conflicts with -k") if ($split_each_word and $kill);
die("-e meaningless without -v") if defined($empty_key) and not $no_verbose;
die("-n meaningless with -f") if defined($empty_key) and not $no_verbose;
die("-f require a parameter >=1") if $key_field < 1;

$empty_key = "\t" if defined($empty_key) and $empty_key =~ /^\\t$/;
$key_field--;





my %hash=();
while(<FH>){
	chomp;
	
	my $k = undef;
	my $v = undef;
	if($key_field == 0){
		die("At least 2 columns required in dictionary file") if !m/\t/;
		m/([^\t]+)[\t](.*)/;
		if(!$invert_dictionary){
			$k = $1;
			$v = $2;
		}else{
			$v = $1;
			$k = $2;
		}
		if(defined($multi_column_separator)){
			$v =~ s/\t/$multi_column_separator/g;
		}
	}else{
		my @F = split /\t/;
		$k = splice @F, $key_field, 1;
		my $separator = "\t" if not defined($multi_column_separator);
		$v = join( defined $multi_column_separator ? $multi_column_separator : "\t", @F);
	}

	if(defined $hash{$k}){
		if(!$duplicated_keys){
			die("Duplicated key in dictionary ($k)");
		}else{
			$hash{$k}.=$glue.$v;
		}
	}else{
		$hash{$k}=$v;
	}
}




my $warning=0;

while(<STDIN>){
	if(!$split_each_word){
		chomp;
		my @F = split /$split_separator/;
		my @G=@F;
		my $print=1;
		for(@columns){
			$a=$F[$_];
			if(defined($a)){
				my ($val, $translated)=@{&translate($a)};
				$print = 0 if (!$translated and $kill);
				$F[$_] = $val;
			}else{
				die("column not defined (-i to ignore)") if !$ignore_missing_columns;
				if(!$warning){
					print STDERR "WARNING: $0, column not defined\n";
					$warning=1;
				}
			}
		}
		if($print){
			if(!$join_lyke_out){
				print @F;
				print "\n";
			}else{
				die("only one column allowed when join lyke output enabled") if scalar(@columns) > 1;
				my $c = $columns[0];
				my $a = $G[$c];
				my @val=split($glue,$F[$c]);
				my $first = 1;
				for(@val){
					if($first){
						$F[$c] = $_;
						print @F;
						print "\n";
						$first = 0;
					}else{
						$F[$c] = $a .$split_separator.$_;
						print @F;
						print "\n";
					}
				}
			}
		}
	}else{
		s/^([\W]+)//;
		print $1 if $1;
		while(s/([^\W]+)([\W]+)//){
			my ($val, $translated)=@{&translate($1)};
			print $val;
			print $2;
		}
	}
}

sub translate
{
	my $a=shift;
	my $b=$hash{$a};
	if(!defined($b)){
		if(defined($empty_key)){
			$b=$empty_key;
		}else{
			warn "missing translation for key ($a)\n" if !$no_verbose;
		}
	}
	if(defined($b)){
		if($append){
			$a .= "\t$b";
		}else{
			$a = $b;
		}
	}
	
	my @tmp=($a, defined($b));
	return \@tmp;
}
