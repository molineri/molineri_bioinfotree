#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;

$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] [-p|print]\n\n\t-p same as sort | uniq";

my $help=0;
my $print=0;
GetOptions (
	'h|help' => \$help,
	'p|print' => \$print
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my %a=();

while(<>){
	chomp;
	$a{$_}=1;
}

if(!$print){
	print scalar(keys %a);
}else{
	while (my ($k, $v) = each (%a)) {
		print $k;
	}
}