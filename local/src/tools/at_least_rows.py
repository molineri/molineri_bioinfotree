#!/usr/bin/env python
from optparse import OptionParser
from os import popen
from sys import stdin, stdout
from vfork.util import exit, format_usage

def main():
	usage = format_usage('''
		%prog LINE_NUM [QUOTED_COMMAND] <INPUT
		
		Calls QUOTED_COMMAND only if the input contains at least LINE_NUM lines.
		
		If omitted, QUOTED_COMMAND is assumed to be "cat".
	''')
	parser = OptionParser(usage='N [QUOTED_COMMAND] <INPUT')
	parser.add_option('-e', '--exit', dest='exit', action='store_true', default=False, help='exit with an error if there are not enough lines')
	options, args = parser.parse_args()

	if len(args) < 1 or len(args) > 2:
		exit('Unexpected argument number.')
	
	try:
		n = int(args[0])
		if n < 0:
			raise ValueError
	except ValueError:
		exit('Invalid value for LINE_NUM: %s' % args[0])
	
	cmd = args[1] if len(args) == 2 else None
	
	lines = stdin.readlines()
	if len(lines) >= n:
		if cmd:
			fd = popen(args[1], 'w')
		else:
			fd = stdout
		
		for line in lines:
			fd.write(line)
		
		retcode = fd.close()
		if retcode is not None:
			exit('Unclean command exit with code: %d' % retcode)
	
	elif options.exit:
		exit('Insufficient number of rows.')

if __name__ == '__main__':
	main()
