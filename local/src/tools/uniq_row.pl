#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] < input_file\n
	remove duplicated columns in each singular row,
	columns are reported in shuffled order
";

my $help=0;
my $glue=",";
GetOptions (
	'h|help' => \$help,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

while(<>){
	chomp;
	my @F = split /\t/;
	my %a=();
	for(@F){
		$a{$_} = 1
	}
	print keys %a;
}
