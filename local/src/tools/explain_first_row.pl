#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] [-a|all_rows]\n";

my $help=0;
my $all_rows=0;
GetOptions (
	'h|help' => \$help,
	'a|all_rows' => \$all_rows,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my $row=0;
while(<>){
	chomp;
	my @F = split /\t/,$_,-1;
	my $col=1;
	print ">$row" if $all_rows;
	for(@F){
		print $col,$_;
		$col++
	}

	last if !$all_rows;
	$row++;
}
