#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from multiprocessing import Pool, cpu_count
from math import ceil
#from subprocess import Popen, PIPE
from os import system
from os import remove as file_remove
from tempfile import NamedTemporaryFile
import itertools

def clean(files):
	map(file_remove,[f.name for f in files])
	

def build_command(cmd, filename):
	return cmd.replace('__filename__',str(filename))

def build_command2(cmd, filename1, filename2):
	return cmd.replace('__filename1__',str(filename1)).replace('__filename2__',str(filename2))

def process(cmd):
	try:
		#stderr.write("running '%s'\n" % cmd)
		#proc = Popen([environ.get('SHELL', 'bash'), '-c', cmd], shell=False)
		retcode=system(cmd)
		#if retcode != 0:
		#	exit("'%s' exited with error code %d" % (cmd, retcode))
		return retcode
	except Exception, e:
		file_remove(file)
		return e

def main():
	usage = format_usage('''
		%prog CMD < STDIN

		use __filename__ in CMD
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-j', '--parallel_processes_num', type=int, dest='parallel_processes_num', default=cpu_count(), help='the numbers of parallel processes to run [default: %default (python cpu_count())]', metavar='P')
	parser.add_option('-n', '--rows', type=int, dest='rows', default=None, help='rows in the stdin [default: %default (users must specify this option)]', metavar='P')
	parser.add_option('-g', '--groups', type=str, dest='groups', default=None, help='split the input file using groups, G take te form N:M where N is the column of the group id and M is the number of groups in the stdin [default: %default ]', metavar='G')
	parser.add_option('-c', '--combinations', action="store_true", dest='combinations', default=False, help='pass to each process 2 file, all the combinations of files are generated [default: %default]')
	parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='some help [default: %default]')
	parser.add_option('-o', '--file_only', dest='file_only', action='store_true', default=False, help='generate the slice files only and output the temporary file names [default: %default]')
	#parser.add_option('-p', '--dump-params', dest='params_file', help='some help FILE [default: %default]', metavar='FILE')

	options, args = parser.parse_args()
	
	if not options.file_only and len(args) != 1:
		exit('Unexpected argument number.')
	
	
	
	if options.file_only and len(args) != 0:
		exit('Unexpected argument number (no argument expected: -o option).')

	cmd_template = None
	if not options.file_only:
		cmd_template = args[0]
	
	if options.file_only and options.combinations:
		exit('-o and -c are incompatibles')
	
	
	if (options.rows is None and options.groups is None) or (options.rows is not None and options.groups is not None):
		exit('Users must specify --rows or --groups option, not both.')

	group_num = group_col = None
	if options.groups:
		try:
			group_col, group_num = options.groups.split(":")
			group_col = int(group_col) -1
			group_num = int(group_num)
		except Exception, e:
			exit("Malformed value (%s) for --group option: %s" % (options.groups, e))
	
	if options.verbose: print >>stderr, "Generating files.";

	files = []
	tmp_file = NamedTemporaryFile(delete=False)
	files.append(tmp_file)
	if options.rows:
		line_n = 0
		for line in stdin:
			if line_n >= (float(options.rows) / options.parallel_processes_num):
				tmp_file.close()
				tmp_file = NamedTemporaryFile(delete=False)
				files.append(tmp_file)
				line_n = 0
			line_n+=1
			tmp_file.write(line)
		tmp_file.close()
	else:
		prev_group_id = None
		group_count = 0
		group_per_slice = ceil(float(group_num) / options.parallel_processes_num)
		for line in stdin:
			tokens = line.split()
			group_id = tokens[group_col]
			if group_id < prev_group_id:
				exit("Lexicografic disorder found in stdin, column %d." % (group_col + 1) )
			if group_id != prev_group_id:
				group_count += 1
				prev_group_id = group_id
				if group_count > group_per_slice:
					tmp_file.close()
					tmp_file = NamedTemporaryFile(delete=False)
					files.append(tmp_file)
					group_count = 1 # in the first run prev_group_id is None, then the first group is numbered 1
			tmp_file.write(line)
		tmp_file.close()


	if not options.file_only:
		if options.verbose: print >>stderr, "Generating commands.";
		cmds=[]
		if options.combinations is False:
			for f in files:
				cmd = build_command(cmd_template, f.name)
				cmds.append(cmd)
		else:
			for f1,f2 in itertools.product(files, files):
				cmd = build_command2(cmd_template, f1.name, f2.name)
				cmds.append(cmd)

		if options.verbose: print >>stderr, cmds

		pool = Pool(processes=options.parallel_processes_num)
		for res in pool.imap(process, cmds):
			if isinstance(res, Exception):
				print >>stderr, e.args[0]
			elif res!=0:
				clean(files)
				exit("Child process returned %d" % res)

		if options.verbose: print >>stderr, 'cleaning tmp files'
		clean(files)
	else:
		print " ".join(f.name for f in files)

if __name__ == '__main__':
	main()
