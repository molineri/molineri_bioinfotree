#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] [-g glue]]\n";

my $help=0;
my $glue=",";
GetOptions (
	'h|help' => \$help,
	'g|glue=s' => \$glue,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my $header = <>;
my @header = split /\t/,$header,-1;
my $NF = scalar(@header);

while(<>){
	chomp;
	my @F = split /\t/,$_,-1;
	print ">$.";
	for(my $i=0; $i<$NF; $i++){
		print $header[$i],$F[$i]
	}
}
