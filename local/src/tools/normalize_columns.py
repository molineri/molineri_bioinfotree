#!/usr/bin/env python
from __future__ import with_statement, division

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from math import sqrt
#from subprocess import Popen, PIPE
#from collections import defaultdict

def main():
	usage = format_usage('''
		%prog < STDIN
		normalizze the columns of the input file so that the sum (the stdev, in -s) of each column is 1.
		If you see strange value in output please read http://docs.python.org/tutorial/floatingpoint.html
		before to report a bug.
	''')
	parser = OptionParser(usage=usage)
	
	#parser.add_option('-c', '--cutoff', type=float, dest='cutoff', default=0.05, help='some help CUTOFF', metavar='CUTOFF')
	#parser.add_option('-p', '--dump-params', dest='params_file', help='some help FILE', metavar='FILE')
	parser.add_option('-H', '--header_col', dest='header_col', action='store_true', default=False, help='do not normalize the first column')
	parser.add_option('-s', '--stdev', dest='stdev', action='store_true', default=False, help='normalize upon stdev instead of sum')
	parser.add_option('-m', '--mean',  dest='mean',  action='store_true', default=False, help='subtract the column mean at each value of the column')

	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')

	prev_tokens = -1;
	cols=[]
	header_col=False
	if options.header_col:
		header_col=[]

	for line in stdin:
		tokens = safe_rstrip(line).split('\t')
		row = []
		if prev_tokens == -1:
			prev_tokens = len(tokens)
		else:
			if len(tokens) != prev_tokens:
				exit("Malformed input (%d\t%d)" % (prev_tokens,len(tokens)));

		if options.header_col:
			header_col.append(tokens.pop(0))

		try:
			row = [int(i) for i in tokens]
		except ValueError:
			row = [float(i) for i in tokens]
		
		for (i,v) in enumerate(row):
			try:
				cols[i].append(v)
			except IndexError:
				cols.append([v]) 
	
	n = len(cols[0])
	sums  = [ sum(c) for c in cols]
	means = [ v/n    for v in sums]
	
	if options.mean:
		for (i,c) in enumerate(cols):
			cols[i] = [ v - means[i] for v in c]
		means = [0]*len(cols)
	
	if not options.stdev:
		print_cols(cols,sums,header_col)
	else:
		stdevs = [0]*len(means)
		for (i,c) in enumerate(cols):
			stdevs[i] = sqrt( sum( ( (v - means[i])**2 for v in c ) ) / (n - 1) )
		print_cols(cols,stdevs,header_col)

def print_cols(cols, norm, header_col):
		for i in range(0,len(cols[0])):
			row = []
			if header_col:
				row.append(header_col.pop(0))
			for j in range(0,len(cols)):
				row.append( str(cols[j][i]/norm[j]) )
			print '\t'.join(row)


if __name__ == '__main__':
	main()

