#!/usr/bin/env python

from optparse import OptionParser
from subprocess import Popen, PIPE
from sys import exit, stdin, stderr

sets = []

def loadsets(fd):
	while True:
		token = fd.readline()
		if token == "":
			break
		
		tokens = token.split()
		
		i = 0	

		while i<len(tokens):
			if len(sets) < len(tokens):
				sets.append([])
			sets[i].append(tokens[i]) 
			i = i+1


def duplicate(k,j,ls):
	D = len(ls)

	i=0
	while i<len(sets):
		if i != k:
			item = sets[i][j]
			n = 0	
			while n < D-1:
				sets[i].insert(j, item)
				n = n+1
		else:
			n = 0
			sets[i][j] = ls[n]
			n = n + 1
			while n < D:
				sets[i].insert(j, ls[n])
				n = n+1
		i = i+1


def expandsets(separator):
	global sets
	
	i = 0
	while i<len(sets):
		j = 0
		while j < len(sets[i]):
			ls = sets[i][j].split(separator)
			if len(ls) > 1 :
				duplicate(i,j,ls)
			j=j+1
		i = i+1

def main():
	parser = OptionParser(usage='%prog  < FILESETS')
#	parser.add_option('-c', '--columns', type=int, dest='column'
	parser.add_option('-s', '--separator', type=str, dest='separator', default=";", help='indicate the separator for multiple values of the same key in output', metavar='SEPARATOR')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	loadsets(stdin)
	expandsets(options.separator)
	
	if len(sets) == 0:
		exit()

	for j in xrange(len(sets[0])):
		for i in xrange(len(sets)-1):
			print '%s\t' % (sets[i][j]),
			i =i+1
		print '%s' % (sets[i][j])
		j = j+1

if __name__ == '__main__':
	main()
