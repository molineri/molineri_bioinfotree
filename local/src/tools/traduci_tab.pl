#!/usr/bin/perl
use warnings;
use strict;

my $filename = shift @ARGV;

open FH,$filename or die("Can't open file ($filename)");

my %hash=();
while(<FH>){
	chomp;
	my ($a,$b)=split /\t/;
	$hash{$a}=$b;
}

my @k=keys(%hash);

while(<>){
	while(s/([^\t]+)([\t\n]+)//){
		my $a=$hash{$1};
		#print STDERR "$1\t$a\n" if defined($a);
		print defined($a) ? "$1\t$a" : $1;
		print $2;
	}
}
