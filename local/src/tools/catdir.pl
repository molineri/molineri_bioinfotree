#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 dirname\n
same of 'cat dirname/*' but as no limits in the number of files in the directory";

my $help=0;
GetOptions (
	'h|help' => \$help,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my $some_dir = shift;

opendir(DIR, $some_dir) || die "can't opendir $some_dir: $!";
my @files = grep { !m/^\./ && -f "$some_dir/$_" } readdir(DIR);
closedir DIR;

for(@files){
	open FH, "$some_dir/$_" or die  "can't open $_: $!";
	my $file=undef;
	read( FH, $file, -s FH ); 
	print $file
}
