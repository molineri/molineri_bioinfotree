#!/usr/bin/perl
use warnings;
use strict;
use lib $ENV{'BIOINFO_ROOT'}.'/local/lib/perl';
use Getopt::Long;
use MakefileParser;

my $usage = "$0 [-f makefile] [-assume-new dependency] [-sif] TARGET
produce a directed graph in the form

target1	dependence1
target1	dependence2
...

of all files that must be rebuild to produce the called TARGET.
  To build a complete graph run the script in a (almost) empty dir
so that all dependency files must be build or use --assume-new option
on a early dependency
";


my $makefile='makefile';
my $recursive=1;
my $tmp_filename='makefile_for_graph';
my $assume_new=undef;
my $tag="BuildMakefileGraph";
my $sif=0;

GetOptions (
	'makefile|f=s' => \$makefile,
	'assume-new|W=s' => \$assume_new,
	'sif' => $sif
) or die($usage);

my $target=shift;
$target or die $usage;

my $fh;
my $makefile_content = get_makefile_content($makefile);
open TMP_MAKEFILE,">$tmp_filename" or die("Can't write on file ($tmp_filename)");

&process($makefile_content);

sub process
{
	my $lines=shift;
	for(split(/\n/,$lines)){
		$_=$_."\n";
		while(m/^[^\s#].*:.*\w+.*\\$/){
			chomp;
			chop;	
			$_.=<$fh>;
			#print STDERR $_,"\n------------------------------\n";
		} 
		if($recursive and m/^include\s+([^\s]+)/){
			die "included file ($1) not found" if ! -e $1;
			my $fh2;
			open $fh2, $1;
			&process($fh2);
		}else{
			print TMP_MAKEFILE; 
			print TMP_MAKEFILE "\t#$tag \$@ : \$^\n" if m/^[^\s#].*[\w%]+.*:.*[\w]+.*$/ and !m/=/;
		}
	}
}

$assume_new = defined $assume_new ? "--assume-new=$assume_new" : '';
my $cmd= "make $assume_new -n -f $tmp_filename $target|" or die("Can't open pipe");
open PIPE, $cmd;
#unlink($tmp_filename);

$,= $sif ? "\t" : "\tdep\t";
$\="\n";
while(<PIPE>){
	if(s/#$tag//){
		s/://;
		chomp;
		my @F=split;
		my $t=shift @F;
		for(@F){
			print $t,$_ if $_;
		}
	}
}

sub get_makefile_content{
	my $makefile_name = shift;
	#configure and use the MakefileParser module to read the makefile
	my $fh;
	open $fh, $makefile_name or die("Can't open $makefile_name");
	set_makefile_name($makefile_name);
	my $makefile = readmakefile($fh);
	close $fh;
	return $makefile;
}
