#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage="$0 [-c] [-g \";\"] COL file
	do the contrary of expandsets
	
	-c output set cardinality instead of its elements
	-a print both the collapsed values and the cardinality
";

my $help=0;
my $glue=";";
my $cardinality=0;
my $add_c=0;
GetOptions (
	'h|help' => \$help,
	'glue|g=s' => \$glue,
	'cardinality|c' => \$cardinality,
	'add|a' => \$add_c
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my $col = shift;
die($usage)if $col !~/^\d+$/;
$col--;

die("-a meaningless without -c") if $add_c and not $cardinality;

my %a=();

while(<>){
	chomp;
	my @F = split /\t/,$_,-1;
	die("ERROR: not enough columns") if $col >= scalar(@F);
	my $element = splice @F,$col,1;
	${$a{join("\t",@F)}}{$element}=1;
}

for(keys %a){
	my $set;
	if ($cardinality) {
		my $c = scalar(keys(%{$a{$_}}));
		if($add_c){
			$set =  join($glue, keys(%{$a{$_}})) . "\t" . $c;
		}else{
			$set = $c;
		}
	} else {
		$set = join($glue, keys(%{$a{$_}}));
	}
	my @F=split /\t/,$_,-1;
	splice @F, $col, 0, $set;
	print @F;
}
