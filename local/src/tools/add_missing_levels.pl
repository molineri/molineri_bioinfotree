#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {my @loc = caller(); CORE::die "WARNING generated at line $loc[2] in $loc[1]: ", @_};

my $usage = "$0 [-d DEFAULT]\n";

my $default = 0;

GetOptions (
	'd|default=s' => \$default,
) or die($usage);

my %id=();
my %level=();
my %val=();

while(<>){
	chomp;
	my @F = split /\t/;
	warn("Unexpected column number") if scalar(@F) != 3;
	$id{$F[0]}=1;
	$level{$F[1]}=1;
	$val{$F[0] . "\t" . $F[1]} = $F[2];
}

foreach my $l (keys(%level)){
	foreach my $i (keys(%id)){
		my $k = $i."\t".$l;
		my $v = defined($val{$k}) ? $val{$k} : $default;
		print $k,$v;
	}
}
