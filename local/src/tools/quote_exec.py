#!/usr/bin/env python
#
# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

from cStringIO import StringIO
from optparse import OptionParser
from sys import exit as system_exit
from subprocess import Popen
from types import StringTypes
from vfork.util import exit, format_usage
import re

class Level(object):
    def __init__(self, index):
        self.index = index
        self.children = []

    def append(self, child):
        self.children.append(child)

    def __repr__(self):
        return '<Level: %d, %s>' % (self.index, repr(self.children))

class Literal(object):
    def __init__(self, content):
        self.content = content
    
    def __repr__(self):
        return '<Literal: %s>' % repr(self.content)

class ExprParser(object):
    def __init__(self, expr):
        self.expr = expr
        self.stack = [Level(0)]

    def parse(self):
        for token in self.iter_tokens():
            if isinstance(token, StringTypes):
                self.stack[-1].append(Literal(token))
            else:
                q = token.group()
                if q == '<[':
                    self.push_level()

                elif q == ']>':
                    self.pop_level(token, 'unbalanced quotes')

                elif q.startswith('<^'):
                    for i in xrange(len(q)-1):
                        self.pop_level(token, 'too many unquote levels')

                else:
                    for i in xrange(len(q)-1):
                        self.push_level()

        if len(self.stack) != 1:
            self.error(len(self.expr), 'unbalanced quotes')
        return self.stack[0]

    def iter_tokens(self):
        quote_rx = re.compile(r'<\[|\]>|<\^+|\^+>')

        pos = 0
        for quote in quote_rx.finditer(self.expr):
            start = quote.start()
            if start > 0 and self.expr[start-1] == '\\':
                if pos != start-1:
                    yield self.expr[pos:start-1]
                pos = start
            else:
                if pos != start:
                    yield self.expr[pos:start]
                yield quote
                pos = quote.end()

        if pos != len(self.expr):
            yield self.expr[pos:]

    def push_level(self):
        self.stack.append(Level(self.stack[-1].index+1))

    def pop_level(self, quote, errmsg):
        level = self.stack.pop()
        if len(self.stack) == 0:
            self.error(quote.start(), errmsg)
        else:
            self.stack[-1].append(level)

    def error(self, pos, errmsg):
        frag = self.expr[max(pos-20, 0):pos+20]
        pointer = ' '*min(20,pos) + '^'
        exit('%s:\n%s\n%s' % (errmsg, frag, pointer))


def parse(expr):
    return ExprParser(expr).parse()

def quote(level):
    cmd = StringIO()

    for c in level.children:
        if isinstance(c, Literal):
            cmd.write(c.content)
        else:
            if level.index == 0:
                cmd.write("'%s'" % quote(c))
            else:
                cmd.write("'\\''%s'\\''" % escape(quote(c)))

    return cmd.getvalue()

def escape(str):
    return str.replace("'", "'\\''")              

def main():
    parser = OptionParser(usage=format_usage('''
        %prog [OPTIONS] QUOTED_COMMAND

        Provides a convenient way to invoke commands (via the bash shell)
        with nested quotes.

        The command to be executed must be written inside single quotes so that
        bash doesn't try to expand it. Apart from those, no other (single) quotes
        should appear. You can use in their places:

        <[           to open a new quote
        ]>           to close a quote
        <^  and ^>   to escape a variable reference by one level
        <^^ and ^^>  to escape a variable reference by two levels
        ...

        You can escape any of such sequences by prependig a backslash (\\).


        Some command examples:

           'repeat_fasta_pipe <[cat]>'

           'outer=<[outer_var]>; repeat_fasta_pipe <[read h; l=$(wc -l); echo -e <[<^${h##>}^>\\t<^^$outer^^>\\t<^$l^>]>]>'

             Note that the 'h' and 'l' variables are defined inside the repeat_fasta_pipe argument, so they are escaped
             once. The 'outer' variable, on the other hand, has to be escaped twice.
    '''))
    parser.add_option('-d', '--debug', dest='debug', action='store_true', default=False, help='show the command expansion and exit')
    options, args = parser.parse_args()
    if len(args) != 1:
        exit('Unexpected command argument.')

    cmd = quote(parse(args[0]))
    if options.debug:
        print cmd
    else:
        proc = Popen(['bash', '-c', cmd], shell=False)
        system_exit(proc.wait())

if __name__ == '__main__':
    main()
