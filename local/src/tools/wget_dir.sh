#!/bin/bash
# Argument = -t test -r server -p password -v

usage()
{
cat << EOF
usage: $0 [-f FILTER] [-c] FTP_DIR_URL 

This script download all the file present in the FTP_DIR_URL.

OPTIONS:
   -h      Show this message
   -f      download only those filenames matching the regexp FILTER
   -v      download only those filenames NOT matching the regexp FILTER
   -c	   continue downloading partially downloaded files
EOF
}

FILTER="."
CONTINUE=""
while getopts "chf:" OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         f)
             FILTER=$OPTARG
             ;;
	 v)
             FILTER="-v $OPTARG"
	     ;;
	 c)
	     CONTINUE="-c"
	     ;;
     esac
done

shift $(($OPTIND - 1))
if [ -z $1 ]
then
	echo "ERROR $0: Wrong argument number"
	usage
	exit 1
fi

wget -O - $1 | grep $FILTER | wget $CONTINUE -i - --base=$1 --force-html
