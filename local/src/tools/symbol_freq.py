#!/usr/bin/env python
from __future__ import division
from optparse import OptionParser
from os.path import basename
from sys import argv, exit, stdin
from vfork.util import exit

def collect(symbols):
	counts = {}
	total = 0
	for symbol in symbols:
		counts[symbol] = counts.get(symbol, 0) + 1
		total += 1
	return counts, total

def main():
	parser = OptionParser(usage='%prog <SYMBOLS')
	parser.add_option('-r', '--reverse', dest='reverse', action='store_true', default=False, help='print count before symbol')	
	parser.add_option('-d', '--double', dest='double', action='store_true', default=False, help='same as symbol_count | cut -f 2 | symbol_count')
	parser.add_option('-c', '--check', dest='check', action='store_true', default=False, help='check a block sorting (tipically used as uniq < IN_FILE | symbol_count -c)')
	parser.add_option('-f', '--frequence', dest='frequence', action='store_true', default=False, help='report frequences instead of count')
	options, args = parser.parse_args()

        if options.double and options.frequence:
            raise Exception("Option -d and -f not allowed together")

	if len(args) != 0:
		exit('Unexpected argument number.')
	
	mode = 'count' if basename(argv[0]) == 'symbol_count' else 'freq'
        if options.frequence:
            mode="freq"
            
	if options.double and mode == 'freq':
		exit("Double option only supported for symbol_count.")

	##############  options.check
	#Do not count, only check for block sorting
	#

	check_set = set()
	if options.check:
		for l in stdin:
			if l in check_set:
				raise Exception("Disorder Found. Input is non block sorted.")
			check_set.add(l)
		return

	#
	#
	##############

	counts, total = collect(l.strip() for l in stdin)
	if options.double:
		counts, total = collect(counts.itervalues())

	for symbol in sorted(counts.iterkeys()):
		count = counts[symbol]
		if mode == 'count':
			if options.reverse:
				print '%d\t%s' % (count, str(symbol))
			else:
				print '%s\t%d' % (str(symbol), count)
		else:
			if options.reverse:
				print '%g\t%s' % (count/total, symbol)
			else:
				print '%s\t%g' % (symbol, count/total)

if __name__ == '__main__':
	main()
