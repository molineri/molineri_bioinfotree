#!/bin/bash
export LC_ALL=POSIX
exec awk -F'\t' -v OFS='\t' "$@"
