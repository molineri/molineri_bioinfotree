#!/usr/bin/perl
use warnings;
use strict;

my $seq='';

while(<>){
	chomp;
	$seq.=$_;
}
$seq = reverse $seq;
$seq =~ tr/ACGT/TGCA/;
print $seq."\n";
