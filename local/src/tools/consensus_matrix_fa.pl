#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;


my $usage = "$0 [-c 0.9] -m multialign_file [-f qualcosa.fa]
	multialign_file: file in which to store the multialignment
		or file already containing the multialigment
	-f qualcosa.fa to do now the multialigment
	with -c option prints the patterns with letter frequency > cutoff
		[-c cutoff on letter frequency]
		default: prints a consensus matrix\n";


my $cutoff = undef;
my $multialign_file = undef;
my $fasta_file = undef;
GetOptions (
	'cutoff|c=f'       => \$cutoff,
	'multial_file|m=s' => \$multialign_file,
	'fasta_file|f=s'   => \$fasta_file
) or die ($usage);

die $usage if !defined $multialign_file;


if (defined $fasta_file) {
	`clustalw -INFILE=$fasta_file -TYPE=DNA -OUTPUT=GDE -CASE=UPPER -OUTORDER=ALIGNED -OUTFILE=$multialign_file >> $multialign_file.err`;
	`rm $multialign_file.err`;
}

open MULTIALIGN,$multialign_file or die ("Can't open multialigment file ($multialign_file)");

my %consesus_matrix = ();
my $pos = 0;
my $seq_num = 0;
while (my $line = <MULTIALIGN>) {
	chomp $line;
	next if (!$line);

	if ($line =~ /^#/) {
		$pos = 0;
		$seq_num++;
		next;
	}

	while (my $char = substr $line,0,1,'') {
		$char = uc $char if ($char =~ /[a-z]/);
		if (!defined $consesus_matrix{$char}[$pos]) {
			$consesus_matrix{$char}[$pos] = 1;
		} else {
			$consesus_matrix{$char}[$pos]++;
		}
		$pos++;
	}
}

my @letters = sort keys %consesus_matrix;

foreach (@letters) {
	for (my $i=0; $i<$pos; $i++) {
		if (!defined $consesus_matrix{$_}[$i]) {
			$consesus_matrix{$_}[$i] = 0;
		} else {
			$consesus_matrix{$_}[$i] /= $seq_num;
		}
	}
}


if (defined $cutoff) {
	my $seq = "";
	my $colora = undef;

	for (my $i=0; $i<$pos; $i++) {
		my $max_lett = undef;
		my $max = 0;
		foreach (@letters) {
			if ($consesus_matrix{$_}[$i] > $max) {
				$max = $consesus_matrix{$_}[$i];
				$max_lett = $_;
			}
		}

		if ($consesus_matrix{$max_lett}[$i] >= $cutoff) {
			if (defined $colora) {
				$seq .= $max_lett;
			} else {
				print $seq;
				$seq = $max_lett;
				$colora = 1;
			}
		} else {
			if (defined $colora) {
				print "\e[4m$seq\e[0m";
				#print "\e[4;40m$seq\e[0m";
				$seq = $max_lett;
				$colora = undef;
			} else {
				$seq .= $max_lett;
			}
		}
	}
	
	if (defined $seq) {
		if (defined $colora) {
			print "\e[4m$seq\e[0m";
#			print "\e[4;40m$seq\e[0m";
		} else {
			print $seq;
		}
	}
	print "\n";
		
		
} else {
	foreach (@letters) {
		print $_,"\t",join ("\t",@{$consesus_matrix{$_}}),"\n";
	}
}
