#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
from vfork.io.colreader import Reader



def main():
	usage = format_usage('''
		%prog UNIVERSE [col1] [col2] ... [colN] < STDIN
		make a random relabeling of STDIN based on the label contained in UNIVERSE, relabel only the colums indicated (if no colum is indicared relabel each column)
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-n', '--repeat', type=int, dest='repeat', default=None, help='repeat the relabeling N times, each time print on stdout a fasta block [default: %default]', metavar='N')
	#parser.add_option('-p', '--dump-params', dest='params_file', help='some help FILE [default: %default]', metavar='FILE')
	#parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='some help [default: %default]')

	options, args = parser.parse_args()
	
	if len(args) == 0:
		exit('Unexpected argument number.')
	
	labels=set()
	with file(args[0], 'r') as fd:
		for l, in Reader(fd, '0s', False):
			labels.add(l)
	
	cols = None
	if args > 1:
		cols = [int(col) -1 for col in args[1:]]

	labels = list(labels)
	relabels = list(labels)
	shuffle(relabels)
	relabeling_map = dict(zip(labels,relabels))

	def process_row(tokens):
		if cols is None:
			tokens = safe_rstrip(line).split('\t')
			print "\t".join(map(lambda x: relabeling_map[x], tokens))
		else:
			for c in cols:
				t = tokens[c]
				tokens[c] = relabeling_map[t]
			print "\t".join(tokens)
		

	if options.repeat is None:
		for line in stdin:
			tokens = safe_rstrip(line).split('\t')
			process_row(tokens)
	else:
		lines=[]
		for line in stdin:
			lines.append(safe_rstrip(line).split('\t'))
		for i in xrange(1,options.repeat+1):
			print ">%d" % i
			if i>1:
				shuffle(relabels)
				relabeling_map = dict(zip(labels,relabels))
			for tokens in lines:
				process_row(tokens)

if __name__ == '__main__':
	main()

