#!/usr/bin/perl
use warnings;
use strict;

my $min=undef;
my $max=undef;
while(<>){
	chomp;
	$min = $_ if (!defined($min) or $min > $_);
	$max = $_ if (!defined($max) or $max < $_);
}

print "$min\t$max\n";
