#!/usr/bin/perl

use warnings;
use strict;

my $usage="$0 N 'PIPE' < FILE\n run and feed the PIPE with the FILE only if FILE contains >=N rows";

die($usage) if !@ARGV;
my $n = shift;
die($usage) if $n!~/^\d+$/;
my $pipe = shift;
die($usage) if $pipe=~/^\d+$/;


my @rows = <>;
if(scalar(@rows) >= $n){
	open PIPE, "|$pipe" or die("Can't open ($pipe)");
	print PIPE @rows if scalar(@rows) >= $n;
}
