#include <stdio.h>
#include <stdlib.h>
int main(int argc, char *argv[]) {
  int num_files = argc - 1;
  FILE *outs[num_files];
  for (int i = 0; i < argc; i++) {
    outs[i] = fopen(argv[i + 1], "a");
  }
  // Credits go to 'man getline' for the guts of this...
  ssize_t read;
  char *line = NULL;
  size_t len = 0;
  int counter = 0;
  while ((read = getline(&line, &len, stdin)) != -1) {
    // I'm pretty sure that buffering here would make a huge difference in performance.
    fwrite(line, read, 1, outs[counter % num_files]);
    counter++;
  }
  if (line)
    free(line);
  for (int i =0; i < num_files; i++) {
    fclose(outs[i]);
  }
  return EXIT_SUCCESS;
}
