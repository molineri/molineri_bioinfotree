#!/usr/bin/perl

use warnings;
use strict;

my $usage = "$0 FRACTION < DATA
	DATA is a single column of unmber file
	return the value V so that the FRACTION of the data are grater than V
	for example if FRACT = 0.05 then the 5% of the DATA are greather than the retruned V
";

my $fract = shift @ARGV;

die($usage) if not defined($fract) or $fract =~ '^-h';

die($usage) if $fract < 0;

my @D=();
my $last=undef;
while(<>){
	chomp;
	die("ERRROR: disorder found") if defined $last and $last > $_;
	push @D,$_;
}

print $D[ int($#D*$fract) ],"\n";
