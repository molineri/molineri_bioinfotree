#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;

$SIG{__WARN__} = sub {die @_};

my $usage="$0 -n N [-s HEAD] [-f HEAD_FILE] FILE\
eliminate N rows at the head of the FILE or, optionally,
substitute the firs N rows with HEAD.
With -f write the removed header in HEAD_FILE.
";

my $help=0;
my $head=undef;
my $n_row=1;
my $filename=undef;
GetOptions (
	'n=i' => \$n_row,
	's=s' => \$head,
	'f=s' => \$filename,
	'h' => \$help
) or die ($usage);

if($help){
	print $usage;
	exit(0);
}

if(defined($head)){
	print "$head\n";
}

if(defined($filename)){
	open FH_HEAD,">$filename" or die("Can't open file ($filename)");
}

my $i=0;
if(!defined($ARGV[0])){
	while(<>){
		if($i>=$n_row){
			print;
		}else{
			if(defined $filename){
				print FH_HEAD $_;
			}
			$i++;
		}
	}
}else{
	for(@ARGV){
		open FH,$_ or die("Can't open file ($_)");
		$i=0;
		while(<FH>){
			if($i>=$n_row){
				print;
			}else{
				if(defined $filename){
					print FH_HEAD $_;
				}
				$i++;
			}
		}
		close FH;
	}
}
