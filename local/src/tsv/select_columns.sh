#!/bin/bash
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

if [[ $# -eq 0 ]]; then
	echo "Unexpected argument number." >&2
	exit 1
fi

cmd='{print '
while [[ $# -gt 0 ]]; do
	if [[ $1 == -h || $1 == --help ]]; then
		echo "Usage: $0 INDEXES <TSV >SWAPPED_TSV" >&2
		echo >&2
		echo "Prints columns from the input file in the requested order." >&2
		exit 1
	fi

	cmd="$cmd\$$1,"
	shift
done

cmd="${cmd%,}}"
exec bawk "$cmd"
