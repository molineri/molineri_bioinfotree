#!/usr/bin/env python
# Copyright 2005 Gabriele Sales <sales@to.infn.it>
# Distributed under the terms of the GNU General Public License v2

from bash import Bash, BashError
from itertools import izip
from optparse import OptionParser
from os import getcwd, stat, system, unlink
from os.path import join as joinPath
from sys import argv, exit, stderr
import errno
import parser

def isNewer(a, b):
	try:
		return stat(a)[8] > stat(b)[8]
	except OSError, e:
		if e.args[0] == errno.ENOENT:
			return True

def fullUpdate(inFile, outFile, sections):
	if isNewer(inFile, outFile):
		return True
	
	for section in sections:
		if isinstance(section, parser.IncludeVerbatim) and isNewer(section.filename, outFile):
			return True
	
	return False

def bindNamedVars(rule):
	try:
		aux = []
		for idx, name in enumerate(rule.attributes['for_each']):
			aux.append('%s=$(%d); ' % (name[0], idx+1))
		return ''.join(aux)
	
	except KeyError:
		return ''

def filterCustomDeps(rule):
	cmd = rule.attributes['cmd']
	return cmd.replace('$^', '$(filter-out $(%s_custom_deps),$^)' % rule.name)

def updateMakefile(sections):
	mkf = file('Makefile', 'w')
	
	rules = tuple(r for r in sections if isinstance(r, parser.Rule))
	print >>mkf, 'all :', ' '.join(r.name for r in rules)
	print >>mkf, 'clean :', ' '.join('%s_clean' % r.name for r in rules), 'make_clean'
	print >>mkf, 'make_clean:'
	print >>mkf, '\trm -f Makefile'
	print >>mkf
	
	for rule in rules:
		print >>mkf, 'define %s_target_build' % rule.name
		print >>mkf, '\tdir=$$(dirname $@); [ -d $${dir} ] || mkdir -p $${dir}'
		print >>mkf, '\t%s%s' % (bindNamedVars(rule), filterCustomDeps(rule))
		print >>mkf, 'endef'
		print >>mkf
	
	for section in sections:
		if isinstance(section, parser.IncludeVerbatim):
			mkf.write(section.content)
			print >>mkf
		
		else:
			customDeps = section.attributes.get('custom_deps', '')
			print >>mkf, 'include %s.d' % section.name
			print >>mkf, '%s_custom_deps:=Makefile %s' % (section.name, customDeps)		
			print >>mkf, '$(%s_targets) : $(%s_custom_deps)' % (section.name, section.name)
			print >>mkf
			
			print >>mkf, '%s : $(%s_targets)' % (section.name, section.name)
			print >>mkf, '%s_clean:' % section.name
			print >>mkf, '\trm -f %s.d $(%s_targets)' % (section.name, section.name)
			print >>mkf

def writeCall(fd, rule, params):
	if len(params):
		params = ',' + ','.join(params)
	else:
		params = ''
	print >>fd, '\t$(call %s_target_build%s)' % (rule.name, params)

def updateDepfile(rule, filename):
	global shell
	df = file(filename, 'w')
	
	allTargets = []
	for context in ruleContexts(rule):
		if context.target in allTargets:
			print >>stderr, "Found duplicated target '%s'." % context.target
			exit(1)
		
		allTargets.append(context.target)
	
		print >>df, '%s : %s' % (context.target, ' '.join(context.sources))
		writeCall(df, rule, context.params)
		print >>df
	
	var = '%s_targets' % rule.name
	allTargets = ' '.join(allTargets)
	print >>df, '%s:=%s' % (var, allTargets)
	print >>df
	
	try:
		shell.export(var, allTargets)
	except BashError, e:
		raise ConfigError, "error exporting '%s': %s" % (var, str(e))
	
	return allTargets

class RuleContext(object):
	__slots__ = ('sources', 'target', 'params')
	
	def __init__(self):
		for attr in self.__slots__:
			setattr(self, attr, None)

def ruleContexts(rule, idx=0):
	global shell
	
	if idx == 0 and not rule.attributes.has_key('for_each'):
		for context in expandVars(rule):
			context.params = tuple()
			yield context
		return
	
	fi = rule.attributes['for_each'][idx]
	hasMore = len(rule.attributes['for_each'])-1 > idx
	
	for item in shell.eval(fi[1]).split():
		shell.export(fi[0], item)
		if hasMore:
			for context in ruleContexts(rule, idx+1):
				context.params.insert(0, item)
				yield context
		else:
			for context in expandVars(rule):
				context.params = [ item ]
				yield context

def adjustTargetPath(target, sourcePrefix, targetPrefix):
	if target.startswith(sourcePrefix):
		target = targetPrefix + target[len(sourcePrefix):]
	elif target[0] != '/':
		target = targetPrefix + target
	return target

def evalTarget(expression, sourcePrefix, targetPrefix):
	target = shell.eval(expression)
	if target.find(' ') != -1 or target.find('\t') != -1:
		raise ConfigError, "unexpected whitespace detected in single target '%s'" % target
	return adjustTargetPath(target, sourcePrefix, targetPrefix)

def expandDefines(attrs):
	global shell

	if 'define' in attrs:
		for var, value in attrs['define']:
			shell.export(var, value)

def expandVars(rule):
	global shell
	attrs = rule.attributes

	for attr in ('source_prefix', 'target_prefix'):
		if not attrs.has_key(attr):
			continue
		
		try:
			v = shell.eval(attrs[attr])
			shell.export(attr, v)
			if v.find(' ') != -1 or v.find('\t') != -1:
				raise ConfigError, "unexpected whitespace detected in %s for rule '%s'" % (attr, rule.name)
		
		except BashError, e:
			raise ConfigError, 'error while expanding %s: %s' % (attr, str(e))
	
	try:
		sourcePrefix = attrs.get('source_prefix', '')
		if len(sourcePrefix) > 0:
			shell.chdir(joinPath(getcwd(), sourcePrefix))
		
		if 'sources' in attrs:
			sourceStr = shell.eval(attrs['sources'])
			if len(sourcePrefix) > 0:
				sources = [ joinPath(sourcePrefix, s) for s in sourceStr.split() ]
			else:
				sources = sourceStr.split()
		else:
			sources = [ None ]
	
	except BashError, e:
		raise ConfigError, 'error while expanding sources: %s' % str(e)
		
	try:
		targetPrefix = attrs.get('target_prefix', '')
		
		if attrs.has_key('target'):
			context = RuleContext()
			context.sources = tuple(sources)
			expandDefines(attrs)
			context.target  = evalTarget(attrs['target'], sourcePrefix, targetPrefix)			
			yield context
		
		else:
			targetSpec = attrs['targets']
			#TODO: better varname detection
			if targetSpec.find('$') == -1:
				targets = targetSpec.split()
				if len(targets) != len(sources):
					raise ConfigError, "source number doesn't match target number (%d:%d) in rule '%s'" % (len(sources), len(targets), rule.name)
				
				for source, target in izip(sources, targets):
					context = RuleContext()
					
					if source is None:
						context.sources = tuple()
					else:
						context.sources = (source,)
					
					context.target  = adjustTargetPath(target, sourcePrefix, targetPrefix)
					yield context
			
			else:
				for source in sources:
					context = RuleContext()
					
					if source is None:
						context.sources = tuple()
					else:
						shell.export('source', source)
						context.sources = (source,)
					
					expandDefines(attrs)
					context.target  = evalTarget(targetSpec, sourcePrefix, targetPrefix)
					yield context
	
	except BashError, e:
		raise ConfigError, 'error while expanding targets: %s' % str(e)

class ConfigError(Exception): pass

if __name__ == '__main__':
	import pdb

	op = OptionParser()
	op.add_option('-c', '--force', dest='force', default=False, action='store_true', help='Force the rebuild of all targets.')
	op.add_option('-f', '--file', dest='inFile', default='scimake', help="Read FILE as a scimakefile.", metavar='FILE')
	op.add_option('-j', '--jobs', dest='jobNum', type='int', default=1, help='Allow NUM jobs at once.', metavar='NUM')
	op.add_option('-n', '--dry-run', action='store_true', dest='dryRun', help="Don't run the generated Makefile.")
	op.add_option('-s', '--silent', action='store_true', dest='silent', help="Don't echo commands.")
	options, args = op.parse_args()
	
	try:
		p = parser.Parser(options.inFile)
	except parser.ParseError, e:
		print >>stderr, 'Parse error:', str(e)
		exit(1)
	except IOError, e:
		print >>stderr, 'Read error:', str(e)
		exit(1)
	
	try:
		shell = Bash()
	
		try:
			filename = 'Makefile'
			if fullUpdate(options.inFile, filename, p.sections):
				updateMakefile(p.sections)
			
			for rule in (s for s in p.sections if isinstance(s, parser.Rule)):
				filename = '%s.d' % rule.name
				updateDepfile(rule, filename)
		
		except (IOError, ConfigError), e:
			if isinstance(e, BashError):
				print >>stderr, 'Bash error:', str(e)
			else:
				print >>stderr, 'Error building %s:' % filename, str(e)
			
			try: unlink(filename)
			except OSError: pass
			exit(1)
		
		if options.force:
			system('touch Makefile')
		
		if not options.dryRun:
			makeOpts = ''
			if options.silent:
				makeOpts += '-s'
			system('make -j%d %s %s' % (options.jobNum, makeOpts, ' '.join(args)))
	
	finally:
# 		print "ELAPSED IN SHELL: ", shell.elapsed
		shell.close()
