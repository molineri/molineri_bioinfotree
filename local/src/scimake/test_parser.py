import unittest as ut
import parser

class TestReader(ut.TestCase):
	def testIterator(self):
		r = parser.Reader('test_parser-1.txt')
		f = file('test_parser-1.txt')
		
		for idx, line in enumerate(r.lines()):
			self.assertEqual(idx+1, r.lineno)
			self.assertEqual(line, f.readline().rstrip())
		
		self.assertEqual(len(f.readline()), 0)

class TestParser(ut.TestCase):
	def testOutOfRuleComments(self):
		try:
			p = parser.Parser('test_parser-1.txt')
			self.fail('undetected whitespace error')
		except parser.ParseError, e:
			self.assertEqual(e.lineno, 4)
	
	def testHeaderGarbage(self):
		try:
			p = parser.Parser('test_parser-2.txt')
			self.fail('undetected header garbage')
		except parser.ParseError, e:
			self.assertEqual(e.lineno, 1)
	
	def testInvalidIndentation(self):
		try:
			p = parser.Parser('test_parser-3.txt')
			self.fail('undetected invalid indentation level')
		except parser.ParseError, e:
			self.assertEqual(e.lineno, 3)
		
		try:
			p = parser.Parser('test_parser-4.txt')
			self.fail('undetected invalid indentation level')
		except parser.ParseError, e:
			self.assertEqual(e.lineno, 3)
	
	def testEmptyRule(self):
		try:
			p = parser.Parser('test_parser-5.txt')
			self.fail('undetected empty rule')
		except parser.ParseError, e:
			self.assertEqual(e.lineno, 2)
	
	def testInvalidAttribute(self):
		try:
			p = parser.Parser('test_parser-6.txt')
			self.fail('undetected invalid attribute')
		except parser.ParseError, e:
			self.assertEqual(e.lineno, 2)
		
		try:
			p = parser.Parser('test_parser-7.txt')
			self.fail('undetected invalid attribute')
		except parser.ParseError, e:
			self.assertEqual(e.lineno, 2)
	
	def testDuplicateAttribute(self):
		try:
			p = parser.Parser('test_parser-8.txt')
			self.fail('undetected duplicate attribute')
		except parser.ParseError, e:
			self.assertEqual(e.lineno, 3)

	def testAttributeConflict(self):
		try:
			p = parser.Parser('test_parser-9.txt')
			self.fail('undetected invalid attribute combination')
		except parser.ParseError, e:
			self.assertEqual(e.lineno, 3)
	
	def testRuleName(self):
		p = parser.Parser('test_parser-10.txt')
		self.assertEqual(len(p.sections), 1)
		self.assertEqual(isinstance(p.sections[0], parser.Rule), True)
		self.assertEqual(p.sections[0].name, 'rulename')
	
	def testAttributes(self):
		p = parser.Parser('test_parser-10.txt')
		rule = p.sections[0]
		self.assertEqual(len(rule.attributes), 3)
		self.assertEqual(rule.attributes['sources'], 'one two')
		self.assertEqual(rule.attributes['target'], 'three')
		self.assertEqual(rule.attributes['cmd'], 'nothing')
	
	def testIncludeVerbatim(self):
		p = parser.Parser('test_parser-11.txt')
		self.assertEqual(len(p.sections), 1)
		self.assertEqual(isinstance(p.sections[0], parser.IncludeVerbatim), True)

		inc = p.sections[0]
		content = file('test_parser-1.txt').read()
		self.assertEqual(inc.content, content)
	
	def testMalformedForEach(self):
		try:
			p = parser.Parser('test_parser-12.txt')
			self.fail('undetected malformed for_each')
		except parser.ParseError, e:
			self.assertEqual(e.lineno, 2)
	
	def testMissingAttribute(self):
		try:
			p = parser.Parser('test_parser-13.txt')
			self.fail('undetected missing attribute')
		except parser.ParseError, e:
			self.assertEqual(e.lineno, 2)
	
	def testCommentedAttribute(self):
		try:
			p = parser.Parser('test_parser-14.txt')
		except parser.ParseError, e:
			self.fail('exception raised for valid input: ' + str(e))
	
	def testMalformedDefine(self):
		try:
			p = parser.Parser('test_parser-15.txt')
			self.fail('undetected malformed define')
		except parser.ParseError, e:
			self.assertEqual(e.lineno, 3)
	
	def testDefine(self):
		p = parser.Parser('test_parser-16.txt')
		rule = p.sections[0]
		self.assertEqual(True, 'define' in rule.attributes)
		self.assertEqual([('s', 'dummy'), ('t', 'dummy2')], rule.attributes['define'])

if __name__ == '__main__':
	ut.main()
