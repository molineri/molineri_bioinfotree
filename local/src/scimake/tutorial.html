<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="generator" content="Docutils 0.3.8: http://docutils.sourceforge.net/" />
<title>SciMake tutorial</title>
<meta name="author" content="Gabriele Sales &lt;sales&#64;to.infn.it&gt;" />
<link rel="stylesheet" href="default.css" type="text/css" />
</head>
<body>
<div class="document" id="scimake-tutorial">
<h1 class="title">SciMake tutorial</h1>
<table class="docinfo" frame="void" rules="none">
<col class="docinfo-name" />
<col class="docinfo-content" />
<tbody valign="top">
<tr><th class="docinfo-name">Author:</th>
<td>Gabriele Sales &lt;<a class="reference" href="mailto:sales&#64;to.infn.it">sales&#64;to.infn.it</a>&gt;</td></tr>
<tr><th class="docinfo-name">Version:</th>
<td>1.0 (08/02/2005)</td></tr>
</tbody>
</table>
<div class="section" id="what-is-it">
<h1><a name="what-is-it">What is it?</a></h1>
<p>SciMake is a software extending the <tt class="docutils literal"><span class="pre">make</span></tt> tool so to simplify automation and dependency tracking in scientific computing projects. It is <em>not</em> a <tt class="docutils literal"><span class="pre">make</span></tt> replacement; rather, it allows you to express rules with a much more compact notation.</p>
</div>
<div class="section" id="basics">
<h1><a name="basics">Basics</a></h1>
<p>When you start SciMake, the software looks for a <tt class="docutils literal"><span class="pre">scimake</span></tt> file into the current directory (you can specify another filename by using the '<tt class="docutils literal"><span class="pre">-f</span></tt>' command-line switch). That text file describes a number of <em>rules</em>, each one listing <em>source</em> files that you want to process into <em>targets</em> by means of an external <em>command</em>. A minimal <tt class="docutils literal"><span class="pre">scimake</span></tt> file is therefore:</p>
<pre class="literal-block">
transformPs:
        sources=a.ps b.ps
        targets=a.pdf b.pdf
        cmd=ps2pdf $&lt; $&#64;
</pre>
<p>Running such rule is equivalent to the following:</p>
<pre class="literal-block">
ps2pdf a.ps a.pdf
ps2pdf b.ps b.pdf
</pre>
<p>As you can see, sources and targets are automatically paired using a many-to-many relation. If, on the contrary, we have many input files and just one target, we can write:</p>
<pre class="literal-block">
mergePs:
        sources=a.ps b.ps
        target=book.ps
        cmd=psmerge -o $&#64; $^
</pre>
<p>The net result is:</p>
<pre class="literal-block">
psmerge -o book.ps a.ps b.ps
</pre>
</div>
<div class="section" id="variable-expansion">
<h1><a name="variable-expansion">Variable expansion</a></h1>
<p>All the attributes of a rule, with the only exception of 'cmd', are interpreted by the <tt class="docutils literal"><span class="pre">bash</span></tt> shell before SciMake actually uses them. We can exploit this preprocessing stage to make our <tt class="docutils literal"><span class="pre">scimake</span></tt> more compact; consider the example already presented in the <a class="reference" href="#basics">Basics</a> section:</p>
<pre class="literal-block">
transformPs:
        sources=a.ps b.ps
        targets=a.pdf b.pdf
        cmd=ps2pdf $&lt; $&#64;
</pre>
<p>Though perfectly legal, this rule is redundant: we can obtain a target name from its source by replacing the 'ps' suffix with 'pdf'. SciMake can do that automatically for us by means of the <tt class="docutils literal"><span class="pre">source</span></tt> variable <a class="footnote-reference" href="#id2" id="id1" name="id1">[1]</a>:</p>
<pre class="literal-block">
transformPs:
        sources=a.ps b.ps
        targets=${source%%ps}pdf
        cmd=ps2pdf $&lt; $&#64;
</pre>
<p>With the target template now in place, we can use a glob instead of the explicit source list:</p>
<pre class="literal-block">
transformPs:
        sources=*.ps
        targets=${source%%ps}pdf
        cmd=ps2pdf $&lt; $&#64;
</pre>
<p>This final form has the big advantage of being extensible: if we add another ps file to our collection, we don't need to change the <tt class="docutils literal"><span class="pre">transformPs</span></tt> rule.</p>
</div>
<div class="section" id="paths">
<h1><a name="paths">Paths</a></h1>
<p>It is often desirable to store sources and targets into different directories; SciMake helps in this task by performing automatic path substitution. Consider the following:</p>
<pre class="literal-block">
transformPs:
        sources=sources/*.ps
        targets=targets/$(basename $source ps)pdf
        cmd=ps2pdf $&lt; $&#64;
</pre>
<p>This rule performs the same ps-to-pdf conversion presented in previous examples, but in this case source Postscripts are searched into the <tt class="docutils literal"><span class="pre">sources</span></tt> directory and resulting pdf files are written into <tt class="docutils literal"><span class="pre">targets</span></tt>. Although the rule may seem simple, it is in fact mixing  static prefixes (directory names) with dynamic suffixes (<tt class="docutils literal"><span class="pre">*.ls</span></tt> and the <tt class="docutils literal"><span class="pre">basename</span></tt> stuff); we can clearly separate the two by using the attributes <tt class="docutils literal"><span class="pre">source_prefix</span></tt> and <tt class="docutils literal"><span class="pre">target_prefix</span></tt>:</p>
<pre class="literal-block">
transformPs:
        source_prefix=sources/
        target_prefix=targets/
        sources=*.ps
        targets=${source%%ps}pdf
        cmd=ps2pdf $&lt; $&#64;
</pre>
<p>As you can see there's no longer any reference to directories neither in <tt class="docutils literal"><span class="pre">sources</span></tt> nor in <tt class="docutils literal"><span class="pre">targets</span></tt>; both attributes are easier to read.  Still this rule is completely equivalent to the previous one.</p>
<p>Please note that the two prefix attributes are independent, so there's no need to use them both at all times.</p>
</div>
<div class="section" id="loops">
<h1><a name="loops">Loops</a></h1>
<p>Sometimes you want to process the same input file multiple times at the change of a parameter; for example, you may want to extract a number of pages from a single ps file. Here is how SciMake can help:</p>
<pre class="literal-block">
dumpPage:
        for_each=page in $(seq 10)
        sources=a.ps
        target=a-$page.ps
        cmd=psselect $$page $&lt; $&#64;
</pre>
<p>The <tt class="docutils literal"><span class="pre">for_each</span></tt> attribute declares the <tt class="docutils literal"><span class="pre">page</span></tt> variable; its values are taken from the exapansion of the <tt class="docutils literal"><span class="pre">$(seq</span> <span class="pre">10)</span></tt> expression (generally speaking, from the expression following the <tt class="docutils literal"><span class="pre">in</span></tt> keyword). That same variable can be later referenced from other attributes; please note that <tt class="docutils literal"><span class="pre">cmd</span></tt> is directly interpreted by <tt class="docutils literal"><span class="pre">make</span></tt>, so references are written prepending <tt class="docutils literal"><span class="pre">$$</span></tt> instead of a single dollar sign.</p>
<hr class="docutils" />
<p><strong>Notes</strong></p>
<table class="docutils footnote" frame="void" id="id2" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label"><a class="fn-backref" href="#id1" name="id2">[1]</a></td><td>The <tt class="docutils literal"><span class="pre">${varname%%pattern}</span></tt> construct is a <tt class="docutils literal"><span class="pre">bash</span></tt> feature; please consult <tt class="docutils literal"><span class="pre">bash</span></tt> man page if you need more informations.</td></tr>
</tbody>
</table>
</div>
</div>
</body>
</html>
