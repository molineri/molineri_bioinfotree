#!/usr/bin/env python
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from array import array
from optparse import OptionParser
from random import shuffle
from sys import stdin, stdout
from vfork.fasta.reader import MultipleBlockStreamingReader
from vfork.fasta.writer import MultipleBlockWriter
from vfork.util import exit, format_usage

def base_shuffle(seq):
	arr = array('c', seq)
	shuffle(arr)
	yield arr.tostring()

def window_shuffle(width):
	def proc(seq):
		block_num = len(seq) // width
		if len(seq) % width:
			block_num += 1

		idxs = array('I', xrange(block_num))
		shuffle(idxs)

		for idx in idxs:
			start = idx * width
			stop = start + width
			yield seq[start:stop]
	
	return proc

def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] <FASTA >SHUFFLED_FASTA

		Shuffles the contents of each FASTA block.
	'''))
	parser.add_option('-w', '--window', dest='window', type='int', help='perform shuffling using a fixed-sized window', metavar='WIDTH')
	options, args = parser.parse_args()

	if len(args) != 0:
		exit('Unexpected argument number.')

	if options.window is None:
		shuffle = base_shuffle
	else:
		if options.window < 1:
			exit('Invalid window width: %s' % str(options.window))
		elif options.window == 1:
			shuffle = base_shuffle
		else:
			shuffle = window_shuffle(options.window)

	reader = MultipleBlockStreamingReader(stdin)
	writer = MultipleBlockWriter(stdout)
	
	for header, content in reader:
		writer.write_header(header)
		for block in shuffle(content):
			writer.write_sequence(block)
	
	writer.flush()
	
if __name__ == '__main__':
	main()

