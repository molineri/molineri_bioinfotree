#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;

$SIG{__WARN__} = sub {die @_};

my $usage="$0 ['-a options1 -b options2'] function.pm file.fa
sub in function.pm must be called 'foreach_fasta'
the signature of the function is:
&foreach_fasta(\$id,\\\@rows,\\\%opt);
\$id contains the entire header row of a single block
\@rows the entire content of a single block splitted in lines
\%opt report the oprtion passed as first argument of $0
";

my $help=0;
GetOptions (
	'h|help' => \$help,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my %opt;
my $tmp=shift(@ARGV);
$tmp or die($usage);

if($tmp=~/\.pm$/){
	unshift @ARGV,$tmp;
}else{
	my $pre=undef;
	for(split /\s+/, $tmp){
		if(m/^-/){
			$opt{$_}=1;
			$pre=$_;
		}else{
			$opt{$pre}=$_;
			$pre=undef;
		}
	}
}


my $filename = pop(@ARGV);
my $modulename=undef;
if($filename=~/\.pm$/){
	$modulename=$filename;
}else{
	$modulename=pop(@ARGV);
	push(@ARGV,$filename);
}

unless ($_ = do $modulename) {
	warn "non si e` potuto fare l'analisi sintattica di $modulename: $@" if $@;
	warn "non si e` potuto eseguire do sul $modulename: $!"   unless defined $_;
	warn "non si e` potuto eseguire il $modulename"           unless $_;
}

my @rows=();
my $id=undef;
while(<>){
	chomp;
	next if length==0 ;
	if(/^>/){
		my $post_id=$_;
		
		&foreach_fasta($id,\@rows,\%opt) if @rows;
		
		$id=$post_id;
		@rows=();
		next;
	}
	push @rows, $_;
}

&foreach_fasta($id,\@rows,\%opt) if @rows;
