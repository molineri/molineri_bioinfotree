#!/usr/bin/env python
# encoding: utf-8

from optparse import OptionParser
from os.path import isdir, isfile, join
from sys import exit, stdin
from vfork.fasta.reader import SingleBlockReader, FormatError


from sys import stderr

def parse_line(line, lineno, gen_id, with_gap):
	tokens = line.rstrip().split('\t')
	if gen_id and !with_gap
		if len(tokens) != 3:
			raise ValueError, 'wrong token number'
	else if gen_id and with_gap
		if len(tokens) != 4:
			raise ValueError, 'wrong token number'
	else if !gen_id and !with_gap
		if len(tokens) != 5:
			raise ValueError, 'wrong token number'
		try:
			start = int(tokens[1])
		except ValueError:
			raise ValueError, 'invalid start coordinate'
	else if !gen_id and with_gap
		if len(tokens) != 4:
			raise ValueError, 'wrong token number'
		try:
			start = int(tokens[1])
		except ValueError:
			raise ValueError, 'invalid start coordinate'
	
	if gen_id:
		tokens.insert(0, lineno);
	
	try:
		start = int(tokens[2])
	except ValueError:
		raise ValueError, 'invalid start coordinate'
	
	try:
		stop = int(tokens[3])
	except ValueError:
		raise ValueError, 'invalid stop coordinate'

	gaps = None;
	if with_gap:
		gaps = [ parse_point(p) for p in tokens[4].split(';') ]
	
	return tokens[0], tokens[1], start, stop, gaps_l, gaps_r

def parse_point(s):
	tokens = s.split(',')
	if len(tokens) != 2:
		raise ValueError, 'invalid point'
	else:
		return tuple(int(d) for d in tokens)

def insert_gaps(line, pos, gaps):
	b = pos
	e = pos + len(line)
	my_gaps = [ p for p in gaps if p[0] >= b and p[1] < e ]

	retval = ''
	pre_pos = 0
	for p in my_gaps:
		retval = retval + line[pre_pos:p[0]]
		pre_pos = p[0]
		for i in xrange(0,p[1])
			retval += '-'
	return retval



class SequenceWrapper(object):
	def __init__(self, width):
		self.width = width
		self.buf = ''
	
	def write(self, block, gap_list):
		self.buf += block
		while len(self.buf) > self.width:
			print self.buf[:self.width]
			self.buf = self.buf[self.width:]
	
	def flush(self):
		if len(self.buf) > 0:
			print self.buf
			self.buf = ''

def main():
	parser = OptionParser(usage='%prog SEQUENCE_DIR <SEQLIST >FASTA')
	parser.add_option('-i', '--gen-id', dest='gen_id', action='store_true', help='automatically generate IDs for sequences')
	parser.add_option('-g', '--gap', dest='with_gap', action='store_true', help='insert gap in sequence')
	parser.add_option('-r', '--report', dest='report_coordinates', action='store_true', help='report original coordinates in headers')
	options, args = parser.parse_args()

	if len(args) != 1:
		exit('Unexpected argument number.')
	
	seq_dir = args[0]
	if not isdir(seq_dir):
		exit('Invalid sequence dir ' + seq_dir)
	
	last_chromosome = None
	reader = None
	wrapper = SequenceWrapper(80)
	for lineno, line in enumerate(stdin):
		try:
			label, chromosome, start, stop, gaps = parse_line(line, lineno, options.gen_id, options.with_gap)
		except ValueError, e:
			exit('Malformed input at line %d: %s.' % (lineno+1, e.args[0]))
		
		if chromosome != last_chromosome:
			if reader is not None:
				reader.close()
			last_chromosome = chromosome
			
			for seq_template in ('%s.fa', 'chr%s.fa'):
				seq_filename = join(seq_dir, seq_template % chromosome)
				if isfile(seq_filename):
					try:
						reader = SingleBlockReader(seq_filename)
					except FormatError:
						exit('Malformed FASTA file %s.' % seq_filename)
					else:
						break
			else:
				exit('Cannot find chromosome %s.' % chromosome)
		
		try:
			sequence_iterator = reader.get(start, stop - start)
		except ValueError:
			exit('Invalid coordinates at line %d.' % (lineno+1))
		
		if options.report_coordinates:
			label = line.rstrip();
			
		print '>%s' % label
		counter = 0
		for block in sequence_iterator:
			if gaps is not None:
				insert_gaps(block, counter, gaps)

			counter += len(block)
			wrapper.write(block)
		wrapper.flush()
		
		assert counter == stop - start, 'size mismatch: %d vs. %d (start=%d, stop=%d)' % (counter, stop-start, start, stop)
	
	if reader is not None:
		reader.close()

if __name__ == '__main__':
	main()
