#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Long;

my $usage=" repeat_fasta.pl -c 'blastall [opzioni] -i \@' [-t tmpdir] [-e|not_exit_on_error] [-r] -g 100 file.fasta
	@ is required in -c
";

my $comando;
my $tmp_dir;
my $remove=0;
my $group=1;
my $not_exit_on_error=0;

GetOptions (
	'comando|c=s' => \$comando,
	'tmp_dir|t=s' => \$tmp_dir,
	'remove|r' => \$remove,
	'group|g=n' => \$group,
	'not_exit_on_error|e' => \$not_exit_on_error
) or die($usage);

$comando or die ($usage);

$comando =~ s/apice/'/g;
$comando =~/@/ or die($usage);
$tmp_dir or chomp($tmp_dir=`pwd`);

my $filename=undef;
my $id=undef;
my $TMP;
my @fasta=<>;
my $imax=scalar(@fasta) - 1;
my $i=0;#conta le righe
my $j=0;#conta le sequenze
for(@fasta){
	
	if(m/>/){
		$j++;
		if(!defined($filename)){#succede solo per la prima sequenza
			$id=$_;
			chomp $id;
			$id=~s/^>//;
			$id=~s/[ \t]/_/;
			$filename=$tmp_dir.'/'.$id;
			$filename=~s/\(//g;
			$filename=~s/\)//g;
			chomp $filename;
			$filename.='.fa';
			open $TMP, ">$filename" or die("Can't write file ($filename)");
			
			if(-e $filename){
				
			}else{
			}
			
			$j--; #non ho capito perche` ci va
		}
	}
	
	if($j==$group or $i==$imax){
		if($i==$imax){
			print $TMP $_;
		}
		
		my $cmd=$comando;
		$cmd=~s/([^\\])@/$1$filename/g;
		$cmd=~s/([^\\])\%/$1$id/g;
		print $cmd,"\n";
		print `$cmd`;
		if(!$not_exit_on_error){
			$? and die $?;
		}
		if($remove){
			unlink $filename;
		}
		close $TMP;
		
		$id=$_;
		chomp $id;
		$id=~s/^>//;
		$id=~s/\s.*$//;
		$filename=$tmp_dir.'/'.$id;
		$filename=~s/\(//g;
		$filename=~s/\)//g;
		chomp $filename;
		$filename.='.fa';
		
		if($i!=$imax){
			open $TMP, ">$filename" or die("Can't write file ($filename)");
		}
		
		$j=0;
		
	}
	
	if(defined($filename) and $i!=$imax){#$filename non e` definito solo per eventuali linee prima di un qualche header >id
		print $TMP $_;
	}
	
	$i++;
}
