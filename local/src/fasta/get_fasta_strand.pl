#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;

my $usage="$0 < file.fa";


my $help=0;
my $glue=",";
GetOptions (
	'h|help' => \$help,
	'g|glue=s' => \$glue,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my $strand = undef;
my $seq = undef;
while(<>){
	if (m/^>/){
		if(defined($seq)){
			$seq = reverse $seq;
			$seq =~ tr/ACGT/TGCA/;
			print $seq;
			$seq="";
		}
		my @F = split /\t/,$_,-1;
		if($F[3]=~/-/){
			$strand=-1
		}else{
			$strand=1
		}
	}
	if(!m/^>/){
		chomp;
		$seq.=$_
		$_ .= "\n";
	}
}
if(defined($seq)){
	$seq = reverse $seq;
	$seq =~ tr/ACGTacgt/TGCAtgca/;
	print $seq;
}
