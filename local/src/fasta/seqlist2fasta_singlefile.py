#!/usr/bin/env python
# encoding: utf-8

from itertools import groupby
from cStringIO import StringIO
from optparse import OptionParser
from os.path import isdir, isfile, join
from sys import exit, stdin, stderr
from vfork.fasta.reader import MultipleBlockReader, FormatError
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage

def parse_line(line, lineno, gen_id, report_coordinates, report_all):
	tokens = safe_rstrip(line).split('\t')

	expected_token_num = 4
	if gen_id:
		expected_token_num -= 1
	if not report_all and expected_token_num != len(tokens):
		raise ValueError, 'wrong token number (expected %d, found %d)' % (expected_token_num, len(tokens))
	
	if gen_id:
		tokens.insert(0, str(lineno))
	
	try:
		start = int(tokens[2])
		if start < 0:
			raise ValueError
	except ValueError:
		raise ValueError, 'invalid start coordinate'
	
	try:
		stop = int(tokens[3])
		if stop <= start:
			raise ValueError
	except ValueError:
		raise ValueError, 'invalid stop coordinate'

	return tokens[0], tokens[1], start, stop, tokens[4:]


def read_coords_file(options):
	last_seq_id = None
	for lineno, line in enumerate(stdin):
		try:
			tokens = parse_line(line, lineno, options.gen_id, options.report_coordinates, options.report_all)
		except ValueError, e:
			exit('Malformed input at line %d: %s\n%s' % (lineno+1, e.args[0], line))
		if last_seq_id != None and tokens[1] < last_seq_id:
			exit('Input not sorted on seq_id at line %d: %s.' % (lineno+1))
		yield tokens

def main():
	parser = OptionParser(usage='%prog SEQUENCE_FILE <SEQLIST >FASTA')
	parser.add_option('-i', '--gen-id', dest='gen_id', action='store_true', help='automatically generate IDs for sequences')
	parser.add_option('-o', '--append-offset', dest='append_offset', action='store_true', help='append the sequence start offset to the FASTA header')
	parser.add_option('-r', '--report', dest='report_coordinates', action='store_true', help='report original coordinates in headers')
	parser.add_option('-s', '--separator', dest='separator', default='\t', help='the character used to separate values in headers', metavar='CHAR')
	parser.add_option('-a', '--report_all', dest='report_all', action='store_true', help='report all the row in fasta header')
	options, args = parser.parse_args()

	if len(args) != 1:
		exit('Unexpected argument number.')
	elif options.append_offset and options.report_coordinates:
		exit('--append-offset and --report are mutually exclusive.')
	elif len(options.separator) != 1:
		exit('Invalid separator character.')
	
	try:
		reader = MultipleBlockReader(args[0])
	except FormatError:
		exit('Malformed FASTA file %s.' % args[0])
	
	for seq_id, coords_iter in groupby(read_coords_file(options), lambda tokens: tokens[1]):
		try:
			block = reader[seq_id]
		except KeyError:
			exit('Invalid seqence id %s' % seq_id)

		for id, seq_id, start, stop, all in coords_iter:
			sequence = block[start:stop]

			label = id
			if options.append_offset:
				label =+ '_%d' % start
			elif options.report_coordinates:
				label = options.separator.join((label, seq_id, str(start), str(stop)))
			elif options.report_all:
				label = options.separator.join((label, seq_id, str(start), str(stop), options.separator.join(all)))
				
				
		
			print '>%s' % label
		
			for pos in xrange(0, len(sequence), 80):
				print sequence[pos:pos+80]

	reader.close()


if __name__ == '__main__':
	main()
