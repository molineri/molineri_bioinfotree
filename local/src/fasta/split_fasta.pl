#!/usr/bin/perl
use POSIX;
use File::Basename;
use Getopt::Long;
use strict;
use warnings;

my $usage="usage: $0 [-e] input_filename n output_dir
produce n fasta file in the output_dir 
the files are named filename.1 ... filename.n, each containing a slice of the original filename

	-e 	produce a output file for each fasta block, ignore the n param
";

my $each_block = 0;
my $help=0;

GetOptions (
	'h|help' => \$help,
	'e|each_block' => \$each_block,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my $filename=shift @ARGV;
open FH, $filename or die("Can't open file ($filename)\n$usage");
$filename=basename($filename);

my $n=shift @ARGV;
die("ERROR: second argument must be a valid number\n$usage") if $n !~/\d+/ and not $each_block;

my $out_dir=shift @ARGV;
die("ERROR: invalid output directory ($out_dir)\n$usage") if !-d $out_dir;


my $header_per_splice =1;
if(not $each_block){
	my $header_n=`grep '>' $filename | wc -l`;
	chomp($header_n);
	$header_n =~ s/^\s+//;
	$header_per_splice = ceil($header_n/$n);
}


my $out_filename=undef;
my $j=0;
my $i=0;
while(<FH>){
	if(m/>/){
	
		$i++;
		if($i>=$header_per_splice or !defined($out_filename)){
			$i=0;
			$j++;
			
			close OUT if defined($out_filename);
			
			if(not $each_block){
				$out_filename =  $filename;
				$out_filename =~ s/\.fa$//;
				$out_filename .= ".$j.fa";
			}else{
				m/^>([^\s]+)/;
				$out_filename = "$out_dir/$1";
			}

			die("ERROR: $out_filename allready exist") if(-e $out_filename);

			open OUT, ">$out_filename" or die("Can't write file ($out_filename)");
			next if $each_block # in questo caso non scrivo l'header
		}
	}

	die("ERROR: empty line before firs header") if !defined($out_filename);
	
	print OUT;
	
}
