#!/usr/bin/env python
from errno import EPIPE
from optparse import OptionParser
from os import environ
from subprocess import Popen, PIPE
from sys import exit, stdin
from vfork.util import exit, format_usage
import re 

class Child(object):
	def __init__(self, cmd):
		self.cmd = cmd
		self.serial = 0
		self.child = None
	
	def start(self, header):
		self.serial += 1
		cmd = self.cmd.replace('$blocknum', str(self.serial))
		cmd = cmd.replace('$QUOTED_HEADER', "'%s'" % header.replace("'", r"'\''"))
		cmd = cmd.replace('$HEADER', header[1:].split(None, 1)[0])
		self.child = Popen([environ.get('SHELL', 'bash'), '-c', cmd], shell=False, stdin=PIPE)
	
	def stop(self):
		if self.child is None:
			return
		
		self.child.stdin.close()
		res = self.child.wait()

		self.child = None
		if res != 0:
			raise ChildError, "'%s' exited with error %d" % (self.cmd, res)
	
	def write(self, data):
		if self.child is not None:
			self.child.stdin.write(data)

class ChildError(Exception): pass

def main():
	parser = OptionParser(usage='%prog COMMAND <FASTA')
	parser.add_option('-g', '--group', dest='group', type='int', default=1, help='process GROUP fasta bloks at a time', metavar='GROUP')
	parser.add_option('-n', '--no_head', action="store_true", dest='no_head', default = False, help='don\'t print header line in stdin of COMMAND for each fasta block, $HEADER in the COMMAND is always replaced with the header (quoted)', metavar='GROUP')
	options, args = parser.parse_args()
	if len(args) != 1:
		exit('Unexpected argument number.')

	try:
		child = Child(args[0])
		count = options.group
		
		for line in stdin:
			if line[0] == '>':
				count += 1
				if count > options.group: 
					child.stop()
					child.start(line.rstrip())
					count = 1
				
			try:
				if not options.no_head or  line[0] != '>':
					child.write(line)
			except IOError, e:
				if e.errno == EPIPE:
					child.stop()
				else:
					raise ChildError, e.args[0]
		
		child.stop()
	
	except ChildError, e:
		exit('Fatal error: %s.' % e.args[0])

if __name__ == '__main__':
	#import pdb
	#pdb.run('main()')
	main()

