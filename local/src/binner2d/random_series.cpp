# include <iostream>
# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <sys/time.h>
# include <getopt.h>
# include <string.h>


using namespace std;

static const char *usage = "random_series -m 0 -M 10 -l inf [-i]\nthis example produce a random series of number in [0..10)\nif -i is given number are integer and max is included in output set\n";

int main (int argc, char *argv[], char *envp[]){
	
	char *min;
	char *max;
	bool integer=false;
	int length=-1;

	//gestione dei parametri da linea di comando
	//
	//
	bool max_set=false;
	bool min_set=false;
	bool length_set=false;
	int c;
	while (c = getopt(argc, argv, "m:M:l:i")){
		if (c == -1) break;

		switch (c) {
			case 'm':
				min=optarg;
				min_set=true;
				break;
			case 'M':
				max=optarg;
				max_set=true;
				break;
			case 'i':
				integer=true;
				break;
			case 'l':
				if(strcmp(optarg,"inf")){
					length=atoi(optarg);
					if(length==0){
						cerr<<"ERROR: -l optarg must be an integer >0 or \"inf\""<<endl;
						exit(-1);
					}
				}
				length_set=true;
				break;
			default:
				printf ("ERROR: unknow option (%s) \n %s", c, usage);
				exit(-1);
		}
	}
	if (optind < argc) {
		printf ("ERROR: unsupported non-option ARGV-elements (%s)\n%s",argv[optind++],usage);
		exit(-1);
		/*while (optind < argc){
			printf ("%s ", argv[optind++]);
		}*/
	}
	
	if(!min_set || !max_set || !length_set){
		cerr<<"ERROR: -m, -M and -l option required" << endl << usage << endl;
		exit(-1);
	}
	//
	//
	// fine controllo parametri da linea di comando
	
	unsigned long int seed=0;	
	struct timeval time;
	struct timezone timez;
    	int rc;
    	rc=gettimeofday(&time, &timez);
	seed=time.tv_sec+time.tv_usec;
	//cerr<<seed<<endl;
	//exit(-1);

	srand48(seed);
	int i=0;	
	if(!integer){
		double rand_min=atof(min);
		double rand_max=atof(max);
		
		while(1){
			if(length > 0 && length <= i) return(0);
			double rand_num=drand48();
			cout << (rand_num*(rand_max - rand_min) + rand_min) << endl;
			length > 0 && i++;
		}
	}else{
		int rand_min=atoi(min);
		int rand_max=atoi(max);
		while(1){
			if(length > 0 && length <= i) return(0);
			double rand_num=drand48();
			rand_num = (rand_num*(rand_max - rand_min) + rand_min);
			int rand_num_int=(int)rand_num;
			if(rand_num - rand_num_int< 0.5){
				cout << rand_num_int;
			}else{
				cout << rand_num_int + 1;
			}
			cout << endl;
			length > 0 && i++;
		}
	}
	return(0);
}
