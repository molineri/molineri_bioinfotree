#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <math.h>
#include <unistd.h>
#include <string>
#include "Histogram2d.h"
#include "StringTokenizer.h"



using namespace std;


int setup(int argc, char* argv[]);
void usage(char** argv);

bool options_xlog=false;
bool options_xmin=false;
bool options_xmax=false;
bool options_xbin_n=false;
float opt_xmin, opt_xmax; 
int opt_xbin_n;
bool options_ylog=false;
bool options_ymin=false;
bool options_ymax=false;
bool options_ybin_n=false;
float opt_ymin, opt_ymax; 
int opt_ybin_n;


int main( int argc, char* argv[] ){

	//controllo le opzioni passate da riga di comando
	if(setup(argc, argv)){
		cout << "main:: termined because setup error"<<endl;
		return(-1);
	}

	// carico i valori nell'istogramma
	Histogram2d *Hist = NULL;
	Hist = new Histogram2d(opt_xmin, opt_xmax, opt_xbin_n, options_xlog, opt_ymin, opt_ymax, opt_ybin_n, options_ylog);

	char line[256];
	float x,y;
	while(cin.getline (line,256)) {
	   	StringTokenizer strtok = StringTokenizer(line,"\t",false);
		x = strtok.nextFloatToken();
		y = strtok.nextFloatToken();
		Hist->add(x,y);
	}

	Hist->print();
}


int setup(int argc, char* argv[]){
	
	////////////////////////////////////////////////////////////////
	// gestione delle opzioni
	//
	int i;
	while((i=getopt(argc, argv, "i:u:ln:I:U:LN:"))!=-1){
		switch(i){
			case 'i':
				opt_xmin=atof(optarg);
				options_xmin=true;
				break;
			case 'u':
				opt_xmax=atof(optarg);
				options_xmax=true;
				break;
			case 'l':
				options_xlog=true;
				break;
			case 'n':
				opt_xbin_n=atoi(optarg);
				options_xbin_n=true;
				if (opt_xbin_n <= 1) {
					cerr<<"n must be > 1"<<endl;
					return -1;
				}
				break;
			case 'I':
				opt_ymin=atof(optarg);
				options_ymin=true;
				break;
			case 'U':
				opt_ymax=atof(optarg);
				options_ymax=true;
				break;
			case 'L':
				options_ylog=true;
				break;
			case 'N':
				opt_ybin_n=atoi(optarg);
				options_ybin_n=true;
				if (opt_ybin_n <= 1) {
					cerr<<"N must be > 1"<<endl;
					return -1;
				}
				break;
				
			default:
				cerr<<"wrong options given (" << i << ")" <<endl;
				usage(argv);
				return -1;
		}
	}
	if (!options_xmin or !options_xmax or !options_xbin_n or !options_ymin or !options_ymax or !options_ybin_n) {
		cerr<<"wrong options given"<<endl;
		usage(argv);
		return -1;
	}

	return(0);
}


void usage(char** argv){
	cout << "usage: " << argv[0] <<  " -i 0 -u 5000 -n 200 [-l] -I 10 -U 50 -N 100 [-L] < filename"<<endl
		<<"\t-i minimum x value"<<endl
		<<"\t-u maximum x value"<<endl
		<<"\t-n x bins number"<<endl
		<<"\t-l to set logscale binning on x axes"<<endl
		<<"\t-I minimum y value"<<endl
		<<"\t-U maximum y value"<<endl
		<<"\t-N y bins number"<<endl
		<<"\t-L to set logscale binning on y axes"<<endl;
	exit(-1);
}

