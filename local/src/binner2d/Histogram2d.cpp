#include "Histogram2d.h"
#include <math.h>
#include <tnt.h>

using namespace TNT;

Histogram2d::Histogram2d(float xmin, float xmax, int xbin_n, bool xl, float ymin, float ymax, int ybin_n, bool yl) : \
	count(xbin_n, ybin_n, 0)
{
	//this->count = new Array2D< int > (xbin_n,ybin_n,0);
	this->xl = xl;
	if (xmin >= xmax) {
		cerr<<"error: Histogram2d::Histogram2d: xmin >= xmax"<<endl;
		exit(-1);
	}
	if (this->xl){
		if (xmin <= 0) {
			cerr<<"error: Histogram2d::Histogram2d: logscale required but xmin <= 0 ("<< xmin <<")"<<endl;
			exit(-1);
		}
		this->xmin = log10(xmin);
		this->xmax = log10(xmax);
	} else {
		this->xmin = xmin;
		this->xmax = xmax;
	}

	this->xtot_span = this->xmax-this->xmin;
	this->xspan = this->xtot_span/xbin_n;
	this->xbin_n = xbin_n;
		

	this->yl = yl;
	if (ymin >= ymax) {
		cerr<<"error: Histogram2d::Histogram2d: ymin >= ymax"<<endl;
		exit(-1);
	}
	if (this->yl){
		if (ymin <= 0) {
			cerr<<"error: Histogram2d::Histogram2d: ymin <= 0"<<endl;
			exit(-1);
		}
		this->ymin = log10(ymin);
		this->ymax = log10(ymax);
	} else {
		this->ymin = ymin;
		this->ymax = ymax;
	}

	this->ytot_span = this->ymax-this->ymin;
	this->yspan = this->ytot_span/ybin_n;
	this->ybin_n = ybin_n;
		
}

void Histogram2d::add(float fx, float fy){
	int xbin, ybin;

	if (this->xl){
		if (fx <= 0) {
			cerr<<"error: Histogram2d::add: fx <= 0"<<endl;
			exit(-1);
		}
		fx = log10(fx);
	}
	if(fx!=this->xmax){
		xbin = (int)((fx - this->xmin) / this->xspan);
	}else{
		xbin = xbin_n - 1;
	}

	if (this->yl){
		if (fy <= 0) {
			cerr<<"error: Histogram2d::add: fy <= 0"<<endl;
			exit(-1);
		}
		fy = log10(fy);
	}
	if(fy!=this->ymax){
		ybin = (int)((fy - this->ymin) / this->yspan);
	}else{
		ybin = ybin_n - 1;
	}

	count[xbin][ybin]++;
}

void Histogram2d::print(){
	for(int i=0; i<this->xbin_n; i++){
		for(int j=0; j<this->ybin_n; j++){
			float xleft=i * this->xspan + this->xmin;
			float yleft=j * this->yspan + this->ymin;
			if (this->xl){
				xleft=exp(xleft * log(10));
			}
			if (this->yl){
				yleft=exp(yleft * log(10));
			}
			cout << xleft << "\t" << yleft << "\t" << this->count[i][j] << endl; 
		}
	}
}

