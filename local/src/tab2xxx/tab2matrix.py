#!/usr/bin/env python
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from array import array
from optparse import OptionParser
from sys import stdin
from vfork.io.colreader import Reader
from vfork.util import exit, format_usage

class SparseMatrix(object):
	def __init__(self, cols_item=set()):
		self.data = {} 
		self.cols_item = cols_item
		self.cols_item_sorted = []
	
	def set(self, x, y, v):
		pos = self.data.get(x,{})
		if y in pos:
			exit("only one value per position allowed")
		pos[y]=v
		self.data[x]=pos
		self.cols_item.add(y)

	def rows(self):
		for i in self.data.iterkeys():
			retval = [i]
			for j in self.cols_item_sorted:
				retval.append(self.data[i].get(j,None))
			yield retval

	def head(self):
		self.cols_item_sorted = sorted(list(self.cols_item))
		return ">ROW_ID\t%s" % "\t".join(self.cols_item_sorted)

def None2NA(v):
	if v is None:
		return "NA"
	else:
		return v;

def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS]
		
		.META: stdin
			1 id1
			2 id2
			3 val

		Builds a weighted incidence matrix out of a map file.
	'''))
	parser.add_option('-c', '--columns', type=str, dest='columns', default=None, help='space separed list of labels, if given produce a value (NA) also for label in COLUMNS, other than those found in the input file [default: %default]', metavar='COLUMNS')
	options, args = parser.parse_args()

	if len(args) != 0:
		exit('Unexpected argument number.')
	
	if options.columns:
		columns=options.columns.split()
		matrix = SparseMatrix(set(columns))
	else:
		matrix = SparseMatrix()
		
	for line in stdin:
		try:
			x,y,v = line.rstrip().split("\t")
		except ValueError:
			raise Exception("Malformed input (%s)" % line)
		matrix.set(x, y, v)
	
	print matrix.head()
	for r in matrix.rows():
		print '\t'.join( [None2NA(v) for v in r] )

if __name__ == '__main__':
	main()
