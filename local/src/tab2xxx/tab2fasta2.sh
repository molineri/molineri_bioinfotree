#!/bin/bash

if [ $# -gt 0 ]; then
	echo "usage: $0 < TABFILE";
	echo "Convert a tab delimited file in a fasta file.";
	echo "The first column is used to group the lines, each line whit";
	echo "the same value in the first column in input is reported in";
	echo "the same fasta block in output.";
	echo "The header of the block is the value of the first column.";
	echo "The first column is not reported in the fasta blocks.";
	exit -1;
fi;

repeat_group_pipe -s 'echo ">$1"; cut -f 2-' 1
