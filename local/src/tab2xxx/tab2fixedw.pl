#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] [-space S]]

convert a tab delimited file into a fixed width file.
Each column in the STDOUT is separed by at least S spaces.
\n";

my $help=0;
my $space=1;
GetOptions (
	'h|help' => \$help,
	'g|space=i' => \$space,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my %l=();
my @rows=();
while(<>){
	chomp;
	my @F = split /\t/,$_,-1;
	push(@rows,\@F); 
	my $i=0; 
	for(@F){ 
		$l{$i} = length if !defined $l{$i} or length > $l{$i};
		$i++
	} 
}

foreach my $r (@rows){
	my $i=0; 
	for(@{$r}){ 
		my $w= $l{$i};
		$w += $space if $i>0; 
		printf('%'.$w.'s', $_); 
		$i++
	}
	print "\n"
}
