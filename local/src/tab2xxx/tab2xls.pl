#!/usr/bin/perl

use strict;
use warnings;
use Spreadsheet::WriteExcel;
use Data::Dumper;
use Getopt::Long;

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] [-f|fasta] < in.tab_separed > out.xls\n
	-f|fasta	one sheet per fasta block
";

my $help=0;
my $fasta=0;
GetOptions (
	'h|help' => \$help,
	'f|fasta' => \$fasta,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my $dest_book  = Spreadsheet::WriteExcel->new('/dev/stdout');
my $dest_sheet = $dest_book->addworksheet() if not $fasta;
my $i=0;
while(<>){
	if($fasta and s/^>//){
		$dest_sheet = $dest_book->addworksheet($_);
		$i=0;
		next;
	}
	my @F = split(/\t/);
	my $j=0;
	for(@F){
		$dest_sheet->write($i, $j,$_);
		$j++;
	}
	$i++
}


$dest_book->close();
