#!/usr/bin/perl

$,="</td><td>"; 
$\="\n";

print '<html><head><style>td {background: #CCCCCC;} </style><title>table</title></head><body><table cellspacing="2">';
while(<>){
	chomp;
	my @F=split /\t/;
	print "<tr>";
	for(@F){
		my $class = m/^\d+(\.\d+)?([eE][+-]\d+)?$/  ? ' class="number"' : "";
		print "\t<td$class>$_</td>";
	}
	print "</tr>";
}
print "</body></html>";
