#!/usr/bin/env python

from itertools import izip
from optparse import OptionParser
from sys import stdin
from vfork.util import exit, format_usage, ignore_broken_pipe

def load_data(fd):
	data = None
	colwidths = None
	
	for idx, line in enumerate(fd):
		tokens = tuple(line.rstrip().split('\t'))
		if data is None:
			data = [tokens]
			colwidths = [ len(t) for t in tokens ]
		elif len(tokens) != len(colwidths):
			exit('Invalid column number at line %d:\n%s' % (idx+1, line.rstrip()))
		else:
			data.append(tokens)
			colwidths = [ max(a,b) for a,b in izip((len(t) for t in tokens), colwidths) ]
	
	return data, colwidths

def build_format(colwidths, sep):
	parts = []
	if len(colwidths):
		for w in colwidths[:-1]:
			parts.append('%%- %ds' % (w+sep,))
		parts.append('%s')
	return ''.join(parts)

def main():
	usage = format_usage('''
		%prog OPTIONS <TSV
		
		Reformats the input aligning table cells with spaces.
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-s', '--separation', dest='separation', type='int', default=2, help='separation between cells (by default: 2)', metavar='SPACES')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	elif options.separation < 0:
		exit('Invalid separation value: %s' % options.separation)
	
	data, colwidths = load_data(stdin)
	fmt = build_format(colwidths, options.separation)
	for tokens in data:
		print fmt % tokens

if __name__ == '__main__':
	ignore_broken_pipe(main)
