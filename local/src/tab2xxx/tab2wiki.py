#!/usr/bin/env python

from optparse import OptionParser
from sys import exit, stdin
from vfork.io.util import safe_rstrip
import re

double_spaces_rx = re.compile(r'\s\s+')
def convert_double_spaces(line):
	return double_spaces_rx.sub('\t', line)

def main():
	parser = OptionParser()
	parser.add_option('-d', '--convert-double-spaces', dest='convert_double_spaces', action='store_true', default=False, help='convert groups of at least two spaces into tabs')
	options, args = parser.parse_args()

	if len(args) != 0:
		exit('Unexpected argument number.')
	
	for line in stdin:
		line = safe_rstrip(line)
		if options.convert_double_spaces:
			line = convert_double_spaces(line)

		print '||%s||' % ('||'.join(line.split('\t')))

if __name__ == '__main__':
	main()
