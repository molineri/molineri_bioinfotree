#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = $0.' [-h|help] [-s SPECIES_LIST]] < FILE.maf\n
	to get the species list form a maf file:
	perl -lane \'print if m/^s/ or m/^e/;\' K562_maf/16665.maf | awk \'{print  $2}\' | grep . |  sed \'s/\..*//\' | perl -nae \'chomp; $a{$_}++; print $_ . " " if $a{$_}==1\'
';

my $help=0;
my $species_opt=0;
GetOptions (
	'h|help' => \$help,
	's|spoecies=s' => \$species_opt,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my @species_list = undef;
die($usage) if not $species_opt;
@species_list = split(/\s+/,$species_opt) if $species_opt;

my $first_of_block = 0;
my $block_len = undef;
my %species_seq=();
my %species_in_block=();
for(@species_list){
	$species_in_block{$_}=0;
}
my $last_row="";
while(<>){
	$last_row = $_;
	chomp;
	if(m/^\s*$/ or m/^##/){#inizio di un blocco
		$first_of_block=1;
		if($.>1){#sempre tranne alla prima riga, faccio qualcosa per gestire la fine del blocco, se sono alla prima riga non ho terminato alcun blocco
			foreach my $s (keys(%species_in_block)){
				if($species_in_block{$s}==0){
					$species_seq{$s}.='-'x$block_len;
				}
				$species_in_block{$s}=0;#resetto il contatore affinche` possa essere usato al blocco successivo
			}
		}
		next;
	}
	next if not (m/^s/ or m/^e/);
	my @F = split /\s+/,$_,-1;
	
	# colonne del maf
	#0	s
	#1	hg19.chr10
	#2	30719994
	#3	65
	#4	+
	#5	135534747
	#6	gggtgtggtggcatgcgcctgtagttccagctatctgggaggctgaggtgggaggatcacttgag
	
	
	if($first_of_block){
		$block_len = length($F[6]);
		$first_of_block=0;
	}

	my $species = $F[1];
	$species =~ s/\..*//;
	$species_in_block{$species}++;

	if(m/^s/){
		$species_seq{$species}.=$F[6];
	}else{
		$species_seq{$species}.="-" x $block_len;
	}
}

if($last_row!~/^\s+$/){
	foreach my $s (keys(%species_in_block)){
		if($species_in_block{$s}==0){
			$species_seq{$s}.='-'x$block_len;
		}
		$species_in_block{$s}=0;#resetto il contatore affinche` possa essere usato al blocco successivo
	}
}

if($species_opt){
		for(@species_list){
			print $_,$species_seq{$_}
		}
}
