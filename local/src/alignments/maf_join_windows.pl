#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] -l LEN -o OVERLAP < FILE.maf_join\n

	-l and -o options are required

	.META: stdin
		1	species_id
		2	gapped_sequence
";

my $help=0;
my $len=undef;
my $ovp=undef;
GetOptions (
	'h|help' => \$help,
	'l|len=i' => \$len,
	'u|overlap=i' => \$ovp,
) or die($usage);

if(not defined($len) or not defined($ovp)){
	die($usage);
}

if($help){
	print $usage;
	exit(0);
}

my %seq = ();
my @species=();

while(<>){
	chomp;
	my @F = split /\t/,$_,-1;
	$seq{$F[0]}=$F[1];
	push @species,$F[0];
}

my %out=();
my $coords_post =0;
my $cur_seq="";
while(my $cur_len = length($seq{$species[0]}) > 0){
	my $coords_added=0;
	my $coords = $coords_post;
	for(@species){
		my $overlap = "";
		if($cur_len>$len){
			$overlap =  substr $seq{$_}, $len-$ovp, $ovp;
		}

		my $cur_seq = substr $seq{$_}, 0, $len, $overlap;

		print $coords, $_, $cur_seq;

		if(not $coords_added){
			$cur_seq =~ s/-//g;
			$coords_post = $coords + length($cur_seq) - $ovp;
			$coords_added=1;
		}
	}
}
