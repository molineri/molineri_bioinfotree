#!/usr/bin/env python
# encoding: utf-8

from __future__ import with_statement

from fcntl import lockf, LOCK_EX, LOCK_UN
from memwatch2 import MemoryUsageSampler
from optparse import OptionParser
from os import getcwd, getpid, system
from os.path import basename, join
from sys import argv, exit, stdout
from time import time

def iter_commands(args, single):
	if single:
		yield ' '.join(args)
	else:
		with file(args[0], 'r') as fd:
			for line in fd:
				yield line.rstrip()

def write_report(elapsed_time, max_resident, max_shared, exit_code, cmd, fd):
	print >>fd, '\t'.join(str(v) for v in (int(elapsed_time), max_resident, max_shared, exit_code, cmd))

def main():
	parser = OptionParser(usage='%prog COMMAND_SCRIPT')
	parser.add_option('-l', '--mem-limit', dest='mem_limit', type='int', default=800*1024, help='enforce an upper limit on commands memory usage (by default 800Mb)', metavar='KBYTES')
	parser.add_option('-o', '--output', dest='output', default=None, help='append output atomically to FILE', metavar='FILE')
	parser.add_option('-q', '--use-queue', dest='queue', default=None, help='use a cluster queue', metavar='QUEUE_TYPE')
	parser.add_option('-s', '--single', dest='single', action='store_true', default=False, help='run a single command; used internally')
	options, args = parser.parse_args()
	
	if (options.single and len(args) < 1) or (not options.single and len(args) != 1):
		exit('Unexpected argument number.')
	elif options.queue is not None and options.output is None:
		exit('The option --use-queue requires --output.')
	
	prog_name = basename(argv[0])
	if options.output is not None:
		options.output = join(getcwd(), options.output)
	
	for serial, cmd in enumerate(iter_commands(args, options.single)):
		if options.queue is None:
			memory_usage = MemoryUsageSampler(5)
			start_time = time()
			max_resident = None
			max_shared = None
			
			for resident, shared in memory_usage.sample(('/bin/bash', '-c', 'ulimit -v %d; %s' % (options.mem_limit, cmd))):
				if max_resident is None or resident > max_resident:
					max_resident = resident
				if max_shared is None or shared > max_shared:
					max_shared = shared
			
			if options.output is None:
				fd = stdout
			else:
				fd = file(options.output, 'a')
				lockf(fd, LOCK_EX)
			
			write_report(time()-start_time, max_resident, max_shared, memory_usage.exit_code, cmd, fd)
			
			if options.output is not None:
				fd.flush()
				lockf(fd, LOCK_UN)
				fd.close()
		
		elif options.queue == 'accoda':
			job_id = 'bch_%d_%d' % (getpid(), serial)
			res = system("accoda --name %s -- %s --single --output='%s' '%s'" % (job_id, prog_name, options.output, cmd))
			if res != 0:
				exit('Error running accoda.')
		
		else:
			exit('Unsupported queue type.')

if __name__ == "__main__":
	main()
