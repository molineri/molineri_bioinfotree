#!/usr/bin/perl

use warnings;
use strict;
$,="\t";
$\="\n";

use Getopt::Long;

my $usage="$0 [-h] [-s sleep_secs] [-i pid] [-n process_name]\n";

my $sleep_secs=1;
my $pid=undef;
my $process_name=undef;
my $human_readable=0;

GetOptions (
	'human_readable|h' => \$human_readable,
	'sleep_secs|s=i' => \$sleep_secs,
	'pid|i=i' => \$pid,
	'process_name|n=s' => \$process_name
);

$pid or $process_name or die($usage."-i pid or -n name required\n");
$pid and $process_name and die($usage."-i pid AUT -n name\n");

defined($pid) or  $pid = `pgrep $process_name`;
chomp($pid);
if($pid=~/\n/){
	print STDERR "WARNING: there are more than one proces with name $process_name\n";
	$pid=~m/^(\d+)\n/;
	$pid=$1;
}
$pid or die("ERROR: proces not found ($process_name)\nwith `pgep $process_name`");

while(1){
	my $info1=`ps -o rss,vsz h $pid`;
	$? and last;
	
	$info1=~s/^\s+//;
	chomp($info1);
	my @tmp =  split(/\s+/,$info1);
	
	my @F=($tmp[0],$tmp[1]);
	
	my $info2=`free | grep ^Swap | awk '{print \$3}'`;
	chomp($info2);
	
	$F[2] = $info2;
	
	
	if($human_readable){
		@F= map{ 
			if($_>1000000){
				$_=sprintf '%.2fG', $_/1000000;
			}elsif($_>1000){
				$_ = sprintf '%.2fM', $_/1000;
			}else{
				$_+="K";
			}
		} @F;
	}
	
	print @F;
	
	sleep($sleep_secs);
}
