from __future__ import with_statement

from glob import glob
from os.path import join, isfile
from sys import exit

def iter_sequences(seqdir, min_size, max_size, steps):
	idx = load_indexes(seqdir)
	for size in xrange(min_size, max_size, (max_size - min_size) / steps):
		yield get_block_by_size(idx, size)

def load_indexes(seqdir):
	idx = []
	for splitted in glob(join(seqdir, '*.splitted')):
		index_path = splitted + '.idx'
		if isfile(index_path):
			idx += load_index(index_path)
	
	idx.sort()
	return idx

def load_index(path):
	idx = []
	with file(path, 'r') as fd:
		for lineno, line in enumerate(fd):
			tokens = line.rstrip().split()
			if len(tokens) != 4:
				exit('Invalid index entry at line %d of file %s' % (lineno+1, path))
			
			try:
				for i in xrange(1, 4):
					tokens[i] = int(tokens[i])
			except ValueError:
				exit('Invalid index entry at line %d of file %s' % (lineno+1, path))
			
			yield (tokens[1], path, tokens[0], tokens[2], tokens[3])

def get_block_by_size(idx, size):
	best_delta = None
	best_entry = None
	
	for entry in idx:
		delta = abs(entry[0] - size)

		if best_delta is None or delta <= best_delta:
			best_delta = delta
			best_entry = entry
		else:
			return best_entry

if __name__ == '__main__':
	for s in iter_sequences('/Users/border/prj/bioinfo/task/sequences/dataset/ensembl/mmusculus/43/', 100, 1000000, 100):
		print s
