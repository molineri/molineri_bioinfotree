#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] COL_1 COL_2< input_file\n
COL_1 and COL_2 designate vertex id, the values are swapped if COL_1>COL_2";

my $help=0;
GetOptions (
	'h|help' => \$help,
) or die($usage);

if($help){
	print STDERR $usage;
	exit(0);
}

my $col1=shift;
my $col2=shift;
die($usage) if !defined $col1 or !defined $col2 or $col1 !~/^\d+$/ or $col2 !~ /^\d+$/;
$col1--;
$col2--;


while(<>){
	chomp;
	my @F = split /\t/;
	if ($F[$col1] =~ /^\d+?/ && $F[$col2] =~ /^\d+?/) {
		if($F[$col1] > $F[$col2]){
			my $tmp = $F[$col1];
			$F[$col1]=$F[$col2];
			$F[$col2]=$tmp;
		}
		print @F;
	} else {
		if($F[$col1] gt $F[$col2]){
			my $tmp = $F[$col1];
			$F[$col1]=$F[$col2];
			$F[$col2]=$tmp;
		}
		print @F;
	}
}
