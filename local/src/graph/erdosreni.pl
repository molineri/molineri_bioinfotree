#!/usr/bin/perl

use warnings;
use strict;
$,="\t";
$\="\n";

my $n = shift;
my $p = shift;

die() if $p > 1 or $p < 0;

for(my $i=0; $i<$n; $i++){
	for(my $j=$i+1; $j<$n; $j++){
		print $i,$j if rand() <= $p;
	}
}
