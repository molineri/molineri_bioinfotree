#!/usr/bin/env python
from igraph import Graph
from sys import exit, stdin, argv

class VertexLog(object):
	def __init__(self):
		self.map = {}
		self.popitem = self.map.popitem
		self.free_id = 0
		self.created_ids = 0
	
	def __getitem__(self, key):
		try:
			return self.map[key]
		except KeyError:
			id = self.free_id
			self.free_id += 1
			self.created_ids += 1

			self.map[key] = id
			return id
	
	def apply(self, graph):
		graph.add_vertices(self.created_ids)
		self.created_ids = 0
	
class EdgeLog(object):
	def __init__(self, vertex_log):
		self.vertex_log = vertex_log
		self.edges = []
	
	def add_edge(self, vertex_from, vertex_to):
		self.edges.append((self.vertex_log[vertex_from], self.vertex_log[vertex_to]))
	
	def apply(self, graph):
		self.vertex_log.apply(graph)
		graph.add_edges(self.edges)
		self.edges = []

def invert_vertex_map(vertex_log):
	try:
		res = {}
		while True:
			value, key = vertex_log.popitem()
			res[key] = value
	except KeyError:
		return res

def input_iterator(fd):
	try:
		for lineno, line in enumerate(fd):
			yield line.split(None)[:2]
	except ValueError:
		exit('Invalid input at line %d: %s' % (lineno+1, line.rstrip()))

def load_edges(input_iter, graph):
	vertex_log = VertexLog()
	edge_log = EdgeLog(vertex_log)

	for vertex_from, vertex_to in input_iter:
		edge_log.add_edge(vertex_from, vertex_to)
	
	edge_log.apply(graph)

	return invert_vertex_map(vertex_log)	

if __name__ == '__main__':
	if len(argv) > 1:
		iter = input_iterator(file(argv[1], 'r'))
	else:
		iter = input_iterator(stdin)
	
	graph = Graph()
	graph.delete_vertices(0)
	vertex_map = load_edges(iter, graph)
	
	for idx, comp_no in enumerate(graph.clusters()):
		print '%d\t%s' % (comp_no, vertex_map[idx])
