#!/usr/bin/perl

use warnings;

my %conn;
while(<>){
	chomp;
	(my $a, my $b)=split;
	$conn{$a}++;
	$conn{$b}++;
}

while(($key,$val)=each(%conn)){
	print "$key\t$val\n";
}
