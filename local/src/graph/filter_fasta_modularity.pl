#!/usr/bin/perl
use warnings;
use strict;

my $print=0;
while(<>){
	if(m/^>/){
		$print =0;
		my ($id,$mod)=split;
		$print = 1 if $mod>0.5;
		print;
		next;
	}
	if($print){
		print;
	}else{
		chomp;
		my ($id) = split;
		print "$id\t0\n";
	}
}
