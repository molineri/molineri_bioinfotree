#!/usr/bin/env python
#
# Copyright 2007-2008 Ivan Molineris <ivan.molineris@gmail.com>
# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

from optparse import OptionParser
from vfork.io.colreader import Reader
from vfork.util import exit, format_usage
from sys import stdin
import networkx as NX

def main():
	parser = OptionParser(usage=format_usage('''
		%prog <NETWORK_FILE
		
		Computes the connected components of the input network.

		The output contains the following two columns:
		* node ID
		* connected component ID
	'''))
	options, args = parser.parse_args()

	Graph = NX.Graph()
	for a, b in Reader(stdin, '0s,1s', False):
		Graph.add_edge(a,b)

	for i,cq in enumerate(NX.connected_components(Graph)):
		for v in cq:
			print "%s\t%i" % (v,i) 

if __name__ == '__main__':
	main()
