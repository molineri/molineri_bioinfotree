#!/usr/bin/perl
use warnings;
use strict;
$,="\t";
$\="\n";

my @conn_comp=();
while(<>){
	my ($a,$b) = split;
	my $found=0;
	my $i=0;
	my @to_merge=();
	for(@conn_comp){
		my $conn=$_;
		for(@{$conn}){
			my $el=$_;
			
			if ($el eq $b){
				push @{$conn}, $a;
				$found++;
				push @to_merge, $i;
				last;
			}
			if ($el eq $a){
				push @{$conn}, $b;
				$found++;
				push @to_merge, $i;
				last;
			}
		}
		last if $found >= 2;
		$i++;
	}
	print STDERR $found if $a == 199;
	if($found==0){
		my @conn=($a,$b);
		push @conn_comp, \@conn;
	}elsif(scalar(@to_merge)>1){
		my ($i,$j) = @to_merge;
		my @tmp = splice @conn_comp,$j;
		push @{$conn_comp[$i]}, @{$tmp[0]};
		@{$conn_comp[$i]} = keys %{
			{ map { $_ => 1 } @{$conn_comp[$i]} }
		};#uniq: map aggiunge la chiave all'array, quindi lo iterpretiamo come un hash anonimo (%{) e facciamo agire keys
		#http://snippets.dzone.com/posts/show/354
	}
}

for(@conn_comp){
	print @{$_};
}
