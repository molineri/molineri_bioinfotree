#!/usr/bin/env python

from optparse import OptionParser
from sys import exit, stdin, stderr
from collections import defaultdict

def traverse(k, node, primi_vicini, to_print, traversed):
	
	traversed.add(node)
	
	retval = []

	for n in primi_vicini[node]:
		to_print.add((k,n))
		if n not in traversed: retval.append(n)
	
	return retval



def main():
	parser = OptionParser()
	options,args = parser.parse_args()
	primi_vicini = defaultdict(list)
	
	for line in stdin:
		a,b = line.split(None)
		primi_vicini[a].append(b)
		primi_vicini[b].append(a)

	traversed = set()
	for k, vicini in primi_vicini.iteritems():
		to_print = set()
		to_traverse = []

		if k in traversed: continue

		to_print.add((k,k))
		traversed.add(k)
		
		for node in vicini:
			to_print.add((k,node))
			to_traverse.append(node)
			while(len(to_traverse)>0):
				n = to_traverse.pop(0)
				to_traverse += traverse(k, n, primi_vicini, to_print, traversed)
				#print >>stderr, "%d\r" % len(to_traverse),

		for el in to_print:
			print "%s\t%s" % el


if __name__ == '__main__':
	main()

