#!/usr/bin/env python

from optparse import OptionParser
from sys import exit, stdin

def main():
	parser = OptionParser()
	options,args = parser.parse_args()
	conn_comp = {}
	for line in stdin:
		a,b = line.split(None)
		conn_a = conn_comp.get(a, None)
		conn_b = conn_comp.get(b, None)

		if conn_a is not None and conn_b is not None:
			for k, v in conn_comp.iteritems():
				if v == conn_b:
					conn_comp[k] = conn_a
		elif conn_a is not None:
				conn_comp[b] = conn_a
		elif conn_b is not None:
				conn_comp[a] = conn_b
		else:
			conn_comp[a]=a
			conn_comp[b]=a

	for k,v in conn_comp.iteritems():
		print '%s\t%s' % (k,v)

if __name__ == '__main__':
	main()
