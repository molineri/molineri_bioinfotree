#!/usr/bin/perl
use warnings;
use strict;
$,="\t";
$\="\n";

my $usage="$0 [-fasta|-col] file.net";

my $out_fasta=0;
my $out_2_columns=0;

use Getopt::Long;
GetOptions (
	'fasta' => \$out_fasta,
	'col' => \$out_2_columns,
) or die($usage);

$out_fasta and $out_2_columns and die($usage);

my %conn_comp;
while(<>){
	my ($a,$b) = split;
	my $conn_a = $conn_comp{$a};
	my $conn_b = $conn_comp{$b};
	if(defined $conn_a and defined $conn_b){
		for(keys %conn_comp){#merging;
			#$conn_comp{$_}=$conn_a if $conn_comp{$_}==$conn_b;
			$conn_comp{$_}=$conn_a if $conn_comp{$_} eq $conn_b;
		}
	}elsif(defined $conn_a){
			$conn_comp{$b}=$conn_a;
	}elsif(defined $conn_b){
			$conn_comp{$a}=$conn_b;
	}else{
		$conn_comp{$a}=$a;
		$conn_comp{$b}=$a;
		#print $conn_comp{$b} if $b==689;
	}
}

if($out_2_columns){
	for(keys %conn_comp){
		print $_, $conn_comp{$_};
	}
}else{
	my %out_con;
	for(keys %conn_comp){
		push @{$out_con{$conn_comp{$_}}},$_;
	}
	if($out_fasta){
		$,="\n";
		for(keys %out_con){
			print ">$_";
			print @{$out_con{$_}};
		}
	}else{
		for(keys %out_con){
			print @{$out_con{$_}};
		}
	}
	
}
