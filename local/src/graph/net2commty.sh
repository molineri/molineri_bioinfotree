traduci -a <(conncomp_py < $1) 2 < $1 \
| sort -k 3,3 \
| repeat_group_pipe 'echo -n ">$1"; cut -f 1,2 | commty | perl -lanpe "s/#Modularity://"' 3 \
| $BIOINFO_ROOT/local/src/graph/filter_fasta_modularity.pl \
| repeat_fasta_pipe -n 'append_each_row -F "_" $HEADER'
