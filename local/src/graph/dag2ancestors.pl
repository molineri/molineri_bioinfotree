#!/usr/bin/perl
use warnings;
use strict;
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {my @loc = caller(1); CORE::die "WARNING generated at line $loc[2] in $loc[1]: ", @_};

my $usage = "$0 file.dag\n";

my %ancestors = ();

while(<>){
	chomp;
	my @F = split /\t/;
	my $id = shift @F;
	push @{$ancestors{$id}}, @F;
}

foreach my $id (keys(%ancestors)){			#fisso un nodo
	my %a=();
	for(@{$ancestors{$id}}){
		$a{$_} = 1
	}						#ho generato una lista univoca di ancestor
	
	my @k = ();

	while(1) {
		@k=keys(%a);				#faccio una copia di questa lista

		for(@k){				#ciclo su questa lista
			for(@{$ancestors{$_}}){		#
				$a{$_} = 1		#e appendo tutti gli ancestor di elementi gia` nella lista
			}
		}
		
		my @k2 = keys(%a);
		if(scalar @k == scalar @k2){		#faccio questo n volte, finche` rifacendo il giro e cercando di aggiungere ancestor non aggiungo piu` niente
			@k = @k2;
			last;
		}
	
		@k = @k2;
	}

	print $id, join(',',$id,@k);			#ho finito di popolare la lista degli ancestor per il nodo $id, stampo e passo al nodo successivo
}
