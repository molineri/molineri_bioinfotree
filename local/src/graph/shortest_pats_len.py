#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr, argv
import atexit
from optparse import OptionParser
from vfork.util import exit
from collections import defaultdict
from gzip import GzipFile
import networkx as NX

script_name = argv[0]

def main():
	parser = OptionParser(usage='''%prog root_id\nnegative length stay for non existent path''') 
	
	parser.add_option('-d', '--directed', dest='directed', action='store_true', default=False, help='the graph given in stdin is considered as a directed graph, ancestors in the first column (A --> B), see --reverse_edge')
	parser.add_option('-f', '--from_file', dest='from_file', action='store_true', default=False, help='the first argument is not a root but a file with a list node used indipendently as root to caclculate shortest path')
	parser.add_option('-r', '--reverse_edge', dest='reverse_edge', action='store_true', default=False, help='reverse edge direction')
	parser.add_option('-a', '--all', dest='all', action='store_true', default=False, help='compute all the shortest path')
	parser.add_option('-g', '--group', dest='group', action='store_true', default=False, help='compute shortest path only between nodes in the given groups')
	parser.add_option('-m', '--missing', type=int, dest='missing', default=10000000, help='the lengh associated to non existend path [default: %default]', metavar='VAL')

	
	options, args = parser.parse_args()

	if options.reverse_edge and not options.directed:
		exit("Option -r is meaningless without option -d");
	
	if options.all:
		if len(args) != 0:
			exit('Unexpected argument number.')
	else:
		if len(args) != 1:
			exit('Unexpected argument number.')
	
	if options.all and options.from_file:
		exit("-a and -f mutally exclusive")

	roots = []

	if not options.from_file and not options.all:
		roots.append(args[0])
	elif not options.all:
		with file(args[0], 'r') as fd:
			for line in fd:
				tokens = line.rstrip().split('\t')
				assert len(tokens) == 1
				roots.append(tokens[0])
	
	if not options.directed:
		Graph = NX.Graph()
	else:
		Graph = NX.DiGraph()

	for line in stdin:
		tokens = line.rstrip().split('\t')
		assert len(tokens) == 2
		if not options.reverse_edge:
			Graph.add_edge(*tuple(tokens))
		else:
			Graph.add_edge(tokens[1],tokens[0])

	if options.all:
		for k1,v in NX.all_pairs_shortest_path_length(Graph).iteritems():
			for k2,l in v.iteritems():
				print "%s\t%s\t%d" % (k1,k2,l)
	else:
		for i,root in enumerate(roots):
			if not options.group:
				shortest_path = NX.single_source_shortest_path(Graph, root)
				for id, path in shortest_path.iteritems():
					print "%s\t%s\t%d" % (root, id, len(path) -1)
			else:
				j=i+1
				while j<len(roots):
                                        try:
					    l = len(NX.shortest_path(Graph, source=root, target=roots[j])) - 1
                                        except NX.exception.NetworkXNoPath:
                                            l = options.missing
					print "%s\t%s\t%d" % (root, roots[j], l)
					j+=1


if __name__ == '__main__':
	main()

