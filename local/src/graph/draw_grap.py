#!/usr/bin/env python
from __future__ import with_statement

from sys import exit, stdin, stderr, argv
from optparse import OptionParser
from vfork.io.util import safe_rstrip
#from subprocess import Popen, PIPE
from collections import defaultdict
from numpy import array, dot, sqrt, random

script_name = argv[0]

class node:
	def __init__(self, label, neibs=[]):
		self.label = label
		self.neibs = set(neibs)
		self.neibs_deept={}
		
		self.position = array([0,0])
		self.force = array([0,0])
		self.velocity = array([0,0])
	
	def load_neibs(self, deept):
		if deept == 1:
			self.neibs_deept[1]=set(self.neibs)
		elif deept == 2:
			ret_val=set(self.neibs)
			for n in self.neibs:
				ret_val |= set(n.neibs)
			self.neibs_deept[2]=ret_val
		elif deept >= 3:
			self.load_neibs(deept-1)
			ret_val = set(self.neibs_deept[deept-1])
			for n in self.neibs_deept[deept-1]:
				ret_val |= set(n.neibs)
			self.neibs_deept[deept] = ret_val
		else:
			raise ValueError, 'deept must be >=1'


	def neibs_str(self, deept=1):
		if deept == 1:
			return [n.__str__() for n in self.neibs]
		else:
			return [n.__str__() for n in self.neibs_deept[deept]]

	def __str__(self):
		return self.label

	def update_force(self, level, potential, force_dumping):
		self.force = array([0,0])
		for n in self.neibs:
			self.force = (self.force + (self.position - n.position))
		for n in self.neibs_deept[level]:
			if n is not self:
				repulsion = repulsive_potential(n.position - self.position, potential)
				self.force = self.force + repulsion
		#print 'fr:', self.force
		self.force = self.force * force_dumping

	def update_velocity(self, dumping):
		self.velocity = self.velocity * dumping
		self.velocity = self.velocity + self.force
		#print "v: ", norm(self.velocity)
	
	def update_position(self, pos_dumping):
		self.position = self.position + self.velocity
		self.position = self.position - (1-pos_dumping) * self.position
		#print 'p: ', self.position

def repulsive_potential(v, function):
	x = norm(v)
	if x > 0.: 
		retval = v/x
	else:
		retval = random.random((2))
		retval = retval / norm(retval)
		
	x = function(x)
	return retval * x

def norm(v):
	return sqrt(dot(v,v))

def potential1(x):
	return (1 + x)/(1 + x*x)

def load_graph():
	neibs =defaultdict(list)
	edges =[]
	nodes = []
	V =defaultdict(lambda:None)
	
	for line in stdin:
		tokens = safe_rstrip(line).split('\t')
		assert len(tokens) == 2
		neibs[tokens[0]].append(tokens[1])
		neibs[tokens[1]].append(tokens[0])
		if V[tokens[0]] is None: V[tokens[0]] = node(tokens[0])
		if V[tokens[1]] is None: V[tokens[1]] = node(tokens[1])
		edges.append((V[tokens[0]], V[tokens[1]]))

	for k,v in neibs.iteritems():
		V[k].neibs = [ V[n] for n in neibs[k] ]
		nodes.append(V[k])
	
	return nodes, edges


def main():

	parser = OptionParser(usage='''%prog PARAM1 PARAM2 < STDIN''')
	
	parser.add_option('-v', '--potential_intensity', type=float, dest='potential_v', default=1., help='the intensity of the potential, with respect to a spring constanto k = 1', metavar='CUTOFF')
	parser.add_option('-d', '--dumping', type=float, dest='dumping', default=0.8, help='dumping factor', metavar='CUTOFF')
	parser.add_option('-p', '--pos_dumping', type=float, dest='pos_dumping', default=0.999, help='actraction by position 0,0', metavar='CUTOFF')
	parser.add_option('-f', '--force_dumping', type=float, dest='force_dumping', default=0.2, help='force dumping', metavar='CUTOFF')
	parser.add_option('-l', '--level', type=int, dest='level', default=3, help='''deepnes of repulsive potential interaction (in term of DEEPNES'nd neigborood)''', metavar='DEEPNES')
	parser.add_option('-n', '--steps', type=int, dest='steps', default=1000, help='the number of integration steps')

	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit(script_name + ': Unexpected argument number.')
	
	if options.dumping >= 1. or options.dumping < 0:
		exit(script_name + ': condition 0<= dumping < 1. not respected')
	if options.pos_dumping >= 1. or options.pos_dumping < 0:
		exit(script_name + ': condition 0<= pos_dumping < 1. not respected')
	if options.force_dumping >= 1. or options.force_dumping < 0:
		exit(script_name + ': condition 0<= force_dumping < 1. not respected')

	
	nodes, edges = load_graph()

	#nodes[1].position=array([0,1])

	for n in nodes:
		n.load_neibs(options.level)
	
	#print nodes[0].position
	#print nodes[1].position
	#nodes[1].update_force(options.level, potential1)
	#print nodes[1].force
	#exit()
	
	t=0
	while t < options.steps:
		for n in nodes:
			n.update_force(options.level, potential1, options.force_dumping)
			n.update_velocity(options.dumping)
			n.update_position(options.pos_dumping)
			#print n, n.position, n.force, n.velocity, n.position
		t+=1

	for n in nodes:
		print "%s\t%f\t%f" % (n.label, n.position[0], n.position[1])

if __name__ == '__main__':
	main()
