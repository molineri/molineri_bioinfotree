#!/usr/bin/env python
#
# Copyright 2009-2010 Gabriele Sales <gbrsales@gmail.com>

from datetime import datetime
from ftplib import FTP
from ftplib import all_errors as FTPError
from optparse import OptionParser
from os.path import split
from time import localtime, strftime
from urlparse import urlparse
from vfork.util import exit, format_usage, safe_import

with safe_import('ftpparse (http://c0re.23.nu/c0de/ftpparsemodule/)'):
    from ftpparse import ftpparse, MTIME_UNKNOWN


def parse_url(url):
    p = urlparse(url)
    if p.scheme != 'ftp':
        exit('Invalid URL scheme: %s' % p.scheme)
    elif len(p.path) == 0:
        exit('No path in URL.')
    else:
        return p.netloc, p.path

    
def main():
    parser = OptionParser(usage=format_usage('''
        %prog [OPTIONS] URL

        Prints the modification date (in the format %Y%m%d)
        of the file corresponding to the specified FTP URL.
    '''))
    options, args = parser.parse_args()
    if len(args) != 1:
        exit('Unexpected argument number.')

    host, path = parse_url(args[0])
    fdir, fname = split(path)
    c = FTP(host)
    c.login()
    c.cwd(fdir)

    listing = []
    c.dir(listing.append)

    for info in ftpparse(listing):
        if info is None or info[0] != fname:
            continue
        elif info[4] == MTIME_UNKNOWN:
            exit('Cannot get timestamp for: ' + args[0])

        print strftime('%y%m%d', localtime(info[3]))

    c.quit()


if __name__ == '__main__':
    main()
