#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;
use Scalar::Util qw(looks_like_number);
$,="\t";
$\="\n";

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 [-h|help] [-p product]] FILEA < FILEB\n
	generate the cartesian product of two matrix. E.g.

	A  B          a  b          A;a  A;b   B;a  B;b
	C  D     x    c  d      =   C;c  C;b   D;c  D;d

        with -p insthead to merge the values with ; make the product it the operand are numeric
";

my $help=0;
my $product=0;
GetOptions (
	'h|help' => \$help,
	'p|product' => \$product,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my $filename_A = shift || undef;

open(FH_A,"<$filename_A") or die("Can't open file ($filename_A)");

my @row_A=();
my @row_B=();

while(<FH_A>){
	chomp;
	my @F = split /\t/,$_,-1;
	push @row_A,\@F;
}

my $i=0;
while(<>){
	chomp;
	my @F = split /\t/,$_,-1;
	my @G;
	foreach my $a (@{$row_A[$i]}){
		foreach my $b (@F){
			if(not $product){
				push(@G, $a . ";" . $b)
			}else{
				if(looks_like_number($a) and looks_like_number($b)){
					push(@G, $a * $b)
				}else{
					push(@G, $a . ";" . $b)
				}
			}
		}
	}
	print @G;
	$i++
}
