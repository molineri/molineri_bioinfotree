#!/usr/bin/env python
#
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from __future__ import with_statement
from labels import load_labels
from optparse import OptionParser
from sys import stdin
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage

def load_matrix(filename):
	with file(filename, 'r') as fd:
		lines = []
		for line in fd:
			lines.append(safe_rstrip(line))
		return lines

def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] LABELS MATRIX <QUERY >ROWS

		Reads the rows corresponding to the requested labels out of a matrix.
	'''))
	options, args = parser.parse_args()

	if len(args) != 2:
		exit('Unexpected argument number.')
	
	labels = load_labels(args[0])
	matrix = load_matrix(args[1])

	if len(matrix) != len(labels):
		exit('Unexpected matrix row number.')

	for line in stdin:
		label = safe_rstrip(line)

		try:
			idx = labels.index(label)
			print matrix[idx]
		except ValueError:
			exit('No such row: %s' % label)

if __name__ == '__main__':
	main()
