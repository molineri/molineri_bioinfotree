# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from __future__ import with_statement
from vfork.io.util import safe_rstrip
from vfork.util import exit

def load_labels(filename):
	with file(filename, 'r') as fd:
		labels = []
		for line_idx, line in enumerate(fd):
			label = safe_rstrip(line)
			if label in labels:
				exit('Duplicated label at line %d of file %s' % (line_idx+1, filename))
			else:
				labels.append(label)
		return labels
