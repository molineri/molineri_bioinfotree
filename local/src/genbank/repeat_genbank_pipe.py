#!/usr/bin/env python
#
# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>
# Copyright 2009 Alessandro Coppe <alexcoppe@gmail.com>

from cStringIO import StringIO
from optparse import OptionParser
from subprocess import Popen, PIPE
from sys import stdin, stdout
from vfork.util import exit, format_usage
import errno

def iter_records(fd):
	record = StringIO()
	for idx, line in enumerate(fd):
		record.write(line)
		if line.rstrip() == '//':
			yield record.getvalue()
			record = StringIO()
	
	if len(record.getvalue()) > 0:
		exit('Malformed GenBank input.')

def iter_by_num(max_num):
	def worker(records):
		try:
			while True:
				accum = StringIO()
				num = 0
				while num < max_num:
					accum.write(records.next())
					num += 1
				yield accum.getvalue()
		except StopIteration:
			data = accum.getvalue()
			if len(data): yield data
	
	return worker

def iter_by_size(max_size):
	def worker(records):
		try:
			accum = StringIO()
			accum_size = 0
			while True:
				record = records.next()
				if accum_size + len(record) > max_size:
					yield accum.getvalue()
					accum = StringIO()
					accum_size = 0
				accum.write(record)
				accum_size += len(record)
		except StopIteration:
			data = accum.getvalue()
			if len(data): yield data
	
	return worker

def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] COMMAND <GENBANK

		Run COMMAND repeatedly on sub-parts of the input GENBANK file.
	'''))
	parser.add_option('-n', '--record-num', dest='record_num', type=int, help='maximum number of records for each part')
	parser.add_option('-s', '--part-size', dest='part_size', type=int, help='maximum size (in bytes) of each part')
	options, args = parser.parse_args()

	if len(args) != 1:
		exit('Unexpected argument number.')
	elif options.record_num is not None and options.record_num <= 0:
		exit('Invalid maximum record number.')
	elif options.part_size is not None and options.part_size <= 0:
		exit('Invalid maximum part size.')
	elif (options.record_num is None and options.part_size is None) or \
	     (options.record_num is not None and options.part_size is not None):
		exit('Use either --record-num or --part-size.')
	
	if options.record_num is not None:
		iter_blocks = iter_by_num(options.record_num)
	else:
		iter_blocks = iter_by_size(options.part_size)
	
	for data in iter_blocks(iter_records(stdin)):
		cmd = Popen(args[0], shell=True, close_fds=True, stdin=PIPE)
		try:
			cmd.communicate(data)
		except IOError, e:
			if e.errno != errno.EPIPE:
				raise

		if cmd.wait() != 0:
			exit('Error running command.')

if __name__ == '__main__':
	main()
