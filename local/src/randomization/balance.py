#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
from random import shuffle
#from subprocess import Popen, PIPE
from collections import defaultdict
#from vfork.io.colreader import Reader



def main():
	usage = format_usage('''
		%prog COL < STDIN
		balance the dataset so that in the col COL each factor is represented in the same number of rows,
		rows are picked at random, each row of the minoritary factor are taken
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-n', '--repetitions', type=int, dest='repetitions', default=None, help='resample N times, all sample has any row with the minoritary factor [default: %default]', metavar='N')
	#parser.add_option('-p', '--dump-params', dest='params_file', help='some help FILE [default: %default]', metavar='FILE')
	#parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='some help [default: %default]')

	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	COL=int(args[0])
	COL-=1
	
	#for id, sample, raw, norm in Reader(stdin, '0u,1s,2i,3f', False):
	factors =defaultdict(list)
	for line in stdin:
		line = safe_rstrip(line)
		tokens = line.split('\t')
		factors[tokens[COL]].append(line)
	
	minor_factor = None
	factor_size  = None
	for (factor, lines) in factors.iteritems():
		l = len(lines)
		if factor_size is None or l < factor_size:
			factor_size = l
			minor_factor = factor

	_print = True
	if options.repetitions is not None:
		_print = True
	else:
		options.repetitions = 1
	
	for i in xrange(0,options.repetitions):
		if _print:
			print ">%d" % i
		for f,lines in factors.iteritems():
			if f != minor_factor:
				shuffle(lines)
				lines = lines[0:factor_size]
			for l in lines:
				print l


if __name__ == '__main__':
	main()

