#!/usr/bin/env python
#
# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>

from optparse import OptionParser
from random import choice, shuffle
from sys import stdin, stdout
from vfork.util import exit, format_usage

def parse_num(s, label):
	try:
		n = int(s)
		if n <= 0: raise ValueError
		return n
	except ValueError:
		exit('Invalid %s value: %s' % (label, s))

def gen_no_repeat(rows, r):
	shuffle(rows)
	return rows[:r]

def gen_repeat(rows, r):
	for i in xrange(r):
		yield choice(rows)
		     
def main():
	parser = OptionParser(usage=format_usage('''
		%prog [OPTIONS] [N] R <ROWS

		Outputs N sets (by default, 1) of R rows each.
		Rows are selected randomly from the input.

		By default each set doesn't contain repetitions. Thus, R cannot be
		greater than the number of input lines. If --allow-dups is enabled, the
		program will always generate R rows, introducing duplicates as necessary.
	'''))
	parser.add_option('-d', '--allow-dups', dest='allow_dups', action='store_true', help='allow duplicate rows in output')
	parser.add_option('-n', '--no-header', dest='no_header', action='store_true', help='omit FASTA header when N==1')
	options, args = parser.parse_args()
	if len(args) < 1 or len(args) > 2:
		exit('Unexpected argument number.')
	
	if len(args) == 1:
		n = 1
		r = parse_num(args[0], 'R')
	else:
		n = parse_num(args[0], 'N')
		r = parse_num(args[1], 'R')

	if options.no_header and n != 1:
		exit("Can't use --no-header when N != 1.")

	rows = list(stdin)
	if not options.allow_dups and len(rows) < r:
		exit('Insufficient row number in input (you can try with --allow-dups).')

	gen = gen_repeat if options.allow_dups else gen_no_repeat
	for t in xrange(n):
		if not options.no_header:
			print '>%d' % (t+1,)

		for row in gen(rows, r):
			stdout.write(row)


if __name__ == '__main__':
	main()
