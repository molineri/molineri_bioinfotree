#ifndef ASCII_MATRIX_CPP
#define ASCII_MATRIX_CPP
#include "AsciiMatrix.h"

using namespace std;
using namespace TNT;

AsciiMatrix::AsciiMatrix(
		istream &param_InFile,
		unsigned int param_head_rows,
		unsigned int param_head_cols,
		unsigned int param_data_cols,
		string param_field_delim,
		bool param_verbose
	){
	
	InFile=&param_InFile;
	head_rows = param_head_rows;
	head_cols = param_head_cols;
	data_cols = param_data_cols;
	field_delim=param_field_delim;
	verbose=param_verbose;
}

template<class Type>
void AsciiMatrix::fill_matrix(	
		Array2D< Type > &mtx,
		Array2D< bool> &present_mtx
	){

	cerr<<"ERROR  AsciiMatrix::fill_matrix: funzione non testata"<<endl;
	exit(-1);
	if(head_rows>0){
		for(int i=0;i<head_rows;i++){//perdo le righe di intestazione
			string line;
			getline(*InFile,line,'\n');
		}
		if(verbose){
			cerr<<"AsciiMatrix::fill_matrix: "<<head_rows << " header row sckipped"<<endl;
		}
	}else if(verbose){
		cerr<<"AsciiMatrix::fill_matrix: no header row sckipped"<<endl;
	}

	
	Type* value = new Type[data_cols];	//i dati di una singola riga
	bool * present  = new bool[data_cols];	//il dato di una colonna puo` non essere presente per una data riga
	
	int i=0;
	while( parse_next_row(value, present)){ 
		for(int j=0; j<data_cols; j++){
			mtx[i][j]=value[j+head_cols];
			present_mtx[i][j]=present[j+head_cols];
		}
		i++;
	}
	
	delete [] value;
	delete [] present;
}

template<class Type>
bool AsciiMatrix::parse_next_row(
		string first_columns[],
		Type value[],
		bool present[]
	){
	
	unsigned int row_index=0;

	string line;
	cerr << "ciao1" << endl;
	if(getline(*InFile,line,'\n')){
		
		
	   	StringTokenizer strtok = StringTokenizer(line,field_delim,false);
	
		unsigned int ct=strtok.countTokens();
		
		for(unsigned int j=0; j<head_cols; j++){//perdo le colonne di intestazione
			string tok=strtok.nextToken();
			first_columns[j]=tok;
		}
		
		for(unsigned int j=0; j<data_cols; j++){
			//cerr << "j: " << j << "\tj_min: " << j_min << endl; 
			cerr << "ciao2" << endl;
			string tok=strtok.nextToken();
			if(
				(tok=="NaN")
				or
				(tok=="nan")
				or
				(tok=="X")
				or
				(tok=="x")
				or
				(tok=="")
			){
				if(verbose){
					cerr	<<"WARNING: AsciiMatrix::parse_next_row: token not a number: (" 
						<<tok<<")"<<endl;
				}
				present[j]=false;
			}else{
				//cout<<"token present: "<<token<<endl;
				double num_val=atof(tok.c_str());
				value[j]=num_val;
				present[j]=true;
			}
		}
		return true;
	}else{
		return false;
	}
}
#endif
