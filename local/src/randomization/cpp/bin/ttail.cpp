#include <iostream>
#include <stdlib.h>
#include "gsl/gsl_cdf.h"

using namespace std;

int main( int argc, char* argv[] ){
	if(argc!=3){
		cerr<<"usage: ttail value degre_of_fredom"<<endl;
		exit(-1);
	}
	cout << gsl_cdf_tdist_Q(atof(argv[1]),atof(argv[2])) << endl;
	return(0);
}
