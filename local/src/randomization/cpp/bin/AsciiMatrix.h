#ifndef ASCII_MATRIX_H
#define ASCII_MATRIX_H

#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <string>
#include "tnt.h"
#include "StringTokenizer.h"

using namespace std;
using namespace TNT;

class AsciiMatrix {
	public:
		AsciiMatrix(
			istream &InFile = cin, 
			unsigned int head_rows=0, 
			unsigned int head_cols=0, 
			unsigned int data_cols=0,
			string field_delim="\t",
			bool verbose=false
		);
		
		template<class Type>
		bool parse_next_row(
			string first_columns[],
			Type value[],
			bool present[]
		);

		template<class T> void fill_matrix(	
			Array2D< T > &mtx,
			Array2D< bool> &present_mtx
		);
		void set_head_rows(unsigned int);
		void set_head_cols(unsigned int);
		void set_data_cols(unsigned int);
		void set_field_delim(string);
		
	private:
		istream* InFile;
		int head_rows;
		int head_cols;
		int data_cols;
		string field_delim;
		bool verbose;
};
#include "AsciiMatrix.cpp"
#endif
