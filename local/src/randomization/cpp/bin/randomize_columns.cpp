#include "AsciiMatrix.h"
#include "RandomGenerator.h"

// to compile
//         g++ -lgsl -lgslcblas $< -o $@
// 

using namespace std;

static const char *usage = "randomize_columns -h 1 -p 2 -c 10\nthis example leave untouched:\n\tthe first line\n\tthe column 0 and 1\nfor each row randomize value in columns from 2 to 12\n";

int main (int argc, char *argv[], char *envp[]){

	int head_rows;
	int head_cols;
	int data_cols;

	//gestione dei parametri da linea di comando
	//
	//
	bool head_rows_set=false;
	bool head_cols_set=false;
	bool data_cols_set=false;
	int c;
	while (c = getopt(argc, argv, "h:p:c:")){
		if (c == -1) break;

		switch (c) {
			case 'h':
				head_rows=atoi(optarg);
				head_rows_set=true;
				break;
			case 'p':
				head_cols=atoi(optarg);
				head_cols_set=true;
				break;
			case 'c':
				data_cols=atoi(optarg);
				data_cols_set=true;
				break;
			default:
				printf ("ERROR: unknow option (%s) \n %s", c, usage);
				exit(-1);
		}
	}
	if (optind < argc) {
		printf ("ERROR: unsupported non-option ARGV-elements (%s)\n%s",argv[optind++],usage);
		exit(-1);
		/*while (optind < argc){
			printf ("%s ", argv[optind++]);
		}*/
	}
	
	if(!head_rows_set || !head_cols_set || !data_cols_set){
		cerr<<"ERROR: randomize_columns: -h, -p and -c option required" << endl << usage << endl;
		exit(-1);
	}
	//
	//
	// fine controllo parametri da linea di comando




	RandomGenerator * RG = new RandomGenerator(false);	
	

	//stampo le linee di intestazione
	string tmp;
	for(int i=0; i<head_rows; i++){
		getline(cin,tmp);
		cout << tmp << endl;
	}
	
	
	
	string delim="\t";
	AsciiMatrix * mtx = new AsciiMatrix(cin,head_rows,head_cols,data_cols,delim);
	
	
	string * first_columns = new string[head_cols];
	double * val = new double[data_cols];
	bool * present = new bool[data_cols];

	
	while(mtx->parse_next_row(first_columns,val,present)){
		cerr << "ciao" << endl;
		RG->shuffle(val,data_cols);
		for(int i=0; i<head_cols; i++){
			if(i!=0){
				cout << "\t";
			}
			cout << first_columns[i];
		}
		for(int i=0; i<data_cols; i++){
			cout << "\t" << val[i];
		}
		cout << endl;
	}	
	return 0;
}
