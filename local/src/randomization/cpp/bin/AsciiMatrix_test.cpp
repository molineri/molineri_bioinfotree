#include <iostream>
#include <string>
#include "AsciiMatrix.h"

using namespace std;


int main( int argc, char* argv[] ){
	string delim="\t";
	AsciiMatrix * test_matrix = new AsciiMatrix(cin,1,1,5,delim);
	int * data  = new int[5];
	bool * present= new bool[5];
	string * label_cols = new string[1];

	string header_row ="";
	getline(cin, header_row);
	cout << header_row << endl;
	while(test_matrix->parse_next_row(label_cols, data, present)){
		cout<<data[0]<<endl;
	}
}
