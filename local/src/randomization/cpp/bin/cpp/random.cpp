# include <iostream>
# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <sys/time.h>

using namespace std;

int main (int argc, char *argv[], char *envp[]){
	if(argc != 4){
		cerr 	<< "usage: random min max number_of_output_row "<<endl
			<<"	min max must be integer "<< endl;
		exit(-1);
	}
	unsigned long int seed=0;	
	struct timeval time;
	struct timezone timez;
    	int rc;
    	rc=gettimeofday(&time, &timez);
	seed=time.tv_sec+time.tv_usec;
	//cerr<<seed<<endl;
	//exit(-1);

	srand48(seed);
	int i_max=atoi(argv[3]);
	int rand_min=atoi(argv[1]);
	int rand_max=atoi(argv[2]);
	for(int i=0; i<i_max; i++){
		double rand_num=drand48();
		if(rand_num<1){
			cout << (int)(rand_num*(rand_max +1 -rand_min)+rand_min) << endl;
		}
	}
	return(0);
}
