# include "RandomGenerator.h"

// to compile
// random_series: random_series.cpp
//         g++ -lgsl -lgslcblas $< -o $@
// 

using namespace std;

static const char *usage = "random_series -m 0 -M 10 -l inf [-i]\nthis example produce a random series of number in [0..10)\nif -i is given number are integer and max is included in output set\n\n\nreads the environment variables GSL_RNG_TYPE and GSL_RNG_SEED and uses their values to set the corresponding library variables\nIf GSL_RNG_TYPE is not set default generator is used; if GSL_RNG_SEED is not set a time funcion is used to seed.\n\n\tgsl_rng_mt19937\n\nThe MT19937 generator of Makoto Matsumoto and Takuji Nishimura is a variant of the twisted generalized feedback shift-register algorithm, and is known as the \"Mersenne Twister\" generator. It has a Mersenne prime period of 2^19937 - 1 (about 10^6000) and is equi-distributed in 623 dimensions. It has passed the DIEHARD statistical tests. It uses 624 words of state per generator and is comparable in speed to the other generators. The original generator used a default seed of 4357 and choosing s equal to zero in gsl_rng_set reproduces this.\n\n\tFor more information see,\n* Makoto Matsumoto and Takuji Nishimura, \"Mersenne Twister: A 623-dimensionally equidistributed uniform pseudorandom number generator\". ACM Transactions on Modeling and Computer Simulation, Vol. 8, No. 1 (Jan. 1998), Pages 3-30 \n\tThe generator gsl_rng_19937 uses the second revision of the seeding procedure published by the two authors above in 2002. The original seeding procedures could cause spurious artifacts for some seed values. They are still available through the alternate generators gsl_rng_mt19937_1999 and gsl_rng_mt19937_1998";

int main (int argc, char *argv[], char *envp[]){
	
	char *min;
	char *max;
	bool integer=false;
	int length=-1;

	//gestione dei parametri da linea di comando
	//
	//
	bool max_set=false;
	bool min_set=false;
	bool length_set=false;
	int c;
	while (c = getopt(argc, argv, "m:M:l:i")){
		if (c == -1) break;

		switch (c) {
			case 'm':
				min=optarg;
				min_set=true;
				break;
			case 'M':
				max=optarg;
				max_set=true;
				break;
			case 'i':
				integer=true;
				break;
			case 'l':
				if(strcmp(optarg,"inf")){
					length=atoi(optarg);
					if(length==0){
						cerr<<"ERROR: -l optarg must be an integer >0 or \"inf\""<<endl;
						exit(-1);
					}
				}
				length_set=true;
				break;
			default:
				printf ("ERROR: unknow option (%s) \n %s", c, usage);
				exit(-1);
		}
	}
	if (optind < argc) {
		printf ("ERROR: unsupported non-option ARGV-elements (%s)\n%s",argv[optind++],usage);
		exit(-1);
		/*while (optind < argc){
			printf ("%s ", argv[optind++]);
		}*/
	}
	
	if(!min_set || !max_set || !length_set){
		cerr<<"ERROR: -m, -M and -l option required" << endl << usage << endl;
		exit(-1);
	}
	//
	//
	// fine controllo parametri da linea di comando




	RandomGenerator * RG = new RandomGenerator(true);	

	int i=0;	
	if(!integer){
		double rand_min=atof(min);
		double rand_max=atof(max);
		RG->set_max(rand_max);
		RG->set_min(rand_min);
		
		while(1){
			if(length > 0 && length <= i) return(0);
			cout << RG->get_double() << endl;
			length > 0 && i++;
		}
	}else{
		int rand_min=atoi(min);
		int rand_max=atoi(max);
		RG->set_max(rand_max);
		RG->set_min(rand_min);
		while(1){
			if(length > 0 && length <= i) return(0);
			cout << RG->get_int() << endl;
			length > 0 && i++;
		}
	}

	return(0);
}
