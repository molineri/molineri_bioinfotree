#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;

$,="\t";
$\="\n";

my $usage = "$0 [-normalize|n] \n with -n return the averages of the columns\n";

my $normalize = undef;
my $count_lines = undef;

GetOptions (
	'normalize|n'     => \$normalize,
	'count_lines|c'   => \$count_lines
) or die ($usage);



my @sum=();
my $n_lines=0;

while(<>){
	chomp;
	my @F=split;
	my $i=0;
	for(@F){
		$sum[$i]+=$_;
		$i++;
	}
	$n_lines++;
}


if ($normalize) {
	@sum=map{$_/=$n_lines} @sum;
}

if ($count_lines) {
	print $n_lines,@sum;
} else {
	print @sum;
}
