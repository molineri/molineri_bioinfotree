#!/usr/bin/env python
# encoding: utf-8

from optparse import OptionParser
from sys import stdin
from vfork.io.colreader import Reader
from vfork.util import exit, format_usage

def main():
	usage = format_usage('''
		%prog <INPUT

		INPUT contiene due colonne:
		 - ID
		 - conteggio
	''')
	parser = OptionParser(usage=usage)
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	
	reader = Reader(stdin, '0s,1i', False)
	last_group = None
	cumulative = None
	while True:
		tokens = reader.readline()
		if tokens is None:
			break
		
		group, count = tokens
		if group < last_group:
			exit("disorder found in column 1")
		elif group != last_group:
			if cumulative is not None:
				print '%s\t%d' % (last_group, cumulative)
			
			last_group = group
			cumulative = count
		else:
			cumulative += count
		
	if cumulative is not None:
		print '%s\t%d' % (last_group, cumulative)

if __name__ == "__main__":
	main()
