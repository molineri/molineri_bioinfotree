#!/usr/bin/perl 

use strict;
use warnings;
use Getopt::Long;

$,="\t";
$\="\n";


my $code_col = undef;
my $sum_col = undef;


my $usage = "cat sorted_file | sum_sorted.pl -s #column_to_sum -c #column_with_code\n";

GetOptions (
	'column_to_sum|s=i'	=>  \$sum_col,
	'column_with_code|c=i'	=>  \$code_col
) or die ($usage);

if ( (!defined $code_col) || (!defined $sum_col) ) {
	die $usage;
}


$sum_col--;
$code_col--;


my $old_code = undef;
my $sum = 0;	

my $line;
my $new_code = undef;
my $new_value = undef;
if (!defined $old_code) {
	$line = <>;
	if (!defined $line) {
		die ("Void file!!");
	}
	chomp $line;
	my @tmp = split /\t/,$line;
	$new_code = $tmp[$code_col];
	$new_value = $tmp[$sum_col];
	$sum = $new_value;
}
while (1) {
	$old_code = $new_code;
	$line = <>;
	if (!defined $line) {
		print $old_code,$sum; 
		last;
	}
	chomp $line;
	my @tmp = split /\t/,$line;
	$new_code = $tmp[$code_col];
	$new_value = $tmp[$sum_col];
	if ($old_code == $new_code) {
		$sum += $new_value;
	} else {
		print $old_code,$sum;
		$sum = $new_value;
	}
}
	


