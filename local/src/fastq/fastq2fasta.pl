#!/usr/bin/perl
use warnings;
use strict;
use Getopt::Long;

$SIG{__WARN__} = sub {die @_};

my $usage = "$0 < file.fastq > file.fa \n";

my $help=0;
GetOptions (
	'h|help' => \$help,
) or die($usage);

if($help){
	print $usage;
	exit(0);
}

my $test=0;
while(<>){
	if(m/^\+/){
		my $garbage = <>; #discarding quality
		$test=0;
		next;
	}
	if(s/^@/>/){
		die("Malformed input file") if $test;
		print;
		my $seq = <>;
		print $seq; 
		$test=1;
		next;
	}

}
