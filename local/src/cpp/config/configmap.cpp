#include "configmap.h"
#include <fstream>
#include <iostream>

using namespace config;
using namespace std;
using namespace __gnu_cxx;

ConfigMap* ConfigMap::load(const char* path)
{
	ifstream in(path);
	if (in.fail())
		throw IOError();
	
	int lineno = 0;
	while (true)
	{
		const int lineLen = 4096;
		char line[lineLen];
		
		in.getline(line, lineLen);
		if (in.eof())
			break;
		if (in.fail())
			throw IOError();
		else
			lineno++;
		
		char* value = line;
		while (*value == ' ' || *value == '\t')
			++value;
		if (*value == 0 || *value == '#')
			continue;
		
		char* key = strsep(&value, ":");
		if (value == NULL || *value == 0)
		{
			cerr << "[WARNING] malformed option at line " << lineno << "; skipping" << endl;
			continue;
		}
		
		options_.insert(pair<string, string>(strip(key), strip(value)));
	}
	
	return this;
}

bool ConfigMap::has_key(const std::string& option) const
{
	hash_map<const string, string, string_hash, StringEquality>::const_iterator it = options_.find(option);
	return it != options_.end();
}

const string& ConfigMap::operator[](const string& option) const
{
	hash_map<const string, string, string_hash, StringEquality>::const_iterator it = options_.find(option);
	if (it == options_.end())
		throw KeyError();
	
	return it->second;
}

char* ConfigMap::strip(char* str)
{
	char* ptr = str;
	while (*ptr == ' ' || *ptr == '\t')
		++ptr;
	
	char* eptr = ptr + strlen(ptr) - 1;
	while (eptr > ptr && (*eptr == ' ' || *eptr == '\t'))
		--eptr;
	*(eptr+1) = 0;
	
	return ptr;
}
