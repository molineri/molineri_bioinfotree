#!/bin/bash
set -e

rm -f config.cache config.h.in

aclocal
glibtoolize --force --copy
autoheader
automake --add-missing --copy --foreign
autoconf
