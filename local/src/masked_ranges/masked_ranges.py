#!/usr/bin/env python

from optparse import OptionParser
from sys import exit, stdin
import re

if __name__ == '__main__':
	parser = OptionParser(usage='%prog <SEQUENCE')
	options, args = parser.parse_args()

	if len(args) != 0:
		exit('Unexpected argument number.')
	
	line = stdin.readline() # skip FASTA header
	sequence = stdin.read()
	if line[0] != '>':
		sequence = line + sequence
	
	tr_table = ''.join(chr(i) for i in xrange(256))
	sequence = sequence.translate(tr_table, '\r\n')
	
	masked_rx = re.compile(r'[acgtnN]+')
	for match in masked_rx.finditer(sequence):
		print '%d\t%d' % (match.start(), match.end())
