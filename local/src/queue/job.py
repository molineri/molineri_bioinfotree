# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from shutil import rmtree
from socket import gethostname
from tempfile import mkdtemp
from time import time

class Job(object):
	def __init__(self, name, command, work_dir):
		self.name = name
		self.command = command
		self.work_dir = work_dir
		self.queue_id = None
	
	@staticmethod
	def gen_name():
		hostname = gethostname().split('.')[0]
		tstamp = int(time() * 100)
	
		digits = '0123456789abcdefghijklmnopqrstxywz'
		digit_num = len(digits)
		id_parts = []
		while tstamp != 0:
			id_parts.append(digits[tstamp % digit_num])
			tstamp = tstamp // digit_num
	
		id_parts.reverse()
		return '%s-%s' % (hostname, ''.join(id_parts))

class TempDir(object):
	def __enter__(self):
		self.path = mkdtemp()
		return self.path
	
	def __exit__(self, type, value, traceback):
		rmtree(self.path)
