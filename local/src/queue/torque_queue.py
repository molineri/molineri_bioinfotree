# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from commands import getstatusoutput
from subprocess import Popen, PIPE
from time import sleep
import queue

class Queue(object):
	def __init__(self, config):
		self.config = config
	
	def submit(self, job, wait=True):
		args = [
			'qsub',
			'-d', job.work_dir,
			'-e', job.name + '.err',
			'-o', job.name + '.out',
			'-N', job.name
		]
		
		try:
			proc = Popen(args, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=False, close_fds=True)
			out, err = proc.communicate(job.command)
		except OSError:
			raise queue.QueueError('cannot queue job', 'you are probably missing the qsub command')
		
		if proc.wait() != 0:
			raise queue.QueueError('cannot queue job', err)
		
		job.queue_id = out.strip()
		if wait:
			self.wait(job)
	
	def wait(self, job):
		if job.queue_id is None:
			raise ValueError, 'invalid job'
		
		while True:
			status, output = getstatusoutput('qstat ' + job.queue_id)
			if 'unknown job' in output.lower():
				break
			elif status != 0:
				raise queue.QueueError('cannot stat job', output)
			else:
				sleep(self.config.poll_interval)
