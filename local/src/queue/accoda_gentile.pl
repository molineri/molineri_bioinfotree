#!/usr/bin/perl

use warnings;
use strict;
use Fcntl ':flock'; # importa le costanti LOCK_*

my $usage="$0 jobs
jobs e` un file che contiene, per ogni riga un comando da accodare, preceduto eventualmetne da 
--name nome_job
";

my $whoami=`whoami`;
chomp $whoami;

my $filename=$ARGV[0];
$filename or die($usage);
defined($ARGV[1]) and die($usage);

open FH,'+<', $filename;

while(1){
	
	##chiedo accesso esclusivo al file
	flock(FH,LOCK_EX);
	################################
	
	##leggo la prima riga in $cmd
	# se non ci sono righe esco
	seek FH, 0, 0;
	my $cmd=<FH>; 
	defined($cmd) or last;
	chomp $cmd;
	$cmd=~/./ or last;
	print "leggo $cmd\n";
	################################

	
	##rialscio il file
	flock(FH,LOCK_UN);
	################################

	my $i=0;
	while(1){
		
		my $tmp=`qstat | grep $whoami`;
		
		my @jobs=split( /\n/, $tmp);

		my $do=1;
		
		for(@jobs){
			if($_=~/\sQ\s/){
				$do=0;
			}
		}

		if($do){
			print 'accodo'."\n";
			print `accoda $cmd`;
			##elimino la prima riga dal file
			my @rows=<FH>;
			`echo > $filename`;
			seek FH, 0, 0;
			if(@rows){
				print FH @rows;
			}
			################################
			last;
		}else{
			print 'aspetto ', scalar(@jobs) , "\n";
			if($i<10){
				$i++;
				sleep 3;
			}else{
				sleep 120;
			}
		}
	}
}
