#!/usr/bin/env python
# Copyright 2008 Gabriele Sales <gbrsales@gmail.com>

from config import Config, ConfigError
from job import Job
from optparse import OptionParser
from os import environ, getcwd
from os.path import join
from queue import make_queue, QueueError
from sys import stdin
from vfork.util import exit, format_usage

def main():
	usage = '''
		%prog [COMMAND...]
		
		This tool accept the command to be queued either on the command line or on the
		standard input. The following two calls are equivalent:
		  $ queue_job ls /usr
		  $ echo "ls /usr" | queue_job
	'''
	parser = OptionParser(usage=format_usage(usage))
	parser.add_option('-c', '--config', dest='config_path', help='the path of the configuration file', metavar='PATH')
	parser.add_option('-n', '--name', dest='name', help='the job name', metavar='NAME')
	parser.add_option('-w', '--wait', dest='wait', action='store_true', default=False, help='wait for command to complete')
	options, args = parser.parse_args()
	
	try:
		bioinfo_root = environ['BIOINFO_ROOT']
	except KeyError:
		exit('Environment variable BIOINFO_ROOT is not defined.')
	
	try:
		config_path = options.config_path if options.config_path else join(bioinfo_root, 'etc', 'queue.conf')
		config = Config.from_file(config_path)
	except ConfigError, e:
		exit('Configuration error: %s' % e.args[0])
	
	if len(args):
		cmd = ' '.join(args)
	else:
		cmd = stdin.read()
		if len(cmd.strip()) == 0:
			exit('Empty command.')
	
	try:
		queue = make_queue(config)
	except ValueError, e:
		exit('Error instantiating queue: %s' % e.args[0])
	
	if options.name:
		name = options.name
	else:
		name = Job.gen_name()
	
	try:
		j = Job(name, cmd, getcwd())
		queue.submit(j, wait=options.wait)
	except QueueError, e:
		exit('%s\n*** %s' % (e.args[0], e.args[1]))

if __name__ == '__main__':
	main()
