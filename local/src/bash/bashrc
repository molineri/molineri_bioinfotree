function interactive_warning {
	if [[ $- == *i* ]]; then
		echo "*!* $1" >&2
		echo "*!* BioinfoTree will not work properly." >&2
	fi
}

function normalize_root {
	export BIOINFO_ROOT="$(cd "$BIOINFO_ROOT" && echo "$PWD")"
	[[ $BIOINFO_NEWROOT ]] && export BIOINFO_NEWROOT="$(cd "$BIOINFO_NEWROOT" && echo "$PWD")"
}

function init_env {
	local bioinfo_path="$BIOINFO_ROOT/local/bin"
	if [[ $PATH != *$bioinfo_path* ]]; then
		export PATH="$BIOINFO_ROOT/binary/$BIOINFO_HOST/local/bin:$bioinfo_path:$PATH"
		export PYTHONPATH="$BIOINFO_ROOT/binary/$BIOINFO_HOST/local/lib/python:$BIOINFO_ROOT/local/lib/python:$PYTHONPATH"
		export R_LIBS_USER="$BIOINFO_ROOT/binary/$BIOINFO_HOST/local/lib/R:$R_LIBS_USER"
		export MANPATH="$BIOINFO_ROOT/binary/$BIOINFO_HOST/local/man:$BIOINFO_ROOT/local/man:$MANPATH"

		case $(uname -s) in
			Darwin)
				export DYLD_LIBRARY_PATH="$BIOINFO_ROOT/binary/$BIOINFO_HOST/local/bin:$BIOINFO_ROOT/local/lib:$DYLD_LIBRARY_PATH";;
			Linux | *)
				export LD_LIBRARY_PATH="$BIOINFO_ROOT/binary/$BIOINFO_HOST/local/lib:$BIOINFO_ROOT/local/lib:$LD_LIBRARY_PATH";;
		esac
	fi
}

function pivot_env {
	if [[ $PATH != *$BIOINFO_ROOT/* ]]; then
		init_env
	else
		local old_root="$BIOINFO_ROOT/"
		local new_root="$BIOINFO_NEWROOT/"
		export PATH="${PATH//$old_root/$new_root}"
		export PYTHONPATH="${PYTHONPATH//$old_root/$new_root}"
		export R_LIBS_USER="${R_LIBS_USER//$old_root/$new_root}"
		export MANPATH="${MANPATH//$old_root/$new_root}"

		case $(uname -s) in
			Darwin)
				export DYLD_LIBRARY_PATH="${DYLD_LIBRARY_PATH//$old_root/$new_root}";;
			Linux | *)
				export LD_LIBRARY_PATH="${LD_LIBRARY_PATH//$old_root/$new_root}";;
		esac

		export BIOINFO_ROOT="${BIOINFO_NEWROOT}"
		unset BIOINFO_NEWROOT
	fi
}

function bmake_env {
	if [[ $MAKE_PATH ]]; then
		if [[ $PATH != *$MAKE_PATH* ]]; then
			export PATH="$MAKE_PATH:$PATH"
			export PYTHONPATH="$MAKE_PYTHON_PATH:$PYTHONPATH"
			export MANPATH="$MAKE_MAN_PATH:$MANPATH"

			case $(uname -s) in
				Darwin)
					export DYLD_LIBRARY_PATH="$MAKE_LIBRARY_PATH:$DYLD_LIBRARY_PATH";;
				Linux | *)
					export LD_LIBRARY_PATH="$MAKE_LIBRARY_PATH:$LD_LIBRARY_PATH";;
			esac
		fi
		return
	fi
}

function bchtree {
	if [[ $# -ne 1 ]]; then
		echo "Unexpected argument number." >&2
		return 1
	elif [[ $1 == "-h" ]]; then
		# Weak test, but we cannot use getopts as it pollutes
		# the environment with a number of variables.
		echo "Usage: bchtree NEW_ROOT"
		echo "Sets the environment for an alternative BioinfoTree."
		return 1
	elif [[ ! -d $1 ]]; then
		echo "Not a directory: $1" >&2
		return 1
	elif [[ ! -e $1/local/share/bash/bashrc ]]; then
		echo "Destination does not seem a BioinfoTree: $1" >&2
		return 1
	else
		BIOINFO_NEWROOT="$1" source "$1/local/share/bash/bashrc"
		cd "$1"
		echo "Now using alternative BioinfoTree: $1"
	fi
}


if [[ -z $BIOINFO_ROOT ]]; then
	interactive_warning "BIOINFO_ROOT is not defined."
elif [[ -z $BIOINFO_HOST ]]; then
	interactive_warning "BIOINFO_HOST is not defined."
elif [[ ${BASH_VERSINFO[0]} -lt 3 ]]; then
	interactive_warning "bash version is too old."
else
	normalize_root
	if [[ $BIOINFO_NEWROOT ]]; then
		pivot_env
	else
		init_env
	fi
	bmake_env

	set -o pipefail
	[[ $- == *i* ]] && alias cdb="cd $BIOINFO_ROOT"
fi
