\name{saferequire}
\alias{saferequire}
\title{Safely Require Packages}
\description{
	Requires a package. Exits with an informative error message in case it can't be found.
}
\usage{
	saferequire(pkgname, description)
}
\arguments{
	\item{pkgname}{a string holding the name of the required package.}
	\item{description}{a short description of the package that will be shown to the user in case of error.
                           If NULL, it will be set to pkgname.}
}
\seealso{
	\code{\link[base]{library}}.
}
\examples{
require("saferequire", quietly=TRUE)
saferequire("mypackage", "package description")
}