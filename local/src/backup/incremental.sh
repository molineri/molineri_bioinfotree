#!/bin/bash
#
# Copyright 2009 Gabriele Sales <gbrsales@gmail.com>

function error {
	[[ $do_cleanup == 1 ]] && cleanup
	echo "[bioinfotree backup] $1" >&2
	exit 1
}

function cleanup {
	echo "[bioinfotree backup] Cleaning up..." >&2
	[[ -d "$VOLUME/inprogress" ]] && rm -rf "$VOLUME/inprogress"
	dotlockfile -u "$LOCKFILE"
}

# Check environment
if [ -z "$BIOINFO_ROOT" ]; then
	error "BIOINFO_ROOT is not defined."
elif [ -z "$BIOINFO_HOST" ]; then
	error "BIOINFO_HOST is not defined."
elif [ ${BASH_VERSINFO[0]} -lt 3 ]; then
	error "bash version is too old."
fi

# Command line
while getopts "v" flag; do
	if [[ $flag == v ]]; then
		verbose=1
	fi
done

if [[ $# -ge $OPTIND ]]; then
	error "Unexpected argument number."
fi

# Load configuration
conf="$BIOINFO_ROOT/etc/backup.conf"
[ -f "$conf" ] || error "Configuration file 'backup.conf' missing."
source "$conf"

# Acquire the lock and set the cleanup trap
dotlockfile -r1 "$LOCKFILE" || error "Another incremental backup is running. Aborting."
do_cleanup=1
trap cleanup TERM INT

# Check backup volume
umask $UMASK
mkdir -p "$VOLUME/0" || error "Error creating backup volume."

# Push data to the backup volume
exclude=""
if [[ -n $EXCLUDE ]]; then
	for pattern in $EXCLUDE; do
		exclude="$exclude --exclude $pattern"
	done
fi
options=""
[[ $verbose == 1 ]] && options="$options -v --progress"

[[ -d $VOLUME/$LEVELS ]] && mv "$VOLUME/$LEVELS" "$VOLUME/inprogress"
rsync -a --delete $options $exclude --link-dest="$VOLUME/0" "${BIOINFO_ROOT%/}" "$VOLUME/inprogress" || error "rsync error."

# Rotate levels
for level in $(seq $LEVELS -1 1); do
	level_dir="$VOLUME/$(($level-1))"
	if [[ -d $level_dir ]]; then
		mv "$level_dir" "$VOLUME/$level" || error "Error rotating levels."
	fi
done
mv "$VOLUME/inprogress" "$VOLUME/0" || error "Error rotating levels."

# Remove the lock
dotlockfile -u "$LOCKFILE"
