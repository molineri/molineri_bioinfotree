#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;

$,="\t";
$\="\n";

my $usage = "cat qualcosa.binner | $0 [-r]
	-r if the right side of the bin is used as a label
	(default: left side)
	
	tiene conto della larghezza dei bin
	";

my $right_side = undef;
GetOptions (
        'right_side|r' => \$right_side
) or die ($usage);


my @val=();

my $area = 0;
my $x_pre = undef;
my $y = undef;

while(<>){
	chomp;
	my @F=split;
	$y = $F[1] if ($right_side);
	if (defined $x_pre) {
		$area += (abs($F[0] - $x_pre)) * $y;
	}
	push @val,\@F;
	$x_pre = $F[0];
	$y = $F[1] if (!$right_side);
}

for(@val){
	print ${$_}[0],${$_}[1]/$area;	
}
