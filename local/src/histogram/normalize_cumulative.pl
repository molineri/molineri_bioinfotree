#!/usr/bin/perl

use warnings;
use strict;

$,="\t";
$\="\n";


my $max = undef;
my @val = ();
while(<>){
	chomp;
	my @F = split;
	push @val,\@F;
	$max = $F[1];
}

for (@val) {
	print ${$_}[0], ${$_}[1]/$max;
}
