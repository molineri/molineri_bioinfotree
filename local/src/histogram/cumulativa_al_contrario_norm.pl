#!/usr/bin/perl

use warnings;
use strict;

my @row=();
my $sum_tot=0;
while(<>){
	(my $x,my $y)=split;
	$sum_tot+=$y;
	push @row, $_;
}
my $sum=0;
while($_=pop(@row)){
	(my $x,my $y)=split;
	$sum+=$y;
	print $x, "\t",$sum/$sum_tot,"\n";
}
