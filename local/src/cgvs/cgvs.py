#!/usr/bin/env python

from __future__ import division

from optparse import OptionParser
from sys import exit, maxint, stdin, stdout
from vfork.draw.plot import CGVPlot
from vfork.draw.surface import PNGSurface

def read_stripes(filename):
	with file(filename, 'r') as fd:
		try:
			for lineno, line in enumerate(fd):
				stripe = []
				tokens = line.rstrip().split('\t')
				if len(tokens) not in (2, 6):
					raise ValueError
				stripe.append(int(tokens[0]))
				stripe.append(int(tokens[1]))
				if len(tokens)>2:
					stripe.append(tuple(float(c) for c in tokens[2:]))
				else:
					stripe.append( ( 0.5, 0.5, 0.5, 0.5 ) );
				yield(stripe)
		except ValueError:
			exit('Malformed input at line %d: %s of file (%s)' % (lineno+1, line, filename))


def read_segments(fd, x_min, x_max, y_min, y_max):
	segments = []
	space_bounds = [maxint, 0, maxint, 0]
	
	try:
		for lineno, line in enumerate(fd):
			tokens = line.rstrip().split('\t')
			
			if len(tokens) not in (4, 7):
				raise ValueError
			
			coords = [ int(i) for i in tokens[:4] ]
			
			box_x_min = min(coords[0], coords[1])
			box_x_max = max(coords[0], coords[1])
			box_y_min = min(coords[2], coords[3])
			box_y_max = max(coords[2], coords[3])
			
			if x_min is not None and box_x_max <= x_min:
				continue
			elif x_max is not None and box_x_min >= x_max:
				continue
			elif y_min is not None and box_y_max <= y_min:
				continue
			elif y_max is not None and box_y_min >= y_max:
				continue
			
			if box_x_min < space_bounds[0]:
				space_bounds[0] = box_x_min
			if box_x_max > space_bounds[1]:
				space_bounds[1] = box_x_max
			if box_y_min < space_bounds[2]:
				space_bounds[2] = box_y_min
			if box_y_max > space_bounds[3]:
				space_bounds[3] = box_y_max
			
			if len(tokens) > 4:
				coords.append(tuple(float(c) for c in tokens[4:]))
			else:
				coords.append(None)
			
			segments.append(coords)
	
	except ValueError:
		exit('Malformed input at line %d: %s' % (lineno+1, line))
	
	if x_min is not None:
		space_bounds[0] = x_min
	if x_max is not None:
		space_bounds[1] = x_max
	if y_min is not None:
		space_bounds[2] = y_min
	if y_max is not None:
		space_bounds[3] = y_max
	
	return segments, space_bounds

'''def rescale_segments(segments, space_bounds, image_width, image_height, fixed_aspect_ratio):
	x_shift = space_bounds[0]
	y_shift = space_bounds[2]
	x_scale = image_width / (space_bounds[1] - x_shift)
	y_scale = image_height / (space_bounds[3] - y_shift)
	
	if fixed_aspect_ratio and x_scale != y_scale:
		if x_scale > y_scale:
			x_scale = y_scale
		else:
			y_scale = x_scale
	
	for segment in segments:
		segment[0] = (segment[0] - x_shift) * x_scale
		segment[1] = (segment[1] - x_shift) * x_scale
		segment[2] = (segment[2] - y_shift) * y_scale
		segment[3] = (segment[3] - y_shift) * y_scale
	
	return segments'''

def main():
	parser = OptionParser(usage='%prog <SEGMENTS')
	parser.add_option('-a', '--x-min', type='int', dest='x_min', help='minimum x coordinate of the drawing rectangle')
	parser.add_option('-b', '--x-max', type='int', dest='x_max', help='maximum x coordinate of the drawing rectangle')
	parser.add_option('-c', '--y-min', type='int', dest='y_min', help='minimum y coordinate of the drawing rectangle')
	parser.add_option('-d', '--y-max', type='int', dest='y_max', help='maximum y coordinate of the drawing rectangle')
	parser.add_option('-f', '--fixed-aspect-ratio', action='store_true', dest='fixed_aspect_ratio', default=False, help='use a fixed aspect ratio of 1:1')
	parser.add_option('-i', '--width', type='int', dest='width', default=800, help='the width of the PNG image (default: 800)')
	parser.add_option('-j', '--height', type='int', dest='height', default=800, help='the height of the PNG image (default: 800)')
	parser.add_option('-k', '--background', action='store_true', dest='background', default=False, help='use a white background (by default it\'s transparent)')
	parser.add_option('--sh', '--stripes-horizontal', type='string', dest='stripes_horizontal', default=None, help='flie containing the horizonltal stripes')
	parser.add_option('--sv', '--stripes-vertical', type='string', dest='stripes_vertical', default=None, help='flie containing the vertical stripes')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	elif options.x_min is not None and options.x_min < 0:
			exit('Invalid minimum x coordinate.')
	elif options.x_max is not None and options.x_max < 1:
		exit('Invalid maximum x coordinate.')
	elif options.x_min is not None and options.x_max is not None and options.x_min >= options.x_max:
		exit('Invalid maximum x coordinate.')
	elif options.y_min is not None and options.y_min < 0:
		exit('Invalid minimum y coordinate.')
	elif options.y_max is not None and options.y_max < 1:
		exit('Invalid maximum y coordinate.')
	elif options.y_min is not None and options.y_max is not None and options.y_min >= options.y_max:
		exit('Invalid maximum y coordinate.')
	elif options.fixed_aspect_ratio and options.x_min is not None and options.x_max is not None and options.y_min is not None and options.y_max is not None and \
	     (options.x_max - options.x_min) != (options.y_max - options.y_min):
		exit('The provided coordinates don\'t allow a fixed aspect ratio.')
	
	segments, space_bounds = read_segments(stdin, options.x_min, options.x_max, options.y_min, options.y_max)
	if len(segments) == 0:
		exit('Nothing to do.')
	
	if options.fixed_aspect_ratio:
		exit('Fixed aspect ratio is not yet supported.')
	
	surface = PNGSurface(options.width, options.height)
	if options.background:
		surface.rgba = (1,1,1)
		surface.fill_rectangle(0, 0, options.width, options.height)
	
	plot = CGVPlot(space_bounds[0], space_bounds[2], space_bounds[1]-space_bounds[0], space_bounds[3]-space_bounds[2], surface)
	black = (0,0,0)
	for segment in segments:
		color = segment[4]
		plot.draw_segment(segment[0], segment[1], segment[2], segment[3], color if color is not None else black)
	
	if(options.stripes_horizontal):
		for b,e,rgba in read_stripes(options.stripes_horizontal):
			plot.draw_horizontal_region(b,e-b,rgba)	

	if(options.stripes_vertical):
		for b,e,rgba in read_stripes(options.stripes_vertical):
			plot.draw_vertical_region(b,e-b,rgba)

	surface.write_to_file(stdout)

if __name__ == '__main__':
	main()
