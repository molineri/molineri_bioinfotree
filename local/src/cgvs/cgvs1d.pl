#!/usr/bin/perl
use warnings;
use Getopt::Long;
use strict;
$,="\t";
$\="\n";

my $usage = "$0 [-c coverage_filename] cgvs options and args < coords";

my $coverage_filename='';
GetOptions (
	'coverage|c=s' => \$coverage_filename
) or die($usage);

if ($coverage_filename){
	open COV, "$coverage_filename" or die("can't open file ($coverage_filename)");
}

my $options = '-k ';

$options .= join " ",@ARGV;


open PIPE,"| cgvs $options" or die("Can't open PIPE");

my @data=();
while(<STDIN>){
	chomp;
	my @F=split;
	die if scalar(@F)!=2;
	push @data,\@F;
}

@data = sort {
	(${$a}[0] + ${$a}[1])/2 <=> (${$b}[0] + ${$b}[1])/2
}  @data;

my $space=5;

my $min=undef;
my $i=1;
for(@data){
	my @F=@{$_};
	print PIPE @F,$i*5,$i*5;
	$i++;
	$min = $F[0] if !defined($min) or $F[0] < $min ;
}

print PIPE $min, $min + 30, $i*$space, $i*$space, 0, 0 , 1;


if($coverage_filename){

	my @data_cov=();
	my $max=0;
	my $trash = <COV>;
	while(<COV>){
		chomp;
		my @F = split;
		push @data_cov, \@F;
		$max=$F[1] if $max < $F[1];
	}

	my $scale = ($i*$space)/$max;

	my $pre_x=$min;
	my $pre_y=0;
	for(@data_cov){
		my ($x,$y)=@{$_};
		print PIPE $pre_x, 	$x, int($pre_y * $scale), int($pre_y * $scale), 	1, 0, 0;
		print PIPE $x, 		$x, int($pre_y * $scale), int($y * $scale), 		1, 0, 0;
		$pre_x=$x;
		$pre_y=$y;
	}
}
