#!/usr/bin/env python
#
# Copyright 2010 Gabriele Sales <gbrsales@gmail.com>

from optparse import OptionParser
from sys import stdin
from vfork.util import format_usage, exit, ignore_broken_pipe
from xml.sax import make_parser, SAXParseException
from xml.sax.handler import ContentHandler, feature_namespaces

class ModelError(ValueError): pass

class UserModel(ContentHandler):
    def __init__(self, reader, writer, fields):
        self.reader = reader
        self.writer = writer
        self.fields = fields

    def startDocument(self):
        self.stack = []
    
    def startElement(self, name, attrs):
        self.stack.append(name)
        if len(self.stack) == 2:
            self.writer.start_from_user_model(name, self.fields)
            self.reader.setContentHandler(self.writer)

class ModelGenerator(ContentHandler):
    def __init__(self, reader, writer):
        self.reader = reader
        self.writer = writer

    def startDocument(self):
        self.stack = []
        self.record = None
        self.fields = []
        self.values = []
    
    def startElement(self, name, attrs):
        self.stack.append(name)
        level = len(self.stack)
        if level == 2:
            self.record = name
        elif level == 3:
            self.fields.append(name)
        elif level > 3:
            raise ModelError, 'too many nested levels'
        
    def endElement(self, name):
        self.stack.pop()
        if len(self.stack) == 1:
            self.writer.start_from_generated_model(name, self.fields, self.values)
            self.reader.setContentHandler(self.writer)
        
    def characters(self, content):
        if len(self.stack) != 3: return
            
        if len(self.values) < len(self.fields):
            self.values.append(content)
        else:
            old = self.values.pop()
            self.values.append(old + content)


class WriterError(ValueError): pass

class Writer(ContentHandler):
    def __init__(self, missing_proc, ignore_unknown):
        self.missing_proc = missing_proc
        self.ignore_unknown = ignore_unknown
    
    def start_from_user_model(self, container_name, fields):
        self._init_params(container_name, fields)
        self.startElement(container_name, None)

    def start_from_generated_model(self, container_name, fields, values):
        self._init_params(container_name, fields)
        print '\t'.join(values)

    def _init_params(self, container_name, fields):
        self.container_name = container_name
        self.field_map = dict(zip(fields, range(len(fields))))
        self.stack = [None]
        self.values = None
        self.vidx = None

    def startElement(self, name, attrs):
        level = len(self.stack)
        if level == 1:
            if name != self.container_name:
                raise WriterError, 'unexpected element "%s"' % name
            self.values = [None for i in range(len(self.field_map))]
        elif level == 2:
            try:
                self.vidx = self.field_map[name]
                if self.values[self.vidx] is not None:
                    raise WriterError, 'repeated field "%s"' % name
            except KeyError:
                if not self.ignore_unknown:
                    raise WriterError, 'unexpected field "%s"' % name
        else:
            raise WriterError, 'too many nested levels'
        
        self.stack.append(name)

    def endElement(self, name):
        self.stack.pop()
        self.vidx = None

        if len(self.stack) == 1:
            values = self.missing_proc(self.values)
            if values is not None:
                print '\t'.join(values)
    
    def characters(self, content):
        if self.vidx is not None:
            old = self.values[self.vidx]
            self.values[self.vidx] = (old+content) if old is not None else content


def replace_missing(default):
    def applyDefault(v):
        return v if v is not None else default

    def proc(values):
        return [ applyDefault(v) for v in values ]

    return proc

def stop_on_missing(values):
    if None in values:
        raise WriterError, 'some fields are missing'
    else:
        return values

def drop_missing(values):
    return None if None in values else values


def main():
    parser = OptionParser(usage=format_usage('''
      %prog [OPTIONS] [FIELD...] <XML >TSV

      Converts an XML document containing tabular data into a tab-delimited format.

      If the field list is not provided explicitly, the program builds it from the
      first child of the root element of the XML document. It then scans the rest
      of the document looking for the same fields, ignoring their order (it always
      uses the order of the first child).

      Missing fields are reported as an error unless either the --allow-missing or
      the --drop-missing options are used. Unknown fields are not reported as
      errors when using the --allow-unknown option: their values, however, are not
      printed out.

      Node attributes and namespaces are ignored.
    '''))
    parser.add_option('-m', '--allow-missing', dest='allow_missing', action='store_true', default=False, help='allow missing fields')
    parser.add_option('-d', '--default-value', dest='default_value', default='', help='placeholder for missing values; the empty string by default')
    parser.add_option('-x', '--drop-missing', dest='drop_missing', action='store_true', help='drop records with missing fields')
    parser.add_option('-u', '--ignore-unknown', dest='ignore_unknown', action='store_true', default=False, help='allow unknown fields')
    options, args = parser.parse_args()

    if options.allow_missing and options.drop_missing:
        exit('Cannot use --allow-missing and --drop-missing at the same time.')

    if options.allow_missing:
        missing_proc = replace_missing(options.default_value)
    elif options.drop_missing:
        missing_proc = drop_missing
    else:
        missing_proc = stop_on_missing

    if len(args) > 0:
        options.ignore_unknown = True

    reader = make_parser()
    writer = Writer(missing_proc, options.ignore_unknown)
    if len(args) == 0:
        model = ModelGenerator(reader, writer)
    else:
        model = UserModel(reader, writer, args)

    reader.setContentHandler(model)
    reader.setFeature(feature_namespaces, False)

    try:
        reader.parse(stdin)
    except ModelError, e:
        exit('Error building data model: ' + e.args[0] + '.')
    except WriterError, e:
        exit('Error converting data: ' + e.args[0] + '.')
    except SAXParseException, e:
        exit('Parsing error: ' + e.getMessage())

if __name__ == '__main__':
    ignore_broken_pipe(main)
