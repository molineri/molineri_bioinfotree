#!/usr/bin/perl
# This script will convert a bed file to a GFF file for use in the genome browser
# The bed file is described at http://genome.ucsc.edu/FAQ/FAQformat#format1
# the contents include the following in this order:
#   chromosome, start (0-based), stop (exclusive), name, score, strand, etc.


#print "\n\t This program will convert *.bed files to a *.gff v2 file\n";

my $type = "type";
if ($type =~ /\s/) {die("Can't have whitespace in $type\n") }
my $source = "source";

print "##gff-version 2\n";
print "# generated using program $0\n";
# Do the conversion
my @output;
while (<>) {
	my $line = $_;	
	if ($line =~ /^track/i) {next} # skip the track definition line
	chomp $line;
	my @data = split /\t/, $line;
	my $refseq = $data[0];
	my $start = $data[1] + 1; # need to shift from 0-based indexing
	my $end = $data[2] - 1; # need to shift from exclusive number to an inclusive number
	my $score;
	# score is optional in the bed format
	if ($data[4]) { $score = $data[4] } else { $score = '.' }
	my $strand;
	# strand is optional, but if present is either + or -
	if ($data[5]) { $strand = $data[5] } else { $strand = '.' }
	my $phase = '.'; 
	my ($name, $group);
	# name is optional
	if ($data[3]) { 
		$name = $data[3];
		$group = "$type \"$name\"";
	} else {
		$group = "Experiment \"$type\"";
	}
	print "$refseq\t$source\t$type\t$start\t$end\t$score\t$strand\t$phase\t$group\n";
}
close INFILE;

# Output
