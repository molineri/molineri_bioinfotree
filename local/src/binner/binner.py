#!/usr/bin/env python
from __future__ import with_statement
from math import ceil, floor, exp, log10
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit
from sys import stdin

def input_iterator(fd):
	for lineno, line in enumerate(fd):
		try:
			value = float(line.rstrip())
		except ValueError:
			exit('Invalid input at line %d: %s' % (lineno+1, line.rstrip()))
		else:
			yield value


class Counter(object):
	def __init__(self, log_scale=False, bin_num=None, bin_size=None, coord_min=None, coord_max=None):
		assert bin_num is not None or bin_size is not None, 'one of bin_num and bin_size must be non-null'
		self.log_scale = log_scale
		self.bin_num = bin_num
		self.bin_size = bin_size
		self.coord_min = coord_min
		self.coord_max = coord_max
	
	def load_values(self, iter):
		raise NotImplementedError
	
	def iter_counts(self, bin_side, write_bin_fd=None):
		reduced_coord_min = log10(self.coord_min) if self.log_scale else self.coord_min
		self._finalize_counts()
		
		for bin_idx, bin_count in enumerate(self.bins):
			coord = reduced_coord_min + (bin_idx * self.bin_size)
			
			if(write_bin_fd is not None):
				coords = [coord, coord + self.bin_size]
				if self.log_scale: 
					coords = [10**i for i in coords]
				print >> write_bin_fd, '%g\t%g' % tuple(coords)

			if bin_side == 'm':
				coord += self.bin_size / 2
			elif bin_side == 'r':
				coord += self.bin_size
			
			if self.log_scale:
				coord = 10**coord

			
			yield coord, bin_count
	
	def _compute_bin_params(self):
		if self.log_scale:
			span = log10(self.coord_max) - log10(self.coord_min)
		else:
			span = self.coord_max - self.coord_min
		
		if self.bin_num:
			self.bin_size = float(span) / self.bin_num
		elif self.bin_size:
			self.bin_num = int(ceil(span / self.bin_size))
	
	def _process_input(self, iter):
		bin_idx_max = self.bin_num - 1
		reduced_coord_min = self.coord_min
		
		if self.log_scale:
			reduced_coord_min = log10(self.coord_min)
		
		for value in iter:
			if self.coord_min <= value <= self.coord_max:
				if self.log_scale:
					value = log10(value)
				bin_idx = int(floor((value - reduced_coord_min) / self.bin_size))
				self.bins[min(bin_idx, bin_idx_max)] += 1

class InMemoryCounter(Counter):
	def __init__(self, **kwargs):
		Counter.__init__(self, **kwargs)
		self.values = []
	
	def load_values(self, iter):
		observed_coord_min = None
		observed_coord_max = None
		
		for value in iter:
			if self.coord_min is None:
				if observed_coord_min is None or observed_coord_min > value:
					observed_coord_min = value
			elif value < self.coord_min:
				continue
			
			if self.coord_max is None:
				if observed_coord_max is None or observed_coord_max < value:
					observed_coord_max = value
			elif value > self.coord_max:
				continue
			
			self.values.append(value)
		
		if self.coord_min is None:
			self.coord_min = observed_coord_min
		if self.coord_max is None:
			self.coord_max = observed_coord_max
	
	def _finalize_counts(self):
		self._compute_bin_params()
		self.bins = [0] * self.bin_num
		self._process_input(self.values.__iter__())
		del self.values

class GivenBinsCounter(Counter):
	def __init__(self, predef_bins, log_scale = None):
		self.log_scale = log_scale
		self.predef_bins = predef_bins
		self.bins=[]
		for b in predef_bins:
			self.bins.append([b,0])

	def load_values(self, iter):
		for value in iter:
			set = False
			for id, bin in enumerate(self.predef_bins):
				if value >= bin[0] and value < bin[1]: #primo estremo compreso, ultimo escluso (tranne l'ultimo bin, che e` chiuso ad entrambi gli estremi)
					set = True
					self.bins[id][1]+=1
			if not set:
				self.bins[-1][1]+=1


	def _finalize_counts(self):
		pass
	
	def iter_counts(self, bin_side, write_bin_fd=None):
		for coords, bin_count in self.bins:
			if(write_bin_fd is not None):
				print >> write_bin_fd, '%g\t%g' % (coords)

			if bin_side == 'm':
				coord = sum(coords) / 2
			elif bin_side == 'r':
				coord = coords[1]
			else:
				coord = coords[0]
			
			yield coord, bin_count


def load_bin_from_file(filename):
	retval=[]
	with file(filename, 'r') as fd:
		for line in fd:
			tokens = safe_rstrip(line).split('\t')
			assert len(tokens) == 2
			retval.append(tuple([float(i) for i in tokens]))
	return retval


class OutOfMemoryCounter(Counter):
	def __init__(self, **kwargs):
		Counter.__init__(self, **kwargs)
		assert self.coord_min is not None, 'missing minimum coordinate'
		assert self.coord_max is not None, 'missing maximum coordinate'
		self._compute_bin_params()
		self.bins = [0] * self.bin_num
		self.load_values = self._process_input
	
	def _finalize_counts(self):
		pass

if __name__ == '__main__':
	parser = OptionParser(usage='%prog [DATA_FILE]')
	parser.add_option('-a', '--max', dest='coord_max', type='float', default=None, help='maximum coordinate showed in the graph', metavar='MAX')
	parser.add_option('-b', '--bin_label', dest='bin_label', default='l', help='what side of the bin to use as label (l [histeps], m [steps], r); by default l')
	parser.add_option('-i', '--min', dest='coord_min', type='float', default=None, help='minimum coordinate showed in the graph', metavar='MIN')
	parser.add_option('-l', '--log', dest='log_scale', action='store_true', default=False, help='log scale binning')
	parser.add_option('-n', '--bin-num', dest='bin_num', type='int', default=None, help='number of bins (default: 10)', metavar='BINNUM')
	parser.add_option('-m', '--out-of-memory', dest='out_of_memory', action='store_true', default=False, help='enable out of memory mode (for big datasets)')
	parser.add_option('-s', '--bin-size', dest='bin_size', type='float', default=None, help='size of each bin', metavar='BINSIZE')
	parser.add_option('-f', '--bin-from-file', dest='bin_from_file', type='string', default=None, help='file representing bin', metavar='BINFILE')
	parser.add_option('-w', '--bin-in-file', dest='bin_in_file', type='string', default=None, help='write bin on file', metavar='BINFILE')
	options, args = parser.parse_args()
	
	if len(args) > 1:
		exit('Unexpected argument number.')
	elif None not in (options.bin_num, options.bin_size):
		exit("Can't use both bin-num and bin-size options.")
	elif options.bin_num is not None and options.bin_num <= 0:
		exit('Invalid bin number value.')
	elif options.bin_size is not None and options.bin_size <= 0:
		exit('Invalid bin size value.')
	elif options.bin_size is not None and options.log_scale:
		exit("You can't set bin size in log scale mode.")
	elif options.out_of_memory and None in (options.coord_min, options.coord_max):
		exit("Out of memory mode requires you to provide minimum and maximum coordinate values.")
	elif options.out_of_memory and options.bin_from_file:
		exit("-out-of-memory (l) and -bin-from-file (f) are incompatible.")
	elif options.log_scale and options.bin_from_file:
		exit("-log-scale (l) and -bin-from-file (f) are incompatible.")
	elif options.bin_num and options.bin_from_file:
		exit("-bin_num (n) and -bin-from-file (f) are incompatible.")
	
	bin_label = options.bin_label.lower()
	if bin_label not in ('l', 'm', 'r'):
		exit("Invalid bin label value.")
	
	if options.bin_num is None and options.bin_size is None and options.bin_from_file is None:
		options.bin_num = 10
	
	counter = None
	
	if options.out_of_memory:
		counter_class = OutOfMemoryCounter
	elif options.bin_from_file:
		counter_class = GivenBinsCounter
		counter = counter_class(predef_bins = load_bin_from_file(options.bin_from_file), log_scale=options.log_scale)
	else:
		counter_class = InMemoryCounter
	
	if len(args):
		iter = input_iterator(file(args[0], 'r'))
	else:
		iter = input_iterator(stdin)
	
	if not options.bin_from_file:
		counter = counter_class(log_scale=options.log_scale, bin_num=options.bin_num, bin_size=options.bin_size, coord_min=options.coord_min, coord_max=options.coord_max)

	counter.load_values(iter)
	
	write_bin_fd = None
	if options.bin_in_file:
		write_bin_fd = file(options.bin_in_file, 'w')

	for bin_start, count in counter.iter_counts(bin_label, write_bin_fd):
		print '%g\t%d' % (bin_start, count)
