#!/usr/bin/env python
from array import array
from math import ceil, exp, log10
from optparse import OptionParser
from sys import exit, maxint, stdin

def load_values(fdi, log_scale):
	values = []
	coord_min = maxint
	coord_max = 0

	for lineno, line in enumerate(fd):
		try:
			value = float(line)
		except ValueError:
			exit('Invalid input at line %d.' % (lineno+1))

		if log_scale:
			value = log10(value)
		
		values.append(value)
		coord_min = min(coord_min, value)
		coord_max = max(coord_max, value)
	
	values.sort()
	return values, coord_min, coord_max

def compute_bins(options, span):
	if options.bin_num is None and options.bin_size is None:
		options.bin_num = 10
	
	span = float(span)
	if options.bin_num:
		options.bin_size = span / options.bin_num
	elif options.bin_size:
		options.bin_num = int(ceil(span / options.bin_size))

def bin_coords(options, coord_min):
	bin_num = int(options.bin_num)
	bin_size = float(options.bin_size)
	
	bin_idx = 0
	bin_end = coord_min
	while bin_idx < bin_num:
		bin_idx += 1
		bin_start = bin_end
		bin_end = coord_min + (bin_idx * bin_size)
		
		if bin_idx == bin_num:
			bin_end += 1
		yield bin_start, bin_end

if __name__ == '__main__':
	parser = OptionParser(usage='%prog [DATA_FILE]')
	parser.add_option('-a', '--max', dest='coord_max', type='float', default=None, help='maximum coordinate showed in the graph', metavar='MAX')
	parser.add_option('-i', '--min', dest='coord_min', type='float', default=None, help='minimum coordinate showed in the graph', metavar='MIN')
	parser.add_option('-n', '--bin-num', dest='bin_num', type='int', default=None, help='number of bins (default: 10)', metavar='BINNUM')
	parser.add_option('-s', '--bin-size', dest='bin_size', type='float', default=None, help='size of each bin', metavar='BINSIZE')
	parser.add_option('-l', '--log', dest='log_scale', action='store_true', default=False, help='log scale binning')
	options, args = parser.parse_args()
	
	if len(args) > 1:
		exit('Unexpected argument number.')
	elif None not in (options.bin_num, options.bin_size):
		exit("Can't use both bin-num and bin-size options.")
	elif options.bin_num is not None and options.bin_num <= 0:
		exit('Invalid bin number value.')
	elif options.bin_size is not None and options.bin_size <= 0:
		exit('Invalid bin size value.')
	elif options.bin_size is not None and options.log_scale:
		exit("You can't set bin size in log scale mode.")
	
	if len(args):
		fd = file(args[0], 'r')
	else:
		fd = stdin
	
	try:
		values, coord_min, coord_max = load_values(fd, options.log_scale)
	finally:
		fd.close()
	
	if options.coord_min is not None:
		coord_min = int(options.coord_min)
	if options.coord_max is not None:
		coord_max = int(options.coord_max)
	compute_bins(options, coord_max - coord_min)
	
	value_idx = 0
	for bin_start, bin_end in bin_coords(options, coord_min):
		counter = 0
		while value_idx < len(values) and values[value_idx] < bin_end:
			counter += 1
			value_idx += 1
		
		if options.log_scale:
			bin_start = 10**bin_start
		print '%f\t%d' % (bin_start, counter)
