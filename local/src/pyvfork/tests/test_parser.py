from cStringIO import StringIO
from vfork.alignment.parser import Alignment, GapScanner, LalignParser, ParseError, WuBlastParser, BlastzParser
import unittest as ut	

class TestGapScanner(ut.TestCase):
	def testUngapped(self):
		gs = GapScanner()
		gs.feed('ACGTACGTACGT')
		gs.feed('ACGTACGTACGT')
		self.assertEquals(gs.finalize(), [])
	
	def testSingleGap(self):
		gs = GapScanner()
		gs.feed('ACGT---ACGT')
		gs.feed('ACGTACGTACG')
		self.assertEquals(gs.finalize(), [(4, 3)])
	
	def testSingleStartGap(self):
		gs = GapScanner()
		gs.feed('---ACGTACGT')
		gs.feed('ACGTACGTACG')
		self.assertEquals(gs.finalize(), [(0, 3)])
	
	def testSingleEndGap(self):
		gs = GapScanner()
		gs.feed('ACGTACGT---')
		gs.feed('ACGTACGTACG')
		self.assertEquals(gs.finalize(), [(8, 3)])
	
	def testMultiGap(self):
		gs = GapScanner()
		gs.feed('--ACGT--AC')
		gs.feed('ACG--CTAGA')
		self.assertEquals(gs.finalize(), [(0,2), (6,2), (13,2)])
	
	def testSpanningGap(self):
		gs = GapScanner()
		gs.feed('ACGT--')
		gs.feed('--ACGT')
		self.assertEquals(gs.finalize(), [(4, 4)])
	
	def testBigGap(self):
		gs = GapScanner()
		gs.feed('ACGT--')
		gs.feed('------')
		gs.feed('--ACGT')
		self.assertEquals(gs.finalize(), [(4,10)])

lalign_output1 = '''
 Comparison of:
(A) /tmpBVAZaL chr21:10000000:10005000                            - 1608 nt
(B) /tmpB25-ZT chr22:15480000:15490000                            - 6110 nt
 using matrix file: DNA (5/-4), gap-open/ext: -12/-4 E(limit)   0.05

  63.2% identity in 106 nt overlap (945-1048:5370-5473); score:  155 E(10000):  0.002

          950       960       970       980       990      1000    
chr21: TGAATTCTTGCTATATTGTTTACAGATGTAACATAATCACTTTGAAAATTAAACATAATT
       :::::  ::  :::::: :  :   :  :::   :::  :::: :::::::    :::::
chr22: TGAATATTT--TATATTTTAGAATAAGTTAATCAAATTTCTTTTAAAATTATGTTTAATT
    5370        5380      5390      5400      5410      5420       

         1010      1020      1030        1040        
chr21: TTAAAAGAAATTTGATTAACTTATTCTAAAATATA--AAATATTCA
       :: ::::  :::   :::  ::  :  : ::::::  ::  :::::
chr22: TTCAAAGTGATTATGTTACATTTGTAAACAATATAGCAAGAATTCA
      5430      5440      5450      5460      5470   

----------

  60.7% identity in 178 nt overlap (1377-1545:4856-5027); score:  152 E(10000): 0.0036

       1380      1390      1400      1410        1420      1430    
chr21: TCTAAGAGCTTGCATTTAGGTTGTCTCTGAAAAG--AGTCTGATGAATAACTTAACAATA
       ::::: ::::  : ::::: :  : : ::::: :  ::   : ::: : : ::::::  :
chr22: TCTAACAGCTA-CTTTTAGATATTTT-TGAAATGTGAGAGGGGTGAGTTAGTTAACACAA
        4860       4870      4880       4890      4900      4910   

         1440      1450      1460      1470      1480      1490    
chr21: AGAAAGTGAATGCTGAGATTTTAGAGTGAACAGACATTTTAATCTCACAGTCTACTTTTG
       :: :   :: :: :::::::  :  ::  :   ::  :  :::::::   :  :::::  
chr22: AGTA---GACTG-TGAGATTAAAATGTCTATTCACTCTAAAATCTCAGCATTCACTTTCT
             4920       4930      4940      4950      4960         

            1500      1510        1520       1530       1540     
chr21: TGTT---AACTCACCCCTCTCACA--TTTCAAAAATATC-TAAAAGTA-GCTGTTAGA
       : ::   :: : :  : ::  ::   ::::: : : : : :::: : : ::: :::::
chr22: TATTGTCAAGTTATTCATCAGACTCTTTTCAGAGACAACCTAAATGCAAGCTCTTAGA
    4970      4980      4990      5000      5010      5020       

----------
 Comparison of:
(A) /tmpBVAZaL (rev-comp) chr21:10000000:10005000                 - 1608 nt
(B) /tmpB25-ZT chr22:15480000:15490000                            - 6110 nt
 using matrix file: DNA (5/-4), gap-open/ext: -12/-4 E(limit)   0.05


  65.6% identity in 61 nt overlap (122-180:3050-3110); score:   92 E(10000): 3.5e+02

              130       140        150       160       170         
chr21- AGACTGT-GAGATTAAAATG-TCTGTTCACTCTAAAATCTCAGCATTCACTTTCTTATTG
       : ::::: :::::::: ::  ::  : :  :: ::::: :: :  :: : ::: :   ::
chr22: ATACTGTTGAGATTAACATCCTCATTGCCTTCCAAAATATCTGACTTTATTTTTTGTATG
    3050      3060      3070      3080      3090      3100         

     180
chr21- T
       :
chr22: T
    3110

----------
'''

lalign_output2 = '''
 Comparison of:
(A) left.159.fa (rev-comp) ENSG00000162739                          - 14999 nt
(B) right.159.fa ENSMUSG00000015314                                 - 14999 nt
 using matrix file: DNA (5/-4), gap-open/ext: -12/-4 E(limit)     10
'''

class TestLalignParser(ut.TestCase):
	def testParse(self):
		lp = LalignParser()
		it = lp.parse(StringIO(lalign_output1))
		
		parsed_alignment = it.next()
		expected_alignment = Alignment(
			'chr21:10000000:10005000',
			944,
			1048,
			'chr22:15480000:15490000',
			5369,
			5473,
			'+',
			None,
			106,
			155,
			63.2,
			0.002,
			[(95,2)],
			[(9,2)]
		)
		self.assertEquals(parsed_alignment, expected_alignment)

		parsed_alignment = it.next()
		expected_alignment = Alignment(
			'chr21:10000000:10005000',
			1376,
			1545,
			'chr22:15480000:15490000',
			4855,
			5027,
			'+',
			None,
			178,
			152,
			60.7,
			0.0036,
			[(34,2), (124,3), (144,2), (159,1), (168,1)],
			[(11,1), (26,1), (64,3), (72,1)]
		)
		self.assertEquals(parsed_alignment, expected_alignment)		

		parsed_alignment = it.next()
		expected_alignment = Alignment(
			'chr21:10000000:10005000',
			1428,
			1487,
			'chr22:15480000:15490000',
			3049,
			3110,
			'-',
			None,
			61,
			92,
			65.6,
			3.5e2,
			[(7,1), (20,1)],
			[]
		)
		self.assertEquals(parsed_alignment, expected_alignment)
		
		self.assertRaises(StopIteration, it.next)
	
	def testParseEmpty(self):
		lp = LalignParser()
		it = lp.parse(StringIO(lalign_output2))
		self.assertRaises(StopIteration, it.next)

wublast_output1 = '''
BLASTN 2.0MP-WashU [04-May-2006] [linux26-x64-I32LPF64 2006-05-10T17:22:28]

Copyright (C) 1996-2006 Washington University, Saint Louis, Missouri USA.
All Rights Reserved.

Reference:  Gish, W. (1996-2006) http://blast.wustl.edu

Notice:  this program and its default parameter settings are optimized to find
nearly identical sequences rapidly.  To identify weak protein similarities
encoded in nucleic acid, use BLASTX, TBLASTN or TBLASTX.

Query=  chr22_14430000
        (7805 letters; record 1)

Database:  chr21.fa
           1 sequences; 46,944,323 total letters.

WARNING:  Use of the hspsepSmax parameter should be considered with long
          database sequences, to improve the biological relevance of the HSP
          groups that are assembled and to improve the statistical
          discrimination of these groups from random background.
Searching done

                                                                     Smallest
                                                                       Sum
                                                              High  Probability
Sequences producing High-scoring Segment Pairs:              Score  P(N)      N

chr21                                                          612  1.00000   7


>chr21
        Length = 46,944,323

  Plus Strand HSPs:

 Score = 612 (97.9 bits), Expect = 1.0e+03, Sum P(9) = 1.00000
 Identities = 190/252 (75%), Positives = 190/252 (75%), Strand = Plus / Plus

Query:      209 AGGCCGGTTTCTGCTAATTT-CTTTAATTCCAAGACAGTCTCAAATATTTTCTTATTAAC 267
                | || | ||| ||||| ||| |||||||||  ||  ||| ||||||||||| || ||| |
Sbjct: 10173878 AAGCTGATTT-TGCTACTTTTCTTTAATTCTGAGGTAGTTTCAAATATTTTGTTGTTACC 10173936

Query:      268 TTCCTGGAGGGAG----GCTTATCATTCTCT-CTTTTGGATGATTCTAAGTACCAGCTAA 322
                || |||||  ||     | |||||||| ||| |||||||||| || || || ||||||||
Sbjct: 10173937 TTTCTGGAAAGAAATAAGTTTATCATT-TCTACTTTTGGATGTTTTTAGGTGCCAGCTAA 10173995

Query:      323 AATACAGCTATCATTCATTTTCCTTGATTTGGGAGCCTAATTTCTTTAATTTAGTATGCA 382
                |||||| |  ||||| ||||||||||||   | | ||||| |||| |||||||| || | 
Sbjct: 10173996 AATACAACCCTCATTTATTTTCCTTGATGCAGAAACCTAACTTCTCTAATTTAGCATACG 10174055

Query:      383 AGAAAACCAATTTGGAAATATC-AAC--TGTT----TTGGAAACCTTAGACCTAGGTCAT 435
                |||| |||| ||||| |||||  ||   | ||    |||| |||||||||  | |||| |
Sbjct: 10174056 AGAAGACCAGTTTGGGAATATTTAAAGTTCTTCACTTTGGGAACCTTAGATGTTGGTCGT 10174115

Query:      436 CCTTAGTAAGAT 447
                ||||||||| ||
Sbjct: 10174116 CCTTAGTAAAAT 10174127

   Minus Strand HSPs:

 Score = 516 (83.5 bits), Expect = 4.1e+03, Sum P(11) = 1.00000
 Identities = 206/306 (67%), Positives = 206/306 (67%), Strand = Minus / Plus

Query:     2585 TTAATCCA-ATAGCCATTTAAATTGATCTTCAATTAC-TTTGTATGTGGCCAGATATTAT 2528
                |||||||| | |   |||||||  ||  || ||| |  ||||| |||||| |||| |||
Sbjct: 60619249 TTAATCCATAAATTAATTTAAAAAGACATT-AATGAGATTTGTGTGTGGCTAGATGTTAC 60619307

Query:     2527 TTATGGGTTATATTCTAATTCACATTTTTCTGATGTAATTTCAGAGAGCCTAGAAAATAT 2468
                || | | ||| | | | | ||  | |||  |  ||| |||| ||   |||||||||||||
Sbjct: 60619308 TTGTTGATTACAATATTACTCTTAATTTA-TATTGTCATTTTAGTT-GCCTAGAAAATAT 60619365

Query:     2467 CACCTAGTGA-ATATCTACTTCCATGAGGTCAGTGACACATTTTTAGGTCTTCAA-CTTC 2410
                 || | || | || ||||||    |||  | | |||||  | || ||| ||  || |||
Sbjct: 60619366 GAC-TGGTTAGATTTCTACTCTTTTGAAATTAATGACAT-TATTAAGG-CTAAAATCTTG 60619422

Query:     2409 AATATTTAAAATATTTGTCTCTGGAATTTGAAAATAATATGTATGCCTTT-TTTACTGGA 2351
                  |||||||  | |||  |||||| ||||||| | ||| ||||| | ||  |||| |||
Sbjct: 60619423 C-TATTTAATTT-TTTC-CTCTGGTATTTGAATACAATGTGTAT-CTTTGGTTTATTGGC 60619478

Query:     2350 TAAAAAGGCTAATCAGTTGT-TC-ATCTGATTAGTAATCAAAGTTTCTAA-AATTTAATA 2294
                 |||||  |||  || || | || || ||   | || ||||| ||||||| ||  | ||
Sbjct: 60619479 AAAAAATACTATCCATTTTTCTCTATGTGGA-ATTAGTCAAACTTTCTAATAAAGTTATT 60619537

Query:     2293 CTTCAT 2288
                |  |||
Sbjct: 60619538 CAACAT 60619543

  ctxfactor=2.00
  E=4.93307e+07

  Query                        -----  As Used  -----    -----  Computed  ----
  Strand MatID Matrix name     Lambda    K       H      Lambda    K       H
   +1      0   +5,-4           0.192   0.182   0.357    same    same    same
               Q=10,R=10       0.104   0.0151  0.0600    n/a     n/a     n/a
   -1      0   +5,-4           0.192   0.182   0.357    same    same    same
               Q=10,R=10       0.104   0.0151  0.0600    n/a     n/a     n/a

  Query
  Strand MatID  Length  Eff.Length     E    S  W   T   X   E2     S2
   +1      0     2341       446   2.5e+07  30 13 n/a  73  4.9e+02 30
                                                     134  5.0e+02 30
   -1      0     2341       446   2.5e+07  30 13 n/a  73  4.9e+02 30
                                                     134  5.0e+02 30


Statistics:

  Database:  chr21
   Title:  chr21.fa
   Posted:  12:13:19 PM CEST May 30, 2006
   Created:  12:13:19 PM CEST May 30, 2006
   Format:  XDF-1
   # of letters in database:  46,944,323
   # of sequences in database:  1
   # of database sequences satisfying E:  1
  No. of states in DFA:  229 (458 KB)
  Total size of DFA:  494 KB (2066 KB)
  Time to generate neighborhood:  0.00u 0.00s 0.00t   Elapsed:  00:00:00
  No. of threads or processors used:  1
  Search cpu time:  0.16u 0.01s 0.17t   Elapsed:  00:00:00
  Total cpu time:  59038.82u 64.26s 59103.08t   Elapsed:  16:47:16
  Start:  Tue May 30 13:00:29 2006   End:  Wed May 31 05:47:45 2006
WARNINGS ISSUED:  1

EXIT CODE 0
'''

wublast_output2 = '''
BLASTN 2.0MP-WashU [04-May-2006] [linux26-x64-I32LPF64 2006-05-10T17:22:28]

Copyright (C) 1996-2006 Washington University, Saint Louis, Missouri USA.
All Rights Reserved.

Reference:  Gish, W. (1996-2006) http://blast.wustl.edu

Notice:  this program and its default parameter settings are optimized to find
nearly identical sequences rapidly.  To identify weak protein similarities
encoded in nucleic acid, use BLASTX, TBLASTN or TBLASTX.

Query=  AP2_upstream
        (585 letters)

Database:  all.fa
           25 sequences; 3,079,785,051 total letters.

WARNING:  Use of the hspsepSmax parameter should be considered with long
          database sequences, to improve the biological relevance of the HSP
          groups that are assembled and to improve the statistical
          discrimination of these groups from random background.
Searching....10....20....30....40....50....60....70....80....90....100% done

                                                                     Smallest
                                                                       Sum
                                                              High  Probability
Sequences producing High-scoring Segment Pairs:              Score  P(N)      N

chr6                                                          2925  1.4e-124  1
chr18                                                          339  9.1e-08   1
chr8                                                           325  8.2e-06   1
chr10                                                          320  2.0e-05   1
chr13                                                          319  2.3e-05   1
chr21                                                          309  0.00010   1
chr14                                                          306  0.00015   1
chr7                                                           305  0.00017   1
chr16                                                          291  0.0011    1
chr11                                                          284  0.0025    1
chrX                                                           283  0.0029    1
chr19                                                          283  0.0029    1
chr15                                                          279  0.0047    1
chr17                                                          278  0.0053    1
chr22                                                          275  0.0076    1
chr1                                                           274  0.0086    1
chr9                                                           270  0.014     1
chr3                                                           263  0.031     1
chr2                                                           307  0.035     1
chr4                                                           260  0.044     1
chr20                                                          250  0.13      1
chr12                                                          242  0.30      1
chr5                                                           239  0.40      1


>chr6
        Length = 170,896,992

  Plus Strand HSPs:

 Score = 2925 (444.9 bits), Expect = 1.4e-124, P = 1.4e-124
 Identities = 585/585 (100%), Positives = 585/585 (100%), Strand = Plus / Plus

Query:        1 GCTGGGGCAGACCTCGGGATGCAGCGGGGGTCGTGCGTAGCGGGTAGGAGCTGCTTGGGA 60
                ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Sbjct: 10520809 GCTGGGGCAGACCTCGGGATGCAGCGGGGGTCGTGCGTAGCGGGTAGGAGCTGCTTGGGA 10520868

Query:       61 CCGCGTCCGGGCGCTCGTGGGGCTCTCGGCGCGGCAGGCGCTTCCCGGCCCTGTCTGGCC 120
                ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Sbjct: 10520869 CCGCGTCCGGGCGCTCGTGGGGCTCTCGGCGCGGCAGGCGCTTCCCGGCCCTGTCTGGCC 10520928

Query:      121 GCGGGCTCCGCCGCTTCGCAGCCGAGTCTGGGACTCGGGACTCGGCTGCCCGCTGCTGCG 180
                ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Sbjct: 10520929 GCGGGCTCCGCCGCTTCGCAGCCGAGTCTGGGACTCGGGACTCGGCTGCCCGCTGCTGCG 10520988

Query:      181 CGGCTCCTCCATGACCCGGCGCCCTGCCCGGGACCGGCCCGCTCGCTGCACTGGTCCTCT 240
                ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Sbjct: 10520989 CGGCTCCTCCATGACCCGGCGCCCTGCCCGGGACCGGCCCGCTCGCTGCACTGGTCCTCT 10521048

Query:      241 CCTCGGCCTCCTCTTGAGCTTTCCTCGGACAGGGGCACCGTGCGCTCCCCGGCCACCGTC 300
                ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Sbjct: 10521049 CCTCGGCCTCCTCTTGAGCTTTCCTCGGACAGGGGCACCGTGCGCTCCCCGGCCACCGTC 10521108

Query:      301 GCTCTGCCCTCGGCTCTCTGGGGCCGGGGATTCCGGATCGGGAGCCGGGATGCGGGCCGG 360
                ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Sbjct: 10521109 GCTCTGCCCTCGGCTCTCTGGGGCCGGGGATTCCGGATCGGGAGCCGGGATGCGGGCCGG 10521168

Query:      361 GGCGCGCACAGACGCTAGGCGGCCCAGCCCGGGCGCCGCTGGCACCGGCTCGCGGCTCCC 420
                ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Sbjct: 10521169 GGCGCGCACAGACGCTAGGCGGCCCAGCCCGGGCGCCGCTGGCACCGGCTCGCGGCTCCC 10521228

Query:      421 GGCAAAAACCCGTCCGACTGGCTGGGCCACTCCCGCCTGACGCCCCCCCAGATGACACGA 480
                ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Sbjct: 10521229 GGCAAAAACCCGTCCGACTGGCTGGGCCACTCCCGCCTGACGCCCCCCCAGATGACACGA 10521288

Query:      481 CTAGCTCTCCCGGCGTGCTGCCTTTGCCAGTCTACCCGCCAGGTTGTCAGAACAAGCGGC 540
                ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Sbjct: 10521289 CTAGCTCTCCCGGCGTGCTGCCTTTGCCAGTCTACCCGCCAGGTTGTCAGAACAAGCGGC 10521348

Query:      541 ACCTCGCCTCCTGCGTGTCCCTACAGAGCCCCGGCACAATCAGAC 585
                |||||||||||||||||||||||||||||||||||||||||||||
Sbjct: 10521349 ACCTCGCCTCCTGCGTGTCCCTACAGAGCCCCGGCACAATCAGAC 10521393

 Score = 304 (51.7 bits), Expect = 0.00020, P = 0.00020
 Identities = 268/458 (58%), Positives = 268/458 (58%), Strand = Plus / Plus

Query:         7 GCAGACCTCGGGATGCAGCGGGGGTCGTG-CGTAGCGGGTAGGAGCTGCTTGGGACCGC- 64
                 || | |||| || | | |||| |  || | |  ||| ||  | ||| ||  || | ||| 
Sbjct: 143899717 GCTG-CCTCCGGGT-C-GCGGCGCGCGGGGCAGAGCCGG--GCAGCAGCG-GGCAGCGCA 143899770

Query:        65 GTCCG-GGCGCTCGTGGGGCTCTCGGCGCGGCAGGCGCTTC-CCGGCC-CTGTCTGGCCG 121
                 | ||  ||||| || |   | | ||| ||  | | | |  | ||||   | |  ||||||
Sbjct: 143899771 GCCCTCGGCGC-CG-GCCCCACCCGGTGCTCCGGCCCCGCCGCCGGGAGCCGA-TGGCCG 143899827

Query:       122 CGG-GCTCCGCCGCTTCGCAGCCGAGTCTGG--GACTCGGGACTCGGCTGCCCGCTGCTG 178
                  || |  | | |||  ||| || |   | ||  || || || |  ||| | | || || |
Sbjct: 143899828 AGGCGGGCTGGCGCCCCGCGGC-GTCCCCGGTCGA-TCCGGCCG-GGCAGGCGGCGGCGG 143899884

Query:       179 -CGCGGCTCCTCCATGACCC-GGCGCCCTGCCCGGGACCGGCCCGCT-CGCTGCACTGGT 235
                  | | | ||| | | | ||  |||||||  ||||    |||| | || | |||| |||| 
Sbjct: 143899885 TCCCAG-TCCCCGA-GCCCGAGGCGCCCGCCCCGCCGGCGGCGC-CTGCCCTGCGCTGG- 143899940

Query:       236 CCTCTCCTCGGCCTCCT-CTTGAGCTTTCCTCGGACAGGGGCACCGTGCG-C-TCCCCGG 292
                 | || ||  || | |   |    ||   || |  |||| |  |||   || | ||| || 
Sbjct: 143899941 CTTC-CCGGGGACCCGAGCCCCCGCGG-CCGCTCACAGAGCGACCTCTCGTCGTCCTCGA 143899998

Query:       293 CCACCGTC-GCTCTGCCCTCGGCTCTCTGGGGCCGGGGATTCCGGATCGGGAG-C-CGGG 349
                  ||  | | || | || | ||| ||       ||||   | | ||   | | | | ||||
Sbjct: 143899999 GCAGGGGCCGCCC-GCTC-CGGGTCCACATCTCCGGC--T-CAGGTAAGAGCGGCTCGGG 143900053

Query:       350 ATGCGGGCCGGGGCGCGCACAGACGCTAGGC-GGCC-CAGCCCGGGCGCCGCTGGCA-CC 406
                   ||||||||||| | || | | |||  ||| ||   | ||  ||||| ||| | || ||
Sbjct: 143900054 GCGCGGGCCGGGGAGGGC-CGGCCGCG-GGCAGGTGGCCGCGAGGGCGACGCGGCCAACC 143900111

Query:       407 GGCTCGCGGCTCCCGGCAAAAACCCGTCCGACTGGCTG 444
                  |   ||| |  | ||||    |||| | | ||||| |
Sbjct: 143900112 CGGG-GCGCCCGCGGGCACTGGCCCGGCAGGCTGGCGG 143900148

END
'''

wublast_output3 = '''
BLASTN 2.0MP-WashU [04-May-2006] [linux26-i686-ILP32F64 2006-05-09T11:47:08]

Copyright (C) 1996-2006 Washington University, Saint Louis, Missouri USA.
All Rights Reserved.

Reference:  Gish, W. (1996-2006) http://blast.wustl.edu

Notice:  this program and its default parameter settings are optimized to find
nearly identical sequences rapidly.  To identify weak protein similarities
encoded in nucleic acid, use BLASTX, TBLASTN or TBLASTX.

Query=  chr10_195038
        (14,634 letters; record 9)

WARNING:  No precomputed values for Lambda, K and H are available for the
          pam10.4.4 scoring matrix, regardless of the chosen gap penalties.
          Unless overridden on the command line, computed values for ungapped
          alignments will be used, but the reported E-values and P-values may
          be much too low.

Database:  chr12.fa
           1 sequences; 132,289,534 total letters.
Searching done

                                                                     Smallest
                                                              High  Probability
Sequences producing High-scoring Segment Pairs:              Score  P(N)      N


      *** NONE ***



Parameters:
  W=10
  matrix=pam10
  Q=2046
  R=18
  S2=2790
  gapS2=5580
  S=5580
  Y=135374737
  Z=135374737
  kap
  hspmax=0
  cpus=1

  ucdb
  ctxfactor=2.00
  E=0.380942

  Query                        -----  As Used  -----    -----  Computed  ----
  Strand MatID Matrix name     Lambda    K       H      Lambda    K       H
   +1      0   pam10.4.4       0.00693 0.316   0.986    same    same    same
               Q=2046,R=18     0.00693 0.316   0.986     n/a     n/a     n/a
   -1      0   pam10.4.4       0.00693 0.316   0.986    same    same    same
               Q=2046,R=18     0.00693 0.316   0.986     n/a     n/a     n/a

  Query
  Strand MatID  Length  Eff.Length     E      S  W   T    X   E2       S2
   +1      0   14,634 135,374,737      0.19  5580 10 n/a 2002  0.0025  2790
                                                         3535  9.6e-12 5580
   -1      0   14,634 135,374,737      0.19  5580 10 n/a 2002  0.0025  2790
                                                         3535  9.6e-12 5580


Statistics:

  Database:  chr12
   Title:  chr12.fa
   Posted:  7:55:37 PM CEST Jun 09, 2006
   Created:  7:55:37 PM CEST Jun 09, 2006
   Format:  XDF-1
   # of letters in database:  132,289,534  (Z = 135374737)
   # of sequences in database:  1
   # of database sequences satisfying E:  0
  No. of states in DFA:  9778 (688 KB)
  Total size of DFA:  750 KB (2079 KB)
  Time to generate neighborhood:  0.07u 0.00s 0.07t   Elapsed:  00:00:00
  No. of threads or processors used:  1
  Search cpu time:  2.09u 0.18s 2.27t   Elapsed:  00:00:02
  Total cpu time:  2.16u 0.18s 2.34t   Elapsed:  00:01:23
  Start:  Wed Jul 19 10:27:59 2006   End:  Wed Jul 19 10:28:01 2006
WARNINGS ISSUED:  1

EXIT CODE 0

BLASTN 2.0MP-WashU [04-May-2006] [linux26-i686-ILP32F64 2006-05-09T11:47:08]

Copyright (C) 1996-2006 Washington University, Saint Louis, Missouri USA.
All Rights Reserved.

Reference:  Gish, W. (1996-2006) http://blast.wustl.edu

Notice:  this program and its default parameter settings are optimized to find
nearly identical sequences rapidly.  To identify weak protein similarities
encoded in nucleic acid, use BLASTX, TBLASTN or TBLASTX.

Query=  chr10_211225
        (18,382 letters; record 10)

WARNING:  No precomputed values for Lambda, K and H are available for the
          pam10.4.4 scoring matrix, regardless of the chosen gap penalties.
          Unless overridden on the command line, computed values for ungapped
          alignments will be used, but the reported E-values and P-values may
          be much too low.

Database:  chr12.fa
           1 sequences; 132,289,534 total letters.
Searching done

                                                                     Smallest
                                                              High  Probability
Sequences producing High-scoring Segment Pairs:              Score  P(N)      N

chr12                                                        24501  2.3e-58   1


>chr12
        Length = 132,289,534

  Plus Strand HSPs:

 Score = 24501 (246.5 bits), Expect = 2.3e-58, P = 2.3e-58
 Identities = 172/200 (86%), Positives = 172/200 (86%), Strand = Plus / Plus

Query:     5177 CGATCTCAGCTCATGGCAACCTCCGCTTCCCGGGTTCAAGTGATTCTCCAGCCTCAGCCT 5236
                ||||||| |||||  ||||||||||| |||| |||||||| |||||||| ||||||||||
Sbjct: 32053627 CGATCTCGGCTCACTGCAACCTCCGCCTCCCAGGTTCAAGCGATTCTCCTGCCTCAGCCT 32053686

Query:     5237 CCCAAGTAGCTGGGATTACAGGCACCCGCCACCACGCCCAGCTAATTTTTTGTATTTTTA 5296
                ||  ||||||||||| ||||||| |||||| |||||||  |||||||||| || ||||||
Sbjct: 32053687 CCTGAGTAGCTGGGACTACAGGCGCCCGCCTCCACGCCTGGCTAATTTTTCGTGTTTTTA 32053746

Query:     5297 GTAGAGAGAGGGTTTCACCATGTTGGCCAGGCTGATCTCAAACTCCTGACCTCGTGATTC 5356
                |||||||  |||||||||| |||| |||||| || |||||||  | |||||||||||| |
Sbjct: 32053747 GTAGAGACGGGGTTTCACCGTGTTAGCCAGGATGGTCTCAAAAGCATGACCTCGTGATCC 32053806

Query:     5357 GCCCGCCTTGGCGTCCCAAA 5376
                |||||||| ||| |||||||
Sbjct: 32053807 GCCCGCCTCGGCCTCCCAAA 32053826

Parameters:
  W=10
  matrix=pam10
  Q=2046
  R=18
  S2=2790
  gapS2=5580
  S=5580
  Y=135374737
  Z=135374737
  kap
  hspmax=0
  cpus=1

  ucdb
  ctxfactor=2.00
  E=0.380942

  Query                        -----  As Used  -----    -----  Computed  ----
  Strand MatID Matrix name     Lambda    K       H      Lambda    K       H
   +1      0   pam10.4.4       0.00693 0.316   0.986    same    same    same
               Q=2046,R=18     0.00693 0.316   0.986     n/a     n/a     n/a
   -1      0   pam10.4.4       0.00693 0.316   0.986    same    same    same
               Q=2046,R=18     0.00693 0.316   0.986     n/a     n/a     n/a

  Query
  Strand MatID  Length  Eff.Length     E      S  W   T    X   E2       S2
   +1      0   18,382 135,374,737      0.19  5580 10 n/a 2002  0.0025  2790
                                                         3535  9.6e-12 5580
   -1      0   18,382 135,374,737      0.19  5580 10 n/a 2002  0.0025  2790
                                                         3535  9.6e-12 5580


Statistics:

  Database:  chr12
   Title:  chr12.fa
   Posted:  7:55:37 PM CEST Jun 09, 2006
   Created:  7:55:37 PM CEST Jun 09, 2006
   Format:  XDF-1
   # of letters in database:  132,289,534  (Z = 135374737)
   # of sequences in database:  1
   # of database sequences satisfying E:  1
  No. of states in DFA:  53,323 (3750 KB)
  Total size of DFA:  4312 KB (5400 KB)
  Time to generate neighborhood:  0.09u 0.01s 0.10t   Elapsed:  00:00:00
  No. of threads or processors used:  1
  Search cpu time:  14.33u 0.18s 14.51t   Elapsed:  00:00:15
  Total cpu time:  14.43u 0.20s 14.63t   Elapsed:  00:01:38
  Start:  Wed Jul 19 10:28:01 2006   End:  Wed Jul 19 10:28:16 2006
WARNINGS ISSUED:  1

EXIT CODE 0
'''

wublast_output4 = '''
BLASTN 2.0MP-WashU [04-May-2006] [linux26-i786-ILP32F64 2006-05-09T12:19:58]

Copyright (C) 1996-2006 Washington University, Saint Louis, Missouri USA.
All Rights Reserved.

Reference:  Gish, W. (1996-2006) http://blast.wustl.edu

Notice:  this program and its default parameter settings are optimized to find
nearly identical sequences rapidly.  To identify weak protein similarities
encoded in nucleic acid, use BLASTX, TBLASTN or TBLASTX.

Query=  1
        (7 letters)

Database:  b.fa
           1 sequences; 7 total letters.
Searching done

                                                                     Smallest
                                                                       Sum
                                                              High  Probability
Sequences producing High-scoring Segment Pairs:              Score  P(N)      N

2                                                               35  0.098     1


>2
        Length = 7

  Plus Strand HSPs:

 Score = 35 (11.3 bits), Expect = 0.10, P = 0.098
 Identities = 7/7 (100%), Positives = 7/7 (100%), Strand = Plus / Plus

Query:     1 GATTACA 7
             |||||||
Sbjct:     1 GATTACA 7

  Minus Strand HSPs:

 Score = 10 (7.5 bits), Expect = 1.2, P = 0.70
 Identities = 2/2 (100%), Positives = 2/2 (100%), Strand = Minus / Plus

Query:     5 TA 4
             ||
Sbjct:     4 TA 5

 Score = 10 (7.5 bits), Expect = 1.2, P = 0.70
 Identities = 2/2 (100%), Positives = 2/2 (100%), Strand = Minus / Plus

Query:     3 AT 2
             ||
Sbjct:     2 AT 3


Parameters:
  W=2

  ucdb
  ctxfactor=2.00
  E=10

  Query                        -----  As Used  -----    -----  Computed  ----
  Strand MatID Matrix name     Lambda    K       H      Lambda    K       H
   +1      0   +5,-4           0.192   0.182   0.357    same    same    same
               Q=10,R=10       0.104   0.0151  0.0600    n/a     n/a     n/a
   -1      0   +5,-4           0.192   0.182   0.357    same    same    same
               Q=10,R=10       0.104   0.0151  0.0600    n/a     n/a     n/a

  Query
  Strand MatID  Length  Eff.Length     E   S W   T   X   E2    S2
   +1      0        7         7       1.8  1 2  21  73  1.9e+03 1
                                                   134  2.2e+02 1
   -1      0        7         7       1.8  1 2  21  73  1.9e+03 1
                                                   134  2.2e+02 1


Statistics:

  Database:  b.fa
   Title:  b.fa
   Posted:  11:43:44 AM CEST Oct 24, 2006
   Created:  11:43:44 AM CEST Oct 24, 2006
   Format:  XDF-1
   # of letters in database:  7
   # of sequences in database:  1
   # of database sequences satisfying E:  1
  No. of states in DFA:  5 (1 KB)
  Total size of DFA:  1 KB (2049 KB)
  Time to generate neighborhood:  0.00u 0.00s 0.00t   Elapsed:  00:00:00
  No. of threads or processors used:  1
  Search cpu time:  0.00u 0.00s 0.00t   Elapsed:  00:00:00
  Total cpu time:  0.00u 0.00s 0.00t   Elapsed:  00:00:00
  Start:  Tue Oct 24 11:47:23 2006   End:  Tue Oct 24 11:47:23 2006
'''

wublast_output5 = '''
BLASTX 2.0MP-WashU [04-May-2006] [linux26-i786-ILP32F64 2006-05-09T12:19:58]

Copyright (C) 1996-2006 Washington University, Saint Louis, Missouri USA.
All Rights Reserved.

Reference:  Gish, W. (1996-2006) http://blast.wustl.edu
Gish, Warren and David J. States (1993).  Identification of protein coding
regions by database similarity search.  Nat. Genet. 3:266-72.

Query=  29 DUMMY
        (279 letters; record 49)

  Translating both strands of query sequence in all 6 reading frames

Database:  human_protein.fa
           14,266 sequences; 7,848,837 total letters.
Searching....10....20....30....40....50....60....70....80....90....100% done

                                                                     Smallest
                                                                       Sum
                                                              High  Probability
Sequences producing High-scoring Segment Pairs:              Score  P(N)      N

ALG1_HUMAN (Q9BT22) Chitobiosyldiphosphodolichol beta-man...   218  6.8e-18   1


>ALG1_HUMAN (Q9BT22) Chitobiosyldiphosphodolichol beta-mannosyltransferase (EC
            2.4.1.142) (GDP-mannose-dolichol diphosphochitobiose
            mannosyltransferase) (GDP-Man:GlcNAc2-PP-dolichol
            mannosyltransferase) (Beta-1,4-mannosyltransferase)
            (Mannosyltransferase-
        Length = 464

  Minus Strand HSPs:

 Score = 218 (81.8 bits), Expect = 6.8e-18, P = 6.8e-18
 Identities = 42/44 (95%), Positives = 42/44 (95%), Frame = -3

Query:   277 QMLFSNFPDPAGKLNQFWKNLRESQQL*WDESWVQTVLPLVMDT 146
             QMLFSNFPDPAGKLNQF KNLRESQQL WDESWVQTVLPLVMDT
Sbjct:   421 QMLFSNFPDPAGKLNQFRKNLRESQQLRWDESWVQTVLPLVMDT 464


Parameters:
  W=10
  cpus=1

  ctxfactor=5.96
  E=10

  Query                        -----  As Used  -----    -----  Computed  ----
  Frame  MatID Matrix name     Lambda    K       H      Lambda    K       H
   +3      0   BLOSUM62        0.318   0.134   0.401    0.329   0.140   0.509  
               Q=9,R=2         0.244   0.0300  0.180     n/a     n/a     n/a
   +2      0   BLOSUM62        0.318   0.134   0.401    0.342   0.150   0.492  
               Q=9,R=2         0.244   0.0300  0.180     n/a     n/a     n/a
   +1      0   BLOSUM62        0.318   0.134   0.401    0.318   0.131   0.419  
               Q=9,R=2         0.244   0.0300  0.180     n/a     n/a     n/a
   -1      0   BLOSUM62        0.318   0.134   0.401    0.342   0.142   0.555  
               Q=9,R=2         0.244   0.0300  0.180     n/a     n/a     n/a
   -2      0   BLOSUM62        0.318   0.134   0.401    0.325   0.140   0.444  
               Q=9,R=2         0.244   0.0300  0.180     n/a     n/a     n/a
   -3      0   BLOSUM62        0.318   0.134   0.401    0.339   0.148   0.543  
               Q=9,R=2         0.244   0.0300  0.180     n/a     n/a     n/a

  Query
  Frame  MatID  Length  Eff.Length     E    S  W   T  X   E2     S2
   +3      0       92        92       8.8  56 10 n/a 22  0.19    32
                                                     29  0.23    33
   +2      0       92        92       8.8  56 10 n/a 22  0.19    32
                                                     29  0.23    33
   +1      0       93        93       9.4  56 10 n/a 22  0.19    32
                                                     29  0.24    33
   -1      0       93        93       9.4  56 10 n/a 22  0.19    32
                                                     29  0.24    33
   -2      0       92        92       8.8  56 10 n/a 22  0.19    32
                                                     29  0.23    33
   -3      0       92        92       8.8  56 10 n/a 22  0.19    32
                                                     29  0.23    33


Statistics:

  Database:  /home/sales/bioinfo/task/sequences_prot/dataset/swissprot/human_protein.fa
   Title:  human_protein.fa
   Posted:  3:10:11 PM CET Nov 20, 2006
   Created:  3:10:11 PM CET Nov 20, 2006
   Format:  XDF-1
   # of letters in database:  7,848,837
   # of sequences in database:  14,266
   # of database sequences satisfying E:  1
  No. of states in DFA:  3722 (393 KB)
  Total size of DFA:  405 KB (2054 KB)
  Time to generate neighborhood:  0.00u 0.00s 0.00t   Elapsed:  00:00:00
  No. of threads or processors used:  1
  Search cpu time:  0.05u 0.00s 0.05t   Elapsed:  00:00:00
  Total cpu time:  0.05u 0.00s 0.05t   Elapsed:  00:00:02
  Start:  Mon Nov 20 15:41:20 2006   End:  Mon Nov 20 15:41:20 2006

EXIT CODE 0
'''

wublast_output6 = '''
BLASTN 2.0MP-WashU [04-May-2006] [linux26-i786-ILP32F64 2006-05-09T12:19:58]

Copyright (C) 1996-2006 Washington University, Saint Louis, Missouri USA.
All Rights Reserved.

Reference:  Gish, W. (1996-2006) http://blast.wustl.edu

Notice:  this program and its default parameter settings are optimized to find
nearly identical sequences rapidly.  To identify weak protein similarities
encoded in nucleic acid, use BLASTX, TBLASTN or TBLASTX.

Query=  706545
        (9339 letters; record 1)

Database:  genome.fa
           22 sequences; 2,644,093,988 total letters.
Searching....10....20....30....40....50....60....70....80....90....100% done

WARNING:  hspmax=1000 was exceeded by 20 of the database sequences, causing
          the associated cutoff score, S2, to be transiently set as high as
          96.

                                                                     Smallest
                                                                       Sum
                                                              High  Expectation
Sequences producing High-scoring Segment Pairs:              Score  E(N)      N

chr1                                                          6690  0.       16
chr18                                                         1483  7.4e-101  5
chrX                                                          1265  2.0e-84   4
chr14                                                         1805  1.9e-68   2
chr5                                                           435  2.5e-07   2
chr13                                                          350  0.00013   1
chr11                                                          314  0.0054    1


>chr13
        Length = 120,614,378

  Minus Strand HSPs:

 Score = 350 (58.6 bits), Expect = 0.00013, P = 0.00013, Group = 1
 Identities = 184/289 (63%), Positives = 184/289 (63%), Strand = Minus / Plus
 Links = (1)-2-3

Query:     8869 TGTTACGGAGTTAGAGAACACCGAAAGACTACCCGACTCTATCCAGTTGTAATTAGCCAA 8810
                ||| ||  | ||||  ||||  ||  ||| |    ||| | ||||||| |||  |||| |
Sbjct: 67950851 TGTCACCTACTTAGGAAACAAGGA--GACCATGA-ACTGTGTCCAGTTCTAACGAGCCGA 67950907

Query:     8809 TTTTTTAATGTTATAGTGCAGCCAA-CCACAATTCCCCT-TAG-GCCC--AGGT--TAAA 8757
                 | |||| ||| |  |||| ||    ||  | || | || ||  |  |  |  |  | ||
Sbjct: 67950908 ATGTTTA-TGTAAG-GTGCCGCTTTTCCGGATTTTCACTGTAAAGTTCTTAATTCCTTAA 67950965

Query:     8756 GGG--GTAGAAGGTGGAGGGCTTTGAAAGGTCGCAATT-ACTTT-ATTTCTGGGGAACTT 8701
                     || ||   |  ||   |||||  |  | ||  | |||   | ||  | || | | 
Sbjct: 67950966 TTTCCGTGGAGTTTACAGCTATTTGAG-GAAC-CACCTCACTGCCAGTTAAGTGGCAAT- 67951022

Query:     8700 ACAGCTACAT-G-AGGAACTTTACCGAAGCAATTAATCATTTCACATCAGTTTTACAATT 8643
                | |||||  | | || |||   ||| | ||  |||||||||||| |  ||||||||||||
Sbjct: 67951023 A-AGCTAGTTTGCAGAAACCAGACC-ATGCG-TTAATCATTTCAAAAGAGTTTTACAATT 67951079

Query:     8642 CTGTGGAGGGGGACAAGTTGCCAGGAGCTTATCTTTTAGGAATTTAGGA 8594
                 | |||||| ||  |||||||||||| |||||||||||| |||||| ||
Sbjct: 67951080 TTCTGGAGGAGGG-AAGTTGCCAGGAACTTATCTTTTAGTAATTTAAGA 67951127


EXIT CODE 0
'''

wublast_output7 = '''
BLASTN 2.0MP-WashU [04-May-2006] [linux26-x64-I32LPF64 2006-05-10T17:22:28]

Copyright (C) 1996-2006 Washington University, Saint Louis, Missouri USA.
All Rights Reserved.

Reference:  Gish, W. (1996-2006) http://blast.wustl.edu

Notice:  this program and its default parameter settings are optimized to find
nearly identical sequences rapidly.  To identify weak protein similarities
encoded in nucleic acid, use BLASTX, TBLASTN or TBLASTX.

Query=  b
        (0 letters; record 2)


Parameters:


FATAL:  Query sequence has zero length.

EXIT CODE 17

BLASTN 2.0MP-WashU [04-May-2006] [linux26-x64-I32LPF64 2006-05-10T17:22:28]

Copyright (C) 1996-2006 Washington University, Saint Louis, Missouri USA.
All Rights Reserved.

Reference:  Gish, W. (1996-2006) http://blast.wustl.edu

Notice:  this program and its default parameter settings are optimized to find
nearly identical sequences rapidly.  To identify weak protein similarities
encoded in nucleic acid, use BLASTX, TBLASTN or TBLASTX.

Query=  c
        (231 letters; record 3)

Database:  pluto.fa
           3 sequences; 517 total letters.
Searching....10....20....30....40....50....60....70....80....90....100% done

                                                                     Smallest
                                                                       Sum
                                                              High  Probability
Sequences producing High-scoring Segment Pairs:              Score  P(N)      N

c                                                             1155  6.1e-52   1
a                                                             1146  1.3e-51   1


>c
        Length = 231

  Plus Strand HSPs:

 Score = 1155 (179.3 bits), Expect = 6.1e-52, P = 6.1e-52
 Identities = 231/231 (100%), Positives = 231/231 (100%), Strand = Plus / Plus

Query:     1 GATTACACATAGATTACAGATAGATTACAGATAGATTACAGATAGATTACAGATAGATTA 60
             ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Sbjct:     1 GATTACACATAGATTACAGATAGATTACAGATAGATTACAGATAGATTACAGATAGATTA 60

Query:    61 CAGATAGATTACAGATAGATTACAGATAGATTACAGATAGATTACAGATAGATTACAGAT 120
             ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Sbjct:    61 CAGATAGATTACAGATAGATTACAGATAGATTACAGATAGATTACAGATAGATTACAGAT 120

Query:   121 AGATTACAGATAGATTACAGATAGATTACAGATAGATTACAGATAGATTACAGATAGATT 180
             ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Sbjct:   121 AGATTACAGATAGATTACAGATAGATTACAGATAGATTACAGATAGATTACAGATAGATT 180

Query:   181 ACAGATAGATTACAGATAGATTACAGATAGATTACAGATAGATTACAGATA 231
             |||||||||||||||||||||||||||||||||||||||||||||||||||
Sbjct:   181 ACAGATAGATTACAGATAGATTACAGATAGATTACAGATAGATTACAGATA 231

Parameters:

  ctxfactor=2.00
  E=10

  Query                        -----  As Used  -----    -----  Computed  ----
  Strand MatID Matrix name     Lambda    K       H      Lambda    K       H
   +1      0   +5,-4           0.192   0.182   0.357    same    same    same
               Q=10,R=10       0.104   0.0151  0.0600    n/a     n/a     n/a
   -1      0   +5,-4           0.192   0.182   0.357    same    same    same
               Q=10,R=10       0.104   0.0151  0.0600    n/a     n/a     n/a

  Query
  Strand MatID  Length  Eff.Length     E    S  W   T   X   E2     S2
   +1      0      231       231       9.2  51 11 n/a  73  4.1     51
                                                     134  20.     51
   -1      0      231       231       9.2  51 11 n/a  73  4.1     51
                                                     134  20.     51


Statistics:

  Database:  pluto.fa
   Title:  pluto.fa
   Posted:  6:04:17 PM UTC Jan 8, 2008
   Created:  6:04:17 PM UTC Jan 8, 2008
   Format:  XDF-1
   # of letters in database:  517
   # of sequences in database:  3
   # of database sequences satisfying E:  2
  No. of states in DFA:  29 (58 KB)
  Total size of DFA:  68 KB (2053 KB)
  Time to generate neighborhood:  0.01u 0.00s 0.01t   Elapsed:  00:00:00
  No. of threads or processors used:  2
  Search cpu time:  0.00u 0.00s 0.00t   Elapsed:  00:00:00
  Total cpu time:  0.01u 0.00s 0.01t   Elapsed:  00:00:00
  Start:  Wed Jan  9 09:33:39 2008   End:  Wed Jan  9 09:33:39 2008

EXIT CODE 0
'''

class TestWuBlastParser(ut.TestCase):
	def testParseSingleDatabase(self):
		wp = WuBlastParser()
		it = wp.parse(StringIO(wublast_output1))
		
		parsed_alignment = it.next()
		expected_alignment = Alignment(
			'chr22_14430000',
			208,
			447,
			'chr21',
			10173877,
			10174127,
			'+',
			None,
			252,
			612,
			75,
			1000,
			[(20,1), (73,4), (91,1), (202,1), (206,2), (212,4)],
			[(10,1), (87,1)]
		)
		self.assertEquals(parsed_alignment, expected_alignment)
		
		parsed_alignment = it.next()
		expected_alignment = Alignment(
			'chr22_14430000',
			2287,
			2585,
			'chr21',
			60619248,
			60619543,
			'-',
			None,
			306,
			516,
			67,
			4100,
			[(8,1), (37,1), (130,1), (175,1), (230,1), (260,1), (263,1), (290,1)],
			[(30,1), (89,1), (106,1), (123,1), (159,1), (168,1), (181,1), (192,1), (197,1), (224,1), (271,1)]
		)
		self.assertEquals(parsed_alignment, expected_alignment)
		
		self.assertRaises(StopIteration, it.next)
	
	def testParseMultiDatabase(self):
		wp = WuBlastParser()
		it = wp.parse(StringIO(wublast_output2))
		
		parsed_alignment = it.next()
		self.assertEquals(2925, parsed_alignment.score)
		
		parsed_alignment = it.next()
		self.assertEquals(304, parsed_alignment.score)
		
		self.assertRaises(StopIteration, it.next)
	
	def testNoAlignment(self):
		wp = WuBlastParser()
		it = wp.parse(StringIO(wublast_output3))
		
		parsed_alignment = it.next()
		expected_alignment = Alignment(
			'chr10_211225',
			5176,
			5376,
			'chr12',
			32053626,
			32053826,
			'+',
			None,
			200,
			24501,
			86,
			2.3e-58,
			[],
			[]
		)
		self.assertEquals(parsed_alignment, expected_alignment)
		
		self.assertRaises(StopIteration, it.next)
	
	def testShort(self):
		wp = WuBlastParser()
		it = wp.parse(StringIO(wublast_output4))
		
		parsed_alignment = it.next()
		self.assertEquals(0, parsed_alignment.query_start)
		self.assertEquals(7, parsed_alignment.query_stop)
		self.assertEquals(0, parsed_alignment.target_start)
		self.assertEquals(7, parsed_alignment.target_stop)
		
		parsed_alignment = it.next()
		self.assertEquals(3, parsed_alignment.query_start)
		self.assertEquals(5, parsed_alignment.query_stop)
		self.assertEquals(3, parsed_alignment.target_start)
		self.assertEquals(5, parsed_alignment.target_stop)
		
		parsed_alignment = it.next()
		self.assertEquals(1, parsed_alignment.query_start)
		self.assertEquals(3, parsed_alignment.query_stop)
		self.assertEquals(1, parsed_alignment.target_start)
		self.assertEquals(3, parsed_alignment.target_stop)
		
		self.assertRaises(StopIteration, it.next)
	
	def testSpaceInFastaHeader(self):
		wp = WuBlastParser()
		it = wp.parse(StringIO(wublast_output5))
		
		parsed_alignment = it.next()
		
		self.assertEquals('29', parsed_alignment.query_label) 
		self.assertEquals('ALG1_HUMAN', parsed_alignment.target_label)
	
	def testNegativeStrandAndFrame(self):
		wp = WuBlastParser()
		it = wp.parse(StringIO(wublast_output5))
		
		parsed_alignment = it.next()
		expected_alignment = Alignment(
			'29',
			145,
			277,
			'ALG1_HUMAN',
			420,
			464,
			'-',
			-3,
			44,
			218,
			95,
			6.8e-18,
			[],
			[]
		)
		self.assertEquals(parsed_alignment, expected_alignment)
		
		self.assertRaises(StopIteration, it.next)
	
	def testLinks(self):
		wp = WuBlastParser()
		it = wp.parse(StringIO(wublast_output6))
		
		parsed_alignment = it.next()
		self.assertEquals(parsed_alignment.links, '(1)-2-3')
		
		self.assertRaises(StopIteration, it.next)
	
	def testEmptySequence(self):
		wp = WuBlastParser()
		it = wp.parse(StringIO(wublast_output7))
		
		parsed_alignment = it.next()
		expected_alignment = Alignment(
			'c',
			0,
			231,
			'c',
			0,
			231,
			'+',
			None,
			231,
			1155,
			100,
			6.1e-52,
			[],
			[]
		)
		self.assertEquals(parsed_alignment, expected_alignment)
		
		self.assertRaises(StopIteration, it.next)

blastz_output1 = '''
#:lav
s {
  "/home_PG/molineri/bioinfo/task/sequences/dataset/ensembl/hsapiens/42/chr21.fa" 1 46944323 0 1
  "/home_PG/molineri/bioinfo/task/sequences/dataset/ensembl/hsapiens/42/chr9.fa.splitted-" 1 30311 1 11
}
h {
   ">chr21"
   ">hsapiens_chr9_260115 (reverse complement)"
}
a {
  s 4781
  b 34422758 29471
  e 34422901 29617
  l 34422758 29471 34422850 29563 62
  l 34422851 29567 34422901 29617 69
}
x {
  n 0
}
#:lav
s {
  "/home_PG/molineri/bioinfo/task/sequences/dataset/ensembl/hsapiens/42/chr21.fa" 1 46944323 0 1
  "/home_PG/molineri/bioinfo/task/sequences/dataset/ensembl/hsapiens/42/chr9.fa.splitted-" 1 57352 1 13
}
h {
   ">chr21"
   ">hsapiens_chr9_312654 (reverse complement)"
}
a {
  s 3204
  b 13659005 19355
  e 13659077 19426
  l 13659005 19355 13659017 19367 85
  l 13659019 19368 13659077 19426 73
}
x {
  n 0
}
#:lav
s {
  "/dev/stdin" 1 10542539 0 1
  "/home/bioinfo/task/sequences/dataset/ensembl/hsapiens/45/chr1.fa.splitted" 1 10542539 0 1
}
h {
   ">chr1_468"
   ">chr1_468"
}
a {
  s 6534
  b 10488800 3443725
  e 10489188 3444130
  l 10488800 3443725 10488817 3443742 83
  l 10488818 3443748 10488840 3443770 70
  l 10488842 3443771 10488855 3443784 71
  l 10488857 3443785 10488869 3443797 54
  l 10488870 3443799 10488888 3443817 63
  l 10488889 3443821 10488937 3443869 80
  l 10488944 3443870 10488956 3443882 69
  l 10488957 3443892 10488993 3443928 54
  l 10488995 3443929 10489000 3443934 83
  l 10489001 3443948 10489006 3443953 83
  l 10489012 3443954 10489018 3443960 71
  l 10489019 3443966 10489029 3443976 55
  l 10489031 3443977 10489045 3443991 67
  l 10489046 3443997 10489074 3444025 55
  l 10489075 3444027 10489105 3444057 68
  l 10489114 3444058 10489131 3444075 56
  l 10489136 3444076 10489169 3444109 76
  l 10489170 3444112 10489188 3444130 74
}
'''

blastz_output2 = '''
#:lav
s {
  "/dev/stdin" 1 10542539 0 1
  "/home/bioinfo/task/sequences/dataset/ensembl/hsapiens/45/chr1.fa.splitted" 1 10542539 0 1
}
h {
   ">chr1_468"
   ">chr1_468"
}
a {
  s 4043
  b 8303817 7459294
  e 8304039 7459464
  l 8303817 7459294 8303843 7459320 67
  l 8303849 7459321 8303887 7459359 69
  l 8303896 7459360 8303908 7459372 92
  l 8303913 7459373 8303934 7459394 59
  l 8303938 7459395 8303965 7459422 82
  l 8303970 7459423 8303995 7459448 77
  l 8304028 7459449 8304027 7459448 0
  l 8304028 7459453 8304039 7459464 75
}
'''

class TestBlastzParser(ut.TestCase):
	def testParse(self):
		bp = BlastzParser()
		it = bp.parse(StringIO(blastz_output1))
		
		parsed_alignment = it.next()
		expected_alignment = Alignment(
			'chr21',
			34422757,
			34422901,
			'hsapiens_chr9_260115',
			694,
			841,
			'-',
			None,
			147,
			None,
			None,
			None,
			[(51, 3)],
			[]
		)
		self.assertEquals(parsed_alignment, expected_alignment)
		
		parsed_alignment = it.next()
		expected_alignment = Alignment(
			'chr21',
			13659004,
			13659077,
			'hsapiens_chr9_312654',
			37926,
			37998,
			'-',
			None,
			73,
			None,
			None,
			None,
			[],
			[(59, 1)]
		)
		self.assertEquals(parsed_alignment, expected_alignment)
		
		parsed_alignment = it.next()
		expected_alignment = Alignment(
			'chr1_468',
			10488799,
			10489000,
			'chr1_468',
			3443724,
			3443934,
			'+',
			None,
			219,
			None,
			None,
			None,
			[(18, 5), (75, 1), (95, 3), (166, 9)],
			[(46, 1), (61, 1), (147, 6), (212, 1)]
		)
		self.assertEquals(parsed_alignment, expected_alignment)
		
		parsed_alignment = it.next()
		expected_alignment = Alignment(
			'chr1_468',
			10489000,
			10489188,
			'chr1_468',
			3443947,
			3444130,
			'+',
			None,
			201,
			None,
			None,
			None,
			[(18, 5), (50, 5), (84, 1), (180, 2)],
			[(6, 5), (34, 1), (116, 8), (142, 4)]
		)
		self.assertEquals(parsed_alignment, expected_alignment)
		
		self.assertRaises(StopIteration, it.next)
	
	def testDoubleGap(self):
		bp = BlastzParser()
		it = bp.parse(StringIO(blastz_output2))
		
		parsed_alignment = it.next()
		expected_alignment = Alignment(
			'chr1_468',
			8303816,
			8303995,
			'chr1_468',
			7459293,
			7459448,
			'+',
			None,
			179,
			None,
			None,
			None,
			[],
			[(27, 5), (71, 8), (92, 4), (118, 3), (149, 4)]
		)
		self.assertEquals(parsed_alignment, expected_alignment)
		
		parsed_alignment = it.next()
		expected_alignment = Alignment(
			'chr1_468',
			8304027,
			8304039,
			'chr1_468',
			7459452,
			7459464,
			'+',
			None,
			12,
			None,
			None,
			None,
			[],
			[]
		)
		self.assertEquals(parsed_alignment, expected_alignment)
		
		self.assertRaises(StopIteration, it.next)

if __name__ == '__main__':
	ut.main()
