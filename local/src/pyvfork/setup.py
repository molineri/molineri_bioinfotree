#!/usr/bin/env python

from distutils.core import setup, Extension

setup(name='pyvfork',
      version='1.0',
      description='vfork bioinformatic library',
      author='Gabriele Sales',
      author_email='gbrsales@gmail.com',
      packages=['vfork',
                'vfork.alignment',
                'vfork.config',
                'vfork.draw',
                'vfork.fasta',
                'vfork.genbank',
                'vfork.geometry',
                'vfork.io',
                'vfork.sequence',
                'vfork.sql',
                'vfork.stat',
                'vfork.ucsc',
                'vfork.ucsc',
		'vfork.terminal'],
      ext_modules=[Extension('vfork.io._colreader',
                             ['vfork/io/colreader.i',
                              'vfork/io/colreader.c',
                              'vfork/io/strtok.c'])])
