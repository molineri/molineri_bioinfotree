# -*- coding: utf-8 -*-
# Copyright: 2009 Nadia Alramli
# Copyright: 2010 Ivan Molineris <ivan.molineris@gmail.com>
# License: BSD

"""Terminal controller module
Example of usage:
    print BG_BLUE + 'Text on blue background' + NORMAL
    print BLUE + UNDERLINE + 'Blue underlined text' + NORMAL
    print BLUE + BG_YELLOW + BOLD + 'text' + NORMAL
"""

import sys
from time import time, localtime, strftime
from vfork.stat import SimpleMovingAverage

# The current module
MODULE = sys.modules[__name__]

COLORS = "BLUE GREEN CYAN RED MAGENTA YELLOW WHITE BLACK".split()
# List of terminal controls, you can add more to the list.
CONTROLS = {
    'BOL':'cr', 'UP':'cuu1', 'DOWN':'cud1', 'LEFT':'cub1', 'RIGHT':'cuf1',
    'CLEAR_SCREEN':'clear', 'CLEAR_EOL':'el', 'CLEAR_BOL':'el1',
    'CLEAR_EOS':'ed', 'BOLD':'bold', 'BLINK':'blink', 'DIM':'dim',
    'REVERSE':'rev', 'UNDERLINE':'smul', 'NORMAL':'sgr0',
    'HIDE_CURSOR':'cinvis', 'SHOW_CURSOR':'cnorm'
}

# List of numeric capabilities
VALUES = {
    'COLUMNS':'cols', # Width of the terminal (None for unknown)
    'LINES':'lines',  # Height of the terminal (None for unknown)
    'MAX_COLORS': 'colors',
}

def default():
    """Set the default attribute values"""
    for color in COLORS:
        setattr(MODULE, color, '')
        setattr(MODULE, 'BG_%s' % color, '')
    for control in CONTROLS:
        setattr(MODULE, control, '')
    for value in VALUES:
        setattr(MODULE, value, None)

def setup():
    """Set the terminal control strings"""
    # Initializing the terminal
    curses.setupterm()
    # Get the color escape sequence template or '' if not supported
    # setab and setaf are for ANSI escape sequences
    bgColorSeq = curses.tigetstr('setab') or curses.tigetstr('setb') or ''
    fgColorSeq = curses.tigetstr('setaf') or curses.tigetstr('setf') or ''

    for color in COLORS:
        # Get the color index from curses
        colorIndex = getattr(curses, 'COLOR_%s' % color)
        # Set the color escape sequence after filling the template with index
        setattr(MODULE, color, curses.tparm(fgColorSeq, colorIndex))
        # Set background escape sequence
        setattr(
            MODULE, 'BG_%s' % color, curses.tparm(bgColorSeq, colorIndex)
        )
    for control in CONTROLS:
        # Set the control escape sequence
        setattr(MODULE, control, curses.tigetstr(CONTROLS[control]) or '')
    for value in VALUES:
        # Set terminal related values
        setattr(MODULE, value, curses.tigetnum(VALUES[value]))

def render(text):
    """Helper function to apply controls easily
    Example:
    apply("%(GREEN)s%(BOLD)stext%(NORMAL)s") -> a bold green text
    """
    return text % MODULE.__dict__

class ProgressBar(object):
    """Draws an animated terminal progress bar
       Simple usage:
	p = ProgressBar()
	for i in range(101):
		p.render(i, 'step %s' % i)
		time.sleep(0.05)

       Complex usage:
	p = ProgressBar('green', width=20, block='▣', empty='□', out=sys.stdout)
	for i in range(101):
		p.render(i, 'step %s\nProcessing...\nDescription: write something.' % i)
		time.sleep(0.05)
    """

    TEMPLATE = (
     '%(percent)-2s%% %(color)s%(progress)s%(normal)s%(empty)s %(message)s\n'
    )
    PADDING = 7

    def __init__(self, color=None, width=None, block='█', empty=' ', out=sys.stderr, min_refresh=0, eta_moving_average_period = None, min_refresh_time=1):
        """
        color                     -- color name (BLUE GREEN CYAN RED MAGENTA YELLOW WHITE BLACK)
        width                     -- bar width (optinal)
        block                     -- progress display character (default '█')
        empty                     -- bar display character (default ' ')
	out                       -- the output file handle (default sys.stderr)
	min_refresh               -- refresh the bar and messages every min_refresh percent (default 0)
	min_refresh_time          -- 1/min_refresh_time is the maximum refresh rate (default 0 seconds)
	eta_moving_average_period -- the period of the moving average for eta computation (default None)
        """
        if color:
            self.color = getattr(MODULE, color.upper())
        else:
            self.color = ''
        if width and width < COLUMNS - self.PADDING:
            self.width = width
        else:
            # Adjust to the width of the terminal
            self.width = COLUMNS - self.PADDING

	if eta_moving_average_period is not None:
		self.eta_delata_moving_average = SimpleMovingAverage(eta_moving_average_period)
	else:
		self.eta_delata_moving_average = None
		
        self.block = block
        self.empty = empty
	self.min_refresh = min_refresh
	self.min_refresh_time = min_refresh_time
	self.out = out
        self.progress = None
        self.lines = 0
	self.prev_percent = None
	self.prev_message = None
	self.prev_time = None

    def render(self, percent, message = '', eta=None):
        """Print the progress bar
        percent -- the progress percentage %
        message -- message string (optional)
	eta     -- report expected termination time ()
        """

	if percent > 100:
		percent = 100

        inline_msg_len = 0
        if message:
            # The length of the first line in the message
            inline_msg_len = len(message.splitlines()[0])
        if inline_msg_len + self.width + self.PADDING > COLUMNS:
            # The message is too long to fit in one line.
            # Adjust the bar width to fit.
            bar_width = COLUMNS - inline_msg_len -self.PADDING
        else:
            bar_width = self.width

	if self.prev_percent is None or self.prev_message is None \
	   or self.prev_percent < percent - self.min_refresh \
	   or (self.prev_message != message and self.min_refresh == 0):
		
		if self.prev_time is not None:
			t = time()
			delta_t = t - self.prev_time
			if delta_t < self.min_refresh_time and percent < 100: #if percent == 100 procede aniway
				return

		# Check if render is called for the first time
		if self.progress != None:
			self.clear()
		
		if eta is not None:
			if self.prev_time is not None:
				delta = delta_t / (percent - self.prev_percent)
				if self.eta_delata_moving_average is not None:
					delta = self.eta_delata_moving_average(delta)
				remaining_sec = delta * (100 - percent)
				eta_sec = t + remaining_sec 
				message += '\n[ETA: %s (hours remaining: %f)]' % (strftime("%Y-%m-%d %H:%M:%S %Z", localtime(eta_sec)), remaining_sec / 3600)
				self.prev_time=t
			else:
				self.prev_time=time()
		self.progress = (bar_width * int(percent)) / 100
		data = self.TEMPLATE % {
		    'percent': int(percent),
		    'color': self.color,
		    'progress': self.block * int(self.progress),
		    'normal': NORMAL,
		    'empty': self.empty * (bar_width - int(self.progress)),
		    'message': message
		}
		self.out.write(data)
		self.out.flush()
		# The number of lines printed
		self.lines = len(data.splitlines())
		self.prev_percent = percent
		self.prev_message = message

    def clear(self):
        """Clear all printed lines"""
        self.out.write(
            self.lines * (UP + BOL + CLEAR_EOL)
        )

try:
    import curses
    setup()
except Exception, e:
    # There is a failure; set all attributes to default
    print 'Warning: %s' % e
    default()
