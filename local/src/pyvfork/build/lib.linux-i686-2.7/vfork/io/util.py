__author__ = "Gabriele Sales <gbrsales@gmail.com>"
__copyright__ = "2009 Gabriele Sales"

from shutil import rmtree
from tempfile import mkdtemp


def safe_rstrip(line):
	return line.rstrip('\r\n')

class NamedTemporaryDirectory(object):
	''' A temporary directory whose content will be automatically deleted.

            The returned object is meant to be used in a B{with} statement.
	'''

	def __init__(self, dir=None):
		''' Object constructor.

		    @param dir: where to create the temporary directory.
		'''
		self._dir = dir

	def __enter__(self):
		self.path = mkdtemp(dir=self._dir)
		return self

	def __exit__(self, type, value, traceback):
		rmtree(self.path, ignore_errors=True)
